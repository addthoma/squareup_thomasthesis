.class public Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;
.super Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;
.source "StickyHeaderDecoration.java"


# static fields
.field public static final NO_HEADER_ID:J = -0x1L


# instance fields
.field private mAdapter:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;

.field private mHeaderCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private mRenderInline:Z


# direct methods
.method public constructor <init>(Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;)V
    .locals 1

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1, v0}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;-><init>(Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;Z)V

    return-void
.end method

.method public constructor <init>(Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;Z)V
    .locals 0

    .line 53
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;-><init>()V

    .line 54
    iput-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;

    .line 55
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mHeaderCache:Ljava/util/Map;

    .line 56
    iput-boolean p2, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mRenderInline:Z

    return-void
.end method

.method private getHeader(Landroidx/recyclerview/widget/RecyclerView;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 8

    .line 115
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;

    invoke-interface {v0, p2}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;->getHeaderId(I)J

    move-result-wide v0

    .line 117
    iget-object v2, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mHeaderCache:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 118
    iget-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mHeaderCache:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1

    .line 120
    :cond_0
    iget-object v2, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;

    invoke-interface {v2, p1}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;->onCreateHeaderViewHolder(Landroid/view/ViewGroup;)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v2

    .line 121
    iget-object v3, v2, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 124
    iget-object v4, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;

    invoke-interface {v4, v2, p2}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;->onBindHeaderViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V

    .line 126
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getMeasuredWidth()I

    move-result p2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {p2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 127
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getMeasuredHeight()I

    move-result v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 130
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingLeft()I

    move-result v6

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingRight()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    iget v7, v7, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 129
    invoke-static {p2, v6, v7}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result p2

    .line 132
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingTop()I

    move-result v6

    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getPaddingBottom()I

    move-result p1

    add-int/2addr v6, p1

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    iget p1, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 131
    invoke-static {v4, v6, p1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result p1

    .line 134
    invoke-virtual {v3, p2, p1}, Landroid/view/View;->measure(II)V

    .line 135
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result p1

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    invoke-virtual {v3, v5, v5, p1, p2}, Landroid/view/View;->layout(IIII)V

    .line 137
    iget-object p1, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mHeaderCache:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p1, p2, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v2
.end method

.method private getHeaderHeightForLayout(Landroid/view/View;)I
    .locals 1

    .line 206
    iget-boolean v0, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mRenderInline:Z

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    :goto_0
    return p1
.end method

.method private getHeaderTop(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroid/view/View;II)I
    .locals 6

    .line 177
    invoke-direct {p0, p3}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->getHeaderHeightForLayout(Landroid/view/View;)I

    move-result p3

    .line 178
    invoke-virtual {p2}, Landroid/view/View;->getY()F

    move-result p2

    float-to-int p2, p2

    sub-int/2addr p2, p3

    if-nez p5, :cond_2

    .line 180
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getChildCount()I

    move-result p5

    .line 181
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;

    invoke-interface {v0, p4}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;->getHeaderId(I)J

    move-result-wide v0

    const/4 p4, 0x1

    :goto_0
    if-ge p4, p5, :cond_1

    .line 184
    invoke-virtual {p1, p4}, Landroidx/recyclerview/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 186
    iget-object v3, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;

    invoke-interface {v3, v2}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;->getHeaderId(I)J

    move-result-wide v3

    cmp-long v5, v3, v0

    if-eqz v5, :cond_0

    .line 188
    invoke-virtual {p1, p4}, Landroidx/recyclerview/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object p4

    .line 189
    invoke-virtual {p4}, Landroid/view/View;->getY()F

    move-result p4

    float-to-int p4, p4

    invoke-direct {p0, p1, v2}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->getHeader(Landroidx/recyclerview/widget/RecyclerView;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p1

    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    add-int/2addr p3, p1

    sub-int/2addr p4, p3

    if-gez p4, :cond_1

    return p4

    :cond_0
    add-int/lit8 p4, p4, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 199
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    :cond_2
    return p2
.end method

.method private hasHeader(I)Z
    .locals 4

    .line 111
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;

    invoke-interface {v0, p1}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;->getHeaderId(I)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long p1, v0, v2

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private showHeaderAboveItem(I)Z
    .locals 5

    const/4 v0, 0x1

    if-nez p1, :cond_0

    return v0

    .line 82
    :cond_0
    iget-object v1, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;

    add-int/lit8 v2, p1, -0x1

    invoke-interface {v1, v2}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;->getHeaderId(I)J

    move-result-wide v1

    iget-object v3, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;

    invoke-interface {v3, p1}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;->getHeaderId(I)J

    move-result-wide v3

    cmp-long p1, v1, v3

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public clearHeaderCache()V
    .locals 1

    .line 90
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mHeaderCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public findHeaderViewUnder(FF)Landroid/view/View;
    .locals 5

    .line 94
    iget-object v0, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mHeaderCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    .line 95
    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 96
    invoke-static {v1}, Landroidx/core/view/ViewCompat;->getTranslationX(Landroid/view/View;)F

    move-result v2

    .line 97
    invoke-static {v1}, Landroidx/core/view/ViewCompat;->getTranslationY(Landroid/view/View;)F

    move-result v3

    .line 99
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, v2

    cmpl-float v4, p1, v4

    if-ltz v4, :cond_0

    .line 100
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v4, v2

    cmpg-float v2, p1, v4

    if-gtz v2, :cond_0

    .line 101
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v3

    cmpl-float v2, p2, v2

    if-ltz v2, :cond_0

    .line 102
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v2, v3

    cmpg-float v2, p2, v2

    if-gtz v2, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V
    .locals 1

    .line 64
    invoke-virtual {p3, p2}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result p2

    const/4 p4, 0x0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 68
    invoke-direct {p0, p2}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->hasHeader(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    invoke-direct {p0, p2}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->showHeaderAboveItem(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-direct {p0, p3, p2}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->getHeader(Landroidx/recyclerview/widget/RecyclerView;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p2

    iget-object p2, p2, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 72
    invoke-direct {p0, p2}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->getHeaderHeightForLayout(Landroid/view/View;)I

    move-result p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 75
    :goto_0
    invoke-virtual {p1, p4, p2, p4, p4}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method public onDrawOver(Landroid/graphics/Canvas;Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;)V
    .locals 11

    .line 148
    invoke-virtual {p2}, Landroidx/recyclerview/widget/RecyclerView;->getChildCount()I

    move-result p3

    const-wide/16 v0, -0x1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p3, :cond_1

    .line 152
    invoke-virtual {p2, v2}, Landroidx/recyclerview/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 153
    invoke-virtual {p2, v5}, Landroidx/recyclerview/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v7

    const/4 v3, -0x1

    if-eq v7, v3, :cond_0

    .line 155
    invoke-direct {p0, v7}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->hasHeader(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 156
    iget-object v3, p0, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->mAdapter:Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;

    invoke-interface {v3, v7}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderAdapter;->getHeaderId(I)J

    move-result-wide v9

    cmp-long v3, v9, v0

    if-eqz v3, :cond_0

    .line 160
    invoke-direct {p0, p2, v7}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->getHeader(Landroidx/recyclerview/widget/RecyclerView;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object v0

    iget-object v0, v0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 161
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 163
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v1

    move-object v3, p0

    move-object v4, p2

    move-object v6, v0

    move v8, v2

    .line 164
    invoke-direct/range {v3 .. v8}, Lca/barrenechea/widget/recyclerview/decoration/StickyHeaderDecoration;->getHeaderTop(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroid/view/View;II)I

    move-result v3

    int-to-float v1, v1

    int-to-float v3, v3

    .line 165
    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 167
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 168
    invoke-virtual {v0, v3}, Landroid/view/View;->setTranslationY(F)V

    .line 169
    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 170
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    move-wide v0, v9

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
