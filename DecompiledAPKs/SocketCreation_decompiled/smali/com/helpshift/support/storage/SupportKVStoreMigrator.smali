.class public Lcom/helpshift/support/storage/SupportKVStoreMigrator;
.super Ljava/lang/Object;
.source "SupportKVStoreMigrator.java"

# interfaces
.implements Lcom/helpshift/support/storage/SDKMigrator;


# static fields
.field private static final TAG:Ljava/lang/String; = "Helpshift_KVStoreMigratorr"


# instance fields
.field private customMetaData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/io/Serializable;",
            ">;"
        }
    .end annotation
.end field

.field private deviceId:Ljava/lang/String;

.field private enableTypingIndicator:Ljava/lang/Boolean;

.field private fullPrivacy:Ljava/lang/Boolean;

.field private gotoConversationAfterContactUs:Ljava/lang/Boolean;

.field private hideNameEmail:Ljava/lang/Boolean;

.field private hsStorage:Lcom/helpshift/support/HSStorage;

.field private kvStore:Lcom/helpshift/common/platform/KVStore;

.field private metaDataDAO:Lcom/helpshift/meta/dao/MetaDataDAO;

.field private networkRequestDAO:Lcom/helpshift/common/platform/network/NetworkRequestDAO;

.field private requireEmail:Ljava/lang/Boolean;

.field private sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

.field private serverTimeDelta:F

.field private showConversationInfoScreen:Ljava/lang/Boolean;

.field private showConversationResolutionQuestion:Ljava/lang/Boolean;

.field private showSearchOnNewConversation:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lcom/helpshift/support/HSStorage;)V
    .locals 1

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    .line 61
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object p1

    invoke-interface {p1}, Lcom/helpshift/CoreApi;->getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    .line 63
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getPlatform()Lcom/helpshift/common/platform/Platform;

    move-result-object p1

    .line 64
    invoke-interface {p1}, Lcom/helpshift/common/platform/Platform;->getNetworkRequestDAO()Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->networkRequestDAO:Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    .line 65
    invoke-interface {p1}, Lcom/helpshift/common/platform/Platform;->getMetaDataDAO()Lcom/helpshift/meta/dao/MetaDataDAO;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->metaDataDAO:Lcom/helpshift/meta/dao/MetaDataDAO;

    .line 67
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getPlatform()Lcom/helpshift/common/platform/Platform;

    move-result-object p1

    invoke-interface {p1}, Lcom/helpshift/common/platform/Platform;->getKVStore()Lcom/helpshift/common/platform/KVStore;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->kvStore:Lcom/helpshift/common/platform/KVStore;

    return-void
.end method


# virtual methods
.method public backup(Lcom/helpshift/util/VersionName;)V
    .locals 4

    .line 74
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    const-string v0, "requireEmail"

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->contains(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 75
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->storageGetBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->requireEmail:Ljava/lang/Boolean;

    goto :goto_0

    .line 78
    :cond_0
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {p1, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->requireEmail:Ljava/lang/Boolean;

    .line 80
    :goto_0
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    const-string v0, "fullPrivacy"

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->contains(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 81
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->storageGetBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->fullPrivacy:Ljava/lang/Boolean;

    goto :goto_1

    .line 84
    :cond_1
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {p1, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->fullPrivacy:Ljava/lang/Boolean;

    .line 86
    :goto_1
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    const-string v0, "hideNameAndEmail"

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->contains(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 87
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->storageGetBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hideNameEmail:Ljava/lang/Boolean;

    goto :goto_2

    .line 90
    :cond_2
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {p1, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hideNameEmail:Ljava/lang/Boolean;

    .line 92
    :goto_2
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    const-string v0, "showSearchOnNewConversation"

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->contains(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 93
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->storageGetBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->showSearchOnNewConversation:Ljava/lang/Boolean;

    goto :goto_3

    .line 96
    :cond_3
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {p1, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->showSearchOnNewConversation:Ljava/lang/Boolean;

    .line 98
    :goto_3
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    const-string v0, "gotoConversationAfterContactUs"

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->contains(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 99
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->storageGetBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->gotoConversationAfterContactUs:Ljava/lang/Boolean;

    goto :goto_4

    .line 102
    :cond_4
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {p1, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->gotoConversationAfterContactUs:Ljava/lang/Boolean;

    .line 104
    :goto_4
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    const-string v0, "showConversationResolutionQuestion"

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->contains(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 105
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->storageGetBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->showConversationResolutionQuestion:Ljava/lang/Boolean;

    goto :goto_5

    .line 108
    :cond_5
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {p1, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->showConversationResolutionQuestion:Ljava/lang/Boolean;

    .line 110
    :goto_5
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    const-string v0, "showConversationInfoScreen"

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->contains(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 111
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->storageGetBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->showConversationInfoScreen:Ljava/lang/Boolean;

    goto :goto_6

    .line 114
    :cond_6
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {p1, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->showConversationInfoScreen:Ljava/lang/Boolean;

    .line 116
    :goto_6
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    const-string v0, "enableTypingIndicator"

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->contains(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_7

    .line 117
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->storageGetBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->enableTypingIndicator:Ljava/lang/Boolean;

    goto :goto_7

    .line 120
    :cond_7
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {p1, v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->enableTypingIndicator:Ljava/lang/Boolean;

    .line 124
    :goto_7
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const-string v0, "key_support_device_id"

    invoke-interface {p1, v0}, Lcom/helpshift/common/platform/KVStore;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->deviceId:Ljava/lang/String;

    .line 127
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    const-string v0, "serverTimeDelta"

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->contains(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_8

    .line 128
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->storageGetFloat(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    iput p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->serverTimeDelta:F

    goto :goto_8

    .line 131
    :cond_8
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->networkRequestDAO:Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    invoke-interface {p1}, Lcom/helpshift/common/platform/network/NetworkRequestDAO;->getServerTimeDelta()F

    move-result p1

    iput p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->serverTimeDelta:F

    .line 135
    :goto_8
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    const-string v0, "customMetaData"

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->contains(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_a

    .line 136
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hsStorage:Lcom/helpshift/support/HSStorage;

    invoke-virtual {p1, v0}, Lcom/helpshift/support/HSStorage;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 138
    :try_start_0
    invoke-static {p1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 139
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 140
    invoke-virtual {v0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object p1

    .line 141
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->customMetaData:Ljava/util/HashMap;

    .line 142
    :cond_9
    :goto_9
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 143
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 144
    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 145
    instance-of v3, v2, Ljava/io/Serializable;

    if-eqz v3, :cond_9

    .line 146
    iget-object v3, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->customMetaData:Ljava/util/HashMap;

    check-cast v2, Ljava/io/Serializable;

    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_9

    :catch_0
    move-exception p1

    const-string v0, "Helpshift_KVStoreMigratorr"

    const-string v1, "Exception converting meta from storage"

    .line 152
    invoke-static {v0, v1, p1}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_a

    .line 156
    :cond_a
    iget-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->metaDataDAO:Lcom/helpshift/meta/dao/MetaDataDAO;

    invoke-interface {p1}, Lcom/helpshift/meta/dao/MetaDataDAO;->getCustomMetaData()Ljava/util/HashMap;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->customMetaData:Ljava/util/HashMap;

    :cond_b
    :goto_a
    return-void
.end method

.method public restore()V
    .locals 3

    .line 163
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 164
    iget-object v1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->requireEmail:Ljava/lang/Boolean;

    const-string v2, "requireEmail"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    iget-object v1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->fullPrivacy:Ljava/lang/Boolean;

    const-string v2, "fullPrivacy"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    iget-object v1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->hideNameEmail:Ljava/lang/Boolean;

    const-string v2, "hideNameAndEmail"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    iget-object v1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->showSearchOnNewConversation:Ljava/lang/Boolean;

    const-string v2, "showSearchOnNewConversation"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    iget-object v1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->gotoConversationAfterContactUs:Ljava/lang/Boolean;

    const-string v2, "gotoConversationAfterContactUs"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iget-object v1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->showConversationResolutionQuestion:Ljava/lang/Boolean;

    const-string v2, "showConversationResolutionQuestion"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    iget-object v1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->showConversationInfoScreen:Ljava/lang/Boolean;

    const-string v2, "showConversationInfoScreen"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    iget-object v1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->enableTypingIndicator:Ljava/lang/Boolean;

    const-string v2, "enableTypingIndicator"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    new-instance v1, Ljava/util/HashMap;

    invoke-static {}, Lcom/helpshift/support/util/ConfigUtil;->getDefaultApiConfig()Ljava/util/Map;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 173
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    .line 175
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/helpshift/CoreApi;->updateApiConfig(Ljava/util/Map;)V

    .line 178
    iget-object v0, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->networkRequestDAO:Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    iget v1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->serverTimeDelta:F

    invoke-interface {v0, v1}, Lcom/helpshift/common/platform/network/NetworkRequestDAO;->storeServerTimeDelta(F)V

    .line 181
    iget-object v0, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->metaDataDAO:Lcom/helpshift/meta/dao/MetaDataDAO;

    iget-object v1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->customMetaData:Ljava/util/HashMap;

    invoke-interface {v0, v1}, Lcom/helpshift/meta/dao/MetaDataDAO;->saveCustomMetaData(Ljava/util/HashMap;)V

    .line 184
    iget-object v0, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->deviceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->kvStore:Lcom/helpshift/common/platform/KVStore;

    iget-object v1, p0, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->deviceId:Ljava/lang/String;

    const-string v2, "key_support_device_id"

    invoke-interface {v0, v2, v1}, Lcom/helpshift/common/platform/KVStore;->setString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
