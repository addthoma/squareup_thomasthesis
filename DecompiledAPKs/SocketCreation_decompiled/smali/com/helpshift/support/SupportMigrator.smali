.class public Lcom/helpshift/support/SupportMigrator;
.super Ljava/lang/Object;
.source "SupportMigrator.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "Helpshift_SupportMigr"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static deleteDBLockFilesOnSDKMigration(Landroid/content/Context;)V
    .locals 2

    .line 209
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "__hs_supportkvdb_lock"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 210
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 211
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 214
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "__hs_kvdb_lock"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 215
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 216
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 221
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ".backups/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "/helpshift/databases/"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 224
    invoke-static {p0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object p0

    if-eqz p0, :cond_3

    .line 225
    invoke-virtual {p0}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 226
    new-instance v0, Ljava/io/File;

    const-string v1, "__hs__db_profiles"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 227
    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 228
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 231
    :cond_2
    new-instance v0, Ljava/io/File;

    const-string v1, "__hs__kv_backup"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 232
    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result p0

    if-eqz p0, :cond_3

    .line 233
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    .line 238
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error on deleting lock file: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "Helpshift_SupportMigr"

    invoke-static {v0, p0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private static fixDuplicatePreIssues(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/util/VersionName;)V
    .locals 4

    .line 155
    new-instance v0, Lcom/helpshift/util/VersionName;

    const-string v1, "7.0.0"

    invoke-direct {v0, v1}, Lcom/helpshift/util/VersionName;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/helpshift/util/VersionName;->isGreaterThanOrEqualTo(Lcom/helpshift/util/VersionName;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lcom/helpshift/util/VersionName;

    const-string v1, "7.1.0"

    invoke-direct {v0, v1}, Lcom/helpshift/util/VersionName;-><init>(Ljava/lang/String;)V

    .line 156
    invoke-virtual {p2, v0}, Lcom/helpshift/util/VersionName;->isLessThanOrEqualTo(Lcom/helpshift/util/VersionName;)Z

    move-result p2

    if-eqz p2, :cond_5

    .line 159
    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object p2

    invoke-virtual {p2}, Lcom/helpshift/account/domainmodel/UserManagerDM;->getAllUsers()Ljava/util/List;

    move-result-object p2

    .line 160
    invoke-interface {p0}, Lcom/helpshift/common/platform/Platform;->getConversationDAO()Lcom/helpshift/conversation/dao/ConversationDAO;

    move-result-object p0

    .line 163
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 165
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/account/domainmodel/UserDM;

    .line 166
    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getConversationInboxManagerDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;->getConversationInboxDM(Lcom/helpshift/account/domainmodel/UserDM;)Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v2

    .line 167
    invoke-virtual {v2}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getActiveConversationFromStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v2

    if-nez v2, :cond_1

    goto :goto_0

    .line 175
    :cond_1
    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {p0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v1

    .line 176
    invoke-static {v1}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    .line 180
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 183
    iget-object v3, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v3}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_1

    .line 188
    :cond_3
    iget-object v3, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 192
    invoke-interface {p0}, Lcom/helpshift/conversation/dao/ConversationDAO;->dropAndCreateDatabase()V

    .line 193
    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object p0

    invoke-virtual {p0}, Lcom/helpshift/account/domainmodel/UserManagerDM;->resetSyncStateForAllUsers()V

    .line 194
    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object p0

    invoke-virtual {p0}, Lcom/helpshift/account/domainmodel/UserManagerDM;->getActiveUserSetupDM()Lcom/helpshift/account/domainmodel/UserSetupDM;

    move-result-object p0

    invoke-virtual {p0}, Lcom/helpshift/account/domainmodel/UserSetupDM;->startSetup()V

    return-void

    .line 199
    :cond_4
    iget-object v2, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    return-void
.end method

.method public static migrate(Landroid/content/Context;Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/support/HSApiData;Lcom/helpshift/support/HSStorage;)V
    .locals 16

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v12, p4

    .line 36
    invoke-virtual/range {p4 .. p4}, Lcom/helpshift/support/HSStorage;->getLibraryVersion()Ljava/lang/String;

    move-result-object v13

    .line 39
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v0

    const-string v14, "7.5.0"

    if-lez v0, :cond_1

    .line 40
    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 41
    new-instance v3, Lcom/helpshift/util/VersionName;

    const-string v0, "0"

    invoke-direct {v3, v0}, Lcom/helpshift/util/VersionName;-><init>(Ljava/lang/String;)V

    .line 43
    :try_start_0
    new-instance v0, Lcom/helpshift/util/VersionName;

    invoke-direct {v0, v13}, Lcom/helpshift/util/VersionName;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 46
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error in creating SemVer: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "Helpshift_SupportMigr"

    invoke-static {v4, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 52
    :goto_0
    new-instance v3, Lcom/helpshift/util/VersionName;

    const-string v4, "7.0.0"

    invoke-direct {v3, v4}, Lcom/helpshift/util/VersionName;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/helpshift/util/VersionName;->isGreaterThanOrEqualTo(Lcom/helpshift/util/VersionName;)Z

    move-result v3

    xor-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    .line 57
    new-instance v15, Lcom/helpshift/support/storage/LegacyUserDataMigrator;

    .line 58
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v4

    .line 60
    invoke-interface/range {p1 .. p1}, Lcom/helpshift/common/platform/Platform;->getKVStore()Lcom/helpshift/common/platform/KVStore;

    move-result-object v6

    .line 61
    invoke-static/range {p0 .. p0}, Lcom/helpshift/account/dao/legacy/AndroidLegacyProfileDAO;->getInstance(Landroid/content/Context;)Lcom/helpshift/account/dao/legacy/AndroidLegacyProfileDAO;

    move-result-object v7

    .line 62
    invoke-interface/range {p1 .. p1}, Lcom/helpshift/common/platform/Platform;->getBackupDAO()Lcom/helpshift/common/dao/BackupDAO;

    move-result-object v8

    .line 63
    invoke-interface/range {p1 .. p1}, Lcom/helpshift/common/platform/Platform;->getLegacyUserMigrationDataSource()Lcom/helpshift/migration/LegacyProfileMigrationDAO;

    move-result-object v9

    .line 64
    invoke-interface/range {p1 .. p1}, Lcom/helpshift/common/platform/Platform;->getLegacyAnalyticsEventIDDAO()Lcom/helpshift/migration/LegacyAnalyticsEventIDDAO;

    move-result-object v10

    move-object v3, v15

    move-object/from16 v5, p4

    move-object v11, v0

    invoke-direct/range {v3 .. v11}, Lcom/helpshift/support/storage/LegacyUserDataMigrator;-><init>(Lcom/helpshift/CoreApi;Lcom/helpshift/support/HSStorage;Lcom/helpshift/common/platform/KVStore;Lcom/helpshift/migration/legacyUser/LegacyProfileDAO;Lcom/helpshift/common/dao/BackupDAO;Lcom/helpshift/migration/LegacyProfileMigrationDAO;Lcom/helpshift/migration/LegacyAnalyticsEventIDDAO;Lcom/helpshift/util/VersionName;)V

    .line 66
    new-instance v3, Lcom/helpshift/support/storage/SupportKVStoreMigrator;

    invoke-direct {v3, v12}, Lcom/helpshift/support/storage/SupportKVStoreMigrator;-><init>(Lcom/helpshift/support/HSStorage;)V

    .line 69
    invoke-virtual {v15, v0}, Lcom/helpshift/support/storage/LegacyUserDataMigrator;->backup(Lcom/helpshift/util/VersionName;)V

    .line 70
    invoke-virtual {v3, v0}, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->backup(Lcom/helpshift/util/VersionName;)V

    .line 73
    invoke-virtual/range {p3 .. p3}, Lcom/helpshift/support/HSApiData;->clearETagsForFaqs()V

    .line 74
    invoke-virtual/range {p4 .. p4}, Lcom/helpshift/support/HSStorage;->clearDatabase()V

    .line 75
    invoke-virtual {v15}, Lcom/helpshift/support/storage/LegacyUserDataMigrator;->dropProfileDB()V

    .line 77
    invoke-interface/range {p1 .. p1}, Lcom/helpshift/common/platform/Platform;->getConversationDAO()Lcom/helpshift/conversation/dao/ConversationDAO;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/conversation/dao/ConversationDAO;->dropAndCreateDatabase()V

    .line 81
    invoke-virtual/range {p2 .. p2}, Lcom/helpshift/common/domain/Domain;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserManagerDM;->resetSyncStateForAllUsers()V

    .line 83
    invoke-interface/range {p1 .. p1}, Lcom/helpshift/common/platform/Platform;->getKVStore()Lcom/helpshift/common/platform/KVStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/common/platform/KVStore;->removeAllKeys()V

    .line 86
    invoke-virtual {v15}, Lcom/helpshift/support/storage/LegacyUserDataMigrator;->restore()V

    .line 87
    invoke-virtual {v3}, Lcom/helpshift/support/storage/SupportKVStoreMigrator;->restore()V

    .line 90
    invoke-virtual/range {p2 .. p2}, Lcom/helpshift/common/domain/Domain;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserManagerDM;->getActiveUserSetupDM()Lcom/helpshift/account/domainmodel/UserSetupDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserSetupDM;->startSetup()V

    goto :goto_1

    .line 96
    :cond_0
    invoke-static {v1, v2, v0}, Lcom/helpshift/support/SupportMigrator;->fixDuplicatePreIssues(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/util/VersionName;)V

    .line 103
    invoke-static {v1, v2, v0}, Lcom/helpshift/support/SupportMigrator;->updateRejectConversations(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/util/VersionName;)V

    .line 107
    :cond_1
    :goto_1
    invoke-virtual/range {p4 .. p4}, Lcom/helpshift/support/HSStorage;->clearLegacySearchIndexFile()V

    .line 108
    invoke-static/range {p0 .. p0}, Lcom/helpshift/support/SupportMigrator;->deleteDBLockFilesOnSDKMigration(Landroid/content/Context;)V

    .line 111
    invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 112
    invoke-virtual {v12, v14}, Lcom/helpshift/support/HSStorage;->setLibraryVersion(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method private static updateRejectConversations(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/util/VersionName;)V
    .locals 6

    .line 124
    new-instance v0, Lcom/helpshift/util/VersionName;

    const-string v1, "7.0.0"

    invoke-direct {v0, v1}, Lcom/helpshift/util/VersionName;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Lcom/helpshift/util/VersionName;->isGreaterThanOrEqualTo(Lcom/helpshift/util/VersionName;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/helpshift/util/VersionName;

    const-string v1, "7.1.0"

    invoke-direct {v0, v1}, Lcom/helpshift/util/VersionName;-><init>(Ljava/lang/String;)V

    .line 125
    invoke-virtual {p2, v0}, Lcom/helpshift/util/VersionName;->isLessThanOrEqualTo(Lcom/helpshift/util/VersionName;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 126
    invoke-interface {p0}, Lcom/helpshift/common/platform/Platform;->getConversationDAO()Lcom/helpshift/conversation/dao/ConversationDAO;

    move-result-object p2

    .line 127
    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserManagerDM;->getAllUsers()Ljava/util/List;

    move-result-object v0

    .line 129
    invoke-static {v0}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 132
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/account/domainmodel/UserDM;

    .line 134
    invoke-virtual {v1}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {p2, v2, v3}, Lcom/helpshift/conversation/dao/ConversationDAO;->readConversationsWithoutMessages(J)Ljava/util/List;

    move-result-object v2

    .line 136
    invoke-static {v2}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .line 139
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 141
    iget-object v4, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v5, Lcom/helpshift/conversation/dto/IssueState;->REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v4, v5, :cond_3

    iget-boolean v4, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isStartNewConversationClicked:Z

    if-nez v4, :cond_3

    .line 143
    invoke-virtual {v3, p0, p1, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    const/4 v4, 0x1

    .line 144
    invoke-virtual {v3, v4, v4}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setStartNewConversationButtonClicked(ZZ)V

    goto :goto_1

    :cond_4
    return-void
.end method
