.class public interface abstract Lcom/helpshift/support/constants/FaqsColumns;
.super Ljava/lang/Object;
.source "FaqsColumns.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# static fields
.field public static final BODY:Ljava/lang/String; = "body"

.field public static final CATEGORY_TAGS:Ljava/lang/String; = "c_tags"

.field public static final HELPFUL:Ljava/lang/String; = "helpful"

.field public static final LANGUAGE:Ljava/lang/String; = "language"

.field public static final PUBLISH_ID:Ljava/lang/String; = "publish_id"

.field public static final QUESTION_ID:Ljava/lang/String; = "question_id"

.field public static final RTL:Ljava/lang/String; = "rtl"

.field public static final SECTION_ID:Ljava/lang/String; = "section_id"

.field public static final TAGS:Ljava/lang/String; = "tags"

.field public static final TITLE:Ljava/lang/String; = "title"
