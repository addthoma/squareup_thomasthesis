.class Lcom/helpshift/support/conversations/ConversationFragment$2;
.super Ljava/lang/Object;
.source "ConversationFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/support/conversations/ConversationFragment;->initialize(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/support/conversations/ConversationFragment;


# direct methods
.method constructor <init>(Lcom/helpshift/support/conversations/ConversationFragment;)V
    .locals 0

    .line 197
    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment$2;->this$0:Lcom/helpshift/support/conversations/ConversationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 200
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment$2;->this$0:Lcom/helpshift/support/conversations/ConversationFragment;

    iget-object p1, p1, Lcom/helpshift/support/conversations/ConversationFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    invoke-virtual {p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->requestReplyFieldFocus()V

    .line 201
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment$2;->this$0:Lcom/helpshift/support/conversations/ConversationFragment;

    iget-object p1, p1, Lcom/helpshift/support/conversations/ConversationFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    invoke-virtual {p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->showKeyboard()V

    .line 202
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationFragment$2;->this$0:Lcom/helpshift/support/conversations/ConversationFragment;

    iget-object p1, p1, Lcom/helpshift/support/conversations/ConversationFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->markConversationResolutionStatus(Z)V

    return-void
.end method
