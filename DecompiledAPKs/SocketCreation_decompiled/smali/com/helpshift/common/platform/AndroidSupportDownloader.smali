.class public Lcom/helpshift/common/platform/AndroidSupportDownloader;
.super Ljava/lang/Object;
.source "AndroidSupportDownloader.java"

# interfaces
.implements Lcom/helpshift/downloader/SupportDownloader;


# static fields
.field private static final CORE_POOL_SIZE:I = 0x5

.field private static final KEEP_ALIVE_TIME:I = 0x1

.field private static final KEEP_ALIVE_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

.field private static final MAXIMUM_POOL_SIZE:I = 0x5


# instance fields
.field private callbackManager:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Lcom/helpshift/downloader/SupportDownloadStateChangeListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private final downloadManager:Lcom/helpshift/android/commons/downloader/DownloadManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sput-object v0, Lcom/helpshift/common/platform/AndroidSupportDownloader;->KEEP_ALIVE_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/helpshift/common/platform/KVStore;)V
    .locals 9

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/helpshift/common/platform/AndroidSupportDownloader;->context:Landroid/content/Context;

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidSupportDownloader;->callbackManager:Ljava/util/Map;

    .line 44
    new-instance v0, Lcom/helpshift/common/platform/SupportDownloaderKVStorage;

    invoke-direct {v0, p2}, Lcom/helpshift/common/platform/SupportDownloaderKVStorage;-><init>(Lcom/helpshift/common/platform/KVStore;)V

    .line 45
    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    .line 46
    new-instance p2, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v6, Lcom/helpshift/common/platform/AndroidSupportDownloader;->KEEP_ALIVE_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    new-instance v8, Lcom/helpshift/common/domain/HSThreadFactory;

    const-string v1, "sp-dwnld"

    invoke-direct {v8, v1}, Lcom/helpshift/common/domain/HSThreadFactory;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x5

    const/4 v3, 0x5

    const-wide/16 v4, 0x1

    move-object v1, p2

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    .line 52
    new-instance v1, Lcom/helpshift/android/commons/downloader/DownloadManager;

    invoke-direct {v1, p1, v0, p2}, Lcom/helpshift/android/commons/downloader/DownloadManager;-><init>(Landroid/content/Context;Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;Ljava/util/concurrent/ThreadPoolExecutor;)V

    iput-object v1, p0, Lcom/helpshift/common/platform/AndroidSupportDownloader;->downloadManager:Lcom/helpshift/android/commons/downloader/DownloadManager;

    return-void
.end method

.method private declared-synchronized addCallback(Ljava/lang/String;Lcom/helpshift/downloader/SupportDownloadStateChangeListener;)V
    .locals 1

    monitor-enter p0

    if-nez p2, :cond_0

    .line 150
    monitor-exit p0

    return-void

    .line 152
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidSupportDownloader;->callbackManager:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-nez v0, :cond_1

    .line 154
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 156
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 157
    iget-object p2, p0, Lcom/helpshift/common/platform/AndroidSupportDownloader;->callbackManager:Ljava/util/Map;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 158
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private declared-synchronized getCallbacks(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set<",
            "Lcom/helpshift/downloader/SupportDownloadStateChangeListener;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 165
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidSupportDownloader;->callbackManager:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Set;

    if-nez p1, :cond_0

    .line 168
    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    goto :goto_0

    .line 171
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object p1, v0

    .line 173
    :goto_0
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method private getStoragePath()Ljava/lang/String;
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidSupportDownloader;->context:Landroid/content/Context;

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    invoke-static {v0, v1}, Lcom/helpshift/util/ApplicationUtil;->isPermissionGranted(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    sget-object v0, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private declared-synchronized removeCallbacks(Ljava/lang/String;)V
    .locals 1

    monitor-enter p0

    .line 161
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidSupportDownloader;->callbackManager:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method handleDownloadFailure(Ljava/lang/String;)V
    .locals 2

    .line 142
    invoke-direct {p0, p1}, Lcom/helpshift/common/platform/AndroidSupportDownloader;->getCallbacks(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/downloader/SupportDownloadStateChangeListener;

    .line 143
    invoke-interface {v1, p1}, Lcom/helpshift/downloader/SupportDownloadStateChangeListener;->onFailure(Ljava/lang/String;)V

    goto :goto_0

    .line 145
    :cond_0
    invoke-direct {p0, p1}, Lcom/helpshift/common/platform/AndroidSupportDownloader;->removeCallbacks(Ljava/lang/String;)V

    return-void
.end method

.method handleDownloadSuccess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 129
    invoke-direct {p0, p1}, Lcom/helpshift/common/platform/AndroidSupportDownloader;->getCallbacks(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/downloader/SupportDownloadStateChangeListener;

    .line 130
    invoke-interface {v1, p1, p2}, Lcom/helpshift/downloader/SupportDownloadStateChangeListener;->onSuccess(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_0
    invoke-direct {p0, p1}, Lcom/helpshift/common/platform/AndroidSupportDownloader;->removeCallbacks(Ljava/lang/String;)V

    return-void
.end method

.method handleProgressChange(Ljava/lang/String;I)V
    .locals 2

    .line 136
    invoke-direct {p0, p1}, Lcom/helpshift/common/platform/AndroidSupportDownloader;->getCallbacks(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/downloader/SupportDownloadStateChangeListener;

    .line 137
    invoke-interface {v1, p1, p2}, Lcom/helpshift/downloader/SupportDownloadStateChangeListener;->onProgressChange(Ljava/lang/String;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public startDownload(Ljava/lang/String;ZLcom/helpshift/downloader/SupportDownloader$StorageDirType;Lcom/helpshift/common/domain/network/AuthDataProvider;Lcom/helpshift/downloader/SupportDownloadStateChangeListener;)V
    .locals 7

    .line 72
    invoke-direct {p0, p1, p5}, Lcom/helpshift/common/platform/AndroidSupportDownloader;->addCallback(Ljava/lang/String;Lcom/helpshift/downloader/SupportDownloadStateChangeListener;)V

    .line 73
    invoke-direct {p0}, Lcom/helpshift/common/platform/AndroidSupportDownloader;->getStoragePath()Ljava/lang/String;

    move-result-object v0

    .line 75
    sget-object v1, Lcom/helpshift/common/platform/AndroidSupportDownloader$4;->$SwitchMap$com$helpshift$downloader$SupportDownloader$StorageDirType:[I

    invoke-virtual {p3}, Lcom/helpshift/downloader/SupportDownloader$StorageDirType;->ordinal()I

    move-result p3

    aget p3, v1, p3

    const/4 v1, 0x1

    if-eq p3, v1, :cond_3

    const/4 v2, 0x2

    if-eq p3, v2, :cond_1

    const/4 p5, 0x3

    if-eq p3, p5, :cond_0

    goto :goto_0

    :cond_0
    if-nez v0, :cond_2

    goto :goto_1

    :cond_1
    if-nez v0, :cond_2

    .line 83
    invoke-interface {p5, p1}, Lcom/helpshift/downloader/SupportDownloadStateChangeListener;->onFailure(Ljava/lang/String;)V

    return-void

    :cond_2
    :goto_0
    const/4 p3, 0x0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_1
    const/4 p3, 0x1

    .line 94
    :goto_2
    new-instance p5, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;

    invoke-direct {p5}, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;-><init>()V

    .line 95
    invoke-virtual {p5, v1}, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->setUseCache(Z)Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;

    move-result-object p5

    .line 96
    invoke-virtual {p5, v1}, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->setWriteToFile(Z)Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;

    move-result-object p5

    .line 97
    invoke-virtual {p5, p3}, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->setIsNoMedia(Z)Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;

    move-result-object p3

    .line 98
    invoke-virtual {p3, v0}, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->setExternalStorageDirectoryPath(Ljava/lang/String;)Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;

    move-result-object p3

    .line 99
    invoke-virtual {p3}, Lcom/helpshift/android/commons/downloader/DownloadConfig$Builder;->create()Lcom/helpshift/android/commons/downloader/DownloadConfig;

    move-result-object v3

    .line 100
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidSupportDownloader;->downloadManager:Lcom/helpshift/android/commons/downloader/DownloadManager;

    new-instance v4, Lcom/helpshift/common/platform/AndroidSupportDownloader$1;

    invoke-direct {v4, p0, p4}, Lcom/helpshift/common/platform/AndroidSupportDownloader$1;-><init>(Lcom/helpshift/common/platform/AndroidSupportDownloader;Lcom/helpshift/common/domain/network/AuthDataProvider;)V

    new-instance v5, Lcom/helpshift/common/platform/AndroidSupportDownloader$2;

    invoke-direct {v5, p0}, Lcom/helpshift/common/platform/AndroidSupportDownloader$2;-><init>(Lcom/helpshift/common/platform/AndroidSupportDownloader;)V

    new-instance v6, Lcom/helpshift/common/platform/AndroidSupportDownloader$3;

    invoke-direct {v6, p0}, Lcom/helpshift/common/platform/AndroidSupportDownloader$3;-><init>(Lcom/helpshift/common/platform/AndroidSupportDownloader;)V

    move-object v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v6}, Lcom/helpshift/android/commons/downloader/DownloadManager;->startDownload(Ljava/lang/String;ZLcom/helpshift/android/commons/downloader/DownloadConfig;Lcom/helpshift/android/commons/downloader/contracts/NetworkAuthDataFetcher;Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;Lcom/helpshift/android/commons/downloader/contracts/OnProgressChangedListener;)V

    return-void
.end method
