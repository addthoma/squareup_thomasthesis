.class public Lcom/helpshift/model/SdkInfoModel;
.super Ljava/lang/Object;
.source "SdkInfoModel.java"


# static fields
.field private static final CHANGE_SET_ID_PREFIX:Ljava/lang/String; = "hs__change_set_id:"

.field private static final KEY_DEVICE_ID:Ljava/lang/String; = "hs-device-id"

.field private static final KEY_SYNCED_USER_ID:Ljava/lang/String; = "hs-synced-user-id"

.field public static final SDK_LANGUAGE:Ljava/lang/String; = "sdk-language"


# instance fields
.field private backupStorage:Lcom/helpshift/common/dao/BackupDAO;

.field private etags:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private storage:Lcom/helpshift/storage/KeyValueStorage;


# direct methods
.method protected constructor <init>(Lcom/helpshift/storage/KeyValueStorage;Lcom/helpshift/common/platform/Platform;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    .line 31
    invoke-interface {p2}, Lcom/helpshift/common/platform/Platform;->getBackupDAO()Lcom/helpshift/common/dao/BackupDAO;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/model/SdkInfoModel;->backupStorage:Lcom/helpshift/common/dao/BackupDAO;

    .line 32
    iget-object p1, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string p2, "etags"

    invoke-interface {p1, p2}, Lcom/helpshift/storage/KeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/HashMap;

    iput-object p1, p0, Lcom/helpshift/model/SdkInfoModel;->etags:Ljava/util/HashMap;

    .line 33
    iget-object p1, p0, Lcom/helpshift/model/SdkInfoModel;->etags:Ljava/util/HashMap;

    if-nez p1, :cond_0

    .line 34
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/helpshift/model/SdkInfoModel;->etags:Ljava/util/HashMap;

    .line 36
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/model/SdkInfoModel;->updateBackupStorageWithInternalStorage()V

    return-void
.end method

.method private updateBackupStorageWithInternalStorage()V
    .locals 3

    .line 224
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "hs-device-id"

    invoke-interface {v0, v1}, Lcom/helpshift/storage/KeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 226
    iget-object v2, p0, Lcom/helpshift/model/SdkInfoModel;->backupStorage:Lcom/helpshift/common/dao/BackupDAO;

    invoke-interface {v2, v1, v0}, Lcom/helpshift/common/dao/BackupDAO;->storeValue(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "hs-synced-user-id"

    invoke-interface {v0, v1}, Lcom/helpshift/storage/KeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 231
    iget-object v2, p0, Lcom/helpshift/model/SdkInfoModel;->backupStorage:Lcom/helpshift/common/dao/BackupDAO;

    invoke-interface {v2, v1, v0}, Lcom/helpshift/common/dao/BackupDAO;->storeValue(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public addDeviceId(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 125
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 128
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "hs-device-id"

    invoke-interface {v0, v1, p1}, Lcom/helpshift/storage/KeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    .line 130
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->backupStorage:Lcom/helpshift/common/dao/BackupDAO;

    invoke-interface {v0, v1, p1}, Lcom/helpshift/common/dao/BackupDAO;->storeValue(Ljava/lang/String;Ljava/io/Serializable;)V

    return-void

    .line 133
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method public addEtag(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->etags:Ljava/util/HashMap;

    invoke-virtual {v0, p2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    iget-object p1, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    iget-object p2, p0, Lcom/helpshift/model/SdkInfoModel;->etags:Ljava/util/HashMap;

    const-string v0, "etags"

    invoke-interface {p1, v0, p2}, Lcom/helpshift/storage/KeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    return-void
.end method

.method public clearEtag(Ljava/lang/String;)V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->etags:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->etags:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-object p1, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->etags:Ljava/util/HashMap;

    const-string v1, "etags"

    invoke-interface {p1, v1, v0}, Lcom/helpshift/storage/KeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    :cond_0
    return-void
.end method

.method public getChangeSetId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "hs__change_set_id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 193
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    invoke-interface {v0, p1}, Lcom/helpshift/storage/KeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public getCurrentLoggedInId()Ljava/lang/String;
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "current-logged-in-id"

    invoke-interface {v0, v1}, Lcom/helpshift/storage/KeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "hs-device-id"

    invoke-interface {v0, v1}, Lcom/helpshift/storage/KeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->backupStorage:Lcom/helpshift/common/dao/BackupDAO;

    invoke-interface {v0, v1}, Lcom/helpshift/common/dao/BackupDAO;->getValue(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public getDevicePropertiesSyncImmediately()Ljava/lang/Boolean;
    .locals 2

    .line 167
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "hs-device-properties-sync-immediately"

    invoke-interface {v0, v1}, Lcom/helpshift/storage/KeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 169
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getEtag(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->etags:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public getFirstLaunch()Ljava/lang/Boolean;
    .locals 2

    .line 151
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "hs-first-launch"

    invoke-interface {v0, v1}, Lcom/helpshift/storage/KeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public getOneCampaignFetchSuccessful()Ljava/lang/Boolean;
    .locals 2

    .line 159
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "hs-one-campaign-fetch-successful"

    invoke-interface {v0, v1}, Lcom/helpshift/storage/KeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public getSdkLanguage()Ljava/lang/String;
    .locals 2

    .line 179
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "sdk-language"

    invoke-interface {v0, v1}, Lcom/helpshift/storage/KeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getServerTimeDelta()Ljava/lang/Float;
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "server-time-delta"

    invoke-interface {v0, v1}, Lcom/helpshift/storage/KeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    return-object v0
.end method

.method public getUserIdSyncedWithBackend()Ljava/lang/String;
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "hs-synced-user-id"

    invoke-interface {v0, v1}, Lcom/helpshift/storage/KeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->backupStorage:Lcom/helpshift/common/dao/BackupDAO;

    invoke-interface {v0, v1}, Lcom/helpshift/common/dao/BackupDAO;->getValue(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public isDuplicateNotification(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .line 250
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "hs__received_push_campaigns"

    .line 251
    invoke-interface {v0, v1}, Lcom/helpshift/storage/KeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 253
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 256
    :cond_0
    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashSet;

    if-nez v2, :cond_1

    .line 259
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 262
    :cond_1
    invoke-virtual {v2, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 p1, 0x1

    return p1

    .line 266
    :cond_2
    invoke-virtual {v2, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 267
    invoke-virtual {v0, p2, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 268
    iget-object p1, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    invoke-interface {p1, v1, v0}, Lcom/helpshift/storage/KeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    const/4 p1, 0x0

    return p1
.end method

.method public setChangeSetId(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "hs__change_set_id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 188
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    invoke-interface {v0, p2, p1}, Lcom/helpshift/storage/KeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    return-void
.end method

.method public setCurrentLoggedInId(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    if-eqz p1, :cond_0

    .line 106
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 108
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 109
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "current-logged-in-id"

    invoke-interface {v0, v1, p1}, Lcom/helpshift/storage/KeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    return-void

    .line 112
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method public setDevicePropertiesSyncImmediately(Ljava/lang/Boolean;)V
    .locals 2

    .line 175
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "hs-device-properties-sync-immediately"

    invoke-interface {v0, v1, p1}, Lcom/helpshift/storage/KeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    return-void
.end method

.method public setFirstLaunch(Ljava/lang/Boolean;)V
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "hs-first-launch"

    invoke-interface {v0, v1, p1}, Lcom/helpshift/storage/KeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    return-void
.end method

.method public setOneCampaignFetchSuccessful(Ljava/lang/Boolean;)V
    .locals 2

    .line 163
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "hs-one-campaign-fetch-successful"

    invoke-interface {v0, v1, p1}, Lcom/helpshift/storage/KeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    return-void
.end method

.method public setSdkLanguage(Ljava/lang/String;)V
    .locals 2

    .line 183
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "sdk-language"

    invoke-interface {v0, v1, p1}, Lcom/helpshift/storage/KeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    return-void
.end method

.method public setServerTimeDelta(Ljava/lang/Float;)V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "server-time-delta"

    invoke-interface {v0, v1, p1}, Lcom/helpshift/storage/KeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    return-void
.end method

.method public setUserIdSyncedWithBackend(Ljava/lang/String;)V
    .locals 2

    .line 216
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->storage:Lcom/helpshift/storage/KeyValueStorage;

    const-string v1, "hs-synced-user-id"

    invoke-interface {v0, v1, p1}, Lcom/helpshift/storage/KeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    .line 217
    iget-object v0, p0, Lcom/helpshift/model/SdkInfoModel;->backupStorage:Lcom/helpshift/common/dao/BackupDAO;

    invoke-interface {v0, v1, p1}, Lcom/helpshift/common/dao/BackupDAO;->storeValue(Ljava/lang/String;Ljava/io/Serializable;)V

    return-void
.end method
