.class public Lcom/helpshift/conversation/activeconversation/message/input/Input;
.super Ljava/lang/Object;
.source "Input.java"


# instance fields
.field public final botInfo:Ljava/lang/String;

.field public final inputLabel:Ljava/lang/String;

.field public final required:Z

.field public final skipLabel:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/message/input/Input;->botInfo:Ljava/lang/String;

    .line 12
    iput-boolean p2, p0, Lcom/helpshift/conversation/activeconversation/message/input/Input;->required:Z

    .line 13
    iput-object p3, p0, Lcom/helpshift/conversation/activeconversation/message/input/Input;->inputLabel:Ljava/lang/String;

    .line 14
    iput-object p4, p0, Lcom/helpshift/conversation/activeconversation/message/input/Input;->skipLabel:Ljava/lang/String;

    return-void
.end method
