.class public Lcom/helpshift/conversation/util/predicate/ConversationPredicates;
.super Ljava/lang/Object;
.source "ConversationPredicates.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newInProgressConversationPredicate()Lcom/helpshift/util/Predicate;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/helpshift/util/Predicate<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;"
        }
    .end annotation

    .line 21
    new-instance v0, Lcom/helpshift/conversation/util/predicate/ConversationPredicates$2;

    invoke-direct {v0}, Lcom/helpshift/conversation/util/predicate/ConversationPredicates$2;-><init>()V

    return-object v0
.end method

.method public static newSyncedConversationPredicate()Lcom/helpshift/util/Predicate;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/helpshift/util/Predicate<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;"
        }
    .end annotation

    .line 12
    new-instance v0, Lcom/helpshift/conversation/util/predicate/ConversationPredicates$1;

    invoke-direct {v0}, Lcom/helpshift/conversation/util/predicate/ConversationPredicates$1;-><init>()V

    return-object v0
.end method
