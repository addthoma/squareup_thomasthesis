.class public Lcom/helpshift/conversation/viewmodel/ConversationalVM;
.super Lcom/helpshift/conversation/viewmodel/ConversationVM;
.source "ConversationalVM.java"

# interfaces
.implements Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;
.implements Lcom/helpshift/conversation/viewmodel/ConversationalMediatorCallback;
.implements Lcom/helpshift/conversation/viewmodel/ListPickerVMCallback;


# static fields
.field public static final NO_NETWORK_ERROR:I = 0x1

.field public static final POLL_FAILURE_ERROR:I = 0x2

.field public static final PRE_ISSUE_CREATION_ERROR:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Helpshift_ConvsatnlVM"


# instance fields
.field awaitingUserInputForBotStep:Z

.field isInBetweenBotExecution:Z

.field isNetworkAvailable:Z

.field isShowingPollFailureError:Z

.field isUserReplyDraftClearedForBotChange:Z

.field private listPickerVM:Lcom/helpshift/conversation/viewmodel/ListPickerVM;

.field private showConversationHistory:Z


# direct methods
.method public constructor <init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Lcom/helpshift/conversation/activeconversation/ViewableConversation;Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;ZZ)V
    .locals 7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p7

    .line 87
    invoke-direct/range {v0 .. v6}, Lcom/helpshift/conversation/viewmodel/ConversationVM;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Lcom/helpshift/conversation/activeconversation/ViewableConversation;Lcom/helpshift/conversation/activeconversation/ConversationRenderer;Z)V

    const/4 p1, 0x1

    .line 62
    iput-boolean p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isNetworkAvailable:Z

    .line 88
    iput-boolean p6, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->showConversationHistory:Z

    .line 97
    invoke-virtual {p4}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1

    .line 98
    iget-object p2, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {p1, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInProgressState(Lcom/helpshift/conversation/dto/IssueState;)Z

    move-result p2

    if-nez p2, :cond_0

    iget-boolean p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isStartNewConversationClicked:Z

    if-eqz p1, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->onNewConversationButtonClicked()V

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/helpshift/conversation/viewmodel/ConversationalVM;Lcom/helpshift/common/exception/RootAPIException;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->showErrorForNoNetwork(Lcom/helpshift/common/exception/RootAPIException;)V

    return-void
.end method

.method static synthetic access$100(Lcom/helpshift/conversation/viewmodel/ConversationalVM;)Lcom/helpshift/conversation/viewmodel/ListPickerVM;
    .locals 0

    .line 54
    iget-object p0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->listPickerVM:Lcom/helpshift/conversation/viewmodel/ListPickerVM;

    return-object p0
.end method

.method private addGreetingMessageIfNewPreIssue()V
    .locals 9

    .line 147
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    .line 148
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v1, "conversationGreetingMessage"

    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 151
    invoke-static {v4}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-static {v0}, Lcom/helpshift/common/util/HSDateFormatSpec;->getCurrentAdjustedTimeForStorage(Lcom/helpshift/common/platform/Platform;)Ljava/lang/String;

    move-result-object v5

    .line 153
    invoke-static {v5}, Lcom/helpshift/common/util/HSDateFormatSpec;->convertToEpochTime(Ljava/lang/String;)J

    move-result-wide v6

    .line 154
    new-instance v0, Lcom/helpshift/conversation/activeconversation/message/AdminMessageDM;

    const-string v3, ""

    const-string v8, "Bot"

    move-object v2, v0

    invoke-direct/range {v2 .. v8}, Lcom/helpshift/conversation/activeconversation/message/AdminMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 156
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/conversation/activeconversation/message/AdminMessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 157
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v1

    iget-object v1, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iput-object v1, v0, Lcom/helpshift/conversation/activeconversation/message/AdminMessageDM;->conversationLocalId:Ljava/lang/Long;

    .line 158
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/helpshift/conversation/viewmodel/MessageListVM;->addMessages(Ljava/util/Collection;)V

    :cond_0
    return-void
.end method

.method private disableUserInputOptions()V
    .locals 2

    .line 636
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    if-eqz v0, :cond_0

    .line 637
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->hideKeyboard()V

    .line 641
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->attachImageButtonWidget:Lcom/helpshift/widget/ButtonWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    .line 644
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->disableUserTextInput()V

    return-void
.end method

.method private disableUserTextInput()V
    .locals 2

    .line 707
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    return-void
.end method

.method private evaluateBotMessages(Ljava/util/Collection;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;"
        }
    .end annotation

    .line 655
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 656
    iget-boolean v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isInBetweenBotExecution:Z

    .line 657
    invoke-direct {p0, p1, v1}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->processMessagesForBots(Ljava/util/Collection;Z)Ljava/util/List;

    move-result-object p1

    .line 659
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v1, :cond_0

    .line 662
    iget-boolean v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isInBetweenBotExecution:Z

    if-nez v2, :cond_0

    .line 664
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldEnableMessagesClick()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessagesClickOnBotSwitch(Z)V

    .line 665
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->removeOptionsMessageFromUI()V

    .line 670
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    .line 672
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationalVM$10;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM$10;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    goto :goto_0

    .line 686
    :cond_0
    iget-boolean v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isInBetweenBotExecution:Z

    if-eqz v2, :cond_1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    .line 693
    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessagesClickOnBotSwitch(Z)V

    .line 698
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->updateUserInputState()V

    return-object p1
.end method

.method private getConversationalMediator()Lcom/helpshift/conversation/viewmodel/ConversationalMediator;
    .locals 1

    .line 550
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    check-cast v0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;

    return-object v0
.end method

.method private hideListPicker(Z)V
    .locals 2

    .line 617
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationalVM$9;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationalVM$9;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM;Z)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method private incrementCreatedAt(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 5

    .line 990
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->getEpochCreatedAtTime()J

    move-result-wide v1

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    .line 991
    sget-object p2, Lcom/helpshift/common/util/HSDateFormatSpec;->STORAGE_TIME_FORMAT:Lcom/helpshift/common/util/HSSimpleDateFormat;

    invoke-virtual {p2, v0}, Lcom/helpshift/common/util/HSSimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    .line 992
    invoke-static {p2}, Lcom/helpshift/common/util/HSDateFormatSpec;->convertToEpochTime(Ljava/lang/String;)J

    move-result-wide v0

    .line 993
    invoke-virtual {p1, p2}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->setCreatedAt(Ljava/lang/String;)V

    .line 994
    invoke-virtual {p1, v0, v1}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->setEpochCreatedAtTime(J)V

    return-void
.end method

.method private processMessagesForBots(Ljava/util/Collection;Z)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;Z)",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;"
        }
    .end annotation

    .line 181
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 183
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1

    .line 185
    invoke-virtual {p1, v0, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->evaluateBotExecutionState(Ljava/util/List;Z)Z

    move-result p2

    iput-boolean p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isInBetweenBotExecution:Z

    .line 186
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalMediator()Lcom/helpshift/conversation/viewmodel/ConversationalMediator;

    move-result-object p2

    iget-boolean v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isInBetweenBotExecution:Z

    invoke-virtual {p2, v1}, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->setIsBotExecuting(Z)V

    .line 188
    iget-boolean p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isInBetweenBotExecution:Z

    const/4 v1, 0x0

    if-eqz p2, :cond_7

    .line 193
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getLatestActionableBotMessage()Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    move-result-object p1

    .line 194
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalMediator()Lcom/helpshift/conversation/viewmodel/ConversationalMediator;

    move-result-object p2

    invoke-virtual {p2}, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->getRenderedBotMessage()Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    move-result-object p2

    const/4 v2, 0x1

    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 199
    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    iget-object v3, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 200
    iput-boolean v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->awaitingUserInputForBotStep:Z

    return-object v0

    :cond_0
    if-eqz p1, :cond_4

    .line 205
    iget-object p2, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v3, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ADMIN_TEXT_WITH_OPTION_INPUT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-eq p2, v3, :cond_1

    iget-object p2, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v3, Lcom/helpshift/conversation/activeconversation/message/MessageType;->FAQ_LIST_WITH_OPTION_INPUT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-ne p2, v3, :cond_4

    .line 211
    :cond_1
    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p2

    const/4 v3, -0x1

    if-eq p2, v3, :cond_5

    .line 217
    iget-object v3, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v4, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ADMIN_TEXT_WITH_OPTION_INPUT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-ne v3, v4, :cond_2

    .line 218
    move-object v3, p1

    check-cast v3, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithOptionInputDM;

    invoke-virtual {p0, v3}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->splitOptionsBotMessage(Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithOptionInputDM;)Lcom/helpshift/util/ValuePair;

    move-result-object v3

    goto :goto_0

    .line 221
    :cond_2
    move-object v3, p1

    check-cast v3, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageWithOptionInputDM;

    invoke-virtual {p0, v3}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->splitOptionsBotMessage(Lcom/helpshift/conversation/activeconversation/message/FAQListMessageWithOptionInputDM;)Lcom/helpshift/util/ValuePair;

    move-result-object v3

    .line 225
    :goto_0
    iget-object v4, v3, Lcom/helpshift/util/ValuePair;->second:Ljava/lang/Object;

    check-cast v4, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    iget-object v5, v3, Lcom/helpshift/util/ValuePair;->first:Ljava/lang/Object;

    check-cast v5, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    invoke-direct {p0, v4, v5}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->incrementCreatedAt(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 228
    invoke-interface {v0, p2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 229
    iget-object v4, v3, Lcom/helpshift/util/ValuePair;->first:Ljava/lang/Object;

    invoke-interface {v0, p2, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 232
    iget-object v4, v3, Lcom/helpshift/util/ValuePair;->second:Ljava/lang/Object;

    check-cast v4, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;

    iget-object v4, v4, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;

    iget-object v4, v4, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->type:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;

    sget-object v5, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;->PILL:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;

    if-ne v4, v5, :cond_3

    add-int/2addr p2, v2

    .line 233
    iget-object v4, v3, Lcom/helpshift/util/ValuePair;->second:Ljava/lang/Object;

    invoke-interface {v0, p2, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 235
    :cond_3
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalMediator()Lcom/helpshift/conversation/viewmodel/ConversationalMediator;

    move-result-object p2

    iget-object v3, v3, Lcom/helpshift/util/ValuePair;->second:Ljava/lang/Object;

    check-cast v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    invoke-virtual {p2, v3}, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->setBotMessageDMToRender(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    goto :goto_1

    .line 239
    :cond_4
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalMediator()Lcom/helpshift/conversation/viewmodel/ConversationalMediator;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->setBotMessageDMToRender(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    :cond_5
    :goto_1
    if-eqz p1, :cond_6

    .line 244
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->removeOptionsMessageFromUI()V

    .line 245
    iput-boolean v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->awaitingUserInputForBotStep:Z

    goto :goto_2

    .line 248
    :cond_6
    iput-boolean v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->awaitingUserInputForBotStep:Z

    goto :goto_2

    .line 252
    :cond_7
    iput-boolean v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->awaitingUserInputForBotStep:Z

    :goto_2
    return-object v0
.end method

.method private removeOptionsMessageFromUI()V
    .locals 5

    .line 260
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    if-nez v0, :cond_0

    return-void

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/MessageListVM;->copyOfUIMessageDMs()Ljava/util/List;

    move-result-object v0

    .line 265
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 266
    invoke-static {v0}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 267
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 268
    iget-object v3, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v4, Lcom/helpshift/conversation/activeconversation/message/MessageType;->OPTION_INPUT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-ne v3, v4, :cond_1

    .line 269
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 272
    :cond_2
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/viewmodel/MessageListVM;->remove(Ljava/util/List;)V

    :cond_3
    const/4 v0, 0x0

    .line 279
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->hideListPicker(Z)V

    return-void
.end method

.method private showErrorForNoNetwork(Lcom/helpshift/common/exception/RootAPIException;)V
    .locals 1

    .line 436
    iget-object p1, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    instance-of p1, p1, Lcom/helpshift/common/exception/NetworkException;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {p1}, Lcom/helpshift/common/platform/Platform;->isOnline()Z

    move-result p1

    if-nez p1, :cond_0

    .line 437
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$5;

    invoke-direct {v0, p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM$5;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM;)V

    invoke-virtual {p1, v0}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    :cond_0
    return-void
.end method

.method private updateUserInputState()V
    .locals 4

    .line 716
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 717
    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    .line 720
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->disableUserInputOptions()V

    goto :goto_1

    .line 722
    :cond_0
    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v0, v1, :cond_8

    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v0, v1, :cond_8

    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->COMPLETED_ISSUE_CREATED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v0, v1, :cond_1

    goto :goto_1

    .line 727
    :cond_1
    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isInBetweenBotExecution:Z

    if-eqz v0, :cond_6

    .line 730
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->attachImageButtonWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v0, v3}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    .line 732
    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->awaitingUserInputForBotStep:Z

    if-eqz v0, :cond_2

    goto :goto_1

    .line 738
    :cond_2
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->disableUserInputOptions()V

    .line 744
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    if-eqz v0, :cond_7

    .line 745
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 747
    iget-object v1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v1}, Lcom/helpshift/common/util/HSObservableList;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 749
    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/helpshift/common/util/HSObservableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 750
    instance-of v1, v0, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;

    if-nez v1, :cond_3

    instance-of v1, v0, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForOptionInput;

    if-eqz v1, :cond_5

    .line 752
    :cond_3
    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    .line 753
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->getState()Lcom/helpshift/conversation/activeconversation/message/UserMessageState;

    move-result-object v0

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/UserMessageState;->SENT:Lcom/helpshift/conversation/activeconversation/message/UserMessageState;

    if-ne v0, v1, :cond_4

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    :cond_5
    :goto_0
    move v3, v2

    goto :goto_1

    .line 759
    :cond_6
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 762
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->disableUserInputOptions()V

    :cond_7
    const/4 v3, 0x1

    .line 769
    :cond_8
    :goto_1
    invoke-virtual {p0, v3}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->showFakeTypingIndicator(Z)V

    return-void
.end method


# virtual methods
.method public addAll(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)V"
        }
    .end annotation

    .line 511
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    .line 512
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->hasBotSwitchedToAnotherBotInPollerResponse(Ljava/util/Collection;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 515
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessagesClickOnBotSwitch(Z)V

    .line 519
    :cond_0
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->evaluateBotMessages(Ljava/util/Collection;)Ljava/util/List;

    move-result-object p1

    .line 522
    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isInBetweenBotExecution:Z

    if-eqz v0, :cond_1

    .line 526
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 527
    iget-boolean v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isUserReplyDraftClearedForBotChange:Z

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->containsAtleastOneUserMessage()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 528
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->clearUserReplyDraft()V

    const/4 v0, 0x1

    .line 529
    iput-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isUserReplyDraftClearedForBotChange:Z

    goto :goto_0

    .line 533
    :cond_1
    iput-boolean v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isUserReplyDraftClearedForBotChange:Z

    .line 536
    :cond_2
    :goto_0
    invoke-super {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->addAll(Ljava/util/Collection;)V

    return-void
.end method

.method protected buildUIMessages(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ")",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;"
        }
    .end annotation

    .line 165
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 166
    iget-object v1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iget-object v2, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->processMessagesForBots(Ljava/util/Collection;Z)Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 175
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method protected createMediator(Lcom/helpshift/common/domain/Domain;)Lcom/helpshift/conversation/viewmodel/ConversationMediator;
    .locals 1

    .line 111
    new-instance v0, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;

    invoke-direct {v0, p1, p0}, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/conversation/viewmodel/ConversationalMediatorCallback;)V

    return-object v0
.end method

.method public createPreIssue()V
    .locals 2

    .line 299
    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isNetworkAvailable:Z

    if-nez v0, :cond_0

    .line 300
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "No internet connection."

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->onCreateConversationFailure(Ljava/lang/Exception;)V

    return-void

    .line 305
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->disableUserInputOptions()V

    const/4 v0, 0x1

    .line 306
    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->showFakeTypingIndicator(Z)V

    .line 309
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0, v1, p0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->createPreIssue(Lcom/helpshift/conversation/activeconversation/ViewableConversation;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$StartNewConversationListener;)V

    return-void
.end method

.method protected createWidgetGateway()V
    .locals 4

    .line 105
    new-instance v0, Lcom/helpshift/widget/ConversationalWidgetGateway;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v2}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v2

    iget-object v3, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-direct {v0, v1, v2, v3}, Lcom/helpshift/widget/ConversationalWidgetGateway;-><init>(Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;)V

    iput-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->widgetGateway:Lcom/helpshift/widget/WidgetGateway;

    return-void
.end method

.method getConversationalRenderer()Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;
    .locals 1

    .line 546
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    check-cast v0, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    return-object v0
.end method

.method public handleAdminSuggestedQuestionRead(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 906
    invoke-static {p3}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 907
    iget-object v3, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->conversationLocalId:Ljava/lang/Long;

    .line 908
    iget-object v4, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    .line 909
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$13;

    move-object v1, v0

    move-object v2, p0

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/helpshift/conversation/viewmodel/ConversationalVM$13;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    :cond_0
    return-void
.end method

.method public handleOptionSelected(Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;Z)V
    .locals 4

    .line 561
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    if-nez v0, :cond_0

    return-void

    .line 567
    :cond_0
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->type:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;->PILL:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;

    const/4 v2, 0x1

    if-ne v0, v1, :cond_1

    .line 571
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/MessageListVM;->getUiMessageDMs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 572
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->messageListVM:Lcom/helpshift/conversation/viewmodel/MessageListVM;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/helpshift/conversation/viewmodel/MessageListVM;->remove(Ljava/util/List;)V

    .line 576
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    sub-int/2addr v0, v2

    invoke-interface {v1, v0, v2}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->updateMessages(II)V

    .line 580
    :cond_1
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->updateLastUserActivityTime()V

    .line 582
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->type:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;->PILL:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;

    if-ne v0, v1, :cond_2

    .line 584
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->disableUserInputOptions()V

    goto :goto_0

    .line 586
    :cond_2
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->type:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;->PICKER:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;

    if-ne v0, v1, :cond_3

    .line 587
    invoke-direct {p0, v2}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->hideListPicker(Z)V

    .line 590
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM;Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;Z)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public handleOptionSelectedForPicker(Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;Z)V
    .locals 1

    const/4 v0, 0x0

    .line 1011
    iput-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->listPickerVM:Lcom/helpshift/conversation/viewmodel/ListPickerVM;

    .line 1012
    invoke-virtual {p0, p1, p2, p3}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->handleOptionSelected(Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;Z)V

    return-void
.end method

.method public handleOptionSelectedForPicker(Lcom/helpshift/conversation/viewmodel/OptionUIModel;Z)V
    .locals 1

    .line 1041
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->listPickerVM:Lcom/helpshift/conversation/viewmodel/ListPickerVM;

    if-eqz v0, :cond_0

    .line 1042
    invoke-virtual {v0, p1, p2}, Lcom/helpshift/conversation/viewmodel/ListPickerVM;->handleOptionSelectedForPicker(Lcom/helpshift/conversation/viewmodel/OptionUIModel;Z)V

    :cond_0
    return-void
.end method

.method public handlePreIssueCreationSuccess()V
    .locals 2

    .line 856
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationalVM$11;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM$11;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public hidePickerClearButton()V
    .locals 1

    .line 1053
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalRenderer()Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->hidePickerClearButton()V

    return-void
.end method

.method protected initMessagesList()V
    .locals 0

    .line 116
    invoke-super {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->initMessagesList()V

    .line 117
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->addGreetingMessageIfNewPreIssue()V

    return-void
.end method

.method public onConversationInboxPollFailure()V
    .locals 2

    const-string v0, "Helpshift_ConvsatnlVM"

    const-string v1, "On conversation inbox poll failure"

    .line 314
    invoke-static {v0, v1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 317
    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->showFakeTypingIndicator(Z)V

    .line 322
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->isOnline()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->awaitingUserInputForBotStep:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isInBetweenBotExecution:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    .line 323
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    .line 324
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isIssueInProgress()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 326
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationalVM$1;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM$1;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    const/4 v0, 0x1

    .line 334
    iput-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isShowingPollFailureError:Z

    :cond_1
    return-void
.end method

.method public onConversationInboxPollSuccess()V
    .locals 2

    .line 340
    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isShowingPollFailureError:Z

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationalVM$2;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM$2;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    const/4 v0, 0x0

    .line 349
    iput-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isShowingPollFailureError:Z

    :cond_0
    return-void
.end method

.method public onCreateConversationFailure(Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "Helpshift_ConvsatnlVM"

    const-string v1, "Error filing a pre-issue"

    .line 890
    invoke-static {v0, v1, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 891
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$12;

    invoke-direct {v0, p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM$12;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM;)V

    invoke-virtual {p1, v0}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public onCreateConversationSuccess(J)V
    .locals 0

    .line 851
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->handlePreIssueCreationSuccess()V

    return-void
.end method

.method public onIssueStatusChange(Lcom/helpshift/conversation/dto/IssueState;)V
    .locals 2

    .line 813
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 814
    invoke-super {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onIssueStatusChange(Lcom/helpshift/conversation/dto/IssueState;)V

    .line 818
    iget-boolean p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isInBetweenBotExecution:Z

    if-eqz p1, :cond_0

    .line 819
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->attachImageButtonWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {p1, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    :cond_0
    return-void

    .line 824
    :cond_1
    sget-object v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$15;->$SwitchMap$com$helpshift$conversation$dto$IssueState:[I

    invoke-virtual {p1}, Lcom/helpshift/conversation/dto/IssueState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    goto :goto_0

    .line 835
    :cond_2
    iput-boolean v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->awaitingUserInputForBotStep:Z

    .line 836
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->removeOptionsMessageFromUI()V

    .line 838
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->handleConversationRejectedState()V

    .line 841
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->updateUIOnNewMessageReceived()V

    goto :goto_0

    .line 827
    :cond_3
    iput-boolean v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->awaitingUserInputForBotStep:Z

    .line 828
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    sget-object v0, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->START_NEW_CONVERSATION:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    invoke-virtual {p1, v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->showStartNewConversation(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    .line 831
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->updateUIOnNewMessageReceived()V

    .line 846
    :goto_0
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->updateUserInputState()V

    return-void
.end method

.method public onListPickerSearchQueryChange(Ljava/lang/String;)V
    .locals 1

    .line 1035
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->listPickerVM:Lcom/helpshift/conversation/viewmodel/ListPickerVM;

    if-eqz v0, :cond_0

    .line 1036
    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/viewmodel/ListPickerVM;->onListPickerSearchQueryChange(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onNetworkAvailable()V
    .locals 1

    const/4 v0, 0x1

    .line 774
    iput-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isNetworkAvailable:Z

    .line 777
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    .line 778
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 779
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->createPreIssue()V

    goto :goto_0

    .line 783
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->updateUserInputState()V

    .line 787
    :goto_0
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalRenderer()Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->hideNetworkErrorFooter()V

    return-void
.end method

.method public onNetworkUnAvailable()V
    .locals 4

    const/4 v0, 0x0

    .line 792
    iput-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isNetworkAvailable:Z

    .line 795
    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->showFakeTypingIndicator(Z)V

    .line 800
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    .line 801
    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v1

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    .line 802
    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v1

    iget-object v1, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->awaitingUserInputForBotStep:Z

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 804
    :goto_0
    iget-boolean v3, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isInBetweenBotExecution:Z

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->awaitingUserInputForBotStep:Z

    if-nez v3, :cond_2

    const/4 v0, 0x1

    :cond_2
    if-nez v1, :cond_3

    if-eqz v0, :cond_4

    .line 807
    :cond_3
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalRenderer()Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->showNetworkErrorFooter(I)V

    :cond_4
    return-void
.end method

.method public onNewConversationButtonClicked()V
    .locals 2

    .line 931
    invoke-super {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onNewConversationButtonClicked()V

    .line 932
    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->showConversationHistory:Z

    if-eqz v0, :cond_1

    .line 935
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->hideAllFooterWidgets()V

    .line 938
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getOpenConversationWithMessages()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    if-nez v0, :cond_0

    .line 943
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->createLocalPreIssueConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 947
    :cond_0
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v1, v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->onNewConversationStarted(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    .line 949
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->refreshVM()V

    .line 950
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->renderMenuItems()V

    .line 951
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->initMessagesList()V

    .line 953
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->notifyRefreshList()V

    goto :goto_0

    .line 956
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->openFreshConversationScreen()V

    :goto_0
    return-void
.end method

.method public onSkipClick()V
    .locals 3

    .line 473
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->updateLastUserActivityTime()V

    .line 475
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalMediator()Lcom/helpshift/conversation/viewmodel/ConversationalMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->getRenderedBotMessage()Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    move-result-object v0

    .line 476
    instance-of v1, v0, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;

    if-eqz v1, :cond_0

    .line 480
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->clearUserReplyDraft()V

    .line 483
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->disableUserInputOptions()V

    .line 485
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v2, Lcom/helpshift/conversation/viewmodel/ConversationalVM$7;

    invoke-direct {v2, p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM$7;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    invoke-virtual {v1, v2}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    .line 503
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalRenderer()Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->hideSkipButton()V

    return-void
.end method

.method public onUIMessageListUpdated()V
    .locals 1

    .line 999
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    if-eqz v0, :cond_0

    .line 1000
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->conversationMediator:Lcom/helpshift/conversation/viewmodel/ConversationMediator;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderReplyBoxWidget()V

    :cond_0
    return-void
.end method

.method protected prefillReplyBox()V
    .locals 3

    .line 123
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getUserReplyText()Ljava/lang/String;

    move-result-object v0

    .line 127
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v1

    .line 128
    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->containsAtleastOneUserMessage()Z

    move-result v1

    if-nez v1, :cond_0

    .line 129
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->conversationInboxDM:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getConversationArchivalPrefillText()Ljava/lang/String;

    move-result-object v0

    .line 131
    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v1, "conversationPrefillText"

    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    .line 141
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v1, v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->setReply(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public refreshVM()V
    .locals 1

    .line 284
    invoke-super {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->refreshVM()V

    .line 287
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    .line 288
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->createPreIssue()V

    goto :goto_0

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-direct {p0, v0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->evaluateBotMessages(Ljava/util/Collection;)Ljava/util/List;

    :goto_0
    return-void
.end method

.method public retryMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 2

    .line 451
    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isNetworkAvailable:Z

    if-nez v0, :cond_0

    return-void

    .line 455
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationalVM$6;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationalVM$6;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method protected sendTextMessage(Ljava/lang/String;)V
    .locals 3

    .line 382
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->updateLastUserActivityTime()V

    .line 384
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 386
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->containsAtleastOneUserMessage()Z

    move-result v0

    if-nez v0, :cond_0

    .line 387
    invoke-static {p1}, Lcom/helpshift/common/StringUtils;->userVisibleCharacterCount(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    invoke-virtual {v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getMinimumConversationDescriptionLength()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 388
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalRenderer()Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    move-result-object p1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->showReplyValidationFailedError(I)V

    return-void

    .line 392
    :cond_0
    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isInBetweenBotExecution:Z

    if-nez v0, :cond_1

    .line 393
    invoke-super {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->sendTextMessage(Ljava/lang/String;)V

    return-void

    .line 398
    :cond_1
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalMediator()Lcom/helpshift/conversation/viewmodel/ConversationalMediator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationalMediator;->getRenderedBotMessage()Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 400
    instance-of v1, v0, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;

    if-nez v1, :cond_2

    goto :goto_0

    .line 405
    :cond_2
    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;

    .line 406
    iget-object v1, v0, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/TextInput;

    .line 407
    iget-object v2, v0, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/TextInput;

    invoke-virtual {v2, p1}, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->validate(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 410
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalRenderer()Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    move-result-object p1

    iget v0, v1, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->keyboard:I

    invoke-interface {p1, v0}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->showReplyValidationFailedError(I)V

    return-void

    .line 414
    :cond_3
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalRenderer()Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    move-result-object v1

    invoke-interface {v1}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->hideReplyValidationFailedError()V

    .line 417
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->disableUserInputOptions()V

    .line 418
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->clearReply()V

    .line 420
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v2, Lcom/helpshift/conversation/viewmodel/ConversationalVM$4;

    invoke-direct {v2, p0, p1, v0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM$4;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM;Ljava/lang/String;Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;)V

    invoke-virtual {v1, v2}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void

    .line 401
    :cond_4
    :goto_0
    invoke-super {p0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->sendTextMessage()V

    return-void
.end method

.method public showEmptyListPickerView()V
    .locals 1

    .line 1017
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalRenderer()Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->showEmptyListPickerView()V

    return-void
.end method

.method showFakeTypingIndicator(Z)V
    .locals 2

    .line 359
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationalVM$3;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationalVM$3;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM;Z)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public showListPicker(Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;)V
    .locals 2

    .line 1022
    new-instance v0, Lcom/helpshift/conversation/viewmodel/ListPickerVM;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-direct {v0, v1, p1, p0}, Lcom/helpshift/conversation/viewmodel/ListPickerVM;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;Lcom/helpshift/conversation/viewmodel/ListPickerVMCallback;)V

    iput-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->listPickerVM:Lcom/helpshift/conversation/viewmodel/ListPickerVM;

    .line 1023
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationalVM$14;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationalVM$14;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM;Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public showPickerClearButton()V
    .locals 1

    .line 1048
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalRenderer()Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->showPickerClearButton()V

    return-void
.end method

.method splitOptionsBotMessage(Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithOptionInputDM;)Lcom/helpshift/util/ValuePair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithOptionInputDM;",
            ")",
            "Lcom/helpshift/util/ValuePair<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            "Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 980
    :cond_0
    new-instance v0, Lcom/helpshift/conversation/activeconversation/message/AdminMessageDM;

    invoke-direct {v0, p1}, Lcom/helpshift/conversation/activeconversation/message/AdminMessageDM;-><init>(Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithOptionInputDM;)V

    .line 981
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;

    invoke-direct {v1, p1}, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;-><init>(Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithOptionInputDM;)V

    .line 983
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v1, p1, v2}, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 984
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v0, p1, v2}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 985
    new-instance p1, Lcom/helpshift/util/ValuePair;

    invoke-direct {p1, v0, v1}, Lcom/helpshift/util/ValuePair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method

.method splitOptionsBotMessage(Lcom/helpshift/conversation/activeconversation/message/FAQListMessageWithOptionInputDM;)Lcom/helpshift/util/ValuePair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/helpshift/conversation/activeconversation/message/FAQListMessageWithOptionInputDM;",
            ")",
            "Lcom/helpshift/util/ValuePair<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            "Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 966
    :cond_0
    new-instance v0, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;

    invoke-direct {v0, p1}, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;-><init>(Lcom/helpshift/conversation/activeconversation/message/FAQListMessageWithOptionInputDM;)V

    .line 967
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;

    invoke-direct {v1, p1}, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;-><init>(Lcom/helpshift/conversation/activeconversation/message/FAQListMessageWithOptionInputDM;)V

    .line 969
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v1, p1, v2}, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 970
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v0, p1, v2}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 971
    new-instance p1, Lcom/helpshift/util/ValuePair;

    invoke-direct {p1, v0, v1}, Lcom/helpshift/util/ValuePair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method

.method public update(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 0

    .line 541
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->updateUserInputState()V

    .line 542
    invoke-super {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->update(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    return-void
.end method

.method public bridge synthetic update(Ljava/lang/Object;)V
    .locals 0

    .line 54
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->update(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    return-void
.end method

.method public updateListPickerOptions(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/viewmodel/OptionUIModel;",
            ">;)V"
        }
    .end annotation

    .line 1006
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->getConversationalRenderer()Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->updateListPickerOptions(Ljava/util/List;)V

    return-void
.end method
