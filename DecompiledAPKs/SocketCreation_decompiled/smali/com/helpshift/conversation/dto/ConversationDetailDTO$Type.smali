.class public interface abstract Lcom/helpshift/conversation/dto/ConversationDetailDTO$Type;
.super Ljava/lang/Object;
.source "ConversationDetailDTO.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/helpshift/conversation/dto/ConversationDetailDTO;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Type"
.end annotation


# static fields
.field public static final ARCHIVAL_TEXT:I = 0x3

.field public static final NONE:I = 0x0

.field public static final PREFILL_CONFIG:I = 0x2

.field public static final USER_INPUT:I = 0x1
