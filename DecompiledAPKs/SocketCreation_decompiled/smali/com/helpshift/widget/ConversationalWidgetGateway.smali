.class public Lcom/helpshift/widget/ConversationalWidgetGateway;
.super Lcom/helpshift/widget/WidgetGateway;
.source "ConversationalWidgetGateway.java"


# instance fields
.field private conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;


# direct methods
.method public constructor <init>(Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p3}, Lcom/helpshift/widget/WidgetGateway;-><init>(Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;)V

    .line 18
    iput-object p2, p0, Lcom/helpshift/widget/ConversationalWidgetGateway;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    return-void
.end method


# virtual methods
.method public getDefaultVisibilityForAttachImageButton()Z
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/helpshift/widget/ConversationalWidgetGateway;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/helpshift/widget/WidgetGateway;->getDefaultVisibilityForAttachImageButton()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getDefaultVisibilityForConversationInfoButtonWidget()Z
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/helpshift/widget/ConversationalWidgetGateway;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/helpshift/widget/WidgetGateway;->getDefaultVisibilityForConversationInfoButtonWidget()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public updateReplyBoxWidget(Lcom/helpshift/widget/ButtonWidget;Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)V
    .locals 1

    .line 26
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p2, 0x0

    .line 27
    invoke-virtual {p1, p2}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    return-void

    .line 31
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/helpshift/widget/WidgetGateway;->updateReplyBoxWidget(Lcom/helpshift/widget/ButtonWidget;Lcom/helpshift/conversation/activeconversation/ConversationDM;Z)V

    return-void
.end method
