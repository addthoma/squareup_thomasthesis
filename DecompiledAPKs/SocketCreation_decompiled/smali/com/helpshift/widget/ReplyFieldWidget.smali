.class public Lcom/helpshift/widget/ReplyFieldWidget;
.super Lcom/helpshift/widget/Widget;
.source "ReplyFieldWidget.java"


# instance fields
.field private isEnabled:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    .line 7
    invoke-direct {p0}, Lcom/helpshift/widget/Widget;-><init>()V

    .line 8
    iput-boolean p1, p0, Lcom/helpshift/widget/ReplyFieldWidget;->isEnabled:Z

    return-void
.end method


# virtual methods
.method public isEnabled()Z
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/helpshift/widget/ReplyFieldWidget;->isEnabled:Z

    return v0
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 17
    iget-boolean v0, p0, Lcom/helpshift/widget/ReplyFieldWidget;->isEnabled:Z

    if-eq v0, p1, :cond_0

    .line 18
    iput-boolean p1, p0, Lcom/helpshift/widget/ReplyFieldWidget;->isEnabled:Z

    .line 19
    invoke-virtual {p0}, Lcom/helpshift/widget/ReplyFieldWidget;->notifyChanged()V

    :cond_0
    return-void
.end method
