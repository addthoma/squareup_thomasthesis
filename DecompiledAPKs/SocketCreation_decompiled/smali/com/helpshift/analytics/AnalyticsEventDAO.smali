.class public interface abstract Lcom/helpshift/analytics/AnalyticsEventDAO;
.super Ljava/lang/Object;
.source "AnalyticsEventDAO.java"


# virtual methods
.method public abstract getUnsentAnalytics()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract removeAnalyticsData(Ljava/lang/String;)V
.end method

.method public abstract saveUnsentAnalyticsData(Ljava/lang/String;Ljava/util/HashMap;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method
