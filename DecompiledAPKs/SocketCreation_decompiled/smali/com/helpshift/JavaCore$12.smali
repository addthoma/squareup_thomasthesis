.class Lcom/helpshift/JavaCore$12;
.super Lcom/helpshift/common/domain/F;
.source "JavaCore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/JavaCore;->sendFailedApiCalls()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/JavaCore;


# direct methods
.method constructor <init>(Lcom/helpshift/JavaCore;)V
    .locals 0

    .line 336
    iput-object p1, p0, Lcom/helpshift/JavaCore$12;->this$0:Lcom/helpshift/JavaCore;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 2

    .line 341
    iget-object v0, p0, Lcom/helpshift/JavaCore$12;->this$0:Lcom/helpshift/JavaCore;

    invoke-virtual {v0}, Lcom/helpshift/JavaCore;->getFaqDM()Lcom/helpshift/faq/FaqsDM;

    .line 342
    iget-object v0, p0, Lcom/helpshift/JavaCore$12;->this$0:Lcom/helpshift/JavaCore;

    invoke-virtual {v0}, Lcom/helpshift/JavaCore;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object v0

    .line 343
    iget-object v1, p0, Lcom/helpshift/JavaCore$12;->this$0:Lcom/helpshift/JavaCore;

    invoke-virtual {v1}, Lcom/helpshift/JavaCore;->getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    .line 346
    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserManagerDM;->getActiveUserSetupDM()Lcom/helpshift/account/domainmodel/UserSetupDM;

    .line 347
    iget-object v0, p0, Lcom/helpshift/JavaCore$12;->this$0:Lcom/helpshift/JavaCore;

    invoke-virtual {v0}, Lcom/helpshift/JavaCore;->getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    .line 348
    iget-object v0, p0, Lcom/helpshift/JavaCore$12;->this$0:Lcom/helpshift/JavaCore;

    invoke-static {v0}, Lcom/helpshift/JavaCore;->access$000(Lcom/helpshift/JavaCore;)Lcom/helpshift/common/domain/Domain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getAutoRetryFailedEventDM()Lcom/helpshift/common/AutoRetryFailedEventDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/common/AutoRetryFailedEventDM;->sendAllEvents()V

    return-void
.end method
