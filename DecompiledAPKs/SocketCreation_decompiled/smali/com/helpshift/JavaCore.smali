.class public Lcom/helpshift/JavaCore;
.super Ljava/lang/Object;
.source "JavaCore.java"

# interfaces
.implements Lcom/helpshift/CoreApi;


# static fields
.field private static final TAG:Ljava/lang/String; = "Helpshift_JavaCore"


# instance fields
.field final analyticsEventDM:Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

.field private domain:Lcom/helpshift/common/domain/Domain;

.field private isSDKSessionActive:Z

.field private final metaDataDM:Lcom/helpshift/meta/MetaDataDM;

.field private final parallelThreader:Lcom/helpshift/common/domain/Threader;

.field final platform:Lcom/helpshift/common/platform/Platform;

.field final sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

.field private final serialThreader:Lcom/helpshift/common/domain/Threader;

.field private userManagerDM:Lcom/helpshift/account/domainmodel/UserManagerDM;


# direct methods
.method public constructor <init>(Lcom/helpshift/common/platform/Platform;)V
    .locals 1

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 64
    iput-boolean v0, p0, Lcom/helpshift/JavaCore;->isSDKSessionActive:Z

    .line 67
    iput-object p1, p0, Lcom/helpshift/JavaCore;->platform:Lcom/helpshift/common/platform/Platform;

    .line 68
    new-instance v0, Lcom/helpshift/common/domain/Domain;

    invoke-direct {v0, p1}, Lcom/helpshift/common/domain/Domain;-><init>(Lcom/helpshift/common/platform/Platform;)V

    iput-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    .line 69
    iget-object p1, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/JavaCore;->userManagerDM:Lcom/helpshift/account/domainmodel/UserManagerDM;

    .line 70
    iget-object p1, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getSerialThreader()Lcom/helpshift/common/domain/Threader;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/JavaCore;->serialThreader:Lcom/helpshift/common/domain/Threader;

    .line 71
    iget-object p1, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getParallelThreader()Lcom/helpshift/common/domain/Threader;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/JavaCore;->parallelThreader:Lcom/helpshift/common/domain/Threader;

    .line 72
    iget-object p1, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/JavaCore;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    .line 73
    iget-object p1, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/JavaCore;->analyticsEventDM:Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    .line 74
    iget-object p1, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getMetaDataDM()Lcom/helpshift/meta/MetaDataDM;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/JavaCore;->metaDataDM:Lcom/helpshift/meta/MetaDataDM;

    return-void
.end method

.method static synthetic access$000(Lcom/helpshift/JavaCore;)Lcom/helpshift/common/domain/Domain;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    return-object p0
.end method

.method static synthetic access$100(Lcom/helpshift/JavaCore;)Lcom/helpshift/account/domainmodel/UserManagerDM;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/helpshift/JavaCore;->userManagerDM:Lcom/helpshift/account/domainmodel/UserManagerDM;

    return-object p0
.end method

.method private runParallel(Lcom/helpshift/common/domain/F;)V
    .locals 1

    .line 489
    iget-object v0, p0, Lcom/helpshift/JavaCore;->parallelThreader:Lcom/helpshift/common/domain/Threader;

    invoke-interface {v0, p1}, Lcom/helpshift/common/domain/Threader;->thread(Lcom/helpshift/common/domain/F;)Lcom/helpshift/common/domain/F;

    move-result-object p1

    invoke-virtual {p1}, Lcom/helpshift/common/domain/F;->f()V

    return-void
.end method

.method private runSerial(Lcom/helpshift/common/domain/F;)V
    .locals 1

    .line 485
    iget-object v0, p0, Lcom/helpshift/JavaCore;->serialThreader:Lcom/helpshift/common/domain/Threader;

    invoke-interface {v0, p1}, Lcom/helpshift/common/domain/Threader;->thread(Lcom/helpshift/common/domain/F;)Lcom/helpshift/common/domain/F;

    move-result-object p1

    invoke-virtual {p1}, Lcom/helpshift/common/domain/F;->f()V

    return-void
.end method


# virtual methods
.method public declared-synchronized clearAnonymousUser()Z
    .locals 3

    monitor-enter p0

    .line 165
    :try_start_0
    new-instance v0, Lcom/helpshift/account/domainmodel/UserLoginManager;

    iget-object v1, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/JavaCore;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v0, p0, v1, v2}, Lcom/helpshift/account/domainmodel/UserLoginManager;-><init>(Lcom/helpshift/CoreApi;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserLoginManager;->clearAnonymousUser()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public fetchServerConfig()V
    .locals 1

    .line 237
    new-instance v0, Lcom/helpshift/JavaCore$6;

    invoke-direct {v0, p0}, Lcom/helpshift/JavaCore$6;-><init>(Lcom/helpshift/JavaCore;)V

    invoke-direct {p0, v0}, Lcom/helpshift/JavaCore;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 1

    .line 128
    invoke-virtual {p0}, Lcom/helpshift/JavaCore;->getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getActiveConversationFromStorage()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    return-object v0
.end method

.method public getActiveConversationOrPreIssue()Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 1

    .line 134
    invoke-virtual {p0}, Lcom/helpshift/JavaCore;->getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getActiveConversationOrPreIssue()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    return-object v0
.end method

.method public getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/helpshift/JavaCore;->analyticsEventDM:Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    return-object v0
.end method

.method public getAttachmentFileManagerDM()Lcom/helpshift/common/domain/AttachmentFileManagerDM;
    .locals 1

    .line 439
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getAttachmentFileManagerDM()Lcom/helpshift/common/domain/AttachmentFileManagerDM;

    move-result-object v0

    return-object v0
.end method

.method public getAutoRetryFailedEventDM()Lcom/helpshift/common/AutoRetryFailedEventDM;
    .locals 1

    .line 444
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getAutoRetryFailedEventDM()Lcom/helpshift/common/AutoRetryFailedEventDM;

    move-result-object v0

    return-object v0
.end method

.method public getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;
    .locals 1

    .line 419
    invoke-virtual {p0}, Lcom/helpshift/JavaCore;->getConversationInboxManagerDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;->getActiveConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v0

    return-object v0
.end method

.method getConversationInboxManagerDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;
    .locals 1

    .line 476
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getConversationInboxManagerDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;

    move-result-object v0

    return-object v0
.end method

.method public getConversationInboxPoller()Lcom/helpshift/conversation/ConversationInboxPoller;
    .locals 1

    .line 375
    invoke-virtual {p0}, Lcom/helpshift/JavaCore;->getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getConversationInboxPoller()Lcom/helpshift/conversation/ConversationInboxPoller;

    move-result-object v0

    return-object v0
.end method

.method public getConversationInfoViewModel(Lcom/helpshift/conversation/activeconversation/ConversationInfoRenderer;)Lcom/helpshift/conversation/viewmodel/ConversationInfoVM;
    .locals 2

    .line 118
    new-instance v0, Lcom/helpshift/conversation/viewmodel/ConversationInfoVM;

    iget-object v1, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-direct {v0, v1, p1}, Lcom/helpshift/conversation/viewmodel/ConversationInfoVM;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/conversation/activeconversation/ConversationInfoRenderer;)V

    return-object v0
.end method

.method public getConversationViewModel(Ljava/lang/Long;Lcom/helpshift/conversation/activeconversation/ConversationRenderer;Z)Lcom/helpshift/conversation/viewmodel/ConversationVM;
    .locals 9

    .line 91
    invoke-virtual {p0}, Lcom/helpshift/JavaCore;->getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v0

    const/4 v1, 0x0

    .line 92
    invoke-virtual {v0, v1, p1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getConversationDM(ZLjava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object v6

    .line 93
    new-instance p1, Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v3, p0, Lcom/helpshift/JavaCore;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v4, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    .line 95
    invoke-virtual {p0}, Lcom/helpshift/JavaCore;->getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v5

    move-object v2, p1

    move-object v7, p2

    move v8, p3

    invoke-direct/range {v2 .. v8}, Lcom/helpshift/conversation/viewmodel/ConversationVM;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Lcom/helpshift/conversation/activeconversation/ViewableConversation;Lcom/helpshift/conversation/activeconversation/ConversationRenderer;Z)V

    return-object p1
.end method

.method public getConversationalViewModel(ZLjava/lang/Long;Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;Z)Lcom/helpshift/conversation/viewmodel/ConversationalVM;
    .locals 9

    .line 105
    invoke-virtual {p0}, Lcom/helpshift/JavaCore;->getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v0

    .line 106
    invoke-virtual {v0, p1, p2}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getConversationDM(ZLjava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object v5

    .line 107
    new-instance p2, Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    iget-object v2, p0, Lcom/helpshift/JavaCore;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v3, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    .line 109
    invoke-virtual {p0}, Lcom/helpshift/JavaCore;->getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v4

    move-object v1, p2

    move-object v6, p3

    move v7, p1

    move v8, p4

    invoke-direct/range {v1 .. v8}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Lcom/helpshift/conversation/activeconversation/ViewableConversation;Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;ZZ)V

    return-object p2
.end method

.method public getCryptoDM()Lcom/helpshift/crypto/CryptoDM;
    .locals 1

    .line 429
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getCryptoDM()Lcom/helpshift/crypto/CryptoDM;

    move-result-object v0

    return-object v0
.end method

.method public getCustomIssueFieldDM()Lcom/helpshift/cif/CustomIssueFieldDM;
    .locals 1

    .line 365
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getCustomIssueFieldDM()Lcom/helpshift/cif/CustomIssueFieldDM;

    move-result-object v0

    return-object v0
.end method

.method public getDelegate()Lcom/helpshift/delegate/UIThreadDelegateDecorator;
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getDelegate()Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    move-result-object v0

    return-object v0
.end method

.method public getDomain()Lcom/helpshift/common/domain/Domain;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    return-object v0
.end method

.method public getErrorReportsDM()Lcom/helpshift/logger/ErrorReportsDM;
    .locals 1

    .line 481
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getErrorReportsDM()Lcom/helpshift/logger/ErrorReportsDM;

    move-result-object v0

    return-object v0
.end method

.method public getFaqDM()Lcom/helpshift/faq/FaqsDM;
    .locals 1

    .line 424
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getFaqsDM()Lcom/helpshift/faq/FaqsDM;

    move-result-object v0

    return-object v0
.end method

.method public getLocaleProviderDM()Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;
    .locals 1

    .line 434
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getLocaleProviderDM()Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;

    move-result-object v0

    return-object v0
.end method

.method public getMetaDataDM()Lcom/helpshift/meta/MetaDataDM;
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/helpshift/JavaCore;->metaDataDM:Lcom/helpshift/meta/MetaDataDM;

    return-object v0
.end method

.method public getNewConversationViewModel(Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;)Lcom/helpshift/conversation/viewmodel/NewConversationVM;
    .locals 4

    .line 84
    new-instance v0, Lcom/helpshift/conversation/viewmodel/NewConversationVM;

    iget-object v1, p0, Lcom/helpshift/JavaCore;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v2, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p0}, Lcom/helpshift/JavaCore;->getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/helpshift/conversation/viewmodel/NewConversationVM;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;)V

    return-object v0
.end method

.method public getNotificationCountAsync(Lcom/helpshift/common/FetchDataFromThread;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/helpshift/common/FetchDataFromThread<",
            "Lcom/helpshift/util/ValuePair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 396
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/JavaCore$14;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/JavaCore$14;-><init>(Lcom/helpshift/JavaCore;Lcom/helpshift/common/FetchDataFromThread;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public getNotificationCountSync()I
    .locals 1

    .line 390
    invoke-virtual {p0}, Lcom/helpshift/JavaCore;->getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->getNotificationCountSync()I

    move-result v0

    return v0
.end method

.method public getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;
    .locals 1

    .line 370
    iget-object v0, p0, Lcom/helpshift/JavaCore;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    return-object v0
.end method

.method public getScreenshotPreviewModel(Lcom/helpshift/conversation/activeconversation/ScreenshotPreviewRenderer;)Lcom/helpshift/conversation/viewmodel/ScreenshotPreviewVM;
    .locals 2

    .line 123
    new-instance v0, Lcom/helpshift/conversation/viewmodel/ScreenshotPreviewVM;

    iget-object v1, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-direct {v0, v1, p1}, Lcom/helpshift/conversation/viewmodel/ScreenshotPreviewVM;-><init>(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/conversation/activeconversation/ScreenshotPreviewRenderer;)V

    return-object v0
.end method

.method public getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/helpshift/JavaCore;->userManagerDM:Lcom/helpshift/account/domainmodel/UserManagerDM;

    return-object v0
.end method

.method public getUserSetupVM(Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;)Lcom/helpshift/conversation/usersetup/UserSetupVM;
    .locals 4

    .line 139
    new-instance v0, Lcom/helpshift/conversation/usersetup/UserSetupVM;

    iget-object v1, p0, Lcom/helpshift/JavaCore;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v2, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p0}, Lcom/helpshift/JavaCore;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object v3

    invoke-virtual {v3}, Lcom/helpshift/account/domainmodel/UserManagerDM;->getActiveUserSetupDM()Lcom/helpshift/account/domainmodel/UserSetupDM;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, p1}, Lcom/helpshift/conversation/usersetup/UserSetupVM;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserSetupDM;Lcom/helpshift/conversation/activeconversation/usersetup/UserSetupRenderer;)V

    return-object v0
.end method

.method public handlePushNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 380
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/JavaCore$13;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/helpshift/JavaCore$13;-><init>(Lcom/helpshift/JavaCore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public isActiveConversationActionable()Z
    .locals 1

    .line 144
    invoke-virtual {p0}, Lcom/helpshift/JavaCore;->getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->isActiveConversationActionable()Z

    move-result v0

    return v0
.end method

.method public isSDKSessionActive()Z
    .locals 1

    .line 150
    iget-boolean v0, p0, Lcom/helpshift/JavaCore;->isSDKSessionActive:Z

    return v0
.end method

.method public declared-synchronized login(Lcom/helpshift/HelpshiftUser;)Z
    .locals 3

    monitor-enter p0

    .line 169
    :try_start_0
    new-instance v0, Lcom/helpshift/account/domainmodel/UserLoginManager;

    iget-object v1, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/JavaCore;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v0, p0, v1, v2}, Lcom/helpshift/account/domainmodel/UserLoginManager;-><init>(Lcom/helpshift/CoreApi;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    invoke-virtual {v0, p1}, Lcom/helpshift/account/domainmodel/UserLoginManager;->login(Lcom/helpshift/HelpshiftUser;)Z

    move-result p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized logout()Z
    .locals 3

    monitor-enter p0

    .line 186
    :try_start_0
    new-instance v0, Lcom/helpshift/account/domainmodel/UserLoginManager;

    iget-object v1, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/JavaCore;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v0, p0, v1, v2}, Lcom/helpshift/account/domainmodel/UserLoginManager;-><init>(Lcom/helpshift/CoreApi;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserLoginManager;->logout()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onSDKSessionEnded()V
    .locals 1

    const/4 v0, 0x0

    .line 226
    iput-boolean v0, p0, Lcom/helpshift/JavaCore;->isSDKSessionActive:Z

    .line 227
    new-instance v0, Lcom/helpshift/JavaCore$5;

    invoke-direct {v0, p0}, Lcom/helpshift/JavaCore$5;-><init>(Lcom/helpshift/JavaCore;)V

    invoke-direct {p0, v0}, Lcom/helpshift/JavaCore;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public onSDKSessionStarted()V
    .locals 1

    const/4 v0, 0x1

    .line 215
    iput-boolean v0, p0, Lcom/helpshift/JavaCore;->isSDKSessionActive:Z

    .line 216
    new-instance v0, Lcom/helpshift/JavaCore$4;

    invoke-direct {v0, p0}, Lcom/helpshift/JavaCore$4;-><init>(Lcom/helpshift/JavaCore;)V

    invoke-direct {p0, v0}, Lcom/helpshift/JavaCore;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public refreshPoller()V
    .locals 2

    .line 255
    invoke-virtual {p0}, Lcom/helpshift/JavaCore;->getConversationInboxPoller()Lcom/helpshift/conversation/ConversationInboxPoller;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/ConversationInboxPoller;->refreshPoller(Z)V

    return-void
.end method

.method public resetPreIssueConversations()V
    .locals 1

    .line 472
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getConversationInboxManagerDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/conversation/domainmodel/ConversationInboxManagerDM;->resetPreIssueConversations()V

    return-void
.end method

.method public sendAnalyticsEvent()V
    .locals 1

    .line 298
    new-instance v0, Lcom/helpshift/JavaCore$10;

    invoke-direct {v0, p0}, Lcom/helpshift/JavaCore$10;-><init>(Lcom/helpshift/JavaCore;)V

    invoke-direct {p0, v0}, Lcom/helpshift/JavaCore;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public sendAppStartEvent()V
    .locals 1

    .line 318
    new-instance v0, Lcom/helpshift/JavaCore$11;

    invoke-direct {v0, p0}, Lcom/helpshift/JavaCore$11;-><init>(Lcom/helpshift/JavaCore;)V

    invoke-direct {p0, v0}, Lcom/helpshift/JavaCore;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public sendFailedApiCalls()V
    .locals 2

    .line 336
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/JavaCore$12;

    invoke-direct {v1, p0}, Lcom/helpshift/JavaCore$12;-><init>(Lcom/helpshift/JavaCore;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public sendRequestIdsForSuccessfulApiCalls()V
    .locals 2

    .line 449
    iget-object v0, p0, Lcom/helpshift/JavaCore;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/JavaCore$15;

    invoke-direct {v1, p0}, Lcom/helpshift/JavaCore$15;-><init>(Lcom/helpshift/JavaCore;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public setDelegateListener(Lcom/helpshift/delegate/RootDelegate;)V
    .locals 1

    .line 155
    new-instance v0, Lcom/helpshift/JavaCore$1;

    invoke-direct {v0, p0, p1}, Lcom/helpshift/JavaCore$1;-><init>(Lcom/helpshift/JavaCore;Lcom/helpshift/delegate/RootDelegate;)V

    invoke-direct {p0, v0}, Lcom/helpshift/JavaCore;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public setInstallCredentials(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 260
    new-instance v0, Lcom/helpshift/JavaCore$7;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/helpshift/JavaCore$7;-><init>(Lcom/helpshift/JavaCore;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/helpshift/JavaCore;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public setNameAndEmail(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 174
    new-instance v0, Lcom/helpshift/JavaCore$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/helpshift/JavaCore$2;-><init>(Lcom/helpshift/JavaCore;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/helpshift/JavaCore;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public setPushToken(Ljava/lang/String;)V
    .locals 1

    .line 191
    new-instance v0, Lcom/helpshift/JavaCore$3;

    invoke-direct {v0, p0, p1}, Lcom/helpshift/JavaCore$3;-><init>(Lcom/helpshift/JavaCore;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/helpshift/JavaCore;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public updateApiConfig(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 281
    new-instance v0, Lcom/helpshift/JavaCore$9;

    invoke-direct {v0, p0, p1, p0}, Lcom/helpshift/JavaCore$9;-><init>(Lcom/helpshift/JavaCore;Ljava/util/Map;Lcom/helpshift/CoreApi;)V

    invoke-direct {p0, v0}, Lcom/helpshift/JavaCore;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public updateInstallConfig(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 270
    new-instance v0, Lcom/helpshift/JavaCore$8;

    invoke-direct {v0, p0, p1}, Lcom/helpshift/JavaCore$8;-><init>(Lcom/helpshift/JavaCore;Ljava/util/Map;)V

    invoke-direct {p0, v0}, Lcom/helpshift/JavaCore;->runSerial(Lcom/helpshift/common/domain/F;)V

    return-void
.end method
