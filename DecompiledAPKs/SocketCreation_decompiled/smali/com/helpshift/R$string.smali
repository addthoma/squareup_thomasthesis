.class public final Lcom/helpshift/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/helpshift/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f120001

.field public static final abc_action_bar_up_description:I = 0x7f120002

.field public static final abc_action_menu_overflow_description:I = 0x7f120003

.field public static final abc_action_mode_done:I = 0x7f120004

.field public static final abc_activity_chooser_view_see_all:I = 0x7f120005

.field public static final abc_activitychooserview_choose_application:I = 0x7f120006

.field public static final abc_capital_off:I = 0x7f120007

.field public static final abc_capital_on:I = 0x7f120008

.field public static final abc_menu_alt_shortcut_label:I = 0x7f120009

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f12000a

.field public static final abc_menu_delete_shortcut_label:I = 0x7f12000b

.field public static final abc_menu_enter_shortcut_label:I = 0x7f12000c

.field public static final abc_menu_function_shortcut_label:I = 0x7f12000d

.field public static final abc_menu_meta_shortcut_label:I = 0x7f12000e

.field public static final abc_menu_shift_shortcut_label:I = 0x7f12000f

.field public static final abc_menu_space_shortcut_label:I = 0x7f120010

.field public static final abc_menu_sym_shortcut_label:I = 0x7f120011

.field public static final abc_prepend_shortcut_label:I = 0x7f120012

.field public static final abc_search_hint:I = 0x7f120013

.field public static final abc_searchview_description_clear:I = 0x7f120014

.field public static final abc_searchview_description_query:I = 0x7f120015

.field public static final abc_searchview_description_search:I = 0x7f120016

.field public static final abc_searchview_description_submit:I = 0x7f120017

.field public static final abc_searchview_description_voice:I = 0x7f120018

.field public static final abc_shareactionprovider_share_with:I = 0x7f120019

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f12001a

.field public static final abc_toolbar_collapse_description:I = 0x7f12001b

.field public static final appbar_scrolling_view_behavior:I = 0x7f1200d2

.field public static final bottom_sheet_behavior:I = 0x7f120190

.field public static final character_counter_content_description:I = 0x7f1203f3

.field public static final character_counter_pattern:I = 0x7f1203f5

.field public static final fab_transformation_scrim_behavior:I = 0x7f120aa4

.field public static final fab_transformation_sheet_behavior:I = 0x7f120aa5

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f120b51

.field public static final hs__agent_message_voice_over:I = 0x7f120b54

.field public static final hs__agent_message_with_name_voice_over:I = 0x7f120b55

.field public static final hs__app_review_button:I = 0x7f120b56

.field public static final hs__attach_screenshot_btn:I = 0x7f120b57

.field public static final hs__attachment_downloaded__voice_over:I = 0x7f120b58

.field public static final hs__attachment_downloading_voice_over:I = 0x7f120b59

.field public static final hs__attachment_not_downloaded_voice_over:I = 0x7f120b5a

.field public static final hs__authentication_failed:I = 0x7f120b5b

.field public static final hs__ca_msg:I = 0x7f120b5c

.field public static final hs__change_btn:I = 0x7f120b5d

.field public static final hs__chat_hint:I = 0x7f120b5e

.field public static final hs__confirmation_footer_msg:I = 0x7f120b5f

.field public static final hs__confirmation_msg:I = 0x7f120b60

.field public static final hs__connecting:I = 0x7f120b61

.field public static final hs__contact_us_btn:I = 0x7f120b62

.field public static final hs__conversation_detail_error:I = 0x7f120b63

.field public static final hs__conversation_end_msg:I = 0x7f120b64

.field public static final hs__conversation_header:I = 0x7f120b65

.field public static final hs__conversation_issue_id_header:I = 0x7f120b66

.field public static final hs__conversation_publish_id_voice_over:I = 0x7f120b67

.field public static final hs__conversation_redacted_status:I = 0x7f120b68

.field public static final hs__conversation_redacted_status_multiple:I = 0x7f120b69

.field public static final hs__conversation_rejected_status:I = 0x7f120b6a

.field public static final hs__conversation_started_message:I = 0x7f120b6b

.field public static final hs__conversations_divider_voice_over:I = 0x7f120b6c

.field public static final hs__copied_to_clipboard:I = 0x7f120b6d

.field public static final hs__copy:I = 0x7f120b6e

.field public static final hs__copy_to_clipboard_tooltip:I = 0x7f120b6f

.field public static final hs__could_not_open_attachment_msg:I = 0x7f120b70

.field public static final hs__could_not_reach_support_msg:I = 0x7f120b71

.field public static final hs__cr_msg:I = 0x7f120b72

.field public static final hs__csat_additonal_feedback_message:I = 0x7f120b73

.field public static final hs__csat_dislike_message:I = 0x7f120b74

.field public static final hs__csat_disliked_rating_message:I = 0x7f120b75

.field public static final hs__csat_like_message:I = 0x7f120b76

.field public static final hs__csat_liked_rating_message:I = 0x7f120b77

.field public static final hs__csat_message:I = 0x7f120b78

.field public static final hs__csat_ok_rating_message:I = 0x7f120b79

.field public static final hs__csat_option_message:I = 0x7f120b7a

.field public static final hs__csat_ratingbar:I = 0x7f120b7b

.field public static final hs__csat_submit_toast:I = 0x7f120b7c

.field public static final hs__data_not_found_msg:I = 0x7f120b7d

.field public static final hs__date_input_validation_error:I = 0x7f120b7e

.field public static final hs__default_notification_channel_desc:I = 0x7f120b7f

.field public static final hs__default_notification_channel_name:I = 0x7f120b80

.field public static final hs__default_notification_content_title:I = 0x7f120b81

.field public static final hs__description_invalid_length_error:I = 0x7f120b82

.field public static final hs__done_btn:I = 0x7f120b83

.field public static final hs__email_hint:I = 0x7f120b84

.field public static final hs__email_input_validation_error:I = 0x7f120b85

.field public static final hs__email_required_hint:I = 0x7f120b86

.field public static final hs__faq_fetching_fail_message:I = 0x7f120b87

.field public static final hs__faqs_search_footer:I = 0x7f120b88

.field public static final hs__feedback_button:I = 0x7f120b89

.field public static final hs__fetching_faqs_message:I = 0x7f120b8a

.field public static final hs__file_not_found_msg:I = 0x7f120b8b

.field public static final hs__help_header:I = 0x7f120b8c

.field public static final hs__image_downloaded_voice_over:I = 0x7f120b8d

.field public static final hs__image_downloading_voice_over:I = 0x7f120b8e

.field public static final hs__image_not_downloaded_voice_over:I = 0x7f120b8f

.field public static final hs__invalid_description_error:I = 0x7f120b90

.field public static final hs__invalid_email_error:I = 0x7f120b91

.field public static final hs__invalid_faq_publish_id_error:I = 0x7f120b92

.field public static final hs__invalid_section_publish_id_error:I = 0x7f120b93

.field public static final hs__issue_archival_message:I = 0x7f120b94

.field public static final hs__jump_button_voice_over:I = 0x7f120b95

.field public static final hs__jump_button_with_new_message_voice_over:I = 0x7f120b96

.field public static final hs__landscape_date_input_validation_error:I = 0x7f120b97

.field public static final hs__landscape_email_input_validation_error:I = 0x7f120b98

.field public static final hs__landscape_input_validation_dialog_title:I = 0x7f120b99

.field public static final hs__landscape_number_input_validation_error:I = 0x7f120b9a

.field public static final hs__mark_no:I = 0x7f120b9b

.field public static final hs__mark_yes:I = 0x7f120b9c

.field public static final hs__mark_yes_no_question:I = 0x7f120b9d

.field public static final hs__message_not_sent:I = 0x7f120b9e

.field public static final hs__messages_loading_error_text:I = 0x7f120b9f

.field public static final hs__messages_loading_text:I = 0x7f120ba0

.field public static final hs__network_error_msg:I = 0x7f120ba1

.field public static final hs__network_error_pre_issue_creation:I = 0x7f120ba2

.field public static final hs__network_reconnecting_error:I = 0x7f120ba3

.field public static final hs__network_unavailable_msg:I = 0x7f120ba4

.field public static final hs__new_conversation_btn:I = 0x7f120ba5

.field public static final hs__new_conversation_footer_generic_reason:I = 0x7f120ba6

.field public static final hs__new_conversation_header:I = 0x7f120ba7

.field public static final hs__new_conversation_hint:I = 0x7f120ba8

.field public static final hs__no_internet_error:I = 0x7f120ba9

.field public static final hs__no_internet_error_mgs:I = 0x7f120baa

.field public static final hs__no_search_results_message:I = 0x7f120bab

.field public static final hs__number_input_validation_error:I = 0x7f120bac

.field public static final hs__permission_denied_message:I = 0x7f120bad

.field public static final hs__permission_denied_snackbar_action:I = 0x7f120bae

.field public static final hs__permission_not_granted:I = 0x7f120baf

.field public static final hs__permission_rationale_snackbar_action_label:I = 0x7f120bb0

.field public static final hs__picker_clear_query_voice_over:I = 0x7f120bb1

.field public static final hs__picker_no_results:I = 0x7f120bb2

.field public static final hs__picker_option_list_item_voice_over:I = 0x7f120bb3

.field public static final hs__picker_options_expand_header_voice_over:I = 0x7f120bb4

.field public static final hs__picker_options_list_collapse_btn_voice_over:I = 0x7f120bb5

.field public static final hs__picker_search_edit_back_btn_voice_over:I = 0x7f120bb6

.field public static final hs__picker_search_hint:I = 0x7f120bb7

.field public static final hs__picker_search_icon_voice_over:I = 0x7f120bb8

.field public static final hs__question_header:I = 0x7f120bb9

.field public static final hs__question_helpful_message:I = 0x7f120bba

.field public static final hs__question_unhelpful_message:I = 0x7f120bbb

.field public static final hs__rate_button:I = 0x7f120bbc

.field public static final hs__remove_screenshot_btn:I = 0x7f120bbd

.field public static final hs__retry_button_voice_over:I = 0x7f120bbe

.field public static final hs__retry_faq_fetching_button:I = 0x7f120bbf

.field public static final hs__review_close_button:I = 0x7f120bc0

.field public static final hs__review_message:I = 0x7f120bc1

.field public static final hs__review_request_message:I = 0x7f120bc2

.field public static final hs__review_title:I = 0x7f120bc3

.field public static final hs__screenshot_add:I = 0x7f120bc4

.field public static final hs__screenshot_cloud_attach_error:I = 0x7f120bc5

.field public static final hs__screenshot_limit_error:I = 0x7f120bc6

.field public static final hs__screenshot_remove:I = 0x7f120bc7

.field public static final hs__screenshot_upload_error_msg:I = 0x7f120bc8

.field public static final hs__search_footer:I = 0x7f120bc9

.field public static final hs__search_hint:I = 0x7f120bca

.field public static final hs__search_result_message:I = 0x7f120bcb

.field public static final hs__search_result_title:I = 0x7f120bcc

.field public static final hs__search_title:I = 0x7f120bcd

.field public static final hs__select_a_question_message:I = 0x7f120bce

.field public static final hs__send_anyway:I = 0x7f120bcf

.field public static final hs__send_msg_btn:I = 0x7f120bd0

.field public static final hs__sending_fail_msg:I = 0x7f120bd1

.field public static final hs__sending_msg:I = 0x7f120bd2

.field public static final hs__ssl_handshake_error:I = 0x7f120bd3

.field public static final hs__ssl_peer_unverified_error:I = 0x7f120bd4

.field public static final hs__starting_download:I = 0x7f120bd5

.field public static final hs__submit_conversation_btn:I = 0x7f120bd6

.field public static final hs__tap_to_retry:I = 0x7f120bd7

.field public static final hs__try_again_msg:I = 0x7f120bd8

.field public static final hs__user_failed_message_voice_over:I = 0x7f120bd9

.field public static final hs__user_sending_message_voice_over:I = 0x7f120bda

.field public static final hs__user_sent_message_voice_over:I = 0x7f120bdb

.field public static final hs__user_setup_retry_description:I = 0x7f120bdc

.field public static final hs__username_blank_error:I = 0x7f120bdd

.field public static final hs__username_hint:I = 0x7f120bde

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f121026

.field public static final password_toggle_content_description:I = 0x7f121391

.field public static final path_password_eye:I = 0x7f121394

.field public static final path_password_eye_mask_strike_through:I = 0x7f121395

.field public static final path_password_eye_mask_visible:I = 0x7f121396

.field public static final path_password_strike_through:I = 0x7f121397

.field public static final search_menu_title:I = 0x7f121790

.field public static final status_bar_notification_info_overflow:I = 0x7f1218ca


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 1681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
