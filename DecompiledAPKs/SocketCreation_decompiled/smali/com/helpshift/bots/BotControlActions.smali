.class public interface abstract Lcom/helpshift/bots/BotControlActions;
.super Ljava/lang/Object;
.source "BotControlActions.java"


# static fields
.field public static final BOT_CANCELLED:Ljava/lang/String; = "bot_cancelled"

.field public static final BOT_ENDED:Ljava/lang/String; = "bot_ended"

.field public static final BOT_STARTED:Ljava/lang/String; = "bot_started"

.field public static final UNSUPPORTED_BOT_INPUT:Ljava/lang/String; = "unsupported_bot_input"
