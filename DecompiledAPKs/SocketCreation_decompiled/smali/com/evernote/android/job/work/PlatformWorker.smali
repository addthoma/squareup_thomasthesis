.class public Lcom/evernote/android/job/work/PlatformWorker;
.super Landroidx/work/Worker;
.source "PlatformWorker.java"


# static fields
.field private static final CAT:Lcom/evernote/android/job/util/JobCat;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 23
    new-instance v0, Lcom/evernote/android/job/util/JobCat;

    const-string v1, "PlatformWorker"

    invoke-direct {v0, v1}, Lcom/evernote/android/job/util/JobCat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/android/job/work/PlatformWorker;->CAT:Lcom/evernote/android/job/util/JobCat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroidx/work/WorkerParameters;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1, p2}, Landroidx/work/Worker;-><init>(Landroid/content/Context;Landroidx/work/WorkerParameters;)V

    return-void
.end method

.method private getJobId()I
    .locals 1

    .line 79
    invoke-virtual {p0}, Lcom/evernote/android/job/work/PlatformWorker;->getTags()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/evernote/android/job/work/JobProxyWorkManager;->getJobIdFromTags(Ljava/util/Collection;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public doWork()Landroidx/work/ListenableWorker$Result;
    .locals 6

    .line 32
    invoke-direct {p0}, Lcom/evernote/android/job/work/PlatformWorker;->getJobId()I

    move-result v0

    if-gez v0, :cond_0

    .line 34
    invoke-static {}, Landroidx/work/ListenableWorker$Result;->failure()Landroidx/work/ListenableWorker$Result;

    move-result-object v0

    return-object v0

    .line 38
    :cond_0
    :try_start_0
    new-instance v1, Lcom/evernote/android/job/JobProxy$Common;

    invoke-virtual {p0}, Lcom/evernote/android/job/work/PlatformWorker;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/evernote/android/job/work/PlatformWorker;->CAT:Lcom/evernote/android/job/util/JobCat;

    invoke-direct {v1, v2, v3, v0}, Lcom/evernote/android/job/JobProxy$Common;-><init>(Landroid/content/Context;Lcom/evernote/android/job/util/JobCat;I)V

    const/4 v2, 0x1

    .line 40
    invoke-virtual {v1, v2, v2}, Lcom/evernote/android/job/JobProxy$Common;->getPendingRequest(ZZ)Lcom/evernote/android/job/JobRequest;

    move-result-object v3

    if-nez v3, :cond_1

    .line 42
    invoke-static {}, Landroidx/work/ListenableWorker$Result;->failure()Landroidx/work/ListenableWorker$Result;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    invoke-static {v0}, Lcom/evernote/android/job/work/TransientBundleHolder;->cleanUpBundle(I)V

    return-object v1

    :cond_1
    const/4 v4, 0x0

    .line 46
    :try_start_1
    invoke-virtual {v3}, Lcom/evernote/android/job/JobRequest;->isTransient()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 47
    invoke-static {v0}, Lcom/evernote/android/job/work/TransientBundleHolder;->getBundle(I)Landroid/os/Bundle;

    move-result-object v4

    if-nez v4, :cond_2

    .line 49
    sget-object v1, Lcom/evernote/android/job/work/PlatformWorker;->CAT:Lcom/evernote/android/job/util/JobCat;

    const-string v4, "Transient bundle is gone for request %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v2, v5

    invoke-virtual {v1, v4, v2}, Lcom/evernote/android/job/util/JobCat;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    invoke-static {}, Landroidx/work/ListenableWorker$Result;->failure()Landroidx/work/ListenableWorker$Result;

    move-result-object v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61
    invoke-static {v0}, Lcom/evernote/android/job/work/TransientBundleHolder;->cleanUpBundle(I)V

    return-object v1

    .line 54
    :cond_2
    :try_start_2
    invoke-virtual {v1, v3, v4}, Lcom/evernote/android/job/JobProxy$Common;->executeJobRequest(Lcom/evernote/android/job/JobRequest;Landroid/os/Bundle;)Lcom/evernote/android/job/Job$Result;

    move-result-object v1

    .line 55
    sget-object v2, Lcom/evernote/android/job/Job$Result;->SUCCESS:Lcom/evernote/android/job/Job$Result;

    if-ne v2, v1, :cond_3

    .line 56
    invoke-static {}, Landroidx/work/ListenableWorker$Result;->success()Landroidx/work/ListenableWorker$Result;

    move-result-object v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 61
    invoke-static {v0}, Lcom/evernote/android/job/work/TransientBundleHolder;->cleanUpBundle(I)V

    return-object v1

    .line 58
    :cond_3
    :try_start_3
    invoke-static {}, Landroidx/work/ListenableWorker$Result;->failure()Landroidx/work/ListenableWorker$Result;

    move-result-object v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 61
    invoke-static {v0}, Lcom/evernote/android/job/work/TransientBundleHolder;->cleanUpBundle(I)V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/evernote/android/job/work/TransientBundleHolder;->cleanUpBundle(I)V

    .line 62
    throw v1
.end method

.method public onStopped()V
    .locals 4

    .line 67
    invoke-direct {p0}, Lcom/evernote/android/job/work/PlatformWorker;->getJobId()I

    move-result v0

    .line 68
    invoke-virtual {p0}, Lcom/evernote/android/job/work/PlatformWorker;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/evernote/android/job/JobManager;->create(Landroid/content/Context;)Lcom/evernote/android/job/JobManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/evernote/android/job/JobManager;->getJob(I)Lcom/evernote/android/job/Job;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    .line 71
    invoke-virtual {v1}, Lcom/evernote/android/job/Job;->cancel()V

    .line 72
    sget-object v0, Lcom/evernote/android/job/work/PlatformWorker;->CAT:Lcom/evernote/android/job/util/JobCat;

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v2

    const-string v1, "Called onStopped for %s"

    invoke-virtual {v0, v1, v3}, Lcom/evernote/android/job/util/JobCat;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 74
    :cond_0
    sget-object v1, Lcom/evernote/android/job/work/PlatformWorker;->CAT:Lcom/evernote/android/job/util/JobCat;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v2

    const-string v0, "Called onStopped, job %d not found"

    invoke-virtual {v1, v0, v3}, Lcom/evernote/android/job/util/JobCat;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
