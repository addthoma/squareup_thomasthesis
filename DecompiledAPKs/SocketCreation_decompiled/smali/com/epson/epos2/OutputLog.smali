.class Lcom/epson/epos2/OutputLog;
.super Ljava/lang/Object;
.source "OutputLog.java"


# static fields
.field private static final LOGIF_FUNC_CB_EVENT:I = 0x3

.field private static final LOGIF_FUNC_IN:I = 0x0

.field private static final LOGIF_FUNC_OP_INFO:I = 0x4

.field private static final LOGIF_FUNC_OUT_WITHOUT_RET:I = 0x2

.field private static final LOGIF_FUNC_OUT_WITH_RET:I = 0x1

.field private static final LOGIF_OP_STR_BT_PAIRED_NG:Ljava/lang/String; = "Bluetooth isn\'t paired."

.field private static final LOGIF_OP_STR_BT_PAIRED_OK:Ljava/lang/String; = "Bluetooth is paired."

.field private static final LOGIF_RETURN_NONE:I


# direct methods
.method constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getFreeMemorySize(Ljava/io/File;)J
    .locals 2

    .line 172
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0
.end method

.method private static getTotalMemorySize(Ljava/io/File;)J
    .locals 2

    .line 160
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0
.end method

.method protected static native nativeLogOutput(IJLjava/lang/String;ILjava/lang/String;)V
.end method

.method protected static native nativeReadLogSettings(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public static outputException(Ljava/lang/String;JLjava/lang/Exception;)V
    .locals 1

    const/4 v0, 0x2

    .line 43
    invoke-static {v0, p1, p2, p0, p3}, Lcom/epson/epos2/OutputLog;->processOutputExceptionLog(IJLjava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public static varargs outputLogCallFunction(Ljava/lang/String;J[Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x0

    .line 35
    invoke-static {v0, p1, p2, p0, p3}, Lcom/epson/epos2/OutputLog;->processOutputLogData(IJLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static varargs outputLogEvent(Ljava/lang/String;J[Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x3

    .line 47
    invoke-static {v0, p1, p2, p0, p3}, Lcom/epson/epos2/OutputLog;->processOutputLogData(IJLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static varargs outputLogInfo(Ljava/lang/String;J[Ljava/lang/Object;)V
    .locals 1

    const/4 v0, 0x4

    .line 52
    invoke-static {v0, p1, p2, p0, p3}, Lcom/epson/epos2/OutputLog;->processOutputLogData(IJLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public static varargs outputLogReturnFunction(Ljava/lang/String;JI[Ljava/lang/Object;)V
    .locals 6

    const/4 v0, 0x1

    move-wide v1, p1

    move-object v3, p0

    move v4, p3

    move-object v5, p4

    .line 39
    invoke-static/range {v0 .. v5}, Lcom/epson/epos2/OutputLog;->processOutputLogDataWithResult(IJLjava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private static processOutputExceptionLog(IJLjava/lang/String;Ljava/lang/Exception;)V
    .locals 8

    .line 146
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 147
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 149
    invoke-virtual {p4, v1}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 151
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V

    .line 152
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v6, 0x0

    move v2, p0

    move-wide v3, p1

    move-object v5, p3

    invoke-static/range {v2 .. v7}, Lcom/epson/epos2/OutputLog;->nativeLogOutput(IJLjava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method private static processOutputLogData(IJLjava/lang/String;[Ljava/lang/Object;)V
    .locals 7

    const-string v0, "("

    if-eqz p4, :cond_2

    const/4 v1, 0x0

    const-string v2, ""

    .line 93
    :goto_0
    array-length v3, p4

    if-ge v1, v3, :cond_2

    .line 94
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 95
    aget-object v2, p4, v1

    if-nez v2, :cond_0

    .line 96
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 98
    :cond_0
    aget-object v2, p4, v1

    instance-of v2, v2, Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 99
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p4, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 106
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p4, v1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    add-int/lit8 v1, v1, 0x1

    const-string v2, ", "

    goto :goto_0

    .line 111
    :cond_2
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v5, 0x0

    move v1, p0

    move-wide v2, p1

    move-object v4, p3

    .line 113
    invoke-static/range {v1 .. v6}, Lcom/epson/epos2/OutputLog;->nativeLogOutput(IJLjava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method private static processOutputLogDataWithResult(IJLjava/lang/String;I[Ljava/lang/Object;)V
    .locals 7

    const-string v0, "("

    if-eqz p5, :cond_2

    const/4 v1, 0x0

    const-string v2, ""

    .line 122
    :goto_0
    array-length v3, p5

    if-ge v1, v3, :cond_2

    .line 123
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 124
    aget-object v2, p5, v1

    if-nez v2, :cond_0

    .line 125
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "null"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 127
    :cond_0
    aget-object v2, p5, v1

    instance-of v2, v2, Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 128
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p5, v1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 135
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p5, v1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    add-int/lit8 v1, v1, 0x1

    const-string v2, ", "

    goto :goto_0

    .line 140
    :cond_2
    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move v1, p0

    move-wide v2, p1

    move-object v4, p3

    move v5, p4

    .line 142
    invoke-static/range {v1 .. v6}, Lcom/epson/epos2/OutputLog;->nativeLogOutput(IJLjava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static readLogSettings(Landroid/content/Context;)V
    .locals 11

    const-string v0, ""

    if-eqz p0, :cond_0

    const/4 v1, 0x0

    .line 60
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object p0

    invoke-virtual {p0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    move-object v1, v0

    const-string v4, "Android"

    .line 68
    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 69
    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 71
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object p0

    invoke-static {p0}, Lcom/epson/epos2/OutputLog;->getTotalMemorySize(Ljava/io/File;)J

    move-result-wide v2

    .line 72
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object p0

    invoke-static {p0}, Lcom/epson/epos2/OutputLog;->getFreeMemorySize(Ljava/io/File;)J

    move-result-wide v7

    .line 73
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object p0

    invoke-static {p0}, Lcom/epson/epos2/OutputLog;->getFreeMemorySize(Ljava/io/File;)J

    move-result-wide v9

    const/4 p0, 0x2

    new-array p0, p0, [Ljava/lang/Object;

    .line 75
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/4 v7, 0x0

    aput-object v0, p0, v7

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/4 v2, 0x1

    aput-object v0, p0, v2

    const-string v0, "%d/%d"

    invoke-static {v0, p0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    new-array v0, v2, [Ljava/lang/Object;

    .line 76
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v7

    const-string v2, "%d"

    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    :try_start_1
    const-string v2, "NOT_USE"

    const-string v3, "NOT_USE"

    move-object v7, p0

    .line 79
    invoke-static/range {v1 .. v8}, Lcom/epson/epos2/OutputLog;->nativeReadLogSettings(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method
