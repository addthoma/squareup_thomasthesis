.class public Lcom/epson/epos2/msr/Data;
.super Ljava/lang/Object;
.source "Data.java"


# instance fields
.field private accountNumber:Ljava/lang/String;

.field private expirationData:Ljava/lang/String;

.field private firstName:Ljava/lang/String;

.field private middleInitial:Ljava/lang/String;

.field private serviceCode:Ljava/lang/String;

.field private surname:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private track1:Ljava/lang/String;

.field private track1_dd:Ljava/lang/String;

.field private track2:Ljava/lang/String;

.field private track2_dd:Ljava/lang/String;

.field private track4:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setAccountNumber(Ljava/lang/String;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/epson/epos2/msr/Data;->accountNumber:Ljava/lang/String;

    return-void
.end method

.method private setExpirationData(Ljava/lang/String;)V
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/epson/epos2/msr/Data;->expirationData:Ljava/lang/String;

    return-void
.end method

.method private setFirstName(Ljava/lang/String;)V
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/epson/epos2/msr/Data;->firstName:Ljava/lang/String;

    return-void
.end method

.method private setMiddleInitial(Ljava/lang/String;)V
    .locals 0

    .line 93
    iput-object p1, p0, Lcom/epson/epos2/msr/Data;->middleInitial:Ljava/lang/String;

    return-void
.end method

.method private setServiceCode(Ljava/lang/String;)V
    .locals 0

    .line 99
    iput-object p1, p0, Lcom/epson/epos2/msr/Data;->serviceCode:Ljava/lang/String;

    return-void
.end method

.method private setSurname(Ljava/lang/String;)V
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/epson/epos2/msr/Data;->surname:Ljava/lang/String;

    return-void
.end method

.method private setTitle(Ljava/lang/String;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/epson/epos2/msr/Data;->title:Ljava/lang/String;

    return-void
.end method

.method private setTrack1(Ljava/lang/String;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/epson/epos2/msr/Data;->track1:Ljava/lang/String;

    return-void
.end method

.method private setTrack1_dd(Ljava/lang/String;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/epson/epos2/msr/Data;->track1_dd:Ljava/lang/String;

    return-void
.end method

.method private setTrack2(Ljava/lang/String;)V
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/epson/epos2/msr/Data;->track2:Ljava/lang/String;

    return-void
.end method

.method private setTrack2_dd(Ljava/lang/String;)V
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/epson/epos2/msr/Data;->track2_dd:Ljava/lang/String;

    return-void
.end method

.method private setTrack4(Ljava/lang/String;)V
    .locals 0

    .line 78
    iput-object p1, p0, Lcom/epson/epos2/msr/Data;->track4:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAccountNumber()Ljava/lang/String;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/epson/epos2/msr/Data;->accountNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getExpirationData()Ljava/lang/String;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/epson/epos2/msr/Data;->expirationData:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/epson/epos2/msr/Data;->firstName:Ljava/lang/String;

    return-object v0
.end method

.method public getMiddleInitial()Ljava/lang/String;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/epson/epos2/msr/Data;->middleInitial:Ljava/lang/String;

    return-object v0
.end method

.method public getServiceCode()Ljava/lang/String;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/epson/epos2/msr/Data;->serviceCode:Ljava/lang/String;

    return-object v0
.end method

.method public getSurname()Ljava/lang/String;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/epson/epos2/msr/Data;->surname:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/epson/epos2/msr/Data;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getTrack1()Ljava/lang/String;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/epson/epos2/msr/Data;->track1:Ljava/lang/String;

    return-object v0
.end method

.method public getTrack1_dd()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/epson/epos2/msr/Data;->track1_dd:Ljava/lang/String;

    return-object v0
.end method

.method public getTrack2()Ljava/lang/String;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/epson/epos2/msr/Data;->track2:Ljava/lang/String;

    return-object v0
.end method

.method public getTrack2_dd()Ljava/lang/String;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/epson/epos2/msr/Data;->track2_dd:Ljava/lang/String;

    return-object v0
.end method

.method public getTrack4()Ljava/lang/String;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/epson/epos2/msr/Data;->track4:Ljava/lang/String;

    return-object v0
.end method
