.class public Lcom/epson/eposprint/Print;
.super Ljava/lang/Object;
.source "Print.java"


# static fields
.field public static final DEVTYPE_BLUETOOTH:I = 0x1

.field public static final DEVTYPE_TCP:I = 0x0

.field public static final DEVTYPE_USB:I = 0x2

.field public static final FALSE:I = 0x0

.field private static final ON_BATTERY_LOW:I = 0xb

.field private static final ON_BATTERY_OK:I = 0xc

.field private static final ON_BATTERY_STATUS_CHANGE:I = 0xd

.field private static final ON_COVER_OK:I = 0x4

.field private static final ON_COVER_OPEN:I = 0x5

.field private static final ON_DRAWER_CLOSED:I = 0x9

.field private static final ON_DRAWER_OPEN:I = 0xa

.field private static final ON_OFFLINE:I = 0x2

.field private static final ON_ONLINE:I = 0x1

.field private static final ON_PAPER_END:I = 0x8

.field private static final ON_PAPER_NEAR_END:I = 0x7

.field private static final ON_PAPER_OK:I = 0x6

.field private static final ON_POWER_OFF:I = 0x3

.field private static final ON_STATUS_CHANGE:I = 0x0

.field public static final PARAM_DEFAULT:I = -0x2

.field public static final PARAM_UNSPECIFIED:I = -0x1

.field public static final ST_AUTOCUTTER_ERR:I = 0x800

.field public static final ST_AUTORECOVER_ERR:I = 0x4000

.field public static final ST_BATTERY_OFFLINE:I = 0x4

.field public static final ST_BATTERY_OVERHEAT:I = 0x40000000

.field public static final ST_BUZZER:I = 0x1000000

.field public static final ST_COVER_OPEN:I = 0x20

.field public static final ST_DRAWER_KICK:I = 0x4

.field public static final ST_HEAD_OVERHEAT:I = 0x10000000

.field public static final ST_MECHANICAL_ERR:I = 0x400

.field public static final ST_MOTOR_OVERHEAT:I = 0x20000000

.field public static final ST_NO_RESPONSE:I = 0x1

.field public static final ST_OFF_LINE:I = 0x8

.field public static final ST_PANEL_SWITCH:I = 0x200

.field public static final ST_PAPER_FEED:I = 0x40

.field public static final ST_PRINT_SUCCESS:I = 0x2

.field public static final ST_RECEIPT_END:I = 0x80000

.field public static final ST_RECEIPT_NEAR_END:I = 0x20000

.field public static final ST_UNRECOVER_ERR:I = 0x2000

.field public static final ST_WAIT_ON_LINE:I = 0x100

.field public static final ST_WRONG_PAPER:I = 0x1000

.field public static final TRUE:I = 0x1


# instance fields
.field private mBatteryLowEventListener:Lcom/epson/eposprint/BatteryLowEventListener;

.field private mBatteryOkEventListener:Lcom/epson/eposprint/BatteryOkEventListener;

.field private mBatteryStatusChangeEventListener:Lcom/epson/eposprint/BatteryStatusChangeEventListener;

.field private mContext:Landroid/content/Context;

.field private mCoverOkEventListener:Lcom/epson/eposprint/CoverOkEventListener;

.field private mCoverOpenEventListener:Lcom/epson/eposprint/CoverOpenEventListener;

.field private mDrawerClosedEventListener:Lcom/epson/eposprint/DrawerClosedEventListener;

.field private mDrawerOpenEventListener:Lcom/epson/eposprint/DrawerOpenEventListener;

.field private mOfflineEventListener:Lcom/epson/eposprint/OfflineEventListener;

.field private mOnlineEventListener:Lcom/epson/eposprint/OnlineEventListener;

.field private mPaperEndEventListener:Lcom/epson/eposprint/PaperEndEventListener;

.field private mPaperNearEndEventListener:Lcom/epson/eposprint/PaperNearEndEventListener;

.field private mPaperOkEventListener:Lcom/epson/eposprint/PaperOkEventListener;

.field private mPowerOffEventListener:Lcom/epson/eposprint/PowerOffEventListener;

.field private mPrinterHandle:J

.field private mStatusChangeEventListener:Lcom/epson/eposprint/StatusChangeEventListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epos2"

    .line 20
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 29
    iput-wide v0, p0, Lcom/epson/eposprint/Print;->mPrinterHandle:J

    const/4 v2, 0x0

    .line 31
    iput-object v2, p0, Lcom/epson/eposprint/Print;->mContext:Landroid/content/Context;

    .line 320
    iput-wide v0, p0, Lcom/epson/eposprint/Print;->mPrinterHandle:J

    .line 322
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v1, "sdk"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v1, "google_sdk"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v1, "sdk_phone_armv7"

    .line 323
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v1, "sdk_google_phone_armv7"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 325
    invoke-direct {p0}, Lcom/epson/eposprint/Print;->eposSetAVDInfo()V

    :cond_2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 13

    .line 331
    invoke-direct {p0}, Lcom/epson/eposprint/Print;-><init>()V

    const-string v0, ""

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    .line 336
    :try_start_0
    invoke-virtual {p1, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    :cond_0
    :goto_0
    const-string v3, "ePOS-Print SDK for Android"

    const-string v4, "1.9.0"

    const-string v5, "Android"

    .line 346
    sget-object v6, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 347
    sget-object v7, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 349
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Lcom/epson/eposprint/Print;->getTotalMemorySize(Ljava/io/File;)J

    move-result-wide v1

    .line 351
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v8

    invoke-static {v8}, Lcom/epson/eposprint/Print;->getFreeMemorySize(Ljava/io/File;)J

    move-result-wide v8

    .line 352
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v10

    invoke-static {v10}, Lcom/epson/eposprint/Print;->getFreeMemorySize(Ljava/io/File;)J

    move-result-wide v10

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    .line 353
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    const/4 v9, 0x0

    aput-object v8, v12, v9

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v12, v2

    const-string v1, "%d/%d"

    invoke-static {v1, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-array v1, v2, [Ljava/lang/Object;

    .line 354
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v1, v9

    const-string v10, "%d"

    invoke-static {v10, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 356
    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v11, "sdk"

    invoke-static {v1, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v11, "google_sdk"

    invoke-static {v1, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v11, "sdk_phone_armv7"

    .line 357
    invoke-static {v1, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v11, "sdk_google_phone_armv7"

    invoke-static {v1, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v11, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v11, 0x1

    :goto_2
    move-object v1, p0

    move-object v2, v0

    move-object v9, v10

    .line 361
    :try_start_1
    invoke-direct/range {v1 .. v9}, Lcom/epson/eposprint/Print;->eposReadLogSettings(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    nop

    .line 367
    :goto_3
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mContext:Landroid/content/Context;

    if-eqz v11, :cond_3

    .line 372
    invoke-direct {p0}, Lcom/epson/eposprint/Print;->eposSetAVDInfo()V

    .line 378
    :cond_3
    :try_start_2
    invoke-direct {p0, v0}, Lcom/epson/eposprint/Print;->eposSetDBFilePath(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    return-void
.end method

.method private native eposBeginTransaction(J)I
.end method

.method private native eposClosePrinter(J)I
.end method

.method private native eposEndTransaction(J)I
.end method

.method private native eposEventLog(Ljava/lang/String;Ljava/lang/String;I)I
.end method

.method private native eposEventLog(Ljava/lang/String;Ljava/lang/String;III)I
.end method

.method private native eposGetStatus(J[I[I)I
.end method

.method private native eposOpenPrinter([JILjava/lang/String;IILjava/lang/Object;IZ)I
.end method

.method private native eposReadLogSettings(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private native eposSendData(JJI[I[I)I
.end method

.method private native eposSetAVDInfo()V
.end method

.method private native eposSetDBFilePath(Ljava/lang/String;)I
.end method

.method private native eposSetEventCallback(J)V
.end method

.method private static getFreeMemorySize(Ljava/io/File;)J
    .locals 2

    .line 641
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0
.end method

.method private static getTotalMemorySize(Ljava/io/File;)J
    .locals 2

    .line 629
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0
.end method

.method private onBatteryLow(Ljava/lang/String;)V
    .locals 2

    .line 283
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mBatteryLowEventListener:Lcom/epson/eposprint/BatteryLowEventListener;

    if-eqz v0, :cond_0

    .line 284
    invoke-interface {v0, p1}, Lcom/epson/eposprint/BatteryLowEventListener;->onBatteryLowEvent(Ljava/lang/String;)V

    .line 286
    :try_start_0
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mBatteryLowEventListener:Lcom/epson/eposprint/BatteryLowEventListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xb

    invoke-direct {p0, p1, v0, v1}, Lcom/epson/eposprint/Print;->eposEventLog(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private onBatteryOk(Ljava/lang/String;)V
    .locals 2

    .line 295
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mBatteryOkEventListener:Lcom/epson/eposprint/BatteryOkEventListener;

    if-eqz v0, :cond_0

    .line 296
    invoke-interface {v0, p1}, Lcom/epson/eposprint/BatteryOkEventListener;->onBatteryOkEvent(Ljava/lang/String;)V

    .line 298
    :try_start_0
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mBatteryOkEventListener:Lcom/epson/eposprint/BatteryOkEventListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xc

    invoke-direct {p0, p1, v0, v1}, Lcom/epson/eposprint/Print;->eposEventLog(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private onBatteryStatusChange(Ljava/lang/String;I)V
    .locals 7

    .line 307
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mBatteryStatusChangeEventListener:Lcom/epson/eposprint/BatteryStatusChangeEventListener;

    if-eqz v0, :cond_0

    .line 308
    invoke-interface {v0, p1, p2}, Lcom/epson/eposprint/BatteryStatusChangeEventListener;->onBatteryStatusChangeEvent(Ljava/lang/String;I)V

    .line 310
    :try_start_0
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mBatteryStatusChangeEventListener:Lcom/epson/eposprint/BatteryStatusChangeEventListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0xd

    const/4 v5, 0x1

    move-object v1, p0

    move-object v2, p1

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/epson/eposprint/Print;->eposEventLog(Ljava/lang/String;Ljava/lang/String;III)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private onCoverOk(Ljava/lang/String;)V
    .locals 2

    .line 199
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mCoverOkEventListener:Lcom/epson/eposprint/CoverOkEventListener;

    if-eqz v0, :cond_0

    .line 200
    invoke-interface {v0, p1}, Lcom/epson/eposprint/CoverOkEventListener;->onCoverOkEvent(Ljava/lang/String;)V

    .line 202
    :try_start_0
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mCoverOkEventListener:Lcom/epson/eposprint/CoverOkEventListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-direct {p0, p1, v0, v1}, Lcom/epson/eposprint/Print;->eposEventLog(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private onCoverOpen(Ljava/lang/String;)V
    .locals 2

    .line 211
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mCoverOpenEventListener:Lcom/epson/eposprint/CoverOpenEventListener;

    if-eqz v0, :cond_0

    .line 212
    invoke-interface {v0, p1}, Lcom/epson/eposprint/CoverOpenEventListener;->onCoverOpenEvent(Ljava/lang/String;)V

    .line 214
    :try_start_0
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mCoverOpenEventListener:Lcom/epson/eposprint/CoverOpenEventListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x5

    invoke-direct {p0, p1, v0, v1}, Lcom/epson/eposprint/Print;->eposEventLog(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private onDrawerClosed(Ljava/lang/String;)V
    .locals 2

    .line 259
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mDrawerClosedEventListener:Lcom/epson/eposprint/DrawerClosedEventListener;

    if-eqz v0, :cond_0

    .line 260
    invoke-interface {v0, p1}, Lcom/epson/eposprint/DrawerClosedEventListener;->onDrawerClosedEvent(Ljava/lang/String;)V

    .line 262
    :try_start_0
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mDrawerClosedEventListener:Lcom/epson/eposprint/DrawerClosedEventListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x9

    invoke-direct {p0, p1, v0, v1}, Lcom/epson/eposprint/Print;->eposEventLog(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private onDrawerOpen(Ljava/lang/String;)V
    .locals 2

    .line 271
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mDrawerOpenEventListener:Lcom/epson/eposprint/DrawerOpenEventListener;

    if-eqz v0, :cond_0

    .line 272
    invoke-interface {v0, p1}, Lcom/epson/eposprint/DrawerOpenEventListener;->onDrawerOpenEvent(Ljava/lang/String;)V

    .line 274
    :try_start_0
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mDrawerOpenEventListener:Lcom/epson/eposprint/DrawerOpenEventListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xa

    invoke-direct {p0, p1, v0, v1}, Lcom/epson/eposprint/Print;->eposEventLog(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private onOffline(Ljava/lang/String;)V
    .locals 2

    .line 175
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mOfflineEventListener:Lcom/epson/eposprint/OfflineEventListener;

    if-eqz v0, :cond_0

    .line 176
    invoke-interface {v0, p1}, Lcom/epson/eposprint/OfflineEventListener;->onOfflineEvent(Ljava/lang/String;)V

    .line 178
    :try_start_0
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mOfflineEventListener:Lcom/epson/eposprint/OfflineEventListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1}, Lcom/epson/eposprint/Print;->eposEventLog(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private onOnline(Ljava/lang/String;)V
    .locals 2

    .line 163
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mOnlineEventListener:Lcom/epson/eposprint/OnlineEventListener;

    if-eqz v0, :cond_0

    .line 164
    invoke-interface {v0, p1}, Lcom/epson/eposprint/OnlineEventListener;->onOnlineEvent(Ljava/lang/String;)V

    .line 166
    :try_start_0
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mOnlineEventListener:Lcom/epson/eposprint/OnlineEventListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/epson/eposprint/Print;->eposEventLog(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private onPaperEnd(Ljava/lang/String;)V
    .locals 2

    .line 247
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mPaperEndEventListener:Lcom/epson/eposprint/PaperEndEventListener;

    if-eqz v0, :cond_0

    .line 248
    invoke-interface {v0, p1}, Lcom/epson/eposprint/PaperEndEventListener;->onPaperEndEvent(Ljava/lang/String;)V

    .line 250
    :try_start_0
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mPaperEndEventListener:Lcom/epson/eposprint/PaperEndEventListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x8

    invoke-direct {p0, p1, v0, v1}, Lcom/epson/eposprint/Print;->eposEventLog(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private onPaperNearEnd(Ljava/lang/String;)V
    .locals 2

    .line 235
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mPaperNearEndEventListener:Lcom/epson/eposprint/PaperNearEndEventListener;

    if-eqz v0, :cond_0

    .line 236
    invoke-interface {v0, p1}, Lcom/epson/eposprint/PaperNearEndEventListener;->onPaperNearEndEvent(Ljava/lang/String;)V

    .line 238
    :try_start_0
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mPaperNearEndEventListener:Lcom/epson/eposprint/PaperNearEndEventListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x7

    invoke-direct {p0, p1, v0, v1}, Lcom/epson/eposprint/Print;->eposEventLog(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private onPaperOk(Ljava/lang/String;)V
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mPaperOkEventListener:Lcom/epson/eposprint/PaperOkEventListener;

    if-eqz v0, :cond_0

    .line 224
    invoke-interface {v0, p1}, Lcom/epson/eposprint/PaperOkEventListener;->onPaperOkEvent(Ljava/lang/String;)V

    .line 226
    :try_start_0
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mPaperOkEventListener:Lcom/epson/eposprint/PaperOkEventListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-direct {p0, p1, v0, v1}, Lcom/epson/eposprint/Print;->eposEventLog(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private onPowerOff(Ljava/lang/String;)V
    .locals 2

    .line 187
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mPowerOffEventListener:Lcom/epson/eposprint/PowerOffEventListener;

    if-eqz v0, :cond_0

    .line 188
    invoke-interface {v0, p1}, Lcom/epson/eposprint/PowerOffEventListener;->onPowerOffEvent(Ljava/lang/String;)V

    .line 190
    :try_start_0
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mPowerOffEventListener:Lcom/epson/eposprint/PowerOffEventListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-direct {p0, p1, v0, v1}, Lcom/epson/eposprint/Print;->eposEventLog(Ljava/lang/String;Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method private onStatusChange(Ljava/lang/String;I)V
    .locals 7

    .line 151
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mStatusChangeEventListener:Lcom/epson/eposprint/StatusChangeEventListener;

    if-eqz v0, :cond_0

    .line 152
    invoke-interface {v0, p1, p2}, Lcom/epson/eposprint/StatusChangeEventListener;->onStatusChangeEvent(Ljava/lang/String;I)V

    .line 154
    :try_start_0
    iget-object v0, p0, Lcom/epson/eposprint/Print;->mStatusChangeEventListener:Lcom/epson/eposprint/StatusChangeEventListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    move-object v2, p1

    move v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/epson/eposprint/Print;->eposEventLog(Ljava/lang/String;Ljava/lang/String;III)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method


# virtual methods
.method public beginTransaction()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 477
    iget-wide v0, p0, Lcom/epson/eposprint/Print;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v2, v0

    if-eqz v4, :cond_1

    .line 481
    invoke-direct {p0, v0, v1}, Lcom/epson/eposprint/Print;->eposBeginTransaction(J)I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 483
    :cond_0
    new-instance v1, Lcom/epson/eposprint/EposException;

    invoke-direct {v1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v1

    .line 478
    :cond_1
    new-instance v0, Lcom/epson/eposprint/EposException;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method public closePrinter()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 464
    iget-wide v0, p0, Lcom/epson/eposprint/Print;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v2, v0

    if-eqz v4, :cond_1

    .line 468
    invoke-direct {p0, v0, v1}, Lcom/epson/eposprint/Print;->eposClosePrinter(J)I

    move-result v0

    if-nez v0, :cond_0

    .line 472
    iput-wide v2, p0, Lcom/epson/eposprint/Print;->mPrinterHandle:J

    return-void

    .line 470
    :cond_0
    new-instance v1, Lcom/epson/eposprint/EposException;

    invoke-direct {v1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v1

    .line 465
    :cond_1
    new-instance v0, Lcom/epson/eposprint/EposException;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method public endTransaction()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 488
    iget-wide v0, p0, Lcom/epson/eposprint/Print;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v2, v0

    if-eqz v4, :cond_1

    .line 492
    invoke-direct {p0, v0, v1}, Lcom/epson/eposprint/Print;->eposEndTransaction(J)I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 494
    :cond_0
    new-instance v1, Lcom/epson/eposprint/EposException;

    invoke-direct {v1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v1

    .line 489
    :cond_1
    new-instance v0, Lcom/epson/eposprint/EposException;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method protected finalize()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const-wide/16 v0, 0x0

    .line 389
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 392
    iget-wide v2, p0, Lcom/epson/eposprint/Print;->mPrinterHandle:J

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 393
    invoke-direct {p0, v2, v3}, Lcom/epson/eposprint/Print;->eposClosePrinter(J)I

    .line 394
    iput-wide v0, p0, Lcom/epson/eposprint/Print;->mPrinterHandle:J

    :cond_0
    return-void

    :catchall_0
    move-exception v2

    .line 392
    iget-wide v3, p0, Lcom/epson/eposprint/Print;->mPrinterHandle:J

    cmp-long v5, v0, v3

    if-eqz v5, :cond_1

    .line 393
    invoke-direct {p0, v3, v4}, Lcom/epson/eposprint/Print;->eposClosePrinter(J)I

    .line 394
    iput-wide v0, p0, Lcom/epson/eposprint/Print;->mPrinterHandle:J

    .line 396
    :cond_1
    throw v2
.end method

.method public getStatus([I[I)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    if-eqz p1, :cond_3

    .line 536
    array-length v1, p1

    if-eqz v1, :cond_3

    if-eqz p2, :cond_2

    .line 540
    array-length v1, p2

    if-eqz v1, :cond_2

    const-wide/16 v0, 0x0

    .line 544
    iget-wide v2, p0, Lcom/epson/eposprint/Print;->mPrinterHandle:J

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    .line 548
    invoke-direct {p0, v2, v3, p1, p2}, Lcom/epson/eposprint/Print;->eposGetStatus(J[I[I)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 550
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2

    .line 545
    :cond_1
    new-instance p1, Lcom/epson/eposprint/EposException;

    const/4 p2, 0x6

    invoke-direct {p1, p2}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p1

    .line 541
    :cond_2
    new-instance p1, Lcom/epson/eposprint/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p1

    .line 537
    :cond_3
    new-instance p1, Lcom/epson/eposprint/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p1
.end method

.method public openPrinter(ILjava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    const/4 v0, -0x2

    .line 402
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/epson/eposprint/Print;->openPrinter(ILjava/lang/String;II)V

    return-void
.end method

.method public openPrinter(ILjava/lang/String;II)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    const/4 v5, -0x2

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    .line 409
    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/epson/eposprint/Print;->openPrinter(ILjava/lang/String;III)V
    :try_end_0
    .catch Lcom/epson/eposprint/EposException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    .line 412
    invoke-virtual {p1}, Lcom/epson/eposprint/EposException;->getErrorStatus()I

    move-result p2

    const/4 p3, 0x4

    if-ne p2, p3, :cond_0

    .line 413
    new-instance p1, Lcom/epson/eposprint/EposException;

    const/4 p2, 0x2

    invoke-direct {p1, p2}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p1

    .line 415
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-virtual {p1}, Lcom/epson/eposprint/EposException;->getErrorStatus()I

    move-result p1

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2
.end method

.method public openPrinter(ILjava/lang/String;III)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    move-object v9, p0

    const/4 v0, 0x1

    if-eqz p2, :cond_5

    .line 427
    iget-wide v1, v9, Lcom/epson/eposprint/Print;->mPrinterHandle:J

    const/4 v3, 0x6

    const-wide/16 v4, 0x0

    cmp-long v6, v4, v1

    if-nez v6, :cond_4

    const/4 v1, 0x2

    move v2, p1

    if-ne v1, v2, :cond_2

    .line 433
    invoke-static {}, Lcom/epson/epsonio/SupportUsb;->isSupport()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 436
    iget-object v1, v9, Lcom/epson/eposprint/Print;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 437
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, v3}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0

    .line 434
    :cond_1
    new-instance v1, Lcom/epson/eposprint/EposException;

    invoke-direct {v1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v1

    :cond_2
    :goto_0
    new-array v10, v0, [J

    const/4 v11, 0x0

    aput-wide v4, v10, v11

    .line 450
    iget-object v6, v9, Lcom/epson/eposprint/Print;->mContext:Landroid/content/Context;

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, v10

    move v2, p1

    move-object v3, p2

    move v4, p3

    move/from16 v5, p4

    move/from16 v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/epson/eposprint/Print;->eposOpenPrinter([JILjava/lang/String;IILjava/lang/Object;IZ)I

    move-result v0

    if-nez v0, :cond_3

    .line 460
    aget-wide v0, v10, v11

    iput-wide v0, v9, Lcom/epson/eposprint/Print;->mPrinterHandle:J

    return-void

    .line 455
    :cond_3
    new-instance v1, Lcom/epson/eposprint/EposException;

    invoke-direct {v1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v1

    .line 428
    :cond_4
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, v3}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0

    .line 424
    :cond_5
    new-instance v1, Lcom/epson/eposprint/EposException;

    invoke-direct {v1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v1
.end method

.method public sendData(Lcom/epson/eposprint/Builder;I[I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput v1, v0, v1

    .line 506
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/epson/eposprint/Print;->sendData(Lcom/epson/eposprint/Builder;I[I[I)V

    return-void
.end method

.method public sendData(Lcom/epson/eposprint/Builder;I[I[I)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    if-eqz p1, :cond_4

    if-eqz p3, :cond_3

    .line 515
    array-length v1, p3

    if-eqz v1, :cond_3

    if-eqz p4, :cond_2

    .line 519
    array-length v1, p4

    if-eqz v1, :cond_2

    const-wide/16 v0, 0x0

    .line 523
    iget-wide v3, p0, Lcom/epson/eposprint/Print;->mPrinterHandle:J

    cmp-long v2, v0, v3

    if-eqz v2, :cond_1

    .line 527
    invoke-virtual {p1}, Lcom/epson/eposprint/Builder;->getHandle()J

    move-result-wide v5

    move-object v2, p0

    move v7, p2

    move-object v8, p3

    move-object v9, p4

    invoke-direct/range {v2 .. v9}, Lcom/epson/eposprint/Print;->eposSendData(JJI[I[I)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 530
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    const/4 p4, 0x0

    aget p3, p3, p4

    invoke-direct {p2, p1, p3}, Lcom/epson/eposprint/EposException;-><init>(II)V

    throw p2

    .line 524
    :cond_1
    new-instance p1, Lcom/epson/eposprint/EposException;

    const/4 p2, 0x6

    invoke-direct {p1, p2}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p1

    .line 520
    :cond_2
    new-instance p1, Lcom/epson/eposprint/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p1

    .line 516
    :cond_3
    new-instance p1, Lcom/epson/eposprint/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p1

    .line 512
    :cond_4
    new-instance p1, Lcom/epson/eposprint/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p1
.end method

.method public setBatteryLowEventCallback(Lcom/epson/eposprint/BatteryLowEventListener;)V
    .locals 0

    .line 611
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mBatteryLowEventListener:Lcom/epson/eposprint/BatteryLowEventListener;

    return-void
.end method

.method public setBatteryOkEventCallback(Lcom/epson/eposprint/BatteryOkEventListener;)V
    .locals 0

    .line 616
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mBatteryOkEventListener:Lcom/epson/eposprint/BatteryOkEventListener;

    return-void
.end method

.method public setBatteryStatusChangeEventCallback(Lcom/epson/eposprint/BatteryStatusChangeEventListener;)V
    .locals 0

    .line 621
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mBatteryStatusChangeEventListener:Lcom/epson/eposprint/BatteryStatusChangeEventListener;

    return-void
.end method

.method public setCoverOkEventCallback(Lcom/epson/eposprint/CoverOkEventListener;)V
    .locals 0

    .line 576
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mCoverOkEventListener:Lcom/epson/eposprint/CoverOkEventListener;

    return-void
.end method

.method public setCoverOpenEventCallback(Lcom/epson/eposprint/CoverOpenEventListener;)V
    .locals 0

    .line 581
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mCoverOpenEventListener:Lcom/epson/eposprint/CoverOpenEventListener;

    return-void
.end method

.method public setDrawerClosedEventCallback(Lcom/epson/eposprint/DrawerClosedEventListener;)V
    .locals 0

    .line 601
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mDrawerClosedEventListener:Lcom/epson/eposprint/DrawerClosedEventListener;

    return-void
.end method

.method public setDrawerOpenEventCallback(Lcom/epson/eposprint/DrawerOpenEventListener;)V
    .locals 0

    .line 606
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mDrawerOpenEventListener:Lcom/epson/eposprint/DrawerOpenEventListener;

    return-void
.end method

.method public setOfflineEventCallback(Lcom/epson/eposprint/OfflineEventListener;)V
    .locals 0

    .line 566
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mOfflineEventListener:Lcom/epson/eposprint/OfflineEventListener;

    return-void
.end method

.method public setOnlineEventCallback(Lcom/epson/eposprint/OnlineEventListener;)V
    .locals 0

    .line 561
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mOnlineEventListener:Lcom/epson/eposprint/OnlineEventListener;

    return-void
.end method

.method public setPaperEndEventCallback(Lcom/epson/eposprint/PaperEndEventListener;)V
    .locals 0

    .line 596
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mPaperEndEventListener:Lcom/epson/eposprint/PaperEndEventListener;

    return-void
.end method

.method public setPaperNearEndEventCallback(Lcom/epson/eposprint/PaperNearEndEventListener;)V
    .locals 0

    .line 591
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mPaperNearEndEventListener:Lcom/epson/eposprint/PaperNearEndEventListener;

    return-void
.end method

.method public setPaperOkEventCallback(Lcom/epson/eposprint/PaperOkEventListener;)V
    .locals 0

    .line 586
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mPaperOkEventListener:Lcom/epson/eposprint/PaperOkEventListener;

    return-void
.end method

.method public setPowerOffEventCallback(Lcom/epson/eposprint/PowerOffEventListener;)V
    .locals 0

    .line 571
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mPowerOffEventListener:Lcom/epson/eposprint/PowerOffEventListener;

    return-void
.end method

.method public setStatusChangeEventCallback(Lcom/epson/eposprint/StatusChangeEventListener;)V
    .locals 0

    .line 557
    iput-object p1, p0, Lcom/epson/eposprint/Print;->mStatusChangeEventListener:Lcom/epson/eposprint/StatusChangeEventListener;

    return-void
.end method
