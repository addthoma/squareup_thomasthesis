.class public Lcom/epson/epsonio/DeviceInfo;
.super Ljava/lang/Object;
.source "DeviceInfo.java"


# instance fields
.field private mDeviceName:Ljava/lang/String;

.field private mDeviceType:I

.field private mIpAddress:Ljava/lang/String;

.field private mMacAddress:Ljava/lang/String;

.field private mPrinterName:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 6
    iput v0, p0, Lcom/epson/epsonio/DeviceInfo;->mDeviceType:I

    const-string v0, ""

    .line 7
    iput-object v0, p0, Lcom/epson/epsonio/DeviceInfo;->mPrinterName:Ljava/lang/String;

    .line 8
    iput-object v0, p0, Lcom/epson/epsonio/DeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 9
    iput-object v0, p0, Lcom/epson/epsonio/DeviceInfo;->mIpAddress:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/epson/epsonio/DeviceInfo;->mMacAddress:Ljava/lang/String;

    .line 13
    iput p1, p0, Lcom/epson/epsonio/DeviceInfo;->mDeviceType:I

    .line 14
    iput-object p2, p0, Lcom/epson/epsonio/DeviceInfo;->mPrinterName:Ljava/lang/String;

    .line 15
    iput-object p3, p0, Lcom/epson/epsonio/DeviceInfo;->mDeviceName:Ljava/lang/String;

    .line 16
    iput-object p4, p0, Lcom/epson/epsonio/DeviceInfo;->mIpAddress:Ljava/lang/String;

    .line 17
    iput-object p5, p0, Lcom/epson/epsonio/DeviceInfo;->mMacAddress:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/epson/epsonio/DeviceInfo;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceType()I
    .locals 1

    .line 28
    iget v0, p0, Lcom/epson/epsonio/DeviceInfo;->mDeviceType:I

    return v0
.end method

.method public getIpAddress()Ljava/lang/String;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/epson/epsonio/DeviceInfo;->mIpAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getMacAddress()Ljava/lang/String;
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/epson/epsonio/DeviceInfo;->mMacAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getPrinterName()Ljava/lang/String;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/epson/epsonio/DeviceInfo;->mPrinterName:Ljava/lang/String;

    return-object v0
.end method
