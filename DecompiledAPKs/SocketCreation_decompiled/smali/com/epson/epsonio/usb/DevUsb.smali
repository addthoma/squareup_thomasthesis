.class public Lcom/epson/epsonio/usb/DevUsb;
.super Ljava/lang/Object;
.source "DevUsb.java"


# static fields
.field private static final ACTION_USB_PERMISSION:Ljava/lang/String; = "com.android.eposprint.USB_PERMISSION"

.field private static final DEFAULT_PRINTER_NAME:Ljava/lang/String; = "TM Printer"

.field private static final PID_PRINTER_CLASS_MAX:I = 0xeff

.field private static final PID_PRINTER_CLASS_MIN:I = 0xe00

.field private static final PID_VENDER_CLASS:I = 0x202

.field private static final VID_EPSON:I = 0x4b8

.field private static mConnectionType:I

.field private static mContext:Landroid/content/Context;

.field private static mDeviceInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/epson/epsonio/DeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static mDiscoverThread:Ljava/lang/Thread;

.field private static mDiscoverThreadRunning:Ljava/lang/Boolean;

.field private static mUsbDeviceList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Landroid/hardware/usb/UsbDevice;",
            ">;"
        }
    .end annotation
.end field

.field private static mUsbManager:Landroid/hardware/usb/UsbManager;

.field private static mUsbReceiver:Landroid/content/BroadcastReceiver;

.field private static final unregisterReceiverCallbackHandler:Landroid/os/Handler;

.field private static unregisterReceiverSynchronizer:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/epson/epsonio/usb/DevUsb;->mDeviceInfoList:Ljava/util/ArrayList;

    const/4 v0, 0x0

    .line 44
    sput-object v0, Lcom/epson/epsonio/usb/DevUsb;->mDiscoverThread:Ljava/lang/Thread;

    const/4 v1, 0x0

    .line 45
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lcom/epson/epsonio/usb/DevUsb;->mDiscoverThreadRunning:Ljava/lang/Boolean;

    .line 46
    sput-object v0, Lcom/epson/epsonio/usb/DevUsb;->mUsbReceiver:Landroid/content/BroadcastReceiver;

    .line 47
    sput-object v0, Lcom/epson/epsonio/usb/DevUsb;->mContext:Landroid/content/Context;

    .line 48
    sput v1, Lcom/epson/epsonio/usb/DevUsb;->mConnectionType:I

    .line 49
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/epson/epsonio/usb/DevUsb;->unregisterReceiverSynchronizer:Ljava/lang/Integer;

    .line 320
    new-instance v0, Lcom/epson/epsonio/usb/DevUsb$3;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/epson/epsonio/usb/DevUsb$3;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/epson/epsonio/usb/DevUsb;->unregisterReceiverCallbackHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static CreateDiscoverThread()Ljava/lang/Thread;
    .locals 2

    .line 242
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/epson/epsonio/usb/DevUsb$2;

    invoke-direct {v1}, Lcom/epson/epsonio/usb/DevUsb$2;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    return-object v0
.end method

.method static synthetic access$000()I
    .locals 1

    .line 26
    sget v0, Lcom/epson/epsonio/usb/DevUsb;->mConnectionType:I

    return v0
.end method

.method static synthetic access$100()Ljava/util/ArrayList;
    .locals 1

    .line 26
    sget-object v0, Lcom/epson/epsonio/usb/DevUsb;->mDeviceInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/Boolean;
    .locals 1

    .line 26
    sget-object v0, Lcom/epson/epsonio/usb/DevUsb;->mDiscoverThreadRunning:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$300()Ljava/util/HashMap;
    .locals 1

    .line 26
    sget-object v0, Lcom/epson/epsonio/usb/DevUsb;->mUsbDeviceList:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$302(Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0

    .line 26
    sput-object p0, Lcom/epson/epsonio/usb/DevUsb;->mUsbDeviceList:Ljava/util/HashMap;

    return-object p0
.end method

.method static synthetic access$400()Landroid/hardware/usb/UsbManager;
    .locals 1

    .line 26
    sget-object v0, Lcom/epson/epsonio/usb/DevUsb;->mUsbManager:Landroid/hardware/usb/UsbManager;

    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/Integer;
    .locals 1

    .line 26
    sget-object v0, Lcom/epson/epsonio/usb/DevUsb;->unregisterReceiverSynchronizer:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$600()Landroid/content/BroadcastReceiver;
    .locals 1

    .line 26
    sget-object v0, Lcom/epson/epsonio/usb/DevUsb;->mUsbReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$700()Landroid/content/Context;
    .locals 1

    .line 26
    sget-object v0, Lcom/epson/epsonio/usb/DevUsb;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static final getResult([II)[Lcom/epson/epsonio/DeviceInfo;
    .locals 5

    const/4 p1, 0x0

    if-eqz p0, :cond_2

    .line 169
    array-length v0, p0

    if-nez v0, :cond_0

    goto :goto_2

    :cond_0
    const/16 v0, 0xff

    const/4 v1, 0x0

    .line 173
    aput v0, p0, v1

    .line 175
    sget-object v0, Lcom/epson/epsonio/usb/DevUsb;->mDiscoverThreadRunning:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x6

    .line 176
    aput v0, p0, v1

    return-object p1

    .line 183
    :cond_1
    :try_start_0
    sget-object v0, Lcom/epson/epsonio/usb/DevUsb;->mDeviceInfoList:Ljava/util/ArrayList;

    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :try_start_1
    sget-object v2, Lcom/epson/epsonio/usb/DevUsb;->mDeviceInfoList:Ljava/util/ArrayList;

    sget-object v3, Lcom/epson/epsonio/usb/DevUsb;->mDeviceInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Lcom/epson/epsonio/DeviceInfo;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/epson/epsonio/DeviceInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 186
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 188
    :try_start_3
    aput v1, p0, v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catchall_0
    move-exception p1

    move-object v4, v2

    move-object v2, p1

    move-object p1, v4

    goto :goto_0

    :catchall_1
    move-exception v2

    .line 186
    :goto_0
    :try_start_4
    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v2
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    :catch_0
    move-object v2, p1

    :catch_1
    const/4 p1, 0x5

    .line 192
    aput p1, p0, v1

    :goto_1
    return-object v2

    :cond_2
    :goto_2
    return-object p1
.end method

.method public static final start(Landroid/content/Context;ILjava/lang/String;)I
    .locals 2

    const/4 v0, 0x1

    const/16 v1, 0x103

    if-eq v1, p1, :cond_0

    return v0

    :cond_0
    if-eqz p0, :cond_3

    if-eqz p2, :cond_1

    goto :goto_0

    .line 66
    :cond_1
    sget-object p1, Lcom/epson/epsonio/usb/DevUsb;->mDeviceInfoList:Ljava/util/ArrayList;

    monitor-enter p1

    .line 68
    :try_start_0
    sget-object p2, Lcom/epson/epsonio/usb/DevUsb;->mDeviceInfoList:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 69
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    sput-object p0, Lcom/epson/epsonio/usb/DevUsb;->mContext:Landroid/content/Context;

    .line 74
    sget-object p0, Lcom/epson/epsonio/usb/DevUsb;->mUsbManager:Landroid/hardware/usb/UsbManager;

    if-nez p0, :cond_2

    .line 75
    sget-object p0, Lcom/epson/epsonio/usb/DevUsb;->mContext:Landroid/content/Context;

    const-string p1, "usb"

    invoke-virtual {p0, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/hardware/usb/UsbManager;

    sput-object p0, Lcom/epson/epsonio/usb/DevUsb;->mUsbManager:Landroid/hardware/usb/UsbManager;

    .line 79
    :cond_2
    new-instance p0, Lcom/epson/epsonio/usb/DevUsb$1;

    invoke-direct {p0}, Lcom/epson/epsonio/usb/DevUsb$1;-><init>()V

    sput-object p0, Lcom/epson/epsonio/usb/DevUsb;->mUsbReceiver:Landroid/content/BroadcastReceiver;

    .line 140
    :try_start_1
    new-instance p0, Landroid/content/IntentFilter;

    const-string p1, "com.android.eposprint.USB_PERMISSION"

    invoke-direct {p0, p1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 141
    sget-object p1, Lcom/epson/epsonio/usb/DevUsb;->mContext:Landroid/content/Context;

    sget-object p2, Lcom/epson/epsonio/usb/DevUsb;->mUsbReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, p2, p0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 149
    :catch_0
    invoke-static {}, Lcom/epson/epsonio/usb/DevUsb;->CreateDiscoverThread()Ljava/lang/Thread;

    move-result-object p0

    sput-object p0, Lcom/epson/epsonio/usb/DevUsb;->mDiscoverThread:Ljava/lang/Thread;

    .line 150
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    sput-object p0, Lcom/epson/epsonio/usb/DevUsb;->mDiscoverThreadRunning:Ljava/lang/Boolean;

    .line 151
    sget-object p0, Lcom/epson/epsonio/usb/DevUsb;->mDiscoverThread:Ljava/lang/Thread;

    invoke-virtual {p0}, Ljava/lang/Thread;->start()V

    const/4 p0, 0x0

    return p0

    :catchall_0
    move-exception p0

    .line 69
    :try_start_2
    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p0

    :cond_3
    :goto_0
    return v0
.end method

.method public static final stop()I
    .locals 6

    .line 205
    sget-object v0, Lcom/epson/epsonio/usb/DevUsb;->mDiscoverThreadRunning:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x6

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 209
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/epson/epsonio/usb/DevUsb;->mDiscoverThreadRunning:Ljava/lang/Boolean;

    .line 212
    sget-object v1, Lcom/epson/epsonio/usb/DevUsb;->mUsbReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_2

    .line 213
    sget-object v1, Lcom/epson/epsonio/usb/DevUsb;->unregisterReceiverCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getId()J

    move-result-wide v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v3

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1

    .line 215
    sget-object v1, Lcom/epson/epsonio/usb/DevUsb;->unregisterReceiverSynchronizer:Ljava/lang/Integer;

    monitor-enter v1

    .line 217
    :try_start_0
    sget-object v2, Lcom/epson/epsonio/usb/DevUsb;->unregisterReceiverCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 218
    sget-object v3, Lcom/epson/epsonio/usb/DevUsb;->unregisterReceiverCallbackHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    :try_start_1
    sget-object v2, Lcom/epson/epsonio/usb/DevUsb;->unregisterReceiverSynchronizer:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227
    :catch_0
    :try_start_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 231
    :cond_1
    sget-object v1, Lcom/epson/epsonio/usb/DevUsb;->mContext:Landroid/content/Context;

    sget-object v2, Lcom/epson/epsonio/usb/DevUsb;->mUsbReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_2
    :goto_0
    return v0
.end method
