.class public Lcom/epson/epsonio/bluetooth/AdapterHandler;
.super Landroid/os/Handler;
.source "AdapterHandler.java"


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mDoLoop:Z

.field private mSynchronizeFlag:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 17
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const/4 v0, 0x0

    .line 12
    iput-object v0, p0, Lcom/epson/epsonio/bluetooth/AdapterHandler;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v0, 0x1

    .line 13
    iput-boolean v0, p0, Lcom/epson/epsonio/bluetooth/AdapterHandler;->mDoLoop:Z

    .line 14
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/epson/epsonio/bluetooth/AdapterHandler;->mSynchronizeFlag:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getAdapter()Landroid/bluetooth/BluetoothAdapter;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epsonio/EpsonIoException;
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/epson/epsonio/bluetooth/AdapterHandler;->mSynchronizeFlag:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x0

    .line 22
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/epson/epsonio/bluetooth/AdapterHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 24
    invoke-virtual {p0, v1}, Lcom/epson/epsonio/bluetooth/AdapterHandler;->sendMessage(Landroid/os/Message;)Z

    move-result v1

    const/16 v2, 0xff

    if-eqz v1, :cond_1

    .line 28
    :catch_0
    :goto_0
    iget-boolean v1, p0, Lcom/epson/epsonio/bluetooth/AdapterHandler;->mDoLoop:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 30
    :try_start_1
    iget-object v1, p0, Lcom/epson/epsonio/bluetooth/AdapterHandler;->mSynchronizeFlag:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v1

    .line 36
    :try_start_2
    new-instance v3, Lcom/epson/epsonio/EpsonIoException;

    const/4 v4, 0x0

    invoke-direct {v3, v4, v1}, Lcom/epson/epsonio/EpsonIoException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 37
    invoke-virtual {v3, v2}, Lcom/epson/epsonio/EpsonIoException;->setStatus(I)V

    .line 38
    throw v3

    .line 41
    :cond_0
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 42
    iget-object v0, p0, Lcom/epson/epsonio/bluetooth/AdapterHandler;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    return-object v0

    .line 25
    :cond_1
    :try_start_3
    new-instance v1, Lcom/epson/epsonio/EpsonIoException;

    invoke-direct {v1, v2}, Lcom/epson/epsonio/EpsonIoException;-><init>(I)V

    throw v1

    :catchall_0
    move-exception v1

    .line 41
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 1

    .line 47
    iget p1, p1, Landroid/os/Message;->what:I

    if-nez p1, :cond_0

    .line 49
    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object p1

    iput-object p1, p0, Lcom/epson/epsonio/bluetooth/AdapterHandler;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 p1, 0x0

    .line 52
    iput-object p1, p0, Lcom/epson/epsonio/bluetooth/AdapterHandler;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 55
    :goto_0
    iget-object p1, p0, Lcom/epson/epsonio/bluetooth/AdapterHandler;->mSynchronizeFlag:Ljava/lang/Object;

    monitor-enter p1

    const/4 v0, 0x0

    .line 56
    :try_start_1
    iput-boolean v0, p0, Lcom/epson/epsonio/bluetooth/AdapterHandler;->mDoLoop:Z

    .line 57
    iget-object v0, p0, Lcom/epson/epsonio/bluetooth/AdapterHandler;->mSynchronizeFlag:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 58
    monitor-exit p1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    :goto_1
    return-void
.end method
