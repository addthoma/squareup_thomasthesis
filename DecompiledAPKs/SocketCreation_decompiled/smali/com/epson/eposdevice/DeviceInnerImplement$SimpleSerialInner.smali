.class public Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;
.super Lcom/epson/eposdevice/simpleserial/SimpleSerial;
.source "DeviceInnerImplement.java"

# interfaces
.implements Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/eposdevice/DeviceInnerImplement;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "SimpleSerialInner"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/epson/eposdevice/DeviceInnerImplement;


# direct methods
.method protected constructor <init>(Lcom/epson/eposdevice/DeviceInnerImplement;J)V
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;->this$0:Lcom/epson/eposdevice/DeviceInnerImplement;

    .line 160
    invoke-direct {p0, p2, p3}, Lcom/epson/eposdevice/simpleserial/SimpleSerial;-><init>(J)V

    return-void
.end method


# virtual methods
.method public deleteInstance()V
    .locals 0

    .line 168
    invoke-virtual {p0}, Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;->innerDeleteInstance()V

    return-void
.end method

.method public getDeviceHandle()J
    .locals 2

    .line 164
    invoke-virtual {p0}, Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;->getInnerHandle()J

    move-result-wide v0

    return-wide v0
.end method

.method protected outputException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    .line 183
    iget-object v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;->this$0:Lcom/epson/eposdevice/DeviceInnerImplement;

    iget v1, v0, Lcom/epson/eposdevice/DeviceInnerImplement;->LOGIF_FUNC_OUT_WITHOUT_RET:I

    invoke-virtual {p0}, Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;->getDeviceHandle()J

    move-result-wide v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/epson/eposdevice/DeviceInnerImplement;->processOutputExceptionLog(IJLjava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method protected varargs outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 6

    .line 173
    iget-object v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;->this$0:Lcom/epson/eposdevice/DeviceInnerImplement;

    iget v1, v0, Lcom/epson/eposdevice/DeviceInnerImplement;->LOGIF_FUNC_IN:I

    invoke-virtual {p0}, Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;->getDeviceHandle()J

    move-result-wide v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/epson/eposdevice/DeviceInnerImplement;->processOutputLogData(IJLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected varargs outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 6

    .line 188
    iget-object v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;->this$0:Lcom/epson/eposdevice/DeviceInnerImplement;

    iget v1, v0, Lcom/epson/eposdevice/DeviceInnerImplement;->LOGIF_FUNC_CB_EVENT:I

    invoke-virtual {p0}, Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;->getDeviceHandle()J

    move-result-wide v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/epson/eposdevice/DeviceInnerImplement;->processOutputLogData(IJLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected varargs outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 6

    .line 178
    iget-object v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;->this$0:Lcom/epson/eposdevice/DeviceInnerImplement;

    iget v1, v0, Lcom/epson/eposdevice/DeviceInnerImplement;->LOGIF_FUNC_OUT_WITH_RET:I

    invoke-virtual {p0}, Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;->getDeviceHandle()J

    move-result-wide v2

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/epson/eposdevice/DeviceInnerImplement;->processOutputLogData(IJLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
