.class public Lcom/epson/eposdevice/EposException;
.super Ljava/lang/Exception;
.source "EposException.java"


# static fields
.field public static final ERR_CONNECT:I = 0x4

.field public static final ERR_FAILURE:I = 0xff

.field public static final ERR_ILLEGAL:I = 0x5

.field public static final ERR_IN_USE:I = 0x8

.field public static final ERR_MEMORY:I = 0xc

.field public static final ERR_PARAM:I = 0x1

.field public static final ERR_PROCESSING:I = 0x2

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mErrorStatus:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    const/16 v0, 0xff

    .line 14
    iput v0, p0, Lcom/epson/eposdevice/EposException;->mErrorStatus:I

    .line 21
    iput p1, p0, Lcom/epson/eposdevice/EposException;->mErrorStatus:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 p1, 0xff

    .line 14
    iput p1, p0, Lcom/epson/eposdevice/EposException;->mErrorStatus:I

    return-void
.end method


# virtual methods
.method public getErrorStatus()I
    .locals 1

    .line 25
    iget v0, p0, Lcom/epson/eposdevice/EposException;->mErrorStatus:I

    return v0
.end method
