.class public Lcom/epson/eposdevice/Device;
.super Lcom/epson/eposdevice/DeviceInnerImplement;
.source "Device.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;,
        Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;,
        Lcom/epson/eposdevice/Device$ConnectCallbackAdapter;
    }
.end annotation


# static fields
.field public static final DEV_TYPE_DISPLAY:I = 0x1

.field public static final DEV_TYPE_KEYBOARD:I = 0x2

.field public static final DEV_TYPE_PRINTER:I = 0x0

.field public static final DEV_TYPE_SCANNER:I = 0x3

.field public static final DEV_TYPE_SIMPLE_SERIAL:I = 0x4

.field public static final FALSE:I = 0x0

.field public static final PARAM_DEFAULT:I = -0x2

.field public static final PARAM_UNSPECIFIED:I = -0x1

.field private static final SDK_NAME:Ljava/lang/String; = "ePOS-Device SDK for Android"

.field private static final SDK_VERSION:Ljava/lang/String; = "1.2.0"

.field public static final TRUE:I = 0x1


# instance fields
.field private mCommBoxManager:Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;

.field private mCreatedDeviceList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;",
            ">;"
        }
    .end annotation
.end field

.field private mDeviceHandle:J

.field private mDisconnectListener:Lcom/epson/eposdevice/DisconnectListener;

.field private mReconnectListener:Lcom/epson/eposdevice/ReconnectListener;

.field private mReconnectingListener:Lcom/epson/eposdevice/ReconnectingListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epos2"

    .line 17
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Lcom/epson/eposdevice/DeviceInnerImplement;-><init>()V

    .line 40
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/epson/eposdevice/Device;->mCreatedDeviceList:Ljava/util/Vector;

    const-wide/16 v0, 0x0

    .line 379
    iput-wide v0, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    const/4 v0, 0x0

    .line 380
    iput-object v0, p0, Lcom/epson/eposdevice/Device;->mCommBoxManager:Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;

    .line 49
    invoke-virtual {p0}, Lcom/epson/eposdevice/Device;->initializeDeviceInstance()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    .line 52
    invoke-direct {p0}, Lcom/epson/eposdevice/DeviceInnerImplement;-><init>()V

    .line 40
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/epson/eposdevice/Device;->mCreatedDeviceList:Ljava/util/Vector;

    const-wide/16 v0, 0x0

    .line 379
    iput-wide v0, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    const/4 v0, 0x0

    .line 380
    iput-object v0, p0, Lcom/epson/eposdevice/Device;->mCommBoxManager:Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;

    const-string v1, ""

    if-eqz p1, :cond_0

    .line 57
    :try_start_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    move-object v3, v1

    const-string v6, "Android"

    .line 64
    sget-object v7, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 65
    sget-object v8, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 67
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/eposdevice/Device;->getTotalMemorySize(Ljava/io/File;)J

    move-result-wide v0

    .line 68
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/eposdevice/Device;->getFreeMemorySize(Ljava/io/File;)J

    move-result-wide v4

    .line 69
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object p1

    invoke-static {p1}, Lcom/epson/eposdevice/Device;->getFreeMemorySize(Ljava/io/File;)J

    move-result-wide v9

    .line 71
    sget-object p1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    const/4 v5, 0x0

    aput-object v4, v2, v5

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v2, v1

    const-string v0, "%d/%d"

    invoke-static {p1, v0, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 72
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v5

    const-string v2, "%d"

    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    :try_start_1
    const-string v4, "ePOS-Device SDK for Android"

    const-string v5, "1.2.0"

    move-object v2, p0

    move-object v9, p1

    .line 75
    invoke-virtual/range {v2 .. v10}, Lcom/epson/eposdevice/Device;->nativeReadLogSettings(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 80
    :catch_1
    invoke-virtual {p0}, Lcom/epson/eposdevice/Device;->initializeDeviceInstance()V

    return-void
.end method

.method static synthetic access$000(Lcom/epson/eposdevice/Device;)Ljava/util/Vector;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/epson/eposdevice/Device;->mCreatedDeviceList:Ljava/util/Vector;

    return-object p0
.end method

.method private static getFreeMemorySize(Ljava/io/File;)J
    .locals 2

    .line 101
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object p0

    invoke-virtual {p0}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0
.end method

.method private static getTotalMemorySize(Ljava/io/File;)J
    .locals 2

    .line 88
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object p0

    invoke-virtual {p0}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0
.end method

.method private onDisconnect(Ljava/lang/String;)V
    .locals 2

    .line 348
    iget-object v0, p0, Lcom/epson/eposdevice/Device;->mDisconnectListener:Lcom/epson/eposdevice/DisconnectListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "onDisconnect"

    .line 349
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/Device;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    iget-object v0, p0, Lcom/epson/eposdevice/Device;->mDisconnectListener:Lcom/epson/eposdevice/DisconnectListener;

    invoke-interface {v0, p1}, Lcom/epson/eposdevice/DisconnectListener;->onDisconnect(Ljava/lang/String;)V

    .line 352
    :cond_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/Device;->deleteAllDeviceObject()V

    return-void
.end method

.method private onReconnect(Ljava/lang/String;)V
    .locals 2

    .line 341
    iget-object v0, p0, Lcom/epson/eposdevice/Device;->mReconnectListener:Lcom/epson/eposdevice/ReconnectListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "onReconnect"

    .line 342
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/Device;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 343
    iget-object v0, p0, Lcom/epson/eposdevice/Device;->mReconnectListener:Lcom/epson/eposdevice/ReconnectListener;

    invoke-interface {v0, p1}, Lcom/epson/eposdevice/ReconnectListener;->onReconnect(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private onReconnecting(Ljava/lang/String;)V
    .locals 2

    .line 334
    iget-object v0, p0, Lcom/epson/eposdevice/Device;->mReconnectingListener:Lcom/epson/eposdevice/ReconnectingListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "onReconnecting"

    .line 335
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/Device;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 336
    iget-object v0, p0, Lcom/epson/eposdevice/Device;->mReconnectingListener:Lcom/epson/eposdevice/ReconnectingListener;

    invoke-interface {v0, p1}, Lcom/epson/eposdevice/ReconnectingListener;->onReconnecting(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public connect(Ljava/lang/String;Lcom/epson/eposdevice/ConnectListener;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v3, 0x1

    aput-object p2, v1, v3

    const-string v4, "connect"

    .line 148
    invoke-virtual {p0, v4, v1}, Lcom/epson/eposdevice/Device;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    const-wide/16 v5, 0x0

    .line 154
    :try_start_0
    iget-wide v7, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    cmp-long v1, v5, v7

    if-eqz v1, :cond_1

    .line 157
    iget-wide v5, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    new-instance v1, Lcom/epson/eposdevice/Device$ConnectCallbackAdapter;

    invoke-direct {v1, p0, p2}, Lcom/epson/eposdevice/Device$ConnectCallbackAdapter;-><init>(Lcom/epson/eposdevice/Device;Lcom/epson/eposdevice/ConnectListener;)V

    invoke-virtual {p0, v5, v6, p1, v1}, Lcom/epson/eposdevice/Device;->nativeConnect(JLjava/lang/String;Lcom/epson/eposdevice/NativeDevice$NativeConnectCallbackAdapter;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p2, v0, v3

    .line 167
    invoke-virtual {p0, v4, v0}, Lcom/epson/eposdevice/Device;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 159
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1

    .line 155
    :cond_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    const/4 p2, 0x5

    invoke-direct {p1, p2}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1

    .line 152
    :cond_2
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v3}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 163
    invoke-virtual {p0, v4, p1}, Lcom/epson/eposdevice/Device;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 164
    throw p1
.end method

.method public createDevice(Ljava/lang/String;IIILcom/epson/eposdevice/CreateDeviceListener;)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v0, p5

    const/4 v10, 0x5

    new-array v1, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v1, v11

    .line 202
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v12, 0x1

    aput-object v2, v1, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v13, 0x2

    aput-object v2, v1, v13

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v14, 0x3

    aput-object v2, v1, v14

    const/4 v15, 0x4

    aput-object v0, v1, v15

    const-string v8, "createDevice"

    invoke-virtual {v9, v8, v1}, Lcom/epson/eposdevice/Device;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_2

    if-eqz v0, :cond_2

    const-wide/16 v1, 0x0

    .line 209
    :try_start_0
    iget-wide v3, v9, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    cmp-long v5, v1, v3

    if-eqz v5, :cond_1

    .line 213
    iget-wide v2, v9, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    new-instance v7, Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;

    invoke-direct {v7, v9, v0}, Lcom/epson/eposdevice/Device$CreateDeviceCallbackAdapter;-><init>(Lcom/epson/eposdevice/Device;Lcom/epson/eposdevice/CreateDeviceListener;)V
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_1

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move-object/from16 v16, v7

    move/from16 v7, p4

    move-object/from16 v17, v8

    move-object/from16 v8, v16

    :try_start_1
    invoke-virtual/range {v1 .. v8}, Lcom/epson/eposdevice/Device;->nativeCreateDevice(JLjava/lang/String;IIILcom/epson/eposdevice/NativeDevice$NativeCreateDeviceCallbackAdapter;)I

    move-result v1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v1, :cond_0

    new-array v1, v10, [Ljava/lang/Object;

    aput-object p1, v1, v11

    .line 223
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v12

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v13

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v14

    aput-object v0, v1, v15

    move-object/from16 v2, v17

    invoke-virtual {v9, v2, v1}, Lcom/epson/eposdevice/Device;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v2, v17

    .line 215
    :try_start_2
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    :catch_0
    move-exception v0

    move-object/from16 v2, v17

    goto :goto_0

    :cond_1
    move-object v2, v8

    .line 210
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v10}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    :catch_1
    move-exception v0

    move-object v2, v8

    goto :goto_0

    :cond_2
    move-object v2, v8

    .line 206
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v12}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_2
    .catch Lcom/epson/eposdevice/EposException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    move-exception v0

    .line 219
    :goto_0
    invoke-virtual {v9, v2, v0}, Lcom/epson/eposdevice/Device;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 220
    throw v0
.end method

.method protected deleteAllDeviceObject()V
    .locals 2

    .line 369
    iget-object v0, p0, Lcom/epson/eposdevice/Device;->mCreatedDeviceList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 371
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 372
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;

    invoke-interface {v1}, Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;->deleteInstance()V

    goto :goto_0

    .line 375
    :cond_0
    iget-object v0, p0, Lcom/epson/eposdevice/Device;->mCreatedDeviceList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->removeAllElements()V

    return-void
.end method

.method public deleteDevice(Ljava/lang/Object;Lcom/epson/eposdevice/DeleteDeviceListener;)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v3, 0x1

    aput-object p2, v1, v3

    const-string v4, "deleteDevice"

    .line 227
    invoke-virtual {p0, v4, v1}, Lcom/epson/eposdevice/Device;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    .line 234
    :try_start_0
    instance-of v1, p1, Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;

    if-eqz v1, :cond_2

    const-wide/16 v5, 0x0

    .line 238
    iget-wide v7, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    cmp-long v1, v5, v7

    if-eqz v1, :cond_1

    .line 242
    move-object v1, p1

    check-cast v1, Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;

    invoke-interface {v1}, Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;->getDeviceHandle()J

    move-result-wide v8

    .line 243
    iget-wide v6, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    new-instance v10, Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;

    move-object v1, p1

    check-cast v1, Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;

    invoke-direct {v10, p0, p2, v1}, Lcom/epson/eposdevice/Device$DeleteDeviceCallbackAdapter;-><init>(Lcom/epson/eposdevice/Device;Lcom/epson/eposdevice/DeleteDeviceListener;Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;)V

    move-object v5, p0

    invoke-virtual/range {v5 .. v10}, Lcom/epson/eposdevice/Device;->nativeDeleteDevice(JJLcom/epson/eposdevice/NativeDevice$NativeDeleteDeviceCallbackAdapter;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p2, v0, v3

    .line 253
    invoke-virtual {p0, v4, v0}, Lcom/epson/eposdevice/Device;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 245
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1

    .line 239
    :cond_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    const/4 p2, 0x5

    invoke-direct {p1, p2}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1

    .line 235
    :cond_2
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v3}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1

    .line 231
    :cond_3
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v3}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 249
    invoke-virtual {p0, v4, p1}, Lcom/epson/eposdevice/Device;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 250
    throw p1
.end method

.method public disconnect()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "disconnect"

    .line 171
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/Device;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    const-wide/16 v3, 0x0

    .line 174
    :try_start_0
    iget-wide v5, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    cmp-long v1, v3, v5

    if-eqz v1, :cond_1

    .line 178
    iget-wide v3, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/eposdevice/Device;->nativeDisconnect(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 188
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/Device;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 180
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    .line 175
    :cond_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 184
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/Device;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 185
    throw v0
.end method

.method protected finalize()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .line 131
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/Device;->deleteAllDeviceObject()V

    .line 132
    iget-object v0, p0, Lcom/epson/eposdevice/Device;->mCommBoxManager:Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/epson/eposdevice/Device;->mCommBoxManager:Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;

    invoke-virtual {v0}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->deleteInstance()V

    .line 134
    iput-object v1, p0, Lcom/epson/eposdevice/Device;->mCommBoxManager:Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;

    .line 137
    :cond_0
    iget-wide v2, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    .line 138
    iget-wide v2, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    invoke-virtual {p0, v2, v3, v1}, Lcom/epson/eposdevice/Device;->nativeSetDeviceCallback(JLcom/epson/eposdevice/NativeDevice;)V

    .line 139
    iget-wide v0, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    invoke-virtual {p0, v0, v1}, Lcom/epson/eposdevice/Device;->nativeDeleteHandle(J)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    :cond_1
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 144
    throw v0
.end method

.method public getAdmin()Ljava/lang/String;
    .locals 5

    .line 257
    iget-wide v0, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v2, v0

    if-nez v4, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 261
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/epson/eposdevice/Device;->nativeGetAdmin(J)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    return-object v0
.end method

.method public getCommBoxManager()Lcom/epson/eposdevice/commbox/CommBoxManager;
    .locals 1

    .line 283
    iget-object v0, p0, Lcom/epson/eposdevice/Device;->mCommBoxManager:Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 5

    .line 270
    iget-wide v0, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v2, v0

    if-nez v4, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 274
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/epson/eposdevice/Device;->nativeGetLocal(J)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    return-object v0
.end method

.method protected initializeDeviceInstance()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [J

    .line 112
    invoke-virtual {p0, v0}, Lcom/epson/eposdevice/Device;->nativeCreateHandle([J)I

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    .line 118
    aget-wide v2, v0, v1

    iput-wide v2, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    const-wide/16 v2, 0x0

    aput-wide v2, v0, v1

    .line 120
    iget-wide v2, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/eposdevice/Device;->nativeGetCommBoxManager(J[J)I

    move-result v2

    if-nez v2, :cond_0

    .line 126
    new-instance v2, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;

    aget-wide v3, v0, v1

    invoke-direct {v2, p0, v3, v4}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;-><init>(Lcom/epson/eposdevice/DeviceInnerImplement;J)V

    iput-object v2, p0, Lcom/epson/eposdevice/Device;->mCommBoxManager:Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;

    return-void

    .line 122
    :cond_0
    iget-wide v0, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    invoke-virtual {p0, v0, v1}, Lcom/epson/eposdevice/Device;->nativeDeleteHandle(J)I

    .line 123
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v2}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    .line 115
    :cond_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
.end method

.method public isConnected()Z
    .locals 5

    .line 192
    iget-wide v0, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v2, v0

    if-nez v4, :cond_0

    const/4 v0, 0x0

    return v0

    .line 196
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/epson/eposdevice/Device;->nativeIsConnect(J)Z

    move-result v0

    return v0
.end method

.method protected isEmptyCallbacks()Z
    .locals 1

    .line 325
    iget-object v0, p0, Lcom/epson/eposdevice/Device;->mReconnectingListener:Lcom/epson/eposdevice/ReconnectingListener;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/epson/eposdevice/Device;->mReconnectListener:Lcom/epson/eposdevice/ReconnectListener;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/epson/eposdevice/Device;->mDisconnectListener:Lcom/epson/eposdevice/DisconnectListener;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected nativeOnDisconnect(Ljava/lang/String;)V
    .locals 0

    .line 357
    invoke-direct {p0, p1}, Lcom/epson/eposdevice/Device;->onDisconnect(Ljava/lang/String;)V

    return-void
.end method

.method protected nativeOnReconnect(Ljava/lang/String;)V
    .locals 0

    .line 365
    invoke-direct {p0, p1}, Lcom/epson/eposdevice/Device;->onReconnect(Ljava/lang/String;)V

    return-void
.end method

.method protected nativeOnReconnecting(Ljava/lang/String;)V
    .locals 0

    .line 361
    invoke-direct {p0, p1}, Lcom/epson/eposdevice/Device;->onReconnecting(Ljava/lang/String;)V

    return-void
.end method

.method protected outputException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 6

    .line 466
    iget v1, p0, Lcom/epson/eposdevice/Device;->LOGIF_FUNC_OUT_WITHOUT_RET:I

    iget-wide v2, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/epson/eposdevice/Device;->processOutputExceptionLog(IJLjava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method protected varargs outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 6

    .line 458
    iget v1, p0, Lcom/epson/eposdevice/Device;->LOGIF_FUNC_IN:I

    iget-wide v2, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/epson/eposdevice/Device;->processOutputLogData(IJLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected varargs outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 6

    .line 470
    iget v1, p0, Lcom/epson/eposdevice/Device;->LOGIF_FUNC_CB_EVENT:I

    iget-wide v2, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/epson/eposdevice/Device;->processOutputLogData(IJLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected varargs outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 6

    .line 462
    iget v1, p0, Lcom/epson/eposdevice/Device;->LOGIF_FUNC_OUT_WITH_RET:I

    iget-wide v2, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/epson/eposdevice/Device;->processOutputLogData(IJLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public setDisconnectEventCallback(Lcom/epson/eposdevice/DisconnectListener;)V
    .locals 2

    .line 311
    invoke-virtual {p0}, Lcom/epson/eposdevice/Device;->isEmptyCallbacks()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 312
    iget-wide v0, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/Device;->nativeSetDeviceCallback(JLcom/epson/eposdevice/NativeDevice;)V

    .line 315
    :cond_0
    iput-object p1, p0, Lcom/epson/eposdevice/Device;->mDisconnectListener:Lcom/epson/eposdevice/DisconnectListener;

    .line 317
    invoke-virtual {p0}, Lcom/epson/eposdevice/Device;->isEmptyCallbacks()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 318
    iget-wide v0, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    const/4 p1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/Device;->nativeSetDeviceCallback(JLcom/epson/eposdevice/NativeDevice;)V

    :cond_1
    return-void
.end method

.method public setReconnectEventCallback(Lcom/epson/eposdevice/ReconnectListener;)V
    .locals 2

    .line 299
    invoke-virtual {p0}, Lcom/epson/eposdevice/Device;->isEmptyCallbacks()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 300
    iget-wide v0, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/Device;->nativeSetDeviceCallback(JLcom/epson/eposdevice/NativeDevice;)V

    .line 303
    :cond_0
    iput-object p1, p0, Lcom/epson/eposdevice/Device;->mReconnectListener:Lcom/epson/eposdevice/ReconnectListener;

    .line 305
    invoke-virtual {p0}, Lcom/epson/eposdevice/Device;->isEmptyCallbacks()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 306
    iget-wide v0, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    const/4 p1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/Device;->nativeSetDeviceCallback(JLcom/epson/eposdevice/NativeDevice;)V

    :cond_1
    return-void
.end method

.method public setReconnectingEventCallback(Lcom/epson/eposdevice/ReconnectingListener;)V
    .locals 2

    .line 287
    invoke-virtual {p0}, Lcom/epson/eposdevice/Device;->isEmptyCallbacks()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 288
    iget-wide v0, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/Device;->nativeSetDeviceCallback(JLcom/epson/eposdevice/NativeDevice;)V

    .line 291
    :cond_0
    iput-object p1, p0, Lcom/epson/eposdevice/Device;->mReconnectingListener:Lcom/epson/eposdevice/ReconnectingListener;

    .line 293
    invoke-virtual {p0}, Lcom/epson/eposdevice/Device;->isEmptyCallbacks()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 294
    iget-wide v0, p0, Lcom/epson/eposdevice/Device;->mDeviceHandle:J

    const/4 p1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/Device;->nativeSetDeviceCallback(JLcom/epson/eposdevice/NativeDevice;)V

    :cond_1
    return-void
.end method
