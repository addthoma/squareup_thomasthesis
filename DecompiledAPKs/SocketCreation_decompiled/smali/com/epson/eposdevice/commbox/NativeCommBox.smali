.class abstract Lcom/epson/eposdevice/commbox/NativeCommBox;
.super Ljava/lang/Object;
.source "NativeCommBox.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/eposdevice/commbox/NativeCommBox$NativeCommBoxSendDataCallbackAdapter;,
        Lcom/epson/eposdevice/commbox/NativeCommBox$NativeCommBoxHistoryCallbackAdapter;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected native nativeCommBoxGetHistory(J[JLcom/epson/eposdevice/commbox/NativeCommBox$NativeCommBoxHistoryCallbackAdapter;)I
.end method

.method protected native nativeCommBoxSendData(JLjava/lang/String;Ljava/lang/String;[JLcom/epson/eposdevice/commbox/NativeCommBox$NativeCommBoxSendDataCallbackAdapter;)I
.end method

.method protected abstract nativeOnCommBoxReceive(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected native nativeSetCommBoxReceiveEventCallback(JLcom/epson/eposdevice/commbox/NativeCommBox;)I
.end method
