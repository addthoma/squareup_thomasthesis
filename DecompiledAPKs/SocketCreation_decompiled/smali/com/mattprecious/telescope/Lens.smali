.class public abstract Lcom/mattprecious/telescope/Lens;
.super Ljava/lang/Object;
.source "Lens.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCapture(Landroid/graphics/Bitmap;Lcom/mattprecious/telescope/BitmapProcessorListener;)V
    .locals 0

    .line 25
    invoke-interface {p2, p1}, Lcom/mattprecious/telescope/BitmapProcessorListener;->onBitmapReady(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public abstract onCapture(Ljava/io/File;)V
.end method
