.class public Lcom/felhr/usbserial/BLED112SerialDevice;
.super Lcom/felhr/usbserial/UsbSerialDevice;
.source "BLED112SerialDevice.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final BLED112_DEFAULT_CONTROL_LINE:I = 0x3

.field private static final BLED112_DEFAULT_LINE_CODING:[B

.field private static final BLED112_DISCONNECT_CONTROL_LINE:I = 0x2

.field private static final BLED112_GET_LINE_CODING:I = 0x21

.field private static final BLED112_REQTYPE_DEVICE2HOST:I = 0xa1

.field private static final BLED112_REQTYPE_HOST2DEVICE:I = 0x21

.field private static final BLED112_SET_CONTROL_LINE_STATE:I = 0x22

.field private static final BLED112_SET_LINE_CODING:I = 0x20

.field private static final CLASS_ID:Ljava/lang/String;


# instance fields
.field private inEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private final mInterface:Landroid/hardware/usb/UsbInterface;

.field private outEndpoint:Landroid/hardware/usb/UsbEndpoint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    const-class v0, Lcom/felhr/usbserial/BLED112SerialDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/felhr/usbserial/BLED112SerialDevice;->CLASS_ID:Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [B

    .line 31
    fill-array-data v0, :array_0

    sput-object v0, Lcom/felhr/usbserial/BLED112SerialDevice;->BLED112_DEFAULT_LINE_CODING:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x1t
        -0x3et
        0x0t
        0x0t
        0x0t
        0x8t
    .end array-data
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 51
    invoke-direct {p0, p1, p2}, Lcom/felhr/usbserial/UsbSerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V

    const/4 p2, 0x1

    .line 52
    invoke-virtual {p1, p2}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object p1

    iput-object p1, p0, Lcom/felhr/usbserial/BLED112SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    return-void
.end method

.method private getLineCoding()[B
    .locals 9

    const/4 v0, 0x7

    new-array v0, v0, [B

    .line 282
    iget-object v1, p0, Lcom/felhr/usbserial/BLED112SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    array-length v7, v0

    const/16 v2, 0xa1

    const/16 v3, 0x21

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x0

    move-object v6, v0

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result v1

    .line 283
    sget-object v2, Lcom/felhr/usbserial/BLED112SerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Control Transfer Response: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method private setControlCommand(II[B)I
    .locals 9

    if-eqz p3, :cond_0

    .line 272
    array-length v0, p3

    move v7, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 274
    :goto_0
    iget-object v1, p0, Lcom/felhr/usbserial/BLED112SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    const/16 v2, 0x21

    const/4 v5, 0x0

    const/4 v8, 0x0

    move v3, p1

    move v4, p2

    move-object v6, p3

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result p1

    .line 275
    sget-object p2, Lcom/felhr/usbserial/BLED112SerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Control Transfer Response: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return p1
.end method


# virtual methods
.method public close()V
    .locals 3

    const/16 v0, 0x22

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 102
    invoke-direct {p0, v0, v1, v2}, Lcom/felhr/usbserial/BLED112SerialDevice;->setControlCommand(II[B)I

    .line 103
    invoke-virtual {p0}, Lcom/felhr/usbserial/BLED112SerialDevice;->killWorkingThread()V

    .line 104
    invoke-virtual {p0}, Lcom/felhr/usbserial/BLED112SerialDevice;->killWriteThread()V

    .line 105
    iget-object v0, p0, Lcom/felhr/usbserial/BLED112SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/BLED112SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0, v1}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    return-void
.end method

.method public getBreak(Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;)V
    .locals 0

    return-void
.end method

.method public getCTS(Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;)V
    .locals 0

    return-void
.end method

.method public getDSR(Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;)V
    .locals 0

    return-void
.end method

.method public getFrame(Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;)V
    .locals 0

    return-void
.end method

.method public getOverrun(Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;)V
    .locals 0

    return-void
.end method

.method public getParity(Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;)V
    .locals 0

    return-void
.end method

.method public open()Z
    .locals 7

    .line 59
    invoke-virtual {p0}, Lcom/felhr/usbserial/BLED112SerialDevice;->restartWorkingThread()V

    .line 60
    invoke-virtual {p0}, Lcom/felhr/usbserial/BLED112SerialDevice;->restartWriteThread()V

    .line 62
    iget-object v0, p0, Lcom/felhr/usbserial/BLED112SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/BLED112SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    sget-object v0, Lcom/felhr/usbserial/BLED112SerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v1, "Interface succesfully claimed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 67
    :cond_0
    sget-object v0, Lcom/felhr/usbserial/BLED112SerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v1, "Interface could not be claimed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    :goto_0
    iget-object v0, p0, Lcom/felhr/usbserial/BLED112SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v3, 0x0

    :goto_1
    add-int/lit8 v4, v0, -0x1

    if-gt v3, v4, :cond_2

    .line 74
    iget-object v4, p0, Lcom/felhr/usbserial/BLED112SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v4, v3}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v4

    .line 75
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    .line 76
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v5

    const/16 v6, 0x80

    if-ne v5, v6, :cond_1

    .line 78
    iput-object v4, p0, Lcom/felhr/usbserial/BLED112SerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    goto :goto_2

    .line 81
    :cond_1
    iput-object v4, p0, Lcom/felhr/usbserial/BLED112SerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    const/16 v0, 0x20

    .line 86
    sget-object v3, Lcom/felhr/usbserial/BLED112SerialDevice;->BLED112_DEFAULT_LINE_CODING:[B

    invoke-direct {p0, v0, v1, v3}, Lcom/felhr/usbserial/BLED112SerialDevice;->setControlCommand(II[B)I

    const/16 v0, 0x22

    const/4 v1, 0x3

    const/4 v3, 0x0

    .line 87
    invoke-direct {p0, v0, v1, v3}, Lcom/felhr/usbserial/BLED112SerialDevice;->setControlCommand(II[B)I

    .line 90
    new-instance v0, Landroid/hardware/usb/UsbRequest;

    invoke-direct {v0}, Landroid/hardware/usb/UsbRequest;-><init>()V

    .line 91
    iget-object v1, p0, Lcom/felhr/usbserial/BLED112SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v3, p0, Lcom/felhr/usbserial/BLED112SerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {v0, v1, v3}, Landroid/hardware/usb/UsbRequest;->initialize(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;)Z

    .line 94
    iget-object v1, p0, Lcom/felhr/usbserial/BLED112SerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {p0, v0, v1}, Lcom/felhr/usbserial/BLED112SerialDevice;->setThreadsParams(Landroid/hardware/usb/UsbRequest;Landroid/hardware/usb/UsbEndpoint;)V

    return v2
.end method

.method public setBaudRate(I)V
    .locals 3

    .line 123
    invoke-direct {p0}, Lcom/felhr/usbserial/BLED112SerialDevice;->getLineCoding()[B

    move-result-object v0

    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x3

    .line 125
    aput-byte v1, v0, v2

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x2

    .line 126
    aput-byte v1, v0, v2

    shr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x1

    .line 127
    aput-byte v1, v0, v2

    shr-int/lit8 p1, p1, 0x18

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    const/4 v1, 0x0

    .line 128
    aput-byte p1, v0, v1

    const/16 p1, 0x20

    .line 130
    invoke-direct {p0, p1, v1, v0}, Lcom/felhr/usbserial/BLED112SerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public setBreak(Z)V
    .locals 0

    return-void
.end method

.method public setDTR(Z)V
    .locals 0

    return-void
.end method

.method public setDataBits(I)V
    .locals 3

    .line 136
    invoke-direct {p0}, Lcom/felhr/usbserial/BLED112SerialDevice;->getLineCoding()[B

    move-result-object v0

    const/4 v1, 0x5

    const/4 v2, 0x6

    if-eq p1, v1, :cond_3

    if-eq p1, v2, :cond_2

    const/4 v1, 0x7

    if-eq p1, v1, :cond_1

    const/16 v1, 0x8

    if-eq p1, v1, :cond_0

    goto :goto_0

    .line 149
    :cond_0
    aput-byte v1, v0, v2

    goto :goto_0

    .line 146
    :cond_1
    aput-byte v1, v0, v2

    goto :goto_0

    .line 143
    :cond_2
    aput-byte v2, v0, v2

    goto :goto_0

    .line 140
    :cond_3
    aput-byte v1, v0, v2

    :goto_0
    const/16 p1, 0x20

    const/4 v1, 0x0

    .line 153
    invoke-direct {p0, p1, v1, v0}, Lcom/felhr/usbserial/BLED112SerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public setFlowControl(I)V
    .locals 0

    return-void
.end method

.method public setParity(I)V
    .locals 4

    .line 182
    invoke-direct {p0}, Lcom/felhr/usbserial/BLED112SerialDevice;->getLineCoding()[B

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x5

    if-eqz p1, :cond_4

    const/4 v3, 0x1

    if-eq p1, v3, :cond_3

    const/4 v3, 0x2

    if-eq p1, v3, :cond_2

    const/4 v3, 0x3

    if-eq p1, v3, :cond_1

    const/4 v3, 0x4

    if-eq p1, v3, :cond_0

    goto :goto_0

    .line 198
    :cond_0
    aput-byte v3, v0, v2

    goto :goto_0

    .line 195
    :cond_1
    aput-byte v3, v0, v2

    goto :goto_0

    .line 192
    :cond_2
    aput-byte v3, v0, v2

    goto :goto_0

    .line 189
    :cond_3
    aput-byte v3, v0, v2

    goto :goto_0

    .line 186
    :cond_4
    aput-byte v1, v0, v2

    :goto_0
    const/16 p1, 0x20

    .line 202
    invoke-direct {p0, p1, v1, v0}, Lcom/felhr/usbserial/BLED112SerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public setRTS(Z)V
    .locals 0

    return-void
.end method

.method public setStopBits(I)V
    .locals 5

    .line 160
    invoke-direct {p0}, Lcom/felhr/usbserial/BLED112SerialDevice;->getLineCoding()[B

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x4

    if-eq p1, v2, :cond_2

    const/4 v4, 0x2

    if-eq p1, v4, :cond_1

    const/4 v4, 0x3

    if-eq p1, v4, :cond_0

    goto :goto_0

    .line 167
    :cond_0
    aput-byte v2, v0, v3

    goto :goto_0

    .line 170
    :cond_1
    aput-byte v4, v0, v3

    goto :goto_0

    .line 164
    :cond_2
    aput-byte v1, v0, v3

    :goto_0
    const/16 p1, 0x20

    .line 174
    invoke-direct {p0, p1, v1, v0}, Lcom/felhr/usbserial/BLED112SerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public syncClose()V
    .locals 0

    return-void
.end method

.method public syncOpen()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
