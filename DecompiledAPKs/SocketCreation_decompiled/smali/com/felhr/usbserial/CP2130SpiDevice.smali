.class public Lcom/felhr/usbserial/CP2130SpiDevice;
.super Lcom/felhr/usbserial/UsbSpiDevice;
.source "CP2130SpiDevice.java"


# static fields
.field private static final BM_REQ_DEVICE_2_HOST:I = 0xc0

.field private static final BM_REQ_HOST_2_DEVICE:I = 0x40

.field private static final CLASS_ID:Ljava/lang/String;

.field public static final CLOCK_12MHz:I = 0x0

.field public static final CLOCK_187_5KHz:I = 0x6

.field public static final CLOCK_1_5MHz:I = 0x3

.field public static final CLOCK_375KHz:I = 0x5

.field public static final CLOCK_3MHz:I = 0x2

.field public static final CLOCK_6MHz:I = 0x1

.field public static final CLOCK_750KHz:I = 0x4

.field public static final CLOCK_93_75KHz:I = 0x7

.field private static final GET_SPI_WORD:I = 0x30

.field private static final SET_GPIO_CHIP_SELECT:I = 0x25

.field private static final SET_SPI_WORD:I = 0x31


# instance fields
.field private currentChannel:I

.field private inEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private final mInterface:Landroid/hardware/usb/UsbInterface;

.field private outEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private requestIN:Landroid/hardware/usb/UsbRequest;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    const-class v0, Lcom/felhr/usbserial/CP2130SpiDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/felhr/usbserial/CP2130SpiDevice;->CLASS_ID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V
    .locals 1

    const/4 v0, -0x1

    .line 41
    invoke-direct {p0, p1, p2, v0}, Lcom/felhr/usbserial/CP2130SpiDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/felhr/usbserial/UsbSpiDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V

    const/4 p2, 0x0

    if-ltz p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    .line 47
    :goto_0
    invoke-virtual {p1, p3}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object p1

    iput-object p1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    .line 48
    iput p2, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->currentChannel:I

    return-void
.end method

.method private getSpiWord()[B
    .locals 3

    const/4 v0, 0x0

    const/16 v1, 0x30

    const/4 v2, 0x2

    .line 265
    invoke-direct {p0, v1, v0, v0, v2}, Lcom/felhr/usbserial/CP2130SpiDevice;->setControlCommandIn(IIII)[B

    move-result-object v0

    return-object v0
.end method

.method private openCP2130()Z
    .locals 6

    .line 189
    iget-object v0, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    .line 191
    sget-object v0, Lcom/felhr/usbserial/CP2130SpiDevice;->CLASS_ID:Ljava/lang/String;

    const-string v3, "Interface succesfully claimed"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    iget-object v0, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v0

    :goto_0
    add-int/lit8 v3, v0, -0x1

    if-gt v1, v3, :cond_1

    .line 202
    iget-object v3, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v3, v1}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v3

    .line 203
    invoke-virtual {v3}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 204
    invoke-virtual {v3}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v4

    const/16 v5, 0x80

    if-ne v4, v5, :cond_0

    .line 206
    iput-object v3, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    goto :goto_1

    .line 209
    :cond_0
    iput-object v3, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v2

    .line 194
    :cond_2
    sget-object v0, Lcom/felhr/usbserial/CP2130SpiDevice;->CLASS_ID:Ljava/lang/String;

    const-string v2, "Interface could not be claimed"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method private setControlCommandIn(IIII)[B
    .locals 8

    .line 282
    new-array p3, p4, [B

    .line 283
    iget-object v0, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbInterface;->getId()I

    move-result v4

    const/16 v1, 0xc0

    const/16 v7, 0x1388

    move v2, p1

    move v3, p2

    move-object v5, p3

    move v6, p4

    invoke-virtual/range {v0 .. v7}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result p1

    .line 284
    sget-object p2, Lcom/felhr/usbserial/CP2130SpiDevice;->CLASS_ID:Ljava/lang/String;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Control Transfer Response: "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object p3
.end method

.method private setControlCommandOut(III[B)I
    .locals 8

    if-eqz p4, :cond_0

    .line 273
    array-length p3, p4

    move v6, p3

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    const/4 v6, 0x0

    .line 275
    :goto_0
    iget-object v0, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    const/16 v1, 0x40

    iget-object p3, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {p3}, Landroid/hardware/usb/UsbInterface;->getId()I

    move-result v4

    const/16 v7, 0x1388

    move v2, p1

    move v3, p2

    move-object v5, p4

    invoke-virtual/range {v0 .. v7}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result p1

    .line 276
    sget-object p2, Lcom/felhr/usbserial/CP2130SpiDevice;->CLASS_ID:Ljava/lang/String;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Control Transfer Response: "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return p1
.end method

.method private setGpioChipSelect(IZ)V
    .locals 4

    const/4 v0, 0x2

    new-array v1, v0, [B

    if-ltz p1, :cond_2

    const/16 v2, 0xa

    if-gt p1, v2, :cond_2

    int-to-byte v2, p1

    const/4 v3, 0x0

    aput-byte v2, v1, v3

    const/4 v2, 0x1

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    aput-byte v0, v1, v2

    const/16 p2, 0x25

    .line 256
    invoke-direct {p0, p2, v3, v3, v1}, Lcom/felhr/usbserial/CP2130SpiDevice;->setControlCommandOut(III[B)I

    move-result p2

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 259
    iput p1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->currentChannel:I

    :cond_1
    return-void

    .line 244
    :cond_2
    sget-object p1, Lcom/felhr/usbserial/CP2130SpiDevice;->CLASS_ID:Ljava/lang/String;

    const-string p2, "Channel not valid"

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private setSetSpiWord(II)V
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [B

    if-ltz p1, :cond_0

    const/16 v1, 0xa

    if-gt p1, v1, :cond_0

    int-to-byte p1, p1

    const/4 v1, 0x0

    aput-byte p1, v0, v1

    int-to-byte p1, p2

    const/4 p2, 0x1

    aput-byte p1, v0, p2

    .line 229
    aget-byte p1, v0, p2

    or-int/lit8 p1, p1, 0x8

    int-to-byte p1, p1

    aput-byte p1, v0, p2

    const/16 p1, 0x31

    .line 231
    invoke-direct {p0, p1, v1, v1, v0}, Lcom/felhr/usbserial/CP2130SpiDevice;->setControlCommandOut(III[B)I

    return-void

    .line 225
    :cond_0
    sget-object p1, Lcom/felhr/usbserial/CP2130SpiDevice;->CLASS_ID:Ljava/lang/String;

    const-string p2, "Channel not valid"

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public closeSPI()V
    .locals 2

    .line 182
    invoke-virtual {p0}, Lcom/felhr/usbserial/CP2130SpiDevice;->killWorkingThread()V

    .line 183
    invoke-virtual {p0}, Lcom/felhr/usbserial/CP2130SpiDevice;->killWriteThread()V

    .line 184
    iget-object v0, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0, v1}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    return-void
.end method

.method public connectSPI()Z
    .locals 2

    .line 55
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2130SpiDevice;->openCP2130()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/felhr/usbserial/CP2130SpiDevice;->restartWorkingThread()V

    .line 62
    invoke-virtual {p0}, Lcom/felhr/usbserial/CP2130SpiDevice;->restartWriteThread()V

    .line 65
    iget-object v0, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {p0, v0, v1}, Lcom/felhr/usbserial/CP2130SpiDevice;->setThreadsParams(Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)V

    const/4 v0, 0x1

    return v0
.end method

.method public getClockDivider()I
    .locals 2

    .line 175
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2130SpiDevice;->getSpiWord()[B

    move-result-object v0

    .line 176
    iget v1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->currentChannel:I

    aget-byte v0, v0, v1

    and-int/lit8 v0, v0, 0x7

    return v0
.end method

.method public getSelectedSlave()I
    .locals 1

    .line 73
    iget v0, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->currentChannel:I

    return v0
.end method

.method public readMISO(I)V
    .locals 3

    const/16 v0, 0x8

    new-array v0, v0, [B

    const/4 v1, 0x0

    aput-byte v1, v0, v1

    const/4 v2, 0x1

    aput-byte v1, v0, v2

    const/4 v2, 0x2

    aput-byte v1, v0, v2

    const/4 v1, 0x3

    const/16 v2, -0x80

    aput-byte v2, v0, v1

    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x4

    aput-byte v1, v0, v2

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x5

    aput-byte v1, v0, v2

    shr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x6

    aput-byte v1, v0, v2

    shr-int/lit8 p1, p1, 0x18

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    const/4 v1, 0x7

    aput-byte p1, v0, v1

    .line 139
    iget-object p1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {p1, v0}, Lcom/felhr/usbserial/SerialBuffer;->putWriteBuffer([B)V

    return-void
.end method

.method public selectSlave(I)V
    .locals 1

    const/16 v0, 0xa

    if-gt p1, v0, :cond_1

    if-gez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 169
    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CP2130SpiDevice;->setGpioChipSelect(IZ)V

    return-void

    .line 165
    :cond_1
    :goto_0
    sget-object p1, Lcom/felhr/usbserial/CP2130SpiDevice;->CLASS_ID:Ljava/lang/String;

    const-string v0, "selected slave must be in 0-10 range"

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setClock(I)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 121
    :pswitch_0
    iget p1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->currentChannel:I

    const/4 v0, 0x7

    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CP2130SpiDevice;->setSetSpiWord(II)V

    goto :goto_0

    .line 118
    :pswitch_1
    iget p1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->currentChannel:I

    const/4 v0, 0x6

    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CP2130SpiDevice;->setSetSpiWord(II)V

    goto :goto_0

    .line 115
    :pswitch_2
    iget p1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->currentChannel:I

    const/4 v0, 0x5

    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CP2130SpiDevice;->setSetSpiWord(II)V

    goto :goto_0

    .line 112
    :pswitch_3
    iget p1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->currentChannel:I

    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CP2130SpiDevice;->setSetSpiWord(II)V

    goto :goto_0

    .line 109
    :pswitch_4
    iget p1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->currentChannel:I

    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CP2130SpiDevice;->setSetSpiWord(II)V

    goto :goto_0

    .line 106
    :pswitch_5
    iget p1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->currentChannel:I

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CP2130SpiDevice;->setSetSpiWord(II)V

    goto :goto_0

    .line 103
    :pswitch_6
    iget p1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->currentChannel:I

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CP2130SpiDevice;->setSetSpiWord(II)V

    goto :goto_0

    .line 100
    :pswitch_7
    iget p1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->currentChannel:I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/felhr/usbserial/CP2130SpiDevice;->setSetSpiWord(II)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public writeMOSI([B)V
    .locals 5

    .line 79
    array-length v0, p1

    const/16 v1, 0x8

    add-int/2addr v0, v1

    new-array v0, v0, [B

    const/4 v2, 0x0

    .line 80
    aput-byte v2, v0, v2

    const/4 v3, 0x1

    .line 81
    aput-byte v2, v0, v3

    const/4 v4, 0x2

    .line 82
    aput-byte v3, v0, v4

    const/4 v3, 0x3

    const/16 v4, -0x80

    .line 83
    aput-byte v4, v0, v3

    .line 84
    array-length v3, p1

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    const/4 v4, 0x4

    aput-byte v3, v0, v4

    .line 85
    array-length v3, p1

    shr-int/2addr v3, v1

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    const/4 v4, 0x5

    aput-byte v3, v0, v4

    .line 86
    array-length v3, p1

    shr-int/lit8 v3, v3, 0x10

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    const/4 v4, 0x6

    aput-byte v3, v0, v4

    .line 87
    array-length v3, p1

    shr-int/lit8 v3, v3, 0x18

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    const/4 v4, 0x7

    aput-byte v3, v0, v4

    .line 89
    array-length v3, p1

    invoke-static {p1, v2, v0, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 91
    iget-object p1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {p1, v0}, Lcom/felhr/usbserial/SerialBuffer;->putWriteBuffer([B)V

    return-void
.end method

.method public writeRead([BI)V
    .locals 5

    .line 145
    array-length v0, p1

    const/16 v1, 0x8

    add-int/2addr v0, v1

    new-array v0, v0, [B

    const/4 v2, 0x0

    .line 146
    aput-byte v2, v0, v2

    const/4 v3, 0x1

    .line 147
    aput-byte v2, v0, v3

    const/4 v3, 0x2

    .line 148
    aput-byte v3, v0, v3

    const/4 v3, 0x3

    const/16 v4, -0x80

    .line 149
    aput-byte v4, v0, v3

    and-int/lit16 v3, p2, 0xff

    int-to-byte v3, v3

    const/4 v4, 0x4

    .line 150
    aput-byte v3, v0, v4

    shr-int/lit8 v3, p2, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    const/4 v4, 0x5

    .line 151
    aput-byte v3, v0, v4

    shr-int/lit8 v3, p2, 0x10

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    const/4 v4, 0x6

    .line 152
    aput-byte v3, v0, v4

    shr-int/lit8 p2, p2, 0x18

    and-int/lit16 p2, p2, 0xff

    int-to-byte p2, p2

    const/4 v3, 0x7

    .line 153
    aput-byte p2, v0, v3

    .line 155
    array-length p2, p1

    invoke-static {p1, v2, v0, v1, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 157
    iget-object p1, p0, Lcom/felhr/usbserial/CP2130SpiDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {p1, v0}, Lcom/felhr/usbserial/SerialBuffer;->putWriteBuffer([B)V

    return-void
.end method
