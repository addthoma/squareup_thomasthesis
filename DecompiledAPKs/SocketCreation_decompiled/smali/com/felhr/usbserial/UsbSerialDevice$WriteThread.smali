.class Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;
.super Lcom/felhr/usbserial/AbstractWorkerThread;
.source "UsbSerialDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felhr/usbserial/UsbSerialDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WriteThread"
.end annotation


# instance fields
.field private outEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field final synthetic this$0:Lcom/felhr/usbserial/UsbSerialDevice;


# direct methods
.method private constructor <init>(Lcom/felhr/usbserial/UsbSerialDevice;)V
    .locals 0

    .line 395
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    invoke-direct {p0}, Lcom/felhr/usbserial/AbstractWorkerThread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/felhr/usbserial/UsbSerialDevice;Lcom/felhr/usbserial/UsbSerialDevice$1;)V
    .locals 0

    .line 395
    invoke-direct {p0, p1}, Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;-><init>(Lcom/felhr/usbserial/UsbSerialDevice;)V

    return-void
.end method


# virtual methods
.method public doRun()V
    .locals 5

    .line 402
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    iget-object v0, v0, Lcom/felhr/usbserial/UsbSerialDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {v0}, Lcom/felhr/usbserial/SerialBuffer;->getWriteBuffer()[B

    move-result-object v0

    .line 403
    array-length v1, v0

    if-lez v1, :cond_0

    .line 404
    iget-object v1, p0, Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;->this$0:Lcom/felhr/usbserial/UsbSerialDevice;

    iget-object v1, v1, Lcom/felhr/usbserial/UsbSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    array-length v3, v0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    :cond_0
    return-void
.end method

.method public setUsbEndpoint(Landroid/hardware/usb/UsbEndpoint;)V
    .locals 0

    .line 409
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    return-void
.end method
