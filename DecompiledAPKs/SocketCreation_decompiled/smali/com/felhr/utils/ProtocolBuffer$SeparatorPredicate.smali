.class Lcom/felhr/utils/ProtocolBuffer$SeparatorPredicate;
.super Ljava/lang/Object;
.source "ProtocolBuffer.java"

# interfaces
.implements Lcom/annimon/stream/function/IntPredicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felhr/utils/ProtocolBuffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SeparatorPredicate"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/felhr/utils/ProtocolBuffer;


# direct methods
.method private constructor <init>(Lcom/felhr/utils/ProtocolBuffer;)V
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/felhr/utils/ProtocolBuffer$SeparatorPredicate;->this$0:Lcom/felhr/utils/ProtocolBuffer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/felhr/utils/ProtocolBuffer;Lcom/felhr/utils/ProtocolBuffer$1;)V
    .locals 0

    .line 143
    invoke-direct {p0, p1}, Lcom/felhr/utils/ProtocolBuffer$SeparatorPredicate;-><init>(Lcom/felhr/utils/ProtocolBuffer;)V

    return-void
.end method


# virtual methods
.method public test(I)Z
    .locals 5

    .line 146
    iget-object v0, p0, Lcom/felhr/utils/ProtocolBuffer$SeparatorPredicate;->this$0:Lcom/felhr/utils/ProtocolBuffer;

    invoke-static {v0}, Lcom/felhr/utils/ProtocolBuffer;->access$100(Lcom/felhr/utils/ProtocolBuffer;)[B

    move-result-object v0

    aget-byte v0, v0, p1

    iget-object v1, p0, Lcom/felhr/utils/ProtocolBuffer$SeparatorPredicate;->this$0:Lcom/felhr/utils/ProtocolBuffer;

    invoke-static {v1}, Lcom/felhr/utils/ProtocolBuffer;->access$200(Lcom/felhr/utils/ProtocolBuffer;)[B

    move-result-object v1

    const/4 v2, 0x0

    aget-byte v1, v1, v2

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    const/4 v1, 0x1

    .line 147
    :goto_0
    iget-object v3, p0, Lcom/felhr/utils/ProtocolBuffer$SeparatorPredicate;->this$0:Lcom/felhr/utils/ProtocolBuffer;

    invoke-static {v3}, Lcom/felhr/utils/ProtocolBuffer;->access$200(Lcom/felhr/utils/ProtocolBuffer;)[B

    move-result-object v3

    array-length v3, v3

    sub-int/2addr v3, v0

    if-gt v1, v3, :cond_1

    .line 148
    iget-object v3, p0, Lcom/felhr/utils/ProtocolBuffer$SeparatorPredicate;->this$0:Lcom/felhr/utils/ProtocolBuffer;

    invoke-static {v3}, Lcom/felhr/utils/ProtocolBuffer;->access$100(Lcom/felhr/utils/ProtocolBuffer;)[B

    move-result-object v3

    add-int v4, p1, v1

    aget-byte v3, v3, v4

    iget-object v4, p0, Lcom/felhr/utils/ProtocolBuffer$SeparatorPredicate;->this$0:Lcom/felhr/utils/ProtocolBuffer;

    invoke-static {v4}, Lcom/felhr/utils/ProtocolBuffer;->access$200(Lcom/felhr/utils/ProtocolBuffer;)[B

    move-result-object v4

    aget-byte v4, v4, v1

    if-eq v3, v4, :cond_0

    return v2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0

    :cond_2
    return v2
.end method
