.class final Lcom/f2prateek/rx/preferences2/BooleanAdapter;
.super Ljava/lang/Object;
.source "BooleanAdapter.java"

# interfaces
.implements Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/f2prateek/rx/preferences2/RealPreference$Adapter<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/f2prateek/rx/preferences2/BooleanAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/f2prateek/rx/preferences2/BooleanAdapter;

    invoke-direct {v0}, Lcom/f2prateek/rx/preferences2/BooleanAdapter;-><init>()V

    sput-object v0, Lcom/f2prateek/rx/preferences2/BooleanAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/BooleanAdapter;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/Boolean;
    .locals 1

    const/4 v0, 0x0

    .line 10
    invoke-interface {p2, p1, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic get(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/Object;
    .locals 0

    .line 6
    invoke-virtual {p0, p1, p2}, Lcom/f2prateek/rx/preferences2/BooleanAdapter;->get(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public set(Ljava/lang/String;Ljava/lang/Boolean;Landroid/content/SharedPreferences$Editor;)V
    .locals 0

    .line 15
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-interface {p3, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;Landroid/content/SharedPreferences$Editor;)V
    .locals 0

    .line 6
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2, p3}, Lcom/f2prateek/rx/preferences2/BooleanAdapter;->set(Ljava/lang/String;Ljava/lang/Boolean;Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method
