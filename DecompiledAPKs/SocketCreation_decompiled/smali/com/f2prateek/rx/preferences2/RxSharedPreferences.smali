.class public final Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
.super Ljava/lang/Object;
.source "RxSharedPreferences.java"


# static fields
.field private static final DEFAULT_BOOLEAN:Ljava/lang/Boolean;

.field private static final DEFAULT_FLOAT:Ljava/lang/Float;

.field private static final DEFAULT_INTEGER:Ljava/lang/Integer;

.field private static final DEFAULT_LONG:Ljava/lang/Long;

.field private static final DEFAULT_STRING:Ljava/lang/String; = ""


# instance fields
.field private final keyChanges:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final preferences:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    .line 21
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_FLOAT:Ljava/lang/Float;

    const/4 v0, 0x0

    .line 22
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_INTEGER:Ljava/lang/Integer;

    .line 23
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_BOOLEAN:Ljava/lang/Boolean;

    const-wide/16 v0, 0x0

    .line 24
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_LONG:Ljava/lang/Long;

    return-void
.end method

.method private constructor <init>(Landroid/content/SharedPreferences;)V
    .locals 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    .line 39
    new-instance v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences$1;

    invoke-direct {v0, p0, p1}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences$1;-><init>(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;Landroid/content/SharedPreferences;)V

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    return-void
.end method

.method public static create(Landroid/content/SharedPreferences;)Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
    .locals 1

    const-string v0, "preferences == null"

    .line 30
    invoke-static {p0, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    invoke-direct {v0, p0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;-><init>(Landroid/content/SharedPreferences;)V

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public getBoolean(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 62
    sget-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_BOOLEAN:Ljava/lang/Boolean;

    invoke-virtual {p0, p1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    return-object p1
.end method

.method public getBoolean(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    const-string v0, "key == null"

    .line 68
    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue == null"

    .line 69
    invoke-static {p2, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v2, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/f2prateek/rx/preferences2/BooleanAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/BooleanAdapter;

    iget-object v6, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public getEnum(Ljava/lang/String;Ljava/lang/Enum;Ljava/lang/Class;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum<",
            "TT;>;>(",
            "Ljava/lang/String;",
            "TT;",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "key == null"

    .line 77
    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue == null"

    .line 78
    invoke-static {p2, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "enumClass == null"

    .line 79
    invoke-static {p3, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v2, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    new-instance v5, Lcom/f2prateek/rx/preferences2/EnumAdapter;

    invoke-direct {v5, p3}, Lcom/f2prateek/rx/preferences2/EnumAdapter;-><init>(Ljava/lang/Class;)V

    iget-object v6, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public getFloat(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .line 86
    sget-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_FLOAT:Ljava/lang/Float;

    invoke-virtual {p0, p1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getFloat(Ljava/lang/String;Ljava/lang/Float;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    return-object p1
.end method

.method public getFloat(Ljava/lang/String;Ljava/lang/Float;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    const-string v0, "key == null"

    .line 92
    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue == null"

    .line 93
    invoke-static {p2, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v2, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/f2prateek/rx/preferences2/FloatAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/FloatAdapter;

    iget-object v6, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public getInteger(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 101
    sget-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_INTEGER:Ljava/lang/Integer;

    invoke-virtual {p0, p1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getInteger(Ljava/lang/String;Ljava/lang/Integer;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    return-object p1
.end method

.method public getInteger(Ljava/lang/String;Ljava/lang/Integer;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v0, "key == null"

    .line 107
    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue == null"

    .line 108
    invoke-static {p2, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v2, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/f2prateek/rx/preferences2/IntegerAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/IntegerAdapter;

    iget-object v6, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public getLong(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 116
    sget-object v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->DEFAULT_LONG:Ljava/lang/Long;

    invoke-virtual {p0, p1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getLong(Ljava/lang/String;Ljava/lang/Long;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    return-object p1
.end method

.method public getLong(Ljava/lang/String;Ljava/lang/Long;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    const-string v0, "key == null"

    .line 122
    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue == null"

    .line 123
    invoke-static {p2, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v2, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/f2prateek/rx/preferences2/LongAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/LongAdapter;

    iget-object v6, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public getObject(Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/Preference$Converter;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TT;",
            "Lcom/f2prateek/rx/preferences2/Preference$Converter<",
            "TT;>;)",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "key == null"

    .line 132
    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue == null"

    .line 133
    invoke-static {p2, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "converter == null"

    .line 134
    invoke-static {p3, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v2, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    new-instance v5, Lcom/f2prateek/rx/preferences2/ConverterAdapter;

    invoke-direct {v5, p3}, Lcom/f2prateek/rx/preferences2/ConverterAdapter;-><init>(Lcom/f2prateek/rx/preferences2/Preference$Converter;)V

    iget-object v6, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public getString(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, ""

    .line 142
    invoke-virtual {p0, p1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    return-object p1
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "key == null"

    .line 148
    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue == null"

    .line 149
    invoke-static {p2, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v2, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/f2prateek/rx/preferences2/StringAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/StringAdapter;

    iget-object v6, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method

.method public getStringSet(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 160
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p1

    return-object p1
.end method

.method public getStringSet(Ljava/lang/String;Ljava/util/Set;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const-string v0, "key == null"

    .line 168
    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultValue == null"

    .line 169
    invoke-static {p2, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference;

    iget-object v2, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->preferences:Landroid/content/SharedPreferences;

    sget-object v5, Lcom/f2prateek/rx/preferences2/StringSetAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/StringSetAdapter;

    iget-object v6, p0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->keyChanges:Lio/reactivex/Observable;

    move-object v1, v0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/f2prateek/rx/preferences2/RealPreference;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;Lio/reactivex/Observable;)V

    return-object v0
.end method
