.class final Lcom/squareup/orders/CreateOrderRequest$Modifier$ProtoAdapter_Modifier;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/CreateOrderRequest$Modifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Modifier"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/CreateOrderRequest$Modifier;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1502
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/CreateOrderRequest$Modifier;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/CreateOrderRequest$Modifier;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1523
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;-><init>()V

    .line 1524
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1525
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 1531
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1529
    :cond_0
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->base_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;

    goto :goto_0

    .line 1528
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->name(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;

    goto :goto_0

    .line 1527
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;

    goto :goto_0

    .line 1535
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1536
    invoke-virtual {v0}, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$Modifier;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1500
    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$Modifier$ProtoAdapter_Modifier;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/CreateOrderRequest$Modifier;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/CreateOrderRequest$Modifier;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1515
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$Modifier;->catalog_object_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1516
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$Modifier;->name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1517
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1518
    invoke-virtual {p2}, Lcom/squareup/orders/CreateOrderRequest$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1500
    check-cast p2, Lcom/squareup/orders/CreateOrderRequest$Modifier;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/CreateOrderRequest$Modifier$ProtoAdapter_Modifier;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/CreateOrderRequest$Modifier;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/CreateOrderRequest$Modifier;)I
    .locals 4

    .line 1507
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/CreateOrderRequest$Modifier;->catalog_object_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$Modifier;->name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 1508
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$Modifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v3, 0x3

    .line 1509
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1510
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1500
    check-cast p1, Lcom/squareup/orders/CreateOrderRequest$Modifier;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$Modifier$ProtoAdapter_Modifier;->encodedSize(Lcom/squareup/orders/CreateOrderRequest$Modifier;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/CreateOrderRequest$Modifier;)Lcom/squareup/orders/CreateOrderRequest$Modifier;
    .locals 2

    .line 1541
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Modifier;->newBuilder()Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;

    move-result-object p1

    .line 1542
    iget-object v0, p1, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 1543
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1544
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$Modifier;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1500
    check-cast p1, Lcom/squareup/orders/CreateOrderRequest$Modifier;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$Modifier$ProtoAdapter_Modifier;->redact(Lcom/squareup/orders/CreateOrderRequest$Modifier;)Lcom/squareup/orders/CreateOrderRequest$Modifier;

    move-result-object p1

    return-object p1
.end method
