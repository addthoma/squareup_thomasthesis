.class public final Lcom/squareup/orders/model/Order$Source$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$Source;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$Source;",
        "Lcom/squareup/orders/model/Order$Source$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public application_id:Ljava/lang/String;

.field public application_name:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1742
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public application_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Source$Builder;
    .locals 0

    .line 1778
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Source$Builder;->application_id:Ljava/lang/String;

    return-object p0
.end method

.method public application_name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Source$Builder;
    .locals 0

    .line 1765
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Source$Builder;->application_name:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$Source;
    .locals 5

    .line 1784
    new-instance v0, Lcom/squareup/orders/model/Order$Source;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Source$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$Source$Builder;->application_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$Source$Builder;->application_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/orders/model/Order$Source;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1735
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Source$Builder;->build()Lcom/squareup/orders/model/Order$Source;

    move-result-object v0

    return-object v0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Source$Builder;
    .locals 0

    .line 1752
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Source$Builder;->name:Ljava/lang/String;

    return-object p0
.end method
