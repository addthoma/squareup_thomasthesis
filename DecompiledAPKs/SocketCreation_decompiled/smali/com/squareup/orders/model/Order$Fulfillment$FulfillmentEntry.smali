.class public final Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$Fulfillment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FulfillmentEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$ProtoAdapter_FulfillmentEntry;,
        Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;",
        "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LINE_ITEM_UID:Ljava/lang/String; = ""

.field public static final DEFAULT_QUANTITY:Ljava/lang/String; = ""

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final line_item_uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final metadata:Ljava/util/Map;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        keyAdapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7179
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$ProtoAdapter_FulfillmentEntry;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$ProtoAdapter_FulfillmentEntry;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 7253
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 7258
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 7259
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->uid:Ljava/lang/String;

    .line 7260
    iput-object p2, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->line_item_uid:Ljava/lang/String;

    .line 7261
    iput-object p3, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    const-string p1, "metadata"

    .line 7262
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->metadata:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 7279
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 7280
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;

    .line 7281
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->uid:Ljava/lang/String;

    .line 7282
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->line_item_uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->line_item_uid:Ljava/lang/String;

    .line 7283
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    .line 7284
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->metadata:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->metadata:Ljava/util/Map;

    .line 7285
    invoke-interface {v1, p1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 7290
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 7292
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 7293
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7294
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->line_item_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7295
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 7296
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 7297
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;
    .locals 2

    .line 7267
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;-><init>()V

    .line 7268
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->uid:Ljava/lang/String;

    .line 7269
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->line_item_uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->line_item_uid:Ljava/lang/String;

    .line 7270
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->quantity:Ljava/lang/String;

    .line 7271
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->metadata:Ljava/util/Map;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->metadata:Ljava/util/Map;

    .line 7272
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 7178
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->newBuilder()Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 7304
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7305
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7306
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->line_item_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", line_item_uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->line_item_uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7307
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 7308
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", metadata=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FulfillmentEntry{"

    .line 7309
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
