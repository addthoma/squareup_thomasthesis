.class public final Lcom/squareup/orders/model/Order$ReturnTip;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReturnTip"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$ReturnTip$ProtoAdapter_ReturnTip;,
        Lcom/squareup/orders/model/Order$ReturnTip$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$ReturnTip;",
        "Lcom/squareup/orders/model/Order$ReturnTip$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$ReturnTip;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SOURCE_TIP_UID:Ljava/lang/String; = ""

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final applied_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final source_tip_uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14945
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnTip$ProtoAdapter_ReturnTip;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReturnTip$ProtoAdapter_ReturnTip;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnTip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;)V
    .locals 1

    .line 14982
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/orders/model/Order$ReturnTip;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 14987
    sget-object v0, Lcom/squareup/orders/model/Order$ReturnTip;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 14988
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->uid:Ljava/lang/String;

    .line 14989
    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnTip;->source_tip_uid:Ljava/lang/String;

    .line 14990
    iput-object p3, p0, Lcom/squareup/orders/model/Order$ReturnTip;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 15006
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$ReturnTip;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 15007
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$ReturnTip;

    .line 15008
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnTip;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReturnTip;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnTip;->uid:Ljava/lang/String;

    .line 15009
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->source_tip_uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnTip;->source_tip_uid:Ljava/lang/String;

    .line 15010
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$ReturnTip;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 15011
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 15016
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 15018
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnTip;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 15019
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15020
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->source_tip_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15021
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 15022
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$ReturnTip$Builder;
    .locals 2

    .line 14995
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnTip$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReturnTip$Builder;-><init>()V

    .line 14996
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->uid:Ljava/lang/String;

    .line 14997
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->source_tip_uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->source_tip_uid:Ljava/lang/String;

    .line 14998
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 14999
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnTip;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 14944
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnTip;->newBuilder()Lcom/squareup/orders/model/Order$ReturnTip$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 15029
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 15030
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15031
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->source_tip_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", source_tip_uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->source_tip_uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15032
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnTip;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReturnTip{"

    .line 15033
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
