.class public final Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$ReturnLineItemModifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$ReturnLineItemModifier;",
        "Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public catalog_object_id:Ljava/lang/String;

.field public catalog_object_version:Ljava/lang/Long;

.field public name:Ljava/lang/String;

.field public source_modifier_uid:Ljava/lang/String;

.field public total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13239
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public base_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;
    .locals 0

    .line 13297
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$ReturnLineItemModifier;
    .locals 10

    .line 13316
    new-instance v9, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->uid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->source_modifier_uid:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->catalog_object_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->catalog_object_version:Ljava/lang/Long;

    iget-object v5, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->name:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v7, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 13224
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->build()Lcom/squareup/orders/model/Order$ReturnLineItemModifier;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;
    .locals 0

    .line 13269
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_object_version(Ljava/lang/Long;)Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;
    .locals 0

    .line 13274
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->catalog_object_version:Ljava/lang/Long;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;
    .locals 0

    .line 13284
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public source_modifier_uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;
    .locals 0

    .line 13259
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->source_modifier_uid:Ljava/lang/String;

    return-object p0
.end method

.method public total_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;
    .locals 0

    .line 13310
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;
    .locals 0

    .line 13248
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
