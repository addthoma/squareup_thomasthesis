.class public final enum Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;
.super Ljava/lang/Enum;
.source "Order.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$ServiceCharge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CalculationPhase"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase$ProtoAdapter_CalculationPhase;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum SERVICE_CHARGE_CALCULATION_PHASE_DO_NOT_USE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

.field public static final enum SUBTOTAL_PHASE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

.field public static final enum TOTAL_PHASE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 6452
    new-instance v0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    const/4 v1, 0x0

    const-string v2, "SERVICE_CHARGE_CALCULATION_PHASE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->SERVICE_CHARGE_CALCULATION_PHASE_DO_NOT_USE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    .line 6460
    new-instance v0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    const/4 v2, 0x1

    const-string v3, "SUBTOTAL_PHASE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->SUBTOTAL_PHASE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    .line 6468
    new-instance v0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    const/4 v3, 0x2

    const-string v4, "TOTAL_PHASE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->TOTAL_PHASE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    .line 6451
    sget-object v4, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->SERVICE_CHARGE_CALCULATION_PHASE_DO_NOT_USE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->SUBTOTAL_PHASE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->TOTAL_PHASE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->$VALUES:[Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    .line 6470
    new-instance v0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase$ProtoAdapter_CalculationPhase;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase$ProtoAdapter_CalculationPhase;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 6474
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 6475
    iput p3, p0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 6485
    :cond_0
    sget-object p0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->TOTAL_PHASE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    return-object p0

    .line 6484
    :cond_1
    sget-object p0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->SUBTOTAL_PHASE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    return-object p0

    .line 6483
    :cond_2
    sget-object p0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->SERVICE_CHARGE_CALCULATION_PHASE_DO_NOT_USE:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;
    .locals 1

    .line 6451
    const-class v0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;
    .locals 1

    .line 6451
    sget-object v0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->$VALUES:[Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    invoke-virtual {v0}, [Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 6492
    iget v0, p0, Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;->value:I

    return v0
.end method
