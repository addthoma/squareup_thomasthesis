.class final Lcom/squareup/orders/model/Order$FulfillmentRecipient$ProtoAdapter_FulfillmentRecipient;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$FulfillmentRecipient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FulfillmentRecipient"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$FulfillmentRecipient;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 8792
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$FulfillmentRecipient;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8819
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;-><init>()V

    .line 8820
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 8821
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 8830
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 8828
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/common/privacyvault/VaultedData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/privacyvault/VaultedData;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->vaulted_data(Lcom/squareup/protos/common/privacyvault/VaultedData;)Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;

    goto :goto_0

    .line 8827
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/connect/v2/resources/Address;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/resources/Address;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->address(Lcom/squareup/protos/connect/v2/resources/Address;)Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;

    goto :goto_0

    .line 8826
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->phone_number(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;

    goto :goto_0

    .line 8825
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->email_address(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;

    goto :goto_0

    .line 8824
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->display_name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;

    goto :goto_0

    .line 8823
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->customer_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;

    goto :goto_0

    .line 8834
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 8835
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8790
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$ProtoAdapter_FulfillmentRecipient;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$FulfillmentRecipient;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8808
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->customer_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8809
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->display_name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8810
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->email_address:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8811
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->phone_number:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8812
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Address;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8813
    sget-object v0, Lcom/squareup/protos/common/privacyvault/VaultedData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 8814
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 8790
    check-cast p2, Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$ProtoAdapter_FulfillmentRecipient;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$FulfillmentRecipient;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$FulfillmentRecipient;)I
    .locals 4

    .line 8797
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->customer_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->display_name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 8798
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->email_address:Ljava/lang/String;

    const/4 v3, 0x3

    .line 8799
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->phone_number:Ljava/lang/String;

    const/4 v3, 0x4

    .line 8800
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/resources/Address;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    const/4 v3, 0x5

    .line 8801
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/privacyvault/VaultedData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    const/4 v3, 0x6

    .line 8802
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8803
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 8790
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$ProtoAdapter_FulfillmentRecipient;->encodedSize(Lcom/squareup/orders/model/Order$FulfillmentRecipient;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$FulfillmentRecipient;)Lcom/squareup/orders/model/Order$FulfillmentRecipient;
    .locals 2

    .line 8840
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->newBuilder()Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 8841
    iput-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->display_name:Ljava/lang/String;

    .line 8842
    iput-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->email_address:Ljava/lang/String;

    .line 8843
    iput-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->phone_number:Ljava/lang/String;

    .line 8844
    iput-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    .line 8845
    iget-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/privacyvault/VaultedData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/privacyvault/VaultedData;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    .line 8846
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 8847
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 8790
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentRecipient$ProtoAdapter_FulfillmentRecipient;->redact(Lcom/squareup/orders/model/Order$FulfillmentRecipient;)Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    move-result-object p1

    return-object p1
.end method
