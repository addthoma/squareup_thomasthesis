.class public final Lcom/squareup/orders/model/Order$ReservedDiscountCode;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReservedDiscountCode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$ReservedDiscountCode$ProtoAdapter_ReservedDiscountCode;,
        Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$ReservedDiscountCode;",
        "Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$ReservedDiscountCode;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_PRICING_RULE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_QUANTITY:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final pricing_rule_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final quantity:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15263
    new-instance v0, Lcom/squareup/orders/model/Order$ReservedDiscountCode$ProtoAdapter_ReservedDiscountCode;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$ProtoAdapter_ReservedDiscountCode;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 15271
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->DEFAULT_QUANTITY:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 1

    .line 15301
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/orders/model/Order$ReservedDiscountCode;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 15306
    sget-object v0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 15307
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->id:Ljava/lang/String;

    .line 15308
    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->pricing_rule_id:Ljava/lang/String;

    .line 15309
    iput-object p3, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->quantity:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 15325
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$ReservedDiscountCode;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 15326
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$ReservedDiscountCode;

    .line 15327
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->id:Ljava/lang/String;

    .line 15328
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->pricing_rule_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->pricing_rule_id:Ljava/lang/String;

    .line 15329
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->quantity:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->quantity:Ljava/lang/Integer;

    .line 15330
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 15335
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 15337
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 15338
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15339
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->pricing_rule_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15340
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->quantity:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 15341
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;
    .locals 2

    .line 15314
    new-instance v0, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;-><init>()V

    .line 15315
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;->id:Ljava/lang/String;

    .line 15316
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->pricing_rule_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;->pricing_rule_id:Ljava/lang/String;

    .line 15317
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->quantity:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;->quantity:Ljava/lang/Integer;

    .line 15318
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 15262
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->newBuilder()Lcom/squareup/orders/model/Order$ReservedDiscountCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 15348
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 15349
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15350
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->pricing_rule_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", pricing_rule_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->pricing_rule_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15351
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->quantity:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedDiscountCode;->quantity:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReservedDiscountCode{"

    .line 15352
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
