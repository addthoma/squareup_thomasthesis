.class public final Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$MoneyAmounts;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$MoneyAmounts;",
        "Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public discount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public tax_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public tip_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public total_money:Lcom/squareup/protos/connect/v2/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10958
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/model/Order$MoneyAmounts;
    .locals 8

    .line 11013
    new-instance v7, Lcom/squareup/orders/model/Order$MoneyAmounts;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v5, p0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/orders/model/Order$MoneyAmounts;-><init>(Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 10947
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->build()Lcom/squareup/orders/model/Order$MoneyAmounts;

    move-result-object v0

    return-object v0
.end method

.method public discount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;
    .locals 0

    .line 10987
    iput-object p1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public service_charge_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;
    .locals 0

    .line 11007
    iput-object p1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public tax_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;
    .locals 0

    .line 10977
    iput-object p1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public tip_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;
    .locals 0

    .line 10997
    iput-object p1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;
    .locals 0

    .line 10967
    iput-object p1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method
