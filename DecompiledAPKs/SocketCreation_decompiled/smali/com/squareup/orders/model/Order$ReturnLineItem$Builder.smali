.class public final Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$ReturnLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$ReturnLineItem;",
        "Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public applied_discounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;",
            ">;"
        }
    .end annotation
.end field

.field public applied_taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;"
        }
    .end annotation
.end field

.field public base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public catalog_category_id:Ljava/lang/String;

.field public catalog_item_id:Ljava/lang/String;

.field public catalog_object_id:Ljava/lang/String;

.field public catalog_object_version:Ljava/lang/Long;

.field public category_name:Ljava/lang/String;

.field public gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

.field public name:Ljava/lang/String;

.field public note:Ljava/lang/String;

.field public quantity:Ljava/lang/String;

.field public quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

.field public return_discounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnDiscount;",
            ">;"
        }
    .end annotation
.end field

.field public return_modifiers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnLineItemModifier;",
            ">;"
        }
    .end annotation
.end field

.field public return_taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTax;",
            ">;"
        }
    .end annotation
.end field

.field public sku:Ljava/lang/String;

.field public source_line_item_uid:Ljava/lang/String;

.field public total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public total_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public uid:Ljava/lang/String;

.field public variation_name:Ljava/lang/String;

.field public variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 12605
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 12606
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_modifiers:Ljava/util/List;

    .line 12607
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_taxes:Ljava/util/List;

    .line 12608
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_discounts:Ljava/util/List;

    .line 12609
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->applied_taxes:Ljava/util/List;

    .line 12610
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->applied_discounts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public applied_discounts(Ljava/util/List;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;",
            ">;)",
            "Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;"
        }
    .end annotation

    .line 12830
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 12831
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->applied_discounts:Ljava/util/List;

    return-object p0
.end method

.method public applied_taxes(Ljava/util/List;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;)",
            "Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;"
        }
    .end annotation

    .line 12816
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 12817
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->applied_taxes:Ljava/util/List;

    return-object p0
.end method

.method public base_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12843
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$ReturnLineItem;
    .locals 2

    .line 12911
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnLineItem;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/orders/model/Order$ReturnLineItem;-><init>(Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 12554
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->build()Lcom/squareup/orders/model/Order$ReturnLineItem;

    move-result-object v0

    return-object v0
.end method

.method public catalog_category_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12723
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_category_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_item_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12713
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_item_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12686
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_object_version(Ljava/lang/Long;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12691
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_object_version:Ljava/lang/Long;

    return-object p0
.end method

.method public category_name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12754
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->category_name:Ljava/lang/String;

    return-object p0
.end method

.method public gross_return_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12869
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public item_type(Lcom/squareup/orders/model/Order$LineItem$ItemType;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12734
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12641
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12676
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public quantity(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12655
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->quantity:Ljava/lang/String;

    return-object p0
.end method

.method public quantity_unit(Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12666
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    return-object p0
.end method

.method public return_discounts(Ljava/util/List;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnDiscount;",
            ">;)",
            "Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;"
        }
    .end annotation

    .line 12802
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 12803
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_discounts:Ljava/util/List;

    return-object p0
.end method

.method public return_modifiers(Ljava/util/List;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnLineItemModifier;",
            ">;)",
            "Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;"
        }
    .end annotation

    .line 12764
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 12765
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_modifiers:Ljava/util/List;

    return-object p0
.end method

.method public return_taxes(Ljava/util/List;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTax;",
            ">;)",
            "Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;"
        }
    .end annotation

    .line 12783
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 12784
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_taxes:Ljava/util/List;

    return-object p0
.end method

.method public sku(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12744
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->sku:Ljava/lang/String;

    return-object p0
.end method

.method public source_line_item_uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12629
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->source_line_item_uid:Ljava/lang/String;

    return-object p0
.end method

.method public total_discount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12893
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12905
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public total_tax_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12881
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12619
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method

.method public variation_name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12703
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->variation_name:Ljava/lang/String;

    return-object p0
.end method

.method public variation_total_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 0

    .line 12857
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method
