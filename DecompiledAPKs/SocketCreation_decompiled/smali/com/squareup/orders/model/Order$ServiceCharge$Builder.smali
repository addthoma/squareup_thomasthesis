.class public final Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$ServiceCharge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$ServiceCharge;",
        "Lcom/squareup/orders/model/Order$ServiceCharge$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public applied_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public applied_taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;"
        }
    .end annotation
.end field

.field public calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

.field public catalog_object_id:Ljava/lang/String;

.field public catalog_object_version:Ljava/lang/Long;

.field public metadata:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;

.field public taxable:Ljava/lang/Boolean;

.field public taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Tax;",
            ">;"
        }
    .end annotation
.end field

.field public total_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public type:Lcom/squareup/orders/model/Order$ServiceCharge$Type;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 6224
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 6225
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->taxes:Ljava/util/List;

    .line 6226
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->applied_taxes:Ljava/util/List;

    .line 6227
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->metadata:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0

    .line 6285
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public applied_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0

    .line 6302
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public applied_taxes(Ljava/util/List;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;)",
            "Lcom/squareup/orders/model/Order$ServiceCharge$Builder;"
        }
    .end annotation

    .line 6392
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 6393
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->applied_taxes:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$ServiceCharge;
    .locals 20

    move-object/from16 v0, p0

    .line 6439
    new-instance v18, Lcom/squareup/orders/model/Order$ServiceCharge;

    move-object/from16 v1, v18

    iget-object v2, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->uid:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->catalog_object_id:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->catalog_object_version:Ljava/lang/Long;

    iget-object v6, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->percentage:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v8, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v9, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v10, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v11, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    iget-object v12, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->taxable:Ljava/lang/Boolean;

    iget-object v13, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->taxes:Ljava/util/List;

    iget-object v14, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->applied_taxes:Ljava/util/List;

    iget-object v15, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->metadata:Ljava/util/Map;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->type:Lcom/squareup/orders/model/Order$ServiceCharge$Type;

    move-object/from16 v16, v1

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v17

    move-object/from16 v1, v19

    invoke-direct/range {v1 .. v17}, Lcom/squareup/orders/model/Order$ServiceCharge;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;Ljava/lang/Boolean;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Lcom/squareup/orders/model/Order$ServiceCharge$Type;Lokio/ByteString;)V

    return-object v18
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 6193
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->build()Lcom/squareup/orders/model/Order$ServiceCharge;

    move-result-object v0

    return-object v0
.end method

.method public calculation_phase(Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0

    .line 6339
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->calculation_phase:Lcom/squareup/orders/model/Order$ServiceCharge$CalculationPhase;

    return-object p0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0

    .line 6252
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_object_version(Ljava/lang/Long;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0

    .line 6260
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->catalog_object_version:Ljava/lang/Long;

    return-object p0
.end method

.method public metadata(Ljava/util/Map;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/orders/model/Order$ServiceCharge$Builder;"
        }
    .end annotation

    .line 6420
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/Map;)V

    .line 6421
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->metadata:Ljava/util/Map;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0

    .line 6244
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0

    .line 6273
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method

.method public taxable(Ljava/lang/Boolean;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0

    .line 6351
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->taxable:Ljava/lang/Boolean;

    return-object p0
.end method

.method public taxes(Ljava/util/List;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Tax;",
            ">;)",
            "Lcom/squareup/orders/model/Order$ServiceCharge$Builder;"
        }
    .end annotation

    .line 6370
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 6371
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->taxes:Ljava/util/List;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0

    .line 6319
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public total_tax_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0

    .line 6331
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public type(Lcom/squareup/orders/model/Order$ServiceCharge$Type;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0

    .line 6433
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->type:Lcom/squareup/orders/model/Order$ServiceCharge$Type;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ServiceCharge$Builder;
    .locals 0

    .line 6236
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ServiceCharge$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
