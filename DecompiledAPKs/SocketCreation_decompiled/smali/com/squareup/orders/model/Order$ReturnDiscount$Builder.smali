.class public final Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$ReturnDiscount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$ReturnDiscount;",
        "Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public applied_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public catalog_object_id:Ljava/lang/String;

.field public catalog_object_version:Ljava/lang/Long;

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;

.field public scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

.field public source_discount_uid:Ljava/lang/String;

.field public type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14737
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;
    .locals 0

    .line 14819
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public applied_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;
    .locals 0

    .line 14831
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$ReturnDiscount;
    .locals 13

    .line 14850
    new-instance v12, Lcom/squareup/orders/model/Order$ReturnDiscount;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->uid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->source_discount_uid:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->catalog_object_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->catalog_object_version:Ljava/lang/Long;

    iget-object v5, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->name:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    iget-object v7, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->percentage:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v9, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v10, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/orders/model/Order$ReturnDiscount;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Discount$Type;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 14716
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->build()Lcom/squareup/orders/model/Order$ReturnDiscount;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;
    .locals 0

    .line 14766
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_object_version(Ljava/lang/Long;)Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;
    .locals 0

    .line 14771
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->catalog_object_version:Ljava/lang/Long;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;
    .locals 0

    .line 14781
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;
    .locals 0

    .line 14807
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method

.method public scope(Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;)Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;
    .locals 0

    .line 14844
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    return-object p0
.end method

.method public source_discount_uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;
    .locals 0

    .line 14756
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->source_discount_uid:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/orders/model/Order$LineItem$Discount$Type;)Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;
    .locals 0

    .line 14794
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;
    .locals 0

    .line 14746
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnDiscount$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
