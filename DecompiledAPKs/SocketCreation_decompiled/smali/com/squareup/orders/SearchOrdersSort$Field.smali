.class public final enum Lcom/squareup/orders/SearchOrdersSort$Field;
.super Ljava/lang/Enum;
.source "SearchOrdersSort.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/SearchOrdersSort;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Field"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/SearchOrdersSort$Field$ProtoAdapter_Field;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orders/SearchOrdersSort$Field;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orders/SearchOrdersSort$Field;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/SearchOrdersSort$Field;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CLOSED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

.field public static final enum CREATED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

.field public static final enum DO_NOT_USE:Lcom/squareup/orders/SearchOrdersSort$Field;

.field public static final enum UPDATED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 166
    new-instance v0, Lcom/squareup/orders/SearchOrdersSort$Field;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/orders/SearchOrdersSort$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/SearchOrdersSort$Field;->DO_NOT_USE:Lcom/squareup/orders/SearchOrdersSort$Field;

    .line 175
    new-instance v0, Lcom/squareup/orders/SearchOrdersSort$Field;

    const/4 v2, 0x1

    const-string v3, "CREATED_AT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/orders/SearchOrdersSort$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/SearchOrdersSort$Field;->CREATED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

    .line 184
    new-instance v0, Lcom/squareup/orders/SearchOrdersSort$Field;

    const/4 v3, 0x2

    const-string v4, "UPDATED_AT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/orders/SearchOrdersSort$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/SearchOrdersSort$Field;->UPDATED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

    .line 194
    new-instance v0, Lcom/squareup/orders/SearchOrdersSort$Field;

    const/4 v4, 0x3

    const-string v5, "CLOSED_AT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/orders/SearchOrdersSort$Field;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/SearchOrdersSort$Field;->CLOSED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/orders/SearchOrdersSort$Field;

    .line 165
    sget-object v5, Lcom/squareup/orders/SearchOrdersSort$Field;->DO_NOT_USE:Lcom/squareup/orders/SearchOrdersSort$Field;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/orders/SearchOrdersSort$Field;->CREATED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orders/SearchOrdersSort$Field;->UPDATED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/orders/SearchOrdersSort$Field;->CLOSED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/orders/SearchOrdersSort$Field;->$VALUES:[Lcom/squareup/orders/SearchOrdersSort$Field;

    .line 196
    new-instance v0, Lcom/squareup/orders/SearchOrdersSort$Field$ProtoAdapter_Field;

    invoke-direct {v0}, Lcom/squareup/orders/SearchOrdersSort$Field$ProtoAdapter_Field;-><init>()V

    sput-object v0, Lcom/squareup/orders/SearchOrdersSort$Field;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 200
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 201
    iput p3, p0, Lcom/squareup/orders/SearchOrdersSort$Field;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/orders/SearchOrdersSort$Field;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 212
    :cond_0
    sget-object p0, Lcom/squareup/orders/SearchOrdersSort$Field;->CLOSED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

    return-object p0

    .line 211
    :cond_1
    sget-object p0, Lcom/squareup/orders/SearchOrdersSort$Field;->UPDATED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

    return-object p0

    .line 210
    :cond_2
    sget-object p0, Lcom/squareup/orders/SearchOrdersSort$Field;->CREATED_AT:Lcom/squareup/orders/SearchOrdersSort$Field;

    return-object p0

    .line 209
    :cond_3
    sget-object p0, Lcom/squareup/orders/SearchOrdersSort$Field;->DO_NOT_USE:Lcom/squareup/orders/SearchOrdersSort$Field;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orders/SearchOrdersSort$Field;
    .locals 1

    .line 165
    const-class v0, Lcom/squareup/orders/SearchOrdersSort$Field;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orders/SearchOrdersSort$Field;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orders/SearchOrdersSort$Field;
    .locals 1

    .line 165
    sget-object v0, Lcom/squareup/orders/SearchOrdersSort$Field;->$VALUES:[Lcom/squareup/orders/SearchOrdersSort$Field;

    invoke-virtual {v0}, [Lcom/squareup/orders/SearchOrdersSort$Field;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orders/SearchOrdersSort$Field;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 219
    iget v0, p0, Lcom/squareup/orders/SearchOrdersSort$Field;->value:I

    return v0
.end method
