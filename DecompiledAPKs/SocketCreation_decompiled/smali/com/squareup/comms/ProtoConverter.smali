.class public Lcom/squareup/comms/ProtoConverter;
.super Ljava/lang/Object;
.source "ProtoConverter.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromProto(Lcom/squareup/comms/protos/common/Card$Brand;)Lcom/squareup/Card$Brand;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 24
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/comms/protos/common/Card$Brand;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/Card$Brand;->valueOf(Ljava/lang/String;)Lcom/squareup/Card$Brand;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static fromProto(Lcom/squareup/comms/protos/common/Card$InputType;)Lcom/squareup/Card$InputType;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 33
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/comms/protos/common/Card$InputType;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/Card$InputType;->valueOf(Ljava/lang/String;)Lcom/squareup/Card$InputType;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static fromProto(Lcom/squareup/comms/protos/common/Card;)Lcom/squareup/Card;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 56
    :cond_0
    new-instance v0, Lcom/squareup/Card$Builder;

    invoke-direct {v0}, Lcom/squareup/Card$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/comms/protos/common/Card;->track_data:Ljava/lang/String;

    .line 57
    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->trackData(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/protos/common/Card;->pan:Ljava/lang/String;

    .line 58
    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/protos/common/Card;->country_code:Ljava/lang/String;

    .line 59
    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->countryCode(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/protos/common/Card;->name:Ljava/lang/String;

    .line 60
    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->name(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/protos/common/Card;->title:Ljava/lang/String;

    .line 61
    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->title(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/protos/common/Card;->expiration_month:Ljava/lang/String;

    .line 62
    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->expirationMonth(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/protos/common/Card;->expiration_year:Ljava/lang/String;

    .line 63
    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->expirationYear(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/protos/common/Card;->service_code:Ljava/lang/String;

    .line 64
    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->serviceCode(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/protos/common/Card;->pin_verification:Ljava/lang/String;

    .line 65
    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->pinVerification(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/protos/common/Card;->discretionary_data:Ljava/lang/String;

    .line 66
    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->discretionaryData(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/protos/common/Card;->verification:Ljava/lang/String;

    .line 67
    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->verification(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/protos/common/Card;->postal_code:Ljava/lang/String;

    .line 68
    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->postalCode(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/protos/common/Card;->brand:Lcom/squareup/comms/protos/common/Card$Brand;

    .line 69
    invoke-static {v1}, Lcom/squareup/comms/ProtoConverter;->fromProto(Lcom/squareup/comms/protos/common/Card$Brand;)Lcom/squareup/Card$Brand;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object v0

    iget-object p0, p0, Lcom/squareup/comms/protos/common/Card;->input_type:Lcom/squareup/comms/protos/common/Card$InputType;

    .line 70
    invoke-static {p0}, Lcom/squareup/comms/ProtoConverter;->fromProto(Lcom/squareup/comms/protos/common/Card$InputType;)Lcom/squareup/Card$InputType;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/Card$Builder;->inputType(Lcom/squareup/Card$InputType;)Lcom/squareup/Card$Builder;

    move-result-object p0

    .line 71
    invoke-virtual {p0}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static fromProto(Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;)Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->getValue()I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static fromReaderProto(Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;)Lcom/squareup/comms/protos/buyer/ProcessMessageFromReader;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 97
    :cond_0
    new-instance v0, Lcom/squareup/comms/protos/buyer/ProcessMessageFromReader$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/ProcessMessageFromReader$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 99
    invoke-virtual {v1, p0}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p0

    invoke-static {p0}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p0

    .line 98
    invoke-virtual {v0, p0}, Lcom/squareup/comms/protos/buyer/ProcessMessageFromReader$Builder;->process_message_from_reader(Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/ProcessMessageFromReader$Builder;

    move-result-object p0

    .line 101
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/ProcessMessageFromReader$Builder;->build()Lcom/squareup/comms/protos/buyer/ProcessMessageFromReader;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static fromReaderProto(Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;)Lcom/squareup/comms/protos/seller/SendMessageToReader;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 116
    :cond_0
    new-instance v0, Lcom/squareup/comms/protos/seller/SendMessageToReader$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/seller/SendMessageToReader$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 118
    invoke-virtual {v1, p0}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p0

    invoke-static {p0}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p0

    .line 117
    invoke-virtual {v0, p0}, Lcom/squareup/comms/protos/seller/SendMessageToReader$Builder;->send_message_to_reader(Lokio/ByteString;)Lcom/squareup/comms/protos/seller/SendMessageToReader$Builder;

    move-result-object p0

    .line 120
    invoke-virtual {p0}, Lcom/squareup/comms/protos/seller/SendMessageToReader$Builder;->build()Lcom/squareup/comms/protos/seller/SendMessageToReader;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static toProto(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 77
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->swigValue()I

    move-result p0

    invoke-static {p0}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->fromValue(I)Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static toProto(Lcom/squareup/Card$Brand;)Lcom/squareup/comms/protos/common/Card$Brand;
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 19
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/Card$Brand;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/comms/protos/common/Card$Brand;->valueOf(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Brand;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method static toProto(Lcom/squareup/Card$InputType;)Lcom/squareup/comms/protos/common/Card$InputType;
    .locals 0

    .line 28
    invoke-virtual {p0}, Lcom/squareup/Card$InputType;->name()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/comms/protos/common/Card$InputType;->valueOf(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$InputType;

    move-result-object p0

    return-object p0
.end method

.method public static toProto(Lcom/squareup/Card;)Lcom/squareup/comms/protos/common/Card;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto/16 :goto_0

    .line 37
    :cond_0
    new-instance v0, Lcom/squareup/comms/protos/common/Card$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/common/Card$Builder;-><init>()V

    .line 38
    invoke-virtual {p0}, Lcom/squareup/Card;->getTrackData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/Card$Builder;->track_data(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;

    move-result-object v0

    .line 39
    invoke-virtual {p0}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;

    move-result-object v0

    .line 40
    invoke-virtual {p0}, Lcom/squareup/Card;->getCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/Card$Builder;->country_code(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;

    move-result-object v0

    .line 41
    invoke-virtual {p0}, Lcom/squareup/Card;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/Card$Builder;->name(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;

    move-result-object v0

    .line 42
    invoke-virtual {p0}, Lcom/squareup/Card;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/Card$Builder;->title(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;

    move-result-object v0

    .line 43
    invoke-virtual {p0}, Lcom/squareup/Card;->getExpirationYear()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/Card$Builder;->expiration_year(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;

    move-result-object v0

    .line 44
    invoke-virtual {p0}, Lcom/squareup/Card;->getExpirationMonth()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/Card$Builder;->expiration_month(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;

    move-result-object v0

    .line 45
    invoke-virtual {p0}, Lcom/squareup/Card;->getServiceCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/Card$Builder;->service_code(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;

    move-result-object v0

    .line 46
    invoke-virtual {p0}, Lcom/squareup/Card;->getPinVerification()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/Card$Builder;->pin_verification(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;

    move-result-object v0

    .line 47
    invoke-virtual {p0}, Lcom/squareup/Card;->getDiscretionaryData()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/Card$Builder;->discretionary_data(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;

    move-result-object v0

    .line 48
    invoke-virtual {p0}, Lcom/squareup/Card;->getVerification()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/Card$Builder;->verification(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;

    move-result-object v0

    .line 49
    invoke-virtual {p0}, Lcom/squareup/Card;->getPostalCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/Card$Builder;->postal_code(Ljava/lang/String;)Lcom/squareup/comms/protos/common/Card$Builder;

    move-result-object v0

    .line 50
    invoke-virtual {p0}, Lcom/squareup/Card;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/comms/ProtoConverter;->toProto(Lcom/squareup/Card$Brand;)Lcom/squareup/comms/protos/common/Card$Brand;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/common/Card$Builder;->brand(Lcom/squareup/comms/protos/common/Card$Brand;)Lcom/squareup/comms/protos/common/Card$Builder;

    move-result-object v0

    .line 51
    invoke-virtual {p0}, Lcom/squareup/Card;->getInputType()Lcom/squareup/Card$InputType;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/comms/ProtoConverter;->toProto(Lcom/squareup/Card$InputType;)Lcom/squareup/comms/protos/common/Card$InputType;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/comms/protos/common/Card$Builder;->input_type(Lcom/squareup/comms/protos/common/Card$InputType;)Lcom/squareup/comms/protos/common/Card$Builder;

    move-result-object p0

    .line 52
    invoke-virtual {p0}, Lcom/squareup/comms/protos/common/Card$Builder;->build()Lcom/squareup/comms/protos/common/Card;

    move-result-object p0

    :goto_0
    return-object p0
.end method

.method public static toReaderProto(Lcom/squareup/comms/protos/buyer/ProcessMessageFromReader;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 88
    :cond_0
    :try_start_0
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object p0, p0, Lcom/squareup/comms/protos/buyer/ProcessMessageFromReader;->process_message_from_reader:Lokio/ByteString;

    .line 89
    invoke-virtual {p0}, Lokio/ByteString;->toByteArray()[B

    move-result-object p0

    .line 88
    invoke-virtual {v0, p0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object p0

    :catch_0
    move-exception p0

    .line 91
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid ProcessMessageFromReader."

    invoke-direct {v0, v1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method public static toReaderProto(Lcom/squareup/comms/protos/seller/SendMessageToReader;)Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 107
    :cond_0
    :try_start_0
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object p0, p0, Lcom/squareup/comms/protos/seller/SendMessageToReader;->send_message_to_reader:Lokio/ByteString;

    .line 108
    invoke-virtual {p0}, Lokio/ByteString;->toByteArray()[B

    move-result-object p0

    .line 107
    invoke-virtual {v0, p0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/protos/ReaderProtos$SendMessageToReader;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object p0

    :catch_0
    move-exception p0

    .line 110
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid SendMessageToReader."

    invoke-direct {v0, v1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method
