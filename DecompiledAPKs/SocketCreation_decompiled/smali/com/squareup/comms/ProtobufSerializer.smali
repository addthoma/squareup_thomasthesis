.class public Lcom/squareup/comms/ProtobufSerializer;
.super Ljava/lang/Object;
.source "ProtobufSerializer.java"

# interfaces
.implements Lcom/squareup/comms/Serializer;


# static fields
.field public static final MAGIC_INT:I = 0xbb

.field private static final SIZE_HEADER:I = 0x8


# instance fields
.field public inputBuffer:[B

.field private readPos:I

.field private writePos:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-array p1, p1, [B

    iput-object p1, p0, Lcom/squareup/comms/ProtobufSerializer;->inputBuffer:[B

    return-void
.end method

.method private appendToInputBuffer([BII)V
    .locals 5

    .line 107
    iget-object v0, p0, Lcom/squareup/comms/ProtobufSerializer;->inputBuffer:[B

    array-length v1, v0

    iget v2, p0, Lcom/squareup/comms/ProtobufSerializer;->writePos:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, p3

    if-gez v1, :cond_1

    .line 108
    iget v1, p0, Lcom/squareup/comms/ProtobufSerializer;->readPos:I

    sub-int/2addr v2, v1

    .line 112
    array-length v1, v0

    add-int v3, v2, p3

    if-ge v1, v3, :cond_0

    .line 113
    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 114
    new-array v0, v0, [B

    .line 121
    :cond_0
    iget-object v1, p0, Lcom/squareup/comms/ProtobufSerializer;->inputBuffer:[B

    iget v3, p0, Lcom/squareup/comms/ProtobufSerializer;->readPos:I

    const/4 v4, 0x0

    invoke-static {v1, v3, v0, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 122
    iput v2, p0, Lcom/squareup/comms/ProtobufSerializer;->writePos:I

    .line 123
    iput v4, p0, Lcom/squareup/comms/ProtobufSerializer;->readPos:I

    .line 125
    iput-object v0, p0, Lcom/squareup/comms/ProtobufSerializer;->inputBuffer:[B

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/squareup/comms/ProtobufSerializer;->inputBuffer:[B

    iget v1, p0, Lcom/squareup/comms/ProtobufSerializer;->writePos:I

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130
    iget p1, p0, Lcom/squareup/comms/ProtobufSerializer;->writePos:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/squareup/comms/ProtobufSerializer;->writePos:I

    return-void
.end method


# virtual methods
.method public consume([BII)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII)",
            "Ljava/util/List<",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation

    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/comms/ProtobufSerializer;->appendToInputBuffer([BII)V

    .line 61
    new-instance p1, Ljava/io/ByteArrayInputStream;

    iget-object p2, p0, Lcom/squareup/comms/ProtobufSerializer;->inputBuffer:[B

    iget p3, p0, Lcom/squareup/comms/ProtobufSerializer;->readPos:I

    iget v0, p0, Lcom/squareup/comms/ProtobufSerializer;->writePos:I

    sub-int/2addr v0, p3

    invoke-direct {p1, p2, p3, v0}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 62
    new-instance p2, Ljava/io/DataInputStream;

    invoke-direct {p2, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 64
    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 68
    :goto_0
    :try_start_0
    invoke-virtual {p2}, Ljava/io/DataInputStream;->available()I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    goto :goto_1

    .line 72
    :cond_0
    invoke-virtual {p2}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    const/16 v1, 0xbb

    if-ne v0, v1, :cond_3

    .line 74
    invoke-virtual {p2}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 75
    invoke-virtual {p2}, Ljava/io/DataInputStream;->available()I

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ge v1, v0, :cond_1

    .line 95
    :goto_1
    invoke-static {p1}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    .line 96
    invoke-static {p2}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    return-object p3

    .line 79
    :cond_1
    :try_start_1
    new-array v1, v0, [B

    const/4 v2, 0x0

    .line 80
    invoke-virtual {p2, v1, v2, v0}, Ljava/io/DataInputStream;->read([BII)I

    move-result v2

    if-ne v2, v0, :cond_2

    .line 84
    iget v2, p0, Lcom/squareup/comms/ProtobufSerializer;->readPos:I

    add-int/lit8 v0, v0, 0x8

    add-int/2addr v2, v0

    iput v2, p0, Lcom/squareup/comms/ProtobufSerializer;->readPos:I

    .line 86
    sget-object v0, Lcom/squareup/comms/protos/wrapper/Envelope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/comms/protos/wrapper/Envelope;

    .line 87
    invoke-static {v0}, Lcom/squareup/comms/MessageWrapper;->unwrap(Lcom/squareup/comms/protos/wrapper/Envelope;)Lcom/squareup/wire/Message;

    move-result-object v0

    .line 88
    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 82
    :cond_2
    new-instance p3, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected result"

    invoke-direct {p3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p3

    .line 72
    :cond_3
    new-instance p3, Ljava/lang/RuntimeException;

    const-string v0, "Invalid header."

    invoke-direct {p3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p3
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p3

    goto :goto_2

    :catch_0
    move-exception p3

    .line 93
    :try_start_2
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 95
    :goto_2
    invoke-static {p1}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    .line 96
    invoke-static {p2}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    .line 97
    throw p3
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    .line 101
    iput v0, p0, Lcom/squareup/comms/ProtobufSerializer;->writePos:I

    .line 102
    iput v0, p0, Lcom/squareup/comms/ProtobufSerializer;->readPos:I

    return-void
.end method

.method public serialize(Lcom/squareup/wire/Message;)[B
    .locals 4

    .line 37
    invoke-static {p1}, Lcom/squareup/comms/MessageWrapper;->wrap(Lcom/squareup/wire/Message;)Lcom/squareup/comms/protos/wrapper/Envelope;

    move-result-object p1

    .line 39
    sget-object v0, Lcom/squareup/comms/protos/wrapper/Envelope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p1

    .line 40
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 41
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v2, 0xbb

    .line 44
    :try_start_0
    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 45
    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    const/4 v2, 0x0

    .line 46
    array-length v3, p1

    invoke-virtual {v1, p1, v2, v3}, Ljava/io/DataOutputStream;->write([BII)V

    .line 47
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 49
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    invoke-static {v0}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    .line 54
    invoke-static {v1}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    .line 51
    :try_start_1
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 53
    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    .line 54
    invoke-static {v1}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    .line 55
    throw p1
.end method
