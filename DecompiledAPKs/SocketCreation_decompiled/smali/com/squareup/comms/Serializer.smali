.class public interface abstract Lcom/squareup/comms/Serializer;
.super Ljava/lang/Object;
.source "Serializer.java"


# virtual methods
.method public abstract consume([BII)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII)",
            "Ljava/util/List<",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation
.end method

.method public abstract reset()V
.end method

.method public abstract serialize(Lcom/squareup/wire/Message;)[B
.end method
