.class public Lcom/squareup/comms/FilteringRemoteBusConnection;
.super Ljava/lang/Object;
.source "FilteringRemoteBusConnection.java"

# interfaces
.implements Lcom/squareup/comms/RemoteBusConnection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;
    }
.end annotation


# instance fields
.field private final observable:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation
.end field

.field private final pending:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;",
            ">;"
        }
    .end annotation
.end field

.field private final poster:Lcom/squareup/comms/MessagePoster;


# direct methods
.method public constructor <init>(Lcom/squareup/comms/RemoteBusConnection;)V
    .locals 1

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/squareup/comms/FilteringRemoteBusConnection;->pending:Ljava/util/Queue;

    .line 73
    iput-object p1, p0, Lcom/squareup/comms/FilteringRemoteBusConnection;->poster:Lcom/squareup/comms/MessagePoster;

    .line 74
    invoke-interface {p1}, Lcom/squareup/comms/RemoteBusConnection;->observable()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/comms/-$$Lambda$FilteringRemoteBusConnection$tK1AfzYu7MF9a3nbHbBlLBoJhVM;

    invoke-direct {v0, p0}, Lcom/squareup/comms/-$$Lambda$FilteringRemoteBusConnection$tK1AfzYu7MF9a3nbHbBlLBoJhVM;-><init>(Lcom/squareup/comms/FilteringRemoteBusConnection;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/comms/-$$Lambda$FilteringRemoteBusConnection$SFyF8m4Mxl7wKhcw4xkQa43F0kk;->INSTANCE:Lcom/squareup/comms/-$$Lambda$FilteringRemoteBusConnection$SFyF8m4Mxl7wKhcw4xkQa43F0kk;

    .line 94
    invoke-virtual {p1, v0}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/FilteringRemoteBusConnection;->observable:Lrx/Observable;

    return-void
.end method

.method private static asList(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;[TE;)",
            "Ljava/util/List<",
            "TE;>;"
        }
    .end annotation

    .line 125
    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 126
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method static synthetic lambda$new$1(Lcom/squareup/wire/Message;)Ljava/lang/Boolean;
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 94
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method public static messageString(Lcom/squareup/wire/Message;)Ljava/lang/String;
    .locals 0

    .line 121
    invoke-virtual {p0}, Lcom/squareup/wire/Message;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public synthetic lambda$new$0$FilteringRemoteBusConnection(Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;
    .locals 6

    .line 76
    instance-of v0, p1, Lcom/squareup/comms/protos/buyer/MessageReceived;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/squareup/comms/FilteringRemoteBusConnection;->pending:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;

    .line 79
    check-cast p1, Lcom/squareup/comms/protos/buyer/MessageReceived;

    .line 80
    invoke-virtual {v0}, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;->getMessageString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, Lcom/squareup/comms/protos/buyer/MessageReceived;->received_message_string:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    iget-object p1, p1, Lcom/squareup/comms/protos/buyer/MessageReceived;->received_message_string:Ljava/lang/String;

    aput-object p1, v5, v3

    .line 82
    invoke-virtual {v0}, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;->getMessageString()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v5, v2

    const/4 p1, 0x2

    iget-object v0, p0, Lcom/squareup/comms/FilteringRemoteBusConnection;->pending:Ljava/util/Queue;

    aput-object v0, v5, p1

    const-string p1, "Unexpected message: %s vs %s. Remaining: %s"

    .line 80
    invoke-static {v4, p1, v5}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-object v1

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/squareup/comms/FilteringRemoteBusConnection;->pending:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;

    .line 87
    invoke-virtual {v4, p1}, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;->ignore(Lcom/squareup/wire/Message;)Z

    move-result v4

    if-eqz v4, :cond_1

    new-array v0, v2, [Ljava/lang/Object;

    aput-object p1, v0, v3

    const-string p1, "Ignoring: %s"

    .line 88
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v1

    :cond_2
    return-object p1
.end method

.method public observable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/comms/FilteringRemoteBusConnection;->observable:Lrx/Observable;

    return-object v0
.end method

.method public post(Lcom/squareup/wire/Message;)V
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/comms/FilteringRemoteBusConnection;->pending:Ljava/util/Queue;

    invoke-static {p1}, Lcom/squareup/comms/FilteringRemoteBusConnection;->messageString(Lcom/squareup/wire/Message;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;->allowAll(Ljava/lang/String;)Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 99
    iget-object v0, p0, Lcom/squareup/comms/FilteringRemoteBusConnection;->poster:Lcom/squareup/comms/MessagePoster;

    invoke-interface {v0, p1}, Lcom/squareup/comms/MessagePoster;->post(Lcom/squareup/wire/Message;)V

    return-void
.end method

.method public final varargs postAndBlacklist(Lcom/squareup/wire/Message;Ljava/lang/Class;[Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/wire/Message;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/wire/Message;",
            ">;[",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/wire/Message;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    .line 108
    iget-object v0, p0, Lcom/squareup/comms/FilteringRemoteBusConnection;->pending:Ljava/util/Queue;

    invoke-static {p2, p3}, Lcom/squareup/comms/FilteringRemoteBusConnection;->asList(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-static {p1}, Lcom/squareup/comms/FilteringRemoteBusConnection;->messageString(Lcom/squareup/wire/Message;)Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;->blacklist(Ljava/util/List;Ljava/lang/String;)Lcom/squareup/comms/FilteringRemoteBusConnection$PendingAck;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object p2, p0, Lcom/squareup/comms/FilteringRemoteBusConnection;->poster:Lcom/squareup/comms/MessagePoster;

    invoke-interface {p2, p1}, Lcom/squareup/comms/MessagePoster;->post(Lcom/squareup/wire/Message;)V

    return-void
.end method
