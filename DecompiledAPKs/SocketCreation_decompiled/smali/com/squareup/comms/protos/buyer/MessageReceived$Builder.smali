.class public final Lcom/squareup/comms/protos/buyer/MessageReceived$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MessageReceived.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/MessageReceived;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/MessageReceived;",
        "Lcom/squareup/comms/protos/buyer/MessageReceived$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0006\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/MessageReceived$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/MessageReceived;",
        "()V",
        "received_message_string",
        "",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public received_message_string:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/MessageReceived;
    .locals 3

    .line 77
    new-instance v0, Lcom/squareup/comms/protos/buyer/MessageReceived;

    .line 78
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/MessageReceived$Builder;->received_message_string:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/MessageReceived$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    .line 77
    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/MessageReceived;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    return-object v0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "received_message_string"

    aput-object v2, v0, v1

    .line 79
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 68
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/MessageReceived$Builder;->build()Lcom/squareup/comms/protos/buyer/MessageReceived;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final received_message_string(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/MessageReceived$Builder;
    .locals 1

    const-string v0, "received_message_string"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/MessageReceived$Builder;->received_message_string:Ljava/lang/String;

    return-object p0
.end method
