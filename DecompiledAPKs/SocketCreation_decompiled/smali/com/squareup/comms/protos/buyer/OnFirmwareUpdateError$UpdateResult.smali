.class public final enum Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;
.super Ljava/lang/Enum;
.source "OnFirmwareUpdateError.kt"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UpdateResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0018\u0008\u0086\u0001\u0018\u0000 \u001b2\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u0001\u001bB\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019j\u0002\u0008\u001a\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;",
        "",
        "Lcom/squareup/wire/WireEnum;",
        "value",
        "",
        "(Ljava/lang/String;II)V",
        "getValue",
        "()I",
        "CRS_FWUP_RESULT_SUCCESS",
        "CRS_FWUP_RESULT_SEND_DATA",
        "CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_HEADER",
        "CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE",
        "CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_WILL_RETRY",
        "CRS_FWUP_RESULT_NOT_INITIALIZED",
        "CRS_FWUP_RESULT_INVALID_IMAGE_HEADER",
        "CRS_FWUP_RESULT_INVALID_IMAGE",
        "CRS_FWUP_RESULT_DECRYPTION_FAILURE",
        "CRS_FWUP_RESULT_FLASH_FAILURE",
        "CRS_FWUP_RESULT_BAD_ARGUMENT",
        "CRS_FWUP_RESULT_BAD_HEADER",
        "CRS_FWUP_RESULT_BAD_WRITE_ALIGNMENT",
        "CRS_FWUP_RESULT_BAD_ENCRYPTION_KEY",
        "CRS_FWUP_RESULT_DUPLICATE_UPDATE_TO_SLOT",
        "CRS_FWUP_RESULT_ENCRYPTED_UPDATE_REQUIRED",
        "CRS_FWUP_RESULT_ERROR_UNKNOWN",
        "CRS_FWUP_RESULT_INVALID_IMAGE_VERSION",
        "CRS_FWUP_RESULT_NONE",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CRS_FWUP_RESULT_BAD_ARGUMENT:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_BAD_ENCRYPTION_KEY:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_BAD_HEADER:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_BAD_WRITE_ALIGNMENT:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_DECRYPTION_FAILURE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_DUPLICATE_UPDATE_TO_SLOT:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_ENCRYPTED_UPDATE_REQUIRED:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_ERROR_UNKNOWN:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_FLASH_FAILURE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_INVALID_IMAGE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_INVALID_IMAGE_HEADER:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_INVALID_IMAGE_VERSION:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_NONE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_NOT_INITIALIZED:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_SEND_DATA:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_HEADER:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_WILL_RETRY:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final enum CRS_FWUP_RESULT_SUCCESS:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

.field public static final Companion:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult$Companion;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x13

    new-array v0, v0, [Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/4 v2, 0x0

    const-string v3, "CRS_FWUP_RESULT_SUCCESS"

    .line 124
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_SUCCESS:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/4 v2, 0x1

    const-string v3, "CRS_FWUP_RESULT_SEND_DATA"

    .line 126
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_SEND_DATA:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/4 v2, 0x2

    const-string v3, "CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_HEADER"

    .line 128
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_HEADER:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/4 v2, 0x3

    const-string v3, "CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE"

    .line 130
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/4 v2, 0x4

    const-string v3, "CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_WILL_RETRY"

    .line 132
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_SEND_DATA_INVALID_IMAGE_WILL_RETRY:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/4 v2, 0x5

    const-string v3, "CRS_FWUP_RESULT_NOT_INITIALIZED"

    .line 134
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_NOT_INITIALIZED:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/4 v2, 0x6

    const-string v3, "CRS_FWUP_RESULT_INVALID_IMAGE_HEADER"

    .line 136
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_INVALID_IMAGE_HEADER:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/4 v2, 0x7

    const-string v3, "CRS_FWUP_RESULT_INVALID_IMAGE"

    .line 138
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_INVALID_IMAGE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/16 v2, 0x8

    const-string v3, "CRS_FWUP_RESULT_DECRYPTION_FAILURE"

    .line 140
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_DECRYPTION_FAILURE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/16 v2, 0x9

    const-string v3, "CRS_FWUP_RESULT_FLASH_FAILURE"

    .line 142
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_FLASH_FAILURE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/16 v2, 0xa

    const-string v3, "CRS_FWUP_RESULT_BAD_ARGUMENT"

    .line 144
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_BAD_ARGUMENT:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/16 v2, 0xb

    const-string v3, "CRS_FWUP_RESULT_BAD_HEADER"

    .line 146
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_BAD_HEADER:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/16 v2, 0xc

    const-string v3, "CRS_FWUP_RESULT_BAD_WRITE_ALIGNMENT"

    .line 148
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_BAD_WRITE_ALIGNMENT:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/16 v2, 0xd

    const-string v3, "CRS_FWUP_RESULT_BAD_ENCRYPTION_KEY"

    .line 150
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_BAD_ENCRYPTION_KEY:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/16 v2, 0xe

    const-string v3, "CRS_FWUP_RESULT_DUPLICATE_UPDATE_TO_SLOT"

    .line 152
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_DUPLICATE_UPDATE_TO_SLOT:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const-string v2, "CRS_FWUP_RESULT_ENCRYPTED_UPDATE_REQUIRED"

    const/16 v3, 0xf

    const/16 v4, 0xf

    .line 154
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_ENCRYPTED_UPDATE_REQUIRED:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const-string v2, "CRS_FWUP_RESULT_ERROR_UNKNOWN"

    const/16 v3, 0x10

    const/16 v4, 0x10

    .line 156
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_ERROR_UNKNOWN:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const-string v2, "CRS_FWUP_RESULT_INVALID_IMAGE_VERSION"

    const/16 v3, 0x11

    const/16 v4, 0x11

    .line 158
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_INVALID_IMAGE_VERSION:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const-string v2, "CRS_FWUP_RESULT_NONE"

    const/16 v3, 0x12

    const/16 v4, 0xff

    .line 160
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->CRS_FWUP_RESULT_NONE:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->$VALUES:[Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    new-instance v0, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->Companion:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult$Companion;

    .line 164
    new-instance v0, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult$Companion$ADAPTER$1;

    .line 165
    const-class v1, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult$Companion$ADAPTER$1;-><init>(Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 121
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->value:I

    return-void
.end method

.method public static final fromValue(I)Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->Companion:Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult$Companion;->fromValue(I)Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;
    .locals 1

    const-class v0, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;
    .locals 1

    sget-object v0, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->$VALUES:[Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    invoke-virtual {v0}, [Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 122
    iget v0, p0, Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError$UpdateResult;->value:I

    return v0
.end method
