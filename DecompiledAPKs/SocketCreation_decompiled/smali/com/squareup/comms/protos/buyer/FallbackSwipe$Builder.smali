.class public final Lcom/squareup/comms/protos/buyer/FallbackSwipe$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FallbackSwipe.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/FallbackSwipe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/FallbackSwipe;",
        "Lcom/squareup/comms/protos/buyer/FallbackSwipe$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0008\u001a\u00020\u0002H\u0016J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005J\u0010\u0010\u0006\u001a\u00020\u00002\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/FallbackSwipe$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/FallbackSwipe;",
        "()V",
        "card",
        "Lcom/squareup/comms/protos/common/Card;",
        "type",
        "Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public card:Lcom/squareup/comms/protos/common/Card;

.field public type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/FallbackSwipe;
    .locals 4

    .line 100
    new-instance v0, Lcom/squareup/comms/protos/buyer/FallbackSwipe;

    .line 101
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/FallbackSwipe$Builder;->type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    .line 102
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/FallbackSwipe$Builder;->card:Lcom/squareup/comms/protos/common/Card;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/FallbackSwipe$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    .line 100
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/comms/protos/buyer/FallbackSwipe;-><init>(Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;Lcom/squareup/comms/protos/common/Card;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 83
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/FallbackSwipe$Builder;->build()Lcom/squareup/comms/protos/buyer/FallbackSwipe;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final card(Lcom/squareup/comms/protos/common/Card;)Lcom/squareup/comms/protos/buyer/FallbackSwipe$Builder;
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/FallbackSwipe$Builder;->card:Lcom/squareup/comms/protos/common/Card;

    return-object p0
.end method

.method public final type(Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;)Lcom/squareup/comms/protos/buyer/FallbackSwipe$Builder;
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/FallbackSwipe$Builder;->type:Lcom/squareup/comms/protos/buyer/FallbackSwipe$ChipCardFallbackIndicator;

    return-object p0
.end method
