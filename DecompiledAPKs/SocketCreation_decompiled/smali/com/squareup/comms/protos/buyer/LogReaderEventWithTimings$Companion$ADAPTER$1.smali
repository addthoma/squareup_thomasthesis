.class public final Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion$ADAPTER$1;
.super Lcom/squareup/wire/ProtoAdapter;
.source "LogReaderEventWithTimings.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLogReaderEventWithTimings.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LogReaderEventWithTimings.kt\ncom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion$ADAPTER$1\n+ 2 ProtoReader.kt\ncom/squareup/wire/ProtoReader\n*L\n1#1,309:1\n415#2,7:310\n*E\n*S KotlinDebug\n*F\n+ 1 LogReaderEventWithTimings.kt\ncom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion$ADAPTER$1\n*L\n156#1,7:310\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000-\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\r\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0002H\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "com/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion$ADAPTER$1",
        "Lcom/squareup/wire/ProtoAdapter;",
        "Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;",
        "decode",
        "reader",
        "Lcom/squareup/wire/ProtoReader;",
        "encode",
        "",
        "writer",
        "Lcom/squareup/wire/ProtoWriter;",
        "value",
        "encodedSize",
        "",
        "redact",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V
    .locals 0

    .line 135
    invoke-direct {p0, p1, p2}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;
    .locals 9

    const-string v0, "reader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 153
    move-object v1, v0

    check-cast v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    .line 154
    check-cast v0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    .line 155
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/List;

    .line 310
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v3

    .line 312
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v5

    const/4 v6, -0x1

    const/4 v7, 0x2

    const/4 v8, 0x1

    if-ne v5, v6, :cond_2

    .line 316
    invoke-virtual {p1, v3, v4}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    .line 164
    new-instance v3, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    const/4 v4, 0x0

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    invoke-direct {v3, v1, v0, v2, p1}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;-><init>(Lcom/squareup/comms/protos/buyer/BuyerEventName;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Ljava/util/List;Lokio/ByteString;)V

    return-object v3

    :cond_0
    new-array p1, v7, [Ljava/lang/Object;

    aput-object v0, p1, v4

    const-string v0, "card_reader_info"

    aput-object v0, p1, v8

    .line 166
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_1
    new-array p1, v7, [Ljava/lang/Object;

    aput-object v1, p1, v4

    const-string v0, "event_name"

    aput-object v0, p1, v8

    .line 165
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_2
    if-eq v5, v8, :cond_5

    if-eq v5, v7, :cond_4

    const/4 v6, 0x3

    if-eq v5, v6, :cond_3

    .line 161
    invoke-virtual {p1, v5}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 160
    :cond_3
    sget-object v5, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$BuyerPaymentTiming;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 159
    :cond_4
    sget-object v0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    goto :goto_0

    .line 158
    :cond_5
    sget-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v1, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0

    .line 134
    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion$ADAPTER$1;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;)V
    .locals 3

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    sget-object v0, Lcom/squareup/comms/protos/buyer/BuyerEventName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 147
    sget-object v0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 148
    sget-object v0, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$BuyerPaymentTiming;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->payment_timings:Ljava/util/List;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 149
    invoke-virtual {p2}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0

    .line 134
    check-cast p2, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion$ADAPTER$1;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;)I
    .locals 4

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 140
    sget-object v0, Lcom/squareup/comms/protos/buyer/BuyerEventName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 141
    sget-object v1, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    sget-object v1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$BuyerPaymentTiming;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->payment_timings:Ljava/util/List;

    const/4 v3, 0x3

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 134
    check-cast p1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion$ADAPTER$1;->encodedSize(Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;)Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;
    .locals 8

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 174
    sget-object v0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    .line 175
    iget-object v0, p1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->payment_timings:Ljava/util/List;

    sget-object v1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$BuyerPaymentTiming;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->-redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)Ljava/util/List;

    move-result-object v4

    .line 176
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v1, p1

    .line 173
    invoke-static/range {v1 .. v7}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;->copy$default(Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;Lcom/squareup/comms/protos/buyer/BuyerEventName;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Ljava/util/List;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 134
    check-cast p1, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings$Companion$ADAPTER$1;->redact(Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;)Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;

    move-result-object p1

    return-object p1
.end method
