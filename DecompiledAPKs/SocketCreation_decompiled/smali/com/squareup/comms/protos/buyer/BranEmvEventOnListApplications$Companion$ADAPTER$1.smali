.class public final Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Companion$ADAPTER$1;
.super Lcom/squareup/wire/ProtoAdapter;
.source "BranEmvEventOnListApplications.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBranEmvEventOnListApplications.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BranEmvEventOnListApplications.kt\ncom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Companion$ADAPTER$1\n+ 2 ProtoReader.kt\ncom/squareup/wire/ProtoReader\n*L\n1#1,127:1\n415#2,7:128\n*E\n*S KotlinDebug\n*F\n+ 1 BranEmvEventOnListApplications.kt\ncom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Companion$ADAPTER$1\n*L\n103#1,7:128\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000-\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\r\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0002H\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "com/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Companion$ADAPTER$1",
        "Lcom/squareup/wire/ProtoAdapter;",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;",
        "decode",
        "reader",
        "Lcom/squareup/wire/ProtoReader;",
        "encode",
        "",
        "writer",
        "Lcom/squareup/wire/ProtoWriter;",
        "value",
        "encodedSize",
        "",
        "redact",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V
    .locals 0

    .line 88
    invoke-direct {p0, p1, p2}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;
    .locals 5

    const-string v0, "reader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 128
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 130
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 134
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    .line 109
    new-instance v1, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;

    invoke-direct {v1, v0, p1}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v1

    :cond_0
    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    .line 106
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 105
    :cond_1
    sget-object v3, Lcom/squareup/comms/protos/buyer/EmvApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0

    .line 87
    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Companion$ADAPTER$1;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;)V
    .locals 3

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    sget-object v0, Lcom/squareup/comms/protos/buyer/EmvApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;->applications:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 98
    invoke-virtual {p2}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0

    .line 87
    check-cast p2, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Companion$ADAPTER$1;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;)I
    .locals 3

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    sget-object v0, Lcom/squareup/comms/protos/buyer/EmvApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;->applications:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 94
    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 87
    check-cast p1, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Companion$ADAPTER$1;->encodedSize(Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;
    .locals 2

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    iget-object v0, p1, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;->applications:Ljava/util/List;

    sget-object v1, Lcom/squareup/comms/protos/buyer/EmvApplication;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->-redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)Ljava/util/List;

    move-result-object v0

    .line 118
    sget-object v1, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    .line 116
    invoke-virtual {p1, v0, v1}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;->copy(Ljava/util/List;Lokio/ByteString;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 87
    check-cast p1, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Companion$ADAPTER$1;->redact(Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;

    move-result-object p1

    return-object p1
.end method
