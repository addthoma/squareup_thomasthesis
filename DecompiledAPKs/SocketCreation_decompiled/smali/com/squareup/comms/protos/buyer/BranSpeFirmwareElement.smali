.class public final enum Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;
.super Ljava/lang/Enum;
.source "BranSpeFirmwareElement.kt"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000e\u0008\u0086\u0001\u0018\u0000 \u00112\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u0001\u0011B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;",
        "",
        "Lcom/squareup/wire/WireEnum;",
        "value",
        "",
        "(Ljava/lang/String;II)V",
        "getValue",
        "()I",
        "HWID",
        "SERIAL_NUMBER",
        "K21",
        "K400_CPU0_BOOTLOADER",
        "K400_CPU1_BOOTLOADER",
        "K400_CPU0_FIRMWARE",
        "K400_CPU1_FIRMWARE",
        "FPGA",
        "TMS_CAPK",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement$Companion;

.field public static final enum FPGA:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

.field public static final enum HWID:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

.field public static final enum K21:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

.field public static final enum K400_CPU0_BOOTLOADER:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

.field public static final enum K400_CPU0_FIRMWARE:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

.field public static final enum K400_CPU1_BOOTLOADER:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

.field public static final enum K400_CPU1_FIRMWARE:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

.field public static final enum SERIAL_NUMBER:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

.field public static final enum TMS_CAPK:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    new-instance v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    const/4 v2, 0x0

    const-string v3, "HWID"

    .line 18
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->HWID:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    const/4 v2, 0x1

    const-string v3, "SERIAL_NUMBER"

    .line 20
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->SERIAL_NUMBER:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    const/4 v2, 0x2

    const-string v3, "K21"

    .line 22
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->K21:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    const/4 v2, 0x3

    const-string v3, "K400_CPU0_BOOTLOADER"

    .line 24
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->K400_CPU0_BOOTLOADER:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    const/4 v2, 0x4

    const-string v3, "K400_CPU1_BOOTLOADER"

    .line 26
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->K400_CPU1_BOOTLOADER:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    const/4 v2, 0x5

    const-string v3, "K400_CPU0_FIRMWARE"

    .line 28
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->K400_CPU0_FIRMWARE:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    const/4 v2, 0x6

    const-string v3, "K400_CPU1_FIRMWARE"

    .line 30
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->K400_CPU1_FIRMWARE:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    const/4 v2, 0x7

    const-string v3, "FPGA"

    .line 32
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->FPGA:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    const/16 v2, 0x8

    const-string v3, "TMS_CAPK"

    .line 34
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->TMS_CAPK:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->$VALUES:[Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    new-instance v0, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->Companion:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement$Companion;

    .line 38
    new-instance v0, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement$Companion$ADAPTER$1;

    .line 40
    const-class v1, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement$Companion$ADAPTER$1;-><init>(Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->value:I

    return-void
.end method

.method public static final fromValue(I)Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->Companion:Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement$Companion;->fromValue(I)Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;
    .locals 1

    const-class v0, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    return-object p0
.end method

.method public static values()[Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;
    .locals 1

    sget-object v0, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->$VALUES:[Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    invoke-virtual {v0}, [Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 16
    iget v0, p0, Lcom/squareup/comms/protos/buyer/BranSpeFirmwareElement;->value:I

    return v0
.end method
