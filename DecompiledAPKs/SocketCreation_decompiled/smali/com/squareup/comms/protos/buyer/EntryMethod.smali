.class public final enum Lcom/squareup/comms/protos/buyer/EntryMethod;
.super Ljava/lang/Enum;
.source "EntryMethod.kt"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/EntryMethod$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/comms/protos/buyer/EntryMethod;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0007\u0008\u0086\u0001\u0018\u0000 \n2\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u0001\nB\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007j\u0002\u0008\u0008j\u0002\u0008\t\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/EntryMethod;",
        "",
        "Lcom/squareup/wire/WireEnum;",
        "value",
        "",
        "(Ljava/lang/String;II)V",
        "getValue",
        "()I",
        "DIP",
        "TAP",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/comms/protos/buyer/EntryMethod;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/EntryMethod;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/buyer/EntryMethod$Companion;

.field public static final enum DIP:Lcom/squareup/comms/protos/buyer/EntryMethod;

.field public static final enum TAP:Lcom/squareup/comms/protos/buyer/EntryMethod;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/comms/protos/buyer/EntryMethod;

    new-instance v1, Lcom/squareup/comms/protos/buyer/EntryMethod;

    const/4 v2, 0x0

    const-string v3, "DIP"

    .line 15
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/EntryMethod;->DIP:Lcom/squareup/comms/protos/buyer/EntryMethod;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/EntryMethod;

    const/4 v2, 0x1

    const-string v3, "TAP"

    .line 17
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/EntryMethod;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/EntryMethod;->TAP:Lcom/squareup/comms/protos/buyer/EntryMethod;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/comms/protos/buyer/EntryMethod;->$VALUES:[Lcom/squareup/comms/protos/buyer/EntryMethod;

    new-instance v0, Lcom/squareup/comms/protos/buyer/EntryMethod$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/EntryMethod$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/EntryMethod;->Companion:Lcom/squareup/comms/protos/buyer/EntryMethod$Companion;

    .line 21
    new-instance v0, Lcom/squareup/comms/protos/buyer/EntryMethod$Companion$ADAPTER$1;

    .line 22
    const-class v1, Lcom/squareup/comms/protos/buyer/EntryMethod;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/EntryMethod$Companion$ADAPTER$1;-><init>(Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/comms/protos/buyer/EntryMethod;->value:I

    return-void
.end method

.method public static final fromValue(I)Lcom/squareup/comms/protos/buyer/EntryMethod;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/comms/protos/buyer/EntryMethod;->Companion:Lcom/squareup/comms/protos/buyer/EntryMethod$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/comms/protos/buyer/EntryMethod$Companion;->fromValue(I)Lcom/squareup/comms/protos/buyer/EntryMethod;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/EntryMethod;
    .locals 1

    const-class v0, Lcom/squareup/comms/protos/buyer/EntryMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/comms/protos/buyer/EntryMethod;

    return-object p0
.end method

.method public static values()[Lcom/squareup/comms/protos/buyer/EntryMethod;
    .locals 1

    sget-object v0, Lcom/squareup/comms/protos/buyer/EntryMethod;->$VALUES:[Lcom/squareup/comms/protos/buyer/EntryMethod;

    invoke-virtual {v0}, [Lcom/squareup/comms/protos/buyer/EntryMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/comms/protos/buyer/EntryMethod;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 13
    iget v0, p0, Lcom/squareup/comms/protos/buyer/EntryMethod;->value:I

    return v0
.end method
