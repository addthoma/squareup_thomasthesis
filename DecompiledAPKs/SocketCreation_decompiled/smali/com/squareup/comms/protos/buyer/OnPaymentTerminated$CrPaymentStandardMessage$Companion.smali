.class public final Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage$Companion;
.super Ljava/lang/Object;
.source "OnPaymentTerminated.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\u0008H\u0007R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage$Companion;",
        "",
        "()V",
        "ADAPTER",
        "Lcom/squareup/wire/ProtoAdapter;",
        "Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;",
        "fromValue",
        "value",
        "",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 201
    invoke-direct {p0}, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromValue(I)Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    packed-switch p1, :pswitch_data_0

    .line 247
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 246
    :pswitch_1
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_UNLOCK_PHONE_TO_PAY:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto/16 :goto_0

    .line 245
    :pswitch_2
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PRESENT_CARD_AGAIN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto/16 :goto_0

    .line 244
    :pswitch_3
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_SEE_PHONE_FOR_INSTRUCTIONS:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto/16 :goto_0

    .line 243
    :pswitch_4
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NO_MSG:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto/16 :goto_0

    .line 242
    :pswitch_5
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_INSERT_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto/16 :goto_0

    .line 241
    :pswitch_6
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INSERT_SWIPE_OR_TRY_ANOTHER_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto/16 :goto_0

    .line 240
    :pswitch_7
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AUTHORISING_PLS_WAIT:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto/16 :goto_0

    .line 239
    :pswitch_8
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_APPROVED_PLS_SIGN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto/16 :goto_0

    .line 238
    :pswitch_9
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_PRESENT_ONE_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 237
    :pswitch_a
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLS_INSERT_OR_SWIPE_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 236
    :pswitch_b
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CARD_READ_OK_PLS_REMOVE_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 235
    :pswitch_c
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PROCESSING:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 234
    :pswitch_d
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PRESENT_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 233
    :pswitch_e
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_WELCOME:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 232
    :pswitch_f
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_TRY_AGAIN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 231
    :pswitch_10
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_USE_MAG_STRIP:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 230
    :pswitch_11
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_USE_CHIP_READER:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 229
    :pswitch_12
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_REMOVE_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 228
    :pswitch_13
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PROCESSING_ERROR:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 227
    :pswitch_14
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PLEASE_WAIT:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 226
    :pswitch_15
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_PIN_OK:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 225
    :pswitch_16
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NOT_ACCEPTED:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 224
    :pswitch_17
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INSERT_CARD:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 223
    :pswitch_18
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_INCORRECT_PIN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 222
    :pswitch_19
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_ENTER_PIN:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 221
    :pswitch_1a
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_ENTER_AMOUNT:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 220
    :pswitch_1b
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_DECLINED:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 219
    :pswitch_1c
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CARD_ERROR:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 218
    :pswitch_1d
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CANCEL_OR_ENTER:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 217
    :pswitch_1e
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_CALL_YOUR_BANK:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 216
    :pswitch_1f
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_APPROVED:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 215
    :pswitch_20
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AMOUNT_OK:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 214
    :pswitch_21
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_AMOUNT:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    goto :goto_0

    .line 213
    :pswitch_22
    sget-object p1, Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;->CR_PAYMENT_STD_MSG_NONE:Lcom/squareup/comms/protos/buyer/OnPaymentTerminated$CrPaymentStandardMessage;

    :goto_0
    return-object p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
