.class public final enum Lcom/squareup/comms/protos/buyer/BuyerEventName;
.super Ljava/lang/Enum;
.source "BuyerEventName.kt"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/BuyerEventName$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/comms/protos/buyer/BuyerEventName;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008-\u0008\u0086\u0001\u0018\u0000 02\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u00010B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019j\u0002\u0008\u001aj\u0002\u0008\u001bj\u0002\u0008\u001cj\u0002\u0008\u001dj\u0002\u0008\u001ej\u0002\u0008\u001fj\u0002\u0008 j\u0002\u0008!j\u0002\u0008\"j\u0002\u0008#j\u0002\u0008$j\u0002\u0008%j\u0002\u0008&j\u0002\u0008\'j\u0002\u0008(j\u0002\u0008)j\u0002\u0008*j\u0002\u0008+j\u0002\u0008,j\u0002\u0008-j\u0002\u0008.j\u0002\u0008/\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/BuyerEventName;",
        "",
        "Lcom/squareup/wire/WireEnum;",
        "value",
        "",
        "(Ljava/lang/String;II)V",
        "getValue",
        "()I",
        "INIT_CORE_DUMP_FOUND",
        "INIT_TAMPER_DATA",
        "INIT_READY_FOR_PAYMENTS",
        "INIT_FULL_COMMS",
        "PAYMENT_USE_CHIP_CARD",
        "PAYMENT_CARD_INSERTED",
        "PAYMENT_STARTING_PAYMENT_ON_READER",
        "PAYMENT_CARD_REMOVED",
        "PAYMENT_CARD_REMOVED_DURING_PAYMENT",
        "PAYMENT_CARD_ERROR",
        "PAYMENT_TECHNICAL_FALLBACK",
        "PAYMENT_MAGSWIPE_SUCCESS",
        "PAYMENT_MAGSWIPE_FAILURE",
        "PAYMENT_MAGSWIPE_PASSTHROUGH",
        "PAYMENT_SCHEME_FALLBACK",
        "PAYMENT_APPLICATION_SELECTION",
        "PAYMENT_APPLICATION_SELECTED",
        "PAYMENT_SEND_ARQC_TO_SERVER",
        "PAYMENT_SEND_ARPC_TO_READER",
        "PAYMENT_COMPLETE_APPROVED",
        "PAYMENT_COMPLETE_TERMINATED",
        "PAYMENT_COMPLETE_DECLINED",
        "CONTACTLESS_PAYMENT_ACTION_REQUIRED",
        "CONTACTLESS_PAYMENT_UNLOCK_DEVICE",
        "CONTACTLESS_PAYMENT_MULTIPLE_CARDS",
        "CONTACTLESS_PAYMENT_ERROR_PRESENT_AGAIN",
        "CONTACTLESS_PAYMENT_FALLBACK_INSERT_OR_SWIPE",
        "CONTACTLESS_PAYMENT_COMPLETE_TIMEOUT",
        "CONTACTLESS_PAYMENT_COMPLETE_APPROVED",
        "CONTACTLESS_PAYMENT_COMPLETE_TERMINATED",
        "CONTACTLESS_PAYMENT_COMPLETE_DECLINED",
        "CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_TRY_ANOTHER_CARD",
        "CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_INSERT_CARD",
        "PAYMENT_CARD_MUST_TAP",
        "CONTACTLESS_PAYMENT_CARD_ERROR_TRY_ANOTHER_CARD",
        "CONTACTLESS_PAYMENT_INTERFACE_UNAVAILABLE",
        "CONTACTLESS_PAYMENT_CARD_BLOCKED",
        "CONTACTLESS_PAYMENT_COLLISION_DETECTED",
        "CONTACTLESS_PAYMENT_REQUEST_TAP_CARD",
        "CONTACTLESS_PAYMENT_PROCESSING_ERROR",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/BuyerEventName;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CONTACTLESS_PAYMENT_ACTION_REQUIRED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_CARD_BLOCKED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_CARD_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_COLLISION_DETECTED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_COMPLETE_APPROVED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_COMPLETE_DECLINED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_COMPLETE_TIMEOUT:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_ERROR_PRESENT_AGAIN:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_FALLBACK_INSERT_OR_SWIPE:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_INTERFACE_UNAVAILABLE:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_INSERT_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_TRY_ANOTHER_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_MULTIPLE_CARDS:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_PROCESSING_ERROR:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_REQUEST_TAP_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum CONTACTLESS_PAYMENT_UNLOCK_DEVICE:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final Companion:Lcom/squareup/comms/protos/buyer/BuyerEventName$Companion;

.field public static final enum INIT_CORE_DUMP_FOUND:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum INIT_FULL_COMMS:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum INIT_READY_FOR_PAYMENTS:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum INIT_TAMPER_DATA:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_APPLICATION_SELECTED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_APPLICATION_SELECTION:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_CARD_ERROR:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_CARD_INSERTED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_CARD_MUST_TAP:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_CARD_REMOVED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_CARD_REMOVED_DURING_PAYMENT:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_COMPLETE_APPROVED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_COMPLETE_DECLINED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_MAGSWIPE_FAILURE:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_MAGSWIPE_PASSTHROUGH:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_MAGSWIPE_SUCCESS:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_SCHEME_FALLBACK:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_SEND_ARPC_TO_READER:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_SEND_ARQC_TO_SERVER:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_STARTING_PAYMENT_ON_READER:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_TECHNICAL_FALLBACK:Lcom/squareup/comms/protos/buyer/BuyerEventName;

.field public static final enum PAYMENT_USE_CHIP_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x28

    new-array v0, v0, [Lcom/squareup/comms/protos/buyer/BuyerEventName;

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/4 v2, 0x0

    const-string v3, "INIT_CORE_DUMP_FOUND"

    .line 15
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->INIT_CORE_DUMP_FOUND:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/4 v2, 0x1

    const-string v3, "INIT_TAMPER_DATA"

    .line 17
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->INIT_TAMPER_DATA:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/4 v2, 0x2

    const-string v3, "INIT_READY_FOR_PAYMENTS"

    .line 19
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->INIT_READY_FOR_PAYMENTS:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/4 v2, 0x3

    const-string v3, "INIT_FULL_COMMS"

    .line 21
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->INIT_FULL_COMMS:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/4 v2, 0x4

    const-string v3, "PAYMENT_USE_CHIP_CARD"

    .line 23
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_USE_CHIP_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/4 v2, 0x5

    const-string v3, "PAYMENT_CARD_INSERTED"

    .line 25
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_CARD_INSERTED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/4 v2, 0x6

    const-string v3, "PAYMENT_STARTING_PAYMENT_ON_READER"

    .line 27
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_STARTING_PAYMENT_ON_READER:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/4 v2, 0x7

    const-string v3, "PAYMENT_CARD_REMOVED"

    .line 29
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_CARD_REMOVED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x8

    const-string v3, "PAYMENT_CARD_REMOVED_DURING_PAYMENT"

    .line 31
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_CARD_REMOVED_DURING_PAYMENT:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x9

    const-string v3, "PAYMENT_CARD_ERROR"

    .line 33
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_CARD_ERROR:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0xa

    const-string v3, "PAYMENT_TECHNICAL_FALLBACK"

    .line 35
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_TECHNICAL_FALLBACK:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0xb

    const-string v3, "PAYMENT_MAGSWIPE_SUCCESS"

    .line 37
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_MAGSWIPE_SUCCESS:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0xc

    const-string v3, "PAYMENT_MAGSWIPE_FAILURE"

    .line 39
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_MAGSWIPE_FAILURE:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0xd

    const-string v3, "PAYMENT_MAGSWIPE_PASSTHROUGH"

    .line 41
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_MAGSWIPE_PASSTHROUGH:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0xe

    const-string v3, "PAYMENT_SCHEME_FALLBACK"

    .line 43
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_SCHEME_FALLBACK:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "PAYMENT_APPLICATION_SELECTION"

    const/16 v3, 0xf

    const/16 v4, 0xf

    .line 45
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_APPLICATION_SELECTION:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "PAYMENT_APPLICATION_SELECTED"

    const/16 v3, 0x10

    const/16 v4, 0x10

    .line 47
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_APPLICATION_SELECTED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "PAYMENT_SEND_ARQC_TO_SERVER"

    const/16 v3, 0x11

    const/16 v4, 0x11

    .line 49
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_SEND_ARQC_TO_SERVER:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "PAYMENT_SEND_ARPC_TO_READER"

    const/16 v3, 0x12

    const/16 v4, 0x12

    .line 51
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_SEND_ARPC_TO_READER:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "PAYMENT_COMPLETE_APPROVED"

    const/16 v3, 0x13

    const/16 v4, 0x13

    .line 53
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_COMPLETE_APPROVED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "PAYMENT_COMPLETE_TERMINATED"

    const/16 v3, 0x14

    const/16 v4, 0x14

    .line 55
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "PAYMENT_COMPLETE_DECLINED"

    const/16 v3, 0x15

    const/16 v4, 0x15

    .line 57
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_COMPLETE_DECLINED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_ACTION_REQUIRED"

    const/16 v3, 0x16

    const/16 v4, 0x16

    .line 59
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_ACTION_REQUIRED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_UNLOCK_DEVICE"

    const/16 v3, 0x17

    const/16 v4, 0x17

    .line 61
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_UNLOCK_DEVICE:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_MULTIPLE_CARDS"

    const/16 v3, 0x18

    const/16 v4, 0x18

    .line 63
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_MULTIPLE_CARDS:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_ERROR_PRESENT_AGAIN"

    const/16 v3, 0x19

    const/16 v4, 0x19

    .line 65
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_ERROR_PRESENT_AGAIN:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_FALLBACK_INSERT_OR_SWIPE"

    const/16 v3, 0x1a

    const/16 v4, 0x1a

    .line 67
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_FALLBACK_INSERT_OR_SWIPE:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_COMPLETE_TIMEOUT"

    const/16 v3, 0x1b

    const/16 v4, 0x1b

    .line 69
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_COMPLETE_TIMEOUT:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_COMPLETE_APPROVED"

    const/16 v3, 0x1c

    const/16 v4, 0x1c

    .line 71
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_COMPLETE_APPROVED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_COMPLETE_TERMINATED"

    const/16 v3, 0x1d

    const/16 v4, 0x1d

    .line 73
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_COMPLETE_TERMINATED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_COMPLETE_DECLINED"

    const/16 v3, 0x1e

    const/16 v4, 0x1e

    .line 75
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_COMPLETE_DECLINED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_TRY_ANOTHER_CARD"

    const/16 v3, 0x1f

    const/16 v4, 0x1f

    .line 77
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_TRY_ANOTHER_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_INSERT_CARD"

    const/16 v3, 0x20

    const/16 v4, 0x20

    .line 79
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_LIMIT_EXCEEDED_INSERT_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "PAYMENT_CARD_MUST_TAP"

    const/16 v3, 0x21

    const/16 v4, 0x21

    .line 81
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->PAYMENT_CARD_MUST_TAP:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_CARD_ERROR_TRY_ANOTHER_CARD"

    const/16 v3, 0x22

    const/16 v4, 0x22

    .line 83
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_CARD_ERROR_TRY_ANOTHER_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_INTERFACE_UNAVAILABLE"

    const/16 v3, 0x23

    const/16 v4, 0x23

    .line 88
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_INTERFACE_UNAVAILABLE:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_CARD_BLOCKED"

    const/16 v3, 0x24

    const/16 v4, 0x24

    .line 90
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_CARD_BLOCKED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_COLLISION_DETECTED"

    const/16 v3, 0x25

    const/16 v4, 0x25

    .line 92
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_COLLISION_DETECTED:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_REQUEST_TAP_CARD"

    const/16 v3, 0x26

    const/16 v4, 0x26

    .line 94
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_REQUEST_TAP_CARD:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const-string v2, "CONTACTLESS_PAYMENT_PROCESSING_ERROR"

    const/16 v3, 0x27

    const/16 v4, 0x27

    .line 96
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/BuyerEventName;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->CONTACTLESS_PAYMENT_PROCESSING_ERROR:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/comms/protos/buyer/BuyerEventName;->$VALUES:[Lcom/squareup/comms/protos/buyer/BuyerEventName;

    new-instance v0, Lcom/squareup/comms/protos/buyer/BuyerEventName$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/BuyerEventName$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/BuyerEventName;->Companion:Lcom/squareup/comms/protos/buyer/BuyerEventName$Companion;

    .line 100
    new-instance v0, Lcom/squareup/comms/protos/buyer/BuyerEventName$Companion$ADAPTER$1;

    .line 101
    const-class v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/BuyerEventName$Companion$ADAPTER$1;-><init>(Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/BuyerEventName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/comms/protos/buyer/BuyerEventName;->value:I

    return-void
.end method

.method public static final fromValue(I)Lcom/squareup/comms/protos/buyer/BuyerEventName;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/comms/protos/buyer/BuyerEventName;->Companion:Lcom/squareup/comms/protos/buyer/BuyerEventName$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/comms/protos/buyer/BuyerEventName$Companion;->fromValue(I)Lcom/squareup/comms/protos/buyer/BuyerEventName;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/BuyerEventName;
    .locals 1

    const-class v0, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/comms/protos/buyer/BuyerEventName;
    .locals 1

    sget-object v0, Lcom/squareup/comms/protos/buyer/BuyerEventName;->$VALUES:[Lcom/squareup/comms/protos/buyer/BuyerEventName;

    invoke-virtual {v0}, [Lcom/squareup/comms/protos/buyer/BuyerEventName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/comms/protos/buyer/BuyerEventName;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 13
    iget v0, p0, Lcom/squareup/comms/protos/buyer/BuyerEventName;->value:I

    return v0
.end method
