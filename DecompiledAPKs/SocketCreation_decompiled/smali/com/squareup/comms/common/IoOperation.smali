.class final Lcom/squareup/comms/common/IoOperation;
.super Ljava/lang/Object;
.source "IoOperation.java"


# instance fields
.field private final channel:Ljava/nio/channels/SelectableChannel;

.field private final ioCompletion:Lcom/squareup/comms/common/IoCompletion;

.field private final ops:I


# direct methods
.method constructor <init>(Ljava/nio/channels/SelectableChannel;Lcom/squareup/comms/common/IoCompletion;I)V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/comms/common/IoOperation;->channel:Ljava/nio/channels/SelectableChannel;

    .line 18
    iput-object p2, p0, Lcom/squareup/comms/common/IoOperation;->ioCompletion:Lcom/squareup/comms/common/IoCompletion;

    .line 19
    iput p3, p0, Lcom/squareup/comms/common/IoOperation;->ops:I

    return-void
.end method

.method private toString(I)Ljava/lang/String;
    .locals 2

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    and-int/lit8 v1, p1, 0x10

    if-lez v1, :cond_0

    const-string v1, "A"

    .line 32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    and-int/lit8 v1, p1, 0x8

    if-lez v1, :cond_1

    const-string v1, "C"

    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    and-int/lit8 v1, p1, 0x1

    if-lez v1, :cond_2

    const-string v1, "R"

    .line 34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    and-int/lit8 p1, p1, 0x4

    if-lez p1, :cond_3

    const-string p1, "W"

    .line 35
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method ioCompletion()Lcom/squareup/comms/common/IoCompletion;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/comms/common/IoOperation;->ioCompletion:Lcom/squareup/comms/common/IoCompletion;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 27
    iget-object v1, p0, Lcom/squareup/comms/common/IoOperation;->channel:Ljava/nio/channels/SelectableChannel;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget v1, p0, Lcom/squareup/comms/common/IoOperation;->ops:I

    invoke-direct {p0, v1}, Lcom/squareup/comms/common/IoOperation;->toString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    const-string v1, "IoOperation { %s, %s }"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
