.class public interface abstract Lcom/squareup/comms/common/IoCompletion;
.super Ljava/lang/Object;
.source "IoCompletion.java"


# virtual methods
.method public abstract onError(Ljava/io/IOException;)V
.end method

.method public abstract onReady(Ljava/nio/channels/SelectionKey;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
