.class Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;
.super Ljava/lang/Object;
.source "Callbacks.java"

# interfaces
.implements Lcom/squareup/comms/net/ConnectionFactory$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/net/Callbacks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SerialConnectionFactoryCallback"
.end annotation


# instance fields
.field private final delegate:Lcom/squareup/comms/net/ConnectionFactory$Callback;

.field private final ioThread:Lcom/squareup/comms/common/IoThread;


# direct methods
.method constructor <init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/net/ConnectionFactory$Callback;)V
    .locals 0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;->ioThread:Lcom/squareup/comms/common/IoThread;

    .line 79
    iput-object p2, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;->delegate:Lcom/squareup/comms/net/ConnectionFactory$Callback;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;)Lcom/squareup/comms/net/ConnectionFactory$Callback;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;->delegate:Lcom/squareup/comms/net/ConnectionFactory$Callback;

    return-object p0
.end method


# virtual methods
.method public onNewConnection(Lcom/squareup/comms/net/Connection;Lcom/squareup/comms/net/Device;)V
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;->ioThread:Lcom/squareup/comms/common/IoThread;

    new-instance v1, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback$1;-><init>(Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;Lcom/squareup/comms/net/Connection;Lcom/squareup/comms/net/Device;)V

    invoke-virtual {v0, v1}, Lcom/squareup/comms/common/IoThread;->post(Ljava/lang/Runnable;)V

    return-void
.end method
