.class public final Lcom/squareup/comms/net/Callbacks;
.super Ljava/lang/Object;
.source "Callbacks.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;,
        Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;,
        Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static serializedConnectionCallback(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/net/ConnectionCallback;)Lcom/squareup/comms/net/ConnectionCallback;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;

    invoke-direct {v0, p0, p1}, Lcom/squareup/comms/net/Callbacks$SerialConnectionCallback;-><init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/net/ConnectionCallback;)V

    return-object v0
.end method

.method public static serializedConnectionListener(Ljava/util/concurrent/Executor;Lcom/squareup/comms/ConnectionListener;)Lcom/squareup/comms/ConnectionListener;
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/comms/net/Callbacks$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/comms/net/Callbacks$1;-><init>(Ljava/util/concurrent/Executor;Lcom/squareup/comms/ConnectionListener;)V

    return-object v0
.end method

.method static serializedFactoryCallback(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/net/ConnectionFactory$Callback;)Lcom/squareup/comms/net/ConnectionFactory$Callback;
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;

    invoke-direct {v0, p0, p1}, Lcom/squareup/comms/net/Callbacks$SerialConnectionFactoryCallback;-><init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/net/ConnectionFactory$Callback;)V

    return-object v0
.end method

.method public static serializedHealthCheckerCallback(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/HealthChecker$Callback;)Lcom/squareup/comms/HealthChecker$Callback;
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback;

    invoke-direct {v0, p0, p1}, Lcom/squareup/comms/net/Callbacks$SerialHealthCheckerCallback;-><init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/HealthChecker$Callback;)V

    return-object v0
.end method
