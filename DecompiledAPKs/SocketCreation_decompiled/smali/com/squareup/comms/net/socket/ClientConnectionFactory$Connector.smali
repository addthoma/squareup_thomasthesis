.class final Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;
.super Ljava/lang/Object;
.source "ClientConnectionFactory.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/net/socket/ClientConnectionFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Connector"
.end annotation


# instance fields
.field private final callback:Lcom/squareup/comms/net/ConnectionFactory$Callback;

.field private final retry:I

.field private final socket:Ljava/nio/channels/SocketChannel;

.field final synthetic this$0:Lcom/squareup/comms/net/socket/ClientConnectionFactory;


# direct methods
.method constructor <init>(Lcom/squareup/comms/net/socket/ClientConnectionFactory;ILcom/squareup/comms/net/ConnectionFactory$Callback;)V
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->this$0:Lcom/squareup/comms/net/socket/ClientConnectionFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput p2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->retry:I

    .line 147
    iput-object p3, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->callback:Lcom/squareup/comms/net/ConnectionFactory$Callback;

    .line 150
    :try_start_0
    invoke-static {}, Ljava/nio/channels/SocketChannel;->open()Ljava/nio/channels/SocketChannel;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->socket:Ljava/nio/channels/SocketChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    invoke-static {p1}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->access$000(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Ljava/util/List;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->socket:Ljava/nio/channels/SocketChannel;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :catch_0
    move-exception p1

    .line 152
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method


# virtual methods
.method public run()V
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 160
    :try_start_0
    iget-object v2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->this$0:Lcom/squareup/comms/net/socket/ClientConnectionFactory;

    invoke-static {v2}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->access$100(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/net/InetSocketAddress;

    invoke-direct {v2, v1}, Ljava/net/InetSocketAddress;-><init>(I)V

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/net/InetSocketAddress;

    iget-object v3, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->this$0:Lcom/squareup/comms/net/socket/ClientConnectionFactory;

    .line 162
    invoke-static {v3}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->access$100(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 163
    :goto_0
    new-instance v3, Ljava/net/InetSocketAddress;

    iget-object v4, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->this$0:Lcom/squareup/comms/net/socket/ClientConnectionFactory;

    invoke-static {v4}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->access$200(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->this$0:Lcom/squareup/comms/net/socket/ClientConnectionFactory;

    invoke-static {v5}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->access$300(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)I

    move-result v5

    invoke-direct {v3, v4, v5}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    const-string v4, "Binding client to: %s, connecting to %s, retry: %d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v1

    aput-object v3, v5, v0

    const/4 v6, 0x2

    .line 165
    iget v7, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->retry:I

    .line 166
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 165
    invoke-static {v4, v5}, Ltimber/log/Timber;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 167
    iget-object v4, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v4}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/net/Socket;->bind(Ljava/net/SocketAddress;)V

    .line 168
    iget-object v2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2}, Ljava/nio/channels/SocketChannel;->socket()Ljava/net/Socket;

    move-result-object v2

    const/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 169
    iget-object v2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v2, v1}, Ljava/nio/channels/SocketChannel;->configureBlocking(Z)Ljava/nio/channels/SelectableChannel;

    .line 170
    iget-object v2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->callback:Lcom/squareup/comms/net/ConnectionFactory$Callback;

    new-instance v3, Lcom/squareup/comms/net/socket/SocketConnection;

    iget-object v4, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->this$0:Lcom/squareup/comms/net/socket/ClientConnectionFactory;

    invoke-static {v4}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->access$400(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Lcom/squareup/comms/common/IoThread;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->socket:Ljava/nio/channels/SocketChannel;

    invoke-direct {v3, v4, v5}, Lcom/squareup/comms/net/socket/SocketConnection;-><init>(Lcom/squareup/comms/common/IoThread;Ljava/nio/channels/SocketChannel;)V

    sget-object v4, Lcom/squareup/comms/net/Device;->VOID_DEVICE:Lcom/squareup/comms/net/Device;

    invoke-interface {v2, v3, v4}, Lcom/squareup/comms/net/ConnectionFactory$Callback;->onNewConnection(Lcom/squareup/comms/net/Connection;Lcom/squareup/comms/net/Device;)V

    .line 171
    iget-object v2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->this$0:Lcom/squareup/comms/net/socket/ClientConnectionFactory;

    invoke-static {v2}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->access$000(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->socket:Ljava/nio/channels/SocketChannel;

    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    new-array v3, v0, [Ljava/lang/Object;

    .line 173
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    const-string v1, "Client connection failure (%s), attempting reconnect"

    invoke-static {v1, v3}, Ltimber/log/Timber;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 174
    iget-object v1, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->this$0:Lcom/squareup/comms/net/socket/ClientConnectionFactory;

    invoke-static {v1}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->access$500(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;

    move-result-object v1

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->retry:I

    invoke-interface {v1, v2, v3}, Lcom/squareup/comms/net/socket/SocketConnectionFailureListener;->onError(Ljava/lang/String;I)V

    .line 176
    :try_start_1
    iget-object v1, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->socket:Ljava/nio/channels/SocketChannel;

    invoke-virtual {v1}, Ljava/nio/channels/SocketChannel;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 179
    :catch_1
    iget-object v1, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->this$0:Lcom/squareup/comms/net/socket/ClientConnectionFactory;

    invoke-static {v1}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->access$000(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->socket:Ljava/nio/channels/SocketChannel;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 180
    iget-object v1, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->this$0:Lcom/squareup/comms/net/socket/ClientConnectionFactory;

    iget v2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->retry:I

    add-int/2addr v2, v0

    invoke-static {v1, v2}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->access$600(Lcom/squareup/comms/net/socket/ClientConnectionFactory;I)V

    :goto_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 185
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    .line 186
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->this$0:Lcom/squareup/comms/net/socket/ClientConnectionFactory;

    .line 187
    invoke-static {v2}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->access$100(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->this$0:Lcom/squareup/comms/net/socket/ClientConnectionFactory;

    .line 188
    invoke-static {v2}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->access$200(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/squareup/comms/net/socket/ClientConnectionFactory$Connector;->this$0:Lcom/squareup/comms/net/socket/ClientConnectionFactory;

    .line 189
    invoke-static {v2}, Lcom/squareup/comms/net/socket/ClientConnectionFactory;->access$300(Lcom/squareup/comms/net/socket/ClientConnectionFactory;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    const-string v2, "Connector[%d]: %s -> %s:%d"

    .line 185
    invoke-static {v0, v2, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
