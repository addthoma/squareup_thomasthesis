.class public final Lsquareup/spe/R12DeviceManifest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "R12DeviceManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/R12DeviceManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/spe/R12DeviceManifest;",
        "Lsquareup/spe/R12DeviceManifest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public ble_manifest:Lsquareup/spe/TICC2640Manifest;

.field public comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

.field public crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

.field public hwid:Lokio/ByteString;

.field public k21_manifest:Lsquareup/spe/K21Manifest;

.field public k400_manifest:Lsquareup/spe/K400Manifest;

.field public max_compression_version:Ljava/lang/Integer;

.field public mlb_serial_number:Ljava/lang/String;

.field public pts_version:Ljava/lang/Integer;

.field public serial_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 217
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public ble_manifest(Lsquareup/spe/TICC2640Manifest;)Lsquareup/spe/R12DeviceManifest$Builder;
    .locals 0

    .line 246
    iput-object p1, p0, Lsquareup/spe/R12DeviceManifest$Builder;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 196
    invoke-virtual {p0}, Lsquareup/spe/R12DeviceManifest$Builder;->build()Lsquareup/spe/R12DeviceManifest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/spe/R12DeviceManifest;
    .locals 13

    .line 272
    new-instance v12, Lsquareup/spe/R12DeviceManifest;

    iget-object v1, p0, Lsquareup/spe/R12DeviceManifest$Builder;->hwid:Lokio/ByteString;

    iget-object v2, p0, Lsquareup/spe/R12DeviceManifest$Builder;->serial_number:Ljava/lang/String;

    iget-object v3, p0, Lsquareup/spe/R12DeviceManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    iget-object v4, p0, Lsquareup/spe/R12DeviceManifest$Builder;->k400_manifest:Lsquareup/spe/K400Manifest;

    iget-object v5, p0, Lsquareup/spe/R12DeviceManifest$Builder;->k21_manifest:Lsquareup/spe/K21Manifest;

    iget-object v6, p0, Lsquareup/spe/R12DeviceManifest$Builder;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    iget-object v7, p0, Lsquareup/spe/R12DeviceManifest$Builder;->max_compression_version:Ljava/lang/Integer;

    iget-object v8, p0, Lsquareup/spe/R12DeviceManifest$Builder;->pts_version:Ljava/lang/Integer;

    iget-object v9, p0, Lsquareup/spe/R12DeviceManifest$Builder;->mlb_serial_number:Ljava/lang/String;

    iget-object v10, p0, Lsquareup/spe/R12DeviceManifest$Builder;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lsquareup/spe/R12DeviceManifest;-><init>(Lokio/ByteString;Ljava/lang/String;Lsquareup/spe/CommsProtocolVersion;Lsquareup/spe/K400Manifest;Lsquareup/spe/K21Manifest;Lsquareup/spe/TICC2640Manifest;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Lsquareup/spe/CRQGT4Manifest;Lokio/ByteString;)V

    return-object v12
.end method

.method public comms_protocol_version(Lsquareup/spe/CommsProtocolVersion;)Lsquareup/spe/R12DeviceManifest$Builder;
    .locals 0

    .line 231
    iput-object p1, p0, Lsquareup/spe/R12DeviceManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    return-object p0
.end method

.method public crq_gt4_manifest(Lsquareup/spe/CRQGT4Manifest;)Lsquareup/spe/R12DeviceManifest$Builder;
    .locals 0

    .line 266
    iput-object p1, p0, Lsquareup/spe/R12DeviceManifest$Builder;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    return-object p0
.end method

.method public hwid(Lokio/ByteString;)Lsquareup/spe/R12DeviceManifest$Builder;
    .locals 0

    .line 221
    iput-object p1, p0, Lsquareup/spe/R12DeviceManifest$Builder;->hwid:Lokio/ByteString;

    return-object p0
.end method

.method public k21_manifest(Lsquareup/spe/K21Manifest;)Lsquareup/spe/R12DeviceManifest$Builder;
    .locals 0

    .line 241
    iput-object p1, p0, Lsquareup/spe/R12DeviceManifest$Builder;->k21_manifest:Lsquareup/spe/K21Manifest;

    return-object p0
.end method

.method public k400_manifest(Lsquareup/spe/K400Manifest;)Lsquareup/spe/R12DeviceManifest$Builder;
    .locals 0

    .line 236
    iput-object p1, p0, Lsquareup/spe/R12DeviceManifest$Builder;->k400_manifest:Lsquareup/spe/K400Manifest;

    return-object p0
.end method

.method public max_compression_version(Ljava/lang/Integer;)Lsquareup/spe/R12DeviceManifest$Builder;
    .locals 0

    .line 251
    iput-object p1, p0, Lsquareup/spe/R12DeviceManifest$Builder;->max_compression_version:Ljava/lang/Integer;

    return-object p0
.end method

.method public mlb_serial_number(Ljava/lang/String;)Lsquareup/spe/R12DeviceManifest$Builder;
    .locals 0

    .line 261
    iput-object p1, p0, Lsquareup/spe/R12DeviceManifest$Builder;->mlb_serial_number:Ljava/lang/String;

    return-object p0
.end method

.method public pts_version(Ljava/lang/Integer;)Lsquareup/spe/R12DeviceManifest$Builder;
    .locals 0

    .line 256
    iput-object p1, p0, Lsquareup/spe/R12DeviceManifest$Builder;->pts_version:Ljava/lang/Integer;

    return-object p0
.end method

.method public serial_number(Ljava/lang/String;)Lsquareup/spe/R12DeviceManifest$Builder;
    .locals 0

    .line 226
    iput-object p1, p0, Lsquareup/spe/R12DeviceManifest$Builder;->serial_number:Ljava/lang/String;

    return-object p0
.end method
