.class final Lsquareup/spe/FwupAssetGroup$ProtoAdapter_FwupAssetGroup;
.super Lcom/squareup/wire/ProtoAdapter;
.source "FwupAssetGroup.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/FwupAssetGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FwupAssetGroup"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/spe/FwupAssetGroup;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 289
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/spe/FwupAssetGroup;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 287
    invoke-virtual {p0, p1}, Lsquareup/spe/FwupAssetGroup$ProtoAdapter_FwupAssetGroup;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/FwupAssetGroup;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/FwupAssetGroup;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 326
    new-instance v0, Lsquareup/spe/FwupAssetGroup$Builder;

    invoke-direct {v0}, Lsquareup/spe/FwupAssetGroup$Builder;-><init>()V

    .line 327
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 328
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 342
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 340
    :pswitch_0
    sget-object v3, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v3}, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu1_fw_b(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;

    goto :goto_0

    .line 339
    :pswitch_1
    sget-object v3, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v3}, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu1_fw_a(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;

    goto :goto_0

    .line 338
    :pswitch_2
    sget-object v3, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v3}, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu0_fw_b(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;

    goto :goto_0

    .line 337
    :pswitch_3
    sget-object v3, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v3}, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu0_fw_a(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;

    goto :goto_0

    .line 336
    :pswitch_4
    sget-object v3, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v3}, Lsquareup/spe/FwupAssetGroup$Builder;->crq_gt4_fw(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;

    goto :goto_0

    .line 335
    :pswitch_5
    iget-object v3, v0, Lsquareup/spe/FwupAssetGroup$Builder;->tms_capk:Ljava/util/List;

    sget-object v4, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 334
    :pswitch_6
    sget-object v3, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v3}, Lsquareup/spe/FwupAssetGroup$Builder;->fpga(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;

    goto :goto_0

    .line 333
    :pswitch_7
    sget-object v3, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v3}, Lsquareup/spe/FwupAssetGroup$Builder;->k400_cpu1_fw(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;

    goto :goto_0

    .line 332
    :pswitch_8
    sget-object v3, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v3}, Lsquareup/spe/FwupAssetGroup$Builder;->k400_cpu0_fw(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;

    goto :goto_0

    .line 331
    :pswitch_9
    sget-object v3, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v3}, Lsquareup/spe/FwupAssetGroup$Builder;->k21_fw_b(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;

    goto/16 :goto_0

    .line 330
    :pswitch_a
    sget-object v3, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v3}, Lsquareup/spe/FwupAssetGroup$Builder;->k21_fw_a(Lsquareup/spe/FwupAssetDescriptor;)Lsquareup/spe/FwupAssetGroup$Builder;

    goto/16 :goto_0

    .line 346
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/spe/FwupAssetGroup$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 347
    invoke-virtual {v0}, Lsquareup/spe/FwupAssetGroup$Builder;->build()Lsquareup/spe/FwupAssetGroup;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 287
    check-cast p2, Lsquareup/spe/FwupAssetGroup;

    invoke-virtual {p0, p1, p2}, Lsquareup/spe/FwupAssetGroup$ProtoAdapter_FwupAssetGroup;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/FwupAssetGroup;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/FwupAssetGroup;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 310
    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/FwupAssetGroup;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 311
    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/FwupAssetGroup;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 312
    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/FwupAssetGroup;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 313
    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/FwupAssetGroup;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 314
    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/FwupAssetGroup;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 315
    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lsquareup/spe/FwupAssetGroup;->tms_capk:Ljava/util/List;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 316
    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/FwupAssetGroup;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 317
    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 318
    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 319
    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 320
    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 321
    invoke-virtual {p2}, Lsquareup/spe/FwupAssetGroup;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 287
    check-cast p1, Lsquareup/spe/FwupAssetGroup;

    invoke-virtual {p0, p1}, Lsquareup/spe/FwupAssetGroup$ProtoAdapter_FwupAssetGroup;->encodedSize(Lsquareup/spe/FwupAssetGroup;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/spe/FwupAssetGroup;)I
    .locals 4

    .line 294
    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/FwupAssetGroup;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/FwupAssetGroup;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    const/4 v3, 0x2

    .line 295
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/FwupAssetGroup;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    const/4 v3, 0x3

    .line 296
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/FwupAssetGroup;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    const/4 v3, 0x4

    .line 297
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/FwupAssetGroup;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    const/4 v3, 0x5

    .line 298
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 299
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lsquareup/spe/FwupAssetGroup;->tms_capk:Ljava/util/List;

    const/4 v3, 0x6

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/FwupAssetGroup;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    const/4 v3, 0x7

    .line 300
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    const/16 v3, 0x8

    .line 301
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/FwupAssetGroup;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    const/16 v3, 0x9

    .line 302
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    const/16 v3, 0xa

    .line 303
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/FwupAssetGroup;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    const/16 v3, 0xb

    .line 304
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 305
    invoke-virtual {p1}, Lsquareup/spe/FwupAssetGroup;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 287
    check-cast p1, Lsquareup/spe/FwupAssetGroup;

    invoke-virtual {p0, p1}, Lsquareup/spe/FwupAssetGroup$ProtoAdapter_FwupAssetGroup;->redact(Lsquareup/spe/FwupAssetGroup;)Lsquareup/spe/FwupAssetGroup;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/spe/FwupAssetGroup;)Lsquareup/spe/FwupAssetGroup;
    .locals 2

    .line 352
    invoke-virtual {p1}, Lsquareup/spe/FwupAssetGroup;->newBuilder()Lsquareup/spe/FwupAssetGroup$Builder;

    move-result-object p1

    .line 353
    iget-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v0, :cond_0

    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/FwupAssetDescriptor;

    iput-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k21_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    .line 354
    :cond_0
    iget-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v0, :cond_1

    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/FwupAssetDescriptor;

    iput-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k21_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    .line 355
    :cond_1
    iget-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v0, :cond_2

    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/FwupAssetDescriptor;

    iput-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k400_cpu0_fw:Lsquareup/spe/FwupAssetDescriptor;

    .line 356
    :cond_2
    iget-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v0, :cond_3

    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/FwupAssetDescriptor;

    iput-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k400_cpu1_fw:Lsquareup/spe/FwupAssetDescriptor;

    .line 357
    :cond_3
    iget-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v0, :cond_4

    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/FwupAssetGroup$Builder;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/FwupAssetDescriptor;

    iput-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->fpga:Lsquareup/spe/FwupAssetDescriptor;

    .line 358
    :cond_4
    iget-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->tms_capk:Ljava/util/List;

    sget-object v1, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 359
    iget-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v0, :cond_5

    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/FwupAssetGroup$Builder;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/FwupAssetDescriptor;

    iput-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->crq_gt4_fw:Lsquareup/spe/FwupAssetDescriptor;

    .line 360
    :cond_5
    iget-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v0, :cond_6

    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/FwupAssetDescriptor;

    iput-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu0_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    .line 361
    :cond_6
    iget-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v0, :cond_7

    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/FwupAssetDescriptor;

    iput-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu0_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    .line 362
    :cond_7
    iget-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v0, :cond_8

    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/FwupAssetDescriptor;

    iput-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu1_fw_a:Lsquareup/spe/FwupAssetDescriptor;

    .line 363
    :cond_8
    iget-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    if-eqz v0, :cond_9

    sget-object v0, Lsquareup/spe/FwupAssetDescriptor;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/FwupAssetDescriptor;

    iput-object v0, p1, Lsquareup/spe/FwupAssetGroup$Builder;->k450_cpu1_fw_b:Lsquareup/spe/FwupAssetDescriptor;

    .line 364
    :cond_9
    invoke-virtual {p1}, Lsquareup/spe/FwupAssetGroup$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 365
    invoke-virtual {p1}, Lsquareup/spe/FwupAssetGroup$Builder;->build()Lsquareup/spe/FwupAssetGroup;

    move-result-object p1

    return-object p1
.end method
