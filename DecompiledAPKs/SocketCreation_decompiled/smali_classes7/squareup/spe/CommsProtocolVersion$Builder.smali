.class public final Lsquareup/spe/CommsProtocolVersion$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CommsProtocolVersion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/CommsProtocolVersion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/spe/CommsProtocolVersion;",
        "Lsquareup/spe/CommsProtocolVersion$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public app:Ljava/lang/Integer;

.field public ep:Ljava/lang/Integer;

.field public transport:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public app(Ljava/lang/Integer;)Lsquareup/spe/CommsProtocolVersion$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lsquareup/spe/CommsProtocolVersion$Builder;->app:Ljava/lang/Integer;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lsquareup/spe/CommsProtocolVersion$Builder;->build()Lsquareup/spe/CommsProtocolVersion;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/spe/CommsProtocolVersion;
    .locals 5

    .line 131
    new-instance v0, Lsquareup/spe/CommsProtocolVersion;

    iget-object v1, p0, Lsquareup/spe/CommsProtocolVersion$Builder;->ep:Ljava/lang/Integer;

    iget-object v2, p0, Lsquareup/spe/CommsProtocolVersion$Builder;->transport:Ljava/lang/Integer;

    iget-object v3, p0, Lsquareup/spe/CommsProtocolVersion$Builder;->app:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lsquareup/spe/CommsProtocolVersion;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public ep(Ljava/lang/Integer;)Lsquareup/spe/CommsProtocolVersion$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lsquareup/spe/CommsProtocolVersion$Builder;->ep:Ljava/lang/Integer;

    return-object p0
.end method

.method public transport(Ljava/lang/Integer;)Lsquareup/spe/CommsProtocolVersion$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lsquareup/spe/CommsProtocolVersion$Builder;->transport:Ljava/lang/Integer;

    return-object p0
.end method
