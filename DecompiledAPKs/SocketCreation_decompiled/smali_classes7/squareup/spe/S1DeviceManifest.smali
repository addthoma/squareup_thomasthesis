.class public final Lsquareup/spe/S1DeviceManifest;
.super Lcom/squareup/wire/Message;
.source "S1DeviceManifest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/S1DeviceManifest$ProtoAdapter_S1DeviceManifest;,
        Lsquareup/spe/S1DeviceManifest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/spe/S1DeviceManifest;",
        "Lsquareup/spe/S1DeviceManifest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/S1DeviceManifest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MAX_COMPRESSION_VERSION:Ljava/lang/Integer;

.field public static final DEFAULT_PTS_VERSION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.CommsProtocolVersion#ADAPTER"
        tag = 0x65
    .end annotation
.end field

.field public final max_compression_version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x67
    .end annotation
.end field

.field public final pts_version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x68
    .end annotation
.end field

.field public final stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.STM32F2Manifest#ADAPTER"
        tag = 0x66
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lsquareup/spe/S1DeviceManifest$ProtoAdapter_S1DeviceManifest;

    invoke-direct {v0}, Lsquareup/spe/S1DeviceManifest$ProtoAdapter_S1DeviceManifest;-><init>()V

    sput-object v0, Lsquareup/spe/S1DeviceManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 28
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lsquareup/spe/S1DeviceManifest;->DEFAULT_MAX_COMPRESSION_VERSION:Ljava/lang/Integer;

    .line 30
    sput-object v0, Lsquareup/spe/S1DeviceManifest;->DEFAULT_PTS_VERSION:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Lsquareup/spe/CommsProtocolVersion;Lsquareup/spe/STM32F2Manifest;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 6

    .line 70
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lsquareup/spe/S1DeviceManifest;-><init>(Lsquareup/spe/CommsProtocolVersion;Lsquareup/spe/STM32F2Manifest;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lsquareup/spe/CommsProtocolVersion;Lsquareup/spe/STM32F2Manifest;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 76
    sget-object v0, Lsquareup/spe/S1DeviceManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 77
    iput-object p1, p0, Lsquareup/spe/S1DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    .line 78
    iput-object p2, p0, Lsquareup/spe/S1DeviceManifest;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    .line 79
    iput-object p3, p0, Lsquareup/spe/S1DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    .line 80
    iput-object p4, p0, Lsquareup/spe/S1DeviceManifest;->pts_version:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 97
    :cond_0
    instance-of v1, p1, Lsquareup/spe/S1DeviceManifest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 98
    :cond_1
    check-cast p1, Lsquareup/spe/S1DeviceManifest;

    .line 99
    invoke-virtual {p0}, Lsquareup/spe/S1DeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/spe/S1DeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    iget-object v3, p1, Lsquareup/spe/S1DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    .line 100
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    iget-object v3, p1, Lsquareup/spe/S1DeviceManifest;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    .line 101
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    iget-object v3, p1, Lsquareup/spe/S1DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    .line 102
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->pts_version:Ljava/lang/Integer;

    iget-object p1, p1, Lsquareup/spe/S1DeviceManifest;->pts_version:Ljava/lang/Integer;

    .line 103
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 108
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 110
    invoke-virtual {p0}, Lsquareup/spe/S1DeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lsquareup/spe/CommsProtocolVersion;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lsquareup/spe/STM32F2Manifest;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->pts_version:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 115
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lsquareup/spe/S1DeviceManifest;->newBuilder()Lsquareup/spe/S1DeviceManifest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/spe/S1DeviceManifest$Builder;
    .locals 2

    .line 85
    new-instance v0, Lsquareup/spe/S1DeviceManifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/S1DeviceManifest$Builder;-><init>()V

    .line 86
    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    iput-object v1, v0, Lsquareup/spe/S1DeviceManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    .line 87
    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    iput-object v1, v0, Lsquareup/spe/S1DeviceManifest$Builder;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    .line 88
    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/S1DeviceManifest$Builder;->max_compression_version:Ljava/lang/Integer;

    .line 89
    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->pts_version:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/S1DeviceManifest$Builder;->pts_version:Ljava/lang/Integer;

    .line 90
    invoke-virtual {p0}, Lsquareup/spe/S1DeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/spe/S1DeviceManifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 123
    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    if-eqz v1, :cond_0

    const-string v1, ", comms_protocol_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124
    :cond_0
    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    if-eqz v1, :cond_1

    const-string v1, ", stm32f2_manifest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125
    :cond_1
    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", max_compression_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->max_compression_version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126
    :cond_2
    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->pts_version:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", pts_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest;->pts_version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "S1DeviceManifest{"

    .line 127
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
