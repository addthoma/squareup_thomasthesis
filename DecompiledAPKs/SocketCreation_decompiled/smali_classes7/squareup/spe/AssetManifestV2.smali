.class public final Lsquareup/spe/AssetManifestV2;
.super Lcom/squareup/wire/Message;
.source "AssetManifestV2.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/AssetManifestV2$ProtoAdapter_AssetManifestV2;,
        Lsquareup/spe/AssetManifestV2$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/spe/AssetManifestV2;",
        "Lsquareup/spe/AssetManifestV2$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/AssetManifestV2;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PERCENT_COMPLETE:Ljava/lang/Integer;

.field public static final DEFAULT_VALID:Ljava/lang/Boolean;

.field public static final DEFAULT_VERSION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final percent_complete:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x3
    .end annotation
.end field

.field public final valid:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 25
    new-instance v0, Lsquareup/spe/AssetManifestV2$ProtoAdapter_AssetManifestV2;

    invoke-direct {v0}, Lsquareup/spe/AssetManifestV2$ProtoAdapter_AssetManifestV2;-><init>()V

    sput-object v0, Lsquareup/spe/AssetManifestV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lsquareup/spe/AssetManifestV2;->DEFAULT_VERSION:Ljava/lang/Integer;

    .line 31
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lsquareup/spe/AssetManifestV2;->DEFAULT_VALID:Ljava/lang/Boolean;

    .line 33
    sput-object v1, Lsquareup/spe/AssetManifestV2;->DEFAULT_PERCENT_COMPLETE:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;)V
    .locals 1

    .line 54
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lsquareup/spe/AssetManifestV2;-><init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 59
    sget-object v0, Lsquareup/spe/AssetManifestV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 60
    iput-object p1, p0, Lsquareup/spe/AssetManifestV2;->version:Ljava/lang/Integer;

    .line 61
    iput-object p2, p0, Lsquareup/spe/AssetManifestV2;->valid:Ljava/lang/Boolean;

    .line 62
    iput-object p3, p0, Lsquareup/spe/AssetManifestV2;->percent_complete:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 78
    :cond_0
    instance-of v1, p1, Lsquareup/spe/AssetManifestV2;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 79
    :cond_1
    check-cast p1, Lsquareup/spe/AssetManifestV2;

    .line 80
    invoke-virtual {p0}, Lsquareup/spe/AssetManifestV2;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/spe/AssetManifestV2;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->version:Ljava/lang/Integer;

    iget-object v3, p1, Lsquareup/spe/AssetManifestV2;->version:Ljava/lang/Integer;

    .line 81
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->valid:Ljava/lang/Boolean;

    iget-object v3, p1, Lsquareup/spe/AssetManifestV2;->valid:Ljava/lang/Boolean;

    .line 82
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->percent_complete:Ljava/lang/Integer;

    iget-object p1, p1, Lsquareup/spe/AssetManifestV2;->percent_complete:Ljava/lang/Integer;

    .line 83
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 88
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 90
    invoke-virtual {p0}, Lsquareup/spe/AssetManifestV2;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 91
    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->version:Ljava/lang/Integer;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->valid:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 93
    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->percent_complete:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 94
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lsquareup/spe/AssetManifestV2;->newBuilder()Lsquareup/spe/AssetManifestV2$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/spe/AssetManifestV2$Builder;
    .locals 2

    .line 67
    new-instance v0, Lsquareup/spe/AssetManifestV2$Builder;

    invoke-direct {v0}, Lsquareup/spe/AssetManifestV2$Builder;-><init>()V

    .line 68
    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->version:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/AssetManifestV2$Builder;->version:Ljava/lang/Integer;

    .line 69
    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->valid:Ljava/lang/Boolean;

    iput-object v1, v0, Lsquareup/spe/AssetManifestV2$Builder;->valid:Ljava/lang/Boolean;

    .line 70
    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->percent_complete:Ljava/lang/Integer;

    iput-object v1, v0, Lsquareup/spe/AssetManifestV2$Builder;->percent_complete:Ljava/lang/Integer;

    .line 71
    invoke-virtual {p0}, Lsquareup/spe/AssetManifestV2;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/spe/AssetManifestV2$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->version:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 103
    :cond_0
    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->valid:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", valid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->valid:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 104
    :cond_1
    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->percent_complete:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", percent_complete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/AssetManifestV2;->percent_complete:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AssetManifestV2{"

    .line 105
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
