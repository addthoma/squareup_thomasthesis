.class public final Lsquareup/items/merchant/GetResponse;
.super Lcom/squareup/wire/Message;
.source "GetResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/GetResponse$ProtoAdapter_GetResponse;,
        Lsquareup/items/merchant/GetResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/GetResponse;",
        "Lsquareup/items/merchant/GetResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/GetResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATALOG_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_PAGINATION_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final attribute_definition:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.AttributeDefinition#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/AttributeDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final catalog_object:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.CatalogObject#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public final catalog_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x4
    .end annotation
.end field

.field public final pagination_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    new-instance v0, Lsquareup/items/merchant/GetResponse$ProtoAdapter_GetResponse;

    invoke-direct {v0}, Lsquareup/items/merchant/GetResponse$ProtoAdapter_GetResponse;-><init>()V

    sput-object v0, Lsquareup/items/merchant/GetResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 28
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lsquareup/items/merchant/GetResponse;->DEFAULT_CATALOG_VERSION:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/AttributeDefinition;",
            ">;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .line 71
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lsquareup/items/merchant/GetResponse;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/AttributeDefinition;",
            ">;",
            "Ljava/lang/Long;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 77
    sget-object v0, Lsquareup/items/merchant/GetResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p5, "catalog_object"

    .line 78
    invoke-static {p5, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/items/merchant/GetResponse;->catalog_object:Ljava/util/List;

    .line 79
    iput-object p2, p0, Lsquareup/items/merchant/GetResponse;->pagination_token:Ljava/lang/String;

    const-string p1, "attribute_definition"

    .line 80
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/items/merchant/GetResponse;->attribute_definition:Ljava/util/List;

    .line 81
    iput-object p4, p0, Lsquareup/items/merchant/GetResponse;->catalog_version:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 98
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/GetResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 99
    :cond_1
    check-cast p1, Lsquareup/items/merchant/GetResponse;

    .line 100
    invoke-virtual {p0}, Lsquareup/items/merchant/GetResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/GetResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->catalog_object:Ljava/util/List;

    iget-object v3, p1, Lsquareup/items/merchant/GetResponse;->catalog_object:Ljava/util/List;

    .line 101
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->pagination_token:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/GetResponse;->pagination_token:Ljava/lang/String;

    .line 102
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->attribute_definition:Ljava/util/List;

    iget-object v3, p1, Lsquareup/items/merchant/GetResponse;->attribute_definition:Ljava/util/List;

    .line 103
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->catalog_version:Ljava/lang/Long;

    iget-object p1, p1, Lsquareup/items/merchant/GetResponse;->catalog_version:Ljava/lang/Long;

    .line 104
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 109
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 111
    invoke-virtual {p0}, Lsquareup/items/merchant/GetResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->catalog_object:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->pagination_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->attribute_definition:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->catalog_version:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 116
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lsquareup/items/merchant/GetResponse;->newBuilder()Lsquareup/items/merchant/GetResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/GetResponse$Builder;
    .locals 2

    .line 86
    new-instance v0, Lsquareup/items/merchant/GetResponse$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/GetResponse$Builder;-><init>()V

    .line 87
    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->catalog_object:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/items/merchant/GetResponse$Builder;->catalog_object:Ljava/util/List;

    .line 88
    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->pagination_token:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/GetResponse$Builder;->pagination_token:Ljava/lang/String;

    .line 89
    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->attribute_definition:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/items/merchant/GetResponse$Builder;->attribute_definition:Ljava/util/List;

    .line 90
    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->catalog_version:Ljava/lang/Long;

    iput-object v1, v0, Lsquareup/items/merchant/GetResponse$Builder;->catalog_version:Ljava/lang/Long;

    .line 91
    invoke-virtual {p0}, Lsquareup/items/merchant/GetResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/GetResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->catalog_object:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", catalog_object="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->catalog_object:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125
    :cond_0
    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->pagination_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", pagination_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->pagination_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    :cond_1
    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->attribute_definition:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", attribute_definition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->attribute_definition:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 127
    :cond_2
    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->catalog_version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", catalog_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/GetResponse;->catalog_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetResponse{"

    .line 128
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
