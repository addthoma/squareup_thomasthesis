.class public final Lsquareup/items/merchant/Query$Range;
.super Lcom/squareup/wire/Message;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Range"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/Query$Range$ProtoAdapter_Range;,
        Lsquareup/items/merchant/Query$Range$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/Query$Range;",
        "Lsquareup/items/merchant/Query$Range$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/Query$Range;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BEGIN:Ljava/lang/Long;

.field public static final DEFAULT_END:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final begin:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final end:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 422
    new-instance v0, Lsquareup/items/merchant/Query$Range$ProtoAdapter_Range;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$Range$ProtoAdapter_Range;-><init>()V

    sput-object v0, Lsquareup/items/merchant/Query$Range;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 426
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lsquareup/items/merchant/Query$Range;->DEFAULT_BEGIN:Ljava/lang/Long;

    .line 428
    sput-object v0, Lsquareup/items/merchant/Query$Range;->DEFAULT_END:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 1

    .line 443
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lsquareup/items/merchant/Query$Range;-><init>(Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 447
    sget-object v0, Lsquareup/items/merchant/Query$Range;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 448
    iput-object p1, p0, Lsquareup/items/merchant/Query$Range;->begin:Ljava/lang/Long;

    .line 449
    iput-object p2, p0, Lsquareup/items/merchant/Query$Range;->end:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 464
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/Query$Range;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 465
    :cond_1
    check-cast p1, Lsquareup/items/merchant/Query$Range;

    .line 466
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$Range;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/Query$Range;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query$Range;->begin:Ljava/lang/Long;

    iget-object v3, p1, Lsquareup/items/merchant/Query$Range;->begin:Ljava/lang/Long;

    .line 467
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query$Range;->end:Ljava/lang/Long;

    iget-object p1, p1, Lsquareup/items/merchant/Query$Range;->end:Ljava/lang/Long;

    .line 468
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 473
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 475
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$Range;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 476
    iget-object v1, p0, Lsquareup/items/merchant/Query$Range;->begin:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 477
    iget-object v1, p0, Lsquareup/items/merchant/Query$Range;->end:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 478
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 421
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$Range;->newBuilder()Lsquareup/items/merchant/Query$Range$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/Query$Range$Builder;
    .locals 2

    .line 454
    new-instance v0, Lsquareup/items/merchant/Query$Range$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$Range$Builder;-><init>()V

    .line 455
    iget-object v1, p0, Lsquareup/items/merchant/Query$Range;->begin:Ljava/lang/Long;

    iput-object v1, v0, Lsquareup/items/merchant/Query$Range$Builder;->begin:Ljava/lang/Long;

    .line 456
    iget-object v1, p0, Lsquareup/items/merchant/Query$Range;->end:Ljava/lang/Long;

    iput-object v1, v0, Lsquareup/items/merchant/Query$Range$Builder;->end:Ljava/lang/Long;

    .line 457
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$Range;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/Query$Range$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 485
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 486
    iget-object v1, p0, Lsquareup/items/merchant/Query$Range;->begin:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", begin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query$Range;->begin:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 487
    :cond_0
    iget-object v1, p0, Lsquareup/items/merchant/Query$Range;->end:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", end="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query$Range;->end:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Range{"

    .line 488
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
