.class public final Lsquareup/items/merchant/DeleteRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeleteRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/DeleteRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/DeleteRequest;",
        "Lsquareup/items/merchant/DeleteRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object_token:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public lock_duration_ms:Ljava/lang/Long;

.field public lock_token:Ljava/lang/String;

.field public merchant_token:Ljava/lang/String;

.field public open_transaction:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 164
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 165
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/DeleteRequest$Builder;->catalog_object_token:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 153
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteRequest$Builder;->build()Lsquareup/items/merchant/DeleteRequest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/DeleteRequest;
    .locals 8

    .line 216
    new-instance v7, Lsquareup/items/merchant/DeleteRequest;

    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest$Builder;->catalog_object_token:Ljava/util/List;

    iget-object v2, p0, Lsquareup/items/merchant/DeleteRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v3, p0, Lsquareup/items/merchant/DeleteRequest$Builder;->open_transaction:Ljava/lang/Boolean;

    iget-object v4, p0, Lsquareup/items/merchant/DeleteRequest$Builder;->lock_token:Ljava/lang/String;

    iget-object v5, p0, Lsquareup/items/merchant/DeleteRequest$Builder;->lock_duration_ms:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lsquareup/items/merchant/DeleteRequest;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v7
.end method

.method public catalog_object_token(Ljava/util/List;)Lsquareup/items/merchant/DeleteRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lsquareup/items/merchant/DeleteRequest$Builder;"
        }
    .end annotation

    .line 169
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 170
    iput-object p1, p0, Lsquareup/items/merchant/DeleteRequest$Builder;->catalog_object_token:Ljava/util/List;

    return-object p0
.end method

.method public lock_duration_ms(Ljava/lang/Long;)Lsquareup/items/merchant/DeleteRequest$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lsquareup/items/merchant/DeleteRequest$Builder;->lock_duration_ms:Ljava/lang/Long;

    return-object p0
.end method

.method public lock_token(Ljava/lang/String;)Lsquareup/items/merchant/DeleteRequest$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lsquareup/items/merchant/DeleteRequest$Builder;->lock_token:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lsquareup/items/merchant/DeleteRequest$Builder;
    .locals 0

    .line 180
    iput-object p1, p0, Lsquareup/items/merchant/DeleteRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public open_transaction(Ljava/lang/Boolean;)Lsquareup/items/merchant/DeleteRequest$Builder;
    .locals 0

    .line 192
    iput-object p1, p0, Lsquareup/items/merchant/DeleteRequest$Builder;->open_transaction:Ljava/lang/Boolean;

    return-object p0
.end method
