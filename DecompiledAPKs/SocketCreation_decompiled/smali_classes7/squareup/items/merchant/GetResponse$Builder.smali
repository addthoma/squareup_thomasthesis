.class public final Lsquareup/items/merchant/GetResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/GetResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/GetResponse;",
        "Lsquareup/items/merchant/GetResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attribute_definition:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/AttributeDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public catalog_object:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;"
        }
    .end annotation
.end field

.field public catalog_version:Ljava/lang/Long;

.field public pagination_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 140
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 141
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/GetResponse$Builder;->catalog_object:Ljava/util/List;

    .line 142
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/GetResponse$Builder;->attribute_definition:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public attribute_definition(Ljava/util/List;)Lsquareup/items/merchant/GetResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/AttributeDefinition;",
            ">;)",
            "Lsquareup/items/merchant/GetResponse$Builder;"
        }
    .end annotation

    .line 163
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 164
    iput-object p1, p0, Lsquareup/items/merchant/GetResponse$Builder;->attribute_definition:Ljava/util/List;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 131
    invoke-virtual {p0}, Lsquareup/items/merchant/GetResponse$Builder;->build()Lsquareup/items/merchant/GetResponse;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/GetResponse;
    .locals 7

    .line 182
    new-instance v6, Lsquareup/items/merchant/GetResponse;

    iget-object v1, p0, Lsquareup/items/merchant/GetResponse$Builder;->catalog_object:Ljava/util/List;

    iget-object v2, p0, Lsquareup/items/merchant/GetResponse$Builder;->pagination_token:Ljava/lang/String;

    iget-object v3, p0, Lsquareup/items/merchant/GetResponse$Builder;->attribute_definition:Ljava/util/List;

    iget-object v4, p0, Lsquareup/items/merchant/GetResponse$Builder;->catalog_version:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lsquareup/items/merchant/GetResponse;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v6
.end method

.method public catalog_object(Ljava/util/List;)Lsquareup/items/merchant/GetResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/CatalogObject;",
            ">;)",
            "Lsquareup/items/merchant/GetResponse$Builder;"
        }
    .end annotation

    .line 146
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 147
    iput-object p1, p0, Lsquareup/items/merchant/GetResponse$Builder;->catalog_object:Ljava/util/List;

    return-object p0
.end method

.method public catalog_version(Ljava/lang/Long;)Lsquareup/items/merchant/GetResponse$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lsquareup/items/merchant/GetResponse$Builder;->catalog_version:Ljava/lang/Long;

    return-object p0
.end method

.method public pagination_token(Ljava/lang/String;)Lsquareup/items/merchant/GetResponse$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lsquareup/items/merchant/GetResponse$Builder;->pagination_token:Ljava/lang/String;

    return-object p0
.end method
