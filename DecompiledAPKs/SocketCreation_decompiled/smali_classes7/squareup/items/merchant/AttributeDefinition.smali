.class public final Lsquareup/items/merchant/AttributeDefinition;
.super Lcom/squareup/wire/Message;
.source "AttributeDefinition.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/AttributeDefinition$ProtoAdapter_AttributeDefinition;,
        Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;,
        Lsquareup/items/merchant/AttributeDefinition$AttributeClass;,
        Lsquareup/items/merchant/AttributeDefinition$Scope;,
        Lsquareup/items/merchant/AttributeDefinition$Type;,
        Lsquareup/items/merchant/AttributeDefinition$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/AttributeDefinition;",
        "Lsquareup/items/merchant/AttributeDefinition$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/AttributeDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ATTRIBUTE_CLASS:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

.field public static final DEFAULT_DISPLAY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_LEGACY_SCOPE:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_SCOPE:Lsquareup/items/merchant/AttributeDefinition$Scope;

.field public static final DEFAULT_SEARCHABLE:Ljava/lang/Boolean;

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lsquareup/items/merchant/AttributeDefinition$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final attribute_class:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.AttributeDefinition$AttributeClass#ADAPTER"
        tag = 0x1b
    .end annotation
.end field

.field public final display_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final legacy_scope:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final scope:Lsquareup/items/merchant/AttributeDefinition$Scope;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.AttributeDefinition$Scope#ADAPTER"
        tag = 0x19
    .end annotation
.end field

.field public final searchable:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final type:Lsquareup/items/merchant/AttributeDefinition$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.AttributeDefinition$Type#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.AttributeDefinition$AttributeVisibility#ADAPTER"
        tag = 0x18
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$ProtoAdapter_AttributeDefinition;

    invoke-direct {v0}, Lsquareup/items/merchant/AttributeDefinition$ProtoAdapter_AttributeDefinition;-><init>()V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lsquareup/items/merchant/AttributeDefinition$Type;->TYPE_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$Type;

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition;->DEFAULT_TYPE:Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 36
    sget-object v0, Lsquareup/items/merchant/AttributeDefinition$Scope;->SCOPE_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$Scope;

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition;->DEFAULT_SCOPE:Lsquareup/items/merchant/AttributeDefinition$Scope;

    const/4 v0, 0x0

    .line 38
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition;->DEFAULT_SEARCHABLE:Ljava/lang/Boolean;

    .line 42
    sget-object v0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->ATTRIBUTE_CLASS_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition;->DEFAULT_ATTRIBUTE_CLASS:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lsquareup/items/merchant/AttributeDefinition$Type;Ljava/lang/String;Lsquareup/items/merchant/AttributeDefinition$Scope;Ljava/lang/Boolean;Ljava/lang/String;Lsquareup/items/merchant/AttributeDefinition$AttributeClass;Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;)V
    .locals 11

    .line 104
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lsquareup/items/merchant/AttributeDefinition;-><init>(Ljava/lang/String;Ljava/lang/String;Lsquareup/items/merchant/AttributeDefinition$Type;Ljava/lang/String;Lsquareup/items/merchant/AttributeDefinition$Scope;Ljava/lang/Boolean;Ljava/lang/String;Lsquareup/items/merchant/AttributeDefinition$AttributeClass;Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lsquareup/items/merchant/AttributeDefinition$Type;Ljava/lang/String;Lsquareup/items/merchant/AttributeDefinition$Scope;Ljava/lang/Boolean;Ljava/lang/String;Lsquareup/items/merchant/AttributeDefinition$AttributeClass;Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;Lokio/ByteString;)V
    .locals 1

    .line 110
    sget-object v0, Lsquareup/items/merchant/AttributeDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 111
    iput-object p1, p0, Lsquareup/items/merchant/AttributeDefinition;->token:Ljava/lang/String;

    .line 112
    iput-object p2, p0, Lsquareup/items/merchant/AttributeDefinition;->name:Ljava/lang/String;

    .line 113
    iput-object p3, p0, Lsquareup/items/merchant/AttributeDefinition;->type:Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 114
    iput-object p4, p0, Lsquareup/items/merchant/AttributeDefinition;->legacy_scope:Ljava/lang/String;

    .line 115
    iput-object p5, p0, Lsquareup/items/merchant/AttributeDefinition;->scope:Lsquareup/items/merchant/AttributeDefinition$Scope;

    .line 116
    iput-object p6, p0, Lsquareup/items/merchant/AttributeDefinition;->searchable:Ljava/lang/Boolean;

    .line 117
    iput-object p7, p0, Lsquareup/items/merchant/AttributeDefinition;->display_name:Ljava/lang/String;

    .line 118
    iput-object p8, p0, Lsquareup/items/merchant/AttributeDefinition;->attribute_class:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    .line 119
    iput-object p9, p0, Lsquareup/items/merchant/AttributeDefinition;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 141
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/AttributeDefinition;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 142
    :cond_1
    check-cast p1, Lsquareup/items/merchant/AttributeDefinition;

    .line 143
    invoke-virtual {p0}, Lsquareup/items/merchant/AttributeDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/AttributeDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->token:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/AttributeDefinition;->token:Ljava/lang/String;

    .line 144
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->name:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/AttributeDefinition;->name:Ljava/lang/String;

    .line 145
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->type:Lsquareup/items/merchant/AttributeDefinition$Type;

    iget-object v3, p1, Lsquareup/items/merchant/AttributeDefinition;->type:Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 146
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->legacy_scope:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/AttributeDefinition;->legacy_scope:Ljava/lang/String;

    .line 147
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->scope:Lsquareup/items/merchant/AttributeDefinition$Scope;

    iget-object v3, p1, Lsquareup/items/merchant/AttributeDefinition;->scope:Lsquareup/items/merchant/AttributeDefinition$Scope;

    .line 148
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->searchable:Ljava/lang/Boolean;

    iget-object v3, p1, Lsquareup/items/merchant/AttributeDefinition;->searchable:Ljava/lang/Boolean;

    .line 149
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->display_name:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/AttributeDefinition;->display_name:Ljava/lang/String;

    .line 150
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->attribute_class:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    iget-object v3, p1, Lsquareup/items/merchant/AttributeDefinition;->attribute_class:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    .line 151
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    iget-object p1, p1, Lsquareup/items/merchant/AttributeDefinition;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    .line 152
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 157
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_9

    .line 159
    invoke-virtual {p0}, Lsquareup/items/merchant/AttributeDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 160
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 161
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 162
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->type:Lsquareup/items/merchant/AttributeDefinition$Type;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lsquareup/items/merchant/AttributeDefinition$Type;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 163
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->legacy_scope:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 164
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->scope:Lsquareup/items/merchant/AttributeDefinition$Scope;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lsquareup/items/merchant/AttributeDefinition$Scope;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 165
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->searchable:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 166
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 167
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->attribute_class:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;->hashCode()I

    move-result v2

    :cond_8
    add-int/2addr v0, v2

    .line 169
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_9
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lsquareup/items/merchant/AttributeDefinition;->newBuilder()Lsquareup/items/merchant/AttributeDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/AttributeDefinition$Builder;
    .locals 2

    .line 124
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/AttributeDefinition$Builder;-><init>()V

    .line 125
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->token:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/AttributeDefinition$Builder;->token:Ljava/lang/String;

    .line 126
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->name:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/AttributeDefinition$Builder;->name:Ljava/lang/String;

    .line 127
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->type:Lsquareup/items/merchant/AttributeDefinition$Type;

    iput-object v1, v0, Lsquareup/items/merchant/AttributeDefinition$Builder;->type:Lsquareup/items/merchant/AttributeDefinition$Type;

    .line 128
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->legacy_scope:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/AttributeDefinition$Builder;->legacy_scope:Ljava/lang/String;

    .line 129
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->scope:Lsquareup/items/merchant/AttributeDefinition$Scope;

    iput-object v1, v0, Lsquareup/items/merchant/AttributeDefinition$Builder;->scope:Lsquareup/items/merchant/AttributeDefinition$Scope;

    .line 130
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->searchable:Ljava/lang/Boolean;

    iput-object v1, v0, Lsquareup/items/merchant/AttributeDefinition$Builder;->searchable:Ljava/lang/Boolean;

    .line 131
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->display_name:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/AttributeDefinition$Builder;->display_name:Ljava/lang/String;

    .line 132
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->attribute_class:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    iput-object v1, v0, Lsquareup/items/merchant/AttributeDefinition$Builder;->attribute_class:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    .line 133
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    iput-object v1, v0, Lsquareup/items/merchant/AttributeDefinition$Builder;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    .line 134
    invoke-virtual {p0}, Lsquareup/items/merchant/AttributeDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/AttributeDefinition$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 177
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    :cond_0
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 179
    :cond_1
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->type:Lsquareup/items/merchant/AttributeDefinition$Type;

    if-eqz v1, :cond_2

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->type:Lsquareup/items/merchant/AttributeDefinition$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 180
    :cond_2
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->legacy_scope:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", legacy_scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->legacy_scope:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    :cond_3
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->scope:Lsquareup/items/merchant/AttributeDefinition$Scope;

    if-eqz v1, :cond_4

    const-string v1, ", scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->scope:Lsquareup/items/merchant/AttributeDefinition$Scope;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 182
    :cond_4
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->searchable:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", searchable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->searchable:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 183
    :cond_5
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->display_name:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", display_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->display_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    :cond_6
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->attribute_class:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    if-eqz v1, :cond_7

    const-string v1, ", attribute_class="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->attribute_class:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 185
    :cond_7
    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    if-eqz v1, :cond_8

    const-string v1, ", visibility="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AttributeDefinition{"

    .line 186
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
