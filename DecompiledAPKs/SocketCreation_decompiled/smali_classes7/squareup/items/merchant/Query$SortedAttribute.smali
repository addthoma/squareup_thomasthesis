.class public final Lsquareup/items/merchant/Query$SortedAttribute;
.super Lcom/squareup/wire/Message;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SortedAttribute"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/Query$SortedAttribute$ProtoAdapter_SortedAttribute;,
        Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;,
        Lsquareup/items/merchant/Query$SortedAttribute$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/Query$SortedAttribute;",
        "Lsquareup/items/merchant/Query$SortedAttribute$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/Query$SortedAttribute;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_INITIAL_ATTRIBUTE_VALUE:Ljava/lang/String; = ""

.field public static final DEFAULT_SORT_ORDER:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

.field private static final serialVersionUID:J


# instance fields
.field public final initial_attribute_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final sort_order:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.Query$SortedAttribute$SortOrder#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 561
    new-instance v0, Lsquareup/items/merchant/Query$SortedAttribute$ProtoAdapter_SortedAttribute;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$SortedAttribute$ProtoAdapter_SortedAttribute;-><init>()V

    sput-object v0, Lsquareup/items/merchant/Query$SortedAttribute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 567
    sget-object v0, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;->ASCENDING:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    sput-object v0, Lsquareup/items/merchant/Query$SortedAttribute;->DEFAULT_SORT_ORDER:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;)V
    .locals 1

    .line 582
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lsquareup/items/merchant/Query$SortedAttribute;-><init>(Ljava/lang/String;Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;Lokio/ByteString;)V
    .locals 1

    .line 587
    sget-object v0, Lsquareup/items/merchant/Query$SortedAttribute;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 588
    iput-object p1, p0, Lsquareup/items/merchant/Query$SortedAttribute;->initial_attribute_value:Ljava/lang/String;

    .line 589
    iput-object p2, p0, Lsquareup/items/merchant/Query$SortedAttribute;->sort_order:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 604
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/Query$SortedAttribute;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 605
    :cond_1
    check-cast p1, Lsquareup/items/merchant/Query$SortedAttribute;

    .line 606
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$SortedAttribute;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/Query$SortedAttribute;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query$SortedAttribute;->initial_attribute_value:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/Query$SortedAttribute;->initial_attribute_value:Ljava/lang/String;

    .line 607
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query$SortedAttribute;->sort_order:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    iget-object p1, p1, Lsquareup/items/merchant/Query$SortedAttribute;->sort_order:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    .line 608
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 613
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 615
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$SortedAttribute;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 616
    iget-object v1, p0, Lsquareup/items/merchant/Query$SortedAttribute;->initial_attribute_value:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 617
    iget-object v1, p0, Lsquareup/items/merchant/Query$SortedAttribute;->sort_order:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 618
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 560
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$SortedAttribute;->newBuilder()Lsquareup/items/merchant/Query$SortedAttribute$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/Query$SortedAttribute$Builder;
    .locals 2

    .line 594
    new-instance v0, Lsquareup/items/merchant/Query$SortedAttribute$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$SortedAttribute$Builder;-><init>()V

    .line 595
    iget-object v1, p0, Lsquareup/items/merchant/Query$SortedAttribute;->initial_attribute_value:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/Query$SortedAttribute$Builder;->initial_attribute_value:Ljava/lang/String;

    .line 596
    iget-object v1, p0, Lsquareup/items/merchant/Query$SortedAttribute;->sort_order:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    iput-object v1, v0, Lsquareup/items/merchant/Query$SortedAttribute$Builder;->sort_order:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    .line 597
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$SortedAttribute;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/Query$SortedAttribute$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 625
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 626
    iget-object v1, p0, Lsquareup/items/merchant/Query$SortedAttribute;->initial_attribute_value:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", initial_attribute_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query$SortedAttribute;->initial_attribute_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 627
    :cond_0
    iget-object v1, p0, Lsquareup/items/merchant/Query$SortedAttribute;->sort_order:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    if-eqz v1, :cond_1

    const-string v1, ", sort_order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query$SortedAttribute;->sort_order:Lsquareup/items/merchant/Query$SortedAttribute$SortOrder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SortedAttribute{"

    .line 628
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
