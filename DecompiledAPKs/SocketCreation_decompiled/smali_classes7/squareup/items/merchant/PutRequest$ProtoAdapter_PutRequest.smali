.class final Lsquareup/items/merchant/PutRequest$ProtoAdapter_PutRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "PutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/PutRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PutRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/items/merchant/PutRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 295
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/items/merchant/PutRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 293
    invoke-virtual {p0, p1}, Lsquareup/items/merchant/PutRequest$ProtoAdapter_PutRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/PutRequest;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/PutRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 324
    new-instance v0, Lsquareup/items/merchant/PutRequest$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/PutRequest$Builder;-><init>()V

    .line 325
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 326
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 336
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 334
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/PutRequest$Builder;->lock_duration_ms(Ljava/lang/Long;)Lsquareup/items/merchant/PutRequest$Builder;

    goto :goto_0

    .line 333
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/PutRequest$Builder;->lock_token(Ljava/lang/String;)Lsquareup/items/merchant/PutRequest$Builder;

    goto :goto_0

    .line 332
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/PutRequest$Builder;->open_transaction(Ljava/lang/Boolean;)Lsquareup/items/merchant/PutRequest$Builder;

    goto :goto_0

    .line 331
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/PutRequest$Builder;->unit_token(Ljava/lang/String;)Lsquareup/items/merchant/PutRequest$Builder;

    goto :goto_0

    .line 330
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/PutRequest$Builder;->merchant_token(Ljava/lang/String;)Lsquareup/items/merchant/PutRequest$Builder;

    goto :goto_0

    .line 329
    :pswitch_5
    iget-object v3, v0, Lsquareup/items/merchant/PutRequest$Builder;->catalog_object:Ljava/util/List;

    sget-object v4, Lsquareup/items/merchant/CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 328
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/PutRequest$Builder;->idempotency_key(Ljava/lang/String;)Lsquareup/items/merchant/PutRequest$Builder;

    goto :goto_0

    .line 340
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/items/merchant/PutRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 341
    invoke-virtual {v0}, Lsquareup/items/merchant/PutRequest$Builder;->build()Lsquareup/items/merchant/PutRequest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 293
    check-cast p2, Lsquareup/items/merchant/PutRequest;

    invoke-virtual {p0, p1, p2}, Lsquareup/items/merchant/PutRequest$ProtoAdapter_PutRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/PutRequest;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/PutRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 312
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/PutRequest;->idempotency_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 313
    sget-object v0, Lsquareup/items/merchant/CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lsquareup/items/merchant/PutRequest;->catalog_object:Ljava/util/List;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 314
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/PutRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 315
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/PutRequest;->unit_token:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 316
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/PutRequest;->open_transaction:Ljava/lang/Boolean;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 317
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/PutRequest;->lock_token:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 318
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/PutRequest;->lock_duration_ms:Ljava/lang/Long;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 319
    invoke-virtual {p2}, Lsquareup/items/merchant/PutRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 293
    check-cast p1, Lsquareup/items/merchant/PutRequest;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/PutRequest$ProtoAdapter_PutRequest;->encodedSize(Lsquareup/items/merchant/PutRequest;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/items/merchant/PutRequest;)I
    .locals 4

    .line 300
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/PutRequest;->idempotency_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lsquareup/items/merchant/CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 301
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lsquareup/items/merchant/PutRequest;->catalog_object:Ljava/util/List;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/PutRequest;->merchant_token:Ljava/lang/String;

    const/4 v3, 0x3

    .line 302
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/PutRequest;->unit_token:Ljava/lang/String;

    const/4 v3, 0x4

    .line 303
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/PutRequest;->open_transaction:Ljava/lang/Boolean;

    const/4 v3, 0x5

    .line 304
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/PutRequest;->lock_token:Ljava/lang/String;

    const/4 v3, 0x6

    .line 305
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/PutRequest;->lock_duration_ms:Ljava/lang/Long;

    const/4 v3, 0x7

    .line 306
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 307
    invoke-virtual {p1}, Lsquareup/items/merchant/PutRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 293
    check-cast p1, Lsquareup/items/merchant/PutRequest;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/PutRequest$ProtoAdapter_PutRequest;->redact(Lsquareup/items/merchant/PutRequest;)Lsquareup/items/merchant/PutRequest;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/items/merchant/PutRequest;)Lsquareup/items/merchant/PutRequest;
    .locals 2

    .line 346
    invoke-virtual {p1}, Lsquareup/items/merchant/PutRequest;->newBuilder()Lsquareup/items/merchant/PutRequest$Builder;

    move-result-object p1

    .line 347
    iget-object v0, p1, Lsquareup/items/merchant/PutRequest$Builder;->catalog_object:Ljava/util/List;

    sget-object v1, Lsquareup/items/merchant/CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 348
    invoke-virtual {p1}, Lsquareup/items/merchant/PutRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 349
    invoke-virtual {p1}, Lsquareup/items/merchant/PutRequest$Builder;->build()Lsquareup/items/merchant/PutRequest;

    move-result-object p1

    return-object p1
.end method
