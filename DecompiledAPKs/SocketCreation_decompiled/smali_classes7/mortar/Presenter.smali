.class public abstract Lmortar/Presenter;
.super Ljava/lang/Object;
.source "Presenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private loaded:Z

.field private registration:Lmortar/bundler/Bundler;

.field private view:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    .line 28
    new-instance v0, Lmortar/Presenter$1;

    invoke-direct {v0, p0}, Lmortar/Presenter$1;-><init>(Lmortar/Presenter;)V

    iput-object v0, p0, Lmortar/Presenter;->registration:Lmortar/bundler/Bundler;

    return-void
.end method

.method static synthetic access$000(Lmortar/Presenter;)Z
    .locals 0

    .line 22
    iget-boolean p0, p0, Lmortar/Presenter;->loaded:Z

    return p0
.end method

.method static synthetic access$002(Lmortar/Presenter;Z)Z
    .locals 0

    .line 22
    iput-boolean p1, p0, Lmortar/Presenter;->loaded:Z

    return p1
.end method


# virtual methods
.method public dropView(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 93
    iget-object v0, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    .line 94
    iput-boolean p1, p0, Lmortar/Presenter;->loaded:Z

    const/4 p1, 0x0

    .line 95
    iput-object p1, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    :cond_0
    return-void

    .line 92
    :cond_1
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "dropped view must not be null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method protected abstract extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Lmortar/bundler/BundleService;"
        }
    .end annotation
.end method

.method protected getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 100
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final getView()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .line 111
    iget-object v0, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    return-object v0
.end method

.method protected final hasView()Z
    .locals 1

    .line 119
    iget-object v0, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method protected onExitScope()V
    .locals 0

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public final takeView(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    if-eqz p1, :cond_2

    .line 70
    iget-object v0, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    if-eq v0, p1, :cond_1

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p0, v0}, Lmortar/Presenter;->dropView(Ljava/lang/Object;)V

    .line 73
    :cond_0
    iput-object p1, p0, Lmortar/Presenter;->view:Ljava/lang/Object;

    .line 74
    invoke-virtual {p0, p1}, Lmortar/Presenter;->extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;

    move-result-object p1

    iget-object v0, p0, Lmortar/Presenter;->registration:Lmortar/bundler/Bundler;

    invoke-virtual {p1, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    :cond_1
    return-void

    .line 68
    :cond_2
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "new view must not be null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
