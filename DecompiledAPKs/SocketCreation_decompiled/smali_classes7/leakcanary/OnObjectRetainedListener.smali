.class public interface abstract Lleakcanary/OnObjectRetainedListener;
.super Ljava/lang/Object;
.source "OnObjectRetainedListener.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lleakcanary/OnObjectRetainedListener$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008f\u0018\u0000 \u00042\u00020\u0001:\u0001\u0004J\u0008\u0010\u0002\u001a\u00020\u0003H&\u00a8\u0006\u0005"
    }
    d2 = {
        "Lleakcanary/OnObjectRetainedListener;",
        "",
        "onObjectRetained",
        "",
        "Companion",
        "leakcanary-object-watcher"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lleakcanary/OnObjectRetainedListener$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lleakcanary/OnObjectRetainedListener$Companion;->$$INSTANCE:Lleakcanary/OnObjectRetainedListener$Companion;

    sput-object v0, Lleakcanary/OnObjectRetainedListener;->Companion:Lleakcanary/OnObjectRetainedListener$Companion;

    return-void
.end method


# virtual methods
.method public abstract onObjectRetained()V
.end method
