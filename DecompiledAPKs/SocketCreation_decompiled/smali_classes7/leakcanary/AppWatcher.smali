.class public final Lleakcanary/AppWatcher;
.super Ljava/lang/Object;
.source "AppWatcher.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lleakcanary/AppWatcher$Config;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAppWatcher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AppWatcher.kt\nleakcanary/AppWatcher\n+ 2 SharkLog.kt\nshark/SharkLog\n+ 3 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,204:1\n31#2,2:205\n33#2:209\n10894#3,2:207\n*E\n*S KotlinDebug\n*F\n+ 1 AppWatcher.kt\nleakcanary/AppWatcher\n*L\n164#1,2:205\n164#1:209\n164#1,2:207\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001:\u0001\u0018B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0004H\u0002J\u000e\u0010\u0015\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u0017R,\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00048\u0006@FX\u0087\u000e\u00a2\u0006\u0014\n\u0000\u0012\u0004\u0008\u0006\u0010\u0002\u001a\u0004\u0008\u0007\u0010\u0008\"\u0004\u0008\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u000c8F\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\rR\u0011\u0010\u000e\u001a\u00020\u000f8F\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u0019"
    }
    d2 = {
        "Lleakcanary/AppWatcher;",
        "",
        "()V",
        "newConfig",
        "Lleakcanary/AppWatcher$Config;",
        "config",
        "config$annotations",
        "getConfig",
        "()Lleakcanary/AppWatcher$Config;",
        "setConfig",
        "(Lleakcanary/AppWatcher$Config;)V",
        "isInstalled",
        "",
        "()Z",
        "objectWatcher",
        "Lleakcanary/ObjectWatcher;",
        "getObjectWatcher",
        "()Lleakcanary/ObjectWatcher;",
        "logConfigChange",
        "",
        "previousConfig",
        "manualInstall",
        "application",
        "Landroid/app/Application;",
        "Config",
        "leakcanary-object-watcher-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lleakcanary/AppWatcher;

.field private static volatile config:Lleakcanary/AppWatcher$Config;


# direct methods
.method static constructor <clinit>()V
    .locals 21

    .line 15
    new-instance v0, Lleakcanary/AppWatcher;

    invoke-direct {v0}, Lleakcanary/AppWatcher;-><init>()V

    sput-object v0, Lleakcanary/AppWatcher;->INSTANCE:Lleakcanary/AppWatcher;

    .line 153
    invoke-virtual {v0}, Lleakcanary/AppWatcher;->isInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lleakcanary/AppWatcher$Config;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    const/16 v9, 0x3f

    const/4 v10, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v10}, Lleakcanary/AppWatcher$Config;-><init>(ZZZZZJILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    :cond_0
    new-instance v0, Lleakcanary/AppWatcher$Config;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const-wide/16 v17, 0x0

    const/16 v19, 0x3e

    const/16 v20, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v20}, Lleakcanary/AppWatcher$Config;-><init>(ZZZZZJILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    sput-object v0, Lleakcanary/AppWatcher;->config:Lleakcanary/AppWatcher$Config;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic config$annotations()V
    .locals 0
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    return-void
.end method

.method public static final getConfig()Lleakcanary/AppWatcher$Config;
    .locals 1

    .line 153
    sget-object v0, Lleakcanary/AppWatcher;->config:Lleakcanary/AppWatcher$Config;

    return-object v0
.end method

.method private final logConfigChange(Lleakcanary/AppWatcher$Config;Lleakcanary/AppWatcher$Config;)V
    .locals 11

    .line 164
    sget-object v0, Lshark/SharkLog;->INSTANCE:Lshark/SharkLog;

    .line 205
    invoke-virtual {v0}, Lshark/SharkLog;->getLogger()Lshark/SharkLog$Logger;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 165
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/List;

    .line 166
    const-class v2, Lleakcanary/AppWatcher$Config;

    invoke-virtual {v2}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    const-string v3, "Config::class.java.declaredFields"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 207
    array-length v3, v2

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x1

    if-ge v4, v3, :cond_1

    aget-object v6, v2, v4

    const-string v7, "field"

    .line 167
    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 168
    invoke-virtual {v6, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 169
    invoke-virtual {v6, p2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    .line 170
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    xor-int/2addr v5, v7

    if-eqz v5, :cond_0

    .line 171
    move-object v5, v1

    check-cast v5, Ljava/util/Collection;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v6, 0x3d

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 176
    :cond_1
    move-object p1, v1

    check-cast p1, Ljava/util/Collection;

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    xor-int/2addr p1, v5

    if-eqz p1, :cond_2

    move-object v2, v1

    check-cast v2, Ljava/lang/Iterable;

    const-string p1, ", "

    move-object v3, p1

    check-cast v3, Ljava/lang/CharSequence;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x3e

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_2
    const-string p1, "no changes"

    .line 178
    :goto_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Updated AppWatcher.config: Config("

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x29

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lshark/SharkLog$Logger;->d(Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method public static final setConfig(Lleakcanary/AppWatcher$Config;)V
    .locals 2

    const-string v0, "newConfig"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    sget-object v0, Lleakcanary/AppWatcher;->config:Lleakcanary/AppWatcher$Config;

    .line 156
    sput-object p0, Lleakcanary/AppWatcher;->config:Lleakcanary/AppWatcher$Config;

    .line 157
    sget-object v1, Lleakcanary/AppWatcher;->INSTANCE:Lleakcanary/AppWatcher;

    invoke-direct {v1, v0, p0}, Lleakcanary/AppWatcher;->logConfigChange(Lleakcanary/AppWatcher$Config;Lleakcanary/AppWatcher$Config;)V

    return-void
.end method


# virtual methods
.method public final getObjectWatcher()Lleakcanary/ObjectWatcher;
    .locals 1

    .line 186
    sget-object v0, Lleakcanary/internal/InternalAppWatcher;->INSTANCE:Lleakcanary/internal/InternalAppWatcher;

    invoke-virtual {v0}, Lleakcanary/internal/InternalAppWatcher;->getObjectWatcher()Lleakcanary/ObjectWatcher;

    move-result-object v0

    return-object v0
.end method

.method public final isInstalled()Z
    .locals 1

    .line 190
    sget-object v0, Lleakcanary/internal/InternalAppWatcher;->INSTANCE:Lleakcanary/internal/InternalAppWatcher;

    invoke-virtual {v0}, Lleakcanary/internal/InternalAppWatcher;->isInstalled()Z

    move-result v0

    return v0
.end method

.method public final manualInstall(Landroid/app/Application;)V
    .locals 11

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    sget-object v0, Lleakcanary/internal/InternalAppWatcher;->INSTANCE:Lleakcanary/internal/InternalAppWatcher;

    invoke-virtual {v0, p1}, Lleakcanary/internal/InternalAppWatcher;->install(Landroid/app/Application;)V

    .line 201
    sget-object v1, Lleakcanary/AppWatcher;->config:Lleakcanary/AppWatcher$Config;

    sget-object p1, Lleakcanary/internal/InternalAppWatcher;->INSTANCE:Lleakcanary/internal/InternalAppWatcher;

    invoke-virtual {p1}, Lleakcanary/internal/InternalAppWatcher;->isDebuggableBuild()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    const/16 v9, 0x3e

    const/4 v10, 0x0

    invoke-static/range {v1 .. v10}, Lleakcanary/AppWatcher$Config;->copy$default(Lleakcanary/AppWatcher$Config;ZZZZZJILjava/lang/Object;)Lleakcanary/AppWatcher$Config;

    move-result-object p1

    invoke-static {p1}, Lleakcanary/AppWatcher;->setConfig(Lleakcanary/AppWatcher$Config;)V

    return-void
.end method
