.class final synthetic Lleakcanary/internal/InternalAppWatcher$isInstalled$1;
.super Lkotlin/jvm/internal/MutablePropertyReference0;
.source "InternalAppWatcher.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lleakcanary/internal/InternalAppWatcher;)V
    .locals 0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/MutablePropertyReference0;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lleakcanary/internal/InternalAppWatcher$isInstalled$1;->receiver:Ljava/lang/Object;

    check-cast v0, Lleakcanary/internal/InternalAppWatcher;

    .line 23
    invoke-virtual {v0}, Lleakcanary/internal/InternalAppWatcher;->getApplication()Landroid/app/Application;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "application"

    return-object v0
.end method

.method public getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lleakcanary/internal/InternalAppWatcher;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "getApplication()Landroid/app/Application;"

    return-object v0
.end method

.method public set(Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lleakcanary/internal/InternalAppWatcher$isInstalled$1;->receiver:Ljava/lang/Object;

    check-cast v0, Lleakcanary/internal/InternalAppWatcher;

    .line 23
    check-cast p1, Landroid/app/Application;

    invoke-virtual {v0, p1}, Lleakcanary/internal/InternalAppWatcher;->setApplication(Landroid/app/Application;)V

    return-void
.end method
