.class public final Lretrofit2/adapter/rxjava2/RxJava2CallAdapterExposer;
.super Ljava/lang/Object;
.source "RxJava2CallAdapterExposer.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0007\u0008\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002Jb\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u0002H\u0005\u0012\u0004\u0012\u00020\u00010\u0004\"\u0004\u0008\u0000\u0010\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u000b2\u0006\u0010\u0011\u001a\u00020\u000b\u00a8\u0006\u0012"
    }
    d2 = {
        "Lretrofit2/adapter/rxjava2/RxJava2CallAdapterExposer;",
        "",
        "()V",
        "createCallAdapter",
        "Lretrofit2/CallAdapter;",
        "R",
        "responseType",
        "Ljava/lang/reflect/Type;",
        "scheduler",
        "Lio/reactivex/Scheduler;",
        "isAsync",
        "",
        "isResult",
        "isBody",
        "isFlowable",
        "isSingle",
        "isMaybe",
        "isCompletable",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lretrofit2/adapter/rxjava2/RxJava2CallAdapterExposer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    new-instance v0, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterExposer;

    invoke-direct {v0}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterExposer;-><init>()V

    sput-object v0, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterExposer;->INSTANCE:Lretrofit2/adapter/rxjava2/RxJava2CallAdapterExposer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createCallAdapter(Ljava/lang/reflect/Type;Lio/reactivex/Scheduler;ZZZZZZZ)Lretrofit2/CallAdapter;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Lio/reactivex/Scheduler;",
            "ZZZZZZZ)",
            "Lretrofit2/CallAdapter<",
            "TR;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    const-string v0, "responseType"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lretrofit2/adapter/rxjava2/RxJava2CallAdapter;

    move-object v1, v0

    move-object v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    invoke-direct/range {v1 .. v10}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapter;-><init>(Ljava/lang/reflect/Type;Lio/reactivex/Scheduler;ZZZZZZZ)V

    check-cast v0, Lretrofit2/CallAdapter;

    return-object v0
.end method
