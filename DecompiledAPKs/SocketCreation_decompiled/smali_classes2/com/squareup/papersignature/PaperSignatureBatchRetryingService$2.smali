.class Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$2;
.super Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;
.source "PaperSignatureBatchRetryingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->submitTipAndSettleBatch(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall<",
        "Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;",
        "Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;


# direct methods
.method constructor <init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$2;->this$0:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;-><init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Ljava/lang/Object;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic createResult(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 194
    check-cast p1, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$2;->createResult(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected createResult(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;)Ljava/lang/Void;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method protected doRequest(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;)V
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$2;->this$0:Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;

    invoke-static {v0}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->access$000(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;)Lcom/squareup/server/papersignature/PaperSignatureBatchService;

    move-result-object v0

    invoke-interface {v0, p1, p0}, Lcom/squareup/server/papersignature/PaperSignatureBatchService;->submitTipAndSettleBatch(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method protected bridge synthetic doRequest(Ljava/lang/Object;)V
    .locals 0

    .line 194
    check-cast p1, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$2;->doRequest(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;)V

    return-void
.end method

.method protected getErrorMessage(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;)Ljava/lang/String;
    .locals 0

    .line 213
    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_message:Ljava/lang/String;

    return-object p1
.end method

.method protected bridge synthetic getErrorMessage(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 194
    check-cast p1, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$2;->getErrorMessage(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method protected getErrorTitle(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;)Ljava/lang/String;
    .locals 0

    .line 209
    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->error_title:Ljava/lang/String;

    return-object p1
.end method

.method protected bridge synthetic getErrorTitle(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 194
    check-cast p1, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$2;->getErrorTitle(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNextRequestBackoffSeconds(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;)I
    .locals 0

    const/4 p1, -0x1

    return p1
.end method

.method public bridge synthetic getNextRequestBackoffSeconds(Ljava/lang/Object;)I
    .locals 0

    .line 194
    check-cast p1, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$2;->getNextRequestBackoffSeconds(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;)I

    move-result p1

    return p1
.end method

.method public isSuccess(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;)Z
    .locals 1

    .line 201
    iget-object p1, p1, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;->success:Ljava/lang/Boolean;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1
.end method

.method public bridge synthetic isSuccess(Ljava/lang/Object;)Z
    .locals 0

    .line 194
    check-cast p1, Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$2;->isSuccess(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchResponse;)Z

    move-result p1

    return p1
.end method
