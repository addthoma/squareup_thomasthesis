.class public Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;
.super Ljava/lang/Object;
.source "PaperSignatureBatchRetryingService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;,
        Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;
    }
.end annotation


# static fields
.field static final MAX_NUMBER_OF_RETRIES:I = 0x2

.field static final NO_RETRY_BACKOFF:I = -0x1


# instance fields
.field private final paperSignatureBatchService:Lcom/squareup/server/papersignature/PaperSignatureBatchService;

.field private final retryExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;


# direct methods
.method public constructor <init>(Lcom/squareup/server/papersignature/PaperSignatureBatchService;Lcom/squareup/thread/executor/MainThread;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-object p1, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->paperSignatureBatchService:Lcom/squareup/server/papersignature/PaperSignatureBatchService;

    .line 155
    iput-object p2, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->retryExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;)Lcom/squareup/server/papersignature/PaperSignatureBatchService;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->paperSignatureBatchService:Lcom/squareup/server/papersignature/PaperSignatureBatchService;

    return-object p0
.end method


# virtual methods
.method countTendersAwaitingMerchantTip(Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
            "Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipResponse;",
            ">;)V"
        }
    .end annotation

    .line 224
    new-instance v0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$3;-><init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/protos/client/paper_signature/CountTendersAwaitingMerchantTipRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    .line 253
    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$3;->run()V

    return-void
.end method

.method handleResponse(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TRequest:",
            "Ljava/lang/Object;",
            "TResponse:",
            "Ljava/lang/Object;",
            "TResult:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall<",
            "TTRequest;TTResponse;TTResult;>;TTResponse;)V"
        }
    .end annotation

    .line 300
    invoke-virtual {p1, p2}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->isSuccess(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    invoke-virtual {p1, p2}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->onSuccess(Ljava/lang/Object;)V

    return-void

    .line 305
    :cond_0
    invoke-virtual {p1, p2}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->getNextRequestBackoffSeconds(Ljava/lang/Object;)I

    move-result v0

    .line 307
    invoke-virtual {p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->getNumberOfRetries()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_2

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    goto :goto_0

    .line 311
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->incrementNumberOfRetries()V

    .line 312
    iget-object p2, p0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;->retryExecutor:Lcom/squareup/thread/executor/StoppableSerialExecutor;

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    invoke-interface {p2, p1, v0, v1}, Lcom/squareup/thread/executor/StoppableSerialExecutor;->executeDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 309
    :cond_2
    :goto_0
    invoke-virtual {p1, p2}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$RetryingServerCall;->onFailure(Ljava/lang/Object;)V

    :goto_1
    return-void
.end method

.method listTendersAwaitingMerchantTip(Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipRequest;",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
            "Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipResponse;",
            ">;)V"
        }
    .end annotation

    .line 160
    new-instance v0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$1;-><init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/protos/client/paper_signature/ListTendersAwaitingMerchantTipRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    .line 188
    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$1;->run()V

    return-void
.end method

.method submitTipAndSettleBatch(Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 193
    new-instance v0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$2;-><init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/protos/client/paper_signature/SubmitTipAndSettleBatchRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    .line 219
    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$2;->run()V

    return-void
.end method

.method tenderStatus(Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;",
            "Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/paper_signature/AddTipAndSettleResponse;",
            ">;>;)V"
        }
    .end annotation

    .line 258
    new-instance v0, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$4;-><init>(Lcom/squareup/papersignature/PaperSignatureBatchRetryingService;Lcom/squareup/protos/client/paper_signature/TenderStatusRequest;Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$PaperSignatureCallback;)V

    .line 285
    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureBatchRetryingService$4;->run()V

    return-void
.end method
