.class public abstract Lcom/squareup/PosAppComponent$Module;
.super Ljava/lang/Object;
.source "PosAppComponent.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/encryption/AesGcmKeyStoreEncryptorModule;,
        Lcom/squareup/analytics/AnalyticsModule;,
        Lcom/squareup/barcodescanners/BarcodeScannersModule;,
        Lcom/squareup/cashdrawer/CashDrawerModule;,
        Lcom/squareup/loggedout/LoggedOutFeatureModule$DeviceCodeViewBindingModule;,
        Lcom/squareup/location/analytics/LocationAnalyticsModule;,
        Lcom/squareup/http/useragent/NoReaderSdkBucketBinIdModule;,
        Lcom/squareup/notificationcenterdata/logging/NotificationAnalyticsAppModule;,
        Lcom/squareup/account/PendingPreferencesCacheAppModule;,
        Lcom/squareup/ui/main/PosIntentParserModule;,
        Lcom/squareup/pushmessages/PushMessageModule;,
        Lcom/squareup/log/RemoteLoggerModule;,
        Lcom/squareup/hardware/UsbModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/PosAppComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideIsReaderSdk()Z
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method static providePaymentActivityComponent()Ljava/lang/Class;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoMap;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 120
    const-class v0, Lcom/squareup/ui/PaymentActivity$Component;

    return-object v0
.end method

.method static provideTourEducationItemsSeen(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/util/Set<",
            "Lcom/squareup/tour/Education;",
            ">;>;"
        }
    .end annotation

    .line 108
    new-instance v0, Lcom/squareup/settings/EnumSetLocalSetting;

    const-class v1, Lcom/squareup/tour/Education;

    const-string v2, "TOUR_EDUCATION_ITEMS_SEEN_PREFERENCE_KEY"

    invoke-direct {v0, p0, v2, v1}, Lcom/squareup/settings/EnumSetLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Class;)V

    return-object v0
.end method

.method static provideTourEducationItemsSeenVertical(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    const-string v0, "TOUR_EDUCATION_ITEMS_SEEN_VERTICAL_PREFERENCE_KEY"

    .line 115
    invoke-virtual {p0, v0}, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;->getStringSet(Ljava/lang/String;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method abstract bindActivityBackHandler(Lcom/squareup/ui/ResetTaskOnBack;)Lcom/squareup/ui/MainActivityBackHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindEventStreamCommonCrashLogger(Lcom/squareup/log/EventStreamCommonCrashLogger;)Lcom/squareup/log/CrashAdditionalLogger;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindLocationCanceledHandler(Lcom/squareup/ui/ResetTaskOnBack;)Lcom/squareup/ui/LocationActivityBackHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindOnboardingActivityStarter(Lcom/squareup/ui/onboarding/RealOnboardingActivityStarter;)Lcom/squareup/onboarding/OnboardingActivityStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideAppNameFormatter(Lcom/squareup/util/AddAppNameFormatter;)Lcom/squareup/util/AppNameFormatter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideFirmwareNotifications(Lcom/squareup/cardreader/dipper/StatusBarFirmwareUpdateNotificationServiceStarter;)Lcom/squareup/cardreader/dipper/FirmwareUpdateNotificationServiceStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideNightModeManager(Lcom/squareup/noho/NightModeManager;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideProductVersionCode(I)I
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideProductVersionName(Ljava/lang/String;)Ljava/lang/String;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
