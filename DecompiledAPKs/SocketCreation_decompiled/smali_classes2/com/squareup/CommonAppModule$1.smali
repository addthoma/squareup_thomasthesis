.class final Lcom/squareup/CommonAppModule$1;
.super Ljava/lang/Object;
.source "CommonAppModule.java"

# interfaces
.implements Lcom/squareup/crashnado/CrashnadoReporter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/CommonAppModule;->provideCrashnadoReporter(Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CrashReporter;Lcom/squareup/log/OhSnapLogger;)Lcom/squareup/crashnado/CrashnadoReporter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$analytics:Lcom/squareup/analytics/Analytics;

.field final synthetic val$ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field final synthetic val$reporter:Lcom/squareup/log/CrashReporter;


# direct methods
.method constructor <init>(Lcom/squareup/log/OhSnapLogger;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/CrashReporter;)V
    .locals 0

    .line 268
    iput-object p1, p0, Lcom/squareup/CommonAppModule$1;->val$ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    iput-object p2, p0, Lcom/squareup/CommonAppModule$1;->val$analytics:Lcom/squareup/analytics/Analytics;

    iput-object p3, p0, Lcom/squareup/CommonAppModule$1;->val$reporter:Lcom/squareup/log/CrashReporter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failedToInstall(Ljava/lang/Throwable;)V
    .locals 3

    .line 280
    iget-object v0, p0, Lcom/squareup/CommonAppModule$1;->val$ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CRASH_REPORTER:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 281
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not install Crashnado"

    invoke-direct {v0, v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-void
.end method

.method public logCrashnadoState(Ljava/lang/String;)V
    .locals 2

    .line 270
    iget-object v0, p0, Lcom/squareup/CommonAppModule$1;->val$ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CRASH_REPORTER:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    return-void
.end method

.method public preparingIllegalStack(Ljava/lang/String;)V
    .locals 4

    .line 285
    iget-object v0, p0, Lcom/squareup/CommonAppModule$1;->val$ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CRASH_REPORTER:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Preparing illegal stack on "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 286
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Crashnado was not installed, prepareStack called illegally. Expect an UnsatisfiedLinkError shortly."

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-void
.end method

.method public reportCrash(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 3

    .line 274
    iget-object v0, p0, Lcom/squareup/CommonAppModule$1;->val$ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v1, Lcom/squareup/log/OhSnapLogger$EventType;->CRASH_REPORTER:Lcom/squareup/log/OhSnapLogger$EventType;

    const-string v2, "Reporting previous native crash"

    invoke-interface {v0, v1, v2}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 275
    iget-object v0, p0, Lcom/squareup/CommonAppModule$1;->val$analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/analytics/event/v1/CrashEvent;->posNativeCrash(Ljava/lang/Throwable;)Lcom/squareup/analytics/event/v1/CrashEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logCrashSync(Lcom/squareup/analytics/event/v1/CrashEvent;)V

    .line 276
    iget-object v0, p0, Lcom/squareup/CommonAppModule$1;->val$reporter:Lcom/squareup/log/CrashReporter;

    const-string v1, "crashnado"

    invoke-interface {v0, p1, v1, p2}, Lcom/squareup/log/CrashReporter;->crashnadoMiniDump(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
