.class public interface abstract Lcom/squareup/RegisterAppDelegateAppComponent;
.super Ljava/lang/Object;
.source "RegisterAppDelegateComponents.kt"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderContextParent;
.implements Lcom/squareup/log/RegisterExceptionHandler$Component;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u00012\u00020\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H&\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/RegisterAppDelegateAppComponent;",
        "Lcom/squareup/cardreader/CardReaderContextParent;",
        "Lcom/squareup/log/RegisterExceptionHandler$Component;",
        "loggedInComponent",
        "Lcom/squareup/RegisterAppDelegateLoggedInComponent;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract loggedInComponent()Lcom/squareup/RegisterAppDelegateLoggedInComponent;
.end method
