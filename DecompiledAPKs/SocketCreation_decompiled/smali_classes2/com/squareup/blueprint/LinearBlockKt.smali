.class public final Lcom/squareup/blueprint/LinearBlockKt;
.super Ljava/lang/Object;
.source "LinearBlock.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0000\u001a\u0014\u0010\t\u001a\u00020\n*\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00040\u0003\"2\u0010\u0002\u001a\u00020\u0001*\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0000\u001a\u00020\u00018F@FX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0005\u0010\u0006\"\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u000b"
    }
    d2 = {
        "value",
        "",
        "fill",
        "Lcom/squareup/blueprint/Block;",
        "Lcom/squareup/blueprint/LinearBlock$Params;",
        "getFill",
        "(Lcom/squareup/blueprint/Block;)Z",
        "setFill",
        "(Lcom/squareup/blueprint/Block;Z)V",
        "definesSize",
        "",
        "blueprint-core_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final definesSize(Lcom/squareup/blueprint/Block;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "*",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$definesSize"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p0}, Lcom/squareup/blueprint/Block;->getParams()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/blueprint/LinearBlock$Params;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/blueprint/LinearBlock$Params;->setDefinesSize(Z)V

    return-void
.end method

.method public static final getFill(Lcom/squareup/blueprint/Block;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "*",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "$this$fill"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0}, Lcom/squareup/blueprint/Block;->getParams()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/blueprint/LinearBlock$Params;

    invoke-virtual {p0}, Lcom/squareup/blueprint/LinearBlock$Params;->getExtend()Z

    move-result p0

    return p0
.end method

.method public static final setFill(Lcom/squareup/blueprint/Block;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "*",
            "Lcom/squareup/blueprint/LinearBlock$Params;",
            ">;Z)V"
        }
    .end annotation

    const-string v0, "$this$fill"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-virtual {p0}, Lcom/squareup/blueprint/Block;->getParams()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/blueprint/LinearBlock$Params;

    invoke-virtual {p0, p1}, Lcom/squareup/blueprint/LinearBlock$Params;->setExtend(Z)V

    return-void
.end method
