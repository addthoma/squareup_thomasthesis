.class public abstract Lcom/squareup/blueprint/Block;
.super Ljava/lang/Object;
.source "Block.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<C:",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008&\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u00020\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J%\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00028\u00002\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0013H&\u00a2\u0006\u0002\u0010\u0015J \u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0013H&J \u0010\u001b\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0013H&J \u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001f\u001a\u00020 H&J \u0010!\u001a\u00020\u001d2\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001f\u001a\u00020\"H&J\u0018\u0010#\u001a\u00020\u00072\u000e\u0010$\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0000H\u0016J\u0010\u0010%\u001a\u00020\u001d2\u0006\u0010\u0017\u001a\u00020\u0002H&J\u0010\u0010&\u001a\u00020\u001d2\u0006\u0010\u0017\u001a\u00020\u0002H&R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\tR\u0012\u0010\n\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\tR\u0012\u0010\u000c\u001a\u00028\u0001X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/blueprint/Block;",
        "C",
        "Lcom/squareup/blueprint/UpdateContext;",
        "P",
        "",
        "()V",
        "dependableHorizontally",
        "",
        "getDependableHorizontally",
        "()Z",
        "dependableVertically",
        "getDependableVertically",
        "params",
        "getParams",
        "()Ljava/lang/Object;",
        "buildViews",
        "",
        "updateContext",
        "width",
        "",
        "height",
        "(Lcom/squareup/blueprint/UpdateContext;II)V",
        "chainHorizontally",
        "context",
        "chainInfo",
        "Lcom/squareup/blueprint/ChainInfo;",
        "previousMargin",
        "chainVertically",
        "connectHorizontally",
        "Lcom/squareup/blueprint/IdsAndMargins;",
        "previousIds",
        "align",
        "Lcom/squareup/blueprint/HorizontalAlign;",
        "connectVertically",
        "Lcom/squareup/blueprint/VerticalAlign;",
        "isSameLayoutAs",
        "block",
        "startIds",
        "topIds",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract buildViews(Lcom/squareup/blueprint/UpdateContext;II)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TC;II)V"
        }
    .end annotation
.end method

.method public abstract chainHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I
.end method

.method public abstract chainVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/ChainInfo;I)I
.end method

.method public abstract connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;
.end method

.method public abstract connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;
.end method

.method public abstract getDependableHorizontally()Z
.end method

.method public abstract getDependableVertically()Z
.end method

.method public abstract getParams()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation
.end method

.method public isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "**>;)Z"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/blueprint/Block;->getParams()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0}, Lcom/squareup/blueprint/Block;->getParams()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public abstract startIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;
.end method

.method public abstract topIds(Lcom/squareup/blueprint/UpdateContext;)Lcom/squareup/blueprint/IdsAndMargins;
.end method
