.class public final enum Lcom/squareup/blueprint/CenterBlock$Type;
.super Ljava/lang/Enum;
.source "CenterBlock.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/blueprint/CenterBlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/blueprint/CenterBlock$Type;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0007j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/blueprint/CenterBlock$Type;",
        "",
        "horizontally",
        "",
        "vertically",
        "(Ljava/lang/String;IZZ)V",
        "getHorizontally",
        "()Z",
        "getVertically",
        "HORIZONTALLY",
        "VERTICALLY",
        "BOTH",
        "blueprint-core_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/blueprint/CenterBlock$Type;

.field public static final enum BOTH:Lcom/squareup/blueprint/CenterBlock$Type;

.field public static final enum HORIZONTALLY:Lcom/squareup/blueprint/CenterBlock$Type;

.field public static final enum VERTICALLY:Lcom/squareup/blueprint/CenterBlock$Type;


# instance fields
.field private final horizontally:Z

.field private final vertically:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/blueprint/CenterBlock$Type;

    new-instance v1, Lcom/squareup/blueprint/CenterBlock$Type;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const-string v4, "HORIZONTALLY"

    .line 74
    invoke-direct {v1, v4, v2, v3, v2}, Lcom/squareup/blueprint/CenterBlock$Type;-><init>(Ljava/lang/String;IZZ)V

    sput-object v1, Lcom/squareup/blueprint/CenterBlock$Type;->HORIZONTALLY:Lcom/squareup/blueprint/CenterBlock$Type;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/blueprint/CenterBlock$Type;

    const-string v4, "VERTICALLY"

    .line 75
    invoke-direct {v1, v4, v3, v2, v3}, Lcom/squareup/blueprint/CenterBlock$Type;-><init>(Ljava/lang/String;IZZ)V

    sput-object v1, Lcom/squareup/blueprint/CenterBlock$Type;->VERTICALLY:Lcom/squareup/blueprint/CenterBlock$Type;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/blueprint/CenterBlock$Type;

    const/4 v2, 0x2

    const-string v4, "BOTH"

    .line 76
    invoke-direct {v1, v4, v2, v3, v3}, Lcom/squareup/blueprint/CenterBlock$Type;-><init>(Ljava/lang/String;IZZ)V

    sput-object v1, Lcom/squareup/blueprint/CenterBlock$Type;->BOTH:Lcom/squareup/blueprint/CenterBlock$Type;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/blueprint/CenterBlock$Type;->$VALUES:[Lcom/squareup/blueprint/CenterBlock$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .line 70
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-boolean p3, p0, Lcom/squareup/blueprint/CenterBlock$Type;->horizontally:Z

    iput-boolean p4, p0, Lcom/squareup/blueprint/CenterBlock$Type;->vertically:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/blueprint/CenterBlock$Type;
    .locals 1

    const-class v0, Lcom/squareup/blueprint/CenterBlock$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/blueprint/CenterBlock$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/blueprint/CenterBlock$Type;
    .locals 1

    sget-object v0, Lcom/squareup/blueprint/CenterBlock$Type;->$VALUES:[Lcom/squareup/blueprint/CenterBlock$Type;

    invoke-virtual {v0}, [Lcom/squareup/blueprint/CenterBlock$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/blueprint/CenterBlock$Type;

    return-object v0
.end method


# virtual methods
.method public final getHorizontally()Z
    .locals 1

    .line 71
    iget-boolean v0, p0, Lcom/squareup/blueprint/CenterBlock$Type;->horizontally:Z

    return v0
.end method

.method public final getVertically()Z
    .locals 1

    .line 72
    iget-boolean v0, p0, Lcom/squareup/blueprint/CenterBlock$Type;->vertically:Z

    return v0
.end method
