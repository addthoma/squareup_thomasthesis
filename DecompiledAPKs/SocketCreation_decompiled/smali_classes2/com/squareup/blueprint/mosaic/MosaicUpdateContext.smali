.class public final Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;
.super Lcom/squareup/blueprint/UpdateContext;
.source "MosaicUpdateContext.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMosaicUpdateContext.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MosaicUpdateContext.kt\ncom/squareup/blueprint/mosaic/MosaicUpdateContext\n*L\n1#1,157:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000O\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004*\u0001\u0013\u0018\u00002\u00020\u0001:\u0001\"B\u000f\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J$\u0010\u0015\u001a\u00020\u00162\u000c\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0019J\u0006\u0010\u001b\u001a\u00020\u0016J\u0008\u0010\u001c\u001a\u00020\u0016H\u0016J\u000e\u0010\u001d\u001a\u00020\u00162\u0006\u0010\u001e\u001a\u00020\u001fJ\u0006\u0010 \u001a\u00020\u001fJ\u0006\u0010!\u001a\u00020\u0016R\u001e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0006@BX\u0086\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u001a\u0010\n\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\r0\u000c0\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R&\u0010\u000e\u001a\u0014\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0004\u0012\u00020\r0\u000fX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0010\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0014\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
        "Lcom/squareup/blueprint/UpdateContext;",
        "constraintLayout",
        "Landroidx/constraintlayout/widget/ConstraintLayout;",
        "(Landroidx/constraintlayout/widget/ConstraintLayout;)V",
        "<set-?>",
        "",
        "canReuseConstraintSet",
        "getCanReuseConstraintSet",
        "()Z",
        "newModels",
        "",
        "Lcom/squareup/mosaic/core/UiModel;",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
        "viewList",
        "Lcom/squareup/mosaic/lists/ModelsList;",
        "getViewList$blueprint_mosaic_release",
        "()Lcom/squareup/mosaic/lists/ModelsList;",
        "viewListChanges",
        "com/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;",
        "addModel",
        "",
        "uiModel",
        "width",
        "",
        "height",
        "constraintSizes",
        "reset",
        "restoreInstanceState",
        "parcelable",
        "Landroid/os/Parcelable;",
        "saveInstanceState",
        "updateViewRefs",
        "MosaicItemParams",
        "blueprint-mosaic_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private canReuseConstraintSet:Z

.field private newModels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
            ">;>;"
        }
    .end annotation
.end field

.field private final viewList:Lcom/squareup/mosaic/lists/ModelsList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mosaic/lists/ModelsList<",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
            ">;",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
            ">;"
        }
    .end annotation
.end field

.field private final viewListChanges:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;


# direct methods
.method public constructor <init>(Landroidx/constraintlayout/widget/ConstraintLayout;)V
    .locals 2

    const-string v0, "constraintLayout"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, p1}, Lcom/squareup/blueprint/UpdateContext;-><init>(Landroidx/constraintlayout/widget/ConstraintLayout;)V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->newModels:Ljava/util/List;

    const/4 v0, 0x1

    .line 34
    iput-boolean v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->canReuseConstraintSet:Z

    .line 100
    new-instance v0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;-><init>(Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;Landroidx/constraintlayout/widget/ConstraintLayout;)V

    iput-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->viewListChanges:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;

    .line 155
    new-instance v0, Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {p1}, Landroidx/constraintlayout/widget/ConstraintLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string v1, "constraintLayout.context"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->viewListChanges:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;

    check-cast v1, Lcom/squareup/mosaic/lists/ModelsList$Changes;

    invoke-direct {v0, p1, v1}, Lcom/squareup/mosaic/lists/ModelsList;-><init>(Landroid/content/Context;Lcom/squareup/mosaic/lists/ModelsList$Changes;)V

    iput-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    return-void
.end method

.method public static final synthetic access$getCanReuseConstraintSet$p(Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;)Z
    .locals 0

    .line 17
    iget-boolean p0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->canReuseConstraintSet:Z

    return p0
.end method

.method public static final synthetic access$setCanReuseConstraintSet$p(Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;Z)V
    .locals 0

    .line 17
    iput-boolean p1, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->canReuseConstraintSet:Z

    return-void
.end method


# virtual methods
.method public final addModel(Lcom/squareup/mosaic/core/UiModel;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
            ">;II)V"
        }
    .end annotation

    const-string v0, "uiModel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p1}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    invoke-virtual {v0, p2}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->setWidth(I)V

    .line 66
    invoke-virtual {p1}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    invoke-virtual {p2, p3}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->setHeight(I)V

    .line 67
    iget-object p2, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->newModels:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final constraintSizes()V
    .locals 5

    .line 84
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->newModels:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/mosaic/core/UiModel;

    .line 85
    invoke-virtual {p0}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    invoke-virtual {v3}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->getViewId()I

    move-result v3

    invoke-virtual {v1}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    invoke-virtual {v4}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->getWidth()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainWidth(II)V

    .line 86
    invoke-virtual {p0}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->getConstraintSet()Landroidx/constraintlayout/widget/ConstraintSet;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    invoke-virtual {v3}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->getViewId()I

    move-result v3

    invoke-virtual {v1}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    invoke-virtual {v1}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->getHeight()I

    move-result v1

    invoke-virtual {v2, v3, v1}, Landroidx/constraintlayout/widget/ConstraintSet;->constrainHeight(II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final getCanReuseConstraintSet()Z
    .locals 1

    .line 34
    iget-boolean v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->canReuseConstraintSet:Z

    return v0
.end method

.method public final getViewList$blueprint_mosaic_release()Lcom/squareup/mosaic/lists/ModelsList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mosaic/lists/ModelsList<",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
            ">;",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
            ">;"
        }
    .end annotation

    .line 155
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    return-object v0
.end method

.method public reset()V
    .locals 1

    .line 51
    invoke-super {p0}, Lcom/squareup/blueprint/UpdateContext;->reset()V

    const/4 v0, 0x1

    .line 52
    iput-boolean v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->canReuseConstraintSet:Z

    return-void
.end method

.method public final restoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    const-string v0, "parcelable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {v0, p1}, Lcom/squareup/mosaic/lists/ModelsList;->restoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method public final saveInstanceState()Landroid/os/Parcelable;
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    invoke-virtual {v0}, Lcom/squareup/mosaic/lists/ModelsList;->saveInstanceState()Lcom/squareup/mosaic/lists/ModelsList$State;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    return-object v0
.end method

.method public final updateViewRefs()V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->viewList:Lcom/squareup/mosaic/lists/ModelsList;

    iget-object v1, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->newModels:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/mosaic/lists/ModelsList;->bind(Ljava/util/List;)V

    .line 78
    invoke-virtual {p0}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->constraintSizes()V

    .line 79
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->newModels:Ljava/util/List;

    return-void
.end method
