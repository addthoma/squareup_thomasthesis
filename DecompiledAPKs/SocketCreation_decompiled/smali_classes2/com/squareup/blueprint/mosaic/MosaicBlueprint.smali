.class public final Lcom/squareup/blueprint/mosaic/MosaicBlueprint;
.super Ljava/lang/Object;
.source "MosaicBlueprint.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J.\u0010\n\u001a\u00020\u00072\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00070\u00062\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\rR\u001c\u0010\u0005\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/blueprint/mosaic/MosaicBlueprint;",
        "",
        "updateContext",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
        "(Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;)V",
        "currentMain",
        "Lcom/squareup/blueprint/Block;",
        "",
        "getUpdateContext$blueprint_mosaic_release",
        "()Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
        "update",
        "main",
        "extendHorizontally",
        "",
        "extendVertically",
        "blueprint-mosaic_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private currentMain:Lcom/squareup/blueprint/Block;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/blueprint/Block<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;


# direct methods
.method public constructor <init>(Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;)V
    .locals 1

    const-string v0, "updateContext"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    return-void
.end method

.method public static synthetic update$default(Lcom/squareup/blueprint/mosaic/MosaicBlueprint;Lcom/squareup/blueprint/Block;ZZILjava/lang/Object;)V
    .locals 1

    and-int/lit8 p5, p4, 0x2

    const/4 v0, 0x1

    if-eqz p5, :cond_0

    const/4 p2, 0x1

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x1

    .line 59
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->update(Lcom/squareup/blueprint/Block;ZZ)V

    return-void
.end method


# virtual methods
.method public final getUpdateContext$blueprint_mosaic_release()Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    return-object v0
.end method

.method public final update(Lcom/squareup/blueprint/Block;ZZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/blueprint/Block<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;",
            "Lkotlin/Unit;",
            ">;ZZ)V"
        }
    .end annotation

    const-string v0, "main"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->currentMain:Lcom/squareup/blueprint/Block;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/squareup/blueprint/Block;->isSameLayoutAs(Lcom/squareup/blueprint/Block;)Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 63
    :goto_0
    iput-object p1, p0, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->currentMain:Lcom/squareup/blueprint/Block;

    .line 65
    iget-object v2, p0, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    invoke-virtual {v2}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->reset()V

    .line 68
    iget-object v2, p0, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    check-cast v2, Lcom/squareup/blueprint/UpdateContext;

    .line 67
    invoke-virtual {p1, v2, v1, v1}, Lcom/squareup/blueprint/Block;->buildViews(Lcom/squareup/blueprint/UpdateContext;II)V

    .line 73
    iget-object v2, p0, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    invoke-virtual {v2}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->updateViewRefs()V

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    invoke-virtual {v0}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->getCanReuseConstraintSet()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 81
    :cond_1
    new-instance v0, Lcom/squareup/blueprint/IdAndMargin;

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v1}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    .line 82
    new-instance v2, Lcom/squareup/blueprint/IdAndMargin;

    const/4 v3, 0x4

    invoke-direct {v2, v1, v3, v1}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    .line 83
    new-instance v3, Lcom/squareup/blueprint/IdAndMargin;

    const/4 v4, 0x6

    invoke-direct {v3, v1, v4, v1}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    .line 84
    new-instance v4, Lcom/squareup/blueprint/IdAndMargin;

    const/4 v5, 0x7

    invoke-direct {v4, v1, v5, v1}, Lcom/squareup/blueprint/IdAndMargin;-><init>(III)V

    if-eqz p2, :cond_2

    .line 86
    sget-object v1, Lcom/squareup/blueprint/HorizontalAlign;->CENTER:Lcom/squareup/blueprint/HorizontalAlign;

    goto :goto_1

    :cond_2
    sget-object v1, Lcom/squareup/blueprint/HorizontalAlign;->START:Lcom/squareup/blueprint/HorizontalAlign;

    .line 87
    :goto_1
    iget-object v5, p0, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    check-cast v5, Lcom/squareup/blueprint/UpdateContext;

    check-cast v3, Lcom/squareup/blueprint/IdsAndMargins;

    invoke-virtual {p1, v5, v3, v1}, Lcom/squareup/blueprint/Block;->connectHorizontally(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/HorizontalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object v1

    if-eqz p2, :cond_3

    .line 89
    iget-object p2, p0, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    check-cast p2, Lcom/squareup/blueprint/UpdateContext;

    invoke-interface {v1, v4, p2}, Lcom/squareup/blueprint/IdsAndMargins;->connectTo(Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V

    :cond_3
    if-eqz p3, :cond_4

    .line 92
    sget-object p2, Lcom/squareup/blueprint/VerticalAlign;->CENTER:Lcom/squareup/blueprint/VerticalAlign;

    goto :goto_2

    :cond_4
    sget-object p2, Lcom/squareup/blueprint/VerticalAlign;->TOP:Lcom/squareup/blueprint/VerticalAlign;

    .line 93
    :goto_2
    iget-object v1, p0, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    check-cast v1, Lcom/squareup/blueprint/UpdateContext;

    check-cast v0, Lcom/squareup/blueprint/IdsAndMargins;

    invoke-virtual {p1, v1, v0, p2}, Lcom/squareup/blueprint/Block;->connectVertically(Lcom/squareup/blueprint/UpdateContext;Lcom/squareup/blueprint/IdsAndMargins;Lcom/squareup/blueprint/VerticalAlign;)Lcom/squareup/blueprint/IdsAndMargins;

    move-result-object p1

    if-eqz p3, :cond_5

    .line 95
    iget-object p2, p0, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    check-cast p2, Lcom/squareup/blueprint/UpdateContext;

    invoke-interface {p1, v2, p2}, Lcom/squareup/blueprint/IdsAndMargins;->connectTo(Lcom/squareup/blueprint/IdAndMargin;Lcom/squareup/blueprint/UpdateContext;)V

    .line 98
    :cond_5
    iget-object p1, p0, Lcom/squareup/blueprint/mosaic/MosaicBlueprint;->updateContext:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    invoke-virtual {p1}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->applyConstraintSet()V

    return-void
.end method
