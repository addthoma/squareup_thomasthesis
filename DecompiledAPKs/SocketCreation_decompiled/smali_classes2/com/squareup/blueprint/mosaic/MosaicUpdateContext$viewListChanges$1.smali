.class public final Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;
.super Ljava/lang/Object;
.source "MosaicUpdateContext.kt"

# interfaces
.implements Lcom/squareup/mosaic/lists/ModelsList$Changes;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;-><init>(Landroidx/constraintlayout/widget/ConstraintLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/mosaic/lists/ModelsList$Changes<",
        "Lcom/squareup/mosaic/core/UiModel<",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMosaicUpdateContext.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MosaicUpdateContext.kt\ncom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n*L\n1#1,157:1\n1600#2,3:158\n1084#3,2:161\n*E\n*S KotlinDebug\n*F\n+ 1 MosaicUpdateContext.kt\ncom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1\n*L\n121#1,3:158\n134#1,2:161\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00003\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u0001J$\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0012\u0010\u0008\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\tH\u0016J\u0008\u0010\n\u001a\u00020\u0005H\u0016J2\u0010\u000b\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0012\u0010\u0008\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\t2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH\u0016J\u0018\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0010\u001a\u00020\u0007H\u0016J4\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0012\u001a\u00020\u00072\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0014\u001a\u00020\u00072\u000c\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\u0016\u00a8\u0006\u0016"
    }
    d2 = {
        "com/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1",
        "Lcom/squareup/mosaic/lists/ModelsList$Changes;",
        "Lcom/squareup/mosaic/core/UiModel;",
        "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
        "onChanged",
        "",
        "position",
        "",
        "newItems",
        "",
        "onCleared",
        "onInserted",
        "newAndroidViews",
        "Lkotlin/sequences/Sequence;",
        "Landroid/view/View;",
        "onRemoved",
        "count",
        "onUnchanged",
        "oldPosition",
        "oldItem",
        "newPosition",
        "newItem",
        "blueprint-mosaic_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $constraintLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

.field final synthetic this$0:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;


# direct methods
.method constructor <init>(Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;Landroidx/constraintlayout/widget/ConstraintLayout;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/constraintlayout/widget/ConstraintLayout;",
            ")V"
        }
    .end annotation

    .line 100
    iput-object p1, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;->this$0:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    iput-object p2, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;->$constraintLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged(ILjava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "newItems"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    check-cast p2, Ljava/lang/Iterable;

    .line 159
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v2, v0, 0x1

    if-gez v0, :cond_0

    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    check-cast v1, Lcom/squareup/mosaic/core/UiModel;

    .line 122
    iget-object v3, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;->$constraintLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    add-int/2addr v0, p1

    invoke-virtual {v3, v0}, Landroidx/constraintlayout/widget/ConstraintLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 123
    invoke-virtual {v1}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    const-string v3, "androidView"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->setViewId(I)V

    move v0, v2

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onCleared()V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;->$constraintLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v0}, Landroidx/constraintlayout/widget/ConstraintLayout;->removeAllViews()V

    .line 104
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;->this$0:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->access$setCanReuseConstraintSet$p(Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;Z)V

    return-void
.end method

.method public onInserted(ILjava/util/List;Lkotlin/sequences/Sequence;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
            ">;>;",
            "Lkotlin/sequences/Sequence<",
            "+",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newItems"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newAndroidViews"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;->this$0:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->access$setCanReuseConstraintSet$p(Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;Z)V

    .line 134
    check-cast p2, Ljava/lang/Iterable;

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->asSequence(Ljava/lang/Iterable;)Lkotlin/sequences/Sequence;

    move-result-object p2

    invoke-static {p2, p3}, Lkotlin/sequences/SequencesKt;->zip(Lkotlin/sequences/Sequence;Lkotlin/sequences/Sequence;)Lkotlin/sequences/Sequence;

    move-result-object p2

    .line 161
    invoke-interface {p2}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lkotlin/Pair;

    invoke-virtual {p3}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mosaic/core/UiModel;

    invoke-virtual {p3}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Landroid/view/View;

    .line 135
    iget-object v1, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;->this$0:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    invoke-virtual {v1}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->generateViewId()I

    move-result v1

    invoke-virtual {p3, v1}, Landroid/view/View;->setId(I)V

    .line 136
    invoke-virtual {v0}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    invoke-virtual {p3}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->setViewId(I)V

    .line 137
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;->$constraintLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, p3, p1}, Landroidx/constraintlayout/widget/ConstraintLayout;->addView(Landroid/view/View;I)V

    move p1, v1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onRemoved(II)V
    .locals 2

    .line 145
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;->this$0:Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;->access$setCanReuseConstraintSet$p(Lcom/squareup/blueprint/mosaic/MosaicUpdateContext;Z)V

    .line 146
    iget-object v0, p0, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;->$constraintLayout:Landroidx/constraintlayout/widget/ConstraintLayout;

    invoke-virtual {v0, p1, p2}, Landroidx/constraintlayout/widget/ConstraintLayout;->removeViews(II)V

    return-void
.end method

.method public onUnchanged(ILcom/squareup/mosaic/core/UiModel;ILcom/squareup/mosaic/core/UiModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
            ">;I",
            "Lcom/squareup/mosaic/core/UiModel<",
            "Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;",
            ">;)V"
        }
    .end annotation

    const-string p1, "oldItem"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "newItem"

    invoke-static {p4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    invoke-virtual {p4}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    invoke-virtual {p2}, Lcom/squareup/mosaic/core/UiModel;->getParams()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;

    invoke-virtual {p2}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->getViewId()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$MosaicItemParams;->setViewId(I)V

    return-void
.end method

.method public bridge synthetic onUnchanged(ILjava/lang/Object;ILjava/lang/Object;)V
    .locals 0

    .line 100
    check-cast p2, Lcom/squareup/mosaic/core/UiModel;

    check-cast p4, Lcom/squareup/mosaic/core/UiModel;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/blueprint/mosaic/MosaicUpdateContext$viewListChanges$1;->onUnchanged(ILcom/squareup/mosaic/core/UiModel;ILcom/squareup/mosaic/core/UiModel;)V

    return-void
.end method
