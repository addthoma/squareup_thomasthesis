.class Lcom/squareup/calc/TaxAndDiscountAdjuster$ZeroedMap;
.super Ljava/util/LinkedHashMap;
.source "TaxAndDiscountAdjuster.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/calc/TaxAndDiscountAdjuster;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ZeroedMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap<",
        "Ljava/lang/String;",
        "Ljava/math/BigDecimal;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 464
    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/calc/TaxAndDiscountAdjuster$1;)V
    .locals 0

    .line 464
    invoke-direct {p0}, Lcom/squareup/calc/TaxAndDiscountAdjuster$ZeroedMap;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 464
    invoke-virtual {p0, p1}, Lcom/squareup/calc/TaxAndDiscountAdjuster$ZeroedMap;->get(Ljava/lang/Object;)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method

.method public get(Ljava/lang/Object;)Ljava/math/BigDecimal;
    .locals 0

    .line 466
    invoke-super {p0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/math/BigDecimal;

    if-nez p1, :cond_0

    .line 467
    sget-object p1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    :cond_0
    return-object p1
.end method
