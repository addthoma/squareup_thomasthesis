.class public interface abstract Lcom/squareup/catalog/CatalogObjectConverter;
.super Ljava/lang/Object;
.source "CatalogObjectConverter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract createCatalogObject(Ljava/lang/Object;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lsquareup/items/merchant/CatalogObject;"
        }
    .end annotation
.end method

.method public abstract updateCatalogObject(Ljava/lang/Object;Lcom/squareup/catalog/CatalogObjectBuilder;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/squareup/catalog/CatalogObjectBuilder;",
            "Ljava/lang/String;",
            ")",
            "Lsquareup/items/merchant/CatalogObject;"
        }
    .end annotation
.end method
