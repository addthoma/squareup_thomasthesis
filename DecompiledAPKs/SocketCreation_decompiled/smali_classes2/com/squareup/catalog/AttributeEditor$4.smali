.class final Lcom/squareup/catalog/AttributeEditor$4;
.super Ljava/lang/Object;
.source "AttributeEditor.java"

# interfaces
.implements Lcom/squareup/catalog/AttributeEditor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/catalog/AttributeEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/catalog/AttributeEditor<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/Integer;
    .locals 1

    .line 51
    iget-object v0, p1, Lsquareup/items/merchant/Attribute$Builder;->int_value:Ljava/lang/Long;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object p1, p1, Lsquareup/items/merchant/Attribute$Builder;->int_value:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->intValue()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/Object;
    .locals 0

    .line 49
    invoke-virtual {p0, p1}, Lcom/squareup/catalog/AttributeEditor$4;->getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public setValue(Lsquareup/items/merchant/Attribute$Builder;Ljava/lang/Integer;)V
    .locals 2

    if-nez p2, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    .line 55
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Integer;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    :goto_0
    invoke-virtual {p1, p2}, Lsquareup/items/merchant/Attribute$Builder;->int_value(Ljava/lang/Long;)Lsquareup/items/merchant/Attribute$Builder;

    return-void
.end method

.method public bridge synthetic setValue(Lsquareup/items/merchant/Attribute$Builder;Ljava/lang/Object;)V
    .locals 0

    .line 49
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/catalog/AttributeEditor$4;->setValue(Lsquareup/items/merchant/Attribute$Builder;Ljava/lang/Integer;)V

    return-void
.end method
