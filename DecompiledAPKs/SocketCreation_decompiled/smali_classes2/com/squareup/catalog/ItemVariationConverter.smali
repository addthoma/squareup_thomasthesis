.class public Lcom/squareup/catalog/ItemVariationConverter;
.super Ljava/lang/Object;
.source "ItemVariationConverter.java"

# interfaces
.implements Lcom/squareup/catalog/CatalogObjectConverter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/catalog/ItemVariationConverter$ItemVariationConverterConfig;,
        Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/catalog/CatalogObjectConverter<",
        "Lcom/squareup/api/items/ItemVariation;",
        ">;"
    }
.end annotation


# static fields
.field private static final GLOBAL:Ljava/lang/String;


# instance fields
.field private final config:Lcom/squareup/catalog/ItemVariationConverter$ItemVariationConverterConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Lcom/squareup/catalog/ItemVariationConverter$ItemVariationConverterConfig;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/catalog/ItemVariationConverter;->config:Lcom/squareup/catalog/ItemVariationConverter$ItemVariationConverterConfig;

    return-void
.end method

.method static get()Lcom/squareup/catalog/ItemVariationConverter;
    .locals 1

    .line 55
    sget-object v0, Lcom/squareup/catalog/-$$Lambda$ItemVariationConverter$uXzUM6TJVzTSinCfI3OTmpi_1tk;->INSTANCE:Lcom/squareup/catalog/-$$Lambda$ItemVariationConverter$uXzUM6TJVzTSinCfI3OTmpi_1tk;

    invoke-static {v0}, Lcom/squareup/catalog/ItemVariationConverter;->get(Lcom/squareup/catalog/ItemVariationConverter$ItemVariationConverterConfig;)Lcom/squareup/catalog/ItemVariationConverter;

    move-result-object v0

    return-object v0
.end method

.method public static get(Lcom/squareup/catalog/ItemVariationConverter$ItemVariationConverterConfig;)Lcom/squareup/catalog/ItemVariationConverter;
    .locals 1

    .line 51
    new-instance v0, Lcom/squareup/catalog/ItemVariationConverter;

    invoke-direct {v0, p0}, Lcom/squareup/catalog/ItemVariationConverter;-><init>(Lcom/squareup/catalog/ItemVariationConverter$ItemVariationConverterConfig;)V

    return-object v0
.end method

.method private getOptionAttributeDefs()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;",
            ">;"
        }
    .end annotation

    .line 163
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 164
    sget-object v1, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_0:Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v2, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_VALUE_0:Lcom/squareup/catalog/CatalogAttributeDefinition;

    invoke-static {v1, v2}, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;->of(Lcom/squareup/catalog/CatalogAttributeDefinition;Lcom/squareup/catalog/CatalogAttributeDefinition;)Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    sget-object v1, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_1:Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v2, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_VALUE_1:Lcom/squareup/catalog/CatalogAttributeDefinition;

    invoke-static {v1, v2}, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;->of(Lcom/squareup/catalog/CatalogAttributeDefinition;Lcom/squareup/catalog/CatalogAttributeDefinition;)Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    sget-object v1, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_2:Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v2, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_VALUE_2:Lcom/squareup/catalog/CatalogAttributeDefinition;

    invoke-static {v1, v2}, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;->of(Lcom/squareup/catalog/CatalogAttributeDefinition;Lcom/squareup/catalog/CatalogAttributeDefinition;)Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    sget-object v1, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_3:Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v2, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_VALUE_3:Lcom/squareup/catalog/CatalogAttributeDefinition;

    invoke-static {v1, v2}, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;->of(Lcom/squareup/catalog/CatalogAttributeDefinition;Lcom/squareup/catalog/CatalogAttributeDefinition;)Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    sget-object v1, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_4:Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v2, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_VALUE_4:Lcom/squareup/catalog/CatalogAttributeDefinition;

    invoke-static {v1, v2}, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;->of(Lcom/squareup/catalog/CatalogAttributeDefinition;Lcom/squareup/catalog/CatalogAttributeDefinition;)Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    sget-object v1, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_5:Lcom/squareup/catalog/CatalogAttributeDefinition;

    sget-object v2, Lcom/squareup/catalog/CatalogAttributeDefinition;->ITEM_OPTION_VALUE_5:Lcom/squareup/catalog/CatalogAttributeDefinition;

    invoke-static {v1, v2}, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;->of(Lcom/squareup/catalog/CatalogAttributeDefinition;Lcom/squareup/catalog/CatalogAttributeDefinition;)Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method static synthetic lambda$get$0()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method private setGlobalAttributes(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/catalog/CatalogObjectBuilder;)V
    .locals 3

    .line 106
    sget-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->NAME:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v2

    goto :goto_0

    :cond_0
    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->name:Ljava/lang/String;

    :goto_0
    invoke-virtual {p2, v0, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    move-result-object p2

    sget-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->SKU:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->sku:Ljava/lang/String;

    .line 107
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v2

    goto :goto_1

    :cond_1
    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->sku:Ljava/lang/String;

    :goto_1
    invoke-virtual {p2, v0, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    move-result-object p2

    sget-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->ORDINAL:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->ordinal:Ljava/lang/Integer;

    .line 108
    invoke-virtual {p2, v0, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    move-result-object p2

    sget-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->MEASUREMENT_UNIT_TOKEN:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->measurement_unit_token:Ljava/lang/String;

    .line 110
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v2

    goto :goto_2

    :cond_2
    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->measurement_unit_token:Ljava/lang/String;

    .line 109
    :goto_2
    invoke-virtual {p2, v0, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    move-result-object p2

    sget-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->DURATION:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->service_duration:Ljava/lang/Long;

    .line 112
    invoke-virtual {p2, v0, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    move-result-object p2

    sget-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->OBS_AVAILABILITY:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->available_on_online_booking_site:Ljava/lang/Boolean;

    .line 113
    invoke-virtual {p2, v0, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    move-result-object p2

    sget-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->PRICE_DESCRIPTION:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->price_description:Ljava/lang/String;

    .line 115
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, v2

    goto :goto_3

    :cond_3
    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->price_description:Ljava/lang/String;

    .line 114
    :goto_3
    invoke-virtual {p2, v0, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    move-result-object p2

    sget-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->TRANSITION_TIME:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->transition_time:Ljava/lang/Long;

    .line 116
    invoke-virtual {p2, v0, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    move-result-object p2

    sget-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->NO_SHOW_FEE:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    if-nez v1, :cond_4

    move-object v1, v2

    goto :goto_4

    :cond_4
    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    iget-object v1, v1, Lcom/squareup/protos/common/dinero/Money;->cents:Ljava/lang/Long;

    .line 117
    :goto_4
    invoke-virtual {p2, v0, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    move-result-object p2

    sget-object v0, Lcom/squareup/catalog/CatalogAttributeDefinition;->NO_SHOW_FEE_CURRENCY:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    if-nez v1, :cond_5

    goto :goto_5

    :cond_5
    iget-object p1, p1, Lcom/squareup/api/items/ItemVariation;->no_show_fee_money:Lcom/squareup/protos/common/dinero/Money;

    iget-object p1, p1, Lcom/squareup/protos/common/dinero/Money;->currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 121
    invoke-virtual {p1}, Lcom/squareup/protos/common/dinero/CurrencyCode;->toString()Ljava/lang/String;

    move-result-object v2

    .line 119
    :goto_5
    invoke-virtual {p2, v0, v2}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    return-void
.end method

.method private setItemOptions(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/catalog/CatalogObjectBuilder;)V
    .locals 5

    .line 140
    invoke-direct {p0}, Lcom/squareup/catalog/ItemVariationConverter;->getOptionAttributeDefs()Ljava/util/List;

    move-result-object v0

    .line 141
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;

    .line 142
    iget-object v2, v1, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;->optionDefinition:Lcom/squareup/catalog/CatalogAttributeDefinition;

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    .line 143
    iget-object v1, v1, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;->optionValueDefinition:Lcom/squareup/catalog/CatalogAttributeDefinition;

    invoke-virtual {p2, v1, v3}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    goto :goto_0

    .line 146
    :cond_0
    iget-object v0, p1, Lcom/squareup/api/items/ItemVariation;->item_option_values:Ljava/util/List;

    if-nez v0, :cond_1

    return-void

    .line 149
    :cond_1
    invoke-direct {p0}, Lcom/squareup/catalog/ItemVariationConverter;->getOptionAttributeDefs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 150
    iget-object p1, p1, Lcom/squareup/api/items/ItemVariation;->item_option_values:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/ItemOptionValueForItemVariation;

    .line 151
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 155
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;

    .line 156
    iget-object v3, v2, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;->optionDefinition:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object v4, v1, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_id:Ljava/lang/String;

    invoke-virtual {p2, v3, v4}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    .line 157
    iget-object v2, v2, Lcom/squareup/catalog/ItemVariationConverter$OptionAttributes;->optionValueDefinition:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object v1, v1, Lcom/squareup/api/items/ItemOptionValueForItemVariation;->item_option_value_id:Ljava/lang/String;

    invoke-virtual {p2, v2, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    goto :goto_1

    .line 152
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Variation has more option values than it should be allowed to have"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    return-void
.end method

.method private setPrice(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/catalog/CatalogObjectBuilder;Ljava/lang/String;)V
    .locals 3

    .line 125
    iget-object v0, p1, Lcom/squareup/api/items/ItemVariation;->price:Lcom/squareup/protos/common/dinero/Money;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lcom/squareup/api/items/ItemVariation;->price:Lcom/squareup/protos/common/dinero/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/dinero/Money;->cents:Ljava/lang/Long;

    .line 126
    :goto_0
    iget-object v2, p1, Lcom/squareup/api/items/ItemVariation;->price:Lcom/squareup/protos/common/dinero/Money;

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    iget-object v1, p1, Lcom/squareup/api/items/ItemVariation;->price:Lcom/squareup/protos/common/dinero/Money;

    iget-object v1, v1, Lcom/squareup/protos/common/dinero/Money;->currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;

    .line 127
    invoke-virtual {v1}, Lcom/squareup/protos/common/dinero/CurrencyCode;->toString()Ljava/lang/String;

    move-result-object v1

    .line 128
    :goto_1
    sget-object v2, Lcom/squareup/catalog/ItemVariationConverter;->GLOBAL:Ljava/lang/String;

    if-ne p3, v2, :cond_2

    .line 129
    sget-object p3, Lcom/squareup/catalog/CatalogAttributeDefinition;->PRICING_TYPE:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object p1, p1, Lcom/squareup/api/items/ItemVariation;->pricing_type:Lcom/squareup/api/items/PricingType;

    invoke-virtual {p2, p3, p1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    move-result-object p1

    sget-object p2, Lcom/squareup/catalog/CatalogAttributeDefinition;->PRICE:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 130
    invoke-virtual {p1, p2, v0}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    move-result-object p1

    sget-object p2, Lcom/squareup/catalog/CatalogAttributeDefinition;->CURRENCY:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 131
    invoke-virtual {p1, p2, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    goto :goto_2

    .line 133
    :cond_2
    sget-object v2, Lcom/squareup/catalog/CatalogAttributeDefinition;->PRICING_TYPE:Lcom/squareup/catalog/CatalogAttributeDefinition;

    iget-object p1, p1, Lcom/squareup/api/items/ItemVariation;->pricing_type:Lcom/squareup/api/items/PricingType;

    invoke-virtual {p2, v2, p3, p1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setUnitOverride(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    move-result-object p1

    sget-object p2, Lcom/squareup/catalog/CatalogAttributeDefinition;->PRICE:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 134
    invoke-virtual {p1, p2, p3, v0}, Lcom/squareup/catalog/CatalogObjectBuilder;->setUnitOverride(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    move-result-object p1

    sget-object p2, Lcom/squareup/catalog/CatalogAttributeDefinition;->CURRENCY:Lcom/squareup/catalog/CatalogAttributeDefinition;

    .line 135
    invoke-virtual {p1, p2, p3, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->setUnitOverride(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    :goto_2
    return-void
.end method


# virtual methods
.method public createCatalogObject(Lcom/squareup/api/items/ItemVariation;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/ItemVariation;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lsquareup/items/merchant/CatalogObject;"
        }
    .end annotation

    if-eqz p2, :cond_2

    const-string v0, "#"

    .line 63
    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    new-instance v0, Lcom/squareup/catalog/CatalogObjectBuilder;

    new-instance v1, Lsquareup/items/merchant/CatalogObject$Builder;

    invoke-direct {v1}, Lsquareup/items/merchant/CatalogObject$Builder;-><init>()V

    sget-object v2, Lsquareup/items/merchant/CatalogObjectType;->ITEM_VARIATION:Lsquareup/items/merchant/CatalogObjectType;

    .line 69
    invoke-virtual {v1, v2}, Lsquareup/items/merchant/CatalogObject$Builder;->type(Lsquareup/items/merchant/CatalogObjectType;)Lsquareup/items/merchant/CatalogObject$Builder;

    move-result-object v1

    .line 70
    invoke-virtual {v1, p2}, Lsquareup/items/merchant/CatalogObject$Builder;->token(Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject$Builder;

    move-result-object p2

    .line 71
    invoke-virtual {p2}, Lsquareup/items/merchant/CatalogObject$Builder;->build()Lsquareup/items/merchant/CatalogObject;

    move-result-object p2

    invoke-direct {v0, p2}, Lcom/squareup/catalog/CatalogObjectBuilder;-><init>(Lsquareup/items/merchant/CatalogObject;)V

    .line 73
    sget-object p2, Lcom/squareup/catalog/CatalogAttributeDefinition;->REFERENCED_ITEM_ID:Lcom/squareup/catalog/CatalogAttributeDefinition;

    invoke-virtual {v0, p2, p4}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    .line 74
    invoke-direct {p0, p1, v0}, Lcom/squareup/catalog/ItemVariationConverter;->setGlobalAttributes(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/catalog/CatalogObjectBuilder;)V

    .line 78
    sget-object p2, Lcom/squareup/catalog/ItemVariationConverter;->GLOBAL:Ljava/lang/String;

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/catalog/ItemVariationConverter;->setPrice(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/catalog/CatalogObjectBuilder;Ljava/lang/String;)V

    .line 80
    iget-object p2, p0, Lcom/squareup/catalog/ItemVariationConverter;->config:Lcom/squareup/catalog/ItemVariationConverter$ItemVariationConverterConfig;

    invoke-interface {p2}, Lcom/squareup/catalog/ItemVariationConverter$ItemVariationConverterConfig;->canApplyItemOptions()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 81
    invoke-direct {p0, p1, v0}, Lcom/squareup/catalog/ItemVariationConverter;->setItemOptions(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/catalog/CatalogObjectBuilder;)V

    .line 85
    :cond_0
    sget-object p1, Lcom/squareup/catalog/CatalogAttributeDefinition;->COGS_ENABLED:Lcom/squareup/catalog/CatalogAttributeDefinition;

    const/4 p2, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lcom/squareup/catalog/CatalogObjectBuilder;->setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    .line 86
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 87
    sget-object p3, Lcom/squareup/catalog/CatalogAttributeDefinition;->COGS_ENABLED:Lcom/squareup/catalog/CatalogAttributeDefinition;

    const/4 p4, 0x1

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p4

    invoke-virtual {v0, p3, p2, p4}, Lcom/squareup/catalog/CatalogObjectBuilder;->setUnitOverride(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;

    goto :goto_0

    .line 90
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/catalog/CatalogObjectBuilder;->build()Lsquareup/items/merchant/CatalogObject;

    move-result-object p1

    return-object p1

    .line 64
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "The temporaryToken has to be non-null and start with \'#\'."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic createCatalogObject(Ljava/lang/Object;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/api/items/ItemVariation;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/catalog/ItemVariationConverter;->createCatalogObject(Lcom/squareup/api/items/ItemVariation;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;

    move-result-object p1

    return-object p1
.end method

.method public updateCatalogObject(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/catalog/CatalogObjectBuilder;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;
    .locals 0

    .line 96
    invoke-direct {p0, p1, p2}, Lcom/squareup/catalog/ItemVariationConverter;->setGlobalAttributes(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/catalog/CatalogObjectBuilder;)V

    .line 97
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/catalog/ItemVariationConverter;->setPrice(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/catalog/CatalogObjectBuilder;Ljava/lang/String;)V

    .line 98
    iget-object p3, p0, Lcom/squareup/catalog/ItemVariationConverter;->config:Lcom/squareup/catalog/ItemVariationConverter$ItemVariationConverterConfig;

    invoke-interface {p3}, Lcom/squareup/catalog/ItemVariationConverter$ItemVariationConverterConfig;->canApplyItemOptions()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 99
    invoke-direct {p0, p1, p2}, Lcom/squareup/catalog/ItemVariationConverter;->setItemOptions(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/catalog/CatalogObjectBuilder;)V

    .line 102
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/catalog/CatalogObjectBuilder;->build()Lsquareup/items/merchant/CatalogObject;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic updateCatalogObject(Ljava/lang/Object;Lcom/squareup/catalog/CatalogObjectBuilder;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;
    .locals 0

    .line 42
    check-cast p1, Lcom/squareup/api/items/ItemVariation;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/catalog/ItemVariationConverter;->updateCatalogObject(Lcom/squareup/api/items/ItemVariation;Lcom/squareup/catalog/CatalogObjectBuilder;Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject;

    move-result-object p1

    return-object p1
.end method
