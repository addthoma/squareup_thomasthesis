.class final Lcom/squareup/catalog/AttributeEditor$1;
.super Ljava/lang/Object;
.source "AttributeEditor.java"

# interfaces
.implements Lcom/squareup/catalog/AttributeEditor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/catalog/AttributeEditor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/catalog/AttributeEditor<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/Object;
    .locals 0

    .line 15
    invoke-virtual {p0, p1}, Lcom/squareup/catalog/AttributeEditor$1;->getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/String;
    .locals 0

    .line 17
    iget-object p1, p1, Lsquareup/items/merchant/Attribute$Builder;->string_value:Ljava/lang/String;

    return-object p1
.end method

.method public bridge synthetic setValue(Lsquareup/items/merchant/Attribute$Builder;Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/catalog/AttributeEditor$1;->setValue(Lsquareup/items/merchant/Attribute$Builder;Ljava/lang/String;)V

    return-void
.end method

.method public setValue(Lsquareup/items/merchant/Attribute$Builder;Ljava/lang/String;)V
    .locals 0

    .line 21
    invoke-virtual {p1, p2}, Lsquareup/items/merchant/Attribute$Builder;->string_value(Ljava/lang/String;)Lsquareup/items/merchant/Attribute$Builder;

    return-void
.end method
