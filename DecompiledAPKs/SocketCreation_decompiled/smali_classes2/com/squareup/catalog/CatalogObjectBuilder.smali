.class public Lcom/squareup/catalog/CatalogObjectBuilder;
.super Ljava/lang/Object;
.source "CatalogObjectBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;
    }
.end annotation


# instance fields
.field private final attributeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lsquareup/items/merchant/Attribute$Builder;",
            ">;>;"
        }
    .end annotation
.end field

.field private final catalogObject:Lsquareup/items/merchant/CatalogObject;


# direct methods
.method public constructor <init>(Lsquareup/items/merchant/CatalogObject;)V
    .locals 1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/catalog/CatalogObjectBuilder;->attributeMap:Ljava/util/Map;

    .line 34
    iput-object p1, p0, Lcom/squareup/catalog/CatalogObjectBuilder;->catalogObject:Lsquareup/items/merchant/CatalogObject;

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/catalog/CatalogObjectBuilder;->buildMapFromCatalogObject(Lsquareup/items/merchant/CatalogObject;)V

    return-void
.end method

.method private attributesList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Attribute;",
            ">;"
        }
    .end annotation

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 115
    iget-object v1, p0, Lcom/squareup/catalog/CatalogObjectBuilder;->attributeMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 116
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/items/merchant/Attribute$Builder;

    .line 117
    invoke-virtual {v3}, Lsquareup/items/merchant/Attribute$Builder;->build()Lsquareup/items/merchant/Attribute;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private buildMapFromCatalogObject(Lsquareup/items/merchant/CatalogObject;)V
    .locals 3

    .line 97
    iget-object p1, p1, Lsquareup/items/merchant/CatalogObject;->attribute:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/items/merchant/Attribute;

    .line 98
    iget-object v1, v0, Lsquareup/items/merchant/Attribute;->definition_token:Ljava/lang/String;

    .line 99
    invoke-direct {p0, v1}, Lcom/squareup/catalog/CatalogObjectBuilder;->ensureUnitAttributeMap(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 100
    iget-object v2, v0, Lsquareup/items/merchant/Attribute;->unit_token:Ljava/lang/String;

    invoke-virtual {v0}, Lsquareup/items/merchant/Attribute;->newBuilder()Lsquareup/items/merchant/Attribute$Builder;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method private ensureUnitAttributeMap(Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lsquareup/items/merchant/Attribute$Builder;",
            ">;"
        }
    .end annotation

    .line 105
    iget-object v0, p0, Lcom/squareup/catalog/CatalogObjectBuilder;->attributeMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 108
    iget-object v1, p0, Lcom/squareup/catalog/CatalogObjectBuilder;->attributeMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method


# virtual methods
.method public build()Lsquareup/items/merchant/CatalogObject;
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/catalog/CatalogObjectBuilder;->catalogObject:Lsquareup/items/merchant/CatalogObject;

    .line 91
    invoke-virtual {v0}, Lsquareup/items/merchant/CatalogObject;->newBuilder()Lsquareup/items/merchant/CatalogObject$Builder;

    move-result-object v0

    .line 92
    invoke-direct {p0}, Lcom/squareup/catalog/CatalogObjectBuilder;->attributesList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/CatalogObject$Builder;->attribute(Ljava/util/List;)Lsquareup/items/merchant/CatalogObject$Builder;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Lsquareup/items/merchant/CatalogObject$Builder;->build()Lsquareup/items/merchant/CatalogObject;

    move-result-object v0

    return-object v0
.end method

.method public getUnitValueMapForAttribute(Lcom/squareup/catalog/CatalogAttributeDefinition;)Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "TT;>;)",
            "Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap<",
            "TT;>;"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/squareup/catalog/CatalogObjectBuilder;->attributeMap:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/catalog/CatalogAttributeDefinition;->definitionToken:Ljava/lang/String;

    .line 85
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 86
    new-instance v1, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;

    iget-object p1, p1, Lcom/squareup/catalog/CatalogAttributeDefinition;->attributeEditor:Lcom/squareup/catalog/AttributeEditor;

    invoke-direct {v1, p1, v0}, Lcom/squareup/catalog/CatalogObjectBuilder$AttributeMap;-><init>(Lcom/squareup/catalog/AttributeEditor;Ljava/util/Map;)V

    return-object v1
.end method

.method public setGlobalValue(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "TT;>;TT;)",
            "Lcom/squareup/catalog/CatalogObjectBuilder;"
        }
    .end annotation

    if-nez p2, :cond_0

    .line 69
    iget-object p2, p0, Lcom/squareup/catalog/CatalogObjectBuilder;->attributeMap:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/catalog/CatalogAttributeDefinition;->definitionToken:Ljava/lang/String;

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0

    .line 73
    :cond_0
    iget-object v0, p1, Lcom/squareup/catalog/CatalogAttributeDefinition;->definitionToken:Ljava/lang/String;

    .line 74
    invoke-direct {p0, v0}, Lcom/squareup/catalog/CatalogObjectBuilder;->ensureUnitAttributeMap(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 75
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 76
    new-instance v1, Lsquareup/items/merchant/Attribute$Builder;

    invoke-direct {v1}, Lsquareup/items/merchant/Attribute$Builder;-><init>()V

    iget-object v2, p1, Lcom/squareup/catalog/CatalogAttributeDefinition;->definitionToken:Ljava/lang/String;

    .line 77
    invoke-virtual {v1, v2}, Lsquareup/items/merchant/Attribute$Builder;->definition_token(Ljava/lang/String;)Lsquareup/items/merchant/Attribute$Builder;

    move-result-object v1

    .line 78
    iget-object p1, p1, Lcom/squareup/catalog/CatalogAttributeDefinition;->attributeEditor:Lcom/squareup/catalog/AttributeEditor;

    invoke-interface {p1, v1, p2}, Lcom/squareup/catalog/AttributeEditor;->setValue(Lsquareup/items/merchant/Attribute$Builder;Ljava/lang/Object;)V

    .line 79
    sget-object p1, Lcom/squareup/catalog/CatalogUtils;->GLOBAL_DEFAULT_KEY:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setUnitOverride(Lcom/squareup/catalog/CatalogAttributeDefinition;Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/catalog/CatalogObjectBuilder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/catalog/CatalogAttributeDefinition<",
            "TT;>;",
            "Ljava/lang/String;",
            "TT;)",
            "Lcom/squareup/catalog/CatalogObjectBuilder;"
        }
    .end annotation

    const-string v0, "unitToken"

    .line 40
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 42
    iget-object v0, p1, Lcom/squareup/catalog/CatalogAttributeDefinition;->definitionToken:Ljava/lang/String;

    .line 43
    invoke-direct {p0, v0}, Lcom/squareup/catalog/CatalogObjectBuilder;->ensureUnitAttributeMap(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 45
    sget-object v1, Lcom/squareup/catalog/CatalogUtils;->GLOBAL_DEFAULT_KEY:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsquareup/items/merchant/Attribute$Builder;

    if-eqz v1, :cond_0

    .line 48
    iget-object v2, p1, Lcom/squareup/catalog/CatalogAttributeDefinition;->attributeEditor:Lcom/squareup/catalog/AttributeEditor;

    .line 49
    invoke-interface {v2, v1}, Lcom/squareup/catalog/AttributeEditor;->getValue(Lsquareup/items/merchant/Attribute$Builder;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {p3, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    invoke-interface {v0, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0

    .line 54
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lsquareup/items/merchant/Attribute$Builder;

    if-nez v1, :cond_1

    .line 56
    new-instance v1, Lsquareup/items/merchant/Attribute$Builder;

    invoke-direct {v1}, Lsquareup/items/merchant/Attribute$Builder;-><init>()V

    iget-object v2, p1, Lcom/squareup/catalog/CatalogAttributeDefinition;->definitionToken:Ljava/lang/String;

    .line 57
    invoke-virtual {v1, v2}, Lsquareup/items/merchant/Attribute$Builder;->definition_token(Ljava/lang/String;)Lsquareup/items/merchant/Attribute$Builder;

    move-result-object v1

    .line 58
    invoke-virtual {v1, p2}, Lsquareup/items/merchant/Attribute$Builder;->unit_token(Ljava/lang/String;)Lsquareup/items/merchant/Attribute$Builder;

    move-result-object v1

    .line 59
    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    :cond_1
    iget-object p1, p1, Lcom/squareup/catalog/CatalogAttributeDefinition;->attributeEditor:Lcom/squareup/catalog/AttributeEditor;

    invoke-interface {p1, v1, p3}, Lcom/squareup/catalog/AttributeEditor;->setValue(Lsquareup/items/merchant/Attribute$Builder;Ljava/lang/Object;)V

    return-object p0
.end method
