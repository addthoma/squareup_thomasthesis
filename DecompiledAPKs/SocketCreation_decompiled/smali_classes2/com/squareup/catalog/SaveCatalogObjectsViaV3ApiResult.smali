.class public Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;
.super Ljava/lang/Object;
.source "SaveCatalogObjectsViaV3ApiResult.java"


# instance fields
.field public final createdCatalogObjectMerchantCatalogReferencesByClientIds:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/MerchantCatalogObjectReference;",
            ">;"
        }
    .end annotation
.end field

.field public final success:Z


# direct methods
.method private constructor <init>(ZLjava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/MerchantCatalogObjectReference;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean p1, p0, Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;->success:Z

    .line 33
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;->createdCatalogObjectMerchantCatalogReferencesByClientIds:Ljava/util/Map;

    return-void
.end method

.method public static failure()Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;
    .locals 3

    .line 17
    new-instance v0, Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;-><init>(ZLjava/util/Map;)V

    return-object v0
.end method

.method public static success(Ljava/util/Map;)Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/api/items/MerchantCatalogObjectReference;",
            ">;)",
            "Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;"
        }
    .end annotation

    const-string v0, "A map from client Ids to server Ids for catalog objects created via V3 API"

    .line 22
    invoke-static {p0, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 24
    new-instance v0, Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Lcom/squareup/catalog/SaveCatalogObjectsViaV3ApiResult;-><init>(ZLjava/util/Map;)V

    return-object v0
.end method
