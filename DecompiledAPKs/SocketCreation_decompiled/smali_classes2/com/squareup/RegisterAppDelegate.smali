.class public Lcom/squareup/RegisterAppDelegate;
.super Ljava/lang/Object;
.source "RegisterAppDelegate.java"

# interfaces
.implements Lcom/squareup/LoggedInMortarContext;
.implements Lmortar/Scoped;
.implements Lcom/squareup/AppDelegate;
.implements Lcom/squareup/sdk/reader/internal/AppBootstrap;
.implements Lcom/squareup/mortar/AppServiceProvider;
.implements Lcom/squareup/app/HasOnCreate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/RegisterAppDelegate$NotLoggedInException;,
        Lcom/squareup/RegisterAppDelegate$ParentLoggedInComponent;,
        Lcom/squareup/RegisterAppDelegate$ParentAppComponent;
    }
.end annotation


# static fields
.field private static final BETA_BUILD_TYPE:Ljava/lang/String; = "beta"

.field protected static volatile crashReporter:Lcom/squareup/log/CrashReporter;

.field private static final processStartupUptimeMs:J


# instance fields
.field private activityListener:Lcom/squareup/ActivityListener;

.field analytics:Lcom/squareup/analytics/Analytics;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field anrChaperone:Lcom/squareup/anrchaperone/AnrChaperone;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private volatile appComponent:Lcom/squareup/RegisterAppDelegateAppComponent;

.field private final appComponentClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/RegisterAppDelegateAppComponent;",
            ">;"
        }
    .end annotation
.end field

.field appScopedItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field protected final application:Landroid/app/Application;

.field private applicationScope:Lmortar/MortarScope;

.field authenticator:Lcom/squareup/account/LegacyAuthenticator;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field bus:Lcom/squareup/badbus/BadBus;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field deepLinkHelper:Lcom/squareup/analytics/DeepLinkHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field deviceSettingsSettingsInitializer:Lcom/squareup/settings/DeviceSettingsSettingsInitializer;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final emailSupportLedgerEnabled:Z

.field eventStreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private exceptionHandler:Lcom/squareup/log/RegisterExceptionHandler;

.field foregroundServiceStarter:Lcom/squareup/foregroundservice/ForegroundServiceStarter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field isReaderSdk:Z
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private loggedInScope:Lmortar/MortarScope;

.field mainThreadBlockedLogger:Lcom/squareup/log/MainThreadBlockedLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field msFactory:Lcom/squareup/ms/MsFactory;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field notificationWrapper:Lcom/squareup/notification/NotificationWrapper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field ohSnapBusBoy:Lcom/squareup/log/OhSnapBusBoy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field ohSnapLogger:Lcom/squareup/log/OhSnapLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field persistentAccountStatusService:Lcom/squareup/accountstatus/PersistentAccountStatusService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field playServicesVersions:Lcom/squareup/firebase/versions/PlayServicesVersions;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private posBuild:Lcom/squareup/util/PosBuild;

.field queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field readerEventLogger:Lcom/squareup/log/ReaderEventLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field readerSessionIds:Lcom/squareup/log/ReaderSessionIds;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field recorderErrorReporterListener:Lcom/squareup/swipe/RecorderErrorReporterListener;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field remoteLogger:Lcom/squareup/logging/RemoteLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 125
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/RegisterAppDelegate;->processStartupUptimeMs:J

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/RegisterAppDelegateAppComponent;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 185
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/RegisterAppDelegate;-><init>(Landroid/app/Application;Ljava/lang/Class;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Ljava/lang/Class;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/RegisterAppDelegateAppComponent;",
            ">;Z)V"
        }
    .end annotation

    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    .line 193
    iput-object p2, p0, Lcom/squareup/RegisterAppDelegate;->appComponentClass:Ljava/lang/Class;

    .line 194
    iput-boolean p3, p0, Lcom/squareup/RegisterAppDelegate;->emailSupportLedgerEnabled:Z

    .line 195
    invoke-static {p1, p0}, Lcom/squareup/mortar/AppContextWrapper;->install(Landroid/app/Application;Lcom/squareup/mortar/AppServiceProvider;)V

    return-void
.end method

.method private installExceptionHandler()Lcom/squareup/log/RegisterExceptionHandler;
    .locals 7

    .line 588
    sget-object v0, Lcom/squareup/RegisterAppDelegate;->crashReporter:Lcom/squareup/log/CrashReporter;

    if-nez v0, :cond_0

    .line 589
    invoke-virtual {p0}, Lcom/squareup/RegisterAppDelegate;->createCrashReporter()Lcom/squareup/log/CrashReporter;

    move-result-object v0

    sput-object v0, Lcom/squareup/RegisterAppDelegate;->crashReporter:Lcom/squareup/log/CrashReporter;

    .line 593
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    .line 594
    sget-object v3, Lcom/squareup/RegisterAppDelegate;->crashReporter:Lcom/squareup/log/CrashReporter;

    iget-object v4, p0, Lcom/squareup/RegisterAppDelegate;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iget-object v5, p0, Lcom/squareup/RegisterAppDelegate;->posBuild:Lcom/squareup/util/PosBuild;

    iget-object v6, p0, Lcom/squareup/RegisterAppDelegate;->playServicesVersions:Lcom/squareup/firebase/versions/PlayServicesVersions;

    move-object v1, p0

    .line 595
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/RegisterAppDelegate;->createExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;Lcom/squareup/log/CrashReporter;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/PosBuild;Lcom/squareup/firebase/versions/PlayServicesVersions;)Lcom/squareup/log/RegisterExceptionHandler;

    move-result-object v0

    .line 597
    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    return-object v0

    .line 603
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/RegisterExceptionHandler;

    return-object v0
.end method

.method private jumpShipWhenAppIsSinking()V
    .locals 5

    .line 288
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v0, :cond_0

    const-string v3, "RA-13765: null resources"

    :goto_0
    const/4 v1, 0x1

    goto :goto_1

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    .line 293
    invoke-virtual {v0}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lcom/squareup/common/bootstrap/R$bool;->app_version_code_should_match:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 296
    :try_start_0
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    .line 297
    invoke-virtual {v0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v4, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    invoke-virtual {v4}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 298
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    iget-object v4, p0, Lcom/squareup/RegisterAppDelegate;->posBuild:Lcom/squareup/util/PosBuild;

    invoke-interface {v4}, Lcom/squareup/util/PosBuild;->getRegisterVersionCode()I

    move-result v4

    if-eq v0, v4, :cond_1

    const-string v0, "RA-13706: incorrect version code"
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v0

    goto :goto_0

    :catch_0
    const-string v3, "Could not get package info"

    goto :goto_0

    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 310
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 316
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 317
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/squareup/-$$Lambda$RegisterAppDelegate$WTYmXaafKY5RUvv6KRx4xo7Yc6c;

    invoke-direct {v3, p0, v0, v1}, Lcom/squareup/-$$Lambda$RegisterAppDelegate$WTYmXaafKY5RUvv6KRx4xo7Yc6c;-><init>(Lcom/squareup/RegisterAppDelegate;Ljava/lang/IllegalStateException;Ljava/util/concurrent/CountDownLatch;)V

    const-string v0, "Sq-bugsnag-appsinking-notify"

    invoke-direct {v2, v3, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 323
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    const-wide/16 v2, 0x4

    .line 329
    :try_start_1
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const-string v0, "Square"

    const-string v1, "The Android system did not correctly update the app. Restarting."

    .line 333
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0xa

    .line 337
    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    :cond_2
    return-void
.end method

.method static synthetic lambda$warmUpGson$2()V
    .locals 2

    .line 443
    invoke-static {}, Lcom/squareup/RegisterGsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v0

    const-class v1, Lcom/squareup/account/DefaultLogInResponseCache$CachedData;

    invoke-virtual {v0, v1}, Lcom/google/gson/Gson;->getAdapter(Ljava/lang/Class;)Lcom/google/gson/TypeAdapter;

    return-void
.end method

.method private logNotificationChannelAttributes(Landroid/content/Context;)V
    .locals 3

    const-string v0, "notification"

    .line 243
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/NotificationManager;

    .line 246
    invoke-virtual {p1}, Landroid/app/NotificationManager;->getNotificationChannels()Ljava/util/List;

    move-result-object p1

    .line 248
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationChannel;

    .line 249
    invoke-virtual {v0}, Landroid/app/NotificationChannel;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/notification/Channels;->getChannelById(Ljava/lang/String;)Lcom/squareup/notification/Channels;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 250
    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/analytics/event/v1/NotificationChannelEvent;

    invoke-direct {v2, v0}, Lcom/squareup/analytics/event/v1/NotificationChannelEvent;-><init>(Landroid/app/NotificationChannel;)V

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private warmUpGson()V
    .locals 3

    .line 442
    new-instance v0, Ljava/lang/Thread;

    sget-object v1, Lcom/squareup/-$$Lambda$RegisterAppDelegate$27WmXaMTocTENCu1EEweeX3SjeQ;->INSTANCE:Lcom/squareup/-$$Lambda$RegisterAppDelegate$27WmXaMTocTENCu1EEweeX3SjeQ;

    const-string v2, "Sq-warm-up-gson"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 444
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private warmUpProtoClasses()V
    .locals 5

    .line 454
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-eq v0, v1, :cond_0

    return-void

    .line 457
    :cond_0
    sget-object v0, Lcom/squareup/RegisterAppDelegate;->crashReporter:Lcom/squareup/log/CrashReporter;

    new-instance v1, Lcom/squareup/log/OhSnapEvent;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->SAMSUNG_CRASH:Lcom/squareup/log/OhSnapLogger$EventType;

    sget-object v3, Lcom/squareup/log/OhSnapEvent$Orientation;->UNKNOWN:Lcom/squareup/log/OhSnapEvent$Orientation;

    sget-object v4, Lcom/squareup/api/rpc/RequestBatch;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/log/OhSnapEvent;-><init>(Lcom/squareup/log/OhSnapLogger$EventType;Lcom/squareup/log/OhSnapEvent$Orientation;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/log/CrashReporter;->log(Lcom/squareup/log/OhSnapEvent;)V

    .line 458
    sget-object v0, Lcom/squareup/RegisterAppDelegate;->crashReporter:Lcom/squareup/log/CrashReporter;

    new-instance v1, Lcom/squareup/log/OhSnapEvent;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->SAMSUNG_CRASH:Lcom/squareup/log/OhSnapLogger$EventType;

    sget-object v3, Lcom/squareup/log/OhSnapEvent$Orientation;->UNKNOWN:Lcom/squareup/log/OhSnapEvent$Orientation;

    sget-object v4, Lcom/squareup/protos/client/flipper/GetTicketRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/log/OhSnapEvent;-><init>(Lcom/squareup/log/OhSnapLogger$EventType;Lcom/squareup/log/OhSnapEvent$Orientation;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/log/CrashReporter;->log(Lcom/squareup/log/OhSnapEvent;)V

    .line 459
    sget-object v0, Lcom/squareup/RegisterAppDelegate;->crashReporter:Lcom/squareup/log/CrashReporter;

    new-instance v1, Lcom/squareup/log/OhSnapEvent;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->SAMSUNG_CRASH:Lcom/squareup/log/OhSnapLogger$EventType;

    sget-object v3, Lcom/squareup/log/OhSnapEvent$Orientation;->UNKNOWN:Lcom/squareup/log/OhSnapEvent$Orientation;

    sget-object v4, Lcom/squareup/protos/client/flipper/GetTicketResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 460
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/log/OhSnapEvent;-><init>(Lcom/squareup/log/OhSnapLogger$EventType;Lcom/squareup/log/OhSnapEvent$Orientation;Ljava/lang/String;)V

    .line 459
    invoke-interface {v0, v1}, Lcom/squareup/log/CrashReporter;->log(Lcom/squareup/log/OhSnapEvent;)V

    .line 461
    sget-object v0, Lcom/squareup/RegisterAppDelegate;->crashReporter:Lcom/squareup/log/CrashReporter;

    new-instance v1, Lcom/squareup/log/OhSnapEvent;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->SAMSUNG_CRASH:Lcom/squareup/log/OhSnapLogger$EventType;

    sget-object v3, Lcom/squareup/log/OhSnapEvent$Orientation;->UNKNOWN:Lcom/squareup/log/OhSnapEvent$Orientation;

    sget-object v4, Lcom/squareup/protos/client/tickets/v2/ListResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/log/OhSnapEvent;-><init>(Lcom/squareup/log/OhSnapLogger$EventType;Lcom/squareup/log/OhSnapEvent$Orientation;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/log/CrashReporter;->log(Lcom/squareup/log/OhSnapEvent;)V

    .line 462
    sget-object v0, Lcom/squareup/RegisterAppDelegate;->crashReporter:Lcom/squareup/log/CrashReporter;

    new-instance v1, Lcom/squareup/log/OhSnapEvent;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->SAMSUNG_CRASH:Lcom/squareup/log/OhSnapLogger$EventType;

    sget-object v3, Lcom/squareup/log/OhSnapEvent$Orientation;->UNKNOWN:Lcom/squareup/log/OhSnapEvent$Orientation;

    sget-object v4, Lcom/squareup/protos/client/flipper/SafetyNetStartAttestationResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 463
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/log/OhSnapEvent;-><init>(Lcom/squareup/log/OhSnapLogger$EventType;Lcom/squareup/log/OhSnapEvent$Orientation;Ljava/lang/String;)V

    .line 462
    invoke-interface {v0, v1}, Lcom/squareup/log/CrashReporter;->log(Lcom/squareup/log/OhSnapEvent;)V

    return-void
.end method


# virtual methods
.method protected createAppScope()V
    .locals 9

    .line 343
    new-instance v8, Lcom/squareup/AppBootstrapModule;

    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    sget-wide v3, Lcom/squareup/RegisterAppDelegate;->processStartupUptimeMs:J

    sget-object v5, Lcom/squareup/RegisterAppDelegate;->crashReporter:Lcom/squareup/log/CrashReporter;

    iget-object v6, p0, Lcom/squareup/RegisterAppDelegate;->activityListener:Lcom/squareup/ActivityListener;

    iget-boolean v7, p0, Lcom/squareup/RegisterAppDelegate;->emailSupportLedgerEnabled:Z

    move-object v0, v8

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lcom/squareup/AppBootstrapModule;-><init>(Landroid/app/Application;Lcom/squareup/AppDelegate;JLcom/squareup/log/CrashReporter;Lcom/squareup/ActivityListener;Z)V

    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 347
    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->appComponentClass:Ljava/lang/Class;

    invoke-static {v1, v0}, Lcom/squareup/dagger/Components;->createComponent(Ljava/lang/Class;Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/RegisterAppDelegateAppComponent;

    iput-object v0, p0, Lcom/squareup/RegisterAppDelegate;->appComponent:Lcom/squareup/RegisterAppDelegateAppComponent;

    .line 349
    invoke-static {}, Lmortar/MortarScope;->buildRootScope()Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 350
    invoke-static {v0}, Lcom/squareup/leakcanary/ScopedObjectWatcher;->addService(Lmortar/MortarScope$Builder;)V

    .line 351
    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->appComponent:Lcom/squareup/RegisterAppDelegateAppComponent;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->addAsScopeService(Lmortar/MortarScope$Builder;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    .line 353
    const-class v1, Lcom/squareup/dagger/AppScope;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmortar/MortarScope$Builder;->build(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/RegisterAppDelegate;->applicationScope:Lmortar/MortarScope;

    .line 355
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->appComponent:Lcom/squareup/RegisterAppDelegateAppComponent;

    invoke-interface {v0}, Lcom/squareup/RegisterAppDelegateAppComponent;->cardReaderFactory()Lcom/squareup/cardreader/CardReaderFactory;

    move-result-object v0

    .line 356
    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->appComponent:Lcom/squareup/RegisterAppDelegateAppComponent;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderFactory;->initialize(Lcom/squareup/cardreader/CardReaderContextParent;)V

    .line 359
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->exceptionHandler:Lcom/squareup/log/RegisterExceptionHandler;

    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->appComponent:Lcom/squareup/RegisterAppDelegateAppComponent;

    invoke-virtual {v0, v1}, Lcom/squareup/log/RegisterExceptionHandler;->resolveDependencies(Lcom/squareup/log/RegisterExceptionHandler$Component;)V

    .line 360
    invoke-virtual {p0}, Lcom/squareup/RegisterAppDelegate;->inject()V

    .line 362
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->remoteLogger:Lcom/squareup/logging/RemoteLogger;

    const-string v1, "remoteLogger"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/logging/RemoteLogger;

    invoke-static {v0}, Lcom/squareup/logging/Loggers;->installRemote(Lcom/squareup/logging/RemoteLogger;)V

    .line 368
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->applicationScope:Lmortar/MortarScope;

    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->ohSnapBusBoy:Lcom/squareup/log/OhSnapBusBoy;

    iget-object v2, p0, Lcom/squareup/RegisterAppDelegate;->bus:Lcom/squareup/badbus/BadBus;

    invoke-virtual {v1, v2}, Lcom/squareup/log/OhSnapBusBoy;->registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 369
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->applicationScope:Lmortar/MortarScope;

    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->recorderErrorReporterListener:Lcom/squareup/swipe/RecorderErrorReporterListener;

    iget-object v2, p0, Lcom/squareup/RegisterAppDelegate;->bus:Lcom/squareup/badbus/BadBus;

    invoke-virtual {v1, v2}, Lcom/squareup/swipe/RecorderErrorReporterListener;->registerOnBus(Lcom/squareup/badbus/BadBus;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 372
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->applicationScope:Lmortar/MortarScope;

    invoke-virtual {v0, p0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 374
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->activityListener:Lcom/squareup/ActivityListener;

    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->mainThreadBlockedLogger:Lcom/squareup/log/MainThreadBlockedLogger;

    invoke-virtual {v0, v1}, Lcom/squareup/ActivityListener;->registerResumedPausedListener(Lcom/squareup/ActivityListener$ResumedPausedListener;)V

    .line 376
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->activityListener:Lcom/squareup/ActivityListener;

    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->foregroundServiceStarter:Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    invoke-virtual {v0, v1}, Lcom/squareup/ActivityListener;->registerResumedPausedListener(Lcom/squareup/ActivityListener$ResumedPausedListener;)V

    .line 378
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderListeners;->setReaderEventLogger(Lcom/squareup/cardreader/ReaderEventLogger;)V

    .line 379
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->readerEventLogger:Lcom/squareup/log/ReaderEventLogger;

    invoke-virtual {v0}, Lcom/squareup/log/ReaderEventLogger;->initialize()V

    return-void
.end method

.method protected createCrashReporter()Lcom/squareup/log/CrashReporter;
    .locals 2

    .line 427
    new-instance v0, Lcom/squareup/log/BugsnagCrashReporter;

    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    invoke-direct {v0, v1}, Lcom/squareup/log/BugsnagCrashReporter;-><init>(Landroid/app/Application;)V

    return-object v0
.end method

.method protected createExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;Lcom/squareup/log/CrashReporter;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/PosBuild;Lcom/squareup/firebase/versions/PlayServicesVersions;)Lcom/squareup/log/RegisterExceptionHandler;
    .locals 9

    .line 619
    new-instance v8, Lcom/squareup/log/RegisterExceptionHandler;

    invoke-interface {p4}, Lcom/squareup/util/PosBuild;->getRegisterVersionName()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    move-object v0, v8

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/squareup/log/RegisterExceptionHandler;-><init>(Ljava/lang/String;Ljava/lang/Thread$UncaughtExceptionHandler;Landroid/app/Application;Lcom/squareup/log/CrashReporter;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/util/PosBuild;Lcom/squareup/firebase/versions/PlayServicesVersions;)V

    return-object v8
.end method

.method protected destroyApplicationScope()V
    .locals 2

    .line 391
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->applicationScope:Lmortar/MortarScope;

    if-eqz v0, :cond_0

    .line 392
    invoke-virtual {v0}, Lmortar/MortarScope;->destroy()V

    const/4 v0, 0x0

    .line 393
    iput-object v0, p0, Lcom/squareup/RegisterAppDelegate;->applicationScope:Lmortar/MortarScope;

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->activityListener:Lcom/squareup/ActivityListener;

    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->mainThreadBlockedLogger:Lcom/squareup/log/MainThreadBlockedLogger;

    invoke-virtual {v0, v1}, Lcom/squareup/ActivityListener;->unregisterResumedPausedListener(Lcom/squareup/ActivityListener$ResumedPausedListener;)V

    return-void
.end method

.method public getLoggedInComponent(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 631
    invoke-virtual {p0}, Lcom/squareup/RegisterAppDelegate;->getLoggedInMortarScope()Lmortar/MortarScope;

    move-result-object v0

    .line 632
    invoke-static {v0, p1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getLoggedInMortarScope()Lmortar/MortarScope;
    .locals 1

    .line 624
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->loggedInScope:Lmortar/MortarScope;

    if-eqz v0, :cond_0

    return-object v0

    .line 625
    :cond_0
    new-instance v0, Lcom/squareup/RegisterAppDelegate$NotLoggedInException;

    invoke-direct {v0}, Lcom/squareup/RegisterAppDelegate$NotLoggedInException;-><init>()V

    throw v0
.end method

.method public getOhSnapLogger()Lcom/squareup/log/OhSnapLogger;
    .locals 1

    .line 636
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    return-object v0
.end method

.method public getProcessStartupUptime()J
    .locals 2

    .line 544
    sget-wide v0, Lcom/squareup/RegisterAppDelegate;->processStartupUptimeMs:J

    return-wide v0
.end method

.method public getSdkFactory()Lcom/squareup/sdk/reader/internal/SdkFactory;
    .locals 1

    .line 584
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->appComponent:Lcom/squareup/RegisterAppDelegateAppComponent;

    check-cast v0, Lcom/squareup/sdk/reader/internal/SdkFactory;

    return-object v0
.end method

.method public getSystemService(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    .line 403
    invoke-static {p1}, Lcom/squareup/AppDelegate$Locator;->isAppDelegateService(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p0

    :cond_0
    const/4 v0, 0x0

    if-nez p1, :cond_1

    .line 412
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v1, "No name specified for a service"

    invoke-direct {p1, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-object v0

    .line 417
    :cond_1
    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->applicationScope:Lmortar/MortarScope;

    if-eqz v1, :cond_2

    invoke-virtual {v1, p1}, Lmortar/MortarScope;->hasService(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 418
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->applicationScope:Lmortar/MortarScope;

    invoke-virtual {v0, p1}, Lmortar/MortarScope;->getService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_2
    return-object v0
.end method

.method protected inject()V
    .locals 2

    .line 386
    invoke-static {}, Lcom/squareup/mortar/AppContextWrapper;->appContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/RegisterAppDelegate$ParentAppComponent;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/RegisterAppDelegate$ParentAppComponent;

    invoke-interface {v0, p0}, Lcom/squareup/RegisterAppDelegate$ParentAppComponent;->inject(Lcom/squareup/RegisterAppDelegate;)V

    return-void
.end method

.method public isLoggedIn()Z
    .locals 1

    .line 577
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    invoke-interface {v0}, Lcom/squareup/account/LegacyAuthenticator;->isLoggedIn()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$jumpShipWhenAppIsSinking$1$RegisterAppDelegate(Ljava/lang/IllegalStateException;Ljava/util/concurrent/CountDownLatch;)V
    .locals 2

    .line 318
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/log/BugsnagCrashReporter;->createBugsnagClient(Landroid/app/Application;)Lcom/bugsnag/android/Client;

    move-result-object v0

    .line 321
    sget-object v1, Lcom/bugsnag/android/Severity;->WARNING:Lcom/bugsnag/android/Severity;

    invoke-virtual {v0, p1, v1}, Lcom/bugsnag/android/Client;->notifyBlocking(Ljava/lang/Throwable;Lcom/bugsnag/android/Severity;)V

    .line 322
    invoke-virtual {p2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    return-void
.end method

.method public synthetic lambda$onCreate$0$RegisterAppDelegate()Lkotlin/Unit;
    .locals 2

    .line 222
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterErrorName;->APPLICATION_NOT_RESPONDING:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logError(Lcom/squareup/analytics/RegisterErrorName;)V

    .line 223
    new-instance v0, Lcom/squareup/anrchaperone/ApplicationNotResponding;

    invoke-direct {v0}, Lcom/squareup/anrchaperone/ApplicationNotResponding;-><init>()V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 224
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public synthetic lambda$onEnterScope$3$RegisterAppDelegate(Lcom/squareup/dipper/events/DipperEvent$PaymentApproved;)V
    .locals 2

    .line 483
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->readerSessionIds:Lcom/squareup/log/ReaderSessionIds;

    new-instance v1, Lcom/squareup/cardreader/CardReaderId;

    .line 484
    invoke-virtual {p1}, Lcom/squareup/dipper/events/DipperEvent$PaymentApproved;->getCardReaderId()I

    move-result p1

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/CardReaderId;-><init>(I)V

    .line 483
    invoke-virtual {v0, v1}, Lcom/squareup/log/ReaderSessionIds;->onEmvPaymentTerminated(Lcom/squareup/cardreader/CardReaderId;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$4$RegisterAppDelegate(Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 507
    instance-of v0, p1, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedIn;

    if-eqz v0, :cond_1

    .line 508
    iget-object p1, p0, Lcom/squareup/RegisterAppDelegate;->loggedInScope:Lmortar/MortarScope;

    if-eqz p1, :cond_0

    .line 513
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Unexpected double log in"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-void

    .line 516
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/RegisterAppDelegate;->onAuthenticated()V

    .line 517
    iget-object p1, p0, Lcom/squareup/RegisterAppDelegate;->persistentAccountStatusService:Lcom/squareup/accountstatus/PersistentAccountStatusService;

    invoke-virtual {p1}, Lcom/squareup/accountstatus/PersistentAccountStatusService;->onLoggedIn()V

    goto :goto_0

    .line 518
    :cond_1
    instance-of v0, p1, Lcom/squareup/account/LoggedInStatusProvider$LoggedInStatus$LoggedOut;

    if-eqz v0, :cond_3

    .line 521
    iget-object p1, p0, Lcom/squareup/RegisterAppDelegate;->loggedInScope:Lmortar/MortarScope;

    if-eqz p1, :cond_2

    .line 522
    iget-object p1, p0, Lcom/squareup/RegisterAppDelegate;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v0, Lcom/squareup/log/OhSnapLogger$EventType;->DESTROY_SCOPE:Lcom/squareup/log/OhSnapLogger$EventType;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " killing orphan scope "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/RegisterAppDelegate;->loggedInScope:Lmortar/MortarScope;

    invoke-virtual {v2}, Lmortar/MortarScope;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 523
    iget-object p1, p0, Lcom/squareup/RegisterAppDelegate;->loggedInScope:Lmortar/MortarScope;

    invoke-virtual {p1}, Lmortar/MortarScope;->destroy()V

    const/4 p1, 0x0

    .line 524
    iput-object p1, p0, Lcom/squareup/RegisterAppDelegate;->loggedInScope:Lmortar/MortarScope;

    :cond_2
    :goto_0
    return-void

    .line 527
    :cond_3
    new-instance v0, Lkotlin/NotImplementedError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Missing implementation for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected onAuthenticated()V
    .locals 3

    .line 553
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->eventStreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    new-instance v1, Lcom/squareup/accessibility/AccessibilityEnabledEvent;

    iget-object v2, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    invoke-direct {v1, v2}, Lcom/squareup/accessibility/AccessibilityEnabledEvent;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/squareup/eventstream/v2/EventstreamV2;->log(Ljava/lang/Object;)V

    .line 554
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->eventStreamV2:Lcom/squareup/eventstream/v2/EventstreamV2;

    new-instance v1, Lcom/squareup/analytics/GooglePlayServicesAvailabilityEvent;

    iget-object v2, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    invoke-direct {v1, v2}, Lcom/squareup/analytics/GooglePlayServicesAvailabilityEvent;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/squareup/eventstream/v2/EventstreamV2;->log(Ljava/lang/Object;)V

    .line 557
    invoke-static {}, Lcom/squareup/mortar/AppContextWrapper;->appContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/RegisterAppDelegateAppComponent;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/RegisterAppDelegateAppComponent;

    .line 559
    invoke-interface {v0}, Lcom/squareup/RegisterAppDelegateAppComponent;->loggedInComponent()Lcom/squareup/RegisterAppDelegateLoggedInComponent;

    move-result-object v0

    .line 561
    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->applicationScope:Lmortar/MortarScope;

    invoke-virtual {v1}, Lmortar/MortarScope;->buildChild()Lmortar/MortarScope$Builder;

    move-result-object v1

    .line 562
    invoke-static {v1, v0}, Lcom/squareup/dagger/Components;->addAsScopeService(Lmortar/MortarScope$Builder;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    .line 564
    const-class v2, Lcom/squareup/dagger/LoggedInScope;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lmortar/MortarScope$Builder;->build(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/RegisterAppDelegate;->loggedInScope:Lmortar/MortarScope;

    .line 565
    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->loggedInScope:Lmortar/MortarScope;

    invoke-static {v1, v0}, Lcom/squareup/leakcanary/ScopedObjectWatcher;->watchForLeaks(Lmortar/MortarScope;Ljava/lang/Object;)V

    .line 567
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->loggedInScope:Lmortar/MortarScope;

    const-class v1, Lcom/squareup/RegisterAppDelegate$ParentLoggedInComponent;

    .line 568
    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/RegisterAppDelegate$ParentLoggedInComponent;

    .line 569
    invoke-interface {v0}, Lcom/squareup/RegisterAppDelegate$ParentLoggedInComponent;->loggedInScopeRunner()Lcom/squareup/LoggedInScopeRunner;

    move-result-object v0

    .line 573
    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->loggedInScope:Lmortar/MortarScope;

    invoke-virtual {v1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public onCreate()V
    .locals 5

    .line 199
    new-instance v0, Lcom/squareup/android/util/RealPosBuild;

    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    invoke-direct {v0, v1}, Lcom/squareup/android/util/RealPosBuild;-><init>(Landroid/app/Application;)V

    iput-object v0, p0, Lcom/squareup/RegisterAppDelegate;->posBuild:Lcom/squareup/util/PosBuild;

    .line 201
    invoke-direct {p0}, Lcom/squareup/RegisterAppDelegate;->jumpShipWhenAppIsSinking()V

    .line 203
    invoke-static {}, Lcom/squareup/util/X2Build;->isSquareDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    new-instance v0, Ltimber/log/Timber$DebugTree;

    invoke-direct {v0}, Ltimber/log/Timber$DebugTree;-><init>()V

    invoke-static {v0}, Ltimber/log/Timber;->plant(Ltimber/log/Timber$Tree;)V

    .line 207
    :cond_0
    invoke-direct {p0}, Lcom/squareup/RegisterAppDelegate;->installExceptionHandler()Lcom/squareup/log/RegisterExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/RegisterAppDelegate;->exceptionHandler:Lcom/squareup/log/RegisterExceptionHandler;

    .line 208
    invoke-direct {p0}, Lcom/squareup/RegisterAppDelegate;->warmUpProtoClasses()V

    .line 209
    invoke-direct {p0}, Lcom/squareup/RegisterAppDelegate;->warmUpGson()V

    .line 211
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    invoke-static {v0}, Lcom/jakewharton/threetenabp/AndroidThreeTen;->init(Landroid/app/Application;)V

    .line 213
    new-instance v0, Lcom/squareup/ActivityListener;

    invoke-direct {v0}, Lcom/squareup/ActivityListener;-><init>()V

    iput-object v0, p0, Lcom/squareup/RegisterAppDelegate;->activityListener:Lcom/squareup/ActivityListener;

    .line 214
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->activityListener:Lcom/squareup/ActivityListener;

    invoke-virtual {v0, v1}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 216
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    invoke-static {v0}, Lcom/squareup/leakfix/AndroidLeaks;->plugLeaks(Landroid/app/Application;)V

    .line 217
    invoke-virtual {p0}, Lcom/squareup/RegisterAppDelegate;->createAppScope()V

    .line 219
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->deepLinkHelper:Lcom/squareup/analytics/DeepLinkHelper;

    invoke-interface {v0}, Lcom/squareup/analytics/DeepLinkHelper;->init()V

    .line 221
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->anrChaperone:Lcom/squareup/anrchaperone/AnrChaperone;

    new-instance v1, Lcom/squareup/-$$Lambda$RegisterAppDelegate$wBjJNLZvYqLVHwc8d7VWjJd6NXM;

    invoke-direct {v1, p0}, Lcom/squareup/-$$Lambda$RegisterAppDelegate$wBjJNLZvYqLVHwc8d7VWjJd6NXM;-><init>(Lcom/squareup/RegisterAppDelegate;)V

    invoke-virtual {v0, v1}, Lcom/squareup/anrchaperone/AnrChaperone;->supervise(Lkotlin/jvm/functions/Function0;)V

    .line 227
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-lt v0, v1, :cond_1

    .line 228
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iget-object v1, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    invoke-static {}, Lcom/squareup/notification/Channels;->values()[Lcom/squareup/notification/Channels;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/notification/NotificationWrapper;->createDefaultNotificationChannels(Landroid/content/Context;[Lcom/squareup/notification/Channel;)V

    .line 229
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    invoke-direct {p0, v0}, Lcom/squareup/RegisterAppDelegate;->logNotificationChannelAttributes(Landroid/content/Context;)V

    .line 232
    :cond_1
    invoke-static {}, Lcom/squareup/toast/ToastTokenFix;->swallowToastBadTokenExceptions()V

    .line 235
    iget-boolean v0, p0, Lcom/squareup/RegisterAppDelegate;->isReaderSdk:Z

    if-eqz v0, :cond_2

    .line 236
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/api/ReaderSdkInitEvent;

    iget-object v2, p0, Lcom/squareup/RegisterAppDelegate;->application:Landroid/app/Application;

    sget-wide v3, Lcom/squareup/RegisterAppDelegate;->processStartupUptimeMs:J

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/api/ReaderSdkInitEvent;-><init>(Landroid/content/Context;J)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :cond_2
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 472
    new-instance v0, Lcom/squareup/RegisterAppDelegate$1;

    invoke-direct {v0, p0}, Lcom/squareup/RegisterAppDelegate$1;-><init>(Lcom/squareup/RegisterAppDelegate;)V

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 481
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->dipperEvents()Lrx/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/dipper/events/DipperEvent$PaymentApproved;

    .line 482
    invoke-virtual {v0, v1}, Lrx/Observable;->ofType(Ljava/lang/Class;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/-$$Lambda$RegisterAppDelegate$xUw4L7DcedvbnrcpjAXyO2Jds-Q;

    invoke-direct {v1, p0}, Lcom/squareup/-$$Lambda$RegisterAppDelegate$xUw4L7DcedvbnrcpjAXyO2Jds-Q;-><init>(Lcom/squareup/RegisterAppDelegate;)V

    .line 483
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 481
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 486
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->initialize()V

    .line 487
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->deviceSettingsSettingsInitializer:Lcom/squareup/settings/DeviceSettingsSettingsInitializer;

    invoke-virtual {v0}, Lcom/squareup/settings/DeviceSettingsSettingsInitializer;->initialize()V

    .line 490
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->libraryLoader:Lcom/squareup/cardreader/loader/LibraryLoader;

    invoke-interface {v0}, Lcom/squareup/cardreader/loader/LibraryLoader;->load()V

    .line 493
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/squareup/util/X2Build;->isSquareDevice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->msFactory:Lcom/squareup/ms/MsFactory;

    invoke-interface {v0}, Lcom/squareup/ms/MsFactory;->initialize()V

    .line 497
    :cond_0
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->appScopedItems:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmortar/Scoped;

    .line 498
    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    goto :goto_0

    .line 502
    :cond_1
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 504
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    .line 505
    invoke-interface {v0}, Lcom/squareup/account/LegacyAuthenticator;->loggedInStatus()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/-$$Lambda$RegisterAppDelegate$_raYsGBrzHLwFij3q3Dk94KyOEQ;

    invoke-direct {v1, p0}, Lcom/squareup/-$$Lambda$RegisterAppDelegate$_raYsGBrzHLwFij3q3Dk94KyOEQ;-><init>(Lcom/squareup/RegisterAppDelegate;)V

    .line 506
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 504
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 531
    iget-object p1, p0, Lcom/squareup/RegisterAppDelegate;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    const-string v0, "Square app is running"

    invoke-interface {p1, v0}, Lcom/squareup/queue/QueueServiceStarter;->startWaitingForForeground(Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 539
    iget-object v0, p0, Lcom/squareup/RegisterAppDelegate;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->destroy()V

    const/4 v0, 0x0

    .line 540
    iput-object v0, p0, Lcom/squareup/RegisterAppDelegate;->appComponent:Lcom/squareup/RegisterAppDelegateAppComponent;

    return-void
.end method
