.class public final Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;
.super Ljava/lang/Object;
.source "GlobalStateBuyerLanguageSelectionWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGlobalStateBuyerLanguageSelectionWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GlobalStateBuyerLanguageSelectionWorkflow.kt\ncom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion\n+ 2 LegacyState.kt\ncom/squareup/workflow/legacyintegration/LegacyStateKt\n+ 3 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPool$Companion\n+ 4 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPoolKt\n*L\n1#1,44:1\n96#2,2:45\n76#2,4:47\n335#3:51\n412#4:52\n*E\n*S KotlinDebug\n*F\n+ 1 GlobalStateBuyerLanguageSelectionWorkflow.kt\ncom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion\n*L\n41#1,2:45\n41#1,4:47\n41#1:51\n41#1:52\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002JN\u0010\u0003\u001a<\u0012(\u0012&\u0012\u0004\u0012\u00020\u0006\u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n0\u00070\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u000b0\u0004j\u0002`\u000c2\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0007\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;",
        "",
        "()V",
        "legacyHandle",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/buyer/language/Exit;",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionLegacyHandle;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;

    invoke-direct {v0}, Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;-><init>()V

    sput-object v0, Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;->$$INSTANCE:Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic legacyHandle$default(Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;Lcom/squareup/workflow/Snapshot;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 40
    check-cast p1, Lcom/squareup/workflow/Snapshot;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow$Companion;->legacyHandle(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final legacyHandle(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/WorkflowPool$Handle;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lkotlin/Unit;",
            "Lcom/squareup/buyer/language/Exit;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Upgrade consumers to com.squareup.workflow.Workflow"
    .end annotation

    .line 46
    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 47
    sget-object v0, Lcom/squareup/workflow/legacy/WorkflowPool;->Companion:Lcom/squareup/workflow/legacy/WorkflowPool$Companion;

    .line 48
    new-instance v0, Lcom/squareup/workflow/legacyintegration/FakeKClass;

    invoke-direct {v0}, Lcom/squareup/workflow/legacyintegration/FakeKClass;-><init>()V

    check-cast v0, Lkotlin/reflect/KClass;

    .line 49
    new-instance v6, Lcom/squareup/workflow/legacyintegration/LegacyState;

    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    move-object v0, v6

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/workflow/legacyintegration/LegacyState;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 52
    new-instance p1, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-class v0, Lcom/squareup/workflow/legacyintegration/LegacyState;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-class v1, Lkotlin/Unit;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-class v2, Lcom/squareup/buyer/language/Exit;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    const-string v0, ""

    .line 51
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;->makeWorkflowId(Ljava/lang/String;)Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object p1

    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {v0, p1, v6}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Id;Ljava/lang/Object;)V

    return-object v0
.end method
