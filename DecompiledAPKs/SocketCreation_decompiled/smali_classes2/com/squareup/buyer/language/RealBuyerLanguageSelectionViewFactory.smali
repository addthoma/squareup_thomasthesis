.class public final Lcom/squareup/buyer/language/RealBuyerLanguageSelectionViewFactory;
.super Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;
.source "RealBuyerLanguageSelectionViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/buyer/language/RealBuyerLanguageSelectionViewFactory;",
        "Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;",
        "()V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 7
    sget-object v1, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->Companion:Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner$Companion;

    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 6
    invoke-direct {p0, v0}, Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
