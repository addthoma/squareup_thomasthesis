.class public final Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;
.super Ljava/lang/Object;
.source "BuyerLanguageScreenLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/buyer/language/BuyerLanguageScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBuyerLanguageScreenLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BuyerLanguageScreenLayoutRunner.kt\ncom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,54:1\n1103#2,7:55\n1103#2,7:62\n*E\n*S KotlinDebug\n*F\n+ 1 BuyerLanguageScreenLayoutRunner.kt\ncom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner\n*L\n28#1,7:55\n42#1,7:62\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00152\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0015B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J \u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00072\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0002J\u0018\u0010\u0012\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/buyer/language/BuyerLanguageScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "selectEnglish",
        "Landroid/widget/TextView;",
        "selectFrench",
        "selectJapanese",
        "selectSpanish",
        "upGlyph",
        "configureTextView",
        "",
        "textView",
        "localeAndDisplayText",
        "Lcom/squareup/buyer/language/LocaleAndDisplayText;",
        "rendering",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner$Companion;


# instance fields
.field private final selectEnglish:Landroid/widget/TextView;

.field private final selectFrench:Landroid/widget/TextView;

.field private final selectJapanese:Landroid/widget/TextView;

.field private final selectSpanish:Landroid/widget/TextView;

.field private final upGlyph:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->Companion:Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    sget v0, Lcom/squareup/buyer/language/impl/R$id;->buyer_actionbar_up_glyph:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.buyer_actionbar_up_glyph)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->upGlyph:Landroid/view/View;

    .line 19
    sget v0, Lcom/squareup/buyer/language/impl/R$id;->buyer_select_canada_english:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.b\u2026er_select_canada_english)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->selectEnglish:Landroid/widget/TextView;

    .line 20
    sget v0, Lcom/squareup/buyer/language/impl/R$id;->buyer_select_canada_french:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.buyer_select_canada_french)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->selectFrench:Landroid/widget/TextView;

    .line 21
    sget v0, Lcom/squareup/buyer/language/impl/R$id;->buyer_select_spanish:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.buyer_select_spanish)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->selectSpanish:Landroid/widget/TextView;

    .line 22
    sget v0, Lcom/squareup/buyer/language/impl/R$id;->buyer_select_japanese:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.buyer_select_japanese)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->selectJapanese:Landroid/widget/TextView;

    return-void
.end method

.method private final configureTextView(Landroid/widget/TextView;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/BuyerLanguageScreen;)V
    .locals 1

    .line 41
    invoke-virtual {p2}, Lcom/squareup/buyer/language/LocaleAndDisplayText;->getDisplayText()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    check-cast p1, Landroid/view/View;

    .line 62
    new-instance v0, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner$configureTextView$$inlined$onClickDebounced$1;

    invoke-direct {v0, p3, p2}, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner$configureTextView$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/buyer/language/BuyerLanguageScreen;Lcom/squareup/buyer/language/LocaleAndDisplayText;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/buyer/language/BuyerLanguageScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object p2, p0, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->upGlyph:Landroid/view/View;

    .line 55
    new-instance v0, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner$showRendering$$inlined$onClickDebounced$1;

    invoke-direct {v0, p1}, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner$showRendering$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/buyer/language/BuyerLanguageScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    iget-object p2, p0, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->selectEnglish:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/buyer/language/BuyerLanguageScreen;->getEnglishDisplay()Lcom/squareup/buyer/language/LocaleAndDisplayText;

    move-result-object v0

    invoke-direct {p0, p2, v0, p1}, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->configureTextView(Landroid/widget/TextView;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/BuyerLanguageScreen;)V

    .line 31
    iget-object p2, p0, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->selectFrench:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/buyer/language/BuyerLanguageScreen;->getCanadianFrenchDisplay()Lcom/squareup/buyer/language/LocaleAndDisplayText;

    move-result-object v0

    invoke-direct {p0, p2, v0, p1}, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->configureTextView(Landroid/widget/TextView;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/BuyerLanguageScreen;)V

    .line 32
    iget-object p2, p0, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->selectSpanish:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/buyer/language/BuyerLanguageScreen;->getUsSpanishDisplay()Lcom/squareup/buyer/language/LocaleAndDisplayText;

    move-result-object v0

    invoke-direct {p0, p2, v0, p1}, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->configureTextView(Landroid/widget/TextView;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/BuyerLanguageScreen;)V

    .line 33
    iget-object p2, p0, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->selectJapanese:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/buyer/language/BuyerLanguageScreen;->getJapaneseDisplay()Lcom/squareup/buyer/language/LocaleAndDisplayText;

    move-result-object v0

    invoke-direct {p0, p2, v0, p1}, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->configureTextView(Landroid/widget/TextView;Lcom/squareup/buyer/language/LocaleAndDisplayText;Lcom/squareup/buyer/language/BuyerLanguageScreen;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/buyer/language/BuyerLanguageScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/buyer/language/BuyerLanguageScreenLayoutRunner;->showRendering(Lcom/squareup/buyer/language/BuyerLanguageScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
