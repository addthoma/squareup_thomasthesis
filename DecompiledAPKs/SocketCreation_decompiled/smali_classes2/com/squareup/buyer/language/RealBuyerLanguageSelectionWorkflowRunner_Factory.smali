.class public final Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "RealBuyerLanguageSelectionWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;)",
            "Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;-><init>(Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;Lcom/squareup/ui/main/PosContainer;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;

    iget-object v1, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;

    iget-object v2, p0, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0, v1, v2}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner_Factory;->newInstance(Lcom/squareup/buyer/language/BuyerLanguageSelectionViewFactory;Lcom/squareup/buyer/language/GlobalStateBuyerLanguageSelectionWorkflow;Lcom/squareup/ui/main/PosContainer;)Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner_Factory;->get()Lcom/squareup/buyer/language/RealBuyerLanguageSelectionWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
