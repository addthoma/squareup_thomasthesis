.class public final Lcom/squareup/buyer/language/BuyerLanguageScreenKt;
.super Ljava/lang/Object;
.source "BuyerLanguageScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0000\u00a8\u0006\u0003"
    }
    d2 = {
        "withDisplayText",
        "Lcom/squareup/buyer/language/LocaleAndDisplayText;",
        "Ljava/util/Locale;",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final withDisplayText(Ljava/util/Locale;)Lcom/squareup/buyer/language/LocaleAndDisplayText;
    .locals 2

    const-string v0, "$this$withDisplayText"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance v0, Lcom/squareup/buyer/language/LocaleAndDisplayText;

    .line 23
    new-instance v1, Lcom/squareup/locale/LocaleFormatter;

    invoke-direct {v1, p0}, Lcom/squareup/locale/LocaleFormatter;-><init>(Ljava/util/Locale;)V

    invoke-virtual {v1}, Lcom/squareup/locale/LocaleFormatter;->displayLanguage()Ljava/lang/String;

    move-result-object v1

    .line 21
    invoke-direct {v0, p0, v1}, Lcom/squareup/buyer/language/LocaleAndDisplayText;-><init>(Ljava/util/Locale;Ljava/lang/String;)V

    return-object v0
.end method
