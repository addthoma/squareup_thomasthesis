.class public Lcom/squareup/autocapture/AutoCaptureControl;
.super Ljava/lang/Object;
.source "AutoCaptureControl.java"


# instance fields
.field private final autoCaptureTimerStarter:Lcom/squareup/autocapture/AutoCaptureTimerStarter;


# direct methods
.method constructor <init>(Lcom/squareup/autocapture/AutoCaptureTimerStarter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/autocapture/AutoCaptureControl;->autoCaptureTimerStarter:Lcom/squareup/autocapture/AutoCaptureTimerStarter;

    return-void
.end method


# virtual methods
.method public restartQuickTimer(Lcom/squareup/payment/RequiresAuthorization;)V
    .locals 2

    .line 52
    invoke-interface {p1}, Lcom/squareup/payment/RequiresAuthorization;->getUniqueClientId()Ljava/lang/String;

    move-result-object v0

    .line 53
    invoke-interface {p1}, Lcom/squareup/payment/RequiresAuthorization;->canQuickCapture()Z

    move-result p1

    .line 54
    iget-object v1, p0, Lcom/squareup/autocapture/AutoCaptureControl;->autoCaptureTimerStarter:Lcom/squareup/autocapture/AutoCaptureTimerStarter;

    invoke-virtual {v1, v0, p1}, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->restartQuickTimerDebounced(Ljava/lang/String;Z)V

    return-void
.end method

.method public startQuickAndSlowTimers(Lcom/squareup/payment/RequiresAuthorization;)V
    .locals 2

    .line 40
    invoke-interface {p1}, Lcom/squareup/payment/RequiresAuthorization;->getUniqueClientId()Ljava/lang/String;

    move-result-object v0

    .line 41
    invoke-interface {p1}, Lcom/squareup/payment/RequiresAuthorization;->canQuickCapture()Z

    move-result p1

    .line 42
    iget-object v1, p0, Lcom/squareup/autocapture/AutoCaptureControl;->autoCaptureTimerStarter:Lcom/squareup/autocapture/AutoCaptureTimerStarter;

    invoke-virtual {v1, v0, p1}, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->startQuickAndSlowTimers(Ljava/lang/String;Z)V

    return-void
.end method

.method public stop()V
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/autocapture/AutoCaptureControl;->autoCaptureTimerStarter:Lcom/squareup/autocapture/AutoCaptureTimerStarter;

    invoke-virtual {v0}, Lcom/squareup/autocapture/AutoCaptureTimerStarter;->stopAllTimers()V

    return-void
.end method
