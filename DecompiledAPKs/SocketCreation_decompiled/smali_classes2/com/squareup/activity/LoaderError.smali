.class public final Lcom/squareup/activity/LoaderError;
.super Ljava/lang/Object;
.source "LoaderError.java"


# instance fields
.field public final loadingMore:Z

.field public final message:Ljava/lang/String;

.field public final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/squareup/activity/LoaderError;->title:Ljava/lang/String;

    .line 10
    iput-object p2, p0, Lcom/squareup/activity/LoaderError;->message:Ljava/lang/String;

    .line 11
    iput-boolean p3, p0, Lcom/squareup/activity/LoaderError;->loadingMore:Z

    return-void
.end method
