.class public final Lcom/squareup/activity/refund/RefundData;
.super Ljava/lang/Object;
.source "RefundData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/activity/refund/RefundData$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRefundData.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RefundData.kt\ncom/squareup/activity/refund/RefundData\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Maps.kt\nkotlin/collections/MapsKt__MapsKt\n+ 4 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n+ 5 ArraysJVM.kt\nkotlin/collections/ArraysKt__ArraysJVMKt\n*L\n1#1,870:1\n1550#2,3:871\n1360#2:874\n1429#2,3:875\n1587#2,3:878\n704#2:881\n777#2,2:882\n704#2:884\n777#2,2:885\n1360#2:887\n1429#2,3:888\n1587#2,3:891\n1360#2:894\n1429#2,3:895\n1360#2:898\n1429#2,3:899\n1360#2:902\n1429#2,3:903\n1360#2:906\n1429#2,3:907\n704#2:910\n777#2,2:911\n1288#2:913\n1313#2,3:914\n1316#2,3:924\n1360#2:931\n1429#2,3:932\n704#2:935\n777#2,2:936\n1360#2:938\n1429#2,3:939\n1550#2,3:942\n215#2,2:945\n1550#2,3:947\n1550#2,3:950\n1360#2:953\n1429#2,3:954\n1587#2,3:957\n1529#2,3:960\n1550#2,3:963\n704#2:966\n777#2,2:967\n1360#2:969\n1429#2,3:970\n1360#2:975\n1429#2,3:976\n1587#2,3:979\n1412#2,9:982\n1642#2,2:991\n1421#2:993\n1288#2:994\n1313#2,3:995\n1316#2,3:1005\n1143#2,4:1010\n1288#2:1014\n1313#2,3:1015\n1316#2,3:1025\n1143#2,4:1030\n1360#2:1034\n1429#2,3:1035\n1587#2,3:1038\n1550#2,3:1041\n1360#2:1044\n1429#2,3:1045\n1587#2,3:1048\n1360#2:1051\n1429#2,3:1052\n1360#2:1055\n1429#2,3:1056\n1288#2:1059\n1313#2,3:1060\n1316#2,3:1070\n347#3,7:917\n347#3,7:998\n428#3:1008\n378#3:1009\n347#3,7:1018\n428#3:1028\n378#3:1029\n347#3,7:1063\n67#4:927\n92#4,3:928\n37#5,2:973\n*E\n*S KotlinDebug\n*F\n+ 1 RefundData.kt\ncom/squareup/activity/refund/RefundData\n*L\n146#1,3:871\n158#1:874\n158#1,3:875\n159#1,3:878\n162#1:881\n162#1,2:882\n217#1:884\n217#1,2:885\n218#1:887\n218#1,3:888\n219#1,3:891\n468#1:894\n468#1,3:895\n477#1:898\n477#1,3:899\n490#1:902\n490#1,3:903\n568#1:906\n568#1,3:907\n569#1:910\n569#1,2:911\n570#1:913\n570#1,3:914\n570#1,3:924\n587#1:931\n587#1,3:932\n588#1:935\n588#1,2:936\n589#1:938\n589#1,3:939\n608#1,3:942\n615#1,2:945\n630#1,3:947\n636#1,3:950\n648#1:953\n648#1,3:954\n649#1,3:957\n654#1,3:960\n663#1,3:963\n697#1:966\n697#1,2:967\n706#1:969\n706#1,3:970\n747#1:975\n747#1,3:976\n748#1,3:979\n780#1,9:982\n780#1,2:991\n780#1:993\n818#1:994\n818#1,3:995\n818#1,3:1005\n819#1,4:1010\n825#1:1014\n825#1,3:1015\n825#1,3:1025\n826#1,4:1030\n834#1:1034\n834#1,3:1035\n835#1,3:1038\n842#1,3:1041\n848#1:1044\n848#1,3:1045\n849#1,3:1048\n856#1:1051\n856#1,3:1052\n865#1:1055\n865#1,3:1056\n201#1:1059\n201#1,3:1060\n201#1,3:1070\n570#1,7:917\n818#1,7:998\n819#1:1008\n819#1:1009\n825#1,7:1018\n826#1:1028\n826#1:1029\n201#1,7:1063\n571#1:927\n571#1,3:928\n707#1,2:973\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00e6\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010$\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0018\u0002\n\u0002\u0008_\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0011\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u0000 \u00de\u00012\u00020\u0001:\u0002\u00de\u0001B\u00e7\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000e\u0008\u0002\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0005\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0013\u0012\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u0012\n\u0008\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0013\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u0013\u0012\u0006\u0010\u001b\u001a\u00020\u000b\u0012\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u001d\u0012\u0016\u0008\u0002\u0010\u001e\u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u001d\u0012\n\u0008\u0002\u0010\u001f\u001a\u0004\u0018\u00010 \u0012\u0008\u0008\u0002\u0010!\u001a\u00020\u000f\u0012\u0008\u0008\u0002\u0010\"\u001a\u00020#\u0012\u0008\u0008\u0002\u0010$\u001a\u00020%\u0012\u0008\u0008\u0002\u0010&\u001a\u00020\u000f\u0012\u0008\u0008\u0002\u0010\'\u001a\u00020\u000f\u0012\u0008\u0008\u0002\u0010(\u001a\u00020\u000f\u0012\u0008\u0008\u0002\u0010)\u001a\u00020\u000f\u0012\u0008\u0008\u0002\u0010*\u001a\u00020\u000f\u0012\n\u0008\u0002\u0010+\u001a\u0004\u0018\u00010\u000b\u0012\u000c\u0010,\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005\u0012\u000c\u0010.\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005\u0012\u0006\u0010/\u001a\u000200\u0012\n\u0008\u0002\u00101\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u00102J\u000e\u0010y\u001a\u00020\u000f2\u0006\u0010z\u001a\u00020\tJ\t\u0010{\u001a\u00020\u0003H\u00c6\u0003J\t\u0010|\u001a\u00020\u0013H\u00c6\u0003J\u000b\u0010}\u001a\u0004\u0018\u00010\u0016H\u00c6\u0003J\u000b\u0010~\u001a\u0004\u0018\u00010\u0013H\u00c6\u0003J\t\u0010\u007f\u001a\u00020\u0019H\u00c6\u0003J\n\u0010\u0080\u0001\u001a\u00020\u0013H\u00c6\u0003J\n\u0010\u0081\u0001\u001a\u00020\u000bH\u00c6\u0003J\u0016\u0010\u0082\u0001\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u001dH\u00c6\u0003J\u0018\u0010\u0083\u0001\u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u001dH\u00c6\u0003J\u0011\u0010\u0084\u0001\u001a\u0004\u0018\u00010 H\u00c6\u0003\u00a2\u0006\u0002\u0010<J\n\u0010\u0085\u0001\u001a\u00020\u000fH\u00c6\u0003J\u0010\u0010\u0086\u0001\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\n\u0010\u0087\u0001\u001a\u00020#H\u00c6\u0003J\n\u0010\u0088\u0001\u001a\u00020%H\u00c6\u0003J\n\u0010\u0089\u0001\u001a\u00020\u000fH\u00c6\u0003J\n\u0010\u008a\u0001\u001a\u00020\u000fH\u00c6\u0003J\n\u0010\u008b\u0001\u001a\u00020\u000fH\u00c6\u0003J\n\u0010\u008c\u0001\u001a\u00020\u000fH\u00c6\u0003J\n\u0010\u008d\u0001\u001a\u00020\u000fH\u00c6\u0003J\u000c\u0010\u008e\u0001\u001a\u0004\u0018\u00010\u000bH\u00c2\u0003J\u0010\u0010\u008f\u0001\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005H\u00c2\u0003J\u0010\u0010\u0090\u0001\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005H\u00c2\u0003J\u0010\u0010\u0091\u0001\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\n\u0010\u0092\u0001\u001a\u000200H\u00c2\u0003J\u000c\u0010\u0093\u0001\u001a\u0004\u0018\u00010\u000bH\u00c2\u0003J\u0010\u0010\u0094\u0001\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0005H\u00c6\u0003J\n\u0010\u0095\u0001\u001a\u00020\u000bH\u00c6\u0003J\n\u0010\u0096\u0001\u001a\u00020\rH\u00c6\u0003J\n\u0010\u0097\u0001\u001a\u00020\u000fH\u00c6\u0003J\n\u0010\u0098\u0001\u001a\u00020\u0011H\u00c6\u0003J\u000c\u0010\u0099\u0001\u001a\u0004\u0018\u00010\u0013H\u00c6\u0003J\u008a\u0003\u0010\u009a\u0001\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u000e\u0008\u0002\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00052\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\u0010\u001a\u00020\u00112\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0008\u0008\u0002\u0010\u0014\u001a\u00020\u00132\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00162\n\u0008\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00132\u0008\u0008\u0002\u0010\u0018\u001a\u00020\u00192\u0008\u0008\u0002\u0010\u001a\u001a\u00020\u00132\u0008\u0008\u0002\u0010\u001b\u001a\u00020\u000b2\u0014\u0008\u0002\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u001d2\u0016\u0008\u0002\u0010\u001e\u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u001d2\n\u0008\u0002\u0010\u001f\u001a\u0004\u0018\u00010 2\u0008\u0008\u0002\u0010!\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\"\u001a\u00020#2\u0008\u0008\u0002\u0010$\u001a\u00020%2\u0008\u0008\u0002\u0010&\u001a\u00020\u000f2\u0008\u0008\u0002\u0010\'\u001a\u00020\u000f2\u0008\u0008\u0002\u0010(\u001a\u00020\u000f2\u0008\u0008\u0002\u0010)\u001a\u00020\u000f2\u0008\u0008\u0002\u0010*\u001a\u00020\u000f2\n\u0008\u0002\u0010+\u001a\u0004\u0018\u00010\u000b2\u000e\u0008\u0002\u0010,\u001a\u0008\u0012\u0004\u0012\u00020-0\u00052\u000e\u0008\u0002\u0010.\u001a\u0008\u0012\u0004\u0012\u00020-0\u00052\u0008\u0008\u0002\u0010/\u001a\u0002002\n\u0008\u0002\u00101\u001a\u0004\u0018\u00010\u000bH\u00c6\u0001\u00a2\u0006\u0003\u0010\u009b\u0001J\u0011\u0010\u009c\u0001\u001a\u00020\u00002\u0008\u00101\u001a\u0004\u0018\u00010\u000bJ\u0007\u0010\u009d\u0001\u001a\u00020\u0000J\u001a\u0010\u009e\u0001\u001a\u00020\u00002\u0008\u0010\u009f\u0001\u001a\u00030\u00a0\u00012\u0007\u0010\u00a1\u0001\u001a\u00020%J\u0013\u0010\u00a2\u0001\u001a\u00020\u00002\n\u0010\u00a3\u0001\u001a\u0005\u0018\u00010\u00a4\u0001J\u001f\u0010\u00a5\u0001\u001a\u00020\u00002\u0006\u0010!\u001a\u00020\u000f2\u0006\u0010\"\u001a\u00020#2\u0006\u0010(\u001a\u00020\u000fJ\u000f\u0010\u00a6\u0001\u001a\u00020\u00002\u0006\u0010\"\u001a\u00020#J \u0010\u00a7\u0001\u001a\u00020\u00002\u0006\u0010!\u001a\u00020\u000f2\u0006\u0010\"\u001a\u00020#2\u0007\u0010\u00a8\u0001\u001a\u00020%J\u0010\u0010\u00a9\u0001\u001a\u00020\u00002\u0007\u0010\u00aa\u0001\u001a\u00020\u000bJ\u000f\u0010\u00ab\u0001\u001a\u00020\u00002\u0006\u0010\u000c\u001a\u00020\rJ\u0018\u0010\u00ac\u0001\u001a\u00020\u00002\u0007\u0010\u00ad\u0001\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0019J\u000f\u0010\u00ae\u0001\u001a\u00020\u00002\u0006\u0010\u0015\u001a\u00020\u0016J\u0016\u0010\u00af\u0001\u001a\u00020\u00002\r\u0010\u00b0\u0001\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005J\u0016\u0010\u00b1\u0001\u001a\u00020\u00002\r\u0010\u00b0\u0001\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005J\u000f\u0010\u00b2\u0001\u001a\u00020\u00002\u0006\u0010z\u001a\u00020\tJ%\u0010\u00b3\u0001\u001a\u00030\u00b4\u00012\u0008\u0010\u00b5\u0001\u001a\u00030\u00b6\u00012\u0008\u0010\u00b7\u0001\u001a\u00030\u00b8\u00012\u0007\u0010\u00b9\u0001\u001a\u00020\u000fJ\u0015\u0010\u00ba\u0001\u001a\u00020\u000f2\t\u0010\u00bb\u0001\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\u0007\u0010\u00bc\u0001\u001a\u00020\tJ\r\u0010\u00bd\u0001\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005J\u0014\u0010\u00be\u0001\u001a\t\u0012\u0004\u0012\u00020\u00130\u00bf\u0001\u00a2\u0006\u0003\u0010\u00c0\u0001J\u0018\u0010\u00c1\u0001\u001a\u00020\u000b2\r\u0010\u00b0\u0001\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005H\u0002J\u0019\u0010\u00c2\u0001\u001a\u00020\u000b2\u000e\u0010\u00c3\u0001\u001a\t\u0012\u0005\u0012\u00030\u00c4\u00010\u0005H\u0002J)\u0010\u00c5\u0001\u001a\"\u0012\r\u0012\u000b \u00c6\u0001*\u0004\u0018\u00010\u00110\u0011\u0012\u000f\u0012\r \u00c6\u0001*\u0005\u0018\u00010\u00c7\u00010\u00c7\u00010\u001dH\u0002J)\u0010\u00c8\u0001\u001a\"\u0012\r\u0012\u000b \u00c6\u0001*\u0004\u0018\u00010\u00130\u0013\u0012\u000f\u0012\r \u00c6\u0001*\u0005\u0018\u00010\u00c9\u00010\u00c9\u00010\u001dH\u0002J\r\u0010\u00ca\u0001\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005J\u001d\u0010\u00cb\u0001\u001a\t\u0012\u0005\u0012\u00030\u00c4\u00010\u00052\r\u0010\u00b0\u0001\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005J\u001d\u0010\u00cc\u0001\u001a\t\u0012\u0005\u0012\u00030\u00cd\u00010\u00052\r\u0010\u00b0\u0001\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005J\n\u0010\u00ce\u0001\u001a\u0005\u0018\u00010\u00cf\u0001J\t\u0010\u00d0\u0001\u001a\u0004\u0018\u00010\u0013J\r\u0010\u00d1\u0001\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005J\u0007\u0010\u00d2\u0001\u001a\u00020\u000fJ\n\u0010\u00d3\u0001\u001a\u00020-H\u00d6\u0001J\u0018\u0010\u00d4\u0001\u001a\u00020\u000f2\r\u0010\u00b0\u0001\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005H\u0002J\u0007\u0010\u00d5\u0001\u001a\u00020\u000fJ\u0007\u0010\u00d6\u0001\u001a\u00020\u000fJ\u001a\u0010\u00d7\u0001\u001a\t\u0012\u0004\u0012\u00020\u00000\u00d8\u00012\u0008\u0010\u00d9\u0001\u001a\u00030\u00da\u0001H\u0002J\u0018\u0010\u00db\u0001\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00052\u0007\u0010\u00aa\u0001\u001a\u00020\u000bH\u0002J\u000f\u0010\u00dc\u0001\u001a\u00020\u000f2\u0006\u0010z\u001a\u00020\tJ\n\u0010\u00dd\u0001\u001a\u00020\u0013H\u00d6\u0001R\u0011\u0010\u001b\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00083\u00104R\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00085\u00106R\u0011\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00087\u00108R\u0011\u00109\u001a\u00020\u000b8F\u00a2\u0006\u0006\u001a\u0004\u0008:\u00104R\u0015\u0010\u001f\u001a\u0004\u0018\u00010 \u00a2\u0006\n\n\u0002\u0010=\u001a\u0004\u0008;\u0010<R\u0011\u0010\u0014\u001a\u00020\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008>\u00106R\u0011\u0010?\u001a\u00020@8F\u00a2\u0006\u0006\u001a\u0004\u0008A\u0010BR\u0010\u0010+\u001a\u0004\u0018\u00010\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\"\u001a\u00020#\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008C\u0010DR\u0011\u0010$\u001a\u00020%\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008E\u0010FR\u0011\u0010G\u001a\u00020\u000b8F\u00a2\u0006\u0006\u001a\u0004\u0008H\u00104R\u0010\u00101\u001a\u0004\u0018\u00010\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010I\u001a\u00020\u000b8F\u00a2\u0006\u0006\u001a\u0004\u0008J\u00104R\u0011\u0010K\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008L\u00108R\u0011\u0010)\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008M\u00108R\u0011\u0010N\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008O\u00108R\u0011\u0010(\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008P\u00108R\u0011\u0010Q\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008R\u00108R\u0011\u0010*\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u00108R\u0011\u0010!\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u00108R\u0011\u0010\'\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u00108R\u0011\u0010S\u001a\u00020\u000f8F\u00a2\u0006\u0006\u001a\u0004\u0008S\u00108R\u001f\u0010\u001e\u001a\u0010\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u001d\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008T\u0010UR\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008V\u0010WR\u000e\u0010/\u001a\u000200X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u001a\u001a\u00020\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008X\u00106R\u001d\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u000b0\u001d\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008Y\u0010UR\u0011\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008Z\u0010[R\u0011\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\\\u0010]R\u0011\u0010^\u001a\u00020\u000b8F\u00a2\u0006\u0006\u001a\u0004\u0008_\u00104R\u0013\u0010\u0017\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008`\u00106R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008a\u00104R\u0014\u0010b\u001a\u00020\u000b8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008c\u00104R\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008d\u0010WR\u0011\u0010e\u001a\u00020\u000b8F\u00a2\u0006\u0006\u001a\u0004\u0008f\u00104R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008g\u0010hR\u0013\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008i\u0010jR\u0014\u0010.\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010,\u001a\u0008\u0012\u0004\u0012\u00020-0\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010k\u001a\u00020\u000f8F\u00a2\u0006\u0006\u001a\u0004\u0008l\u00108R\u0011\u0010m\u001a\u00020\u000f8F\u00a2\u0006\u0006\u001a\u0004\u0008n\u00108R\u0011\u0010o\u001a\u00020\u000f8F\u00a2\u0006\u0006\u001a\u0004\u0008p\u00108R\u0011\u0010&\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008q\u00108R\u0011\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008r\u0010sR\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008t\u0010WR\u0017\u0010u\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00058F\u00a2\u0006\u0006\u001a\u0004\u0008v\u0010WR\u0011\u0010w\u001a\u00020\u000b8F\u00a2\u0006\u0006\u001a\u0004\u0008x\u00104\u00a8\u0006\u00df\u0001"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundData;",
        "",
        "residualBillResponse",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
        "itemizationDetails",
        "",
        "Lcom/squareup/activity/refund/ItemizationDetails;",
        "refundedItemizationDetails",
        "tenderDetails",
        "Lcom/squareup/activity/refund/TenderDetails;",
        "refundableAmount",
        "Lcom/squareup/protos/common/Money;",
        "refundMode",
        "Lcom/squareup/activity/refund/RefundMode;",
        "canOnlyIssueAmountBasedRefund",
        "",
        "sourceBillId",
        "Lcom/squareup/protos/client/IdPair;",
        "authorizedEmployeeToken",
        "",
        "clientToken",
        "restockRequest",
        "Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;",
        "refundReason",
        "reasonOption",
        "Lcom/squareup/protos/client/bills/Refund$ReasonOption;",
        "otherReason",
        "amountRefundMoney",
        "previouslyRefundedTenders",
        "",
        "itemTokensByVariationTokens",
        "catalogVersion",
        "",
        "isRefundingToGiftCard",
        "destinationGiftCard",
        "Lcom/squareup/Card;",
        "destinationGiftCardSwipe",
        "Lokio/ByteString;",
        "shouldShowRefundGiftCard",
        "isSingleTender",
        "hasResolvedGiftCard",
        "hasInvalidGiftCard",
        "isExchange",
        "currentCashDrawerMoney",
        "selectedItemIndices",
        "",
        "selectedItemForRestockIndices",
        "mathRoundingMode",
        "Ljava/math/RoundingMode;",
        "exchangeRefundMoney",
        "(Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;)V",
        "getAmountRefundMoney",
        "()Lcom/squareup/protos/common/Money;",
        "getAuthorizedEmployeeToken",
        "()Ljava/lang/String;",
        "getCanOnlyIssueAmountBasedRefund",
        "()Z",
        "cashDrawerMoneyAfterRefund",
        "getCashDrawerMoneyAfterRefund",
        "getCatalogVersion",
        "()Ljava/lang/Long;",
        "Ljava/lang/Long;",
        "getClientToken",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "getCurrencyCode",
        "()Lcom/squareup/protos/common/CurrencyCode;",
        "getDestinationGiftCard",
        "()Lcom/squareup/Card;",
        "getDestinationGiftCardSwipe",
        "()Lokio/ByteString;",
        "displayRefundMoney",
        "getDisplayRefundMoney",
        "firstTenderRefundAmount",
        "getFirstTenderRefundAmount",
        "hasDisabledIndices",
        "getHasDisabledIndices",
        "getHasInvalidGiftCard",
        "hasItemizationsFromMultipleSourceBills",
        "getHasItemizationsFromMultipleSourceBills",
        "getHasResolvedGiftCard",
        "hasSelectedIndices",
        "getHasSelectedIndices",
        "isTaxableItemSelected",
        "getItemTokensByVariationTokens",
        "()Ljava/util/Map;",
        "getItemizationDetails",
        "()Ljava/util/List;",
        "getOtherReason",
        "getPreviouslyRefundedTenders",
        "getReasonOption",
        "()Lcom/squareup/protos/client/bills/Refund$ReasonOption;",
        "getRefundMode",
        "()Lcom/squareup/activity/refund/RefundMode;",
        "refundMoney",
        "getRefundMoney",
        "getRefundReason",
        "getRefundableAmount",
        "refundableCashTendersMoney",
        "getRefundableCashTendersMoney",
        "getRefundedItemizationDetails",
        "remainingRefundMoney",
        "getRemainingRefundMoney",
        "getResidualBillResponse",
        "()Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
        "getRestockRequest",
        "()Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;",
        "shouldShiftTendersToGiftCard",
        "getShouldShiftTendersToGiftCard",
        "shouldShowDisabledGiftCardButton",
        "getShouldShowDisabledGiftCardButton",
        "shouldShowEnabledGiftCardButton",
        "getShouldShowEnabledGiftCardButton",
        "getShouldShowRefundGiftCard",
        "getSourceBillId",
        "()Lcom/squareup/protos/client/IdPair;",
        "getTenderDetails",
        "tendersWithResidualMoney",
        "getTendersWithResidualMoney",
        "zeroMoney",
        "getZeroMoney",
        "canRefundTender",
        "tenderDetail",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component15",
        "component16",
        "component17",
        "component18",
        "component19",
        "component2",
        "component20",
        "component21",
        "component22",
        "component23",
        "component24",
        "component25",
        "component26",
        "component27",
        "component28",
        "component29",
        "component3",
        "component30",
        "component31",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "(Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;)Lcom/squareup/activity/refund/RefundData;",
        "copyAsExchange",
        "copyClearingAuthorizationData",
        "copyWithAuthorizationData",
        "readerType",
        "Lcom/squareup/protos/client/bills/CardData$ReaderType;",
        "cardData",
        "copyWithCashDrawerShift",
        "cashDrawerShift",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
        "copyWithGiftCard",
        "copyWithInvalidGiftCard",
        "copyWithIsGiftCardAndSwipe",
        "encryptedSwipe",
        "copyWithRefundAmount",
        "refundAmount",
        "copyWithRefundMode",
        "copyWithRefundReason",
        "reasonText",
        "copyWithRestockRequest",
        "copyWithSelectedItemForRestockIndices",
        "indices",
        "copyWithSelectedItemIndices",
        "copyWithTenderDetails",
        "createCartReturnData",
        "Lcom/squareup/checkout/ReturnCart;",
        "res",
        "Lcom/squareup/util/Res;",
        "namer",
        "Lcom/squareup/checkout/OrderVariationNamer;",
        "discountApplicationIdEnabled",
        "equals",
        "other",
        "getFirstTenderRequiringCardAuthorization",
        "getIndicesOfRestockableSelectedItems",
        "getItemNames",
        "",
        "()[Ljava/lang/String;",
        "getRefundAmount",
        "getRefundAmountForItemizationList",
        "itemizations",
        "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
        "getResidualItemsBySourceToken",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
        "getResidualTipsByTenderToken",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
        "getRestockIndices",
        "getReturnItemizations",
        "getReturnTips",
        "Lcom/squareup/protos/client/bills/ReturnTipLineItem;",
        "getRoundingAdjustment",
        "Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;",
        "getSelectedBillServerToken",
        "getSelectedIndices",
        "hasTenderRequiringCardAuthorization",
        "hashCode",
        "isItemizedAndAnyTaxIncluded",
        "isProcessingRestock",
        "isReadyForRefund",
        "populateCatalogInfo",
        "Lrx/Single;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "resetTenderDetails",
        "tenderHasBeenAtLeastPartiallyRefunded",
        "toString",
        "Companion",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/activity/refund/RefundData$Companion;


# instance fields
.field private final amountRefundMoney:Lcom/squareup/protos/common/Money;

.field private final authorizedEmployeeToken:Ljava/lang/String;

.field private final canOnlyIssueAmountBasedRefund:Z

.field private final catalogVersion:Ljava/lang/Long;

.field private final clientToken:Ljava/lang/String;

.field private final currentCashDrawerMoney:Lcom/squareup/protos/common/Money;

.field private final destinationGiftCard:Lcom/squareup/Card;

.field private final destinationGiftCardSwipe:Lokio/ByteString;

.field private final exchangeRefundMoney:Lcom/squareup/protos/common/Money;

.field private final hasDisabledIndices:Z

.field private final hasInvalidGiftCard:Z

.field private final hasItemizationsFromMultipleSourceBills:Z

.field private final hasResolvedGiftCard:Z

.field private final hasSelectedIndices:Z

.field private final isExchange:Z

.field private final isRefundingToGiftCard:Z

.field private final isSingleTender:Z

.field private final itemTokensByVariationTokens:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final itemizationDetails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation
.end field

.field private final mathRoundingMode:Ljava/math/RoundingMode;

.field private final otherReason:Ljava/lang/String;

.field private final previouslyRefundedTenders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

.field private final refundMode:Lcom/squareup/activity/refund/RefundMode;

.field private final refundReason:Ljava/lang/String;

.field private final refundableAmount:Lcom/squareup/protos/common/Money;

.field private final refundedItemizationDetails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation
.end field

.field private final residualBillResponse:Lcom/squareup/protos/client/bills/GetResidualBillResponse;

.field private final restockRequest:Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

.field private final selectedItemForRestockIndices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedItemIndices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final shouldShowRefundGiftCard:Z

.field private final sourceBillId:Lcom/squareup/protos/client/IdPair;

.field private final tenderDetails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/TenderDetails;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/activity/refund/RefundData$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/activity/refund/RefundData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/activity/refund/RefundData;->Companion:Lcom/squareup/activity/refund/RefundData$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/TenderDetails;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/activity/refund/RefundMode;",
            "Z",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Refund$ReasonOption;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            "Z",
            "Lcom/squareup/Card;",
            "Lokio/ByteString;",
            "ZZZZZ",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/math/RoundingMode;",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p8

    move-object/from16 v8, p10

    move-object/from16 v9, p13

    move-object/from16 v10, p14

    move-object/from16 v11, p15

    move-object/from16 v12, p16

    move-object/from16 v13, p20

    move-object/from16 v14, p21

    move-object/from16 v15, p28

    move-object/from16 v0, p29

    const-string v0, "residualBillResponse"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemizationDetails"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refundedItemizationDetails"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderDetails"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refundableAmount"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refundMode"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sourceBillId"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clientToken"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reasonOption"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "otherReason"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountRefundMoney"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previouslyRefundedTenders"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "destinationGiftCard"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "destinationGiftCardSwipe"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedItemIndices"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedItemForRestockIndices"

    move-object/from16 v15, p29

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mathRoundingMode"

    move-object/from16 v15, p30

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v15, p29

    iput-object v1, v0, Lcom/squareup/activity/refund/RefundData;->residualBillResponse:Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    iput-object v2, v0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    iput-object v3, v0, Lcom/squareup/activity/refund/RefundData;->refundedItemizationDetails:Ljava/util/List;

    iput-object v4, v0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    iput-object v5, v0, Lcom/squareup/activity/refund/RefundData;->refundableAmount:Lcom/squareup/protos/common/Money;

    iput-object v6, v0, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    move/from16 v1, p7

    iput-boolean v1, v0, Lcom/squareup/activity/refund/RefundData;->canOnlyIssueAmountBasedRefund:Z

    iput-object v7, v0, Lcom/squareup/activity/refund/RefundData;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    move-object/from16 v1, p9

    iput-object v1, v0, Lcom/squareup/activity/refund/RefundData;->authorizedEmployeeToken:Ljava/lang/String;

    iput-object v8, v0, Lcom/squareup/activity/refund/RefundData;->clientToken:Ljava/lang/String;

    move-object/from16 v1, p11

    iput-object v1, v0, Lcom/squareup/activity/refund/RefundData;->restockRequest:Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    move-object/from16 v1, p12

    iput-object v1, v0, Lcom/squareup/activity/refund/RefundData;->refundReason:Ljava/lang/String;

    iput-object v9, v0, Lcom/squareup/activity/refund/RefundData;->reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    iput-object v10, v0, Lcom/squareup/activity/refund/RefundData;->otherReason:Ljava/lang/String;

    iput-object v11, v0, Lcom/squareup/activity/refund/RefundData;->amountRefundMoney:Lcom/squareup/protos/common/Money;

    iput-object v12, v0, Lcom/squareup/activity/refund/RefundData;->previouslyRefundedTenders:Ljava/util/Map;

    move-object/from16 v1, p17

    iput-object v1, v0, Lcom/squareup/activity/refund/RefundData;->itemTokensByVariationTokens:Ljava/util/Map;

    move-object/from16 v1, p18

    iput-object v1, v0, Lcom/squareup/activity/refund/RefundData;->catalogVersion:Ljava/lang/Long;

    move/from16 v1, p19

    iput-boolean v1, v0, Lcom/squareup/activity/refund/RefundData;->isRefundingToGiftCard:Z

    iput-object v13, v0, Lcom/squareup/activity/refund/RefundData;->destinationGiftCard:Lcom/squareup/Card;

    iput-object v14, v0, Lcom/squareup/activity/refund/RefundData;->destinationGiftCardSwipe:Lokio/ByteString;

    move/from16 v1, p22

    iput-boolean v1, v0, Lcom/squareup/activity/refund/RefundData;->shouldShowRefundGiftCard:Z

    move/from16 v1, p23

    iput-boolean v1, v0, Lcom/squareup/activity/refund/RefundData;->isSingleTender:Z

    move/from16 v1, p24

    iput-boolean v1, v0, Lcom/squareup/activity/refund/RefundData;->hasResolvedGiftCard:Z

    move/from16 v1, p25

    iput-boolean v1, v0, Lcom/squareup/activity/refund/RefundData;->hasInvalidGiftCard:Z

    move/from16 v1, p26

    iput-boolean v1, v0, Lcom/squareup/activity/refund/RefundData;->isExchange:Z

    move-object/from16 v1, p27

    iput-object v1, v0, Lcom/squareup/activity/refund/RefundData;->currentCashDrawerMoney:Lcom/squareup/protos/common/Money;

    move-object/from16 v1, p28

    iput-object v1, v0, Lcom/squareup/activity/refund/RefundData;->selectedItemIndices:Ljava/util/List;

    iput-object v15, v0, Lcom/squareup/activity/refund/RefundData;->selectedItemForRestockIndices:Ljava/util/List;

    move-object/from16 v1, p30

    iput-object v1, v0, Lcom/squareup/activity/refund/RefundData;->mathRoundingMode:Ljava/math/RoundingMode;

    move-object/from16 v1, p31

    iput-object v1, v0, Lcom/squareup/activity/refund/RefundData;->exchangeRefundMoney:Lcom/squareup/protos/common/Money;

    .line 201
    iget-object v1, v0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 1059
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v2, Ljava/util/Map;

    .line 1060
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 1061
    move-object v4, v3

    check-cast v4, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 201
    invoke-virtual {v4}, Lcom/squareup/activity/refund/ItemizationDetails;->getSourceBillServerToken()Ljava/lang/String;

    move-result-object v4

    .line 1063
    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_0

    .line 1062
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1066
    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1062
    :cond_0
    check-cast v5, Ljava/util/List;

    .line 1070
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1072
    :cond_1
    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-le v1, v3, :cond_2

    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    iput-boolean v1, v0, Lcom/squareup/activity/refund/RefundData;->hasItemizationsFromMultipleSourceBills:Z

    .line 203
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v3

    iput-boolean v1, v0, Lcom/squareup/activity/refund/RefundData;->hasSelectedIndices:Z

    .line 210
    iget-boolean v1, v0, Lcom/squareup/activity/refund/RefundData;->hasItemizationsFromMultipleSourceBills:Z

    if-eqz v1, :cond_3

    iget-boolean v1, v0, Lcom/squareup/activity/refund/RefundData;->hasSelectedIndices:Z

    if-eqz v1, :cond_3

    const/4 v2, 0x1

    :cond_3
    iput-boolean v2, v0, Lcom/squareup/activity/refund/RefundData;->hasDisabledIndices:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 34

    move/from16 v0, p32

    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_0

    .line 84
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    move-object v4, v1

    goto :goto_0

    :cond_0
    move-object/from16 v4, p2

    :goto_0
    and-int/lit8 v1, v0, 0x4

    if-eqz v1, :cond_1

    .line 85
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    move-object v5, v1

    goto :goto_1

    :cond_1
    move-object/from16 v5, p3

    :goto_1
    and-int/lit8 v1, v0, 0x8

    if-eqz v1, :cond_2

    .line 86
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    move-object v6, v1

    goto :goto_2

    :cond_2
    move-object/from16 v6, p4

    :goto_2
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_3

    .line 88
    sget-object v1, Lcom/squareup/activity/refund/RefundMode;->ITEMS:Lcom/squareup/activity/refund/RefundMode;

    move-object v8, v1

    goto :goto_3

    :cond_3
    move-object/from16 v8, p6

    :goto_3
    and-int/lit16 v1, v0, 0x100

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    .line 91
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    move-object v11, v1

    goto :goto_4

    :cond_4
    move-object/from16 v11, p9

    :goto_4
    and-int/lit16 v1, v0, 0x400

    if-eqz v1, :cond_5

    .line 93
    move-object v1, v2

    check-cast v1, Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    move-object v13, v1

    goto :goto_5

    :cond_5
    move-object/from16 v13, p11

    :goto_5
    and-int/lit16 v1, v0, 0x800

    if-eqz v1, :cond_6

    .line 94
    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    move-object v14, v1

    goto :goto_6

    :cond_6
    move-object/from16 v14, p12

    :goto_6
    const/high16 v1, 0x10000

    and-int/2addr v1, v0

    if-eqz v1, :cond_7

    .line 99
    move-object v1, v2

    check-cast v1, Ljava/util/Map;

    move-object/from16 v19, v1

    goto :goto_7

    :cond_7
    move-object/from16 v19, p17

    :goto_7
    const/high16 v1, 0x20000

    and-int/2addr v1, v0

    if-eqz v1, :cond_8

    .line 100
    move-object v1, v2

    check-cast v1, Ljava/lang/Long;

    move-object/from16 v20, v1

    goto :goto_8

    :cond_8
    move-object/from16 v20, p18

    :goto_8
    const/high16 v1, 0x40000

    and-int/2addr v1, v0

    const/4 v3, 0x0

    if-eqz v1, :cond_9

    const/16 v21, 0x0

    goto :goto_9

    :cond_9
    move/from16 v21, p19

    :goto_9
    const/high16 v1, 0x80000

    and-int/2addr v1, v0

    if-eqz v1, :cond_a

    .line 102
    new-instance v1, Lcom/squareup/Card$Builder;

    invoke-direct {v1}, Lcom/squareup/Card$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object v1

    const-string v7, "Card.Builder().build()"

    invoke-static {v1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v22, v1

    goto :goto_a

    :cond_a
    move-object/from16 v22, p20

    :goto_a
    const/high16 v1, 0x100000

    and-int/2addr v1, v0

    if-eqz v1, :cond_b

    .line 103
    sget-object v1, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v23, v1

    goto :goto_b

    :cond_b
    move-object/from16 v23, p21

    :goto_b
    const/high16 v1, 0x200000

    and-int/2addr v1, v0

    if-eqz v1, :cond_c

    const/16 v24, 0x0

    goto :goto_c

    :cond_c
    move/from16 v24, p22

    :goto_c
    const/high16 v1, 0x400000

    and-int/2addr v1, v0

    if-eqz v1, :cond_d

    const/16 v25, 0x0

    goto :goto_d

    :cond_d
    move/from16 v25, p23

    :goto_d
    const/high16 v1, 0x800000

    and-int/2addr v1, v0

    if-eqz v1, :cond_e

    const/16 v26, 0x0

    goto :goto_e

    :cond_e
    move/from16 v26, p24

    :goto_e
    const/high16 v1, 0x1000000

    and-int/2addr v1, v0

    if-eqz v1, :cond_f

    const/16 v27, 0x0

    goto :goto_f

    :cond_f
    move/from16 v27, p25

    :goto_f
    const/high16 v1, 0x2000000

    and-int/2addr v1, v0

    if-eqz v1, :cond_10

    const/16 v28, 0x0

    goto :goto_10

    :cond_10
    move/from16 v28, p26

    :goto_10
    const/high16 v1, 0x4000000

    and-int/2addr v1, v0

    if-eqz v1, :cond_11

    .line 114
    move-object v1, v2

    check-cast v1, Lcom/squareup/protos/common/Money;

    move-object/from16 v29, v1

    goto :goto_11

    :cond_11
    move-object/from16 v29, p27

    :goto_11
    const/high16 v1, 0x40000000    # 2.0f

    and-int/2addr v0, v1

    if-eqz v0, :cond_12

    .line 124
    move-object v0, v2

    check-cast v0, Lcom/squareup/protos/common/Money;

    move-object/from16 v33, v0

    goto :goto_12

    :cond_12
    move-object/from16 v33, p31

    :goto_12
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v7, p5

    move/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v12, p10

    move-object/from16 v15, p13

    move-object/from16 v16, p14

    move-object/from16 v17, p15

    move-object/from16 v18, p16

    move-object/from16 v30, p28

    move-object/from16 v31, p29

    move-object/from16 v32, p30

    invoke-direct/range {v2 .. v33}, Lcom/squareup/activity/refund/RefundData;-><init>(Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public static final synthetic access$populateCatalogInfo(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/cogs/Cogs;)Lrx/Single;
    .locals 0

    .line 82
    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundData;->populateCatalogInfo(Lcom/squareup/cogs/Cogs;)Lrx/Single;

    move-result-object p0

    return-object p0
.end method

.method private final component27()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->currentCashDrawerMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method private final component28()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemIndices:Ljava/util/List;

    return-object v0
.end method

.method private final component29()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemForRestockIndices:Ljava/util/List;

    return-object v0
.end method

.method private final component30()Ljava/math/RoundingMode;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->mathRoundingMode:Ljava/math/RoundingMode;

    return-object v0
.end method

.method private final component31()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->exchangeRefundMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p32

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/activity/refund/RefundData;->residualBillResponse:Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    goto :goto_1

    :cond_1
    move-object/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/activity/refund/RefundData;->refundedItemizationDetails:Ljava/util/List;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/activity/refund/RefundData;->refundableAmount:Lcom/squareup/protos/common/Money;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/squareup/activity/refund/RefundData;->canOnlyIssueAmountBasedRefund:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/activity/refund/RefundData;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/activity/refund/RefundData;->authorizedEmployeeToken:Ljava/lang/String;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-object v11, v0, Lcom/squareup/activity/refund/RefundData;->clientToken:Ljava/lang/String;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget-object v12, v0, Lcom/squareup/activity/refund/RefundData;->restockRequest:Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    goto :goto_a

    :cond_a
    move-object/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget-object v13, v0, Lcom/squareup/activity/refund/RefundData;->refundReason:Ljava/lang/String;

    goto :goto_b

    :cond_b
    move-object/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget-object v14, v0, Lcom/squareup/activity/refund/RefundData;->reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    goto :goto_c

    :cond_c
    move-object/from16 v14, p13

    :goto_c
    and-int/lit16 v15, v1, 0x2000

    if-eqz v15, :cond_d

    iget-object v15, v0, Lcom/squareup/activity/refund/RefundData;->otherReason:Ljava/lang/String;

    goto :goto_d

    :cond_d
    move-object/from16 v15, p14

    :goto_d
    move-object/from16 p14, v15

    and-int/lit16 v15, v1, 0x4000

    if-eqz v15, :cond_e

    iget-object v15, v0, Lcom/squareup/activity/refund/RefundData;->amountRefundMoney:Lcom/squareup/protos/common/Money;

    goto :goto_e

    :cond_e
    move-object/from16 v15, p15

    :goto_e
    const v16, 0x8000

    and-int v16, v1, v16

    move-object/from16 p15, v15

    if-eqz v16, :cond_f

    iget-object v15, v0, Lcom/squareup/activity/refund/RefundData;->previouslyRefundedTenders:Ljava/util/Map;

    goto :goto_f

    :cond_f
    move-object/from16 v15, p16

    :goto_f
    const/high16 v16, 0x10000

    and-int v16, v1, v16

    move-object/from16 p16, v15

    if-eqz v16, :cond_10

    iget-object v15, v0, Lcom/squareup/activity/refund/RefundData;->itemTokensByVariationTokens:Ljava/util/Map;

    goto :goto_10

    :cond_10
    move-object/from16 v15, p17

    :goto_10
    const/high16 v16, 0x20000

    and-int v16, v1, v16

    move-object/from16 p17, v15

    if-eqz v16, :cond_11

    iget-object v15, v0, Lcom/squareup/activity/refund/RefundData;->catalogVersion:Ljava/lang/Long;

    goto :goto_11

    :cond_11
    move-object/from16 v15, p18

    :goto_11
    const/high16 v16, 0x40000

    and-int v16, v1, v16

    move-object/from16 p18, v15

    if-eqz v16, :cond_12

    iget-boolean v15, v0, Lcom/squareup/activity/refund/RefundData;->isRefundingToGiftCard:Z

    goto :goto_12

    :cond_12
    move/from16 v15, p19

    :goto_12
    const/high16 v16, 0x80000

    and-int v16, v1, v16

    move/from16 p19, v15

    if-eqz v16, :cond_13

    iget-object v15, v0, Lcom/squareup/activity/refund/RefundData;->destinationGiftCard:Lcom/squareup/Card;

    goto :goto_13

    :cond_13
    move-object/from16 v15, p20

    :goto_13
    const/high16 v16, 0x100000

    and-int v16, v1, v16

    move-object/from16 p20, v15

    if-eqz v16, :cond_14

    iget-object v15, v0, Lcom/squareup/activity/refund/RefundData;->destinationGiftCardSwipe:Lokio/ByteString;

    goto :goto_14

    :cond_14
    move-object/from16 v15, p21

    :goto_14
    const/high16 v16, 0x200000

    and-int v16, v1, v16

    move-object/from16 p21, v15

    if-eqz v16, :cond_15

    iget-boolean v15, v0, Lcom/squareup/activity/refund/RefundData;->shouldShowRefundGiftCard:Z

    goto :goto_15

    :cond_15
    move/from16 v15, p22

    :goto_15
    const/high16 v16, 0x400000

    and-int v16, v1, v16

    move/from16 p22, v15

    if-eqz v16, :cond_16

    iget-boolean v15, v0, Lcom/squareup/activity/refund/RefundData;->isSingleTender:Z

    goto :goto_16

    :cond_16
    move/from16 v15, p23

    :goto_16
    const/high16 v16, 0x800000

    and-int v16, v1, v16

    move/from16 p23, v15

    if-eqz v16, :cond_17

    iget-boolean v15, v0, Lcom/squareup/activity/refund/RefundData;->hasResolvedGiftCard:Z

    goto :goto_17

    :cond_17
    move/from16 v15, p24

    :goto_17
    const/high16 v16, 0x1000000

    and-int v16, v1, v16

    move/from16 p24, v15

    if-eqz v16, :cond_18

    iget-boolean v15, v0, Lcom/squareup/activity/refund/RefundData;->hasInvalidGiftCard:Z

    goto :goto_18

    :cond_18
    move/from16 v15, p25

    :goto_18
    const/high16 v16, 0x2000000

    and-int v16, v1, v16

    move/from16 p25, v15

    if-eqz v16, :cond_19

    iget-boolean v15, v0, Lcom/squareup/activity/refund/RefundData;->isExchange:Z

    goto :goto_19

    :cond_19
    move/from16 v15, p26

    :goto_19
    const/high16 v16, 0x4000000

    and-int v16, v1, v16

    move/from16 p26, v15

    if-eqz v16, :cond_1a

    iget-object v15, v0, Lcom/squareup/activity/refund/RefundData;->currentCashDrawerMoney:Lcom/squareup/protos/common/Money;

    goto :goto_1a

    :cond_1a
    move-object/from16 v15, p27

    :goto_1a
    const/high16 v16, 0x8000000

    and-int v16, v1, v16

    move-object/from16 p27, v15

    if-eqz v16, :cond_1b

    iget-object v15, v0, Lcom/squareup/activity/refund/RefundData;->selectedItemIndices:Ljava/util/List;

    goto :goto_1b

    :cond_1b
    move-object/from16 v15, p28

    :goto_1b
    const/high16 v16, 0x10000000

    and-int v16, v1, v16

    move-object/from16 p28, v15

    if-eqz v16, :cond_1c

    iget-object v15, v0, Lcom/squareup/activity/refund/RefundData;->selectedItemForRestockIndices:Ljava/util/List;

    goto :goto_1c

    :cond_1c
    move-object/from16 v15, p29

    :goto_1c
    const/high16 v16, 0x20000000

    and-int v16, v1, v16

    move-object/from16 p29, v15

    if-eqz v16, :cond_1d

    iget-object v15, v0, Lcom/squareup/activity/refund/RefundData;->mathRoundingMode:Ljava/math/RoundingMode;

    goto :goto_1d

    :cond_1d
    move-object/from16 v15, p30

    :goto_1d
    const/high16 v16, 0x40000000    # 2.0f

    and-int v1, v1, v16

    if-eqz v1, :cond_1e

    iget-object v1, v0, Lcom/squareup/activity/refund/RefundData;->exchangeRefundMoney:Lcom/squareup/protos/common/Money;

    goto :goto_1e

    :cond_1e
    move-object/from16 v1, p31

    :goto_1e
    move-object/from16 p1, v2

    move-object/from16 p2, v3

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move-object/from16 p11, v12

    move-object/from16 p12, v13

    move-object/from16 p13, v14

    move-object/from16 p30, v15

    move-object/from16 p31, v1

    invoke-virtual/range {p0 .. p31}, Lcom/squareup/activity/refund/RefundData;->copy(Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    return-object v0
.end method

.method public static final fromResidualBill(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLjava/util/Map;)Lcom/squareup/activity/refund/RefundData;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
            "Lcom/squareup/calc/constants/RoundingType;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Z",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/math/BigDecimal;",
            ">;)",
            "Lcom/squareup/activity/refund/RefundData;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/RefundData;->Companion:Lcom/squareup/activity/refund/RefundData$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    invoke-virtual/range {v0 .. v9}, Lcom/squareup/activity/refund/RefundData$Companion;->fromResidualBill(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;ZLjava/util/Map;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    return-object v0
.end method

.method public static final fromResidualBillAndCatalog(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/cogs/Cogs;ZLjava/util/Map;)Lrx/Single;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
            "Lcom/squareup/calc/constants/RoundingType;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/cogs/Cogs;",
            "Z",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/math/BigDecimal;",
            ">;)",
            "Lrx/Single<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/activity/refund/RefundData;->Companion:Lcom/squareup/activity/refund/RefundData$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    invoke-virtual/range {v0 .. v10}, Lcom/squareup/activity/refund/RefundData$Companion;->fromResidualBillAndCatalog(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Lcom/squareup/calc/constants/RoundingType;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/cogs/Cogs;ZLjava/util/Map;)Lrx/Single;

    move-result-object v0

    return-object v0
.end method

.method private final getRefundAmount(Ljava/util/List;)Lcom/squareup/protos/common/Money;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    .line 829
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    sget-object v1, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$6:[I

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 832
    invoke-virtual {p0, p1}, Lcom/squareup/activity/refund/RefundData;->getReturnItemizations(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/activity/refund/RefundData;->getRefundAmountForItemizationList(Ljava/util/List;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 833
    invoke-virtual {p0, p1}, Lcom/squareup/activity/refund/RefundData;->getReturnTips(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 1034
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 1035
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1036
    check-cast v2, Lcom/squareup/protos/client/bills/ReturnTipLineItem;

    .line 834
    iget-object v2, v2, Lcom/squareup/protos/client/bills/ReturnTipLineItem;->tip_line_item:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/TipLineItem;->amounts:Lcom/squareup/protos/client/bills/TipLineItem$Amounts;

    iget-object v2, v2, Lcom/squareup/protos/client/bills/TipLineItem$Amounts;->applied_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1037
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 835
    new-instance p1, Lcom/squareup/protos/common/Money;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v3

    invoke-direct {p1, v2, v3}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 1039
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/Money;

    .line 835
    invoke-static {p1, v2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_1

    .line 831
    :cond_1
    invoke-static {v0, p1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    const-string v0, "sum(\n          getRefund\u2026cyCode), MoneyMath::sum))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 830
    :cond_3
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundData;->amountRefundMoney:Lcom/squareup/protos/common/Money;

    :goto_2
    return-object p1
.end method

.method private final getRefundAmountForItemizationList(Ljava/util/List;)Lcom/squareup/protos/common/Money;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
            ">;)",
            "Lcom/squareup/protos/common/Money;"
        }
    .end annotation

    .line 847
    check-cast p1, Ljava/lang/Iterable;

    .line 1044
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 1045
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1046
    check-cast v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 848
    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Itemization$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1047
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 849
    new-instance p1, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-direct {p1, v1, v2}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 1049
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    .line 849
    invoke-static {p1, v1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    goto :goto_1

    :cond_1
    const-string v0, "itemizations\n        .ma\u2026ncyCode), MoneyMath::sum)"

    .line 1050
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final getRefundableCashTendersMoney()Lcom/squareup/protos/common/Money;
    .locals 5

    .line 216
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 884
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 885
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/activity/refund/TenderDetails;

    .line 217
    invoke-virtual {v3}, Lcom/squareup/activity/refund/TenderDetails;->getType()Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object v3

    sget-object v4, Lcom/squareup/billhistory/model/TenderHistory$Type;->CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-ne v3, v4, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 886
    :cond_2
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 887
    new-instance v0, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 888
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 889
    check-cast v2, Lcom/squareup/activity/refund/TenderDetails;

    .line 218
    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 890
    :cond_3
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 219
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getZeroMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 892
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/Money;

    .line 219
    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    goto :goto_3

    :cond_4
    const-string v0, "tenderDetails\n        .f\u2026  .fold(zeroMoney, ::sum)"

    .line 893
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method private final getResidualItemsBySourceToken()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;",
            ">;"
        }
    .end annotation

    .line 817
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->residualBillResponse:Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_itemization:Ljava/util/List;

    const-string v1, "residualBillResponse.residual_itemization"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 994
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v1, Ljava/util/Map;

    .line 995
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 996
    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;

    .line 818
    iget-object v3, v3, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;->source_itemization_token_pair:Lcom/squareup/protos/client/IdPair;

    .line 998
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    .line 997
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1001
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 997
    :cond_0
    check-cast v4, Ljava/util/List;

    .line 1005
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1008
    :cond_1
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v0, Ljava/util/Map;

    .line 1009
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 1010
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1011
    check-cast v2, Ljava/util/Map$Entry;

    .line 1009
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 819
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    return-object v0
.end method

.method private final getResidualTipsByTenderToken()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;",
            ">;"
        }
    .end annotation

    .line 824
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->residualBillResponse:Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse;->residual_tip:Ljava/util/List;

    const-string v1, "residualBillResponse.residual_tip"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 1014
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v1, Ljava/util/Map;

    .line 1015
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1016
    move-object v3, v2

    check-cast v3, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;

    .line 825
    iget-object v3, v3, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;->residual_tip:Lcom/squareup/protos/client/bills/TipLineItem;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/TipLineItem;->tender_server_token:Ljava/lang/String;

    .line 1018
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    .line 1017
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1021
    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1017
    :cond_0
    check-cast v4, Ljava/util/List;

    .line 1025
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1028
    :cond_1
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v2

    invoke-static {v2}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v0, Ljava/util/Map;

    .line 1029
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 1030
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1031
    check-cast v2, Ljava/util/Map$Entry;

    .line 1029
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    .line 826
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    return-object v0
.end method

.method private final isItemizedAndAnyTaxIncluded(Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .line 840
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    sget-object v1, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$7:[I

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_4

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    .line 842
    invoke-virtual {p0, p1}, Lcom/squareup/activity/refund/RefundData;->getReturnItemizations(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 1041
    instance-of v0, p1, Ljava/util/Collection;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    .line 1042
    :cond_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    .line 842
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;->itemization:Lcom/squareup/protos/client/bills/Itemization;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization;->amounts:Lcom/squareup/protos/client/bills/Itemization$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Itemization$Amounts;->tax_money:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v0, v3, v5

    if-lez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    .line 1043
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_4
    :goto_1
    return v2
.end method

.method private final populateCatalogInfo(Lcom/squareup/cogs/Cogs;)Lrx/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cogs/Cogs;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/activity/refund/RefundData;",
            ">;"
        }
    .end annotation

    .line 780
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 982
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 991
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 990
    check-cast v2, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 780
    invoke-virtual {v2}, Lcom/squareup/activity/refund/ItemizationDetails;->getCogsId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 990
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 993
    :cond_1
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 781
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 783
    new-instance v1, Lcom/squareup/activity/refund/RefundData$populateCatalogInfo$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/activity/refund/RefundData$populateCatalogInfo$1;-><init>(Lcom/squareup/activity/refund/RefundData;Ljava/util/Set;)V

    check-cast v1, Lcom/squareup/shared/catalog/CatalogTask;

    invoke-interface {p1, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object p1

    const-string v0, "cogs.asSingle { cogsLoca\u2026alogVersion\n      )\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final resetTenderDetails(Lcom/squareup/protos/common/Money;)Ljava/util/List;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/TenderDetails;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 852
    iget-object v2, v0, Lcom/squareup/activity/refund/RefundData;->refundableAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v1, v2}, Lcom/squareup/money/MoneyMath;->greaterThanOrEqualTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v2

    const/4 v3, 0x1

    const/16 v4, 0xa

    if-nez v2, :cond_2

    .line 853
    invoke-static/range {p1 .. p1}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/activity/refund/RefundData;->getTendersWithResidualMoney()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v3, :cond_0

    goto :goto_1

    .line 865
    :cond_0
    iget-object v1, v0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 1055
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v1, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 1056
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 1057
    move-object v4, v3

    check-cast v4, Lcom/squareup/activity/refund/TenderDetails;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 866
    new-instance v3, Lcom/squareup/protos/common/Money;

    move-object v10, v3

    const-wide/16 v11, 0x0

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/activity/refund/RefundData;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v12

    invoke-direct {v3, v11, v12}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x7f9f

    const/16 v21, 0x0

    invoke-static/range {v4 .. v21}, Lcom/squareup/activity/refund/TenderDetails;->copy$default(Lcom/squareup/activity/refund/TenderDetails;Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$AccountType;ZILjava/lang/Object;)Lcom/squareup/activity/refund/TenderDetails;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1058
    :cond_1
    check-cast v2, Ljava/util/List;

    goto :goto_5

    .line 856
    :cond_2
    :goto_1
    iget-object v2, v0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 1051
    new-instance v5, Ljava/util/ArrayList;

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v5, Ljava/util/Collection;

    .line 1052
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 1053
    move-object v6, v4

    check-cast v6, Lcom/squareup/activity/refund/TenderDetails;

    .line 857
    invoke-virtual {v6}, Lcom/squareup/activity/refund/TenderDetails;->getResidualRefundableMoney()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/squareup/money/MoneyMath;->min(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 858
    invoke-virtual {v6}, Lcom/squareup/activity/refund/TenderDetails;->getType()Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object v7

    sget-object v8, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$8:[I

    invoke-virtual {v7}, Lcom/squareup/billhistory/model/TenderHistory$Type;->ordinal()I

    move-result v7

    aget v7, v8, v7

    if-eq v7, v3, :cond_3

    :goto_3
    move-object v12, v4

    goto :goto_4

    .line 859
    :cond_3
    invoke-static {v4}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    goto :goto_3

    :goto_4
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-string v4, "roundedRefundMoney"

    .line 862
    invoke-static {v12, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x7f9f

    const/16 v23, 0x0

    invoke-static/range {v6 .. v23}, Lcom/squareup/activity/refund/TenderDetails;->copy$default(Lcom/squareup/activity/refund/TenderDetails;Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$AccountType;ZILjava/lang/Object;)Lcom/squareup/activity/refund/TenderDetails;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1054
    :cond_4
    move-object v2, v5

    check-cast v2, Ljava/util/List;

    :goto_5
    return-object v2
.end method


# virtual methods
.method public final canRefundTender(Lcom/squareup/activity/refund/TenderDetails;)Z
    .locals 7

    const-string v0, "tenderDetail"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 679
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    .line 681
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v0, v5, v3

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundableAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->isZero(Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/squareup/activity/refund/RefundData;->tenderHasBeenAtLeastPartiallyRefunded(Lcom/squareup/activity/refund/TenderDetails;)Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_1

    .line 682
    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long p1, v5, v3

    if-lez p1, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    return v1
.end method

.method public final component1()Lcom/squareup/protos/client/bills/GetResidualBillResponse;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->residualBillResponse:Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    return-object v0
.end method

.method public final component10()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->clientToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component11()Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->restockRequest:Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    return-object v0
.end method

.method public final component12()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundReason:Ljava/lang/String;

    return-object v0
.end method

.method public final component13()Lcom/squareup/protos/client/bills/Refund$ReasonOption;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    return-object v0
.end method

.method public final component14()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->otherReason:Ljava/lang/String;

    return-object v0
.end method

.method public final component15()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->amountRefundMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component16()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->previouslyRefundedTenders:Ljava/util/Map;

    return-object v0
.end method

.method public final component17()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->itemTokensByVariationTokens:Ljava/util/Map;

    return-object v0
.end method

.method public final component18()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->catalogVersion:Ljava/lang/Long;

    return-object v0
.end method

.method public final component19()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->isRefundingToGiftCard:Z

    return v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    return-object v0
.end method

.method public final component20()Lcom/squareup/Card;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->destinationGiftCard:Lcom/squareup/Card;

    return-object v0
.end method

.method public final component21()Lokio/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->destinationGiftCardSwipe:Lokio/ByteString;

    return-object v0
.end method

.method public final component22()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->shouldShowRefundGiftCard:Z

    return v0
.end method

.method public final component23()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->isSingleTender:Z

    return v0
.end method

.method public final component24()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->hasResolvedGiftCard:Z

    return v0
.end method

.method public final component25()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->hasInvalidGiftCard:Z

    return v0
.end method

.method public final component26()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->isExchange:Z

    return v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundedItemizationDetails:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/TenderDetails;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    return-object v0
.end method

.method public final component5()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundableAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component6()Lcom/squareup/activity/refund/RefundMode;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    return-object v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->canOnlyIssueAmountBasedRefund:Z

    return v0
.end method

.method public final component8()Lcom/squareup/protos/client/IdPair;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public final component9()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->authorizedEmployeeToken:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;)Lcom/squareup/activity/refund/RefundData;
    .locals 34
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/TenderDetails;",
            ">;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/activity/refund/RefundMode;",
            "Z",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Refund$ReasonOption;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            "Z",
            "Lcom/squareup/Card;",
            "Lokio/ByteString;",
            "ZZZZZ",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/math/RoundingMode;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lcom/squareup/activity/refund/RefundData;"
        }
    .end annotation

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v16, p16

    move-object/from16 v17, p17

    move-object/from16 v18, p18

    move/from16 v19, p19

    move-object/from16 v20, p20

    move-object/from16 v21, p21

    move/from16 v22, p22

    move/from16 v23, p23

    move/from16 v24, p24

    move/from16 v25, p25

    move/from16 v26, p26

    move-object/from16 v27, p27

    move-object/from16 v28, p28

    move-object/from16 v29, p29

    move-object/from16 v30, p30

    move-object/from16 v31, p31

    const-string v0, "residualBillResponse"

    move-object/from16 v32, v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemizationDetails"

    move-object/from16 v1, p2

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refundedItemizationDetails"

    move-object/from16 v1, p3

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderDetails"

    move-object/from16 v1, p4

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refundableAmount"

    move-object/from16 v1, p5

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refundMode"

    move-object/from16 v1, p6

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sourceBillId"

    move-object/from16 v1, p8

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clientToken"

    move-object/from16 v1, p10

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reasonOption"

    move-object/from16 v1, p13

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "otherReason"

    move-object/from16 v1, p14

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountRefundMoney"

    move-object/from16 v1, p15

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previouslyRefundedTenders"

    move-object/from16 v1, p16

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "destinationGiftCard"

    move-object/from16 v1, p20

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "destinationGiftCardSwipe"

    move-object/from16 v1, p21

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedItemIndices"

    move-object/from16 v1, p28

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedItemForRestockIndices"

    move-object/from16 v1, p29

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mathRoundingMode"

    move-object/from16 v1, p30

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v33, Lcom/squareup/activity/refund/RefundData;

    move-object/from16 v0, v33

    move-object/from16 v1, v32

    invoke-direct/range {v0 .. v31}, Lcom/squareup/activity/refund/RefundData;-><init>(Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;)V

    return-object v33
.end method

.method public final copyAsExchange(Lcom/squareup/protos/common/Money;)Lcom/squareup/activity/refund/RefundData;
    .locals 34

    if-nez p1, :cond_0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x1

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const v32, 0x7dffffff

    const/16 v33, 0x0

    move-object/from16 v0, p0

    .line 551
    invoke-static/range {v0 .. v33}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v26, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    .line 556
    invoke-direct/range {p0 .. p1}, Lcom/squareup/activity/refund/RefundData;->resetTenderDetails(Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v4

    const v32, 0x3dfffff7    # 0.12499993f

    const/16 v33, 0x0

    move-object/from16 v0, p0

    move-object/from16 v31, p1

    .line 553
    invoke-static/range {v0 .. v33}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final copyClearingAuthorizationData()Lcom/squareup/activity/refund/RefundData;
    .locals 34

    move-object/from16 v0, p0

    .line 490
    iget-object v1, v0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 902
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 903
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 904
    check-cast v3, Lcom/squareup/activity/refund/TenderDetails;

    .line 491
    invoke-virtual {v3}, Lcom/squareup/activity/refund/TenderDetails;->copyClearingCardData()Lcom/squareup/activity/refund/TenderDetails;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 905
    :cond_0
    move-object v4, v2

    check-cast v4, Ljava/util/List;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const v32, 0x7ffffff7

    const/16 v33, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    .line 489
    invoke-static/range {v0 .. v33}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    return-object v0
.end method

.method public final copyWithAuthorizationData(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;)Lcom/squareup/activity/refund/RefundData;
    .locals 37

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const-string v2, "readerType"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "cardData"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 477
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/activity/refund/RefundData;->getFirstTenderRequiringCardAuthorization()Lcom/squareup/activity/refund/TenderDetails;

    move-result-object v2

    move-object/from16 v3, p0

    .line 479
    iget-object v4, v3, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 898
    new-instance v5, Ljava/util/ArrayList;

    const/16 v6, 0xa

    invoke-static {v4, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v5, Ljava/util/Collection;

    .line 899
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    .line 900
    check-cast v6, Lcom/squareup/activity/refund/TenderDetails;

    .line 481
    invoke-static {v6, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v6, v0, v1}, Lcom/squareup/activity/refund/TenderDetails;->copyWithCardData(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;)Lcom/squareup/activity/refund/TenderDetails;

    move-result-object v6

    .line 483
    :cond_0
    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 901
    :cond_1
    move-object v7, v5

    check-cast v7, Ljava/util/List;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    const/16 v34, 0x0

    const v35, 0x7ffffff7

    const/16 v36, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v3, p0

    .line 478
    invoke-static/range {v3 .. v36}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    return-object v0
.end method

.method public final copyWithCashDrawerShift(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)Lcom/squareup/activity/refund/RefundData;
    .locals 35

    move-object/from16 v0, p1

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    .line 538
    iget-object v0, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const v33, 0x7bffffff

    const/16 v34, 0x0

    move-object/from16 v1, p0

    .line 537
    invoke-static/range {v1 .. v34}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    :goto_0
    return-object v0
.end method

.method public final copyWithGiftCard(ZLcom/squareup/Card;Z)Lcom/squareup/activity/refund/RefundData;
    .locals 34

    move-object/from16 v0, p0

    move/from16 v19, p1

    move-object/from16 v20, p2

    move/from16 v24, p3

    const-string v1, "destinationGiftCard"

    move-object/from16 v2, p2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 506
    sget-object v21, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const v32, 0x7e63ffff

    const/16 v33, 0x0

    .line 503
    invoke-static/range {v0 .. v33}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    return-object v0
.end method

.method public final copyWithInvalidGiftCard(Lcom/squareup/Card;)Lcom/squareup/activity/refund/RefundData;
    .locals 34

    move-object/from16 v0, p0

    move-object/from16 v20, p1

    const-string v1, "destinationGiftCard"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x1

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const v32, 0x7e77ffff

    const/16 v33, 0x0

    .line 527
    invoke-static/range {v0 .. v33}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    return-object v0
.end method

.method public final copyWithIsGiftCardAndSwipe(ZLcom/squareup/Card;Lokio/ByteString;)Lcom/squareup/activity/refund/RefundData;
    .locals 34

    move-object/from16 v0, p0

    move/from16 v19, p1

    move-object/from16 v20, p2

    move-object/from16 v21, p3

    const-string v1, "destinationGiftCard"

    move-object/from16 v2, p2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "encryptedSwipe"

    move-object/from16 v2, p3

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x1

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const v32, 0x7e63ffff

    const/16 v33, 0x0

    .line 516
    invoke-static/range {v0 .. v33}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    return-object v0
.end method

.method public final copyWithRefundAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/activity/refund/RefundData;
    .locals 34

    move-object/from16 v0, p0

    move-object/from16 v15, p1

    const-string v1, "refundAmount"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 437
    invoke-direct/range {p0 .. p1}, Lcom/squareup/activity/refund/RefundData;->resetTenderDetails(Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v4

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const v32, 0x7fffbff7

    const/16 v33, 0x0

    .line 435
    invoke-static/range {v0 .. v33}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    return-object v0
.end method

.method public final copyWithRefundMode(Lcom/squareup/activity/refund/RefundMode;)Lcom/squareup/activity/refund/RefundData;
    .locals 68

    move-object/from16 v0, p0

    move-object/from16 v6, p1

    const-string v1, "refundMode"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const v32, 0x7fffffdf

    const/16 v33, 0x0

    .line 440
    invoke-static/range {v0 .. v33}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    move-object/from16 v34, v0

    .line 446
    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundData;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/activity/refund/RefundData;->resetTenderDetails(Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v38

    const/16 v35, 0x0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/16 v39, 0x0

    const/16 v40, 0x0

    const/16 v41, 0x0

    const/16 v42, 0x0

    const/16 v43, 0x0

    const/16 v44, 0x0

    const/16 v45, 0x0

    const/16 v46, 0x0

    const/16 v47, 0x0

    const/16 v48, 0x0

    const/16 v49, 0x0

    const/16 v50, 0x0

    const/16 v51, 0x0

    const/16 v52, 0x0

    const/16 v53, 0x0

    const/16 v54, 0x0

    const/16 v55, 0x0

    const/16 v56, 0x0

    const/16 v57, 0x0

    const/16 v58, 0x0

    const/16 v59, 0x0

    const/16 v60, 0x0

    const/16 v61, 0x0

    const/16 v62, 0x0

    const/16 v63, 0x0

    const/16 v64, 0x0

    const/16 v65, 0x0

    const v66, 0x7ffffff7

    const/16 v67, 0x0

    .line 445
    invoke-static/range {v34 .. v67}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    return-object v0
.end method

.method public final copyWithRefundReason(Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;)Lcom/squareup/activity/refund/RefundData;
    .locals 35

    const-string v0, "reasonText"

    move-object/from16 v14, p1

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "reasonOption"

    move-object/from16 v13, p2

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 455
    sget-object v0, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const v33, 0x7fffe7ff

    const/16 v34, 0x0

    move-object/from16 v1, p0

    move-object/from16 v13, p1

    move-object/from16 v14, p2

    .line 461
    invoke-static/range {v1 .. v34}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const v33, 0x7fffc7ff

    const/16 v34, 0x0

    move-object/from16 v1, p0

    move-object/from16 v13, p1

    move-object/from16 v14, p2

    move-object/from16 v15, p1

    .line 456
    invoke-static/range {v1 .. v34}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final copyWithRestockRequest(Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;)Lcom/squareup/activity/refund/RefundData;
    .locals 34

    move-object/from16 v0, p0

    move-object/from16 v11, p1

    const-string v1, "restockRequest"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const v32, 0x7ffffbff

    const/16 v33, 0x0

    .line 496
    invoke-static/range {v0 .. v33}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    return-object v0
.end method

.method public final copyWithSelectedItemForRestockIndices(Ljava/util/List;)Lcom/squareup/activity/refund/RefundData;
    .locals 34
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/activity/refund/RefundData;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v29, p1

    const-string v1, "indices"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const v32, 0x6fffffff

    const/16 v33, 0x0

    .line 430
    invoke-static/range {v0 .. v33}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    return-object v0
.end method

.method public final copyWithSelectedItemIndices(Ljava/util/List;)Lcom/squareup/activity/refund/RefundData;
    .locals 34
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/squareup/activity/refund/RefundData;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v28, p1

    const-string v1, "indices"

    move-object/from16 v2, p1

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 419
    invoke-direct/range {p0 .. p1}, Lcom/squareup/activity/refund/RefundData;->getRefundAmount(Ljava/util/List;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    move-object/from16 v15, p0

    .line 421
    invoke-direct {v15, v1}, Lcom/squareup/activity/refund/RefundData;->resetTenderDetails(Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v4

    .line 425
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v29

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    move-object/from16 v15, v16

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const v32, 0x67fffff7

    const/16 v33, 0x0

    .line 420
    invoke-static/range {v0 .. v33}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v0

    return-object v0
.end method

.method public final copyWithTenderDetails(Lcom/squareup/activity/refund/TenderDetails;)Lcom/squareup/activity/refund/RefundData;
    .locals 35

    const-string v0, "tenderDetail"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v0, p0

    .line 468
    iget-object v2, v0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 894
    new-instance v3, Ljava/util/ArrayList;

    const/16 v4, 0xa

    invoke-static {v2, v4}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v3, Ljava/util/Collection;

    .line 895
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 896
    check-cast v4, Lcom/squareup/activity/refund/TenderDetails;

    .line 469
    invoke-virtual {v4}, Lcom/squareup/activity/refund/TenderDetails;->getSourceTenderToken()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/activity/refund/TenderDetails;->getSourceTenderToken()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v4, v1

    :cond_0
    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 897
    :cond_1
    move-object v5, v3

    check-cast v5, Ljava/util/List;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x0

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    const/16 v30, 0x0

    const/16 v31, 0x0

    const/16 v32, 0x0

    const v33, 0x7ffffff7

    const/16 v34, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v1, p0

    .line 467
    invoke-static/range {v1 .. v34}, Lcom/squareup/activity/refund/RefundData;->copy$default(Lcom/squareup/activity/refund/RefundData;Lcom/squareup/protos/client/bills/GetResidualBillResponse;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/common/Money;Lcom/squareup/activity/refund/RefundMode;ZLcom/squareup/protos/client/IdPair;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;Ljava/lang/String;Lcom/squareup/protos/client/bills/Refund$ReasonOption;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/util/Map;Ljava/util/Map;Ljava/lang/Long;ZLcom/squareup/Card;Lokio/ByteString;ZZZZZLcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Ljava/math/RoundingMode;Lcom/squareup/protos/common/Money;ILjava/lang/Object;)Lcom/squareup/activity/refund/RefundData;

    move-result-object v1

    return-object v1
.end method

.method public final createCartReturnData(Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;Z)Lcom/squareup/checkout/ReturnCart;
    .locals 10

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "namer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 718
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getSelectedBillServerToken()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 721
    sget-object v3, Lcom/squareup/checkout/ReturnCartItem;->Companion:Lcom/squareup/checkout/ReturnCartItem$Companion;

    .line 722
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/activity/refund/RefundData;->getReturnItemizations(Ljava/util/List;)Ljava/util/List;

    move-result-object v4

    .line 726
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v8

    move-object v5, p1

    move-object v6, p2

    move v7, p3

    .line 721
    invoke-virtual/range {v3 .. v8}, Lcom/squareup/checkout/ReturnCartItem$Companion;->fromProtos(Ljava/util/List;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;ZLjava/util/List;)Ljava/util/List;

    move-result-object v5

    .line 728
    sget-object p1, Lcom/squareup/checkout/ReturnTip;->Companion:Lcom/squareup/checkout/ReturnTip$Companion;

    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/squareup/activity/refund/RefundData;->getReturnTips(Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/checkout/ReturnTip$Companion;->fromReturnTipLineItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    .line 729
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/activity/refund/RefundData;->getRefundAmount(Ljava/util/List;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 731
    new-instance p1, Lcom/squareup/checkout/ReturnCart;

    .line 733
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v3

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, p1

    .line 731
    invoke-direct/range {v1 .. v9}, Lcom/squareup/checkout/ReturnCart;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object p1

    .line 719
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot create a ReturnCart with a null billServerId"

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/activity/refund/RefundData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/activity/refund/RefundData;

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->residualBillResponse:Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->residualBillResponse:Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundedItemizationDetails:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->refundedItemizationDetails:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundableAmount:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->refundableAmount:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->canOnlyIssueAmountBasedRefund:Z

    iget-boolean v1, p1, Lcom/squareup/activity/refund/RefundData;->canOnlyIssueAmountBasedRefund:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->authorizedEmployeeToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->authorizedEmployeeToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->clientToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->clientToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->restockRequest:Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->restockRequest:Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundReason:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->refundReason:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->otherReason:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->otherReason:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->amountRefundMoney:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->amountRefundMoney:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->previouslyRefundedTenders:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->previouslyRefundedTenders:Ljava/util/Map;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->itemTokensByVariationTokens:Ljava/util/Map;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->itemTokensByVariationTokens:Ljava/util/Map;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->catalogVersion:Ljava/lang/Long;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->catalogVersion:Ljava/lang/Long;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->isRefundingToGiftCard:Z

    iget-boolean v1, p1, Lcom/squareup/activity/refund/RefundData;->isRefundingToGiftCard:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->destinationGiftCard:Lcom/squareup/Card;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->destinationGiftCard:Lcom/squareup/Card;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->destinationGiftCardSwipe:Lokio/ByteString;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->destinationGiftCardSwipe:Lokio/ByteString;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->shouldShowRefundGiftCard:Z

    iget-boolean v1, p1, Lcom/squareup/activity/refund/RefundData;->shouldShowRefundGiftCard:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->isSingleTender:Z

    iget-boolean v1, p1, Lcom/squareup/activity/refund/RefundData;->isSingleTender:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->hasResolvedGiftCard:Z

    iget-boolean v1, p1, Lcom/squareup/activity/refund/RefundData;->hasResolvedGiftCard:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->hasInvalidGiftCard:Z

    iget-boolean v1, p1, Lcom/squareup/activity/refund/RefundData;->hasInvalidGiftCard:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->isExchange:Z

    iget-boolean v1, p1, Lcom/squareup/activity/refund/RefundData;->isExchange:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->currentCashDrawerMoney:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->currentCashDrawerMoney:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemIndices:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->selectedItemIndices:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemForRestockIndices:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->selectedItemForRestockIndices:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->mathRoundingMode:Ljava/math/RoundingMode;

    iget-object v1, p1, Lcom/squareup/activity/refund/RefundData;->mathRoundingMode:Ljava/math/RoundingMode;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->exchangeRefundMoney:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/activity/refund/RefundData;->exchangeRefundMoney:Lcom/squareup/protos/common/Money;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmountRefundMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->amountRefundMoney:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getAuthorizedEmployeeToken()Ljava/lang/String;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->authorizedEmployeeToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getCanOnlyIssueAmountBasedRefund()Z
    .locals 1

    .line 89
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->canOnlyIssueAmountBasedRefund:Z

    return v0
.end method

.method public final getCashDrawerMoneyAfterRefund()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 190
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->currentCashDrawerMoney:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_1

    .line 191
    iget-boolean v1, p0, Lcom/squareup/activity/refund/RefundData;->isRefundingToGiftCard:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 193
    :cond_0
    invoke-direct {p0}, Lcom/squareup/activity/refund/RefundData;->getRefundableCashTendersMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    goto :goto_1

    .line 195
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getZeroMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public final getCatalogVersion()Ljava/lang/Long;
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->catalogVersion:Ljava/lang/Long;

    return-object v0
.end method

.method public final getClientToken()Ljava/lang/String;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->clientToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 2

    .line 128
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundableAmount:Lcom/squareup/protos/common/Money;

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v1, "refundableAmount.currency_code"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getDestinationGiftCard()Lcom/squareup/Card;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->destinationGiftCard:Lcom/squareup/Card;

    return-object v0
.end method

.method public final getDestinationGiftCardSwipe()Lokio/ByteString;
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->destinationGiftCardSwipe:Lokio/ByteString;

    return-object v0
.end method

.method public final getDisplayRefundMoney()Lcom/squareup/protos/common/Money;
    .locals 5

    .line 143
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->exchangeRefundMoney:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    return-object v0

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundableAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/money/MoneyMath;->greaterThan(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 146
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getTendersWithResidualMoney()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 871
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_1

    .line 872
    :cond_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/TenderDetails;

    .line 146
    invoke-virtual {v1}, Lcom/squareup/activity/refund/TenderDetails;->getType()Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object v1

    sget-object v4, Lcom/squareup/billhistory/model/TenderHistory$Type;->CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-eq v1, v4, :cond_3

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    const/4 v3, 0x1

    :cond_4
    :goto_1
    if-eqz v3, :cond_5

    .line 147
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_2

    .line 149
    :cond_5
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_2
    const-string v1, "if (tendersWithResidualM\u2026ly(refundMoney)\n        }"

    .line 146
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_3

    .line 152
    :cond_6
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundableAmount:Lcom/squareup/protos/common/Money;

    :goto_3
    return-object v0
.end method

.method public final getFirstTenderRefundAmount()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 171
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/TenderDetails;

    invoke-virtual {v0}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public final getFirstTenderRequiringCardAuthorization()Lcom/squareup/activity/refund/TenderDetails;
    .locals 3

    .line 615
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 945
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/TenderDetails;

    .line 616
    invoke-virtual {v1}, Lcom/squareup/activity/refund/TenderDetails;->requiresCardAuthorization()Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    .line 946
    :cond_1
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Collection contains no element matching the predicate."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final getHasDisabledIndices()Z
    .locals 1

    .line 209
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->hasDisabledIndices:Z

    return v0
.end method

.method public final getHasInvalidGiftCard()Z
    .locals 1

    .line 107
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->hasInvalidGiftCard:Z

    return v0
.end method

.method public final getHasItemizationsFromMultipleSourceBills()Z
    .locals 1

    .line 200
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->hasItemizationsFromMultipleSourceBills:Z

    return v0
.end method

.method public final getHasResolvedGiftCard()Z
    .locals 1

    .line 106
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->hasResolvedGiftCard:Z

    return v0
.end method

.method public final getHasSelectedIndices()Z
    .locals 1

    .line 203
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->hasSelectedIndices:Z

    return v0
.end method

.method public final getIndicesOfRestockableSelectedItems()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 694
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    sget-object v1, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$5:[I

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 696
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemIndices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x0

    invoke-static {v2, v0}, Lkotlin/ranges/RangesKt;->until(II)Lkotlin/ranges/IntRange;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 966
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    check-cast v3, Ljava/util/Collection;

    .line 967
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->intValue()I

    move-result v5

    .line 698
    iget-object v6, p0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemIndices:Ljava/util/List;

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->intValue()I

    move-result v7

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/activity/refund/ItemizationDetails;

    invoke-virtual {v6}, Lcom/squareup/activity/refund/ItemizationDetails;->isItemization()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 699
    iget-object v6, p0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemIndices:Ljava/util/List;

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Number;

    invoke-virtual {v7}, Ljava/lang/Number;->intValue()I

    move-result v7

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/activity/refund/ItemizationDetails;

    invoke-virtual {v6}, Lcom/squareup/activity/refund/ItemizationDetails;->isInventoryTracked()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 700
    iget-object v6, p0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemIndices:Ljava/util/List;

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->intValue()I

    move-result v5

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/activity/refund/ItemizationDetails;

    invoke-virtual {v5}, Lcom/squareup/activity/refund/ItemizationDetails;->getMerchantCatalogToken()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    goto :goto_1

    :cond_1
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_0

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 968
    :cond_2
    check-cast v3, Ljava/util/List;

    goto :goto_2

    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 695
    :cond_4
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v3

    :goto_2
    return-object v3
.end method

.method public final getItemNames()[Ljava/lang/String;
    .locals 3

    .line 706
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 969
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 970
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 971
    check-cast v2, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 706
    invoke-virtual {v2}, Lcom/squareup/activity/refund/ItemizationDetails;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 972
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    .line 974
    invoke-interface {v1, v0}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, [Ljava/lang/String;

    return-object v0

    :cond_1
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.Array<T>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getItemTokensByVariationTokens()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 99
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->itemTokensByVariationTokens:Ljava/util/Map;

    return-object v0
.end method

.method public final getItemizationDetails()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    return-object v0
.end method

.method public final getOtherReason()Ljava/lang/String;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->otherReason:Ljava/lang/String;

    return-object v0
.end method

.method public final getPreviouslyRefundedTenders()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->previouslyRefundedTenders:Ljava/util/Map;

    return-object v0
.end method

.method public final getReasonOption()Lcom/squareup/protos/client/bills/Refund$ReasonOption;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    return-object v0
.end method

.method public final getRefundMode()Lcom/squareup/activity/refund/RefundMode;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    return-object v0
.end method

.method public final getRefundMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->exchangeRefundMoney:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemIndices:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/squareup/activity/refund/RefundData;->getRefundAmount(Ljava/util/List;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final getRefundReason()Ljava/lang/String;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundReason:Ljava/lang/String;

    return-object v0
.end method

.method public final getRefundableAmount()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 87
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundableAmount:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getRefundedItemizationDetails()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/ItemizationDetails;",
            ">;"
        }
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundedItemizationDetails:Ljava/util/List;

    return-object v0
.end method

.method public final getRemainingRefundMoney()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 157
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 874
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 875
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 876
    check-cast v2, Lcom/squareup/activity/refund/TenderDetails;

    .line 158
    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 877
    :cond_0
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 159
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getDisplayRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 879
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/common/Money;

    .line 159
    invoke-static {v0, v2}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_1

    :cond_1
    const-string v1, "tenderDetails\n        .m\u2026ney, MoneyMath::subtract)"

    .line 880
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final getResidualBillResponse()Lcom/squareup/protos/client/bills/GetResidualBillResponse;
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->residualBillResponse:Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    return-object v0
.end method

.method public final getRestockIndices()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 600
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    sget-object v1, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 602
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemForRestockIndices:Ljava/util/List;

    goto :goto_0

    :cond_0
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 601
    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final getRestockRequest()Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->restockRequest:Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    return-object v0
.end method

.method public final getReturnItemizations(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;",
            ">;"
        }
    .end annotation

    const-string v0, "indices"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 565
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    sget-object v1, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_9

    const/4 v1, 0x2

    if-ne v0, v1, :cond_8

    .line 567
    check-cast p1, Ljava/lang/Iterable;

    .line 906
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 907
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 908
    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->intValue()I

    move-result v1

    .line 568
    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/ItemizationDetails;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 909
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 910
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 911
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 569
    invoke-virtual {v2}, Lcom/squareup/activity/refund/ItemizationDetails;->isItemization()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 912
    :cond_2
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 913
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 914
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 915
    move-object v2, v1

    check-cast v2, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 570
    invoke-virtual {v2}, Lcom/squareup/activity/refund/ItemizationDetails;->getSourceItemizationTokenPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    .line 917
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_3

    .line 916
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 920
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 916
    :cond_3
    check-cast v3, Ljava/util/List;

    .line 924
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 927
    :cond_4
    new-instance p1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast p1, Ljava/util/Collection;

    .line 928
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 573
    invoke-direct {p0}, Lcom/squareup/activity/refund/RefundData;->getResidualItemsBySourceToken()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    check-cast v2, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;

    .line 576
    sget-object v3, Lcom/squareup/activity/refund/RefundData;->Companion:Lcom/squareup/activity/refund/RefundData$Companion;

    invoke-static {v3, v2}, Lcom/squareup/activity/refund/RefundData$Companion;->access$unitQuantityOrNull(Lcom/squareup/activity/refund/RefundData$Companion;Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;)Ljava/math/BigDecimal;

    move-result-object v3

    if-eqz v3, :cond_6

    goto :goto_4

    .line 577
    :cond_6
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v3, v1

    invoke-static {v3, v4}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v3

    const-string v1, "BigDecimal.valueOf(this.toLong())"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 578
    :goto_4
    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->mathRoundingMode:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v1}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->createReturnItemization(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualItemization;Ljava/math/BigDecimal;Ljava/math/RoundingMode;)Lcom/squareup/protos/client/bills/Cart$ReturnLineItems$ReturnItemization;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 930
    :cond_7
    check-cast p1, Ljava/util/List;

    goto :goto_5

    :cond_8
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 566
    :cond_9
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :goto_5
    return-object p1
.end method

.method public final getReturnTips(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/ReturnTipLineItem;",
            ">;"
        }
    .end annotation

    const-string v0, "indices"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 584
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    sget-object v1, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_6

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 586
    check-cast p1, Ljava/lang/Iterable;

    .line 931
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 932
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 933
    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    .line 587
    iget-object v3, p0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/activity/refund/ItemizationDetails;

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 934
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 935
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 936
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 588
    invoke-virtual {v3}, Lcom/squareup/activity/refund/ItemizationDetails;->isTip()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 937
    :cond_2
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 938
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 939
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 940
    check-cast v1, Lcom/squareup/activity/refund/ItemizationDetails;

    .line 589
    invoke-direct {p0}, Lcom/squareup/activity/refund/RefundData;->getResidualTipsByTenderToken()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/activity/refund/ItemizationDetails;->getSourceTipTenderToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    check-cast v1, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;

    invoke-static {v1}, Lcom/squareup/activity/refund/IssueRefundsRequestsKt;->createReturnTip(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTip;)Lcom/squareup/protos/client/bills/ReturnTipLineItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 941
    :cond_4
    check-cast v0, Ljava/util/List;

    goto :goto_3

    :cond_5
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 585
    :cond_6
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_3
    return-object v0
.end method

.method public final getRoundingAdjustment()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;
    .locals 6

    .line 745
    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, v3, v4}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 746
    iget-object v3, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v3, Ljava/lang/Iterable;

    .line 975
    new-instance v4, Ljava/util/ArrayList;

    const/16 v5, 0xa

    invoke-static {v3, v5}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v4, Ljava/util/Collection;

    .line 976
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 977
    check-cast v5, Lcom/squareup/activity/refund/TenderDetails;

    .line 747
    invoke-virtual {v5}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 978
    :cond_0
    check-cast v4, Ljava/util/List;

    check-cast v4, Ljava/lang/Iterable;

    .line 980
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/common/Money;

    .line 748
    invoke-static {v0, v4}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_1

    .line 750
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 752
    iget-object v3, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v3, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v5, v3, v1

    if-nez v5, :cond_3

    const/4 v0, 0x0

    return-object v0

    .line 756
    :cond_3
    :goto_2
    new-instance v1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts$Builder;-><init>()V

    .line 757
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts$Builder;->applied_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts$Builder;

    move-result-object v0

    .line 758
    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts$Builder;->build()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;

    move-result-object v0

    .line 760
    new-instance v1, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;-><init>()V

    .line 762
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->amounts(Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Amounts;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    .line 763
    sget-object v0, Lcom/squareup/api/items/CalculationPhase;->SWEDISH_ROUNDING_PHASE:Lcom/squareup/api/items/CalculationPhase;

    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->calculation_phase(Lcom/squareup/api/items/CalculationPhase;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    .line 765
    new-instance v0, Lcom/squareup/protos/client/IdPair$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/IdPair$Builder;-><init>()V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/IdPair$Builder;->client_id(Ljava/lang/String;)Lcom/squareup/protos/client/IdPair$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/IdPair$Builder;->build()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    .line 764
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->rounding_adjustment_line_item_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;

    .line 768
    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem$Builder;->build()Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    move-result-object v0

    return-object v0
.end method

.method public final getSelectedBillServerToken()Ljava/lang/String;
    .locals 2

    .line 225
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->firstOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 226
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/activity/refund/ItemizationDetails;

    invoke-virtual {v0}, Lcom/squareup/activity/refund/ItemizationDetails;->getSourceBillServerToken()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    iget-object v0, v0, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    :goto_0
    return-object v0
.end method

.method public final getSelectedIndices()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 594
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    sget-object v1, Lcom/squareup/activity/refund/RefundData$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {v0}, Lcom/squareup/activity/refund/RefundMode;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 596
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemIndices:Ljava/util/List;

    goto :goto_0

    :cond_0
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 595
    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public final getShouldShiftTendersToGiftCard()Z
    .locals 1

    .line 180
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->shouldShowRefundGiftCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->isRefundingToGiftCard:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getShouldShowDisabledGiftCardButton()Z
    .locals 1

    .line 177
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->shouldShowRefundGiftCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->isSingleTender:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getShouldShowEnabledGiftCardButton()Z
    .locals 1

    .line 174
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->shouldShowRefundGiftCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->isSingleTender:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final getShouldShowRefundGiftCard()Z
    .locals 1

    .line 104
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->shouldShowRefundGiftCard:Z

    return v0
.end method

.method public final getSourceBillId()Lcom/squareup/protos/client/IdPair;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    return-object v0
.end method

.method public final getTenderDetails()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/TenderDetails;",
            ">;"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    return-object v0
.end method

.method public final getTendersWithResidualMoney()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/activity/refund/TenderDetails;",
            ">;"
        }
    .end annotation

    .line 162
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 881
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 882
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/activity/refund/TenderDetails;

    .line 162
    invoke-virtual {v3}, Lcom/squareup/activity/refund/TenderDetails;->getResidualRefundableMoney()Lcom/squareup/protos/common/Money;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-lez v7, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    if-eqz v3, :cond_0

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 883
    :cond_2
    check-cast v1, Ljava/util/List;

    return-object v1
.end method

.method public final getZeroMoney()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 168
    new-instance v0, Lcom/squareup/protos/common/Money;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method

.method public final hasTenderRequiringCardAuthorization()Z
    .locals 3

    .line 608
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 942
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 943
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/activity/refund/TenderDetails;

    .line 608
    invoke-virtual {v1}, Lcom/squareup/activity/refund/TenderDetails;->requiresCardAuthorization()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :cond_2
    :goto_0
    return v2
.end method

.method public hashCode()I
    .locals 4

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->residualBillResponse:Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->refundedItemizationDetails:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->refundableAmount:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/activity/refund/RefundData;->canOnlyIssueAmountBasedRefund:Z

    const/4 v3, 0x1

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :cond_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_7
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->authorizedEmployeeToken:Ljava/lang/String;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_8
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->clientToken:Ljava/lang/String;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_9
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->restockRequest:Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_a
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->refundReason:Ljava/lang/String;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_b
    const/4 v2, 0x0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_c
    const/4 v2, 0x0

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->otherReason:Ljava/lang/String;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_c

    :cond_d
    const/4 v2, 0x0

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->amountRefundMoney:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_d

    :cond_e
    const/4 v2, 0x0

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->previouslyRefundedTenders:Ljava/util/Map;

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_e

    :cond_f
    const/4 v2, 0x0

    :goto_e
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->itemTokensByVariationTokens:Ljava/util/Map;

    if-eqz v2, :cond_10

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_f

    :cond_10
    const/4 v2, 0x0

    :goto_f
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->catalogVersion:Ljava/lang/Long;

    if-eqz v2, :cond_11

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_10

    :cond_11
    const/4 v2, 0x0

    :goto_10
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/activity/refund/RefundData;->isRefundingToGiftCard:Z

    if-eqz v2, :cond_12

    const/4 v2, 0x1

    :cond_12
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->destinationGiftCard:Lcom/squareup/Card;

    if-eqz v2, :cond_13

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_11

    :cond_13
    const/4 v2, 0x0

    :goto_11
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->destinationGiftCardSwipe:Lokio/ByteString;

    if-eqz v2, :cond_14

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_12

    :cond_14
    const/4 v2, 0x0

    :goto_12
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/activity/refund/RefundData;->shouldShowRefundGiftCard:Z

    if-eqz v2, :cond_15

    const/4 v2, 0x1

    :cond_15
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/activity/refund/RefundData;->isSingleTender:Z

    if-eqz v2, :cond_16

    const/4 v2, 0x1

    :cond_16
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/activity/refund/RefundData;->hasResolvedGiftCard:Z

    if-eqz v2, :cond_17

    const/4 v2, 0x1

    :cond_17
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/activity/refund/RefundData;->hasInvalidGiftCard:Z

    if-eqz v2, :cond_18

    const/4 v2, 0x1

    :cond_18
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/squareup/activity/refund/RefundData;->isExchange:Z

    if-eqz v2, :cond_19

    const/4 v2, 0x1

    :cond_19
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->currentCashDrawerMoney:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1a

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_13

    :cond_1a
    const/4 v2, 0x0

    :goto_13
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemIndices:Ljava/util/List;

    if-eqz v2, :cond_1b

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_14

    :cond_1b
    const/4 v2, 0x0

    :goto_14
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemForRestockIndices:Ljava/util/List;

    if-eqz v2, :cond_1c

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_15

    :cond_1c
    const/4 v2, 0x0

    :goto_15
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->mathRoundingMode:Ljava/math/RoundingMode;

    if-eqz v2, :cond_1d

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_16

    :cond_1d
    const/4 v2, 0x0

    :goto_16
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->exchangeRefundMoney:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_1e

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1e
    add-int/2addr v0, v1

    return v0
.end method

.method public final isExchange()Z
    .locals 1

    .line 112
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->isExchange:Z

    return v0
.end method

.method public final isProcessingRestock()Z
    .locals 1

    .line 676
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->restockRequest:Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isReadyForRefund()Z
    .locals 10

    .line 621
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundReason:Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 625
    :cond_0
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    sget-object v2, Lcom/squareup/protos/client/bills/Refund$ReasonOption;->OTHER_REASON:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    const/4 v3, 0x1

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->refundReason:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_2

    return v1

    .line 630
    :cond_2
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 947
    instance-of v2, v0, Ljava/util/Collection;

    if-eqz v2, :cond_4

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 948
    :cond_4
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/activity/refund/TenderDetails;

    .line 630
    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getType()Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object v4

    sget-object v5, Lcom/squareup/billhistory/model/TenderHistory$Type;->CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-ne v4, v5, :cond_6

    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/payment/SwedishRounding;->isRequired(Lcom/squareup/protos/common/Money;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_5

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_7

    return v1

    .line 636
    :cond_7
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 950
    instance-of v2, v0, Ljava/util/Collection;

    const-wide/16 v4, 0x0

    if-eqz v2, :cond_9

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_8
    const/4 v0, 0x0

    goto :goto_4

    .line 951
    :cond_9
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_a
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/activity/refund/TenderDetails;

    .line 636
    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getResidualAfterThisRefund()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v2, v6, v4

    if-gez v2, :cond_b

    const/4 v2, 0x1

    goto :goto_3

    :cond_b
    const/4 v2, 0x0

    :goto_3
    if-eqz v2, :cond_a

    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_c

    return v1

    .line 642
    :cond_c
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getRemainingRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v0, :cond_d

    goto :goto_5

    :cond_d
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v0, v6, v4

    if-nez v0, :cond_e

    return v3

    .line 646
    :cond_e
    :goto_5
    new-instance v0, Lcom/squareup/protos/common/Money;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v6

    iget-object v6, v6, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v0, v2, v6}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    .line 647
    iget-object v2, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 953
    new-instance v6, Ljava/util/ArrayList;

    const/16 v7, 0xa

    invoke-static {v2, v7}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v6, Ljava/util/Collection;

    .line 954
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_f

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 955
    check-cast v7, Lcom/squareup/activity/refund/TenderDetails;

    .line 648
    invoke-virtual {v7}, Lcom/squareup/activity/refund/TenderDetails;->getResidualRefundableMoney()Lcom/squareup/protos/common/Money;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 956
    :cond_f
    check-cast v6, Ljava/util/List;

    check-cast v6, Ljava/lang/Iterable;

    .line 958
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_10

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/protos/common/Money;

    .line 649
    invoke-static {v0, v6}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_7

    .line 652
    :cond_10
    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    const-string v2, "refundMoney.amount"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-gez v0, :cond_16

    .line 654
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 960
    instance-of v2, v0, Ljava/util/Collection;

    if-eqz v2, :cond_12

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_12

    :cond_11
    const/4 v0, 0x1

    goto :goto_a

    .line 961
    :cond_12
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_13
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/activity/refund/TenderDetails;

    .line 654
    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getResidualAfterThisRefund()Lcom/squareup/protos/common/Money;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v2, :cond_14

    goto :goto_8

    :cond_14
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v2, v6, v4

    if-nez v2, :cond_15

    const/4 v2, 0x1

    goto :goto_9

    :cond_15
    :goto_8
    const/4 v2, 0x0

    :goto_9
    if-nez v2, :cond_13

    const/4 v0, 0x0

    :goto_a
    if-eqz v0, :cond_16

    return v3

    .line 663
    :cond_16
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getTendersWithResidualMoney()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 963
    instance-of v2, v0, Ljava/util/Collection;

    if-eqz v2, :cond_18

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_18

    :cond_17
    const/4 v0, 0x0

    goto :goto_c

    .line 964
    :cond_18
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_19
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_17

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/activity/refund/TenderDetails;

    .line 663
    invoke-virtual {v2}, Lcom/squareup/activity/refund/TenderDetails;->getType()Lcom/squareup/billhistory/model/TenderHistory$Type;

    move-result-object v2

    sget-object v6, Lcom/squareup/billhistory/model/TenderHistory$Type;->CASH:Lcom/squareup/billhistory/model/TenderHistory$Type;

    if-eq v2, v6, :cond_1a

    const/4 v2, 0x1

    goto :goto_b

    :cond_1a
    const/4 v2, 0x0

    :goto_b
    if-eqz v2, :cond_19

    const/4 v0, 0x1

    :goto_c
    if-eqz v0, :cond_1b

    return v1

    .line 669
    :cond_1b
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getRemainingRefundMoney()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    if-nez v0, :cond_1c

    goto :goto_d

    :cond_1c
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v0, v6, v4

    if-nez v0, :cond_1d

    return v3

    :cond_1d
    :goto_d
    return v1
.end method

.method public final isRefundingToGiftCard()Z
    .locals 1

    .line 101
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->isRefundingToGiftCard:Z

    return v0
.end method

.method public final isSingleTender()Z
    .locals 1

    .line 105
    iget-boolean v0, p0, Lcom/squareup/activity/refund/RefundData;->isSingleTender:Z

    return v0
.end method

.method public final isTaxableItemSelected()Z
    .locals 1

    .line 165
    invoke-virtual {p0}, Lcom/squareup/activity/refund/RefundData;->getSelectedIndices()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/activity/refund/RefundData;->isItemizedAndAnyTaxIncluded(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public final tenderHasBeenAtLeastPartiallyRefunded(Lcom/squareup/activity/refund/TenderDetails;)Z
    .locals 1

    const-string v0, "tenderDetail"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 687
    iget-object v0, p0, Lcom/squareup/activity/refund/RefundData;->previouslyRefundedTenders:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/squareup/activity/refund/TenderDetails;->getSourceTenderToken()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RefundData(residualBillResponse="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->residualBillResponse:Lcom/squareup/protos/client/bills/GetResidualBillResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", itemizationDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->itemizationDetails:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", refundedItemizationDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->refundedItemizationDetails:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tenderDetails="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->tenderDetails:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", refundableAmount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->refundableAmount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", refundMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->refundMode:Lcom/squareup/activity/refund/RefundMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", canOnlyIssueAmountBasedRefund="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/activity/refund/RefundData;->canOnlyIssueAmountBasedRefund:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", sourceBillId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", authorizedEmployeeToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->authorizedEmployeeToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", clientToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->clientToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", restockRequest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->restockRequest:Lcom/squareup/protos/client/BatchAdjustVariationInventoryRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", refundReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->refundReason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", reasonOption="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->reasonOption:Lcom/squareup/protos/client/bills/Refund$ReasonOption;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", otherReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->otherReason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", amountRefundMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->amountRefundMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", previouslyRefundedTenders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->previouslyRefundedTenders:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", itemTokensByVariationTokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->itemTokensByVariationTokens:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", catalogVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->catalogVersion:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isRefundingToGiftCard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/activity/refund/RefundData;->isRefundingToGiftCard:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", destinationGiftCard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->destinationGiftCard:Lcom/squareup/Card;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", destinationGiftCardSwipe="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->destinationGiftCardSwipe:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shouldShowRefundGiftCard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/activity/refund/RefundData;->shouldShowRefundGiftCard:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isSingleTender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/activity/refund/RefundData;->isSingleTender:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", hasResolvedGiftCard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/activity/refund/RefundData;->hasResolvedGiftCard:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", hasInvalidGiftCard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/activity/refund/RefundData;->hasInvalidGiftCard:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", isExchange="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/activity/refund/RefundData;->isExchange:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", currentCashDrawerMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->currentCashDrawerMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedItemIndices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemIndices:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedItemForRestockIndices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->selectedItemForRestockIndices:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", mathRoundingMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->mathRoundingMode:Ljava/math/RoundingMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", exchangeRefundMoney="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/activity/refund/RefundData;->exchangeRefundMoney:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
