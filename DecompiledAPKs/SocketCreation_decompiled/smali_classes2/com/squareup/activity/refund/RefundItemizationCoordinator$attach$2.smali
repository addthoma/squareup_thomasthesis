.class final Lcom/squareup/activity/refund/RefundItemizationCoordinator$attach$2;
.super Ljava/lang/Object;
.source "RefundItemizationCoordinator.kt"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/activity/refund/RefundItemizationCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Landroid/widget/CompoundButton;",
        "kotlin.jvm.PlatformType",
        "isChecked",
        "",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/activity/refund/RefundItemizationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/activity/refund/RefundItemizationCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$attach$2;->this$0:Lcom/squareup/activity/refund/RefundItemizationCoordinator;

    iput-object p2, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$attach$2;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 109
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$attach$2;->this$0:Lcom/squareup/activity/refund/RefundItemizationCoordinator;

    invoke-static {p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->access$getEventHandler$p(Lcom/squareup/activity/refund/RefundItemizationCoordinator;)Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;

    move-result-object p1

    sget-object p2, Lcom/squareup/activity/refund/RefundMode;->AMOUNT:Lcom/squareup/activity/refund/RefundMode;

    invoke-interface {p1, p2}, Lcom/squareup/activity/refund/RefundItemizationCoordinator$RefundItemizationEventHandler;->onRefundModeSelected(Lcom/squareup/activity/refund/RefundMode;)V

    .line 110
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$attach$2;->this$0:Lcom/squareup/activity/refund/RefundItemizationCoordinator;

    invoke-static {p1}, Lcom/squareup/activity/refund/RefundItemizationCoordinator;->access$getRefundAmountEditor$p(Lcom/squareup/activity/refund/RefundItemizationCoordinator;)Lcom/squareup/widgets/SelectableEditText;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    .line 111
    iget-object p1, p0, Lcom/squareup/activity/refund/RefundItemizationCoordinator$attach$2;->$view:Landroid/view/View;

    const/4 p2, 0x1

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;Z)V

    :cond_0
    return-void
.end method
