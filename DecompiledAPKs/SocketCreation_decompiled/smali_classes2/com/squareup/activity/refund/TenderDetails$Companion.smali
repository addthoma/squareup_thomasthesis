.class public final Lcom/squareup/activity/refund/TenderDetails$Companion;
.super Ljava/lang/Object;
.source "TenderDetails.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/activity/refund/TenderDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTenderDetails.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TenderDetails.kt\ncom/squareup/activity/refund/TenderDetails$Companion\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,141:1\n215#2,2:142\n*E\n*S KotlinDebug\n*F\n+ 1 TenderDetails.kt\ncom/squareup/activity/refund/TenderDetails$Companion\n*L\n90#1,2:142\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J(\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0007\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/activity/refund/TenderDetails$Companion;",
        "",
        "()V",
        "of",
        "Lcom/squareup/activity/refund/TenderDetails;",
        "residualTender",
        "Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;",
        "sourceBillHistory",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "res",
        "Lcom/squareup/util/Res;",
        "editable",
        "",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 53
    invoke-direct {p0}, Lcom/squareup/activity/refund/TenderDetails$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final of(Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Z)Lcom/squareup/activity/refund/TenderDetails;
    .locals 24
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "residualTender"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "sourceBillHistory"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "res"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v3, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->tender:Lcom/squareup/protos/client/bills/Tender;

    const-string v4, "tender"

    .line 71
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1, v3}, Lcom/squareup/activity/refund/BillHistoryUtils;->getSourceBillIdForTender(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/Tender;)Lcom/squareup/protos/client/IdPair;

    move-result-object v4

    .line 72
    iget-object v5, v3, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/Tender$Amounts;->tip_money:Lcom/squareup/protos/common/Money;

    .line 73
    iget-object v6, v3, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v6, v6, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    const/4 v7, 0x0

    .line 69
    invoke-static {v3, v4, v5, v6, v7}, Lcom/squareup/billhistory/model/TenderHistory;->fromTender(Lcom/squareup/protos/client/bills/Tender;Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v4

    if-eqz p4, :cond_0

    .line 78
    new-instance v5, Lcom/squareup/protos/common/Money;

    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget-object v8, v3, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v8, v8, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v8, v8, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    invoke-direct {v5, v6, v8}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    goto :goto_0

    .line 80
    :cond_0
    iget-object v5, v3, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v5, v5, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    :goto_0
    move-object v14, v5

    const/4 v5, 0x0

    .line 87
    sget-object v6, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->UNKNOWN_ENTRY_METHOD:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    .line 88
    sget-object v8, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->DEFAULT_FELICA_BRAND_DO_NOT_USE:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    .line 89
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/billhistory/model/BillHistory;->getAllRelatedTenders()Ljava/util/List;

    move-result-object v1

    const-string v9, "sourceBillHistory.allRelatedTenders"

    invoke-static {v1, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Iterable;

    .line 142
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/squareup/billhistory/model/TenderHistory;

    .line 90
    invoke-virtual {v9}, Lcom/squareup/billhistory/model/TenderHistory;->getId()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->source_tender_server_token:Ljava/lang/String;

    invoke-static {v10, v11}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 92
    instance-of v1, v9, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    const-string v10, ""

    if-eqz v1, :cond_3

    .line 93
    check-cast v9, Lcom/squareup/billhistory/model/CreditCardTenderHistory;

    invoke-virtual {v9}, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->isRefundCardPresenceRequired()Z

    move-result v1

    .line 94
    iget-object v5, v9, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->brand:Lcom/squareup/Card$Brand;

    const-string v6, "it.brand"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/squareup/Card$Brand;->getHumanName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "it.brand.humanName"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iget-object v6, v9, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->felicaBrand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    if-eqz v6, :cond_2

    goto :goto_1

    :cond_2
    move-object v6, v8

    .line 96
    :goto_1
    iget-object v8, v9, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->cardSuffix:Ljava/lang/String;

    const-string v10, "it.cardSuffix"

    invoke-static {v8, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    iget-object v9, v9, Lcom/squareup/billhistory/model/CreditCardTenderHistory;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const-string v10, "it.entryMethod"

    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move/from16 v23, v1

    move-object/from16 v16, v5

    move-object/from16 v17, v6

    move-object/from16 v18, v8

    move-object/from16 v19, v9

    goto :goto_2

    :cond_3
    move-object/from16 v19, v6

    move-object/from16 v17, v8

    move-object/from16 v16, v10

    move-object/from16 v18, v16

    const/16 v23, 0x0

    .line 101
    :goto_2
    new-instance v1, Lcom/squareup/activity/refund/TenderDetails;

    .line 102
    iget-object v9, v0, Lcom/squareup/protos/client/bills/GetResidualBillResponse$ResidualTender;->source_tender_server_token:Ljava/lang/String;

    const-string v0, "residualTender.source_tender_server_token"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderHistory"

    .line 103
    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/billhistory/model/TenderHistory;->getBrandedTenderGlyphGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v10

    const-string v0, "tenderHistory.brandedTenderGlyphGlyph"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-virtual {v4, v2}, Lcom/squareup/billhistory/model/TenderHistory;->getDescription(Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object v11

    const-string v0, "tenderHistory.getDescription(res)"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object v12, v4, Lcom/squareup/billhistory/model/TenderHistory;->type:Lcom/squareup/billhistory/model/TenderHistory$Type;

    const-string v0, "tenderHistory.type"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    iget-object v0, v3, Lcom/squareup/protos/client/bills/Tender;->type:Lcom/squareup/protos/client/bills/Tender$Type;

    if-nez v0, :cond_4

    goto :goto_3

    :cond_4
    sget-object v2, Lcom/squareup/activity/refund/TenderDetails$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/protos/client/bills/Tender$Type;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    .line 108
    :goto_3
    iget-object v0, v3, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    :goto_4
    move-object v13, v0

    goto :goto_5

    .line 107
    :cond_5
    iget-object v0, v3, Lcom/squareup/protos/client/bills/Tender;->amounts:Lcom/squareup/protos/client/bills/Tender$Amounts;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    invoke-static {v0}, Lcom/squareup/payment/SwedishRounding;->apply(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_4

    :goto_5
    const-string/jumbo v0, "when (tender.type) {\n   \u2026s.total_money\n          }"

    .line 106
    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "totalMoneyForTender"

    .line 110
    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    sget-object v20, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    .line 117
    sget-object v21, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    .line 119
    iget-object v0, v3, Lcom/squareup/protos/client/bills/Tender;->method:Lcom/squareup/protos/client/bills/Tender$Method;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Tender$Method;->card_tender:Lcom/squareup/protos/client/bills/CardTender;

    if-eqz v0, :cond_6

    iget-object v7, v0, Lcom/squareup/protos/client/bills/CardTender;->account_type:Lcom/squareup/protos/client/bills/CardTender$AccountType;

    :cond_6
    move-object/from16 v22, v7

    move-object v8, v1

    move/from16 v15, p4

    .line 101
    invoke-direct/range {v8 .. v23}, Lcom/squareup/activity/refund/TenderDetails;-><init>(Ljava/lang/String;Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Lcom/squareup/billhistory/model/TenderHistory$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZLjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Lcom/squareup/protos/client/bills/CardData$ReaderType;Lokio/ByteString;Lcom/squareup/protos/client/bills/CardTender$AccountType;Z)V

    return-object v1

    .line 143
    :cond_7
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Collection contains no element matching the predicate."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
