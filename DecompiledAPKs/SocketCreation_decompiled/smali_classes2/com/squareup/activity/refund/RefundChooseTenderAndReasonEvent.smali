.class public final Lcom/squareup/activity/refund/RefundChooseTenderAndReasonEvent;
.super Lcom/squareup/activity/refund/RefundEvent;
.source "ItemizedRefundAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/activity/refund/RefundChooseTenderAndReasonEvent;",
        "Lcom/squareup/activity/refund/RefundEvent;",
        "()V",
        "activity_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/activity/refund/RefundChooseTenderAndReasonEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 158
    new-instance v0, Lcom/squareup/activity/refund/RefundChooseTenderAndReasonEvent;

    invoke-direct {v0}, Lcom/squareup/activity/refund/RefundChooseTenderAndReasonEvent;-><init>()V

    sput-object v0, Lcom/squareup/activity/refund/RefundChooseTenderAndReasonEvent;->INSTANCE:Lcom/squareup/activity/refund/RefundChooseTenderAndReasonEvent;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 159
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->VIEW:Lcom/squareup/eventstream/v1/EventStream$Name;

    const-string v1, "Itemized Refunds: Choose Tender and Reason"

    const/4 v2, 0x0

    .line 158
    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/activity/refund/RefundEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
