.class public final Lcom/squareup/AppBootstrapModule_ProvideEmailSupportLedgerEnabledFactory;
.super Ljava/lang/Object;
.source "AppBootstrapModule_ProvideEmailSupportLedgerEnabledFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/AppBootstrapModule;


# direct methods
.method public constructor <init>(Lcom/squareup/AppBootstrapModule;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/AppBootstrapModule_ProvideEmailSupportLedgerEnabledFactory;->module:Lcom/squareup/AppBootstrapModule;

    return-void
.end method

.method public static create(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/AppBootstrapModule_ProvideEmailSupportLedgerEnabledFactory;
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/AppBootstrapModule_ProvideEmailSupportLedgerEnabledFactory;

    invoke-direct {v0, p0}, Lcom/squareup/AppBootstrapModule_ProvideEmailSupportLedgerEnabledFactory;-><init>(Lcom/squareup/AppBootstrapModule;)V

    return-object v0
.end method

.method public static provideEmailSupportLedgerEnabled(Lcom/squareup/AppBootstrapModule;)Z
    .locals 0

    .line 32
    invoke-virtual {p0}, Lcom/squareup/AppBootstrapModule;->provideEmailSupportLedgerEnabled()Z

    move-result p0

    return p0
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/AppBootstrapModule_ProvideEmailSupportLedgerEnabledFactory;->module:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideEmailSupportLedgerEnabledFactory;->provideEmailSupportLedgerEnabled(Lcom/squareup/AppBootstrapModule;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/AppBootstrapModule_ProvideEmailSupportLedgerEnabledFactory;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
