.class public final Lcom/squareup/card/ConnectCardBrandConverter;
.super Ljava/lang/Object;
.source "ConnectCardBrandConverter.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nConnectCardBrandConverter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ConnectCardBrandConverter.kt\ncom/squareup/card/ConnectCardBrandConverter\n*L\n1#1,55:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "toCardBrand",
        "Lcom/squareup/Card$Brand;",
        "Lcom/squareup/protos/connect/v2/resources/Card$Brand;",
        "proto-utilities_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toCardBrand(Lcom/squareup/protos/connect/v2/resources/Card$Brand;)Lcom/squareup/Card$Brand;
    .locals 6

    .line 9
    invoke-static {}, Lcom/squareup/card/ConnectV2CardBrandMapping;->values()[Lcom/squareup/card/ConnectV2CardBrandMapping;

    move-result-object v0

    .line 10
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    invoke-virtual {v4}, Lcom/squareup/card/ConnectV2CardBrandMapping;->getConnectV2CardBrand$proto_utilities_release()Lcom/squareup/protos/connect/v2/resources/Card$Brand;

    move-result-object v5

    if-ne v5, p0, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    .line 11
    invoke-virtual {v4}, Lcom/squareup/card/ConnectV2CardBrandMapping;->getCardBrand$proto_utilities_release()Lcom/squareup/Card$Brand;

    move-result-object p0

    if-eqz p0, :cond_3

    goto :goto_3

    :cond_3
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    :goto_3
    return-object p0
.end method
