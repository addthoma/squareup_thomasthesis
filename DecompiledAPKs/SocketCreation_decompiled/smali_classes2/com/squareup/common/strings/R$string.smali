.class public final Lcom/squareup/common/strings/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/common/strings/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final activity_search_request_swipe:I = 0x7f12006b

.field public static final amount:I = 0x7f1200ca

.field public static final apply:I = 0x7f1200d5

.field public static final bank_account_settings:I = 0x7f120150

.field public static final cancel:I = 0x7f120280

.field public static final cart_tax_row_included:I = 0x7f120359

.field public static final ce_card_number_only_hint:I = 0x7f1203e9

.field public static final close_content_description:I = 0x7f12042f

.field public static final confirm:I = 0x7f12047c

.field public static final confirm_instant_transfer:I = 0x7f120483

.field public static final confirmation_popup_discard:I = 0x7f120487

.field public static final contactless_unable_to_process_title:I = 0x7f1204b7

.field public static final contactless_unlock_phone_to_search_title:I = 0x7f1204ba

.field public static final continue_label:I = 0x7f1204cf

.field public static final create:I = 0x7f1205bc

.field public static final create_pin_label:I = 0x7f1205e8

.field public static final crm_cardonfile_dip_retry:I = 0x7f120625

.field public static final crm_cardonfile_read_error:I = 0x7f12062b

.field public static final crm_cardonfile_swipe_retry:I = 0x7f120632

.field public static final dashboard_items_library_url:I = 0x7f1207c4

.field public static final default_itemization_name:I = 0x7f1207e3

.field public static final delete:I = 0x7f1207e5

.field public static final deposits_settings:I = 0x7f12082f

.field public static final devplat_error_cannot_verify_fingerprint:I = 0x7f12083d

.field public static final devplat_error_customer_not_supported:I = 0x7f12083e

.field public static final devplat_error_dev_portal_error_help:I = 0x7f12083f

.field public static final devplat_error_illegal_location_id:I = 0x7f120840

.field public static final devplat_error_invalid_api_version:I = 0x7f120841

.field public static final devplat_error_invalid_api_version_dialog_title:I = 0x7f120842

.field public static final devplat_error_invalid_charge_amount:I = 0x7f120843

.field public static final devplat_error_invalid_client_id:I = 0x7f120844

.field public static final devplat_error_invalid_currency:I = 0x7f120845

.field public static final devplat_error_invalid_customer_id:I = 0x7f120846

.field public static final devplat_error_invalid_fingerprint:I = 0x7f120847

.field public static final devplat_error_invalid_sandbox_client_id:I = 0x7f120848

.field public static final devplat_error_invalid_start_method:I = 0x7f120849

.field public static final devplat_error_invalid_start_method_dialog_title:I = 0x7f12084a

.field public static final devplat_error_invalid_timeout:I = 0x7f12084b

.field public static final devplat_error_invalid_web_callback_uri:I = 0x7f12084c

.field public static final devplat_error_missing_api_version:I = 0x7f12084d

.field public static final devplat_error_missing_charge_amount:I = 0x7f12084e

.field public static final devplat_error_missing_client_id:I = 0x7f12084f

.field public static final devplat_error_missing_currency:I = 0x7f120850

.field public static final devplat_error_missing_result:I = 0x7f120851

.field public static final devplat_error_missing_signature:I = 0x7f120852

.field public static final devplat_error_missing_tender_type:I = 0x7f120853

.field public static final devplat_error_missing_web_callback_uri:I = 0x7f120854

.field public static final devplat_error_no_employee_logged_in:I = 0x7f120855

.field public static final devplat_error_no_network:I = 0x7f120856

.field public static final devplat_error_no_server:I = 0x7f120857

.field public static final devplat_error_note_too_long:I = 0x7f120858

.field public static final devplat_error_timeout_too_high:I = 0x7f120859

.field public static final devplat_error_timeout_too_low:I = 0x7f12085a

.field public static final devplat_error_transaction_canceled:I = 0x7f12085b

.field public static final devplat_error_transaction_in_progress:I = 0x7f12085c

.field public static final devplat_error_transaction_stale:I = 0x7f12085d

.field public static final devplat_error_unknown_error:I = 0x7f12085e

.field public static final devplat_error_unknown_package:I = 0x7f12085f

.field public static final devplat_error_unsupported_api_version:I = 0x7f120860

.field public static final devplat_error_unsupported_web_api_version:I = 0x7f120861

.field public static final devplat_error_user_not_activated:I = 0x7f120862

.field public static final devplat_error_user_not_logged_in:I = 0x7f120863

.field public static final dismiss:I = 0x7f12088f

.field public static final done:I = 0x7f1208d3

.field public static final edit:I = 0x7f1208f9

.field public static final email_resending_failed:I = 0x7f1209a1

.field public static final employee_management_logged_out:I = 0x7f120a1e

.field public static final empty:I = 0x7f120a3a

.field public static final emv_card_error_message:I = 0x7f120a48

.field public static final emv_card_error_title:I = 0x7f120a49

.field public static final emv_fallback_title:I = 0x7f120a52

.field public static final emv_request_swipe_activity_search_message:I = 0x7f120a61

.field public static final emv_request_tap_activity_search_message:I = 0x7f120a62

.field public static final emv_request_tap_activity_search_title:I = 0x7f120a63

.field public static final error_default:I = 0x7f120a84

.field public static final failed:I = 0x7f120aa7

.field public static final finish:I = 0x7f120abf

.field public static final gift_card_hint_url:I = 0x7f120b16

.field public static final hud_reinsert_chip_card:I = 0x7f120beb

.field public static final hud_reinsert_chip_card_to_charge:I = 0x7f120bec

.field public static final hud_reinsert_chip_card_to_search:I = 0x7f120bed

.field public static final hud_remove_chip_card:I = 0x7f120bee

.field public static final instant_deposits_button_text:I = 0x7f120c15

.field public static final instant_deposits_deposit_failed:I = 0x7f120c1b

.field public static final instant_deposits_deposit_result_card_title:I = 0x7f120c1c

.field public static final instant_deposits_linked_card_hint:I = 0x7f120c27

.field public static final instant_deposits_network_error_message:I = 0x7f120c29

.field public static final instant_deposits_network_error_title:I = 0x7f120c2a

.field public static final instant_deposits_title:I = 0x7f120c33

.field public static final instant_deposits_unavailable:I = 0x7f120c35

.field public static final instant_deposits_unavailable_hint:I = 0x7f120c36

.field public static final instant_deposits_unavailable_learn_more:I = 0x7f120c37

.field public static final instant_deposits_unavailable_troubleshooting:I = 0x7f120c38

.field public static final invalid_email:I = 0x7f120c59

.field public static final invalid_email_message:I = 0x7f120c5a

.field public static final items:I = 0x7f120e45

.field public static final items_and_services:I = 0x7f120e46

.field public static final kitchen_printing_name_hint:I = 0x7f120ea7

.field public static final last_4_ssn_hint:I = 0x7f120eaf

.field public static final learn_more:I = 0x7f120eb5

.field public static final learn_more_lowercase_more:I = 0x7f120eb7

.field public static final loading:I = 0x7f120ee7

.field public static final name:I = 0x7f12104b

.field public static final nanp_phone_hint:I = 0x7f12104f

.field public static final network_error_message:I = 0x7f12105d

.field public static final network_error_title:I = 0x7f12105e

.field public static final next:I = 0x7f121074

.field public static final no_connection:I = 0x7f12107f

.field public static final no_customer:I = 0x7f121081

.field public static final ok:I = 0x7f1210e1

.field public static final okay:I = 0x7f1210e2

.field public static final open_file:I = 0x7f121164

.field public static final order_reader:I = 0x7f121202

.field public static final order_reader_magstripe:I = 0x7f121205

.field public static final order_reader_magstripe_validation_system_error_text:I = 0x7f121206

.field public static final order_reader_magstripe_validation_system_error_title:I = 0x7f121207

.field public static final order_validation_modified_text:I = 0x7f12121a

.field public static final order_validation_modified_title:I = 0x7f12121b

.field public static final order_validation_uncorrected_text:I = 0x7f12121c

.field public static final order_validation_uncorrected_title:I = 0x7f12121d

.field public static final payment_failed_card_not_charged:I = 0x7f1213c0

.field public static final payment_note_item_name_quantity:I = 0x7f1213c8

.field public static final payment_note_item_name_quantity_unit:I = 0x7f1213c9

.field public static final percent_character_postfix:I = 0x7f121430

.field public static final price_change_message:I = 0x7f121499

.field public static final price_change_title:I = 0x7f12149a

.field public static final re_enter_address_button_label:I = 0x7f121563

.field public static final reader_sdk_invalid_amount_too_high:I = 0x7f121584

.field public static final reader_sdk_invalid_amount_too_low:I = 0x7f121585

.field public static final retry:I = 0x7f1216a4

.field public static final save:I = 0x7f12176f

.field public static final send_reader_subheading:I = 0x7f1217c1

.field public static final server_error_message:I = 0x7f1217c7

.field public static final server_error_title:I = 0x7f1217c8

.field public static final session_expired_message:I = 0x7f1217cd

.field public static final session_expired_title:I = 0x7f1217ce

.field public static final set_up_instant_deposit:I = 0x7f1217d0

.field public static final shipping_details:I = 0x7f1217eb

.field public static final split_tender_amount_remaining:I = 0x7f121849

.field public static final square_dashboard:I = 0x7f121890

.field public static final ssn_hint:I = 0x7f1218aa

.field public static final swipe_failed_bad_swipe_message:I = 0x7f1218ee

.field public static final swipe_failed_bad_swipe_message_swipe_straight:I = 0x7f1218ef

.field public static final swipe_failed_bad_swipe_title_transactions_history:I = 0x7f1218f0

.field public static final swipe_failed_invalid_card_message:I = 0x7f1218f1

.field public static final swipe_failed_invalid_card_title:I = 0x7f1218f2

.field public static final swipe_failed_square_card_confirmation_message:I = 0x7f1218f3

.field public static final swipe_failed_square_card_confirmation_title:I = 0x7f1218f4

.field public static final tap_to_retry:I = 0x7f12191b

.field public static final update:I = 0x7f121af1

.field public static final upload:I = 0x7f121af8


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
