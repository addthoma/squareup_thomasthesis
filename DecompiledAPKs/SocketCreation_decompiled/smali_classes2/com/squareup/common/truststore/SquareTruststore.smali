.class public final Lcom/squareup/common/truststore/SquareTruststore;
.super Ljava/lang/Object;
.source "SquareTruststore.java"


# instance fields
.field public final sslSocketFactory:Lcom/squareup/common/truststore/SquareSSLSocketFactory;

.field public final trustManager:Ljavax/net/ssl/X509TrustManager;


# direct methods
.method private constructor <init>(Lcom/squareup/common/truststore/SquareSSLSocketFactory;Ljavax/net/ssl/X509TrustManager;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/common/truststore/SquareTruststore;->sslSocketFactory:Lcom/squareup/common/truststore/SquareSSLSocketFactory;

    .line 15
    iput-object p2, p0, Lcom/squareup/common/truststore/SquareTruststore;->trustManager:Ljavax/net/ssl/X509TrustManager;

    return-void
.end method

.method public static create(Landroid/content/Context;)Lcom/squareup/common/truststore/SquareTruststore;
    .locals 3

    .line 20
    :try_start_0
    sget v0, Lcom/squareup/common/truststore/R$raw;->truststore:I

    invoke-static {p0, v0}, Lcom/squareup/common/truststore/SquareSSLSocketFactory;->readTruststore(Landroid/content/Context;I)[Ljavax/net/ssl/TrustManager;

    move-result-object p0

    .line 21
    new-instance v0, Lcom/squareup/common/truststore/SquareSSLSocketFactory;

    invoke-direct {v0, p0}, Lcom/squareup/common/truststore/SquareSSLSocketFactory;-><init>([Ljavax/net/ssl/TrustManager;)V

    .line 22
    new-instance v1, Lcom/squareup/common/truststore/SquareTruststore;

    const/4 v2, 0x0

    aget-object p0, p0, v2

    check-cast p0, Ljavax/net/ssl/X509TrustManager;

    invoke-direct {v1, v0, p0}, Lcom/squareup/common/truststore/SquareTruststore;-><init>(Lcom/squareup/common/truststore/SquareSSLSocketFactory;Ljavax/net/ssl/X509TrustManager;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception p0

    .line 24
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method
