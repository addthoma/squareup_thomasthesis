.class public enum Lcom/squareup/Card$Brand;
.super Ljava/lang/Enum;
.source "Card.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4009
    name = "Brand"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/Card$Brand;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/Card$Brand;

.field public static final enum ALIPAY:Lcom/squareup/Card$Brand;

.field public static final enum AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

.field public static final enum CASH_APP:Lcom/squareup/Card$Brand;

.field public static final enum DISCOVER:Lcom/squareup/Card$Brand;

.field public static final enum DISCOVER_DINERS:Lcom/squareup/Card$Brand;

.field public static final enum EFTPOS:Lcom/squareup/Card$Brand;

.field public static final enum FELICA:Lcom/squareup/Card$Brand;

.field public static final enum INTERAC:Lcom/squareup/Card$Brand;

.field public static final enum JCB:Lcom/squareup/Card$Brand;

.field public static final enum MASTER_CARD:Lcom/squareup/Card$Brand;

.field public static final enum SQUARE_CAPITAL_CARD:Lcom/squareup/Card$Brand;

.field public static final enum SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

.field public static final enum UNION_PAY:Lcom/squareup/Card$Brand;

.field public static final enum UNKNOWN:Lcom/squareup/Card$Brand;

.field public static final enum VISA:Lcom/squareup/Card$Brand;

.field private static jsonToBrand:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/Card$Brand;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final humanName:Ljava/lang/String;

.field private final jsonRepresentation:Ljava/lang/String;

.field private final shortCode:Ljava/lang/String;

.field private final validNumberLengths:[I


# direct methods
.method static constructor <clinit>()V
    .locals 29

    .line 23
    new-instance v7, Lcom/squareup/Card$Brand$1;

    const/4 v8, 0x2

    new-array v6, v8, [I

    fill-array-data v6, :array_0

    const-string v1, "VISA"

    const/4 v2, 0x0

    const-string v3, "VI"

    const-string v4, "Visa"

    const-string/jumbo v5, "visa"

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/Card$Brand$1;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v7, Lcom/squareup/Card$Brand;->VISA:Lcom/squareup/Card$Brand;

    .line 38
    new-instance v0, Lcom/squareup/Card$Brand;

    const/4 v1, 0x4

    new-array v15, v1, [I

    fill-array-data v15, :array_1

    const-string v10, "MASTER_CARD"

    const/4 v11, 0x1

    const-string v12, "MC"

    const-string v13, "MasterCard"

    const-string v14, "mastercard"

    move-object v9, v0

    invoke-direct/range {v9 .. v15}, Lcom/squareup/Card$Brand;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    .line 40
    new-instance v0, Lcom/squareup/Card$Brand;

    new-array v3, v2, [I

    const-string v17, "ALIPAY"

    const/16 v18, 0x2

    const-string v19, "AL"

    const-string v20, "Alipay"

    const-string v21, "alipay"

    move-object/from16 v16, v0

    move-object/from16 v22, v3

    invoke-direct/range {v16 .. v22}, Lcom/squareup/Card$Brand;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v0, Lcom/squareup/Card$Brand;->ALIPAY:Lcom/squareup/Card$Brand;

    .line 42
    new-instance v0, Lcom/squareup/Card$Brand$2;

    const/4 v3, 0x1

    new-array v15, v3, [I

    const/16 v4, 0xf

    aput v4, v15, v2

    const-string v10, "AMERICAN_EXPRESS"

    const/4 v11, 0x3

    const-string v12, "AX"

    const-string v13, "American Express"

    const-string v14, "american_express"

    move-object v9, v0

    invoke-direct/range {v9 .. v15}, Lcom/squareup/Card$Brand$2;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v0, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    .line 48
    new-instance v0, Lcom/squareup/Card$Brand;

    new-array v5, v2, [I

    const-string v17, "CASH_APP"

    const/16 v18, 0x4

    const-string v19, "CA"

    const-string v20, "Cash App"

    const-string v21, "cash_app"

    move-object/from16 v16, v0

    move-object/from16 v22, v5

    invoke-direct/range {v16 .. v22}, Lcom/squareup/Card$Brand;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v0, Lcom/squareup/Card$Brand;->CASH_APP:Lcom/squareup/Card$Brand;

    .line 50
    new-instance v0, Lcom/squareup/Card$Brand;

    new-array v15, v8, [I

    fill-array-data v15, :array_2

    const-string v10, "DISCOVER"

    const/4 v11, 0x5

    const-string v12, "DI"

    const-string v13, "Discover"

    const-string v14, "discover"

    move-object v9, v0

    invoke-direct/range {v9 .. v15}, Lcom/squareup/Card$Brand;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v0, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    .line 52
    new-instance v0, Lcom/squareup/Card$Brand;

    new-array v5, v3, [I

    const/16 v6, 0xe

    aput v6, v5, v2

    const-string v17, "DISCOVER_DINERS"

    const/16 v18, 0x6

    const-string v19, "DD"

    const-string v20, "Diners Club"

    const-string v21, "discover_diners"

    move-object/from16 v16, v0

    move-object/from16 v22, v5

    invoke-direct/range {v16 .. v22}, Lcom/squareup/Card$Brand;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v0, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    .line 54
    new-instance v0, Lcom/squareup/Card$Brand;

    new-array v15, v1, [I

    fill-array-data v15, :array_3

    const-string v10, "EFTPOS"

    const/4 v11, 0x7

    const-string v12, "EF"

    const-string v13, "EFTPOS"

    const-string v14, "eftpos"

    move-object v9, v0

    invoke-direct/range {v9 .. v15}, Lcom/squareup/Card$Brand;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v0, Lcom/squareup/Card$Brand;->EFTPOS:Lcom/squareup/Card$Brand;

    .line 56
    new-instance v0, Lcom/squareup/Card$Brand$3;

    new-array v5, v2, [I

    const-string v17, "FELICA"

    const/16 v18, 0x8

    const-string v19, "FE"

    const-string v20, "Felica"

    const-string v21, "felica"

    move-object/from16 v16, v0

    move-object/from16 v22, v5

    invoke-direct/range {v16 .. v22}, Lcom/squareup/Card$Brand$3;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v0, Lcom/squareup/Card$Brand;->FELICA:Lcom/squareup/Card$Brand;

    .line 67
    new-instance v0, Lcom/squareup/Card$Brand;

    const/4 v5, 0x6

    new-array v15, v5, [I

    fill-array-data v15, :array_4

    const-string v10, "INTERAC"

    const/16 v11, 0x9

    const-string v12, "IN"

    const-string v13, "Interac"

    const-string v14, "interac"

    move-object v9, v0

    invoke-direct/range {v9 .. v15}, Lcom/squareup/Card$Brand;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    .line 69
    new-instance v0, Lcom/squareup/Card$Brand;

    const/4 v7, 0x3

    new-array v9, v7, [I

    fill-array-data v9, :array_5

    const-string v17, "JCB"

    const/16 v18, 0xa

    const-string v19, "JC"

    const-string v20, "JCB"

    const-string v21, "jcb"

    move-object/from16 v16, v0

    move-object/from16 v22, v9

    invoke-direct/range {v16 .. v22}, Lcom/squareup/Card$Brand;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v0, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    .line 71
    new-instance v0, Lcom/squareup/Card$Brand$4;

    new-array v9, v1, [I

    fill-array-data v9, :array_6

    const-string v11, "UNION_PAY"

    const/16 v12, 0xb

    const-string v13, "CU"

    const-string v14, "China UnionPay"

    const-string v15, "unionpay"

    move-object v10, v0

    move-object/from16 v16, v9

    invoke-direct/range {v10 .. v16}, Lcom/squareup/Card$Brand$4;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    .line 78
    new-instance v0, Lcom/squareup/Card$Brand;

    new-array v9, v3, [I

    const/16 v10, 0x10

    aput v10, v9, v2

    const-string v17, "SQUARE_GIFT_CARD_V2"

    const/16 v18, 0xc

    const-string v19, "SQ"

    const-string v20, "Gift Card"

    const-string v21, "square_gift_card_v2"

    move-object/from16 v16, v0

    move-object/from16 v22, v9

    invoke-direct/range {v16 .. v22}, Lcom/squareup/Card$Brand;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    .line 81
    new-instance v0, Lcom/squareup/Card$Brand;

    new-array v9, v3, [I

    aput v10, v9, v2

    const-string v23, "SQUARE_CAPITAL_CARD"

    const/16 v24, 0xd

    const-string v25, "SI"

    const-string v26, "Square Installment Card"

    const-string v27, "square_capital_card"

    move-object/from16 v22, v0

    move-object/from16 v28, v9

    invoke-direct/range {v22 .. v28}, Lcom/squareup/Card$Brand;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v0, Lcom/squareup/Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/Card$Brand;

    .line 85
    new-instance v0, Lcom/squareup/Card$Brand;

    const/4 v9, 0x7

    new-array v15, v9, [I

    fill-array-data v15, :array_7

    const-string v11, "UNKNOWN"

    const/16 v12, 0xe

    const-string v13, "XX"

    const-string v14, "Credit Card"

    const-string v16, "unknown"

    move-object v10, v0

    move-object/from16 v17, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v17

    invoke-direct/range {v10 .. v16}, Lcom/squareup/Card$Brand;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    sput-object v0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    new-array v0, v4, [Lcom/squareup/Card$Brand;

    .line 22
    sget-object v4, Lcom/squareup/Card$Brand;->VISA:Lcom/squareup/Card$Brand;

    aput-object v4, v0, v2

    sget-object v2, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    aput-object v2, v0, v3

    sget-object v2, Lcom/squareup/Card$Brand;->ALIPAY:Lcom/squareup/Card$Brand;

    aput-object v2, v0, v8

    sget-object v2, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    aput-object v2, v0, v7

    sget-object v2, Lcom/squareup/Card$Brand;->CASH_APP:Lcom/squareup/Card$Brand;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    const/4 v2, 0x5

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/Card$Brand;->EFTPOS:Lcom/squareup/Card$Brand;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/Card$Brand;->FELICA:Lcom/squareup/Card$Brand;

    const/16 v2, 0x8

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    const/16 v2, 0x9

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    const/16 v2, 0xa

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_CAPITAL_CARD:Lcom/squareup/Card$Brand;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/Card$Brand;->$VALUES:[Lcom/squareup/Card$Brand;

    return-void

    nop

    :array_0
    .array-data 4
        0x10
        0x13
    .end array-data

    :array_1
    .array-data 4
        0x10
        0x11
        0x12
        0x13
    .end array-data

    :array_2
    .array-data 4
        0x10
        0x13
    .end array-data

    :array_3
    .array-data 4
        0x10
        0x11
        0x12
        0x13
    .end array-data

    :array_4
    .array-data 4
        0xd
        0xe
        0x10
        0x11
        0x12
        0x13
    .end array-data

    :array_5
    .array-data 4
        0xe
        0xf
        0x10
    .end array-data

    :array_6
    .array-data 4
        0x10
        0x11
        0x12
        0x13
    .end array-data

    :array_7
    .array-data 4
        0xd
        0xe
        0xf
        0x10
        0x11
        0x12
        0x13
    .end array-data
.end method

.method private varargs constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[I)V"
        }
    .end annotation

    .line 96
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 97
    iput-object p3, p0, Lcom/squareup/Card$Brand;->shortCode:Ljava/lang/String;

    .line 98
    iput-object p4, p0, Lcom/squareup/Card$Brand;->humanName:Ljava/lang/String;

    .line 99
    iput-object p5, p0, Lcom/squareup/Card$Brand;->jsonRepresentation:Ljava/lang/String;

    .line 100
    iput-object p6, p0, Lcom/squareup/Card$Brand;->validNumberLengths:[I

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[ILcom/squareup/Card$1;)V
    .locals 0

    .line 22
    invoke-direct/range {p0 .. p6}, Lcom/squareup/Card$Brand;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    return-void
.end method

.method public static fromJson(Ljava/lang/String;)Lcom/squareup/Card$Brand;
    .locals 6

    .line 104
    sget-object v0, Lcom/squareup/Card$Brand;->jsonToBrand:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 105
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/squareup/Card$Brand;->jsonToBrand:Ljava/util/Map;

    .line 106
    invoke-static {}, Lcom/squareup/Card$Brand;->values()[Lcom/squareup/Card$Brand;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 107
    sget-object v4, Lcom/squareup/Card$Brand;->jsonToBrand:Ljava/util/Map;

    invoke-virtual {v3}, Lcom/squareup/Card$Brand;->getJsonRepresentation()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 111
    :cond_0
    sget-object v0, Lcom/squareup/Card$Brand;->jsonToBrand:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/Card$Brand;

    return-object p0
.end method

.method public static fromO1Code(I)Lcom/squareup/Card$Brand;
    .locals 3

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-ne p0, v0, :cond_0

    .line 152
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object p0

    .line 154
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal card issuer byte "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 150
    :cond_1
    sget-object p0, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    return-object p0

    .line 148
    :cond_2
    sget-object p0, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    return-object p0

    .line 146
    :cond_3
    sget-object p0, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    return-object p0

    .line 144
    :cond_4
    sget-object p0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object p0

    .line 142
    :cond_5
    sget-object p0, Lcom/squareup/Card$Brand;->VISA:Lcom/squareup/Card$Brand;

    return-object p0
.end method

.method public static fromShortCode(Ljava/lang/String;)Lcom/squareup/Card$Brand;
    .locals 5

    .line 115
    invoke-static {}, Lcom/squareup/Card$Brand;->values()[Lcom/squareup/Card$Brand;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 116
    invoke-virtual {v3}, Lcom/squareup/Card$Brand;->getShortCode()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static fromSqLinkCode(B)Lcom/squareup/Card$Brand;
    .locals 3

    packed-switch p0, :pswitch_data_0

    .line 206
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal card issuer byte: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :pswitch_0
    sget-object p0, Lcom/squareup/Card$Brand;->EFTPOS:Lcom/squareup/Card$Brand;

    return-object p0

    .line 202
    :pswitch_1
    sget-object p0, Lcom/squareup/Card$Brand;->INTERAC:Lcom/squareup/Card$Brand;

    return-object p0

    .line 200
    :pswitch_2
    sget-object p0, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    return-object p0

    .line 198
    :pswitch_3
    sget-object p0, Lcom/squareup/Card$Brand;->UNION_PAY:Lcom/squareup/Card$Brand;

    return-object p0

    .line 196
    :pswitch_4
    sget-object p0, Lcom/squareup/Card$Brand;->DISCOVER_DINERS:Lcom/squareup/Card$Brand;

    return-object p0

    .line 194
    :pswitch_5
    sget-object p0, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    return-object p0

    .line 192
    :pswitch_6
    sget-object p0, Lcom/squareup/Card$Brand;->JCB:Lcom/squareup/Card$Brand;

    return-object p0

    .line 190
    :pswitch_7
    sget-object p0, Lcom/squareup/Card$Brand;->AMERICAN_EXPRESS:Lcom/squareup/Card$Brand;

    return-object p0

    .line 188
    :pswitch_8
    sget-object p0, Lcom/squareup/Card$Brand;->DISCOVER:Lcom/squareup/Card$Brand;

    return-object p0

    .line 186
    :pswitch_9
    sget-object p0, Lcom/squareup/Card$Brand;->MASTER_CARD:Lcom/squareup/Card$Brand;

    return-object p0

    .line 184
    :pswitch_a
    sget-object p0, Lcom/squareup/Card$Brand;->VISA:Lcom/squareup/Card$Brand;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static maxNumberLength()I
    .locals 1

    const/16 v0, 0x10

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/Card$Brand;
    .locals 1

    .line 22
    const-class v0, Lcom/squareup/Card$Brand;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/Card$Brand;

    return-object p0
.end method

.method public static values()[Lcom/squareup/Card$Brand;
    .locals 1

    .line 22
    sget-object v0, Lcom/squareup/Card$Brand;->$VALUES:[Lcom/squareup/Card$Brand;

    invoke-virtual {v0}, [Lcom/squareup/Card$Brand;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/Card$Brand;

    return-object v0
.end method


# virtual methods
.method public checkForPanWarning(Ljava/lang/String;)Lcom/squareup/Card$PanWarning;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public cvvLength()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public getHumanName()Ljava/lang/String;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/Card$Brand;->humanName:Ljava/lang/String;

    return-object v0
.end method

.method public getJsonRepresentation()Ljava/lang/String;
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/Card$Brand;->jsonRepresentation:Ljava/lang/String;

    return-object v0
.end method

.method public getShortCode()Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/Card$Brand;->shortCode:Ljava/lang/String;

    return-object v0
.end method

.method public isMaximumNumberLength(I)Z
    .locals 3

    .line 220
    iget-object v0, p0, Lcom/squareup/Card$Brand;->validNumberLengths:[I

    array-length v1, v0

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    aget v0, v0, v1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2
.end method

.method public isValidNumberLength(I)Z
    .locals 5

    .line 212
    iget-object v0, p0, Lcom/squareup/Card$Brand;->validNumberLengths:[I

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget v4, v0, v3

    if-ne p1, v4, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public toO1Code()B
    .locals 3

    .line 160
    sget-object v0, Lcom/squareup/Card$1;->$SwitchMap$com$squareup$Card$Brand:[I

    invoke-virtual {p0}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    return v1

    :cond_0
    return v2

    :cond_1
    return v1

    :cond_2
    return v2

    :cond_3
    return v1

    :cond_4
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 255
    iget-object v0, p0, Lcom/squareup/Card$Brand;->shortCode:Ljava/lang/String;

    return-object v0
.end method

.method public unmaskedDigits()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public unmaskedDigitsFromPan(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 234
    invoke-virtual {p0}, Lcom/squareup/Card$Brand;->unmaskedDigits()I

    move-result v0

    if-eqz p1, :cond_1

    .line 235
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v0, :cond_0

    goto :goto_0

    .line 238
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v1, v0

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public validateLuhnIfRequired(Ljava/lang/String;)Z
    .locals 0

    .line 242
    invoke-static {p1}, Lcom/squareup/Luhn;->validate(Ljava/lang/CharSequence;)Z

    move-result p1

    return p1
.end method
