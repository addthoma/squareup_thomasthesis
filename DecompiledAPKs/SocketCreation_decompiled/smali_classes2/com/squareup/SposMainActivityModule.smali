.class public abstract Lcom/squareup/SposMainActivityModule;
.super Ljava/lang/Object;
.source "SposMainActivityModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/rootview/AppletDrawerRootViewSetupModule;,
        Lcom/squareup/ui/main/AppletsDrawerActionBarNavigationHelperModule;,
        Lcom/squareup/orderentry/OrderEntryAppletHomeModule;,
        Lcom/squareup/camerahelper/CameraHelperModule;,
        Lcom/squareup/ui/main/CommonMainActivityModule;,
        Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule;,
        Lcom/squareup/x2/NoSquareDeviceMainActivityModule;,
        Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule;,
        Lcom/squareup/ui/main/PosActivityAppletModule;,
        Lcom/squareup/register/tutorial/PosTutorialModule;,
        Lcom/squareup/ui/main/ReaderTutorialModule;,
        Lcom/squareup/tenderpayment/PosPaymentWorkflowModule;,
        Lcom/squareup/tenderpayment/invoices/InvoiceTenderSettingResolverModule;,
        Lcom/squareup/separatedprintouts/api/NoSeparatedPrintoutsWorkflowModule;,
        Lcom/squareup/feedback/NoAppFeedbackModule;,
        Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled$ContactAddressBookEnabledModule;,
        Lcom/squareup/invoices/NoEstimatesModule;,
        Lcom/squareup/egiftcard/activation/PosActivateEGiftCardModule;,
        Lcom/squareup/accountfreeze/wiring/AccountFreezeMainActivityModule;,
        Lcom/squareup/ui/ticket/OpenTicketsMainActivityModule;,
        Lcom/squareup/ui/balance/BalanceAppletModule;,
        Lcom/squareup/balance/squarecard/BalanceSquareCardMainModule;,
        Lcom/squareup/invoices/ShowCreateButtonInvoiceHistoryConfigurationModule;,
        Lcom/squareup/setupguide/NoSetupGuideMainActivityModule;,
        Lcom/squareup/ui/orderhub/util/config/OrderHubConfigModule;,
        Lcom/squareup/salesreport/SalesReportBackButtonModule;,
        Lcom/squareup/spos/receipts/SposReceiptAutoCloseProviderModule;,
        Lcom/squareup/reports/applet/ReportsAppletMainActivityModule;,
        Lcom/squareup/clientactiontranslation/ClientActionTranslationModule;,
        Lcom/squareup/ui/activity/ActivityAppletMainActivityModule;,
        Lcom/squareup/ui/items/ItemsAppletMainActivityModule;,
        Lcom/squareup/ui/crm/applet/CustomersAppletMainActivityModule;,
        Lcom/squareup/capital/flexloan/featuresupport/CapitalFlexLoanFeatureSupportedModule;,
        Lcom/squareup/crm/RealCustomerManagementModule;,
        Lcom/squareup/quickamounts/QuickAmountsModule;,
        Lcom/squareup/quickamounts/ui/QuickAmountsTutorialModule;,
        Lcom/squareup/loyaltycheckin/impl/noop/wiring/NoopLoyaltyFrontOfTransactionSettingModule;,
        Lcom/squareup/register/widgets/card/AndroidThirdPartyGiftCardPanValidationModule;,
        Lcom/squareup/ui/onboarding/OnboardingMainActivityModule;,
        Lcom/squareup/onboarding/OnboardingNotificationsModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideCoreTutorialCreators(Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;Lcom/squareup/disputes/DisputesTutorialCreator;Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;)Ljava/util/List;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;",
            "Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;",
            "Lcom/squareup/disputes/DisputesTutorialCreator;",
            "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/tutorialv2/TutorialCreator;",
            ">;"
        }
    .end annotation

    .line 197
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 198
    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    invoke-interface {v0, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    invoke-interface {v0, p5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    invoke-interface {v0, p6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static provideDeepLinkHandlers(Lcom/squareup/ui/help/HelpDeepLinksHandler;Lcom/squareup/reports/applet/ReportsDeepLinkHandler;Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;Lcom/squareup/ui/items/ItemsDeepLinkHandler;Lcom/squareup/ui/main/ReaderTutorialDeepLinkHandler;Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;Lcom/squareup/ui/balance/BalanceAppletDeepLinkHandler;Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;Lcom/squareup/orderentry/OrderEntryDeepLinkHandler;Lcom/squareup/ui/orderhub/linking/OrderHubDeepLinkHandler;Lcom/squareup/ui/activity/ActivityDeepLinkHandler;)Ljava/util/List;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/help/HelpDeepLinksHandler;",
            "Lcom/squareup/reports/applet/ReportsDeepLinkHandler;",
            "Lcom/squareup/ui/crm/applet/CustomersDeepLinkHandler;",
            "Lcom/squareup/invoices/ui/InvoicesDeepLinkHandler;",
            "Lcom/squareup/ui/items/ItemsDeepLinkHandler;",
            "Lcom/squareup/ui/main/ReaderTutorialDeepLinkHandler;",
            "Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;",
            "Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;",
            "Lcom/squareup/ui/balance/BalanceAppletDeepLinkHandler;",
            "Lcom/squareup/accountfreeze/AccountFreezeDeepLinkHandler;",
            "Lcom/squareup/register/tutorial/link/FirstPaymentTutorialDeepLinksHandler;",
            "Lcom/squareup/items/tutorial/CreateItemTutorialDeepLinkHandler;",
            "Lcom/squareup/orderentry/OrderEntryDeepLinkHandler;",
            "Lcom/squareup/ui/orderhub/linking/OrderHubDeepLinkHandler;",
            "Lcom/squareup/ui/activity/ActivityDeepLinkHandler;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/deeplinks/DeepLinkHandler;",
            ">;"
        }
    .end annotation

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/squareup/deeplinks/DeepLinkHandler;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    aput-object p3, v0, v1

    const/4 v1, 0x4

    aput-object p4, v0, v1

    const/4 v1, 0x5

    aput-object p5, v0, v1

    const/4 v1, 0x6

    aput-object p6, v0, v1

    const/4 v1, 0x7

    aput-object p7, v0, v1

    const/16 v1, 0x8

    aput-object p8, v0, v1

    const/16 v1, 0x9

    aput-object p9, v0, v1

    const/16 v1, 0xa

    aput-object p10, v0, v1

    const/16 v1, 0xb

    aput-object p11, v0, v1

    const/16 v1, 0xc

    aput-object p12, v0, v1

    const/16 v1, 0xd

    aput-object p13, v0, v1

    const/16 v1, 0xe

    aput-object p14, v0, v1

    .line 156
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method abstract provideAppointmentLinkingHandler(Lcom/squareup/appointmentsapi/NoAppointmentLinkingHandler;)Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideCardTutorialRunner(Lcom/squareup/register/tutorial/PosCardTutorialRunner;)Lcom/squareup/register/tutorial/CardTutorialRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideExchangesHost(Lcom/squareup/ui/activity/ExchangesHost$NoExchangesHost;)Lcom/squareup/ui/activity/ExchangesHost;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideOfflineModeToggle(Lcom/squareup/payment/offline/RealOfflineModeCanBeOffered;)Lcom/squareup/payment/offline/OfflineModeCanBeOffered;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideR12EducationDone(Lcom/squareup/ui/main/r12education/R12EducationDoneRedirectToPosApplet;)Lcom/squareup/ui/help/R12EducationDoneRedirector;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideReaderMessageBar(Lcom/squareup/messagebar/RealReaderMessageBar;)Lcom/squareup/messagebar/api/ReaderMessageBar;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTutorialApi(Lcom/squareup/register/tutorial/PosTutorialApi;)Lcom/squareup/register/tutorial/TutorialApi;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providesCofDippedCardInfoProcessor(Lcom/squareup/ui/crm/emv/PosCofDippedCardInfoProcessor;)Lcom/squareup/ui/crm/emv/CofDippedCardInfoProcessor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providesOpenTicketsSpinner(Lcom/squareup/opentickets/RealUnsyncedOpenTicketsSpinner;)Lcom/squareup/opentickets/UnsyncedOpenTicketsSpinner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
