.class public final Lcom/squareup/billhistoryui/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistoryui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final activity_applet_custom_tip:I = 0x7f120054

.field public static final activity_applet_load_all_activity:I = 0x7f120055

.field public static final activity_applet_network_error_message:I = 0x7f120056

.field public static final activity_applet_network_error_title:I = 0x7f120057

.field public static final activity_applet_no_payments_message:I = 0x7f120058

.field public static final activity_applet_no_payments_title:I = 0x7f120059

.field public static final activity_applet_no_previous_transactions_message:I = 0x7f12005a

.field public static final activity_applet_no_previous_transactions_title:I = 0x7f12005b

.field public static final activity_applet_search_dip:I = 0x7f12005c

.field public static final activity_applet_search_dip_tap:I = 0x7f12005d

.field public static final activity_applet_search_no_payments_message:I = 0x7f12005e

.field public static final activity_applet_search_no_payments_title:I = 0x7f12005f

.field public static final activity_applet_search_no_payments_title_deprecated:I = 0x7f120060

.field public static final activity_applet_search_none:I = 0x7f120061

.field public static final activity_applet_search_swipe:I = 0x7f120062

.field public static final activity_applet_search_swipe_dip:I = 0x7f120063

.field public static final activity_applet_search_swipe_dip_tap:I = 0x7f120064

.field public static final activity_applet_search_with_card_reader:I = 0x7f120065

.field public static final activity_applet_search_without_card_reader:I = 0x7f120066

.field public static final activity_applet_search_x2_swipe_dip:I = 0x7f120067

.field public static final activity_applet_search_x2_swipe_dip_tap:I = 0x7f120068

.field public static final activity_applet_tap_disabled:I = 0x7f120069

.field public static final activity_applet_tap_enabled:I = 0x7f12006a

.field public static final add_tip:I = 0x7f120095

.field public static final add_tip_button:I = 0x7f120096

.field public static final additional_tip:I = 0x7f1200a6

.field public static final awaiting_tip:I = 0x7f120119

.field public static final bill_history_empty_view_with_dip_card_reader:I = 0x7f12016c

.field public static final bill_history_empty_view_with_swipe_and_dip_card_readers:I = 0x7f12016d

.field public static final bill_history_empty_view_with_swipe_card_reader:I = 0x7f12016e

.field public static final bill_history_empty_view_without_card_reader:I = 0x7f12016f

.field public static final bill_history_loyalty_earned:I = 0x7f120170

.field public static final bill_history_loyalty_earned_reward_plural:I = 0x7f120171

.field public static final bill_history_loyalty_earned_reward_single:I = 0x7f120172

.field public static final bill_history_loyalty_earned_singular:I = 0x7f120173

.field public static final bill_history_loyalty_earned_zero:I = 0x7f120174

.field public static final bill_history_loyalty_enrolled_in_loyalty:I = 0x7f120175

.field public static final bill_history_loyalty_title_uppercase:I = 0x7f120176

.field public static final bill_history_loyalty_zero_earned_reason_buyer_declined:I = 0x7f120177

.field public static final bill_history_loyalty_zero_earned_reason_client_disabled_loyalty:I = 0x7f120178

.field public static final bill_history_loyalty_zero_earned_reason_missing_loyalty_info:I = 0x7f120179

.field public static final bill_history_loyalty_zero_earned_reason_not_yet_subscribed:I = 0x7f12017a

.field public static final bill_history_loyalty_zero_earned_reason_offline_mode:I = 0x7f12017b

.field public static final bill_history_loyalty_zero_earned_reason_purchase_did_not_qualify:I = 0x7f12017c

.field public static final bill_history_loyalty_zero_earned_reason_returned_from_loyalty_screen:I = 0x7f12017d

.field public static final bill_history_loyalty_zero_earned_reason_returned_from_receipt_screen:I = 0x7f12017e

.field public static final bill_history_loyalty_zero_earned_reason_skipped_screen:I = 0x7f12017f

.field public static final bill_history_loyalty_zero_earned_reason_spend_too_low:I = 0x7f120180

.field public static final bill_history_tender_section_cashier:I = 0x7f120181

.field public static final bulk_settle_add_tips_title:I = 0x7f120198

.field public static final bulk_settle_button_hint:I = 0x7f120199

.field public static final bulk_settle_confirm_settle_button:I = 0x7f12019a

.field public static final bulk_settle_finish_unsettled_body_many:I = 0x7f12019b

.field public static final bulk_settle_finish_unsettled_body_one:I = 0x7f12019c

.field public static final bulk_settle_finish_unsettled_discard_tips_many:I = 0x7f12019d

.field public static final bulk_settle_finish_unsettled_discard_tips_one:I = 0x7f12019e

.field public static final bulk_settle_finish_unsettled_title_many:I = 0x7f12019f

.field public static final bulk_settle_finish_unsettled_title_one:I = 0x7f1201a0

.field public static final bulk_settle_no_payments:I = 0x7f1201a1

.field public static final bulk_settle_receipt_number:I = 0x7f1201a3

.field public static final bulk_settle_search_hint:I = 0x7f1201a4

.field public static final bulk_settle_settle_button_nonzero:I = 0x7f1201a5

.field public static final bulk_settle_settle_button_zero:I = 0x7f1201a6

.field public static final bulk_settle_settle_failed_body_many:I = 0x7f1201a7

.field public static final bulk_settle_settle_failed_body_one:I = 0x7f1201a8

.field public static final bulk_settle_tender_row_total:I = 0x7f1201a9

.field public static final bulk_settle_tender_row_total_with_percentage:I = 0x7f1201aa

.field public static final bulk_settle_uppercase_header_all_tips:I = 0x7f1201ab

.field public static final bulk_settle_uppercase_header_your_tips:I = 0x7f1201ac

.field public static final bulk_settle_uppercase_sort_amount:I = 0x7f1201ad

.field public static final bulk_settle_uppercase_sort_employee:I = 0x7f1201ae

.field public static final bulk_settle_uppercase_sort_time:I = 0x7f1201af

.field public static final bulk_settle_view_all_button:I = 0x7f1201b0

.field public static final buyer_send_receipt_email:I = 0x7f120262

.field public static final buyer_send_receipt_printed:I = 0x7f120263

.field public static final buyer_send_receipt_text:I = 0x7f120265

.field public static final card_info:I = 0x7f12030f

.field public static final collected_by:I = 0x7f120435

.field public static final comp_reason_attributed:I = 0x7f12045e

.field public static final comp_reason_default:I = 0x7f12045f

.field public static final diagnostics_logged:I = 0x7f120865

.field public static final gift_card_checking:I = 0x7f120b03

.field public static final gift_card_checking_failure:I = 0x7f120b04

.field public static final gift_card_checking_failure_not_active_desc:I = 0x7f120b05

.field public static final gift_card_checking_failure_not_active_title:I = 0x7f120b06

.field public static final google_play_square_pos_url:I = 0x7f120b46

.field public static final instant_deposits_processing_deposit:I = 0x7f120c2c

.field public static final instant_deposits_update_debit_card_information:I = 0x7f120c39

.field public static final no_internet_connection:I = 0x7f121088

.field public static final offline_mode_url:I = 0x7f1210dd

.field public static final open_tickets_title:I = 0x7f1211b9

.field public static final open_tickets_title_with_number:I = 0x7f1211ba

.field public static final paper_signature_settle_failed_title:I = 0x7f12132e

.field public static final parenthesis:I = 0x7f121341

.field public static final partial_refund_help_text:I = 0x7f12135c

.field public static final partial_refund_help_url:I = 0x7f12135d

.field public static final payment_expiring:I = 0x7f1213bb

.field public static final payment_expiring_message:I = 0x7f1213bc

.field public static final payment_note_tip:I = 0x7f1213ca

.field public static final payment_note_tip_itemized:I = 0x7f1213cb

.field public static final please_remove_card:I = 0x7f121442

.field public static final print_gift_receipt:I = 0x7f1214a3

.field public static final printing_hud:I = 0x7f1214e7

.field public static final receipt_detail_card_declined_message:I = 0x7f1215a7

.field public static final receipt_detail_card_declined_message_learn_more:I = 0x7f1215a8

.field public static final receipt_detail_card_declined_message_partial:I = 0x7f1215a9

.field public static final receipt_detail_card_declined_title:I = 0x7f1215aa

.field public static final receipt_detail_id:I = 0x7f1215ab

.field public static final receipt_detail_invoice_number:I = 0x7f1215ac

.field public static final receipt_detail_items_dining_option:I = 0x7f1215ad

.field public static final receipt_detail_not_sent:I = 0x7f1215ae

.field public static final receipt_detail_payment_forwarded_message:I = 0x7f1215b1

.field public static final receipt_detail_payment_forwarded_title:I = 0x7f1215b2

.field public static final receipt_detail_payment_pending_message:I = 0x7f1215b3

.field public static final receipt_detail_payment_pending_title:I = 0x7f1215b4

.field public static final receipt_detail_refunded:I = 0x7f1215b5

.field public static final receipt_detail_rounding:I = 0x7f1215b6

.field public static final receipt_detail_settle_additional_tip_title:I = 0x7f1215b7

.field public static final receipt_detail_settle_tip_confirm:I = 0x7f1215b8

.field public static final receipt_detail_settle_tip_failed_body:I = 0x7f1215b9

.field public static final receipt_detail_settle_tip_popup_title:I = 0x7f1215ba

.field public static final receipt_detail_settle_tip_title:I = 0x7f1215bb

.field public static final receipt_detail_tax_breakdown_net_uppercase:I = 0x7f1215bc

.field public static final receipt_detail_tax_breakdown_tax_rate_uppercase:I = 0x7f1215bd

.field public static final receipt_detail_tax_breakdown_tax_uppercase:I = 0x7f1215be

.field public static final receipt_detail_tax_breakdown_total_uppercase:I = 0x7f1215bf

.field public static final receipt_detail_tax_multiple_included:I = 0x7f1215c0

.field public static final receipt_detail_total:I = 0x7f1215c1

.field public static final refund_card_presence_required_hint:I = 0x7f12162e

.field public static final refund_connect_reader_message:I = 0x7f121630

.field public static final refund_connect_reader_title:I = 0x7f121631

.field public static final refund_gift_card_line_item:I = 0x7f12163a

.field public static final refund_help_url:I = 0x7f12163f

.field public static final refund_issuing:I = 0x7f121642

.field public static final refund_past_deadline_message:I = 0x7f121644

.field public static final refund_past_deadline_message_plural:I = 0x7f121645

.field public static final refund_past_deadline_title:I = 0x7f121646

.field public static final refund_requested_toast:I = 0x7f12164d

.field public static final refund_server_4xx_message:I = 0x7f12164f

.field public static final refund_server_4xx_title:I = 0x7f121650

.field public static final refund_tenders_awaiting_tip_message:I = 0x7f121658

.field public static final refund_total_refundable_amount:I = 0x7f121659

.field public static final reprint_ticket:I = 0x7f121695

.field public static final restore_connectivity_to_refund:I = 0x7f1216a0

.field public static final restore_connectivity_to_settle_plural:I = 0x7f1216a1

.field public static final restore_connectivity_to_settle_plural_noperiod:I = 0x7f1216a2

.field public static final restore_connectivity_to_settle_single:I = 0x7f1216a3

.field public static final search_history_hint_deprecated:I = 0x7f12178a

.field public static final search_history_hint_rw:I = 0x7f12178b

.field public static final search_history_x2_hint:I = 0x7f12178c

.field public static final search_transactions_history_hint:I = 0x7f121791

.field public static final search_transactions_history_hint_offline:I = 0x7f121792

.field public static final tax_included:I = 0x7f12193e

.field public static final tax_name_default:I = 0x7f121949

.field public static final tip_percentage:I = 0x7f1219c0

.field public static final titlecase_activity:I = 0x7f1219d9

.field public static final transactions_history_activity:I = 0x7f1219f7

.field public static final uppercase_items_and_services:I = 0x7f121b47

.field public static final uppercase_offline_payments_history_header:I = 0x7f121b54

.field public static final uppercase_offline_payments_history_header_one:I = 0x7f121b55

.field public static final uppercase_receipt_detail_refund_giver:I = 0x7f121b6b

.field public static final uppercase_receipt_detail_refund_timestamp:I = 0x7f121b6c

.field public static final uppercase_receipt_detail_tender_payment_taker:I = 0x7f121b6d

.field public static final uppercase_receipt_detail_tender_payment_timestamp:I = 0x7f121b6e

.field public static final uppercase_receipt_detail_tender_payment_type:I = 0x7f121b6f

.field public static final uppercase_total:I = 0x7f121b8b

.field public static final void_reason_attributed:I = 0x7f121bb5

.field public static final void_reason_default:I = 0x7f121bb6

.field public static final void_transactions_history:I = 0x7f121bba


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
