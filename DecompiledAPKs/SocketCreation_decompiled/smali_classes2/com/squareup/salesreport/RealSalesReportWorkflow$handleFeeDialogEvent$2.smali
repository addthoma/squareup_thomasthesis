.class final Lcom/squareup/salesreport/RealSalesReportWorkflow$handleFeeDialogEvent$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSalesReportWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/RealSalesReportWorkflow;->handleFeeDialogEvent(Lcom/squareup/salesreport/FeeDetailDialogScreen$Event;Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/salesreport/SalesReportState;",
        "-",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/salesreport/SalesReportState;",
        "Lcom/squareup/salesreport/SalesReportOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;

.field final synthetic this$0:Lcom/squareup/salesreport/RealSalesReportWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/RealSalesReportWorkflow;Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$handleFeeDialogEvent$2;->this$0:Lcom/squareup/salesreport/RealSalesReportWorkflow;

    iput-object p2, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$handleFeeDialogEvent$2;->$state:Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 92
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/RealSalesReportWorkflow$handleFeeDialogEvent$2;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/salesreport/SalesReportState;",
            "-",
            "Lcom/squareup/salesreport/SalesReportOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$handleFeeDialogEvent$2;->this$0:Lcom/squareup/salesreport/RealSalesReportWorkflow;

    invoke-static {v0}, Lcom/squareup/salesreport/RealSalesReportWorkflow;->access$getSalesReportAnalytics$p(Lcom/squareup/salesreport/RealSalesReportWorkflow;)Lcom/squareup/salesreport/analytics/SalesReportAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/salesreport/analytics/SalesReportAnalytics;->logFeesLearnMore()V

    .line 222
    iget-object v0, p0, Lcom/squareup/salesreport/RealSalesReportWorkflow$handleFeeDialogEvent$2;->$state:Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;

    invoke-virtual {v0}, Lcom/squareup/salesreport/SalesReportState$ViewingFeesNote;->getPreviousState()Lcom/squareup/salesreport/SalesReportState$ViewingReport;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    .line 223
    sget-object v0, Lcom/squareup/salesreport/SalesReportOutput;->SHOW_FEE_TUTORIAL:Lcom/squareup/salesreport/SalesReportOutput;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    return-void
.end method
