.class final Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$$inlined$apply$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SalesReportCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/SalesReportCoordinator;->configureActionBar(Lcom/squareup/salesreport/SalesReportScreen;ZZLcom/squareup/salesreport/SalesReportState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke",
        "com/squareup/salesreport/SalesReportCoordinator$configureActionBar$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen$inlined:Lcom/squareup/salesreport/SalesReportScreen;

.field final synthetic $showBackArrow$inlined:Z

.field final synthetic $state$inlined:Lcom/squareup/salesreport/SalesReportState;

.field final synthetic this$0:Lcom/squareup/salesreport/SalesReportCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/salesreport/SalesReportCoordinator;Lcom/squareup/salesreport/SalesReportState;Lcom/squareup/salesreport/SalesReportScreen;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$$inlined$apply$lambda$1;->this$0:Lcom/squareup/salesreport/SalesReportCoordinator;

    iput-object p2, p0, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$$inlined$apply$lambda$1;->$state$inlined:Lcom/squareup/salesreport/SalesReportState;

    iput-object p3, p0, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$$inlined$apply$lambda$1;->$screen$inlined:Lcom/squareup/salesreport/SalesReportScreen;

    iput-boolean p4, p0, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$$inlined$apply$lambda$1;->$showBackArrow$inlined:Z

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 177
    invoke-virtual {p0}, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$$inlined$apply$lambda$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    .line 306
    iget-object v0, p0, Lcom/squareup/salesreport/SalesReportCoordinator$configureActionBar$$inlined$apply$lambda$1;->$screen$inlined:Lcom/squareup/salesreport/SalesReportScreen;

    invoke-virtual {v0}, Lcom/squareup/salesreport/SalesReportScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object v0

    sget-object v1, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ExportReport;->INSTANCE:Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ExportReport;

    invoke-interface {v0, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
