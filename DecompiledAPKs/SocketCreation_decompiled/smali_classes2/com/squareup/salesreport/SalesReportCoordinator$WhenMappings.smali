.class public final synthetic Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->values()[Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->SALES_OVERVIEW:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->SALES_DETAILS:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->values()[Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ViewCount;->VIEW_COUNT_FIVE:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ViewCount;->VIEW_COUNT_TEN:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ViewCount;->VIEW_COUNT_ALL:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;->values()[Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;->VISIBLE:Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;->SPACER:Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;->GONE:Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportCoordinator$ComparisonIconState;->ordinal()I

    move-result v1

    aput v4, v0, v1

    return-void
.end method
