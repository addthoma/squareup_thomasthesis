.class final Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$$inlined$forEachIndexed$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "SalesReportCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/salesreport/SalesReportCoordinator;->addItemRows(Lcom/squareup/salesreport/SalesReportScreen;Ljava/util/List;Lcom/squareup/customreport/data/SalesTopItemsReport;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;ZLcom/squareup/salesreport/SalesReportState$ViewCount;Ljava/util/Set;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/view/View;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Landroid/view/View;",
        "invoke",
        "com/squareup/salesreport/SalesReportCoordinator$addItemRows$4$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $grossCountSelection$inlined:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

.field final synthetic $index:I

.field final synthetic $isPhoneOrPortrait$inlined:Z

.field final synthetic $itemsWithVariationsShown$inlined:Ljava/util/Set;

.field final synthetic $rowList$inlined:Ljava/util/List;

.field final synthetic $screen$inlined:Lcom/squareup/salesreport/SalesReportScreen;

.field final synthetic this$0:Lcom/squareup/salesreport/SalesReportCoordinator;


# direct methods
.method constructor <init>(ILcom/squareup/salesreport/SalesReportCoordinator;Ljava/util/List;Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;ZLjava/util/Set;Lcom/squareup/salesreport/SalesReportScreen;)V
    .locals 0

    iput p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$$inlined$forEachIndexed$lambda$1;->$index:I

    iput-object p2, p0, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$$inlined$forEachIndexed$lambda$1;->this$0:Lcom/squareup/salesreport/SalesReportCoordinator;

    iput-object p3, p0, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$$inlined$forEachIndexed$lambda$1;->$rowList$inlined:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$$inlined$forEachIndexed$lambda$1;->$grossCountSelection$inlined:Lcom/squareup/salesreport/SalesReportState$GrossCountSelection;

    iput-boolean p5, p0, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$$inlined$forEachIndexed$lambda$1;->$isPhoneOrPortrait$inlined:Z

    iput-object p6, p0, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$$inlined$forEachIndexed$lambda$1;->$itemsWithVariationsShown$inlined:Ljava/util/Set;

    iput-object p7, p0, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$$inlined$forEachIndexed$lambda$1;->$screen$inlined:Lcom/squareup/salesreport/SalesReportScreen;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 177
    check-cast p1, Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$$inlined$forEachIndexed$lambda$1;->invoke(Landroid/view/View;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Landroid/view/View;)V
    .locals 2

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 650
    iget-object p1, p0, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$$inlined$forEachIndexed$lambda$1;->$screen$inlined:Lcom/squareup/salesreport/SalesReportScreen;

    invoke-virtual {p1}, Lcom/squareup/salesreport/SalesReportScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    new-instance v0, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingVariationsForItem;

    iget v1, p0, Lcom/squareup/salesreport/SalesReportCoordinator$addItemRows$$inlined$forEachIndexed$lambda$1;->$index:I

    invoke-direct {v0, v1}, Lcom/squareup/salesreport/SalesReportScreen$SalesReportEvent$ToggleShowingVariationsForItem;-><init>(I)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
