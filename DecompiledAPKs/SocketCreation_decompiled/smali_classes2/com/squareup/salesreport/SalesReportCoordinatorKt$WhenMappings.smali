.class public final synthetic Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I

.field public static final synthetic $EnumSwitchMapping$3:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->values()[Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->SALES_OVERVIEW:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->SALES_DETAILS:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->values()[Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->SALES_OVERVIEW:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->SALES_DETAILS:Lcom/squareup/salesreport/SalesReportState$TopSectionState;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$TopSectionState;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->values()[Lcom/squareup/salesreport/SalesReportState$ViewCount;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ViewCount;->VIEW_COUNT_FIVE:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ViewCount;->VIEW_COUNT_TEN:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/salesreport/SalesReportState$ViewCount;->VIEW_COUNT_ALL:Lcom/squareup/salesreport/SalesReportState$ViewCount;

    invoke-virtual {v1}, Lcom/squareup/salesreport/SalesReportState$ViewCount;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->values()[Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->CASH:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->CREDIT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->OTHER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->SPLIT_TENDER:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->THIRD_PARTY_CARD:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->SQUARE_GIFT_CARD_V2:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ZERO_AMOUNT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->EXTERNAL:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->INSTALLMENT:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/salesreport/SalesReportCoordinatorKt$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->EMONEY:Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;

    invoke-virtual {v1}, Lcom/squareup/protos/beemo/api/v3/reporting/GroupByValue$PaymentMethod;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1

    return-void
.end method
