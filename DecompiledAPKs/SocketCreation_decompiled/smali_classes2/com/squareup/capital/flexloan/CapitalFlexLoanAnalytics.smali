.class public interface abstract Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;
.super Ljava/lang/Object;
.source "CapitalFlexLoanAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\t\n\u0002\u0010\u000e\n\u0002\u0008\u000f\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0003H&J\u0008\u0010\u0006\u001a\u00020\u0003H&J\u0008\u0010\u0007\u001a\u00020\u0003H&J\u0008\u0010\u0008\u001a\u00020\u0003H&J\u0008\u0010\t\u001a\u00020\u0003H&J\u0008\u0010\n\u001a\u00020\u0003H&J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH&J\u0008\u0010\u000e\u001a\u00020\u0003H&J\u0008\u0010\u000f\u001a\u00020\u0003H&J\u0010\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\rH&J\u0010\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH&J\u0008\u0010\u0013\u001a\u00020\u0003H&J\u0010\u0010\u0014\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\rH&J\u0008\u0010\u0016\u001a\u00020\u0003H&J\u0010\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\rH&J\u0010\u0010\u0018\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\rH&J\u0008\u0010\u0019\u001a\u00020\u0003H&J\u0018\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\rH&J&\u0010\u001b\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0015\u001a\u00020\r2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0002\u0010\u0011\u001a\u00020\rH&\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/capital/flexloan/CapitalFlexLoanAnalytics;",
        "",
        "logClickCapitalActiveOfferRow",
        "",
        "logClickCapitalActivePlanRow",
        "logClickCapitalChooseOffer",
        "logClickCapitalClosedPlanRow",
        "logClickCapitalManagePlan",
        "logClickCapitalPastDuePlanRow",
        "logClickCapitalPendingApplicationRow",
        "logViewCapitalActiveOfferRow",
        "logViewCapitalActivePlan",
        "planId",
        "",
        "logViewCapitalActivePlanRow",
        "logViewCapitalBalanceAppletRow",
        "logViewCapitalBalanceAppletRowError",
        "errorCode",
        "logViewCapitalClosedPlan",
        "logViewCapitalClosedPlanRow",
        "logViewCapitalFlexOffer",
        "offerId",
        "logViewCapitalIneligibleRow",
        "logViewCapitalPastDuePlan",
        "logViewCapitalPendingApplication",
        "logViewCapitalPendingApplicationRow",
        "logViewCapitalPlanError",
        "logViewCapitalSessionBridgeError",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract logClickCapitalActiveOfferRow()V
.end method

.method public abstract logClickCapitalActivePlanRow()V
.end method

.method public abstract logClickCapitalChooseOffer()V
.end method

.method public abstract logClickCapitalClosedPlanRow()V
.end method

.method public abstract logClickCapitalManagePlan()V
.end method

.method public abstract logClickCapitalPastDuePlanRow()V
.end method

.method public abstract logClickCapitalPendingApplicationRow()V
.end method

.method public abstract logViewCapitalActiveOfferRow()V
.end method

.method public abstract logViewCapitalActivePlan(Ljava/lang/String;)V
.end method

.method public abstract logViewCapitalActivePlanRow()V
.end method

.method public abstract logViewCapitalBalanceAppletRow()V
.end method

.method public abstract logViewCapitalBalanceAppletRowError(Ljava/lang/String;)V
.end method

.method public abstract logViewCapitalClosedPlan(Ljava/lang/String;)V
.end method

.method public abstract logViewCapitalClosedPlanRow()V
.end method

.method public abstract logViewCapitalFlexOffer(Ljava/lang/String;)V
.end method

.method public abstract logViewCapitalIneligibleRow()V
.end method

.method public abstract logViewCapitalPastDuePlan(Ljava/lang/String;)V
.end method

.method public abstract logViewCapitalPendingApplication(Ljava/lang/String;)V
.end method

.method public abstract logViewCapitalPendingApplicationRow()V
.end method

.method public abstract logViewCapitalPlanError(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract logViewCapitalSessionBridgeError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method
