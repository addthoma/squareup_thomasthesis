.class final Lcom/squareup/cogs/CogsBuilder$CatalogVersionFile;
.super Ljava/lang/Object;
.source "CogsBuilder.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogFile;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cogs/CogsBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CatalogVersionFile"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogFile<",
        "Lcom/squareup/shared/catalog/StorageMetadata;",
        ">;"
    }
.end annotation


# instance fields
.field private final persistent:Lcom/squareup/persistent/PersistentFactory;


# direct methods
.method constructor <init>(Lcom/squareup/persistent/PersistentFactory;)V
    .locals 0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p1, p0, Lcom/squareup/cogs/CogsBuilder$CatalogVersionFile;->persistent:Lcom/squareup/persistent/PersistentFactory;

    return-void
.end method


# virtual methods
.method public readValue(Ljava/io/File;)Lcom/squareup/shared/catalog/StorageMetadata;
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/cogs/CogsBuilder$CatalogVersionFile;->persistent:Lcom/squareup/persistent/PersistentFactory;

    const-class v1, Lcom/squareup/shared/catalog/StorageMetadata;

    .line 118
    invoke-interface {v0, p1, v1}, Lcom/squareup/persistent/PersistentFactory;->getJsonFile(Ljava/io/File;Ljava/lang/reflect/Type;)Lcom/squareup/persistent/Persistent;

    move-result-object p1

    .line 119
    invoke-interface {p1}, Lcom/squareup/persistent/Persistent;->getSynchronous()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/StorageMetadata;

    return-object p1
.end method

.method public bridge synthetic readValue(Ljava/io/File;)Ljava/lang/Object;
    .locals 0

    .line 108
    invoke-virtual {p0, p1}, Lcom/squareup/cogs/CogsBuilder$CatalogVersionFile;->readValue(Ljava/io/File;)Lcom/squareup/shared/catalog/StorageMetadata;

    move-result-object p1

    return-object p1
.end method

.method public writeValue(Ljava/io/File;Lcom/squareup/shared/catalog/StorageMetadata;)V
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/cogs/CogsBuilder$CatalogVersionFile;->persistent:Lcom/squareup/persistent/PersistentFactory;

    const-class v1, Lcom/squareup/shared/catalog/StorageMetadata;

    .line 124
    invoke-interface {v0, p1, v1}, Lcom/squareup/persistent/PersistentFactory;->getJsonFile(Ljava/io/File;Ljava/lang/reflect/Type;)Lcom/squareup/persistent/Persistent;

    move-result-object p1

    .line 125
    invoke-interface {p1, p2}, Lcom/squareup/persistent/Persistent;->setSynchronous(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic writeValue(Ljava/io/File;Ljava/lang/Object;)V
    .locals 0

    .line 108
    check-cast p2, Lcom/squareup/shared/catalog/StorageMetadata;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cogs/CogsBuilder$CatalogVersionFile;->writeValue(Ljava/io/File;Lcom/squareup/shared/catalog/StorageMetadata;)V

    return-void
.end method
