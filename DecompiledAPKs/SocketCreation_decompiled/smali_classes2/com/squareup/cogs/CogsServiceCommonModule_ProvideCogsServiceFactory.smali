.class public final Lcom/squareup/cogs/CogsServiceCommonModule_ProvideCogsServiceFactory;
.super Ljava/lang/Object;
.source "CogsServiceCommonModule_ProvideCogsServiceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cogs/CogsService;",
        ">;"
    }
.end annotation


# instance fields
.field private final serviceCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/cogs/CogsServiceCommonModule_ProvideCogsServiceFactory;->serviceCreatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/cogs/CogsServiceCommonModule_ProvideCogsServiceFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ServiceCreator;",
            ">;)",
            "Lcom/squareup/cogs/CogsServiceCommonModule_ProvideCogsServiceFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/cogs/CogsServiceCommonModule_ProvideCogsServiceFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cogs/CogsServiceCommonModule_ProvideCogsServiceFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCogsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/cogs/CogsService;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/cogs/CogsServiceCommonModule;->provideCogsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/cogs/CogsService;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cogs/CogsService;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cogs/CogsService;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/cogs/CogsServiceCommonModule_ProvideCogsServiceFactory;->serviceCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ServiceCreator;

    invoke-static {v0}, Lcom/squareup/cogs/CogsServiceCommonModule_ProvideCogsServiceFactory;->provideCogsService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/cogs/CogsService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cogs/CogsServiceCommonModule_ProvideCogsServiceFactory;->get()Lcom/squareup/cogs/CogsService;

    move-result-object v0

    return-object v0
.end method
