.class public final Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;
.super Ljava/lang/Object;
.source "CatalogConnectV2EndpointImpl.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCatalogConnectV2EndpointImpl.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CatalogConnectV2EndpointImpl.kt\ncom/squareup/cogs/CatalogConnectV2EndpointImpl\n*L\n1#1,243:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0016\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00062\u0006\u0010\u0008\u001a\u00020\u000cH\u0016J\u0016\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u00062\u0006\u0010\u0008\u001a\u00020\u000fH\u0016J\u0016\u0010\u0010\u001a\u00020\u00112\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0013H\u0002J\u0016\u0010\u0014\u001a\u00020\u00112\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0013H\u0002J\u0016\u0010\u0015\u001a\u00020\u00112\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0013H\u0002J\u0016\u0010\u0016\u001a\u00020\u00112\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0013H\u0002J\u0016\u0010\u0018\u001a\u00020\u00112\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0013H\u0002J\u001c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0013H\u0002J\u001c\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00062\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0013H\u0002J\u001c\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u00062\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0013H\u0002J\u001c\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00062\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u0013H\u0002J\u001c\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u00062\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u0013H\u0002J\u0016\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020\u00170\u00062\u0006\u0010\u0008\u001a\u00020 H\u0016J\u0016\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u00062\u0006\u0010\u0008\u001a\u00020\"H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;",
        "Lcom/squareup/shared/catalog/connectv2/sync/CatalogConnectV2Endpoint;",
        "catalogConnectV2Service",
        "Lcom/squareup/server/catalog/CatalogConnectV2Service;",
        "(Lcom/squareup/server/catalog/CatalogConnectV2Service;)V",
        "batchRetrieveCatalogObjects",
        "Lcom/squareup/shared/catalog/sync/SyncResult;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;",
        "request",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;",
        "batchUpsert",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;",
        "delete",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;",
        "mapBatchRetrieveToSyncErrorType",
        "Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;",
        "received",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "mapBatchUpsertToSyncErrorType",
        "mapDeleteToSyncErrorType",
        "mapSearchToSyncErrorType",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;",
        "mapUpsertToSyncErrorType",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;",
        "onBatchRetrieveFailure",
        "onBatchUpsertFailure",
        "onDeleteFailure",
        "onSearchFailure",
        "onUpsertFailure",
        "searchCatalogObjects",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;",
        "upsert",
        "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;",
        "cogs_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final catalogConnectV2Service:Lcom/squareup/server/catalog/CatalogConnectV2Service;


# direct methods
.method public constructor <init>(Lcom/squareup/server/catalog/CatalogConnectV2Service;)V
    .locals 1

    const-string v0, "catalogConnectV2Service"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->catalogConnectV2Service:Lcom/squareup/server/catalog/CatalogConnectV2Service;

    return-void
.end method

.method private final mapBatchRetrieveToSyncErrorType(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;"
        }
    .end annotation

    .line 225
    move-object v0, p0

    check-cast v0, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;

    .line 227
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    .line 228
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_0

    .line 229
    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 231
    :cond_0
    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_UNKNOWN:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 234
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 235
    :cond_2
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SESSION_EXPIRED:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 236
    :cond_3
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_NETWORK:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 237
    :cond_4
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_5

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    :goto_0
    return-object p1

    .line 238
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown response type received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final mapBatchUpsertToSyncErrorType(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;"
        }
    .end annotation

    .line 184
    move-object v0, p0

    check-cast v0, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;

    .line 186
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    .line 187
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_0

    .line 188
    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 190
    :cond_0
    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_UNKNOWN:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 193
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 194
    :cond_2
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SESSION_EXPIRED:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 195
    :cond_3
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_NETWORK:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 196
    :cond_4
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_5

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    :goto_0
    return-object p1

    .line 197
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown response type received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final mapDeleteToSyncErrorType(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;"
        }
    .end annotation

    .line 204
    move-object v0, p0

    check-cast v0, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;

    .line 206
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    .line 207
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;->errors:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_0

    .line 208
    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 210
    :cond_0
    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_UNKNOWN:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 213
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 214
    :cond_2
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SESSION_EXPIRED:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 215
    :cond_3
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_NETWORK:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 216
    :cond_4
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_5

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    :goto_0
    return-object p1

    .line 217
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown response type received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final mapSearchToSyncErrorType(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;"
        }
    .end annotation

    .line 143
    move-object v0, p0

    check-cast v0, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;

    .line 145
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    .line 146
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;->errors:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_0

    .line 147
    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 149
    :cond_0
    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_UNKNOWN:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 152
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 153
    :cond_2
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SESSION_EXPIRED:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 154
    :cond_3
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_NETWORK:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 155
    :cond_4
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_5

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    :goto_0
    return-object p1

    .line 156
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown response type received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final mapUpsertToSyncErrorType(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;"
        }
    .end annotation

    .line 163
    move-object v0, p0

    check-cast v0, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;

    .line 165
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    .line 166
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;->errors:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_0

    .line 167
    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 169
    :cond_0
    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->CATALOG_UNKNOWN:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 172
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_CLIENT:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 173
    :cond_2
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SESSION_EXPIRED:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 174
    :cond_3
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_NETWORK:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    goto :goto_0

    .line 175
    :cond_4
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_5

    sget-object p1, Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;->HTTP_SERVER:Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    :goto_0
    return-object p1

    .line 176
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown response type received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final onBatchRetrieveFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;",
            ">;"
        }
    .end annotation

    .line 134
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError;

    .line 135
    invoke-direct {p0, p1}, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->mapBatchRetrieveToSyncErrorType(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    move-result-object v1

    .line 136
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error occurred while attempting to batch retrieve: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 134
    invoke-direct {v0, v1, p1}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    .line 138
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    const-string v0, "SyncResults.error(syncError)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onBatchUpsertFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;",
            ">;"
        }
    .end annotation

    .line 92
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError;

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->mapBatchUpsertToSyncErrorType(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    move-result-object v1

    .line 94
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error occurred while attempting to batch upsert: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 92
    invoke-direct {v0, v1, p1}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    .line 96
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    const-string v0, "SyncResults.error(syncError)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onDeleteFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;",
            ">;"
        }
    .end annotation

    .line 112
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError;

    .line 113
    invoke-direct {p0, p1}, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->mapDeleteToSyncErrorType(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    move-result-object v1

    .line 114
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error occurred while attempting to delete: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 112
    invoke-direct {v0, v1, p1}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    .line 116
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    const-string v0, "SyncResults.error(syncError)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onSearchFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;",
            ">;"
        }
    .end annotation

    .line 51
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError;

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->mapSearchToSyncErrorType(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    move-result-object v1

    .line 53
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error occurred while attempting to search: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 51
    invoke-direct {v0, v1, p1}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    .line 55
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    const-string v0, "SyncResults.error(syncError)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onUpsertFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;",
            ">;)",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;",
            ">;"
        }
    .end annotation

    .line 71
    new-instance v0, Lcom/squareup/shared/catalog/sync/SyncError;

    .line 72
    invoke-direct {p0, p1}, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->mapUpsertToSyncErrorType(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;

    move-result-object v1

    .line 73
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error occurred while attempting to upsert: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 71
    invoke-direct {v0, v1, p1}, Lcom/squareup/shared/catalog/sync/SyncError;-><init>(Lcom/squareup/shared/catalog/sync/SyncError$ErrorType;Ljava/lang/String;)V

    .line 75
    invoke-static {v0}, Lcom/squareup/shared/catalog/sync/SyncResults;->error(Lcom/squareup/shared/catalog/sync/SyncError;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    const-string v0, "SyncResults.error(syncError)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public batchRetrieveCatalogObjects(Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->catalogConnectV2Service:Lcom/squareup/server/catalog/CatalogConnectV2Service;

    invoke-interface {v0, p1}, Lcom/squareup/server/catalog/CatalogConnectV2Service;->batchRetrieve(Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchRetrieveCatalogObjectsRequest;)Lcom/squareup/server/catalog/CatalogConnectV2Service$BatchRetrieveCatalogObjectsStandardResponse;

    move-result-object p1

    .line 123
    invoke-virtual {p1}, Lcom/squareup/server/catalog/CatalogConnectV2Service$BatchRetrieveCatalogObjectsStandardResponse;->blockingSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    .line 126
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    const-string v0, "SyncResults.of(it.response)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 127
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->onBatchRetrieveFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public batchUpsert(Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->catalogConnectV2Service:Lcom/squareup/server/catalog/CatalogConnectV2Service;

    invoke-interface {v0, p1}, Lcom/squareup/server/catalog/CatalogConnectV2Service;->batchUpsert(Lcom/squareup/protos/connect/v2/merchant_catalog/service/BatchUpsertCatalogObjectsRequest;)Lcom/squareup/server/catalog/CatalogConnectV2Service$BatchUpsertCatalogObjectStandardResponse;

    move-result-object p1

    .line 81
    invoke-virtual {p1}, Lcom/squareup/server/catalog/CatalogConnectV2Service$BatchUpsertCatalogObjectStandardResponse;->blockingSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    .line 84
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    const-string v0, "SyncResults.of(it.response)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 85
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->onBatchUpsertFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public delete(Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->catalogConnectV2Service:Lcom/squareup/server/catalog/CatalogConnectV2Service;

    iget-object p1, p1, Lcom/squareup/protos/connect/v2/merchant_catalog/service/DeleteCatalogObjectRequest;->object_id:Ljava/lang/String;

    const-string v1, "request.object_id"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/catalog/CatalogConnectV2Service;->delete(Ljava/lang/String;)Lcom/squareup/server/catalog/CatalogConnectV2Service$DeleteCatalogObjectStandardResponse;

    move-result-object p1

    .line 101
    invoke-virtual {p1}, Lcom/squareup/server/catalog/CatalogConnectV2Service$DeleteCatalogObjectStandardResponse;->blockingSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    .line 104
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    const-string v0, "SyncResults.of(it.response)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->onDeleteFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public searchCatalogObjects(Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->catalogConnectV2Service:Lcom/squareup/server/catalog/CatalogConnectV2Service;

    invoke-interface {v0, p1}, Lcom/squareup/server/catalog/CatalogConnectV2Service;->search(Lcom/squareup/protos/connect/v2/merchant_catalog/service/SearchCatalogObjectsRequest;)Lcom/squareup/server/catalog/CatalogConnectV2Service$SearchCatalogObjectsStandardResponse;

    move-result-object p1

    .line 40
    invoke-virtual {p1}, Lcom/squareup/server/catalog/CatalogConnectV2Service$SearchCatalogObjectsStandardResponse;->blockingSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    .line 43
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    const-string v0, "SyncResults.of(it.response)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 44
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->onSearchFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public upsert(Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;)Lcom/squareup/shared/catalog/sync/SyncResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;",
            ")",
            "Lcom/squareup/shared/catalog/sync/SyncResult<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "request"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->catalogConnectV2Service:Lcom/squareup/server/catalog/CatalogConnectV2Service;

    invoke-interface {v0, p1}, Lcom/squareup/server/catalog/CatalogConnectV2Service;->upsert(Lcom/squareup/protos/connect/v2/merchant_catalog/service/UpsertCatalogObjectRequest;)Lcom/squareup/server/catalog/CatalogConnectV2Service$UpsertCatalogObjectStandardResponse;

    move-result-object p1

    .line 60
    invoke-virtual {p1}, Lcom/squareup/server/catalog/CatalogConnectV2Service$UpsertCatalogObjectStandardResponse;->blockingSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    .line 63
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/sync/SyncResults;->of(Ljava/lang/Object;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    const-string v0, "SyncResults.of(it.response)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 64
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cogs/CatalogConnectV2EndpointImpl;->onUpsertFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/shared/catalog/sync/SyncResult;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
