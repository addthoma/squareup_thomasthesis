.class Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;
.super Ljava/lang/Object;
.source "SqliteCatalogStoreFactory.java"

# interfaces
.implements Lcom/squareup/shared/sql/DatabaseHelper;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cogs/SqliteCatalogStoreFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DBHelper"
.end annotation


# instance fields
.field private final helper:Landroid/database/sqlite/SQLiteOpenHelper;

.field private readableDatabase:Lcom/squareup/shared/sql/SQLDatabase;

.field final synthetic this$0:Lcom/squareup/cogs/SqliteCatalogStoreFactory;

.field private writableDatabase:Lcom/squareup/shared/sql/SQLDatabase;


# direct methods
.method constructor <init>(Lcom/squareup/cogs/SqliteCatalogStoreFactory;Landroid/database/sqlite/SQLiteOpenHelper;)V
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->this$0:Lcom/squareup/cogs/SqliteCatalogStoreFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-object p2, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->helper:Landroid/database/sqlite/SQLiteOpenHelper;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->this$0:Lcom/squareup/cogs/SqliteCatalogStoreFactory;

    invoke-static {v0}, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->access$000(Lcom/squareup/cogs/SqliteCatalogStoreFactory;)V

    const/4 v0, 0x0

    .line 157
    iput-object v0, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->writableDatabase:Lcom/squareup/shared/sql/SQLDatabase;

    .line 158
    iput-object v0, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->readableDatabase:Lcom/squareup/shared/sql/SQLDatabase;

    .line 159
    iget-object v0, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->helper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->close()V

    return-void
.end method

.method public getReadableDatabase()Lcom/squareup/shared/sql/SQLDatabase;
    .locals 2

    .line 148
    iget-object v0, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->this$0:Lcom/squareup/cogs/SqliteCatalogStoreFactory;

    invoke-static {v0}, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->access$000(Lcom/squareup/cogs/SqliteCatalogStoreFactory;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->readableDatabase:Lcom/squareup/shared/sql/SQLDatabase;

    if-nez v0, :cond_0

    .line 150
    new-instance v0, Lcom/squareup/shared/catalog/android/RealSQLDatabase;

    iget-object v1, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->helper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/android/RealSQLDatabase;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    iput-object v0, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->readableDatabase:Lcom/squareup/shared/sql/SQLDatabase;

    .line 152
    :cond_0
    iget-object v0, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->readableDatabase:Lcom/squareup/shared/sql/SQLDatabase;

    return-object v0
.end method

.method public getWritableDatabase()Lcom/squareup/shared/sql/SQLDatabase;
    .locals 2

    .line 140
    iget-object v0, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->this$0:Lcom/squareup/cogs/SqliteCatalogStoreFactory;

    invoke-static {v0}, Lcom/squareup/cogs/SqliteCatalogStoreFactory;->access$000(Lcom/squareup/cogs/SqliteCatalogStoreFactory;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->writableDatabase:Lcom/squareup/shared/sql/SQLDatabase;

    if-nez v0, :cond_0

    .line 142
    new-instance v0, Lcom/squareup/shared/catalog/android/RealSQLDatabase;

    iget-object v1, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->helper:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/shared/catalog/android/RealSQLDatabase;-><init>(Landroid/database/sqlite/SQLiteDatabase;)V

    iput-object v0, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->writableDatabase:Lcom/squareup/shared/sql/SQLDatabase;

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/squareup/cogs/SqliteCatalogStoreFactory$DBHelper;->writableDatabase:Lcom/squareup/shared/sql/SQLDatabase;

    return-object v0
.end method
