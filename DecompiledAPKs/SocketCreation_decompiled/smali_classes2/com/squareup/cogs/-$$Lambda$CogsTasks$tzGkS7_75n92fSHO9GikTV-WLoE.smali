.class public final synthetic Lcom/squareup/cogs/-$$Lambda$CogsTasks$tzGkS7_75n92fSHO9GikTV-WLoE;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# instance fields
.field private final synthetic f$0:Lcom/squareup/cogs/Cogs;

.field private final synthetic f$1:Lcom/squareup/shared/catalog/utils/ElapsedTime;

.field private final synthetic f$2:J


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/utils/ElapsedTime;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$tzGkS7_75n92fSHO9GikTV-WLoE;->f$0:Lcom/squareup/cogs/Cogs;

    iput-object p2, p0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$tzGkS7_75n92fSHO9GikTV-WLoE;->f$1:Lcom/squareup/shared/catalog/utils/ElapsedTime;

    iput-wide p3, p0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$tzGkS7_75n92fSHO9GikTV-WLoE;->f$2:J

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$tzGkS7_75n92fSHO9GikTV-WLoE;->f$0:Lcom/squareup/cogs/Cogs;

    iget-object v1, p0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$tzGkS7_75n92fSHO9GikTV-WLoE;->f$1:Lcom/squareup/shared/catalog/utils/ElapsedTime;

    iget-wide v2, p0, Lcom/squareup/cogs/-$$Lambda$CogsTasks$tzGkS7_75n92fSHO9GikTV-WLoE;->f$2:J

    invoke-static {v0, v1, v2, v3, p1}, Lcom/squareup/cogs/CogsTasks;->lambda$foregroundSyncAsSingle$10(Lcom/squareup/cogs/Cogs;Lcom/squareup/shared/catalog/utils/ElapsedTime;JLio/reactivex/ObservableEmitter;)V

    return-void
.end method
