.class public interface abstract Lcom/squareup/bugreport/Mailer;
.super Ljava/lang/Object;
.source "Mailer.java"


# virtual methods
.method public abstract send(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/bugreport/Attachment;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/bugreport/SendResponse;",
            ">;"
        }
    .end annotation
.end method
