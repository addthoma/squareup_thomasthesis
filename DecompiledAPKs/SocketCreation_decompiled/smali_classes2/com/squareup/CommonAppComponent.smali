.class public interface abstract Lcom/squareup/CommonAppComponent;
.super Ljava/lang/Object;
.source "CommonAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/component/ComponentFactoryComponent;
.implements Lcom/squareup/cardreader/NonX2CardReaderContextParent;
.implements Lcom/squareup/queue/QueueRootModule$Component;
.implements Lcom/squareup/RegisterAppDelegateAppComponent;
.implements Lcom/squareup/RegisterAppDelegate$ParentAppComponent;
.implements Lcom/squareup/log/RegisterExceptionHandler$Component;
.implements Lcom/squareup/hardware/UsbAttachedActivity$Component;
.implements Lcom/squareup/hardware/UsbDetachedReceiver$Component;
.implements Lcom/squareup/ui/SquareActivity$ParentAppComponent;


# virtual methods
.method public abstract apiActivityComponent()Lcom/squareup/ui/ApiActivity$Component;
.end method

.method public abstract loggedInComponent()Lcom/squareup/CommonLoggedInComponent;
.end method
