.class public final Lcom/squareup/brandaudio/RealBrandAudioPlayer;
.super Ljava/lang/Object;
.source "RealBrandAudioPlayer.kt"

# interfaces
.implements Lcom/squareup/brandaudio/BrandAudioPlayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/brandaudio/RealBrandAudioPlayer$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBrandAudioPlayer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBrandAudioPlayer.kt\ncom/squareup/brandaudio/RealBrandAudioPlayer\n*L\n1#1,114:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u000b\u0018\u0000  2\u00020\u0001:\u0001 B3\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0018\u0010\u0017\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u0016H\u0002J\u0018\u0010\u001a\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u0016H\u0016J\u0010\u0010\u001c\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u0012H\u0016J\u0010\u0010\u001d\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00020\u0016H\u0002J\u0008\u0010\u001e\u001a\u00020\u0014H\u0016J\u0008\u0010\u001f\u001a\u00020\u0014H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/brandaudio/RealBrandAudioPlayer;",
        "Lcom/squareup/brandaudio/BrandAudioPlayer;",
        "context",
        "Landroid/app/Application;",
        "audioExecutor",
        "Lcom/squareup/thread/executor/SerialExecutor;",
        "threadEnforcer",
        "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
        "brandAudioSettingsController",
        "Lcom/squareup/brandaudio/BrandAudioSettingsController;",
        "mediaPlayerFactory",
        "Lcom/squareup/brandaudio/MediaPlayerFactory;",
        "(Landroid/app/Application;Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/brandaudio/BrandAudioSettingsController;Lcom/squareup/brandaudio/MediaPlayerFactory;)V",
        "loopRunnable",
        "Ljava/lang/Runnable;",
        "player",
        "Lcom/squareup/brandaudio/AudioPlayer;",
        "preparedAudioPath",
        "",
        "initialize",
        "",
        "turnUpAudioSettings",
        "",
        "playAudio",
        "filePath",
        "shouldLoop",
        "playBrandAudioMessage",
        "brandAudioFilePath",
        "prepareAudio",
        "setBrandLooping",
        "shutDown",
        "stopAudio",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final BRAND_LOOPING_DELAY_MS:J = 0x3e8L

.field public static final Companion:Lcom/squareup/brandaudio/RealBrandAudioPlayer$Companion;


# instance fields
.field private final audioExecutor:Lcom/squareup/thread/executor/SerialExecutor;

.field private final brandAudioSettingsController:Lcom/squareup/brandaudio/BrandAudioSettingsController;

.field private final context:Landroid/app/Application;

.field private final loopRunnable:Ljava/lang/Runnable;

.field private final mediaPlayerFactory:Lcom/squareup/brandaudio/MediaPlayerFactory;

.field private player:Lcom/squareup/brandaudio/AudioPlayer;

.field private preparedAudioPath:Ljava/lang/String;

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/brandaudio/RealBrandAudioPlayer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->Companion:Lcom/squareup/brandaudio/RealBrandAudioPlayer$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/brandaudio/BrandAudioSettingsController;Lcom/squareup/brandaudio/MediaPlayerFactory;)V
    .locals 1
    .param p2    # Lcom/squareup/thread/executor/SerialExecutor;
        .annotation runtime Lcom/squareup/util/AudioThread;
        .end annotation
    .end param
    .param p3    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/util/AudioThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "audioExecutor"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "threadEnforcer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "brandAudioSettingsController"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mediaPlayerFactory"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->context:Landroid/app/Application;

    iput-object p2, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->audioExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    iput-object p3, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iput-object p4, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->brandAudioSettingsController:Lcom/squareup/brandaudio/BrandAudioSettingsController;

    iput-object p5, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->mediaPlayerFactory:Lcom/squareup/brandaudio/MediaPlayerFactory;

    .line 26
    new-instance p1, Lcom/squareup/brandaudio/RealBrandAudioPlayer$loopRunnable$1;

    invoke-direct {p1, p0}, Lcom/squareup/brandaudio/RealBrandAudioPlayer$loopRunnable$1;-><init>(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)V

    check-cast p1, Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->loopRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method public static final synthetic access$getAudioExecutor$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)Lcom/squareup/thread/executor/SerialExecutor;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->audioExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    return-object p0
.end method

.method public static final synthetic access$getBrandAudioSettingsController$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)Lcom/squareup/brandaudio/BrandAudioSettingsController;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->brandAudioSettingsController:Lcom/squareup/brandaudio/BrandAudioSettingsController;

    return-object p0
.end method

.method public static final synthetic access$getContext$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)Landroid/app/Application;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->context:Landroid/app/Application;

    return-object p0
.end method

.method public static final synthetic access$getLoopRunnable$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)Ljava/lang/Runnable;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->loopRunnable:Ljava/lang/Runnable;

    return-object p0
.end method

.method public static final synthetic access$getMediaPlayerFactory$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)Lcom/squareup/brandaudio/MediaPlayerFactory;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->mediaPlayerFactory:Lcom/squareup/brandaudio/MediaPlayerFactory;

    return-object p0
.end method

.method public static final synthetic access$getPlayer$li(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)Lcom/squareup/brandaudio/AudioPlayer;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->player:Lcom/squareup/brandaudio/AudioPlayer;

    return-object p0
.end method

.method public static final synthetic access$getPlayer$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)Lcom/squareup/brandaudio/AudioPlayer;
    .locals 1

    .line 12
    iget-object p0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->player:Lcom/squareup/brandaudio/AudioPlayer;

    if-nez p0, :cond_0

    const-string v0, "player"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getPreparedAudioPath$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)Ljava/lang/String;
    .locals 0

    .line 12
    iget-object p0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->preparedAudioPath:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$playAudio(Lcom/squareup/brandaudio/RealBrandAudioPlayer;Ljava/lang/String;Z)V
    .locals 0

    .line 12
    invoke-direct {p0, p1, p2}, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->playAudio(Ljava/lang/String;Z)V

    return-void
.end method

.method public static final synthetic access$setPlayer$li(Lcom/squareup/brandaudio/RealBrandAudioPlayer;Lcom/squareup/brandaudio/AudioPlayer;)V
    .locals 0

    .line 12
    iput-object p1, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->player:Lcom/squareup/brandaudio/AudioPlayer;

    return-void
.end method

.method public static final synthetic access$setPlayer$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;Lcom/squareup/brandaudio/AudioPlayer;)V
    .locals 0

    .line 12
    iput-object p1, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->player:Lcom/squareup/brandaudio/AudioPlayer;

    return-void
.end method

.method public static final synthetic access$setPreparedAudioPath$p(Lcom/squareup/brandaudio/RealBrandAudioPlayer;Ljava/lang/String;)V
    .locals 0

    .line 12
    iput-object p1, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->preparedAudioPath:Ljava/lang/String;

    return-void
.end method

.method private final playAudio(Ljava/lang/String;Z)V
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 86
    :try_start_0
    iget-object v0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->player:Lcom/squareup/brandaudio/AudioPlayer;

    if-nez v0, :cond_0

    const-string v1, "player"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->preparedAudioPath:Ljava/lang/String;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->prepareAudio(Ljava/lang/String;)V

    .line 95
    :cond_1
    invoke-direct {p0, p2}, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->setBrandLooping(Z)V

    .line 96
    invoke-interface {v0}, Lcom/squareup/brandaudio/AudioPlayer;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p2

    .line 99
    check-cast p2, Ljava/lang/Throwable;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Failed to play brand audio for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method private final setBrandLooping(Z)V
    .locals 1

    const-string v0, "player"

    if-nez p1, :cond_1

    .line 105
    iget-object p1, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->player:Lcom/squareup/brandaudio/AudioPlayer;

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-interface {p1}, Lcom/squareup/brandaudio/AudioPlayer;->clearOnCompletionListener()V

    .line 106
    iget-object p1, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->audioExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    iget-object v0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->loopRunnable:Ljava/lang/Runnable;

    invoke-interface {p1, v0}, Lcom/squareup/thread/executor/SerialExecutor;->cancel(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 108
    :cond_1
    iget-object p1, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->player:Lcom/squareup/brandaudio/AudioPlayer;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v0, Lcom/squareup/brandaudio/RealBrandAudioPlayer$setBrandLooping$1;

    invoke-direct {v0, p0}, Lcom/squareup/brandaudio/RealBrandAudioPlayer$setBrandLooping$1;-><init>(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-interface {p1, v0}, Lcom/squareup/brandaudio/AudioPlayer;->setOnCompletionListener(Lkotlin/jvm/functions/Function0;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public initialize(Z)V
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->audioExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    new-instance v1, Lcom/squareup/brandaudio/RealBrandAudioPlayer$initialize$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/brandaudio/RealBrandAudioPlayer$initialize$1;-><init>(Lcom/squareup/brandaudio/RealBrandAudioPlayer;Z)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/SerialExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public playBrandAudioMessage(Ljava/lang/String;Z)V
    .locals 2

    const-string v0, "brandAudioFilePath"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->audioExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    new-instance v1, Lcom/squareup/brandaudio/RealBrandAudioPlayer$playBrandAudioMessage$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/brandaudio/RealBrandAudioPlayer$playBrandAudioMessage$1;-><init>(Lcom/squareup/brandaudio/RealBrandAudioPlayer;Ljava/lang/String;Z)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/SerialExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public prepareAudio(Ljava/lang/String;)V
    .locals 2

    const-string v0, "filePath"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->audioExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    new-instance v1, Lcom/squareup/brandaudio/RealBrandAudioPlayer$prepareAudio$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/brandaudio/RealBrandAudioPlayer$prepareAudio$1;-><init>(Lcom/squareup/brandaudio/RealBrandAudioPlayer;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/SerialExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public shutDown()V
    .locals 2

    .line 50
    iget-object v0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->audioExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    new-instance v1, Lcom/squareup/brandaudio/RealBrandAudioPlayer$shutDown$1;

    invoke-direct {v1, p0}, Lcom/squareup/brandaudio/RealBrandAudioPlayer$shutDown$1;-><init>(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/SerialExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public stopAudio()V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/brandaudio/RealBrandAudioPlayer;->audioExecutor:Lcom/squareup/thread/executor/SerialExecutor;

    new-instance v1, Lcom/squareup/brandaudio/RealBrandAudioPlayer$stopAudio$1;

    invoke-direct {v1, p0}, Lcom/squareup/brandaudio/RealBrandAudioPlayer$stopAudio$1;-><init>(Lcom/squareup/brandaudio/RealBrandAudioPlayer;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/SerialExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
