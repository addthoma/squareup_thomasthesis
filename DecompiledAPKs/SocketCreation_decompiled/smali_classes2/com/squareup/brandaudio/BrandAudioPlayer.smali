.class public interface abstract Lcom/squareup/brandaudio/BrandAudioPlayer;
.super Ljava/lang/Object;
.source "BrandAudioPlayer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/brandaudio/BrandAudioPlayer$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u001a\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u0005H&J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u0008H&J\u0008\u0010\u000c\u001a\u00020\u0003H&J\u0008\u0010\r\u001a\u00020\u0003H&\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/brandaudio/BrandAudioPlayer;",
        "",
        "initialize",
        "",
        "turnUpAudioSettings",
        "",
        "playBrandAudioMessage",
        "brandAudioFilePath",
        "",
        "shouldLoop",
        "prepareAudio",
        "filePath",
        "shutDown",
        "stopAudio",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract initialize(Z)V
.end method

.method public abstract playBrandAudioMessage(Ljava/lang/String;Z)V
.end method

.method public abstract prepareAudio(Ljava/lang/String;)V
.end method

.method public abstract shutDown()V
.end method

.method public abstract stopAudio()V
.end method
