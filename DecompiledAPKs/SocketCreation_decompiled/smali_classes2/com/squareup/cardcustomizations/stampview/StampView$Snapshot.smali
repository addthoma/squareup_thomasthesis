.class public final Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;
.super Ljava/lang/Object;
.source "StampView.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardcustomizations/stampview/StampView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Snapshot"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot$Creator;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStampView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StampView.kt\ncom/squareup/cardcustomizations/stampview/StampView$Snapshot\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,505:1\n1574#2,2:506\n1574#2,2:508\n461#2,6:510\n*E\n*S KotlinDebug\n*F\n+ 1 StampView.kt\ncom/squareup/cardcustomizations/stampview/StampView$Snapshot\n*L\n479#1,2:506\n486#1,2:508\n493#1,6:510\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0087\u0008\u0018\u00002\u00020\u0001B\u0015\u0012\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tJ\u000f\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\u000b\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0010\u0010\u000c\u001a\u00020\u00002\u0006\u0010\r\u001a\u00020\u0004H\u0007J\u0010\u0010\u000e\u001a\u00020\u00002\u0006\u0010\r\u001a\u00020\u0004H\u0007J\t\u0010\u000f\u001a\u00020\u0010H\u00d6\u0001J\u0016\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016J\u0013\u0010\u0017\u001a\u00020\u00182\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u0010H\u00d6\u0001J\u0010\u0010\u001c\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u001d\u001a\u00020\u001eJ\t\u0010\u001f\u001a\u00020 H\u00d6\u0001J\u0019\u0010!\u001a\u00020\u00122\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u0010H\u00d6\u0001R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;",
        "Landroid/os/Parcelable;",
        "stamps",
        "",
        "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
        "(Ljava/util/List;)V",
        "getStamps",
        "()Ljava/util/List;",
        "bounds",
        "Landroid/graphics/RectF;",
        "component1",
        "copy",
        "copyWith",
        "stamp",
        "copyWithout",
        "describeContents",
        "",
        "draw",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "stampPaint",
        "Landroid/graphics/Paint;",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "stampAtOrNull",
        "pointToCheck",
        "Landroid/graphics/PointF;",
        "toString",
        "",
        "writeToParcel",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "card-customization_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final stamps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot$Creator;

    invoke-direct {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot$Creator;-><init>()V

    sput-object v0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;-><init>(Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;)V"
        }
    .end annotation

    const-string v0, "stamps"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stamps:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 465
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/List;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;-><init>(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;Ljava/util/List;ILjava/lang/Object;)Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stamps:Ljava/util/List;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->copy(Ljava/util/List;)Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final bounds()Landroid/graphics/RectF;
    .locals 4

    .line 485
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 486
    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stamps:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 508
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    .line 487
    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v3

    invoke-virtual {v2}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->bounds()Landroid/graphics/RectF;

    move-result-object v2

    if-eqz v3, :cond_0

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    goto :goto_0

    .line 489
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :cond_2
    return-object v0
.end method

.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stamps:Ljava/util/List;

    return-object v0
.end method

.method public final copy(Ljava/util/List;)Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;)",
            "Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;"
        }
    .end annotation

    const-string v0, "stamps"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    invoke-direct {v0, p1}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public final copyWith(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;)Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;
    .locals 1

    const-string v0, "stamp"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 470
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stamps:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->copy(Ljava/util/List;)Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public final copyWithout(Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;)Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;
    .locals 1

    const-string v0, "stamp"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 475
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stamps:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->minus(Ljava/lang/Iterable;Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->copy(Ljava/util/List;)Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 2

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "stampPaint"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 479
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stamps:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 506
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    .line 480
    invoke-virtual {v1, p1, p2}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stamps:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stamps:Ljava/util/List;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getStamps()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;",
            ">;"
        }
    .end annotation

    .line 465
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stamps:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stamps:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final stampAtOrNull(Landroid/graphics/PointF;)Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;
    .locals 5

    const-string v0, "pointToCheck"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 493
    iget-object v0, p0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stamps:Ljava/util/List;

    .line 510
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v0

    .line 511
    :cond_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 512
    invoke-interface {v0}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v1

    .line 513
    move-object v2, v1

    check-cast v2, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    .line 494
    invoke-virtual {v2}, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;->bounds()Landroid/graphics/RectF;

    move-result-object v2

    iget v3, p1, Landroid/graphics/PointF;->x:F

    iget v4, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 515
    :goto_0
    check-cast v1, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Snapshot(stamps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stamps:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p0, Lcom/squareup/cardcustomizations/stampview/StampView$Snapshot;->stamps:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardcustomizations/stampview/StampView$TransformedStamp;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/os/Parcelable;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_0
    return-void
.end method
