.class public final Lcom/squareup/cardcustomizations/stampview/StampView$StampAdapter;
.super Ljava/lang/Object;
.source "StampView.kt"

# interfaces
.implements Lkotlinx/android/parcel/Parceler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardcustomizations/stampview/StampView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StampAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlinx/android/parcel/Parceler<",
        "Lcom/squareup/cardcustomizations/stampview/Stamp;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\u0008\u00c6\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u001c\u0010\u0007\u001a\u00020\u0008*\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\nH\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/cardcustomizations/stampview/StampView$StampAdapter;",
        "Lkotlinx/android/parcel/Parceler;",
        "Lcom/squareup/cardcustomizations/stampview/Stamp;",
        "()V",
        "create",
        "parcel",
        "Landroid/os/Parcel;",
        "write",
        "",
        "flags",
        "",
        "card-customization_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0xd
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/cardcustomizations/stampview/StampView$StampAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 408
    new-instance v0, Lcom/squareup/cardcustomizations/stampview/StampView$StampAdapter;

    invoke-direct {v0}, Lcom/squareup/cardcustomizations/stampview/StampView$StampAdapter;-><init>()V

    sput-object v0, Lcom/squareup/cardcustomizations/stampview/StampView$StampAdapter;->INSTANCE:Lcom/squareup/cardcustomizations/stampview/StampView$StampAdapter;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/os/Parcel;)Lcom/squareup/cardcustomizations/stampview/Stamp;
    .locals 3

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 410
    new-instance v0, Lcom/squareup/cardcustomizations/stampview/Stamp;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "parcel.readString()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Lcom/squareup/cardcustomizations/stampview/Stamp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic create(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 408
    invoke-virtual {p0, p1}, Lcom/squareup/cardcustomizations/stampview/StampView$StampAdapter;->create(Landroid/os/Parcel;)Lcom/squareup/cardcustomizations/stampview/Stamp;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/cardcustomizations/stampview/Stamp;
    .locals 0

    .line 408
    invoke-static {p0, p1}, Lkotlinx/android/parcel/Parceler$DefaultImpls;->newArray(Lkotlinx/android/parcel/Parceler;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/squareup/cardcustomizations/stampview/Stamp;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 408
    invoke-virtual {p0, p1}, Lcom/squareup/cardcustomizations/stampview/StampView$StampAdapter;->newArray(I)[Lcom/squareup/cardcustomizations/stampview/Stamp;

    move-result-object p1

    return-object p1
.end method

.method public write(Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/os/Parcel;I)V
    .locals 0

    const-string p3, "receiver$0"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p3, "parcel"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 414
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/Stamp;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 415
    invoke-virtual {p1}, Lcom/squareup/cardcustomizations/stampview/Stamp;->getSvgString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic write(Ljava/lang/Object;Landroid/os/Parcel;I)V
    .locals 0

    .line 408
    check-cast p1, Lcom/squareup/cardcustomizations/stampview/Stamp;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/cardcustomizations/stampview/StampView$StampAdapter;->write(Lcom/squareup/cardcustomizations/stampview/Stamp;Landroid/os/Parcel;I)V

    return-void
.end method
