.class final Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/PaymentActivity$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "PA_ComponentImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/DaggerSposReleaseAppComponent;


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent;)V
    .locals 0

    .line 8342
    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 8341
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent;)V

    return-void
.end method

.method private getMediaButtonDisabler()Lcom/squareup/ui/MediaButtonDisabler;
    .locals 1

    .line 8347
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$600(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/AppBootstrapModule;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/MediaButtonDisabler_Factory;->newInstance(Landroid/app/Application;)Lcom/squareup/ui/MediaButtonDisabler;

    move-result-object v0

    return-object v0
.end method

.method private injectPaymentActivity(Lcom/squareup/ui/PaymentActivity;)Lcom/squareup/ui/PaymentActivity;
    .locals 1

    .line 8354
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectAnalytics(Lcom/squareup/ui/SquareActivity;Lcom/squareup/analytics/Analytics;)V

    .line 8355
    new-instance v0, Lcom/squareup/development/drawer/ReleaseContentViewInitializer;

    invoke-direct {v0}, Lcom/squareup/development/drawer/ReleaseContentViewInitializer;-><init>()V

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectContentViewInitializer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/development/drawer/ContentViewInitializer;)V

    .line 8356
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectLocationMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;)V

    .line 8357
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/radiography/FocusedActivityScanner;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFocusedActivityScanner(Lcom/squareup/ui/SquareActivity;Lcom/squareup/radiography/FocusedActivityScanner;)V

    .line 8358
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectCardReaderPauseAndResumer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/cardreader/CardReaderPauseAndResumer;)V

    .line 8359
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->getMediaButtonDisabler()Lcom/squareup/ui/MediaButtonDisabler;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMediaButtonDisabler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/MediaButtonDisabler;)V

    .line 8360
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMinesweeperProvider(Lcom/squareup/ui/SquareActivity;Ljavax/inject/Provider;)V

    .line 8361
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/internet/InternetStatusMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectInternetStatusMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/internet/InternetStatusMonitor;)V

    .line 8362
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFeatures(Lcom/squareup/ui/SquareActivity;Lcom/squareup/settings/server/Features;)V

    .line 8363
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/persistentbundle/PersistentBundleManager;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectPersistentBundleManager(Lcom/squareup/ui/SquareActivity;Lcom/squareup/persistentbundle/PersistentBundleManager;)V

    .line 8364
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectDevice(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/Device;)V

    .line 8365
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ActivityResultHandler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectActivityResultHandler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/ActivityResultHandler;)V

    .line 8366
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AndroidConfigurationChangeMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectConfigurationChangeMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/AndroidConfigurationChangeMonitor;)V

    .line 8367
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/LegacyAuthenticator;

    invoke-static {p1, v0}, Lcom/squareup/ui/PaymentActivity_MembersInjector;->injectAuthenticator(Lcom/squareup/ui/PaymentActivity;Lcom/squareup/account/LegacyAuthenticator;)V

    .line 8368
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3300(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/loggedout/LoggedOutFeatureStarter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/PaymentActivity_MembersInjector;->injectLoggedOutStarter(Lcom/squareup/ui/PaymentActivity;Lcom/squareup/loggedout/LoggedOutStarter;)V

    return-object p1
.end method


# virtual methods
.method public inject(Lcom/squareup/ui/PaymentActivity;)V
    .locals 0

    .line 8351
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$PA_ComponentImpl;->injectPaymentActivity(Lcom/squareup/ui/PaymentActivity;)Lcom/squareup/ui/PaymentActivity;

    return-void
.end method
