.class public final Lcom/squareup/checkoutfe/CheckoutRequest;
.super Lcom/squareup/wire/Message;
.source "CheckoutRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutfe/CheckoutRequest$ProtoAdapter_CheckoutRequest;,
        Lcom/squareup/checkoutfe/CheckoutRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/checkoutfe/CheckoutRequest;",
        "Lcom/squareup/checkoutfe/CheckoutRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/checkoutfe/CheckoutRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final create_order_request:Lcom/squareup/orders/CreateOrderRequest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.CreateOrderRequest#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final create_payment_request:Lcom/squareup/protos/connect/v2/CreatePaymentRequest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.CreatePaymentRequest#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 33
    new-instance v0, Lcom/squareup/checkoutfe/CheckoutRequest$ProtoAdapter_CheckoutRequest;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CheckoutRequest$ProtoAdapter_CheckoutRequest;-><init>()V

    sput-object v0, Lcom/squareup/checkoutfe/CheckoutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/CreatePaymentRequest;Lcom/squareup/orders/CreateOrderRequest;)V
    .locals 1

    .line 51
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/checkoutfe/CheckoutRequest;-><init>(Lcom/squareup/protos/connect/v2/CreatePaymentRequest;Lcom/squareup/orders/CreateOrderRequest;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/CreatePaymentRequest;Lcom/squareup/orders/CreateOrderRequest;Lokio/ByteString;)V
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/checkoutfe/CheckoutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 57
    iput-object p1, p0, Lcom/squareup/checkoutfe/CheckoutRequest;->create_payment_request:Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    .line 58
    iput-object p2, p0, Lcom/squareup/checkoutfe/CheckoutRequest;->create_order_request:Lcom/squareup/orders/CreateOrderRequest;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 73
    :cond_0
    instance-of v1, p1, Lcom/squareup/checkoutfe/CheckoutRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 74
    :cond_1
    check-cast p1, Lcom/squareup/checkoutfe/CheckoutRequest;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutRequest;->create_payment_request:Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    iget-object v3, p1, Lcom/squareup/checkoutfe/CheckoutRequest;->create_payment_request:Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    .line 76
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutRequest;->create_order_request:Lcom/squareup/orders/CreateOrderRequest;

    iget-object p1, p1, Lcom/squareup/checkoutfe/CheckoutRequest;->create_order_request:Lcom/squareup/orders/CreateOrderRequest;

    .line 77
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 82
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 84
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutRequest;->create_payment_request:Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/CreatePaymentRequest;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutRequest;->create_order_request:Lcom/squareup/orders/CreateOrderRequest;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/orders/CreateOrderRequest;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 87
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/checkoutfe/CheckoutRequest$Builder;
    .locals 2

    .line 63
    new-instance v0, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;-><init>()V

    .line 64
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutRequest;->create_payment_request:Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    iput-object v1, v0, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;->create_payment_request:Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    .line 65
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutRequest;->create_order_request:Lcom/squareup/orders/CreateOrderRequest;

    iput-object v1, v0, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;->create_order_request:Lcom/squareup/orders/CreateOrderRequest;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkoutfe/CheckoutRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 32
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CheckoutRequest;->newBuilder()Lcom/squareup/checkoutfe/CheckoutRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutRequest;->create_payment_request:Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    if-eqz v1, :cond_0

    const-string v1, ", create_payment_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutRequest;->create_payment_request:Lcom/squareup/protos/connect/v2/CreatePaymentRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 96
    :cond_0
    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutRequest;->create_order_request:Lcom/squareup/orders/CreateOrderRequest;

    if-eqz v1, :cond_1

    const-string v1, ", create_order_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CheckoutRequest;->create_order_request:Lcom/squareup/orders/CreateOrderRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CheckoutRequest{"

    .line 97
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
