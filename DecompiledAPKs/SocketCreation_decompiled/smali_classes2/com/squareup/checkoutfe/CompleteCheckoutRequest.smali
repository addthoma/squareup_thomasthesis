.class public final Lcom/squareup/checkoutfe/CompleteCheckoutRequest;
.super Lcom/squareup/wire/Message;
.source "CompleteCheckoutRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutfe/CompleteCheckoutRequest$ProtoAdapter_CompleteCheckoutRequest;,
        Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;,
        Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/checkoutfe/CompleteCheckoutRequest;",
        "Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/checkoutfe/CompleteCheckoutRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_IDEMPOTENCY_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_ORDER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_ORDER_VERSION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final idempotency_key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final order_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final order_version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field

.field public final payments:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.checkoutfe.CompleteCheckoutRequest$CompletePayment#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$ProtoAdapter_CompleteCheckoutRequest;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$ProtoAdapter_CompleteCheckoutRequest;-><init>()V

    sput-object v0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 31
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->DEFAULT_ORDER_VERSION:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;",
            ">;)V"
        }
    .end annotation

    .line 60
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/util/List<",
            "Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 65
    sget-object v0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 66
    iput-object p1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->idempotency_key:Ljava/lang/String;

    .line 67
    iput-object p2, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_id:Ljava/lang/String;

    .line 68
    iput-object p3, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_version:Ljava/lang/Integer;

    const-string p1, "payments"

    .line 69
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->payments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 86
    :cond_0
    instance-of v1, p1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 87
    :cond_1
    check-cast p1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;

    .line 88
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->idempotency_key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->idempotency_key:Ljava/lang/String;

    .line 89
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_id:Ljava/lang/String;

    .line 90
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_version:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_version:Ljava/lang/Integer;

    .line 91
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->payments:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->payments:Ljava/util/List;

    .line 92
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 97
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 99
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 100
    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->idempotency_key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 101
    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 102
    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_version:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 103
    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->payments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 104
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;
    .locals 2

    .line 74
    new-instance v0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;-><init>()V

    .line 75
    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->idempotency_key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->idempotency_key:Ljava/lang/String;

    .line 76
    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->order_id:Ljava/lang/String;

    .line 77
    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_version:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->order_version:Ljava/lang/Integer;

    .line 78
    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->payments:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->payments:Ljava/util/List;

    .line 79
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->newBuilder()Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->idempotency_key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", idempotency_key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->idempotency_key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", order_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_version:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", order_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 115
    :cond_2
    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->payments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", payments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->payments:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CompleteCheckoutRequest{"

    .line 116
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
