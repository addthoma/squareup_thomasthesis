.class final Lcom/squareup/checkoutfe/CompleteCheckoutRequest$ProtoAdapter_CompleteCheckoutRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CompleteCheckoutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/CompleteCheckoutRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CompleteCheckoutRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/checkoutfe/CompleteCheckoutRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 331
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/checkoutfe/CompleteCheckoutRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 354
    new-instance v0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;-><init>()V

    .line 355
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 356
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 363
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 361
    :cond_0
    iget-object v3, v0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->payments:Ljava/util/List;

    sget-object v4, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 360
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->order_version(Ljava/lang/Integer;)Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;

    goto :goto_0

    .line 359
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->order_id(Ljava/lang/String;)Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;

    goto :goto_0

    .line 358
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->idempotency_key(Ljava/lang/String;)Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;

    goto :goto_0

    .line 367
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 368
    invoke-virtual {v0}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->build()Lcom/squareup/checkoutfe/CompleteCheckoutRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 329
    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$ProtoAdapter_CompleteCheckoutRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/checkoutfe/CompleteCheckoutRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/checkoutfe/CompleteCheckoutRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 345
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->idempotency_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 346
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 347
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_version:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 348
    sget-object v0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->payments:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 349
    invoke-virtual {p2}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 329
    check-cast p2, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$ProtoAdapter_CompleteCheckoutRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/checkoutfe/CompleteCheckoutRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/checkoutfe/CompleteCheckoutRequest;)I
    .locals 4

    .line 336
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->idempotency_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 337
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->order_version:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 338
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 339
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->payments:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 340
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 329
    check-cast p1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$ProtoAdapter_CompleteCheckoutRequest;->encodedSize(Lcom/squareup/checkoutfe/CompleteCheckoutRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/checkoutfe/CompleteCheckoutRequest;)Lcom/squareup/checkoutfe/CompleteCheckoutRequest;
    .locals 2

    .line 373
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;->newBuilder()Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;

    move-result-object p1

    .line 374
    iget-object v0, p1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->payments:Ljava/util/List;

    sget-object v1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 375
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 376
    invoke-virtual {p1}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$Builder;->build()Lcom/squareup/checkoutfe/CompleteCheckoutRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 329
    check-cast p1, Lcom/squareup/checkoutfe/CompleteCheckoutRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$ProtoAdapter_CompleteCheckoutRequest;->redact(Lcom/squareup/checkoutfe/CompleteCheckoutRequest;)Lcom/squareup/checkoutfe/CompleteCheckoutRequest;

    move-result-object p1

    return-object p1
.end method
