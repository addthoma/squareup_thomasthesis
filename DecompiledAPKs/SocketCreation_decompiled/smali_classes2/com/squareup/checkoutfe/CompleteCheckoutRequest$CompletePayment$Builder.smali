.class public final Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CompleteCheckoutRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;",
        "Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public encrypted_emv_data:Ljava/lang/String;

.field public payment_id:Ljava/lang/String;

.field public update_payment:Lcom/squareup/protos/connect/v2/Payment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 252
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;
    .locals 5

    .line 275
    new-instance v0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;

    iget-object v1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment$Builder;->payment_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment$Builder;->encrypted_emv_data:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment$Builder;->update_payment:Lcom/squareup/protos/connect/v2/Payment;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/Payment;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 245
    invoke-virtual {p0}, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment$Builder;->build()Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment;

    move-result-object v0

    return-object v0
.end method

.method public encrypted_emv_data(Ljava/lang/String;)Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment$Builder;
    .locals 0

    .line 261
    iput-object p1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment$Builder;->encrypted_emv_data:Ljava/lang/String;

    return-object p0
.end method

.method public payment_id(Ljava/lang/String;)Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment$Builder;
    .locals 0

    .line 256
    iput-object p1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment$Builder;->payment_id:Ljava/lang/String;

    return-object p0
.end method

.method public update_payment(Lcom/squareup/protos/connect/v2/Payment;)Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment$Builder;
    .locals 0

    .line 269
    iput-object p1, p0, Lcom/squareup/checkoutfe/CompleteCheckoutRequest$CompletePayment$Builder;->update_payment:Lcom/squareup/protos/connect/v2/Payment;

    return-object p0
.end method
