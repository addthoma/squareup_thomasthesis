.class public final Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasTakenItemizedPaymentBeforeAppsFlyerFactory;
.super Ljava/lang/Object;
.source "AdAnalyticsModule_ProvideHasTakenItemizedPaymentBeforeAppsFlyerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/f2prateek/rx/preferences2/Preference<",
        "Ljava/lang/Boolean;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final rxPrefsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasTakenItemizedPaymentBeforeAppsFlyerFactory;->rxPrefsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasTakenItemizedPaymentBeforeAppsFlyerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ">;)",
            "Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasTakenItemizedPaymentBeforeAppsFlyerFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasTakenItemizedPaymentBeforeAppsFlyerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasTakenItemizedPaymentBeforeAppsFlyerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideHasTakenItemizedPaymentBeforeAppsFlyer(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 38
    invoke-static {p0}, Lcom/squareup/adanalytics/AdAnalyticsModule;->provideHasTakenItemizedPaymentBeforeAppsFlyer(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasTakenItemizedPaymentBeforeAppsFlyerFactory;->rxPrefsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/f2prateek/rx/preferences2/RxSharedPreferences;

    invoke-static {v0}, Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasTakenItemizedPaymentBeforeAppsFlyerFactory;->provideHasTakenItemizedPaymentBeforeAppsFlyer(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/adanalytics/AdAnalyticsModule_ProvideHasTakenItemizedPaymentBeforeAppsFlyerFactory;->get()Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v0

    return-object v0
.end method
