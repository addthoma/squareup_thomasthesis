.class public abstract Lcom/squareup/adanalytics/NoAdAnalyticsModule;
.super Ljava/lang/Object;
.source "NoAdAnalyticsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideAdAnalytics(Lcom/squareup/adanalytics/AdAnalytics$NoAdAnalytics;)Lcom/squareup/adanalytics/AdAnalytics;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
