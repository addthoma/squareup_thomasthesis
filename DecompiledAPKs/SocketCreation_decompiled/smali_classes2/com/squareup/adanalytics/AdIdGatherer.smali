.class public Lcom/squareup/adanalytics/AdIdGatherer;
.super Ljava/lang/Object;
.source "AdIdGatherer.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final context:Landroid/content/Context;

.field private final executor:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/analytics/Analytics;Ljava/util/concurrent/Executor;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/adanalytics/AdIdGatherer;->context:Landroid/content/Context;

    .line 25
    iput-object p2, p0, Lcom/squareup/adanalytics/AdIdGatherer;->analytics:Lcom/squareup/analytics/Analytics;

    .line 26
    iput-object p3, p0, Lcom/squareup/adanalytics/AdIdGatherer;->executor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method private acquireGoogleAdvertisingId()V
    .locals 6

    const/4 v0, 0x0

    .line 39
    :try_start_0
    iget-object v1, p0, Lcom/squareup/adanalytics/AdIdGatherer;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->getAdvertisingIdInfo(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v1

    .line 40
    invoke-virtual {v1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->getId()Ljava/lang/String;

    move-result-object v2

    .line 41
    invoke-virtual {v1}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->isLimitAdTrackingEnabled()Z

    move-result v1

    const-string v3, "Successfully got google ad id: %s, %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v0

    const/4 v5, 0x1

    .line 42
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 43
    iget-object v1, p0, Lcom/squareup/adanalytics/AdIdGatherer;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->setAdvertisingId(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Failed to get advertising id"

    .line 53
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public static synthetic lambda$fi0ouDx4eJzTn770cTmLrQlb2gY(Lcom/squareup/adanalytics/AdIdGatherer;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/adanalytics/AdIdGatherer;->acquireGoogleAdvertisingId()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 30
    iget-object p1, p0, Lcom/squareup/adanalytics/AdIdGatherer;->executor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/squareup/adanalytics/-$$Lambda$AdIdGatherer$fi0ouDx4eJzTn770cTmLrQlb2gY;

    invoke-direct {v0, p0}, Lcom/squareup/adanalytics/-$$Lambda$AdIdGatherer$fi0ouDx4eJzTn770cTmLrQlb2gY;-><init>(Lcom/squareup/adanalytics/AdIdGatherer;)V

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
