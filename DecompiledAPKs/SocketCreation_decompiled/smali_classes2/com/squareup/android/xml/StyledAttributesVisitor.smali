.class public interface abstract Lcom/squareup/android/xml/StyledAttributesVisitor;
.super Ljava/lang/Object;
.source "XmlResourceParsing.kt"

# interfaces
.implements Lcom/squareup/android/xml/AttributesVisitor;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/android/xml/StyledAttributesVisitor;",
        "Lcom/squareup/android/xml/AttributesVisitor;",
        "styled",
        "Landroid/content/res/TypedArray;",
        "getStyled",
        "()Landroid/content/res/TypedArray;",
        "android-xml_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getStyled()Landroid/content/res/TypedArray;
.end method
