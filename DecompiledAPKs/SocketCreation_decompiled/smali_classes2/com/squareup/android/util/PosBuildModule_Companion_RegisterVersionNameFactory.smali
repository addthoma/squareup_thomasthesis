.class public final Lcom/squareup/android/util/PosBuildModule_Companion_RegisterVersionNameFactory;
.super Ljava/lang/Object;
.source "PosBuildModule_Companion_RegisterVersionNameFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final posBuildProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/android/util/PosBuildModule_Companion_RegisterVersionNameFactory;->posBuildProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/android/util/PosBuildModule_Companion_RegisterVersionNameFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;)",
            "Lcom/squareup/android/util/PosBuildModule_Companion_RegisterVersionNameFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/android/util/PosBuildModule_Companion_RegisterVersionNameFactory;

    invoke-direct {v0, p0}, Lcom/squareup/android/util/PosBuildModule_Companion_RegisterVersionNameFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static registerVersionName(Lcom/squareup/util/PosBuild;)Ljava/lang/String;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/android/util/PosBuildModule;->Companion:Lcom/squareup/android/util/PosBuildModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/android/util/PosBuildModule$Companion;->registerVersionName(Lcom/squareup/util/PosBuild;)Ljava/lang/String;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/android/util/PosBuildModule_Companion_RegisterVersionNameFactory;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/lang/String;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/android/util/PosBuildModule_Companion_RegisterVersionNameFactory;->posBuildProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/PosBuild;

    invoke-static {v0}, Lcom/squareup/android/util/PosBuildModule_Companion_RegisterVersionNameFactory;->registerVersionName(Lcom/squareup/util/PosBuild;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
