.class public final Lcom/squareup/android/util/PosBuildModule$Companion;
.super Ljava/lang/Object;
.source "PosBuildModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/android/util/PosBuildModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0087\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0001J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0005\u001a\u00020\u0006H\u0001\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/android/util/PosBuildModule$Companion;",
        "",
        "()V",
        "registerVersionCode",
        "",
        "posBuild",
        "Lcom/squareup/util/PosBuild;",
        "registerVersionName",
        "",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/android/util/PosBuildModule$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final registerVersionCode(Lcom/squareup/util/PosBuild;)I
    .locals 1
    .annotation runtime Lcom/squareup/util/PosSdkVersionCode;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "posBuild"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-interface {p1}, Lcom/squareup/util/PosBuild;->getRegisterVersionCode()I

    move-result p1

    return p1
.end method

.method public final registerVersionName(Lcom/squareup/util/PosBuild;)Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/squareup/util/PosSdkVersionName;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "posBuild"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-interface {p1}, Lcom/squareup/util/PosBuild;->getRegisterVersionName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
