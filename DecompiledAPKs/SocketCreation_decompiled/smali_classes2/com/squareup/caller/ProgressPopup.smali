.class public Lcom/squareup/caller/ProgressPopup;
.super Ljava/lang/Object;
.source "ProgressPopup.java"

# interfaces
.implements Lcom/squareup/mortar/Popup;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/caller/ProgressPopup$DialogState;,
        Lcom/squareup/caller/ProgressPopup$Progress;,
        Lcom/squareup/caller/ProgressPopup$Component;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/mortar/Popup<",
        "Lcom/squareup/caller/ProgressPopup$Progress;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field coordinatorProvider:Lcom/squareup/caller/ProgressDialogCoordinator$Provider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private dialogState:Lcom/squareup/caller/ProgressPopup$DialogState;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/squareup/caller/ProgressPopup;->context:Landroid/content/Context;

    .line 75
    const-class v0, Lcom/squareup/caller/ProgressPopup$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->componentInParent(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/caller/ProgressPopup$Component;

    invoke-interface {p1, p0}, Lcom/squareup/caller/ProgressPopup$Component;->inject(Lcom/squareup/caller/ProgressPopup;)V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    .line 103
    iget-object v0, p0, Lcom/squareup/caller/ProgressPopup;->dialogState:Lcom/squareup/caller/ProgressPopup$DialogState;

    invoke-virtual {v0}, Lcom/squareup/caller/ProgressPopup$DialogState;->cancelAutoDismiss()V

    .line 104
    iget-object v0, p0, Lcom/squareup/caller/ProgressPopup;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/squareup/ui/DialogDestroyer;->get(Landroid/content/Context;)Lcom/squareup/ui/DialogDestroyer;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/caller/ProgressPopup;->dialogState:Lcom/squareup/caller/ProgressPopup$DialogState;

    invoke-static {v1}, Lcom/squareup/caller/ProgressPopup$DialogState;->access$000(Lcom/squareup/caller/ProgressPopup$DialogState;)Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/DialogDestroyer;->dismiss(Landroid/app/Dialog;)V

    const/4 v0, 0x0

    .line 105
    iput-object v0, p0, Lcom/squareup/caller/ProgressPopup;->dialogState:Lcom/squareup/caller/ProgressPopup$DialogState;

    return-void
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/caller/ProgressPopup;->context:Landroid/content/Context;

    return-object v0
.end method

.method getDialog()Landroid/app/Dialog;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/caller/ProgressPopup;->dialogState:Lcom/squareup/caller/ProgressPopup$DialogState;

    invoke-static {v0}, Lcom/squareup/caller/ProgressPopup$DialogState;->access$000(Lcom/squareup/caller/ProgressPopup$DialogState;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public isShowing()Z
    .locals 1

    .line 99
    iget-object v0, p0, Lcom/squareup/caller/ProgressPopup;->dialogState:Lcom/squareup/caller/ProgressPopup$DialogState;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$show$0$ProgressPopup(Lrx/Completable;)Lrx/Subscription;
    .locals 1

    .line 89
    new-instance v0, Lcom/squareup/caller/-$$Lambda$RVKO_jGq9t3HVt78n0FjoD45Gso;

    invoke-direct {v0, p0}, Lcom/squareup/caller/-$$Lambda$RVKO_jGq9t3HVt78n0FjoD45Gso;-><init>(Lcom/squareup/caller/ProgressPopup;)V

    invoke-virtual {p1, v0}, Lrx/Completable;->subscribe(Lrx/functions/Action0;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic show(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)V
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/caller/ProgressPopup$Progress;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/caller/ProgressPopup;->show(Lcom/squareup/caller/ProgressPopup$Progress;ZLcom/squareup/mortar/PopupPresenter;)V

    return-void
.end method

.method public show(Lcom/squareup/caller/ProgressPopup$Progress;ZLcom/squareup/mortar/PopupPresenter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .line 80
    iget-object p2, p0, Lcom/squareup/caller/ProgressPopup;->dialogState:Lcom/squareup/caller/ProgressPopup$DialogState;

    if-nez p2, :cond_0

    .line 81
    iget-object p2, p0, Lcom/squareup/caller/ProgressPopup;->context:Landroid/content/Context;

    invoke-static {p2}, Lcom/squareup/caller/ProgressDialogCoordinator;->inflate(Landroid/content/Context;)Landroid/view/View;

    move-result-object p2

    .line 82
    invoke-static {p2}, Lcom/squareup/caller/ProgressDialogCoordinator;->createDialog(Landroid/view/View;)Landroid/app/Dialog;

    move-result-object v0

    .line 83
    new-instance v1, Lcom/squareup/caller/ProgressPopup$DialogState;

    iget-object v2, p0, Lcom/squareup/caller/ProgressPopup;->coordinatorProvider:Lcom/squareup/caller/ProgressDialogCoordinator$Provider;

    invoke-direct {v1, p3, v0, p2, v2}, Lcom/squareup/caller/ProgressPopup$DialogState;-><init>(Lcom/squareup/mortar/PopupPresenter;Landroid/app/Dialog;Landroid/view/View;Lcom/squareup/coordinators/CoordinatorProvider;)V

    iput-object v1, p0, Lcom/squareup/caller/ProgressPopup;->dialogState:Lcom/squareup/caller/ProgressPopup$DialogState;

    .line 86
    :cond_0
    iget-boolean p2, p1, Lcom/squareup/caller/ProgressPopup$Progress;->complete:Z

    if-eqz p2, :cond_1

    .line 87
    iget-object p2, p0, Lcom/squareup/caller/ProgressPopup;->dialogState:Lcom/squareup/caller/ProgressPopup$DialogState;

    iget-object p1, p1, Lcom/squareup/caller/ProgressPopup$Progress;->message:Ljava/lang/String;

    invoke-virtual {p2, p1}, Lcom/squareup/caller/ProgressPopup$DialogState;->showComplete(Ljava/lang/String;)Lrx/Completable;

    move-result-object p1

    .line 88
    iget-object p2, p0, Lcom/squareup/caller/ProgressPopup;->dialogState:Lcom/squareup/caller/ProgressPopup$DialogState;

    iget-object p2, p2, Lcom/squareup/caller/ProgressPopup$DialogState;->container:Landroid/view/View;

    new-instance p3, Lcom/squareup/caller/-$$Lambda$ProgressPopup$4xX2448ywRwwG-N4udgbSuktMu0;

    invoke-direct {p3, p0, p1}, Lcom/squareup/caller/-$$Lambda$ProgressPopup$4xX2448ywRwwG-N4udgbSuktMu0;-><init>(Lcom/squareup/caller/ProgressPopup;Lrx/Completable;)V

    invoke-static {p2, p3}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 91
    :cond_1
    iget-object p2, p0, Lcom/squareup/caller/ProgressPopup;->dialogState:Lcom/squareup/caller/ProgressPopup$DialogState;

    iget-object p1, p1, Lcom/squareup/caller/ProgressPopup$Progress;->message:Ljava/lang/String;

    invoke-virtual {p2, p1}, Lcom/squareup/caller/ProgressPopup$DialogState;->showProgress(Ljava/lang/String;)V

    .line 94
    :goto_0
    iget-object p1, p0, Lcom/squareup/caller/ProgressPopup;->dialogState:Lcom/squareup/caller/ProgressPopup$DialogState;

    invoke-static {p1}, Lcom/squareup/caller/ProgressPopup$DialogState;->access$000(Lcom/squareup/caller/ProgressPopup$DialogState;)Landroid/app/Dialog;

    move-result-object p1

    .line 95
    invoke-virtual {p1}, Landroid/app/Dialog;->isShowing()Z

    move-result p2

    if-nez p2, :cond_2

    iget-object p2, p0, Lcom/squareup/caller/ProgressPopup;->context:Landroid/content/Context;

    invoke-static {p2}, Lcom/squareup/ui/DialogDestroyer;->get(Landroid/content/Context;)Lcom/squareup/ui/DialogDestroyer;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/squareup/ui/DialogDestroyer;->show(Landroid/app/Dialog;)V

    :cond_2
    return-void
.end method
