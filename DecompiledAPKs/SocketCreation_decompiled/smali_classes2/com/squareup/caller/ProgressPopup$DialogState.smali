.class Lcom/squareup/caller/ProgressPopup$DialogState;
.super Ljava/lang/Object;
.source "ProgressPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/caller/ProgressPopup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DialogState"
.end annotation


# instance fields
.field final container:Landroid/view/View;

.field private coordinator:Lcom/squareup/caller/ProgressDialogCoordinator;

.field private final dialog:Landroid/app/Dialog;

.field private final presenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/mortar/PopupPresenter;Landroid/app/Dialog;Landroid/view/View;Lcom/squareup/coordinators/CoordinatorProvider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            "Ljava/lang/Void;",
            ">;",
            "Landroid/app/Dialog;",
            "Landroid/view/View;",
            "Lcom/squareup/coordinators/CoordinatorProvider;",
            ")V"
        }
    .end annotation

    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput-object p1, p0, Lcom/squareup/caller/ProgressPopup$DialogState;->presenter:Lcom/squareup/mortar/PopupPresenter;

    .line 131
    iput-object p2, p0, Lcom/squareup/caller/ProgressPopup$DialogState;->dialog:Landroid/app/Dialog;

    .line 132
    iput-object p3, p0, Lcom/squareup/caller/ProgressPopup$DialogState;->container:Landroid/view/View;

    .line 134
    new-instance p1, Lcom/squareup/caller/-$$Lambda$ProgressPopup$DialogState$C8b3boOT0GZpAbwgaM8gy3ZmBcw;

    invoke-direct {p1, p0, p4}, Lcom/squareup/caller/-$$Lambda$ProgressPopup$DialogState$C8b3boOT0GZpAbwgaM8gy3ZmBcw;-><init>(Lcom/squareup/caller/ProgressPopup$DialogState;Lcom/squareup/coordinators/CoordinatorProvider;)V

    invoke-static {p3, p1}, Lcom/squareup/coordinators/Coordinators;->bind(Landroid/view/View;Lcom/squareup/coordinators/CoordinatorProvider;)V

    .line 139
    invoke-virtual {p2, p0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/caller/ProgressPopup$DialogState;)Landroid/app/Dialog;
    .locals 0

    .line 120
    iget-object p0, p0, Lcom/squareup/caller/ProgressPopup$DialogState;->dialog:Landroid/app/Dialog;

    return-object p0
.end method


# virtual methods
.method public cancelAutoDismiss()V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/caller/ProgressPopup$DialogState;->coordinator:Lcom/squareup/caller/ProgressDialogCoordinator;

    invoke-virtual {v0}, Lcom/squareup/caller/ProgressDialogCoordinator;->cancelAutoDismiss()V

    return-void
.end method

.method public synthetic lambda$new$0$ProgressPopup$DialogState(Lcom/squareup/coordinators/CoordinatorProvider;Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 135
    invoke-interface {p1, p2}, Lcom/squareup/coordinators/CoordinatorProvider;->provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/caller/ProgressDialogCoordinator;

    iput-object p1, p0, Lcom/squareup/caller/ProgressPopup$DialogState;->coordinator:Lcom/squareup/caller/ProgressDialogCoordinator;

    .line 136
    iget-object p1, p0, Lcom/squareup/caller/ProgressPopup$DialogState;->coordinator:Lcom/squareup/caller/ProgressDialogCoordinator;

    return-object p1
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .line 155
    iget-object p1, p0, Lcom/squareup/caller/ProgressPopup$DialogState;->presenter:Lcom/squareup/mortar/PopupPresenter;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/mortar/PopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method

.method showComplete(Ljava/lang/String;)Lrx/Completable;
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/caller/ProgressPopup$DialogState;->coordinator:Lcom/squareup/caller/ProgressDialogCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/caller/ProgressDialogCoordinator;->showComplete(Ljava/lang/String;)Lrx/Completable;

    move-result-object p1

    return-object p1
.end method

.method showProgress(Ljava/lang/String;)V
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/caller/ProgressPopup$DialogState;->coordinator:Lcom/squareup/caller/ProgressDialogCoordinator;

    invoke-virtual {v0, p1}, Lcom/squareup/caller/ProgressDialogCoordinator;->showProgress(Ljava/lang/String;)V

    return-void
.end method
