.class public final Lcom/squareup/caller/ProgressPopup_MembersInjector;
.super Ljava/lang/Object;
.source "ProgressPopup_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/caller/ProgressPopup;",
        ">;"
    }
.end annotation


# instance fields
.field private final coordinatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/caller/ProgressDialogCoordinator$Provider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/caller/ProgressDialogCoordinator$Provider;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/caller/ProgressPopup_MembersInjector;->coordinatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/caller/ProgressDialogCoordinator$Provider;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/caller/ProgressPopup;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/caller/ProgressPopup_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/caller/ProgressPopup_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectCoordinatorProvider(Lcom/squareup/caller/ProgressPopup;Lcom/squareup/caller/ProgressDialogCoordinator$Provider;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/caller/ProgressPopup;->coordinatorProvider:Lcom/squareup/caller/ProgressDialogCoordinator$Provider;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/caller/ProgressPopup;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/caller/ProgressPopup_MembersInjector;->coordinatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/caller/ProgressDialogCoordinator$Provider;

    invoke-static {p1, v0}, Lcom/squareup/caller/ProgressPopup_MembersInjector;->injectCoordinatorProvider(Lcom/squareup/caller/ProgressPopup;Lcom/squareup/caller/ProgressDialogCoordinator$Provider;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {p0, p1}, Lcom/squareup/caller/ProgressPopup_MembersInjector;->injectMembers(Lcom/squareup/caller/ProgressPopup;)V

    return-void
.end method
