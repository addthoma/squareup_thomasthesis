.class public final Lcom/squareup/AppBootstrapModule_ProvideActivityListenerFactory;
.super Ljava/lang/Object;
.source "AppBootstrapModule_ProvideActivityListenerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ActivityListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/AppBootstrapModule;


# direct methods
.method public constructor <init>(Lcom/squareup/AppBootstrapModule;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/AppBootstrapModule_ProvideActivityListenerFactory;->module:Lcom/squareup/AppBootstrapModule;

    return-void
.end method

.method public static create(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/AppBootstrapModule_ProvideActivityListenerFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/AppBootstrapModule_ProvideActivityListenerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/AppBootstrapModule_ProvideActivityListenerFactory;-><init>(Lcom/squareup/AppBootstrapModule;)V

    return-object v0
.end method

.method public static provideActivityListener(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/ActivityListener;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/AppBootstrapModule;->provideActivityListener()Lcom/squareup/ActivityListener;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ActivityListener;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ActivityListener;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/AppBootstrapModule_ProvideActivityListenerFactory;->module:Lcom/squareup/AppBootstrapModule;

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideActivityListenerFactory;->provideActivityListener(Lcom/squareup/AppBootstrapModule;)Lcom/squareup/ActivityListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/AppBootstrapModule_ProvideActivityListenerFactory;->get()Lcom/squareup/ActivityListener;

    move-result-object v0

    return-object v0
.end method
