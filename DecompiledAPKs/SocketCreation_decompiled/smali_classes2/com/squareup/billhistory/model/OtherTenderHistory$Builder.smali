.class public Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;
.super Lcom/squareup/billhistory/model/TenderHistory$Builder;
.source "OtherTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/model/OtherTenderHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/billhistory/model/TenderHistory$Builder<",
        "Lcom/squareup/billhistory/model/OtherTenderHistory;",
        "Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private name:Ljava/lang/String;

.field private note:Ljava/lang/String;

.field private tenderType:I

.field private tip:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 22
    sget-object v0, Lcom/squareup/billhistory/model/TenderHistory$Type;->OTHER:Lcom/squareup/billhistory/model/TenderHistory$Type;

    invoke-direct {p0, v0}, Lcom/squareup/billhistory/model/TenderHistory$Builder;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Type;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;)Ljava/lang/String;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;)Lcom/squareup/protos/common/Money;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->tip:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;)I
    .locals 0

    .line 14
    iget p0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->tenderType:I

    return p0
.end method


# virtual methods
.method public build()Lcom/squareup/billhistory/model/OtherTenderHistory;
    .locals 2

    .line 54
    new-instance v0, Lcom/squareup/billhistory/model/OtherTenderHistory;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/billhistory/model/OtherTenderHistory;-><init>(Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;Lcom/squareup/billhistory/model/OtherTenderHistory$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/billhistory/model/TenderHistory;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->build()Lcom/squareup/billhistory/model/OtherTenderHistory;

    move-result-object v0

    return-object v0
.end method

.method public from(Lcom/squareup/billhistory/model/OtherTenderHistory;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;
    .locals 1

    .line 26
    iget-object v0, p1, Lcom/squareup/billhistory/model/OtherTenderHistory;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->name:Ljava/lang/String;

    .line 27
    iget-object v0, p1, Lcom/squareup/billhistory/model/OtherTenderHistory;->note:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->note:Ljava/lang/String;

    .line 28
    iget-object v0, p1, Lcom/squareup/billhistory/model/OtherTenderHistory;->tip:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->tip:Lcom/squareup/protos/common/Money;

    .line 29
    iget v0, p1, Lcom/squareup/billhistory/model/OtherTenderHistory;->tenderType:I

    iput v0, p0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->tenderType:I

    .line 30
    invoke-super {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory$Builder;->from(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object p1

    check-cast p1, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    return-object p1
.end method

.method public bridge synthetic from(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/billhistory/model/OtherTenderHistory;

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->from(Lcom/squareup/billhistory/model/OtherTenderHistory;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object p1

    return-object p1
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;
    .locals 0

    .line 34
    iput-object p1, p0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public tenderType(I)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;
    .locals 0

    .line 49
    iput p1, p0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->tenderType:I

    return-object p0
.end method

.method public tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->tip:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public bridge synthetic tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 0

    .line 14
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;->tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/OtherTenderHistory$Builder;

    move-result-object p1

    return-object p1
.end method
