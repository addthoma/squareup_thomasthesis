.class public final Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator_Factory;
.super Ljava/lang/Object;
.source "TenderHistoryTippingCalculator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;",
        ">;"
    }
.end annotation


# instance fields
.field private final paperSignatureSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator_Factory;->settingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)",
            "Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator_Factory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    invoke-direct {v0, p0, p1}, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;-><init>(Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/settings/server/AccountStatusSettings;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/papersignature/PaperSignatureSettings;

    iget-object v1, p0, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {v0, v1}, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator_Factory;->newInstance(Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator_Factory;->get()Lcom/squareup/billhistory/model/TenderHistoryTippingCalculator;

    move-result-object v0

    return-object v0
.end method
