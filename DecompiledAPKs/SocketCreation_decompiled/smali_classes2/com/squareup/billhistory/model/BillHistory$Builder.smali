.class public Lcom/squareup/billhistory/model/BillHistory$Builder;
.super Ljava/lang/Object;
.source "BillHistory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/model/BillHistory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private cart:Lcom/squareup/protos/client/bills/Cart;

.field private creatorName:Ljava/lang/String;

.field private electronicSignature:Ljava/lang/String;

.field private errorMessage:Ljava/lang/String;

.field private errorTitle:Ljava/lang/String;

.field private firstBillId:Lcom/squareup/protos/client/IdPair;

.field private firstCart:Lcom/squareup/protos/client/bills/Cart;

.field private firstOrder:Lcom/squareup/payment/Order;

.field private id:Lcom/squareup/billhistory/model/BillHistoryId;

.field private isRefund:Z

.field private loyaltyDetails:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

.field private final note:Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

.field private order:Lcom/squareup/payment/Order;

.field private paymentState:Lcom/squareup/protos/client/bills/Tender$State;

.field private final pending:Z

.field private final receiptNumbers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private refundAmount:Lcom/squareup/protos/common/Money;

.field private relatedBills:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final sourceBillId:Lcom/squareup/protos/client/IdPair;

.field private storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

.field private tenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;"
        }
    .end annotation
.end field

.field private final time:Ljava/util/Date;

.field private tip:Lcom/squareup/protos/common/Money;

.field private total:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 27

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    .line 912
    iget-object v2, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    iget-object v3, v0, Lcom/squareup/billhistory/model/BillHistory;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v0, Lcom/squareup/billhistory/model/BillHistory;->order:Lcom/squareup/payment/Order;

    iget-object v5, v0, Lcom/squareup/billhistory/model/BillHistory;->tip:Lcom/squareup/protos/common/Money;

    iget-object v6, v0, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    iget-object v7, v0, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    iget-object v8, v0, Lcom/squareup/billhistory/model/BillHistory;->note:Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    iget-object v9, v0, Lcom/squareup/billhistory/model/BillHistory;->receiptNumbers:Ljava/util/List;

    iget-boolean v10, v0, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    iget-object v11, v0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    iget-object v12, v0, Lcom/squareup/billhistory/model/BillHistory;->creatorName:Ljava/lang/String;

    iget-object v13, v0, Lcom/squareup/billhistory/model/BillHistory;->electronicSignature:Ljava/lang/String;

    new-instance v15, Ljava/util/ArrayList;

    move-object v14, v15

    move-object/from16 v26, v1

    .line 925
    invoke-static/range {p1 .. p1}, Lcom/squareup/billhistory/model/BillHistory;->access$000(Lcom/squareup/billhistory/model/BillHistory;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v15, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v15, v0, Lcom/squareup/billhistory/model/BillHistory;->paymentState:Lcom/squareup/protos/client/bills/Tender$State;

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory;->refundAmount:Lcom/squareup/protos/common/Money;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory;->storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-object/from16 v17, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory;->errorTitle:Ljava/lang/String;

    move-object/from16 v18, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory;->errorMessage:Ljava/lang/String;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory;->cart:Lcom/squareup/protos/client/bills/Cart;

    move-object/from16 v20, v1

    iget-boolean v1, v0, Lcom/squareup/billhistory/model/BillHistory;->isRefund:Z

    move/from16 v21, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory;->loyaltyDetails:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    move-object/from16 v22, v1

    .line 934
    invoke-static/range {p1 .. p1}, Lcom/squareup/billhistory/model/BillHistory;->access$100(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/protos/client/IdPair;

    move-result-object v23

    .line 935
    invoke-static/range {p1 .. p1}, Lcom/squareup/billhistory/model/BillHistory;->access$200(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/payment/Order;

    move-result-object v24

    .line 936
    invoke-static/range {p1 .. p1}, Lcom/squareup/billhistory/model/BillHistory;->access$300(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/protos/client/bills/Cart;

    move-result-object v25

    move-object/from16 v1, v26

    .line 912
    invoke-direct/range {v1 .. v25}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;ZLjava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/bills/Tender$State;Lcom/squareup/protos/common/Money;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;ZLcom/squareup/protos/client/bills/Tender$LoyaltyDetails;Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/Order;Lcom/squareup/protos/client/bills/Cart;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;ZLjava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/bills/Tender$State;Lcom/squareup/protos/common/Money;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;ZLcom/squareup/protos/client/bills/Tender$LoyaltyDetails;Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/Order;Lcom/squareup/protos/client/bills/Cart;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistoryId;",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/Date;",
            "Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;",
            "Lcom/squareup/protos/client/bills/Tender$State;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/transactionhistory/pending/StoreAndForwardState;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/Cart;",
            "Z",
            "Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/protos/client/bills/Cart;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 989
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    move-object v1, p2

    .line 990
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    move-object v1, p3

    .line 991
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->order:Lcom/squareup/payment/Order;

    move-object v1, p4

    .line 992
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->tip:Lcom/squareup/protos/common/Money;

    move-object v1, p5

    .line 993
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->total:Lcom/squareup/protos/common/Money;

    move-object v1, p6

    .line 994
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->time:Ljava/util/Date;

    move-object v1, p7

    .line 995
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->note:Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    move-object v1, p8

    .line 996
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->receiptNumbers:Ljava/util/List;

    move v1, p9

    .line 997
    iput-boolean v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->pending:Z

    move-object v1, p10

    .line 998
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->tenders:Ljava/util/List;

    move-object v1, p11

    .line 999
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->creatorName:Ljava/lang/String;

    move-object v1, p12

    .line 1000
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->electronicSignature:Ljava/lang/String;

    move-object v1, p13

    .line 1001
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->relatedBills:Ljava/util/List;

    move-object/from16 v1, p14

    .line 1002
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->paymentState:Lcom/squareup/protos/client/bills/Tender$State;

    move-object/from16 v1, p15

    .line 1003
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->refundAmount:Lcom/squareup/protos/common/Money;

    move-object/from16 v1, p16

    .line 1004
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-object/from16 v1, p17

    .line 1005
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->errorTitle:Ljava/lang/String;

    move-object/from16 v1, p18

    .line 1006
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->errorMessage:Ljava/lang/String;

    move-object/from16 v1, p19

    .line 1007
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    move/from16 v1, p20

    .line 1008
    iput-boolean v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->isRefund:Z

    move-object/from16 v1, p21

    .line 1009
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->loyaltyDetails:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    move-object/from16 v1, p22

    .line 1010
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->firstBillId:Lcom/squareup/protos/client/IdPair;

    move-object/from16 v1, p23

    .line 1011
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->firstOrder:Lcom/squareup/payment/Order;

    move-object/from16 v1, p24

    .line 1012
    iput-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->firstCart:Lcom/squareup/protos/client/bills/Cart;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;Z)V
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistoryId;",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/Date;",
            "Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p4

    move-object/from16 v23, p4

    move-object/from16 v4, p5

    move-object/from16 v7, p7

    move/from16 v9, p9

    .line 960
    invoke-virtual/range {p4 .. p4}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v5

    const-string v6, "time"

    move-object/from16 v8, p6

    .line 961
    invoke-static {v8, v6}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Date;

    invoke-static {v6}, Lcom/squareup/util/Dates;->copy(Ljava/util/Date;)Ljava/util/Date;

    move-result-object v6

    new-instance v8, Ljava/util/ArrayList;

    .line 963
    invoke-static/range {p8 .. p8}, Lcom/squareup/util/SquareCollections;->emptyIfNull(Ljava/util/List;)Ljava/util/List;

    move-result-object v10

    invoke-direct {v8, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v8}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    new-instance v10, Ljava/util/ArrayList;

    .line 965
    invoke-static/range {p3 .. p3}, Lcom/squareup/util/SquareCollections;->emptyIfNull(Ljava/util/List;)Ljava/util/List;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v10}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v10

    new-instance v11, Ljava/util/ArrayList;

    move-object v13, v11

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    sget-object v14, Lcom/squareup/protos/client/bills/Tender$State;->UNKNOWN_STATE:Lcom/squareup/protos/client/bills/Tender$State;

    new-instance v11, Lcom/squareup/protos/common/Money;

    move-object v15, v11

    const-wide/16 v16, 0x0

    .line 970
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    move-object/from16 v25, v0

    invoke-virtual/range {p4 .. p4}, Lcom/squareup/payment/Order;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    invoke-direct {v11, v12, v0}, Lcom/squareup/protos/common/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/CurrencyCode;)V

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v24, 0x0

    move-object/from16 v0, v25

    .line 956
    invoke-direct/range {v0 .. v24}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;ZLjava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/bills/Tender$State;Lcom/squareup/protos/common/Money;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;ZLcom/squareup/protos/client/bills/Tender$LoyaltyDetails;Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/Order;Lcom/squareup/protos/client/bills/Cart;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/billhistory/model/BillHistoryId;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistoryId;",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/Date;",
            "Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    .line 943
    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    .line 942
    invoke-direct/range {v0 .. v9}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;Z)V

    return-void
.end method


# virtual methods
.method public addRelatedBill(Lcom/squareup/server/payment/RelatedBillHistory;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 2

    .line 1036
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->relatedBills:Ljava/util/List;

    const-string v1, "relatedBill"

    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addRelatedBills([Lcom/squareup/server/payment/RelatedBillHistory;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 2

    .line 1041
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->relatedBills:Ljava/util/List;

    const-string v1, "relatedBills"

    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/Object;

    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    return-object p0
.end method

.method public asNewBill(Lcom/squareup/billhistory/model/BillHistoryId;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 27

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p3

    .line 1122
    new-instance v26, Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-object/from16 v1, v26

    iget-object v3, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->order:Lcom/squareup/payment/Order;

    iget-object v5, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->tip:Lcom/squareup/protos/common/Money;

    iget-object v6, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->total:Lcom/squareup/protos/common/Money;

    iget-object v9, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->receiptNumbers:Ljava/util/List;

    iget-boolean v10, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->pending:Z

    iget-object v11, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->tenders:Ljava/util/List;

    iget-object v12, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->creatorName:Ljava/lang/String;

    iget-object v13, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->electronicSignature:Ljava/lang/String;

    iget-object v14, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->relatedBills:Ljava/util/List;

    iget-object v15, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->paymentState:Lcom/squareup/protos/client/bills/Tender$State;

    move-object/from16 p1, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->refundAmount:Lcom/squareup/protos/common/Money;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-object/from16 v17, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->errorTitle:Ljava/lang/String;

    move-object/from16 v18, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->errorMessage:Ljava/lang/String;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    move-object/from16 v20, v1

    iget-boolean v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->isRefund:Z

    move/from16 v21, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->loyaltyDetails:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    move-object/from16 v22, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->firstBillId:Lcom/squareup/protos/client/IdPair;

    move-object/from16 v23, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->firstOrder:Lcom/squareup/payment/Order;

    move-object/from16 v24, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->firstCart:Lcom/squareup/protos/client/bills/Cart;

    move-object/from16 v25, v1

    move-object/from16 v1, p1

    invoke-direct/range {v1 .. v25}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;Ljava/util/List;ZLjava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/client/bills/Tender$State;Lcom/squareup/protos/common/Money;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;ZLcom/squareup/protos/client/bills/Tender$LoyaltyDetails;Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/Order;Lcom/squareup/protos/client/bills/Cart;)V

    return-object v26
.end method

.method public build()Lcom/squareup/billhistory/model/BillHistory;
    .locals 29

    move-object/from16 v0, p0

    .line 1129
    new-instance v27, Lcom/squareup/billhistory/model/BillHistory;

    move-object/from16 v1, v27

    iget-object v2, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    iget-object v3, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->sourceBillId:Lcom/squareup/protos/client/IdPair;

    iget-object v4, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->tenders:Ljava/util/List;

    iget-object v5, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->order:Lcom/squareup/payment/Order;

    iget-object v6, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->tip:Lcom/squareup/protos/common/Money;

    iget-object v7, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->total:Lcom/squareup/protos/common/Money;

    iget-object v8, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->time:Ljava/util/Date;

    iget-object v9, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->note:Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    iget-boolean v10, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->pending:Z

    iget-object v11, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->relatedBills:Ljava/util/List;

    iget-object v12, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->receiptNumbers:Ljava/util/List;

    iget-object v13, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->electronicSignature:Ljava/lang/String;

    iget-object v14, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->creatorName:Ljava/lang/String;

    iget-object v15, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->paymentState:Lcom/squareup/protos/client/bills/Tender$State;

    move-object/from16 v28, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-object/from16 v16, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->refundAmount:Lcom/squareup/protos/common/Money;

    move-object/from16 v17, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->errorTitle:Ljava/lang/String;

    move-object/from16 v18, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->errorMessage:Ljava/lang/String;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    move-object/from16 v20, v1

    iget-boolean v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->isRefund:Z

    move/from16 v21, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->loyaltyDetails:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    move-object/from16 v22, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->firstBillId:Lcom/squareup/protos/client/IdPair;

    move-object/from16 v23, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->firstOrder:Lcom/squareup/payment/Order;

    move-object/from16 v24, v1

    iget-object v1, v0, Lcom/squareup/billhistory/model/BillHistory$Builder;->firstCart:Lcom/squareup/protos/client/bills/Cart;

    move-object/from16 v25, v1

    const/16 v26, 0x0

    move-object/from16 v1, v28

    invoke-direct/range {v1 .. v26}, Lcom/squareup/billhistory/model/BillHistory;-><init>(Lcom/squareup/billhistory/model/BillHistoryId;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/Date;Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;ZLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Tender$State;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;ZLcom/squareup/protos/client/bills/Tender$LoyaltyDetails;Lcom/squareup/protos/client/IdPair;Lcom/squareup/payment/Order;Lcom/squareup/protos/client/bills/Cart;Lcom/squareup/billhistory/model/BillHistory$1;)V

    return-object v27
.end method

.method public getOrder()Lcom/squareup/payment/Order;
    .locals 1

    .line 1021
    iget-object v0, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->order:Lcom/squareup/payment/Order;

    return-object v0
.end method

.method public setCart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1067
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->cart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public setCreatorName(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1031
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->creatorName:Ljava/lang/String;

    return-object p0
.end method

.method public setError(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1061
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->errorTitle:Ljava/lang/String;

    .line 1062
    iput-object p2, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->errorMessage:Ljava/lang/String;

    return-object p0
.end method

.method public setFirstBillId(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1107
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->firstBillId:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public setFirstCart(Lcom/squareup/protos/client/bills/Cart;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1117
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->firstCart:Lcom/squareup/protos/client/bills/Cart;

    return-object p0
.end method

.method public setFirstOrder(Lcom/squareup/payment/Order;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1112
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->firstOrder:Lcom/squareup/payment/Order;

    return-object p0
.end method

.method public setId(Lcom/squareup/billhistory/model/BillHistoryId;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1016
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    return-object p0
.end method

.method public setIsRefund(Z)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1092
    iput-boolean p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->isRefund:Z

    return-object p0
.end method

.method public setLoyaltyDetails(Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1102
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->loyaltyDetails:Lcom/squareup/protos/client/bills/Tender$LoyaltyDetails;

    return-object p0
.end method

.method public setOrder(Lcom/squareup/payment/Order;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1025
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->order:Lcom/squareup/payment/Order;

    .line 1026
    invoke-virtual {p1}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->total(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/BillHistory$Builder;

    return-object p0
.end method

.method public setPaymentState(Lcom/squareup/protos/client/bills/Tender$State;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1051
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->paymentState:Lcom/squareup/protos/client/bills/Tender$State;

    return-object p0
.end method

.method public setRefundAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1097
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->refundAmount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public setRelatedBills(Ljava/util/List;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/RelatedBillHistory;",
            ">;)",
            "Lcom/squareup/billhistory/model/BillHistory$Builder;"
        }
    .end annotation

    const-string v0, "relatedBillHistories"

    .line 1046
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->relatedBills:Ljava/util/List;

    return-object p0
.end method

.method public setStoreAndForward(Lcom/squareup/transactionhistory/pending/StoreAndForwardState;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1056
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    return-object p0
.end method

.method public setTenders(Ljava/util/List;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/TenderHistory;",
            ">;)",
            "Lcom/squareup/billhistory/model/BillHistory$Builder;"
        }
    .end annotation

    .line 1072
    new-instance v0, Ljava/util/ArrayList;

    const-string v1, "tenders"

    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->tenders:Ljava/util/List;

    return-object p0
.end method

.method public signature(Ljava/lang/String;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1087
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->electronicSignature:Ljava/lang/String;

    return-object p0
.end method

.method public tip(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1077
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->tip:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public total(Lcom/squareup/protos/common/Money;)Lcom/squareup/billhistory/model/BillHistory$Builder;
    .locals 0

    .line 1082
    iput-object p1, p0, Lcom/squareup/billhistory/model/BillHistory$Builder;->total:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
