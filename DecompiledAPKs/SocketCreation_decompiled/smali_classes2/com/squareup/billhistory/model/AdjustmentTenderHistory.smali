.class public Lcom/squareup/billhistory/model/AdjustmentTenderHistory;
.super Lcom/squareup/billhistory/model/TenderHistory;
.source "AdjustmentTenderHistory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/TenderHistory;-><init>(Lcom/squareup/billhistory/model/TenderHistory$Builder;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;Lcom/squareup/billhistory/model/AdjustmentTenderHistory$1;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lcom/squareup/billhistory/model/AdjustmentTenderHistory;-><init>(Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;)V

    return-void
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;

    invoke-direct {v0}, Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;->from(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/TenderHistory$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;

    return-object v0
.end method

.method public bridge synthetic buildUpon()Lcom/squareup/billhistory/model/TenderHistory$Builder;
    .locals 1

    .line 3
    invoke-virtual {p0}, Lcom/squareup/billhistory/model/AdjustmentTenderHistory;->buildUpon()Lcom/squareup/billhistory/model/AdjustmentTenderHistory$Builder;

    move-result-object v0

    return-object v0
.end method
