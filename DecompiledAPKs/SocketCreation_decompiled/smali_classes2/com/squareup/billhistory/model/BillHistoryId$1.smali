.class final Lcom/squareup/billhistory/model/BillHistoryId$1;
.super Ljava/lang/Object;
.source "BillHistoryId.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/billhistory/model/BillHistoryId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/billhistory/model/BillHistoryId;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/billhistory/model/BillHistoryId;
    .locals 4

    .line 158
    invoke-static {}, Lcom/squareup/billhistory/model/BillHistoryId$BillType;->values()[Lcom/squareup/billhistory/model/BillHistoryId$BillType;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    .line 159
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/IdPair;

    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    .line 161
    new-instance v2, Lcom/squareup/billhistory/model/BillHistoryId;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, p1, v3}, Lcom/squareup/billhistory/model/BillHistoryId;-><init>(Lcom/squareup/billhistory/model/BillHistoryId$BillType;Lcom/squareup/protos/client/IdPair;Ljava/lang/String;Lcom/squareup/billhistory/model/BillHistoryId$1;)V

    return-object v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 156
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/BillHistoryId$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/billhistory/model/BillHistoryId;
    .locals 0

    .line 165
    new-array p1, p1, [Lcom/squareup/billhistory/model/BillHistoryId;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 156
    invoke-virtual {p0, p1}, Lcom/squareup/billhistory/model/BillHistoryId$1;->newArray(I)[Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object p1

    return-object p1
.end method
