.class public final Lcom/squareup/banklinking/RealBankAccountSettings;
.super Ljava/lang/Object;
.source "RealBankAccountSettings.kt"

# interfaces
.implements Lcom/squareup/banklinking/BankAccountSettings;


# annotations
.annotation runtime Lcom/squareup/banklinking/RealBankAccountSettings$SharedScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/RealBankAccountSettings$SharedScope;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00c6\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0001VBG\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u00a2\u0006\u0002\u0010\u0012J\u000e\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0017H\u0016J\u0016\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0016\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u0006\u0010\u001c\u001a\u00020\u001aH\u0016J\u0016\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u00172\u0006\u0010\u001f\u001a\u00020\u001aH\u0016J(\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0008\u0010%\u001a\u0004\u0018\u00010\u001aH\u0016J\u000e\u0010&\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0017H\u0002J\u001c\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020*0)H\u0002J\u000e\u0010+\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0017H\u0002J\u0016\u0010,\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0016\u0010-\u001a\u00020\u00152\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020.0)H\u0002J\u0010\u0010/\u001a\u00020\u00152\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0016\u00100\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u0006\u0010\u001c\u001a\u00020\u001aH\u0002J\u001c\u00101\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u0002020)H\u0002J\u000e\u00103\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0017H\u0002J\u0010\u00104\u001a\u0002052\u0006\u00106\u001a\u000207H\u0016J\u0008\u00108\u001a\u000205H\u0016J\u000e\u00109\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0017H\u0002J\u0016\u0010:\u001a\u00020\u00152\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020;0)H\u0002J\u0010\u0010<\u001a\u00020\u00152\u0006\u0010=\u001a\u00020;H\u0002J\u0016\u0010>\u001a\u0008\u0012\u0004\u0012\u00020\u001e0\u00172\u0006\u0010\u001f\u001a\u00020\u001aH\u0002J\u0016\u0010?\u001a\u00020\u001e2\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020@0)H\u0002J\u0018\u0010A\u001a\u00020\u001e2\u0006\u0010%\u001a\u00020\u001a2\u0006\u0010=\u001a\u00020@H\u0002J\u000e\u0010B\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0017H\u0002J\u0016\u0010C\u001a\u00020\u00152\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020D0)H\u0002J\u0010\u0010E\u001a\u00020\u00152\u0006\u0010=\u001a\u00020DH\u0002J(\u0010F\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\u0008\u0010%\u001a\u0004\u0018\u00010\u001aH\u0002J$\u0010G\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u0006\u0010#\u001a\u00020$2\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020H0)H\u0002J\u0016\u0010I\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u00172\u0006\u0010#\u001a\u00020$H\u0002J\u000e\u0010J\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0017H\u0002J\u0016\u0010K\u001a\u00020\u00152\u000c\u0010(\u001a\u0008\u0012\u0004\u0012\u00020L0)H\u0002J\u0008\u0010M\u001a\u00020\u0015H\u0002J\u0008\u0010N\u001a\u000205H\u0002J\u000e\u0010O\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0017H\u0016J\u000e\u0010P\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0017H\u0016J\u0008\u0010Q\u001a\u000205H\u0016J\u0008\u0010R\u001a\u000205H\u0002J\u0008\u0010S\u001a\u00020$H\u0002J\u000e\u0010T\u001a\u0008\u0012\u0004\u0012\u00020\u00150UH\u0016R\u0014\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00150\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006W"
    }
    d2 = {
        "Lcom/squareup/banklinking/RealBankAccountSettings;",
        "Lcom/squareup/banklinking/BankAccountSettings;",
        "bankAccountService",
        "Lcom/squareup/server/bankaccount/BankAccountService;",
        "multipassService",
        "Lcom/squareup/api/multipassauth/MultipassService;",
        "messageFactory",
        "Lcom/squareup/receiving/FailureMessageFactory;",
        "analytics",
        "Lcom/squareup/banklinking/BankAccountSettingsAnalytics;",
        "res",
        "Landroid/content/res/Resources;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "unique",
        "Lcom/squareup/util/Unique;",
        "(Lcom/squareup/server/bankaccount/BankAccountService;Lcom/squareup/api/multipassauth/MultipassService;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/banklinking/BankAccountSettingsAnalytics;Landroid/content/res/Resources;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Unique;)V",
        "_state",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/banklinking/BankAccountSettings$State;",
        "cancelVerification",
        "Lio/reactivex/Single;",
        "checkPassword",
        "password",
        "",
        "confirmBankAccount",
        "confirmationToken",
        "getDirectDebitInfo",
        "Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;",
        "sortCode",
        "linkBankAccount",
        "bankAccountDetails",
        "Lcom/squareup/protos/client/bankaccount/BankAccountDetails;",
        "isOnboarding",
        "",
        "idempotenceKey",
        "onCancelVerification",
        "onCancelVerificationFailure",
        "failure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "Lcom/squareup/protos/client/bankaccount/CancelVerificationResponse;",
        "onCancelVerificationSuccess",
        "onCheckPassword",
        "onCheckPasswordFailure",
        "Lcom/squareup/protos/client/multipass/CheckPasswordResponse;",
        "onCheckPasswordSuccess",
        "onConfirmBankAccount",
        "onConfirmBankAccountFailure",
        "Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountResponse;",
        "onConfirmBankAccountSuccess",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onGetBankAccounts",
        "onGetBankAccountsFailure",
        "Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;",
        "onGetBankAccountsSuccess",
        "response",
        "onGetDirectDebitInfo",
        "onGetDirectDebitInfoFailure",
        "Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;",
        "onGetDirectDebitInfoSuccess",
        "onGetLatestBankAccount",
        "onGetLatestBankAccountFailure",
        "Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;",
        "onGetLatestBankAccountSuccess",
        "onLinkBankAccount",
        "onLinkBankAccountFailure",
        "Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;",
        "onLinkBankAccountSuccess",
        "onResendVerificationEmail",
        "onResendVerificationEmailFailure",
        "Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailResponse;",
        "onResendVerificationEmailSuccess",
        "onResetRequestState",
        "refresh",
        "resendVerificationEmail",
        "resetRequestState",
        "setLoadingState",
        "shouldResetRequestState",
        "state",
        "Lio/reactivex/Observable;",
        "SharedScope",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final _state:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation
.end field

.field private final analytics:Lcom/squareup/banklinking/BankAccountSettingsAnalytics;

.field private final bankAccountService:Lcom/squareup/server/bankaccount/BankAccountService;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final multipassService:Lcom/squareup/api/multipassauth/MultipassService;

.field private final res:Landroid/content/res/Resources;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final unique:Lcom/squareup/util/Unique;


# direct methods
.method public constructor <init>(Lcom/squareup/server/bankaccount/BankAccountService;Lcom/squareup/api/multipassauth/MultipassService;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/banklinking/BankAccountSettingsAnalytics;Landroid/content/res/Resources;Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Unique;)V
    .locals 24
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    const-string v9, "bankAccountService"

    invoke-static {v1, v9}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v9, "multipassService"

    invoke-static {v2, v9}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v9, "messageFactory"

    invoke-static {v3, v9}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v9, "analytics"

    invoke-static {v4, v9}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v9, "res"

    invoke-static {v5, v9}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v9, "features"

    invoke-static {v6, v9}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v9, "settings"

    invoke-static {v7, v9}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v9, "unique"

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->bankAccountService:Lcom/squareup/server/bankaccount/BankAccountService;

    iput-object v2, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->multipassService:Lcom/squareup/api/multipassauth/MultipassService;

    iput-object v3, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    iput-object v4, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->analytics:Lcom/squareup/banklinking/BankAccountSettingsAnalytics;

    iput-object v5, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->res:Landroid/content/res/Resources;

    iput-object v6, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->features:Lcom/squareup/settings/server/Features;

    iput-object v7, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object v8, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->unique:Lcom/squareup/util/Unique;

    .line 75
    new-instance v1, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x7ff

    const/16 v23, 0x0

    move-object v10, v1

    invoke-direct/range {v10 .. v23}, Lcom/squareup/banklinking/BankAccountSettings$State;-><init>(Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-static {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v1

    const-string v2, "BehaviorRelay.createDefault(State())"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/banklinking/RealBankAccountSettings;)Landroid/content/res/Resources;
    .locals 0

    .line 60
    iget-object p0, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->res:Landroid/content/res/Resources;

    return-object p0
.end method

.method public static final synthetic access$onCancelVerificationFailure(Lcom/squareup/banklinking/RealBankAccountSettings;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lio/reactivex/Single;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->onCancelVerificationFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onCancelVerificationSuccess(Lcom/squareup/banklinking/RealBankAccountSettings;)Lio/reactivex/Single;
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->onCancelVerificationSuccess()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onCheckPasswordFailure(Lcom/squareup/banklinking/RealBankAccountSettings;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->onCheckPasswordFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onCheckPasswordSuccess(Lcom/squareup/banklinking/RealBankAccountSettings;Ljava/lang/String;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->onCheckPasswordSuccess(Ljava/lang/String;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onConfirmBankAccountFailure(Lcom/squareup/banklinking/RealBankAccountSettings;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lio/reactivex/Single;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->onConfirmBankAccountFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onConfirmBankAccountSuccess(Lcom/squareup/banklinking/RealBankAccountSettings;)Lio/reactivex/Single;
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->onConfirmBankAccountSuccess()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onGetBankAccountsFailure(Lcom/squareup/banklinking/RealBankAccountSettings;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->onGetBankAccountsFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onGetBankAccountsSuccess(Lcom/squareup/banklinking/RealBankAccountSettings;Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->onGetBankAccountsSuccess(Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onGetDirectDebitInfoFailure(Lcom/squareup/banklinking/RealBankAccountSettings;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->onGetDirectDebitInfoFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onGetDirectDebitInfoSuccess(Lcom/squareup/banklinking/RealBankAccountSettings;Ljava/lang/String;Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;)Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/squareup/banklinking/RealBankAccountSettings;->onGetDirectDebitInfoSuccess(Ljava/lang/String;Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;)Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onGetLatestBankAccountFailure(Lcom/squareup/banklinking/RealBankAccountSettings;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->onGetLatestBankAccountFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onGetLatestBankAccountSuccess(Lcom/squareup/banklinking/RealBankAccountSettings;Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->onGetLatestBankAccountSuccess(Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onLinkBankAccountFailure(Lcom/squareup/banklinking/RealBankAccountSettings;ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lio/reactivex/Single;
    .locals 0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/squareup/banklinking/RealBankAccountSettings;->onLinkBankAccountFailure(ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onLinkBankAccountSuccess(Lcom/squareup/banklinking/RealBankAccountSettings;Z)Lio/reactivex/Single;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->onLinkBankAccountSuccess(Z)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onResendVerificationEmailFailure(Lcom/squareup/banklinking/RealBankAccountSettings;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 0

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->onResendVerificationEmailFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$onResendVerificationEmailSuccess(Lcom/squareup/banklinking/RealBankAccountSettings;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->onResendVerificationEmailSuccess()Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$shouldResetRequestState(Lcom/squareup/banklinking/RealBankAccountSettings;)Z
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->shouldResetRequestState()Z

    move-result p0

    return p0
.end method

.method private final onCancelVerification()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    .line 188
    new-instance v0, Lcom/squareup/protos/client/bankaccount/CancelVerificationRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/CancelVerificationRequest$Builder;-><init>()V

    .line 189
    iget-object v1, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v1, Lcom/squareup/banklinking/BankAccountSettings$State;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettings$State;->bankAccount()Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    iget-object v1, v1, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_account_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bankaccount/CancelVerificationRequest$Builder;->bank_account_token(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/CancelVerificationRequest$Builder;

    move-result-object v0

    .line 190
    invoke-virtual {v0}, Lcom/squareup/protos/client/bankaccount/CancelVerificationRequest$Builder;->build()Lcom/squareup/protos/client/bankaccount/CancelVerificationRequest;

    move-result-object v0

    .line 192
    iget-object v1, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->bankAccountService:Lcom/squareup/server/bankaccount/BankAccountService;

    const-string v2, "request"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/server/bankaccount/BankAccountService;->cancelVerification(Lcom/squareup/protos/client/bankaccount/CancelVerificationRequest;)Lcom/squareup/server/bankaccount/BankAccountService$BankAccountStandardResponse;

    move-result-object v0

    .line 193
    invoke-virtual {v0}, Lcom/squareup/server/bankaccount/BankAccountService$BankAccountStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 194
    new-instance v1, Lcom/squareup/banklinking/RealBankAccountSettings$onCancelVerification$1;

    invoke-direct {v1, p0}, Lcom/squareup/banklinking/RealBankAccountSettings$onCancelVerification$1;-><init>(Lcom/squareup/banklinking/RealBankAccountSettings;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "bankAccountService.cance\u2026it)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final onCancelVerificationFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lio/reactivex/Single;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/bankaccount/CancelVerificationResponse;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 442
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v2, Lcom/squareup/banklinking/impl/R$string;->cancel_verification_failed:I

    sget-object v3, Lcom/squareup/banklinking/RealBankAccountSettings$onCancelVerificationFailure$failureMessage$1;->INSTANCE:Lcom/squareup/banklinking/RealBankAccountSettings$onCancelVerificationFailure$failureMessage$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    move-object/from16 v4, p1

    invoke-virtual {v1, v4, v2, v3}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object v15

    .line 447
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v4, v1

    check-cast v4, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 448
    sget-object v12, Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;->FAILURE:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x37f

    const/16 v17, 0x0

    .line 447
    invoke-static/range {v4 .. v17}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v1

    .line 451
    iget-object v2, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 452
    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    const-string v2, "just(state)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method private final onCancelVerificationSuccess()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    .line 437
    invoke-virtual {p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->refresh()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method private final onCheckPassword(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 144
    iget-object v2, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v4, v3

    check-cast v4, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    sget-object v10, Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;->IN_PROGRESS:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x7df

    const/16 v17, 0x0

    invoke-static/range {v4 .. v17}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 146
    new-instance v2, Lcom/squareup/protos/client/multipass/CheckPasswordRequest$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/multipass/CheckPasswordRequest$Builder;-><init>()V

    .line 147
    invoke-virtual {v2, v1}, Lcom/squareup/protos/client/multipass/CheckPasswordRequest$Builder;->password(Ljava/lang/String;)Lcom/squareup/protos/client/multipass/CheckPasswordRequest$Builder;

    move-result-object v2

    .line 148
    invoke-virtual {v2}, Lcom/squareup/protos/client/multipass/CheckPasswordRequest$Builder;->build()Lcom/squareup/protos/client/multipass/CheckPasswordRequest;

    move-result-object v2

    .line 150
    iget-object v3, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->multipassService:Lcom/squareup/api/multipassauth/MultipassService;

    const-string v4, "request"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v3, v2}, Lcom/squareup/api/multipassauth/MultipassService;->checkPassword(Lcom/squareup/protos/client/multipass/CheckPasswordRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v2

    .line 151
    invoke-virtual {v2}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v2

    .line 152
    new-instance v3, Lcom/squareup/banklinking/RealBankAccountSettings$onCheckPassword$1;

    invoke-direct {v3, v0, v1}, Lcom/squareup/banklinking/RealBankAccountSettings$onCheckPassword$1;-><init>(Lcom/squareup/banklinking/RealBankAccountSettings;Ljava/lang/String;)V

    check-cast v3, Lio/reactivex/functions/Function;

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v1

    const-string v2, "multipassService.checkPa\u2026it)\n          }\n        }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method private final onCheckPasswordFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/multipass/CheckPasswordResponse;",
            ">;)",
            "Lcom/squareup/banklinking/BankAccountSettings$State;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 374
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v2, Lcom/squareup/banklinking/impl/R$string;->invalid_password:I

    new-instance v3, Lcom/squareup/banklinking/RealBankAccountSettings$onCheckPasswordFailure$failureMessage$1;

    invoke-direct {v3, v0}, Lcom/squareup/banklinking/RealBankAccountSettings$onCheckPasswordFailure$failureMessage$1;-><init>(Lcom/squareup/banklinking/RealBankAccountSettings;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    move-object/from16 v4, p1

    invoke-virtual {v1, v4, v2, v3}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object v15

    .line 379
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v1, :cond_0

    .line 380
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->analytics:Lcom/squareup/banklinking/BankAccountSettingsAnalytics;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->logCheckPasswordFailure()V

    .line 383
    :cond_0
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    move-object v4, v1

    check-cast v4, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 384
    sget-object v10, Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;->FAILURE:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x3df

    const/16 v17, 0x0

    .line 383
    invoke-static/range {v4 .. v17}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v1

    .line 387
    iget-object v2, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-object v1
.end method

.method private final onCheckPasswordSuccess(Ljava/lang/String;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 16

    move-object/from16 v0, p0

    .line 364
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v2, v1

    check-cast v2, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v3, 0x0

    .line 365
    sget-object v8, Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;->SUCCESS:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x3dd

    const/4 v15, 0x0

    move-object/from16 v4, p1

    .line 364
    invoke-static/range {v2 .. v15}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v1

    .line 369
    iget-object v2, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-object v1
.end method

.method private final onConfirmBankAccount(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    .line 218
    new-instance v0, Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountRequest$Builder;-><init>()V

    .line 219
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountRequest$Builder;->confirmation_token(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountRequest$Builder;

    move-result-object p1

    .line 220
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountRequest$Builder;->build()Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountRequest;

    move-result-object p1

    .line 222
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->bankAccountService:Lcom/squareup/server/bankaccount/BankAccountService;

    const-string v1, "request"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/server/bankaccount/BankAccountService;->confirmBankAccount(Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    .line 223
    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 224
    new-instance v0, Lcom/squareup/banklinking/RealBankAccountSettings$onConfirmBankAccount$1;

    invoke-direct {v0, p0}, Lcom/squareup/banklinking/RealBankAccountSettings$onConfirmBankAccount$1;-><init>(Lcom/squareup/banklinking/RealBankAccountSettings;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "bankAccountService.confi\u2026it)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onConfirmBankAccountFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lio/reactivex/Single;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountResponse;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 482
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v2, Lcom/squareup/banklinking/impl/R$string;->confirm_bank_account_failed:I

    sget-object v3, Lcom/squareup/banklinking/RealBankAccountSettings$onConfirmBankAccountFailure$failureMessage$1;->INSTANCE:Lcom/squareup/banklinking/RealBankAccountSettings$onConfirmBankAccountFailure$failureMessage$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    move-object/from16 v4, p1

    invoke-virtual {v1, v4, v2, v3}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object v15

    .line 488
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v4, v1

    check-cast v4, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 489
    sget-object v14, Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;->FAILURE:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

    const/16 v16, 0x1ff

    const/16 v17, 0x0

    .line 488
    invoke-static/range {v4 .. v17}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v1

    .line 492
    iget-object v2, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 493
    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    const-string v2, "just(state)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method private final onConfirmBankAccountSuccess()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    .line 477
    invoke-virtual {p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->refresh()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method private final onGetBankAccounts()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    .line 131
    invoke-direct {p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->setLoadingState()V

    .line 133
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->bankAccountService:Lcom/squareup/server/bankaccount/BankAccountService;

    new-instance v1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsRequest;

    invoke-direct {v1}, Lcom/squareup/protos/client/bankaccount/GetBankAccountsRequest;-><init>()V

    invoke-interface {v0, v1}, Lcom/squareup/server/bankaccount/BankAccountService;->getBankAccounts(Lcom/squareup/protos/client/bankaccount/GetBankAccountsRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 135
    new-instance v1, Lcom/squareup/banklinking/RealBankAccountSettings$onGetBankAccounts$1;

    invoke-direct {v1, p0}, Lcom/squareup/banklinking/RealBankAccountSettings$onGetBankAccounts$1;-><init>(Lcom/squareup/banklinking/RealBankAccountSettings;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "bankAccountService.getBa\u2026it)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final onGetBankAccountsFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;",
            ">;)",
            "Lcom/squareup/banklinking/BankAccountSettings$State;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 350
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v2, Lcom/squareup/banklinking/impl/R$string;->load_bank_account_error_title:I

    sget-object v3, Lcom/squareup/banklinking/RealBankAccountSettings$onGetBankAccountsFailure$failureMessage$1;->INSTANCE:Lcom/squareup/banklinking/RealBankAccountSettings$onGetBankAccountsFailure$failureMessage$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    move-object/from16 v4, p1

    invoke-virtual {v1, v4, v2, v3}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object v15

    .line 353
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v4, v1

    check-cast v4, Lcom/squareup/banklinking/BankAccountSettings$State;

    .line 354
    sget-object v5, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->COULD_NOT_LOAD:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x3e6

    const/16 v17, 0x0

    .line 353
    invoke-static/range {v4 .. v17}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v1

    .line 359
    iget-object v2, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-object v1
.end method

.method private final onGetBankAccountsSuccess(Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 329
    iget-object v2, v1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->active_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    iget-object v2, v1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->recently_failed_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    :goto_0
    move-object v7, v2

    .line 331
    iget-object v2, v1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->pending_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-eqz v2, :cond_1

    goto :goto_1

    :cond_1
    iget-object v2, v1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->recently_failed_verification_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    :goto_1
    move-object v8, v2

    if-nez v7, :cond_2

    if-nez v8, :cond_2

    .line 333
    sget-object v2, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->NO_BANK_ACCOUNT:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    goto :goto_2

    .line 334
    :cond_2
    sget-object v2, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->HAS_BANK_ACCOUNT:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    :goto_2
    move-object v4, v2

    .line 336
    iget-object v2, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    move-object v3, v2

    check-cast v3, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v5, 0x0

    .line 340
    iget-object v1, v1, Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;->password_required_to_link:Ljava/lang/Boolean;

    const-string v2, "response.password_required_to_link"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x3e2

    const/16 v16, 0x0

    .line 336
    invoke-static/range {v3 .. v16}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v1

    .line 343
    iget-object v2, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-object v1
.end method

.method private final onGetDirectDebitInfo(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;",
            ">;"
        }
    .end annotation

    .line 246
    new-instance v0, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;-><init>()V

    .line 247
    invoke-virtual {v0, p1}, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->sort_code(Ljava/lang/String;)Lcom/squareup/protos/bank_accounts/GbAccount$Builder;

    move-result-object p1

    .line 248
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/GbAccount$Builder;->build()Lcom/squareup/protos/bank_accounts/GbAccount;

    move-result-object p1

    .line 249
    new-instance v0, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;-><init>()V

    .line 250
    invoke-virtual {v0, p1}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->gb_account(Lcom/squareup/protos/bank_accounts/GbAccount;)Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;

    move-result-object p1

    .line 251
    invoke-virtual {p1}, Lcom/squareup/protos/bank_accounts/CountrySpecificDetail$Builder;->build()Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;

    move-result-object p1

    .line 252
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->unique:Lcom/squareup/util/Unique;

    invoke-interface {v0}, Lcom/squareup/util/Unique;->generate()Ljava/lang/String;

    move-result-object v0

    .line 253
    new-instance v1, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;-><init>()V

    .line 254
    iget-object v2, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v2

    const-string v3, "settings.userSettings"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;->unit_token(Ljava/lang/String;)Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;

    move-result-object v1

    .line 255
    invoke-virtual {v1, v0}, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;->idempotence_key(Ljava/lang/String;)Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;

    move-result-object v1

    .line 256
    invoke-virtual {v1, p1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;->country_specific_detail(Lcom/squareup/protos/bank_accounts/CountrySpecificDetail;)Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;

    move-result-object p1

    .line 257
    invoke-virtual {p1}, Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest$Builder;->build()Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest;

    move-result-object p1

    .line 259
    iget-object v1, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->bankAccountService:Lcom/squareup/server/bankaccount/BankAccountService;

    const-string v2, "request"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, p1}, Lcom/squareup/server/bankaccount/BankAccountService;->getDirectDebitInfo(Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest;)Lcom/squareup/server/bankaccount/BankAccountService$GetDirectDebitInfoStandardResponse;

    move-result-object p1

    .line 260
    invoke-virtual {p1}, Lcom/squareup/server/bankaccount/BankAccountService$GetDirectDebitInfoStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 261
    new-instance v1, Lcom/squareup/banklinking/RealBankAccountSettings$onGetDirectDebitInfo$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/banklinking/RealBankAccountSettings$onGetDirectDebitInfo$1;-><init>(Lcom/squareup/banklinking/RealBankAccountSettings;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "bankAccountService.getDi\u2026it)\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final onGetDirectDebitInfoFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;",
            ">;)",
            "Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;"
        }
    .end annotation

    .line 286
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v1, Lcom/squareup/banklinking/impl/R$string;->get_direct_debit_info_failure_title:I

    new-instance v2, Lcom/squareup/banklinking/RealBankAccountSettings$onGetDirectDebitInfoFailure$failureMessage$1;

    invoke-direct {v2, p0}, Lcom/squareup/banklinking/RealBankAccountSettings$onGetDirectDebitInfoFailure$failureMessage$1;-><init>(Lcom/squareup/banklinking/RealBankAccountSettings;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 291
    new-instance v0, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoFailure;

    .line 292
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 293
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    .line 291
    invoke-direct {v0, v1, p1}, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoFailure;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;

    return-object v0
.end method

.method private final onGetDirectDebitInfoSuccess(Ljava/lang/String;Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;)Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;
    .locals 8

    .line 273
    new-instance v7, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;

    .line 275
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p2, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_first_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p2, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_last_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 276
    iget-object v3, p2, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->account_owner_email:Ljava/lang/String;

    const-string v0, "response.account_owner_email"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 277
    iget-object v0, p2, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->business_address:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    iget-object v4, v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;->recipient:Ljava/lang/String;

    const-string v0, "response.business_address.recipient"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 278
    iget-object v0, p2, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->business_address:Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;

    iget-object v0, v0, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse$Address;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    sget-object v1, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    invoke-static {v0, v1}, Lcom/squareup/address/GlobalAddressUtilKt;->formatMultilineAddress(Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/CountryCode;)Ljava/lang/String;

    move-result-object v5

    .line 279
    iget-object p2, p2, Lcom/squareup/protos/deposits/GetDirectDebitInfoResponse;->direct_debit_reference_id:Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    move-object v0, v7

    move-object v1, p1

    .line 273
    invoke-direct/range {v0 .. v6}, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo$DirectDebitInfoSuccess;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v7, Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;

    return-object v7
.end method

.method private final onGetLatestBankAccount()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    .line 118
    invoke-direct {p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->setLoadingState()V

    .line 120
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->bankAccountService:Lcom/squareup/server/bankaccount/BankAccountService;

    new-instance v1, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountRequest;

    invoke-direct {v1}, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountRequest;-><init>()V

    invoke-interface {v0, v1}, Lcom/squareup/server/bankaccount/BankAccountService;->getLatestBankAccount(Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountRequest;)Lcom/squareup/server/bankaccount/BankAccountService$BankAccountStandardResponse;

    move-result-object v0

    .line 121
    invoke-virtual {v0}, Lcom/squareup/server/bankaccount/BankAccountService$BankAccountStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 122
    new-instance v1, Lcom/squareup/banklinking/RealBankAccountSettings$onGetLatestBankAccount$1;

    invoke-direct {v1, p0}, Lcom/squareup/banklinking/RealBankAccountSettings$onGetLatestBankAccount$1;-><init>(Lcom/squareup/banklinking/RealBankAccountSettings;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "bankAccountService.getLa\u2026it)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final onGetLatestBankAccountFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;",
            ">;)",
            "Lcom/squareup/banklinking/BankAccountSettings$State;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 315
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v2, Lcom/squareup/banklinking/impl/R$string;->load_bank_account_error_title:I

    sget-object v3, Lcom/squareup/banklinking/RealBankAccountSettings$onGetLatestBankAccountFailure$failureMessage$1;->INSTANCE:Lcom/squareup/banklinking/RealBankAccountSettings$onGetLatestBankAccountFailure$failureMessage$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    move-object/from16 v4, p1

    invoke-virtual {v1, v4, v2, v3}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object v15

    .line 318
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v4, v1

    check-cast v4, Lcom/squareup/banklinking/BankAccountSettings$State;

    .line 319
    sget-object v5, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->COULD_NOT_LOAD:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x3e6

    const/16 v17, 0x0

    .line 318
    invoke-static/range {v4 .. v17}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v1

    .line 324
    iget-object v2, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-object v1
.end method

.method private final onGetLatestBankAccountSuccess(Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 14

    .line 298
    iget-object v5, p1, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->latest_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    if-nez v5, :cond_0

    .line 300
    sget-object v0, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->NO_BANK_ACCOUNT:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->HAS_BANK_ACCOUNT:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    :goto_0
    move-object v1, v0

    .line 301
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v0, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 305
    iget-object p1, p1, Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;->requires_password:Ljava/lang/Boolean;

    const-string v3, "response.requires_password"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0x3e2

    const/4 v13, 0x0

    .line 301
    invoke-static/range {v0 .. v13}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object p1

    .line 308
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-object p1
.end method

.method private final onLinkBankAccount(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;ZLjava/lang/String;)Lio/reactivex/Single;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bankaccount/BankAccountDetails;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 165
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 166
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v3, v2

    check-cast v3, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 167
    sget-object v10, Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;->IN_PROGRESS:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x7bf

    const/16 v16, 0x0

    .line 166
    invoke-static/range {v3 .. v16}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v2

    .line 165
    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 171
    new-instance v1, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;-><init>()V

    move-object/from16 v2, p1

    .line 172
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;->bank_account_details(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;

    move-result-object v1

    .line 173
    iget-object v2, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v2, Lcom/squareup/banklinking/BankAccountSettings$State;

    iget-object v2, v2, Lcom/squareup/banklinking/BankAccountSettings$State;->password:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;->password(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;

    move-result-object v1

    move-object/from16 v2, p3

    .line 174
    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;->idempotence_key(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;

    move-result-object v1

    .line 175
    invoke-virtual {v1}, Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest$Builder;->build()Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest;

    move-result-object v1

    .line 177
    iget-object v2, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->bankAccountService:Lcom/squareup/server/bankaccount/BankAccountService;

    const-string v3, "request"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Lcom/squareup/server/bankaccount/BankAccountService;->linkBankAccount(Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest;)Lcom/squareup/server/bankaccount/BankAccountService$BankAccountStandardResponse;

    move-result-object v1

    .line 178
    invoke-virtual {v1}, Lcom/squareup/server/bankaccount/BankAccountService$BankAccountStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v1

    .line 179
    new-instance v2, Lcom/squareup/banklinking/RealBankAccountSettings$onLinkBankAccount$1;

    move/from16 v3, p2

    invoke-direct {v2, v0, v3}, Lcom/squareup/banklinking/RealBankAccountSettings$onLinkBankAccount$1;-><init>(Lcom/squareup/banklinking/RealBankAccountSettings;Z)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v1

    const-string v2, "bankAccountService.linkB\u2026it)\n          }\n        }"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method private final onLinkBankAccountFailure(ZLcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lio/reactivex/Single;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 411
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v2, Lcom/squareup/banklinking/impl/R$string;->bank_account_linking_failed:I

    sget-object v3, Lcom/squareup/banklinking/RealBankAccountSettings$onLinkBankAccountFailure$failureMessage$1;->INSTANCE:Lcom/squareup/banklinking/RealBankAccountSettings$onLinkBankAccountFailure$failureMessage$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    move-object/from16 v4, p2

    invoke-virtual {v1, v4, v2, v3}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object v15

    .line 416
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v1, :cond_4

    if-eqz p1, :cond_0

    .line 418
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->analytics:Lcom/squareup/banklinking/BankAccountSettingsAnalytics;

    invoke-virtual {v15}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->logOnboardingAddBankAccountFailure(Ljava/lang/String;)V

    goto :goto_0

    .line 419
    :cond_0
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v1, Lcom/squareup/banklinking/BankAccountSettings$State;

    iget-object v1, v1, Lcom/squareup/banklinking/BankAccountSettings$State;->password:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->analytics:Lcom/squareup/banklinking/BankAccountSettingsAnalytics;

    invoke-virtual {v15}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->logAddBankAccountFailure(Ljava/lang/String;)V

    goto :goto_0

    .line 420
    :cond_2
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->analytics:Lcom/squareup/banklinking/BankAccountSettingsAnalytics;

    invoke-virtual {v15}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->logChangeBankAccountFailure(Ljava/lang/String;)V

    .line 423
    :goto_0
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    move-object v4, v1

    check-cast v4, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 424
    sget-object v11, Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;->FAILURE:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x3bf

    const/16 v17, 0x0

    .line 423
    invoke-static/range {v4 .. v17}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v1

    goto :goto_1

    .line 428
    :cond_4
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    move-object v4, v1

    check-cast v4, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 429
    sget-object v11, Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;->WARNING:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x3bf

    const/16 v17, 0x0

    .line 428
    invoke-static/range {v4 .. v17}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v1

    .line 433
    :goto_1
    iget-object v2, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 434
    invoke-static {v1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v1

    const-string v2, "just(state)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v1
.end method

.method private final onLinkBankAccountSuccess(Z)Lio/reactivex/Single;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    if-eqz p1, :cond_0

    .line 393
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->analytics:Lcom/squareup/banklinking/BankAccountSettingsAnalytics;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->logOnboardingAddBankAccountSuccess()V

    goto :goto_0

    .line 394
    :cond_0
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    check-cast v1, Lcom/squareup/banklinking/BankAccountSettings$State;

    iget-object v1, v1, Lcom/squareup/banklinking/BankAccountSettings$State;->password:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->analytics:Lcom/squareup/banklinking/BankAccountSettingsAnalytics;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->logAddBankAccountSuccess()V

    goto :goto_0

    .line 395
    :cond_2
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->analytics:Lcom/squareup/banklinking/BankAccountSettingsAnalytics;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettingsAnalytics;->logChangeBankAccountSuccess()V

    .line 398
    :goto_0
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 399
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    move-object v3, v2

    check-cast v3, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 400
    sget-object v10, Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;->SUCCESS:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x3bf

    const/16 v16, 0x0

    .line 399
    invoke-static/range {v3 .. v16}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v2

    .line 398
    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 404
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->refresh()Lio/reactivex/Single;

    move-result-object v1

    return-object v1
.end method

.method private final onResendVerificationEmail()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    .line 203
    new-instance v0, Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailRequest$Builder;-><init>()V

    .line 204
    iget-object v1, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    check-cast v1, Lcom/squareup/banklinking/BankAccountSettings$State;

    invoke-virtual {v1}, Lcom/squareup/banklinking/BankAccountSettings$State;->bankAccount()Lcom/squareup/protos/client/bankaccount/BankAccount;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    iget-object v1, v1, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_account_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailRequest$Builder;->bank_account_token(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailRequest$Builder;

    move-result-object v0

    .line 205
    invoke-virtual {v0}, Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailRequest$Builder;->build()Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailRequest;

    move-result-object v0

    .line 207
    iget-object v1, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->bankAccountService:Lcom/squareup/server/bankaccount/BankAccountService;

    const-string v2, "request"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/server/bankaccount/BankAccountService;->resendVerificationEmail(Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 209
    new-instance v1, Lcom/squareup/banklinking/RealBankAccountSettings$onResendVerificationEmail$1;

    invoke-direct {v1, p0}, Lcom/squareup/banklinking/RealBankAccountSettings$onResendVerificationEmail$1;-><init>(Lcom/squareup/banklinking/RealBankAccountSettings;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "bankAccountService.resen\u2026it)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final onResendVerificationEmailFailure(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailResponse;",
            ">;)",
            "Lcom/squareup/banklinking/BankAccountSettings$State;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 465
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    sget v2, Lcom/squareup/common/strings/R$string;->email_resending_failed:I

    sget-object v3, Lcom/squareup/banklinking/RealBankAccountSettings$onResendVerificationEmailFailure$failureMessage$1;->INSTANCE:Lcom/squareup/banklinking/RealBankAccountSettings$onResendVerificationEmailFailure$failureMessage$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    move-object/from16 v4, p1

    invoke-virtual {v1, v4, v2, v3}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object v15

    .line 469
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v4, v1

    check-cast v4, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 470
    sget-object v13, Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;->FAILURE:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    const/4 v14, 0x0

    const/16 v16, 0x2ff

    const/16 v17, 0x0

    .line 469
    invoke-static/range {v4 .. v17}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v1

    .line 473
    iget-object v2, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v2, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-object v1
.end method

.method private final onResendVerificationEmailSuccess()Lcom/squareup/banklinking/BankAccountSettings$State;
    .locals 15

    .line 456
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v1, v0

    check-cast v1, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    sget-object v10, Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;->SUCCESS:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x6ff

    const/4 v14, 0x0

    invoke-static/range {v1 .. v14}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v0

    .line 457
    iget-object v1, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-object v0
.end method

.method private final onResetRequestState()V
    .locals 17

    move-object/from16 v0, p0

    .line 233
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 234
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    move-object v3, v2

    check-cast v3, Lcom/squareup/banklinking/BankAccountSettings$State;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 235
    sget-object v9, Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;->INITIAL:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    .line 236
    sget-object v10, Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;->INITIAL:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    .line 237
    sget-object v11, Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;->INITIAL:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

    .line 238
    sget-object v12, Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;->INITIAL:Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;

    .line 239
    sget-object v13, Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;->INITIAL:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

    const/4 v14, 0x0

    const/16 v15, 0x1f

    const/16 v16, 0x0

    .line 234
    invoke-static/range {v3 .. v16}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v2

    .line 233
    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method private final setLoadingState()V
    .locals 17

    move-object/from16 v0, p0

    .line 505
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v2, "_state.value!!"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/banklinking/BankAccountSettings$State;

    .line 506
    iget-object v1, v3, Lcom/squareup/banklinking/BankAccountSettings$State;->latestBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    sget-object v2, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->LOADING:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    if-eq v1, v2, :cond_1

    .line 507
    iget-object v1, v0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v4, Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;->LOADING:Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x7fe

    const/16 v16, 0x0

    invoke-static/range {v3 .. v16}, Lcom/squareup/banklinking/BankAccountSettings$State;->copy$default(Lcom/squareup/banklinking/BankAccountSettings$State;Lcom/squareup/banklinking/BankAccountSettings$LatestBankAccountState;Ljava/lang/String;ZLcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;Lcom/squareup/banklinking/BankAccountSettings$ResendEmailState;Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;Lcom/squareup/receiving/FailureMessage;ILjava/lang/Object;)Lcom/squareup/banklinking/BankAccountSettings$State;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method private final shouldResetRequestState()Z
    .locals 3

    .line 497
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v1, "_state.value!!"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/banklinking/BankAccountSettings$State;

    .line 501
    iget-object v1, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->checkPasswordState:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    sget-object v2, Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;->FAILURE:Lcom/squareup/banklinking/BankAccountSettings$CheckPasswordState;

    if-eq v1, v2, :cond_2

    iget-object v1, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->linkBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    sget-object v2, Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;->WARNING:Lcom/squareup/banklinking/BankAccountSettings$LinkBankAccountState;

    if-eq v1, v2, :cond_2

    iget-object v1, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->cancelVerificationState:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

    sget-object v2, Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;->FAILURE:Lcom/squareup/banklinking/BankAccountSettings$CancelVerificationState;

    if-eq v1, v2, :cond_2

    iget-object v0, v0, Lcom/squareup/banklinking/BankAccountSettings$State;->confirmBankAccountState:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

    sget-object v1, Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;->FAILURE:Lcom/squareup/banklinking/BankAccountSettings$ConfirmBankAccountState;

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method public cancelVerification()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    .line 106
    invoke-direct {p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->onCancelVerification()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public checkPassword(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    const-string v0, "password"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->onCheckPassword(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public confirmBankAccount(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    const-string v0, "confirmationToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->onConfirmBankAccount(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getDirectDebitInfo(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$DirectDebitInfo;",
            ">;"
        }
    .end annotation

    const-string v0, "sortCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    invoke-direct {p0, p1}, Lcom/squareup/banklinking/RealBankAccountSettings;->onGetDirectDebitInfo(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public linkBankAccount(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;ZLjava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bankaccount/BankAccountDetails;",
            "Z",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    const-string v0, "bankAccountDetails"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/banklinking/RealBankAccountSettings;->onLinkBankAccount(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;ZLjava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    invoke-virtual {p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->refresh()Lio/reactivex/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "refresh().subscribe()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/banklinking/RealBankAccountSettings$onEnterScope$1;

    invoke-direct {v1, p0}, Lcom/squareup/banklinking/RealBankAccountSettings$onEnterScope$1;-><init>(Lcom/squareup/banklinking/RealBankAccountSettings;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "_state.subscribe {\n     \u2026uestState()\n      }\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public refresh()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BANK_LINKING_WITH_TWO_ACCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    invoke-direct {p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->onGetBankAccounts()Lio/reactivex/Single;

    move-result-object v0

    goto :goto_0

    .line 95
    :cond_0
    invoke-direct {p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->onGetLatestBankAccount()Lio/reactivex/Single;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public resendVerificationEmail()Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    .line 108
    invoke-direct {p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->onResendVerificationEmail()Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public resetRequestState()V
    .locals 0

    .line 113
    invoke-direct {p0}, Lcom/squareup/banklinking/RealBankAccountSettings;->onResetRequestState()V

    return-void
.end method

.method public state()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/banklinking/BankAccountSettings$State;",
            ">;"
        }
    .end annotation

    .line 90
    iget-object v0, p0, Lcom/squareup/banklinking/RealBankAccountSettings;->_state:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    return-object v0
.end method
