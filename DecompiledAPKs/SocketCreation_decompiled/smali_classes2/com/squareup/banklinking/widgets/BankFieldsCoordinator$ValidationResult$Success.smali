.class public final Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Success;
.super Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;
.source "BankFieldsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Success"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Success;",
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;",
        "()V",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Success;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Success;

    invoke-direct {v0}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Success;-><init>()V

    sput-object v0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Success;->INSTANCE:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Success;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 47
    sget v0, Lcom/squareup/vectoricons/R$drawable;->icon_check:I

    sget v1, Lcom/squareup/banklinking/R$color;->bank_fields_icon_success:I

    const/4 v2, 0x0

    .line 46
    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;-><init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
