.class public final Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Failure;
.super Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;
.source "BankFieldsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Failure"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Failure;",
        "Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;",
        "()V",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Failure;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Failure;

    invoke-direct {v0}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Failure;-><init>()V

    sput-object v0, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Failure;->INSTANCE:Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult$Failure;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .line 43
    sget v0, Lcom/squareup/vectoricons/R$drawable;->icon_x:I

    sget v1, Lcom/squareup/banklinking/R$color;->bank_fields_icon_failure:I

    const/4 v2, 0x0

    .line 42
    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/banklinking/widgets/BankFieldsCoordinator$ValidationResult;-><init>(IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
