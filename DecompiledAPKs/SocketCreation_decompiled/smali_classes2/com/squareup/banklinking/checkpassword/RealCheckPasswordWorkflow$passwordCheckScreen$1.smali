.class final Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$passwordCheckScreen$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCheckPasswordWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->passwordCheckScreen(Lcom/squareup/banklinking/checkpassword/CheckPasswordState;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "password",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$passwordCheckScreen$1;->this$0:Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;

    iput-object p2, p0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$passwordCheckScreen$1;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$passwordCheckScreen$1;->invoke(Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/String;)V
    .locals 3

    const-string v0, "password"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$passwordCheckScreen$1;->this$0:Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;

    invoke-static {v0}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;->access$getAnalytics$p(Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Settings Bank Account: Edit Bank Account Password"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$passwordCheckScreen$1;->$sink:Lcom/squareup/workflow/Sink;

    new-instance v1, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$CheckPassword;

    invoke-direct {v1, p1}, Lcom/squareup/banklinking/checkpassword/RealCheckPasswordWorkflow$Action$CheckPassword;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
