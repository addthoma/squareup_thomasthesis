.class final Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$4;
.super Lkotlin/jvm/internal/Lambda;
.source "RealCheckBankAccountInfoWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->checkBankAccountInfoScreen(Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoState;Lcom/squareup/workflow/Sink;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $props:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;

.field final synthetic $sink:Lcom/squareup/workflow/Sink;

.field final synthetic this$0:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;Lcom/squareup/workflow/Sink;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$4;->this$0:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;

    iput-object p2, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$4;->$props:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;

    iput-object p3, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$4;->$sink:Lcom/squareup/workflow/Sink;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 53
    invoke-virtual {p0}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$4;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 2

    .line 176
    iget-object v0, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$4;->this$0:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;

    iget-object v1, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$4;->$props:Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;

    invoke-virtual {v1}, Lcom/squareup/banklinking/checkbankinfo/CheckBankAccountInfoProps;->getRequiresPassword()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;->access$logBankLinkingCancelEvent(Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow;Z)V

    .line 177
    iget-object v0, p0, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$checkBankAccountInfoScreen$4;->$sink:Lcom/squareup/workflow/Sink;

    sget-object v1, Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CancelBankLinking;->INSTANCE:Lcom/squareup/banklinking/checkbankinfo/RealCheckBankAccountInfoWorkflow$Action$CancelBankLinking;

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
