.class public abstract Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action;
.super Ljava/lang/Object;
.source "RealFetchBankAccountWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$FinishWithBankSettingsState;,
        Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$ShowErrorMessage;,
        Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$RefetchBankAccount;,
        Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$CancelFetchBankAccount;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0004\u0008\t\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0005\u001a\u00020\u0006*\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0007H\u0016\u0082\u0001\u0004\u000c\r\u000e\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;",
        "Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;",
        "()V",
        "apply",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "CancelFetchBankAccount",
        "FinishWithBankSettingsState",
        "RefetchBankAccount",
        "ShowErrorMessage",
        "Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$FinishWithBankSettingsState;",
        "Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$ShowErrorMessage;",
        "Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$RefetchBankAccount;",
        "Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$CancelFetchBankAccount;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 45
    invoke-direct {p0}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;",
            ">;)",
            "Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Implement Updater.apply"
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-static {p0, p1}, Lcom/squareup/workflow/WorkflowAction$DefaultImpls;->apply(Lcom/squareup/workflow/WorkflowAction;Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;

    return-object p1
.end method

.method public bridge synthetic apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Object;
    .locals 0

    .line 45
    invoke-virtual {p0, p1}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action;->apply(Lcom/squareup/workflow/WorkflowAction$Mutator;)Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;

    move-result-object p1

    return-object p1
.end method

.method public apply(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/banklinking/fetchbank/FetchBankAccountState;",
            "-",
            "Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$this$apply"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    instance-of v0, p0, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$FinishWithBankSettingsState;

    if-eqz v0, :cond_0

    .line 61
    new-instance v0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountSucceeded;

    .line 62
    move-object v1, p0

    check-cast v1, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$FinishWithBankSettingsState;

    invoke-virtual {v1}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$FinishWithBankSettingsState;->getRequiresPassword()Z

    move-result v2

    invoke-virtual {v1}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$FinishWithBankSettingsState;->getPrimaryInstitutionNumber()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$FinishWithBankSettingsState;->getVerificationState()Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    move-result-object v1

    .line 61
    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountSucceeded;-><init>(ZLjava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)V

    .line 60
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    goto :goto_0

    .line 65
    :cond_0
    instance-of v0, p0, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$ShowErrorMessage;

    if-eqz v0, :cond_1

    .line 66
    new-instance v0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountState$CouldNotFetchBankAccount;

    .line 67
    move-object v1, p0

    check-cast v1, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$ShowErrorMessage;

    invoke-virtual {v1}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$ShowErrorMessage;->getFailureMessage()Lcom/squareup/receiving/FailureMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 68
    invoke-virtual {v1}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$ShowErrorMessage;->getFailureMessage()Lcom/squareup/receiving/FailureMessage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v3

    .line 69
    invoke-virtual {v1}, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$ShowErrorMessage;->getFailureMessage()Lcom/squareup/receiving/FailureMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/receiving/FailureMessage;->getRetryable()Z

    move-result v1

    .line 66
    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/banklinking/fetchbank/FetchBankAccountState$CouldNotFetchBankAccount;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    :cond_1
    sget-object v0, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$RefetchBankAccount;->INSTANCE:Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$RefetchBankAccount;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    sget-object v0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountState$FetchingBankAccount;->INSTANCE:Lcom/squareup/banklinking/fetchbank/FetchBankAccountState$FetchingBankAccount;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    goto :goto_0

    .line 75
    :cond_2
    sget-object v0, Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$CancelFetchBankAccount;->INSTANCE:Lcom/squareup/banklinking/fetchbank/RealFetchBankAccountWorkflow$Action$CancelFetchBankAccount;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountCanceled;->INSTANCE:Lcom/squareup/banklinking/fetchbank/FetchBankAccountOutput$FetchBankAccountCanceled;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setOutput(Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
