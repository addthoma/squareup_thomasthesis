.class public Lcom/squareup/cashmanagement/CashReportRenderer;
.super Ljava/lang/Object;
.source "CashReportRenderer.java"


# instance fields
.field private final builder:Lcom/squareup/print/ThermalBitmapBuilder;


# direct methods
.method public constructor <init>(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/cashmanagement/CashReportRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method

.method private renderPaidInOutSection(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/cashmanagement/PaidInOutSection;)V
    .locals 6

    .line 56
    iget-object v0, p2, Lcom/squareup/cashmanagement/PaidInOutSection;->paidInOutEvents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 57
    iget-object v0, p2, Lcom/squareup/cashmanagement/PaidInOutSection;->headerText:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cashmanagement/CashReportRenderer;->renderSectionHeader(Lcom/squareup/print/ThermalBitmapBuilder;Ljava/lang/String;)V

    .line 58
    iget-object v0, p2, Lcom/squareup/cashmanagement/PaidInOutSection;->paidInOutEvents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cashmanagement/PaidInOutSection$PaidInOutEvent;

    .line 59
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 60
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    .line 61
    iget-object v5, v1, Lcom/squareup/cashmanagement/PaidInOutSection$PaidInOutEvent;->label:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, v1, Lcom/squareup/cashmanagement/PaidInOutSection$PaidInOutEvent;->description:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/squareup/print/util/PrintRendererUtils;->appendAsLinesIfNotBlank(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)V

    .line 65
    iget-object v1, v1, Lcom/squareup/cashmanagement/PaidInOutSection$PaidInOutEvent;->amount:Ljava/lang/String;

    invoke-virtual {p1, v2, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->multilineTextAndAmount(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 67
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 68
    iget-object v0, p2, Lcom/squareup/cashmanagement/PaidInOutSection;->totalPaidInOut:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v0, v0, Lcom/squareup/print/payload/LabelAmountPair;->label:Ljava/lang/String;

    iget-object p2, p2, Lcom/squareup/cashmanagement/PaidInOutSection;->totalPaidInOut:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object p2, p2, Lcom/squareup/print/payload/LabelAmountPair;->amount:Ljava/lang/String;

    invoke-virtual {p1, v0, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingTextMedium(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_1
    return-void
.end method

.method private renderReportHeader(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;)V
    .locals 1

    .line 26
    iget-object v0, p2, Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;->headerText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthHeadlineText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 27
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 28
    iget-object p2, p2, Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;->dateText:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 29
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method

.method private renderSectionHeader(Lcom/squareup/print/ThermalBitmapBuilder;Ljava/lang/String;)V
    .locals 0

    .line 47
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 48
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 49
    invoke-virtual {p1, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthMediumText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 50
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 51
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method

.method private renderSummarySection(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/cashmanagement/DrawerSummarySection;)V
    .locals 3

    .line 34
    iget-object v0, p2, Lcom/squareup/cashmanagement/DrawerSummarySection;->headerText:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/squareup/cashmanagement/CashReportRenderer;->renderSectionHeader(Lcom/squareup/print/ThermalBitmapBuilder;Ljava/lang/String;)V

    .line 35
    iget-object v0, p2, Lcom/squareup/cashmanagement/DrawerSummarySection;->normalSummaryRows:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/payload/LabelAmountPair;

    .line 36
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 37
    iget-object v2, v1, Lcom/squareup/print/payload/LabelAmountPair;->label:Ljava/lang/String;

    iget-object v1, v1, Lcom/squareup/print/payload/LabelAmountPair;->amount:Ljava/lang/String;

    invoke-virtual {p1, v2, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 39
    :cond_0
    iget-object p2, p2, Lcom/squareup/cashmanagement/DrawerSummarySection;->mediumSummaryRows:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/payload/LabelAmountPair;

    .line 40
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 41
    iget-object v1, v0, Lcom/squareup/print/payload/LabelAmountPair;->label:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/print/payload/LabelAmountPair;->amount:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->twoColumnsLeftExpandingTextMedium(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_1

    .line 43
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->largeSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method


# virtual methods
.method public renderBitmap(Lcom/squareup/cashmanagement/CashReportPayload;)Landroid/graphics/Bitmap;
    .locals 2

    .line 18
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashReportRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    iget-object v1, p1, Lcom/squareup/cashmanagement/CashReportPayload;->headerSection:Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;

    invoke-direct {p0, v0, v1}, Lcom/squareup/cashmanagement/CashReportRenderer;->renderReportHeader(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/cashmanagement/CashReportPayload$HeaderSection;)V

    .line 19
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashReportRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    iget-object v1, p1, Lcom/squareup/cashmanagement/CashReportPayload;->summarySection:Lcom/squareup/cashmanagement/DrawerSummarySection;

    invoke-direct {p0, v0, v1}, Lcom/squareup/cashmanagement/CashReportRenderer;->renderSummarySection(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/cashmanagement/DrawerSummarySection;)V

    .line 20
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashReportRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    iget-object p1, p1, Lcom/squareup/cashmanagement/CashReportPayload;->paidInOutSection:Lcom/squareup/cashmanagement/PaidInOutSection;

    invoke-direct {p0, v0, p1}, Lcom/squareup/cashmanagement/CashReportRenderer;->renderPaidInOutSection(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/cashmanagement/PaidInOutSection;)V

    .line 21
    iget-object p1, p0, Lcom/squareup/cashmanagement/CashReportRenderer;->builder:Lcom/squareup/print/ThermalBitmapBuilder;

    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->render()Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method
