.class public final Lcom/squareup/cashmanagement/AutoPrintReportSetting_Factory;
.super Ljava/lang/Object;
.source "AutoPrintReportSetting_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cashmanagement/AutoPrintReportSetting;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/BooleanLocalSetting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/BooleanLocalSetting;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/cashmanagement/AutoPrintReportSetting_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/cashmanagement/AutoPrintReportSetting_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/squareup/cashmanagement/AutoPrintReportSetting_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cashmanagement/AutoPrintReportSetting_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/BooleanLocalSetting;",
            ">;)",
            "Lcom/squareup/cashmanagement/AutoPrintReportSetting_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/cashmanagement/AutoPrintReportSetting_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cashmanagement/AutoPrintReportSetting_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/settings/BooleanLocalSetting;)Lcom/squareup/cashmanagement/AutoPrintReportSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Lcom/squareup/settings/BooleanLocalSetting;",
            ")",
            "Lcom/squareup/cashmanagement/AutoPrintReportSetting;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/cashmanagement/AutoPrintReportSetting;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cashmanagement/AutoPrintReportSetting;-><init>(Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/settings/BooleanLocalSetting;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cashmanagement/AutoPrintReportSetting;
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/cashmanagement/AutoPrintReportSetting_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/cashmanagement/AutoPrintReportSetting_Factory;->arg1Provider:Ljavax/inject/Provider;

    iget-object v2, p0, Lcom/squareup/cashmanagement/AutoPrintReportSetting_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/BooleanLocalSetting;

    invoke-static {v0, v1, v2}, Lcom/squareup/cashmanagement/AutoPrintReportSetting_Factory;->newInstance(Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/settings/BooleanLocalSetting;)Lcom/squareup/cashmanagement/AutoPrintReportSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/AutoPrintReportSetting_Factory;->get()Lcom/squareup/cashmanagement/AutoPrintReportSetting;

    move-result-object v0

    return-object v0
.end method
