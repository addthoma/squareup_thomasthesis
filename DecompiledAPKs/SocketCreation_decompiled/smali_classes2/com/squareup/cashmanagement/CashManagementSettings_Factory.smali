.class public final Lcom/squareup/cashmanagement/CashManagementSettings_Factory;
.super Ljava/lang/Object;
.source "CashManagementSettings_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cashmanagement/CashManagementSettings;",
        ">;"
    }
.end annotation


# instance fields
.field private final autoEmailReportSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/AutoEmailReportSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final autoPrintReportSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/AutoPrintReportSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final cashManagementEnabledSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementEnabledSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final defaultStartingCashProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/DefaultStartingCashSetting;",
            ">;"
        }
    .end annotation
.end field

.field private final emailRecipientSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementEnabledSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/DefaultStartingCashSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/AutoEmailReportSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/AutoPrintReportSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;->cashManagementEnabledSettingProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;->defaultStartingCashProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;->autoEmailReportSettingProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p5, p0, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;->autoPrintReportSettingProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p6, p0, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;->emailRecipientSettingProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cashmanagement/CashManagementSettings_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashManagementEnabledSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/DefaultStartingCashSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/AutoEmailReportSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/AutoPrintReportSetting;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;",
            ">;)",
            "Lcom/squareup/cashmanagement/CashManagementSettings_Factory;"
        }
    .end annotation

    .line 50
    new-instance v7, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/cashmanagement/CashManagementEnabledSetting;Lcom/squareup/cashmanagement/DefaultStartingCashSetting;Lcom/squareup/cashmanagement/AutoEmailReportSetting;Lcom/squareup/cashmanagement/AutoPrintReportSetting;Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;)Lcom/squareup/cashmanagement/CashManagementSettings;
    .locals 8

    .line 58
    new-instance v7, Lcom/squareup/cashmanagement/CashManagementSettings;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cashmanagement/CashManagementSettings;-><init>(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/cashmanagement/CashManagementEnabledSetting;Lcom/squareup/cashmanagement/DefaultStartingCashSetting;Lcom/squareup/cashmanagement/AutoEmailReportSetting;Lcom/squareup/cashmanagement/AutoPrintReportSetting;Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/cashmanagement/CashManagementSettings;
    .locals 7

    .line 41
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;->cashManagementEnabledSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/cashmanagement/CashManagementEnabledSetting;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;->defaultStartingCashProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cashmanagement/DefaultStartingCashSetting;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;->autoEmailReportSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cashmanagement/AutoEmailReportSetting;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;->autoPrintReportSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/cashmanagement/AutoPrintReportSetting;

    iget-object v0, p0, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;->emailRecipientSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;

    invoke-static/range {v1 .. v6}, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;->newInstance(Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/cashmanagement/CashManagementEnabledSetting;Lcom/squareup/cashmanagement/DefaultStartingCashSetting;Lcom/squareup/cashmanagement/AutoEmailReportSetting;Lcom/squareup/cashmanagement/AutoPrintReportSetting;Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;)Lcom/squareup/cashmanagement/CashManagementSettings;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/CashManagementSettings_Factory;->get()Lcom/squareup/cashmanagement/CashManagementSettings;

    move-result-object v0

    return-object v0
.end method
