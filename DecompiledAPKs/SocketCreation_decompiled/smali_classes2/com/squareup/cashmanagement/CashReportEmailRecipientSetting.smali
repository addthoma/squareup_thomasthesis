.class public Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;
.super Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;
.source "CashReportEmailRecipientSetting.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final emailRecipient:Lcom/squareup/settings/StringLocalSetting;


# direct methods
.method protected constructor <init>(Lcom/squareup/settings/server/Features;Ljavax/inject/Provider;Lcom/squareup/settings/StringLocalSetting;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;",
            "Lcom/squareup/settings/StringLocalSetting;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, ""

    .line 22
    invoke-direct {p0, p1, v0}, Lcom/squareup/deviceprofiles/DeviceSettingOrLocalSetting;-><init>(Lcom/squareup/settings/server/Features;Ljava/lang/Object;)V

    .line 23
    iput-object p2, p0, Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;->accountStatusProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p3, p0, Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;->emailRecipient:Lcom/squareup/settings/StringLocalSetting;

    return-void
.end method


# virtual methods
.method protected bridge synthetic getValueFromDeviceProfile()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;->getValueFromDeviceProfile()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getValueFromDeviceProfile()Ljava/lang/String;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;->accountStatusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    iget-object v0, v0, Lcom/squareup/server/account/protos/AccountStatusResponse;->preferences:Lcom/squareup/server/account/protos/Preferences;

    if-eqz v0, :cond_1

    .line 33
    iget-object v1, v0, Lcom/squareup/server/account/protos/Preferences;->cash_management_report_recipient_email:Ljava/lang/String;

    if-nez v1, :cond_0

    goto :goto_0

    .line 36
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/Preferences;->cash_management_report_recipient_email:Ljava/lang/String;

    return-object v0

    :cond_1
    :goto_0
    const-string v0, ""

    return-object v0
.end method

.method protected bridge synthetic getValueFromLocalSettings()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;->getValueFromLocalSettings()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getValueFromLocalSettings()Ljava/lang/String;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;->emailRecipient:Lcom/squareup/settings/StringLocalSetting;

    invoke-virtual {v0}, Lcom/squareup/settings/StringLocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method protected isAllowedWhenUsingLocal()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected bridge synthetic setValueLocallyInternal(Ljava/lang/Object;)V
    .locals 0

    .line 13
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;->setValueLocallyInternal(Ljava/lang/String;)V

    return-void
.end method

.method protected setValueLocallyInternal(Ljava/lang/String;)V
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/cashmanagement/CashReportEmailRecipientSetting;->emailRecipient:Lcom/squareup/settings/StringLocalSetting;

    invoke-virtual {v0, p1}, Lcom/squareup/settings/StringLocalSetting;->set(Ljava/lang/String;)V

    return-void
.end method
