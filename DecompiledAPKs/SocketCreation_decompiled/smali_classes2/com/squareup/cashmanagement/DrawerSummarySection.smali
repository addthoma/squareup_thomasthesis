.class public Lcom/squareup/cashmanagement/DrawerSummarySection;
.super Ljava/lang/Object;
.source "DrawerSummarySection.java"


# instance fields
.field public final headerText:Ljava/lang/String;

.field public final mediumSummaryRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation
.end field

.field public final normalSummaryRows:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/cashmanagement/DrawerSummarySection;->headerText:Ljava/lang/String;

    .line 16
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cashmanagement/DrawerSummarySection;->normalSummaryRows:Ljava/util/List;

    .line 17
    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cashmanagement/DrawerSummarySection;->mediumSummaryRows:Ljava/util/List;

    return-void
.end method
