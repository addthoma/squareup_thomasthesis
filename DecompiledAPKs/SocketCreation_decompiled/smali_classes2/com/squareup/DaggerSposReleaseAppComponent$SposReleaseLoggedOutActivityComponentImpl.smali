.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/root/SposReleaseLoggedOutActivityComponent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SposReleaseLoggedOutActivityComponentImpl"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$LMTPV_ComponentImpl;,
        Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$CAS3_ComponentImpl;,
        Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$AS_ComponentImpl;,
        Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$ALS_ComponentImpl;
    }
.end annotation


# instance fields
.field private containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
            ">;"
        }
    .end annotation
.end field

.field private learnMoreTourPopupPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private loggedOutFinisherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutFinisher;",
            ">;"
        }
    .end annotation
.end field

.field private loggedOutOnboardingStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loggedout/LoggedOutOnboardingStarter;",
            ">;"
        }
    .end annotation
.end field

.field private loggedOutScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private navigationListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/navigation/NavigationListener;",
            ">;"
        }
    .end annotation
.end field

.field private provideBrowserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private provideContainerActivityDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerActivityDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private provideFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private provideImpersonationHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/ImpersonationHelper;",
            ">;"
        }
    .end annotation
.end field

.field private provideScreenChangeLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/flowlegacy/ScreenChangeLedgerManager;",
            ">;"
        }
    .end annotation
.end field

.field private realOnboardingTypeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/RealOnboardingType;",
            ">;"
        }
    .end annotation
.end field

.field private screenNavigationLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/navigation/ScreenNavigationLogger;",
            ">;"
        }
    .end annotation
.end field

.field private softInputPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/DaggerSposReleaseAppComponent;


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->initialize()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent;)V

    return-void
.end method

.method static synthetic access$268300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;)Ljavax/inject/Provider;
    .locals 0

    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->loggedOutScopeRunnerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$268400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;)Ljavax/inject/Provider;
    .locals 0

    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->provideFlowProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$269000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;)Ljavax/inject/Provider;
    .locals 0

    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->learnMoreTourPopupPresenterProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method private getAuthenticatorViewFactory()Lcom/squareup/ui/login/AuthenticatorViewFactory;
    .locals 9

    new-instance v8, Lcom/squareup/ui/login/AuthenticatorViewFactory;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->getEmailPasswordLoginCoordinatorFactory()Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->getLoginAlertDialogFactoryFactory()Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->getLoginWarningDialogFactoryFactory()Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;

    move-result-object v3

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->getEnrollSmsCoordinatorFactory()Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;

    move-result-object v4

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->getEnrollGoogleAuthCodeCoordinatorFactory()Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;

    move-result-object v5

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->getVerificationCodeSmsCoordinatorFactory()Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;

    move-result-object v6

    new-instance v7, Lcom/squareup/loggedout/DefaultDeviceCodeViewBinding;

    invoke-direct {v7}, Lcom/squareup/loggedout/DefaultDeviceCodeViewBinding;-><init>()V

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorViewFactory;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;)V

    return-object v8
.end method

.method private getEmailPasswordLoginCoordinatorFactory()Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;
    .locals 5

    new-instance v0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->getGlassSpinner()Lcom/squareup/register/widgets/GlassSpinner;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$266900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/ui/login/PostInstallEncryptedEmail;)V

    return-object v0
.end method

.method private getEnrollGoogleAuthCodeCoordinatorFactory()Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;
    .locals 2

    new-instance v0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;

    invoke-static {}, Lcom/squareup/util/AndroidModule_ProvideClipboardFactory;->provideClipboard()Lcom/squareup/util/Clipboard;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;-><init>(Lcom/squareup/util/Clipboard;)V

    return-object v0
.end method

.method private getEnrollSmsCoordinatorFactory()Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;
    .locals 3

    new-instance v0, Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$21300(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/text/scrubber/PhoneNumberScrubber;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/text/InsertingScrubber;)V

    return-object v0
.end method

.method private getFailureMessageFactory()Lcom/squareup/receiving/FailureMessageFactory;
    .locals 2

    new-instance v0, Lcom/squareup/receiving/FailureMessageFactory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    invoke-direct {v0, v1}, Lcom/squareup/receiving/FailureMessageFactory;-><init>(Lcom/squareup/util/Res;)V

    return-object v0
.end method

.method private getGlassSpinner()Lcom/squareup/register/widgets/GlassSpinner;
    .locals 3

    new-instance v0, Lcom/squareup/register/widgets/GlassSpinner;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$6800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/thread/executor/MainThread;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/register/widgets/GlassSpinner;-><init>(Lcom/squareup/thread/executor/MainThread;Lio/reactivex/Scheduler;)V

    return-object v0
.end method

.method private getLoginAlertDialogFactoryFactory()Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;
    .locals 3

    new-instance v0, Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->provideBrowserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/BrowserLauncher;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;-><init>(Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;)V

    return-object v0
.end method

.method private getLoginWarningDialogFactoryFactory()Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;
    .locals 3

    new-instance v0, Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/util/AddAppNameFormatter;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Res;

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;-><init>(Lcom/squareup/util/AppNameFormatter;Lcom/squareup/util/Res;)V

    return-object v0
.end method

.method private getMediaButtonDisabler()Lcom/squareup/ui/MediaButtonDisabler;
    .locals 1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$600(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/AppBootstrapModule;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/AppBootstrapModule_ProvideApplicationFactory;->provideApplication(Lcom/squareup/AppBootstrapModule;)Landroid/app/Application;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/MediaButtonDisabler_Factory;->newInstance(Landroid/app/Application;)Lcom/squareup/ui/MediaButtonDisabler;

    move-result-object v0

    return-object v0
.end method

.method private getProvider()Lcom/squareup/caller/ProgressDialogCoordinator$Provider;
    .locals 3

    new-instance v0, Lcom/squareup/caller/ProgressDialogCoordinator$Provider;

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-static {}, Lcom/squareup/thread/MainThreadModule_ProvideMainThreadEnforcerFactory;->provideMainThreadEnforcer()Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/caller/ProgressDialogCoordinator$Provider;-><init>(Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v0
.end method

.method private getRealAuthenticator()Lcom/squareup/ui/login/RealAuthenticator;
    .locals 12

    new-instance v11, Lcom/squareup/ui/login/RealAuthenticator;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$267000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/ui/login/LocalSettingTrustedDeviceDetailsStore;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$267100(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/ui/login/AuthenticationServiceEndpoint;

    move-result-object v2

    invoke-static {}, Lcom/squareup/thread/Rx2SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lio/reactivex/Scheduler;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$267200(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/location/AndroidGeoCountryCodeGuesser;

    move-result-object v5

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->getFailureMessageFactory()Lcom/squareup/receiving/FailureMessageFactory;

    move-result-object v6

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$20100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/ToastFactory;

    invoke-static {}, Lcom/squareup/loggedout/LoggedOutFeatureModule_EnableDeviceCodeLoginModule_ProvideSupportsDeviceCodeLoginFactory;->provideSupportsDeviceCodeLogin()Lcom/squareup/ui/login/SupportsDeviceCodeLogin;

    move-result-object v8

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$13200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/account/accountservice/AppAccountCache;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$267300(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/safetynetrecaptcha/impl/RealSafetyNetRecaptchaVerifier;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/login/RealAuthenticator;-><init>(Lcom/squareup/ui/login/TrustedDeviceDetailsStore;Lcom/squareup/ui/login/AuthenticationServiceEndpoint;Lio/reactivex/Scheduler;Lcom/squareup/badbus/BadBus;Lcom/squareup/location/CountryGuesser;Lcom/squareup/receiving/FailureMessageFactory;Lcom/squareup/util/ToastFactory;Lcom/squareup/ui/login/SupportsDeviceCodeLogin;Lcom/squareup/account/accountservice/AppAccountCache;Lcom/squareup/safetynetrecaptcha/SafetyNetRecaptchaVerifier;)V

    return-object v11
.end method

.method private getRealAuthenticatorRendererWorkflow()Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;
    .locals 2

    new-instance v0, Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->getRealAuthenticator()Lcom/squareup/ui/login/RealAuthenticator;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;-><init>(Lcom/squareup/ui/login/RealAuthenticator;)V

    return-object v0
.end method

.method private getVerificationCodeSmsCoordinatorFactory()Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;
    .locals 3

    new-instance v0, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$20100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/ToastFactory;

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/util/ToastFactory;)V

    return-object v0
.end method

.method private initialize()V
    .locals 20

    move-object/from16 v0, p0

    invoke-static {}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOutUi_ProvideTransactionLedgerManagerFactory;->create()Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOutUi_ProvideTransactionLedgerManagerFactory;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOutUi_ProvideScreenChangeLedgerManagerFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/payment/ledger/TransactionLedgerModule_LoggedOutUi_ProvideScreenChangeLedgerManagerFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->provideScreenChangeLedgerManagerProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->provideScreenChangeLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/navigation/NavigationListener_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/navigation/NavigationListener_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->navigationListenerProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/navigation/ScreenNavigationLogger_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/navigation/ScreenNavigationLogger_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->screenNavigationLoggerProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$7300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/SoftInputPresenter_Factory;->create(Ljavax/inject/Provider;)Lcom/squareup/ui/SoftInputPresenter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->softInputPresenterProvider:Ljavax/inject/Provider;

    new-instance v1, Ldagger/internal/DelegateFactory;

    invoke-direct {v1}, Ldagger/internal/DelegateFactory;-><init>()V

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->containerProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->containerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Lcom/squareup/ui/loggedout/LoggedOutActivity_Module_ProvideFlowFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/ui/loggedout/LoggedOutActivity_Module_ProvideFlowFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->provideFlowProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$267400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/onboarding/RealOnboardingType_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/onboarding/RealOnboardingType_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->realOnboardingTypeProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/ui/loggedout/LoggedOutFinisher_Factory;->create()Lcom/squareup/ui/loggedout/LoggedOutFinisher_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->loggedOutFinisherProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$6000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->realOnboardingTypeProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$30800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->loggedOutFinisherProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/loggedout/LoggedOutOnboardingStarter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->loggedOutOnboardingStarterProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$267500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->learnMoreTourPopupPresenterProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->provideFlowProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->containerProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$7600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$267600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v8, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->loggedOutOnboardingStarterProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$267700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v9

    iget-object v10, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->learnMoreTourPopupPresenterProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$267500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v11

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v12

    invoke-static {}, Lcom/squareup/singlesignon/NoSingleSignOnModule_ProvideSingleSignOnFactory;->create()Lcom/squareup/singlesignon/NoSingleSignOnModule_ProvideSingleSignOnFactory;

    move-result-object v13

    invoke-static {}, Lcom/squareup/ui/loggedout/LandingCanceledListener_NoLandingCanceledListenerModule_ProvideLandingCanceledListenerFactory;->create()Lcom/squareup/ui/loggedout/LandingCanceledListener_NoLandingCanceledListenerModule_ProvideLandingCanceledListenerFactory;

    move-result-object v14

    invoke-static {}, Lcom/squareup/ui/login/CreateAccountCanceledListener_NoCreateAccountCanceledListenerModule_ProvideCreateAccountCanceledListenerFactory;->create()Lcom/squareup/ui/login/CreateAccountCanceledListener_NoCreateAccountCanceledListenerModule_ProvideCreateAccountCanceledListenerFactory;

    move-result-object v15

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$17900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v16

    invoke-static {}, Lcom/squareup/ui/loggedout/FinalizeLoginAlertDialogFactory_Factory;->create()Lcom/squareup/ui/loggedout/FinalizeLoginAlertDialogFactory_Factory;

    move-result-object v17

    invoke-static {}, Ldagger/internal/SetFactory;->empty()Ldagger/internal/Factory;

    move-result-object v18

    invoke-static {}, Lcom/squareup/ui/loggedout/NoFinalLogInCheck_Factory;->create()Lcom/squareup/ui/loggedout/NoFinalLogInCheck_Factory;

    move-result-object v19

    invoke-static/range {v2 .. v19}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/loggedout/LoggedOutScopeRunner_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->loggedOutScopeRunnerProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->containerProvider:Ljavax/inject/Provider;

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->navigationListenerProvider:Ljavax/inject/Provider;

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->screenNavigationLoggerProvider:Ljavax/inject/Provider;

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->softInputPresenterProvider:Ljavax/inject/Provider;

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->loggedOutScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-static {v2, v3, v4, v5}, Lcom/squareup/ui/loggedout/LoggedOutActivityScope_Container_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/loggedout/LoggedOutActivityScope_Container_Factory;

    move-result-object v2

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Ldagger/internal/DelegateFactory;->setDelegate(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    invoke-static {}, Lcom/squareup/util/PosBrowserLauncher_Factory;->create()Lcom/squareup/util/PosBrowserLauncher_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->provideBrowserLauncherProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$7300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/loggedout/LoggedOutActivity_Module_ProvideContainerActivityDelegateFactory;->create(Ljavax/inject/Provider;)Lcom/squareup/ui/loggedout/LoggedOutActivity_Module_ProvideContainerActivityDelegateFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->provideContainerActivityDelegateProvider:Ljavax/inject/Provider;

    invoke-static {}, Lcom/squareup/ui/loggedout/LoggedOutActivity_Module_Prod_ProvideImpersonationHelperFactory;->create()Lcom/squareup/ui/loggedout/LoggedOutActivity_Module_Prod_ProvideImpersonationHelperFactory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->provideImpersonationHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private injectLoggedOutActivity(Lcom/squareup/ui/loggedout/LoggedOutActivity;)Lcom/squareup/ui/loggedout/LoggedOutActivity;
    .locals 1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectAnalytics(Lcom/squareup/ui/SquareActivity;Lcom/squareup/analytics/Analytics;)V

    new-instance v0, Lcom/squareup/development/drawer/ReleaseContentViewInitializer;

    invoke-direct {v0}, Lcom/squareup/development/drawer/ReleaseContentViewInitializer;-><init>()V

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectContentViewInitializer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/development/drawer/ContentViewInitializer;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectLocationMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/radiography/FocusedActivityScanner;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFocusedActivityScanner(Lcom/squareup/ui/SquareActivity;Lcom/squareup/radiography/FocusedActivityScanner;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2500(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectCardReaderPauseAndResumer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/cardreader/CardReaderPauseAndResumer;)V

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->getMediaButtonDisabler()Lcom/squareup/ui/MediaButtonDisabler;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMediaButtonDisabler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/MediaButtonDisabler;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2600(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMinesweeperProvider(Lcom/squareup/ui/SquareActivity;Ljavax/inject/Provider;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/internet/InternetStatusMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectInternetStatusMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/internet/InternetStatusMonitor;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFeatures(Lcom/squareup/ui/SquareActivity;Lcom/squareup/settings/server/Features;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/persistentbundle/PersistentBundleManager;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectPersistentBundleManager(Lcom/squareup/ui/SquareActivity;Lcom/squareup/persistentbundle/PersistentBundleManager;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectDevice(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/Device;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3100(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ActivityResultHandler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectActivityResultHandler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/ActivityResultHandler;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3200(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AndroidConfigurationChangeMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectConfigurationChangeMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/AndroidConfigurationChangeMonitor;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectContainer(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->loggedOutScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectRunner(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->provideContainerActivityDelegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerActivityDelegate;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectContainerActivityDelegate(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/container/ContainerActivityDelegate;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->softInputPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/SoftInputPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectSoftInputPresenter(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/ui/SoftInputPresenter;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->provideImpersonationHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/ImpersonationHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectImpersonationHelper(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/account/ImpersonationHelper;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$267800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectEncryptedEmailsFromReferrals(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$39400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/DeepLinkHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectDeepLinkHelper(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/analytics/DeepLinkHelper;)V

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectMainScheduler(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lrx/Scheduler;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->provideBrowserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/BrowserLauncher;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectBrowserLauncher(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/util/BrowserLauncher;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->loggedOutFinisherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/LoggedOutFinisher;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectLoggedOutFinisher(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/ui/loggedout/LoggedOutFinisher;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->loggedOutScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/loggedout/CreateAccountStarter;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_MembersInjector;->injectCreateAccountStarter(Lcom/squareup/ui/loggedout/LoggedOutActivity;Lcom/squareup/loggedout/CreateAccountStarter;)V

    return-object p1
.end method

.method private injectLoggedOutContainerView(Lcom/squareup/ui/loggedout/LoggedOutContainerView;)Lcom/squareup/ui/loggedout/LoggedOutContainerView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutContainerView_MembersInjector;->injectContainer(Lcom/squareup/ui/loggedout/LoggedOutContainerView;Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->learnMoreTourPopupPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutContainerView_MembersInjector;->injectTourPresenter(Lcom/squareup/ui/loggedout/LoggedOutContainerView;Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;)V

    invoke-static {}, Lcom/squareup/x2/NoX2AppModule_ProvideMaybeSquareDeviceFactory;->provideMaybeSquareDevice()Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/loggedout/LoggedOutContainerView_MembersInjector;->injectBadMaybeSquareDeviceCheck(Lcom/squareup/ui/loggedout/LoggedOutContainerView;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V

    return-object p1
.end method

.method private injectProgressPopup(Lcom/squareup/caller/ProgressPopup;)Lcom/squareup/caller/ProgressPopup;
    .locals 1

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->getProvider()Lcom/squareup/caller/ProgressDialogCoordinator$Provider;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/caller/ProgressPopup_MembersInjector;->injectCoordinatorProvider(Lcom/squareup/caller/ProgressPopup;Lcom/squareup/caller/ProgressDialogCoordinator$Provider;)V

    return-object p1
.end method


# virtual methods
.method public authenticatorScope()Lcom/squareup/ui/login/AuthenticatorScope$Component;
    .locals 2

    new-instance v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$AS_ComponentImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$AS_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V

    return-object v0
.end method

.method public authenticatorWorkflowRunner()Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;
    .locals 4

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->getAuthenticatorViewFactory()Lcom/squareup/ui/login/AuthenticatorViewFactory;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->getRealAuthenticatorRendererWorkflow()Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;-><init>(Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;Lcom/squareup/ui/login/AuthenticatorViewFactory;Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;)V

    return-object v0
.end method

.method public browserLauncher()Lcom/squareup/util/BrowserLauncher;
    .locals 1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->provideBrowserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/BrowserLauncher;

    return-object v0
.end method

.method public createAccount()Lcom/squareup/ui/login/CreateAccountScreen$Component;
    .locals 2

    new-instance v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$CAS3_ComponentImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$CAS3_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V

    return-object v0
.end method

.method public inject(Lcom/squareup/caller/ProgressPopup;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->injectProgressPopup(Lcom/squareup/caller/ProgressPopup;)Lcom/squareup/caller/ProgressPopup;

    return-void
.end method

.method public inject(Lcom/squareup/ui/loggedout/LoggedOutActivity;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->injectLoggedOutActivity(Lcom/squareup/ui/loggedout/LoggedOutActivity;)Lcom/squareup/ui/loggedout/LoggedOutActivity;

    return-void
.end method

.method public inject(Lcom/squareup/ui/loggedout/LoggedOutContainerView;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->injectLoggedOutContainerView(Lcom/squareup/ui/loggedout/LoggedOutContainerView;)Lcom/squareup/ui/loggedout/LoggedOutContainerView;

    return-void
.end method

.method public landingScreen()Lcom/squareup/ui/loggedout/AbstractLandingScreen$Component;
    .locals 2

    new-instance v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$ALS_ComponentImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$ALS_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V

    return-object v0
.end method

.method public loggedOutScopeRunner()Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;
    .locals 1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->loggedOutScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    return-object v0
.end method

.method public splashCoordinator()Lcom/squareup/ui/loggedout/SplashCoordinator;
    .locals 5

    new-instance v0, Lcom/squareup/ui/loggedout/SplashCoordinator;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->loggedOutScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/analytics/Analytics;

    invoke-static {}, Lcom/squareup/loggedout/LoggedOutFeatureModule_DefaultSplashScreenModule_GetPagesFactory;->getPages()Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$267800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/EncryptedEmailsFromReferrals;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/loggedout/SplashCoordinator;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/analytics/Analytics;Ljava/util/List;Lcom/squareup/analytics/EncryptedEmailsFromReferrals;)V

    return-object v0
.end method

.method public textBelowImageSplashCoordinator()Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;
    .locals 4

    new-instance v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->loggedOutScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/analytics/Analytics;

    invoke-static {}, Lcom/squareup/loggedout/LoggedOutFeatureModule_DefaultSplashScreenModule_ProvideTextAboveImageSplashScreenConfigFactory;->provideTextAboveImageSplashScreenConfig()Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/loggedout/TextAboveImageSplashCoordinator;-><init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;)V

    return-object v0
.end method

.method public tour()Lcom/squareup/ui/tour/LearnMoreTourPopupView$Component;
    .locals 2

    new-instance v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$LMTPV_ComponentImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl$LMTPV_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedOutActivityComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V

    return-object v0
.end method
