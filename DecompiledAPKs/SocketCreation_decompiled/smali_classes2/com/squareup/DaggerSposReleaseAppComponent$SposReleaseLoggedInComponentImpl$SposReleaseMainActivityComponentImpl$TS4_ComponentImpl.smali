.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/timecards/TimecardsScope$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TS4_ComponentImpl"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl$CIOCS_ComponentImpl;,
        Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl$CIOS_ComponentImpl;
    }
.end annotation


# instance fields
.field private featureReleaseHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/FeatureReleaseHelper;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

.field private timecardsPrintingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/TimecardsPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private timecardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/Timecards;",
            ">;"
        }
    .end annotation
.end field

.field private timecardsScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/TimecardsScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->initialize()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)V

    return-void
.end method

.method static synthetic access$259300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;)Ljavax/inject/Provider;
    .locals 0

    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->featureReleaseHelperProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$259400(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;)Ljavax/inject/Provider;
    .locals 0

    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method static synthetic access$259500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;)Ljavax/inject/Provider;
    .locals 0

    iget-object p0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    return-object p0
.end method

.method private getFeatureReleaseHelper()Lcom/squareup/ui/timecards/FeatureReleaseHelper;
    .locals 2

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$25700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/permissions/EmployeeManagement;

    invoke-static {v0, v1}, Lcom/squareup/ui/timecards/FeatureReleaseHelper_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagement;)Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    move-result-object v0

    return-object v0
.end method

.method private initialize()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1800(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$25700(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/ui/timecards/FeatureReleaseHelper_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/timecards/FeatureReleaseHelper_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->featureReleaseHelperProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$258900(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->featureReleaseHelperProvider:Ljavax/inject/Provider;

    invoke-static {v1, v2}, Lcom/squareup/ui/timecards/Timecards_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/timecards/Timecards_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$27800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$27900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v3, v3, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v3}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$259000(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/TimecardsPrintingDispatcher_Factory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsPrintingDispatcherProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$6000(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v2

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$37300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    iget-object v5, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49300(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v6

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v7

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$7300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v8

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$21900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v9

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$17600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v10

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$55800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v11

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v12

    iget-object v13, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->featureReleaseHelperProvider:Ljavax/inject/Provider;

    iget-object v14, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsPrintingDispatcherProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$49800(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Ljavax/inject/Provider;

    move-result-object v15

    iget-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$1700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v16

    invoke-static/range {v2 .. v16}, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;

    move-result-object v1

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private injectTimecardsActionBarView(Lcom/squareup/ui/timecards/TimecardsActionBarView;)Lcom/squareup/ui/timecards/TimecardsActionBarView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView_MembersInjector;->injectRes(Lcom/squareup/ui/timecards/TimecardsActionBarView;Lcom/squareup/util/Res;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView_MembersInjector;->injectDevice(Lcom/squareup/ui/timecards/TimecardsActionBarView;Lcom/squareup/util/Device;)V

    return-object p1
.end method

.method private injectTimecardsSuccessButtonsView(Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;)Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView_MembersInjector;->injectDevice(Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;Lcom/squareup/util/Device;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-static {p1, v0}, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView_MembersInjector;->injectTimecardsScopeRunner(Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/util/AddAppNameFormatter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView_MembersInjector;->injectAppNameFormatter(Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;Lcom/squareup/util/AppNameFormatter;)V

    return-object p1
.end method


# virtual methods
.method public addNotesCoordinator()Lcom/squareup/ui/timecards/AddNotesCoordinator;
    .locals 3

    new-instance v0, Lcom/squareup/ui/timecards/AddNotesCoordinator;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$59500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lflow/Flow;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/timecards/AddNotesCoordinator;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lflow/Flow;)V

    return-object v0
.end method

.method public clockInConfirmationCoordinator()Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;
    .locals 5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTime;

    move-result-object v2

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/timecards/ClockInConfirmationCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public clockInCoordinator()Lcom/squareup/ui/timecards/ClockInCoordinator;
    .locals 5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTime;

    move-result-object v2

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/timecards/ClockInCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/timecards/ClockInCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public clockInModalCoordinator()Lcom/squareup/ui/timecards/ClockInModalCoordinator;
    .locals 7

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTime;

    move-result-object v3

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/util/AddAppNameFormatter;

    move-result-object v6

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/timecards/ClockInModalCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/AppNameFormatter;)Lcom/squareup/ui/timecards/ClockInModalCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public clockInOrContinueScreen()Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Component;
    .locals 2

    new-instance v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl$CIOCS_ComponentImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl$CIOCS_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V

    return-object v0
.end method

.method public clockInOut()Lcom/squareup/ui/timecards/ClockInOutScreen$Component;
    .locals 2

    new-instance v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl$CIOS_ComponentImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl$CIOS_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V

    return-object v0
.end method

.method public clockOutConfirmationCoordinator()Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;
    .locals 5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTime;

    move-result-object v2

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/timecards/ClockOutConfirmationCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public clockOutCoordinator()Lcom/squareup/ui/timecards/ClockOutCoordinator;
    .locals 8

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTime;

    move-result-object v3

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/Analytics;

    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->getFeatureReleaseHelper()Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    move-result-object v6

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4400(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljava/util/Locale;

    move-result-object v7

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/timecards/ClockOutCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/timecards/FeatureReleaseHelper;Ljava/util/Locale;)Lcom/squareup/ui/timecards/ClockOutCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public editNotesCoordinator()Lcom/squareup/ui/timecards/EditNotesCoordinator;
    .locals 3

    new-instance v0, Lcom/squareup/ui/timecards/EditNotesCoordinator;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$59500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lflow/Flow;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/timecards/EditNotesCoordinator;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lflow/Flow;)V

    return-object v0
.end method

.method public employeeJobsListCoordinator()Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;
    .locals 5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTime;

    move-result-object v2

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/timecards/EmployeeJobsListCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public employeeJobsListModalCoordinator()Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;
    .locals 5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTime;

    move-result-object v2

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/timecards/EmployeeJobsListModalCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public endBreakAfterLoginConfirmationCoordinator()Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;
    .locals 7

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$21900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/util/AddAppNameFormatter;

    move-result-object v6

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/util/AppNameFormatter;)Lcom/squareup/ui/timecards/EndBreakAfterLoginConfirmationCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public endBreakAfterLoginCoordinator()Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;
    .locals 9

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTime;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$21900(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$3000(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/utilities/ui/RealDevice;

    move-result-object v7

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$2800(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/settings/server/FeatureFlagFeatures;

    move-result-object v8

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lrx/Scheduler;Lcom/squareup/time/CurrentTime;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/timecards/EndBreakAfterLoginCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public endBreakConfirmationCoordinator()Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;
    .locals 5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTime;

    move-result-object v2

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public endBreakCoordinator()Lcom/squareup/ui/timecards/EndBreakCoordinator;
    .locals 5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTime;

    move-result-object v2

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/timecards/EndBreakCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/timecards/EndBreakCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public inject(Lcom/squareup/ui/timecards/TimecardsActionBarView;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->injectTimecardsActionBarView(Lcom/squareup/ui/timecards/TimecardsActionBarView;)Lcom/squareup/ui/timecards/TimecardsActionBarView;

    return-void
.end method

.method public inject(Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->injectTimecardsSuccessButtonsView(Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;)Lcom/squareup/ui/timecards/TimecardsSuccessButtonsView;

    return-void
.end method

.method public listBreaksCoordinator()Lcom/squareup/ui/timecards/ListBreaksCoordinator;
    .locals 5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTime;

    move-result-object v2

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/timecards/ListBreaksCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/timecards/ListBreaksCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public scopeRunner()Lcom/squareup/ui/timecards/TimecardsScopeRunner;
    .locals 1

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    return-object v0
.end method

.method public startBreakCoordinator()Lcom/squareup/ui/timecards/StartBreakCoordinator;
    .locals 5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTime;

    move-result-object v2

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/timecards/StartBreakCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/timecards/StartBreakCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public startBreakOrClockOutCoordinator()Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;
    .locals 5

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v0, v0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$5900(Lcom/squareup/DaggerSposReleaseAppComponent;)Lcom/squareup/time/RealCurrentTime;

    move-result-object v2

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v4, v4, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v4}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$700(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v4

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/timecards/StartBreakOrClockOutCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public timecardsLoadingCoordinator()Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;
    .locals 2

    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-static {}, Lcom/squareup/thread/Rx1SchedulerModule_ProvideMainSchedulerFactory;->provideMainScheduler()Lrx/Scheduler;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator_Factory;->newInstance(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lrx/Scheduler;)Lcom/squareup/ui/timecards/TimecardsLoadingCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public viewNotesCoordinator()Lcom/squareup/ui/timecards/ViewNotesCoordinator;
    .locals 3

    new-instance v0, Lcom/squareup/ui/timecards/ViewNotesCoordinator;

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->timecardsScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$TS4_ComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->access$59500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;)Lflow/Flow;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/timecards/ViewNotesCoordinator;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lflow/Flow;)V

    return-object v0
.end method
