.class public interface abstract Lcom/squareup/barcodescanners/BarcodeScanner;
.super Ljava/lang/Object;
.source "BarcodeScanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/barcodescanners/BarcodeScanner$Listener;
    }
.end annotation


# virtual methods
.method public abstract getConnectionType()Ljava/lang/String;
.end method

.method public abstract getManufacturer()Ljava/lang/String;
.end method

.method public abstract getModel()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getSerialNumber()Ljava/lang/String;
.end method
