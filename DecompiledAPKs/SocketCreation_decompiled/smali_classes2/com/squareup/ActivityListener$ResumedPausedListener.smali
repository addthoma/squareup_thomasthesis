.class public interface abstract Lcom/squareup/ActivityListener$ResumedPausedListener;
.super Ljava/lang/Object;
.source "ActivityListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ActivityListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ResumedPausedListener"
.end annotation


# virtual methods
.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method
