.class public final Lcom/squareup/checkoutapplet/CheckoutAppletScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "CheckoutAppletScope.kt"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/checkoutapplet/CheckoutAppletScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutapplet/CheckoutAppletScope$ParentComponent;,
        Lcom/squareup/checkoutapplet/CheckoutAppletScope$Component;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckoutAppletScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CheckoutAppletScope.kt\ncom/squareup/checkoutapplet/CheckoutAppletScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,49:1\n35#2:50\n35#2:51\n24#3,4:52\n*E\n*S KotlinDebug\n*F\n+ 1 CheckoutAppletScope.kt\ncom/squareup/checkoutapplet/CheckoutAppletScope\n*L\n19#1:50\n25#1:51\n47#1,4:52\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0002\u000e\u000fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\nH\u0016R\u001c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00058\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0006\u0010\u0003\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/checkoutapplet/CheckoutAppletScope;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "Lcom/squareup/container/RegistersInScope;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "CREATOR$annotations",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "register",
        "",
        "scope",
        "Component",
        "ParentComponent",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/checkoutapplet/CheckoutAppletScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/checkoutapplet/CheckoutAppletScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/checkoutapplet/CheckoutAppletScope;

    invoke-direct {v0}, Lcom/squareup/checkoutapplet/CheckoutAppletScope;-><init>()V

    sput-object v0, Lcom/squareup/checkoutapplet/CheckoutAppletScope;->INSTANCE:Lcom/squareup/checkoutapplet/CheckoutAppletScope;

    .line 52
    new-instance v0, Lcom/squareup/checkoutapplet/CheckoutAppletScope$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/checkoutapplet/CheckoutAppletScope$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 55
    sput-object v0, Lcom/squareup/checkoutapplet/CheckoutAppletScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method

.method public static synthetic CREATOR$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-super {p0, p1}, Lcom/squareup/ui/main/InMainActivityScope;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 51
    const-class v1, Lcom/squareup/checkoutapplet/CheckoutAppletScope$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 26
    check-cast p1, Lcom/squareup/checkoutapplet/CheckoutAppletScope$ParentComponent;

    .line 28
    invoke-interface {p1}, Lcom/squareup/checkoutapplet/CheckoutAppletScope$ParentComponent;->checkoutAppletWorkflowRunner()Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner;

    move-result-object p1

    const-string v1, "scopeBuilder"

    .line 29
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string p1, "super.buildScope(parentS\u2026vices(scopeBuilder)\n    }"

    .line 25
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    const-class v0, Lcom/squareup/checkoutapplet/CheckoutAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 19
    check-cast v0, Lcom/squareup/checkoutapplet/CheckoutAppletScope$Component;

    .line 20
    invoke-interface {v0}, Lcom/squareup/checkoutapplet/CheckoutAppletScope$Component;->checkoutData()Lcom/squareup/checkout/v2/data/transaction/TransactionData;

    move-result-object v1

    check-cast v1, Lmortar/Scoped;

    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 21
    invoke-interface {v0}, Lcom/squareup/checkoutapplet/CheckoutAppletScope$Component;->checkoutAppletScopeRunner()Lcom/squareup/checkoutapplet/CheckoutAppletScopeRunner;

    move-result-object v0

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
