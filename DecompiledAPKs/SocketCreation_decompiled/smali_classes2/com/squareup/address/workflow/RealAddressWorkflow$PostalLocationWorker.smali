.class public final Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker;
.super Lcom/squareup/workflow/rx2/PublisherWorker;
.source "RealAddressWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/address/workflow/RealAddressWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "PostalLocationWorker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/rx2/PublisherWorker<",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/server/address/PostalLocation;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0080\u0004\u0018\u00002\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0014\u0010\t\u001a\u00020\n2\n\u0010\u000b\u001a\u0006\u0012\u0002\u0008\u00030\u000cH\u0016J\u0016\u0010\r\u001a\u0010\u0012\u000c\u0008\u0001\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u000eH\u0016R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker;",
        "Lcom/squareup/workflow/rx2/PublisherWorker;",
        "",
        "Lcom/squareup/server/address/PostalLocation;",
        "postalCode",
        "",
        "(Lcom/squareup/address/workflow/RealAddressWorkflow;Ljava/lang/String;)V",
        "getPostalCode$impl_release",
        "()Ljava/lang/String;",
        "doesSameWorkAs",
        "",
        "otherWorker",
        "Lcom/squareup/workflow/Worker;",
        "runPublisher",
        "Lorg/reactivestreams/Publisher;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final postalCode:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/address/workflow/RealAddressWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/address/workflow/RealAddressWorkflow;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const-string v0, "postalCode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    iput-object p1, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker;->this$0:Lcom/squareup/address/workflow/RealAddressWorkflow;

    .line 218
    invoke-direct {p0}, Lcom/squareup/workflow/rx2/PublisherWorker;-><init>()V

    iput-object p2, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker;->postalCode:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public doesSameWorkAs(Lcom/squareup/workflow/Worker;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Worker<",
            "*>;)Z"
        }
    .end annotation

    const-string v0, "otherWorker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    instance-of v0, p1, Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker;->postalCode:Ljava/lang/String;

    check-cast p1, Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker;

    iget-object p1, p1, Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker;->postalCode:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final getPostalCode$impl_release()Ljava/lang/String;
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker;->postalCode:Ljava/lang/String;

    return-object v0
.end method

.method public runPublisher()Lorg/reactivestreams/Publisher;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/reactivestreams/Publisher<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/server/address/PostalLocation;",
            ">;>;"
        }
    .end annotation

    .line 220
    iget-object v0, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker;->this$0:Lcom/squareup/address/workflow/RealAddressWorkflow;

    invoke-static {v0}, Lcom/squareup/address/workflow/RealAddressWorkflow;->access$getAddressService$p(Lcom/squareup/address/workflow/RealAddressWorkflow;)Lcom/squareup/server/address/AddressService;

    move-result-object v0

    .line 221
    iget-object v1, p0, Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker;->postalCode:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/server/address/AddressService;->lookUp(Ljava/lang/String;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object v0

    .line 222
    invoke-virtual {v0}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 223
    sget-object v1, Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker$runPublisher$1;->INSTANCE:Lcom/squareup/address/workflow/RealAddressWorkflow$PostalLocationWorker$runPublisher$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 229
    invoke-virtual {v0}, Lio/reactivex/Single;->toFlowable()Lio/reactivex/Flowable;

    move-result-object v0

    const-string v1, "addressService\n         \u2026}\n          .toFlowable()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lorg/reactivestreams/Publisher;

    return-object v0
.end method
