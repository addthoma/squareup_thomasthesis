.class public final Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;
.super Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;
.source "AddressBodyScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PickCityInput"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0011\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001BA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0014\u0008\u0002\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000b\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u0018\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\tH\u00c6\u0003J\u0015\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000bH\u00c6\u0003JM\u0010\u001d\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u000e\u0008\u0002\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0014\u0008\u0002\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000bH\u00c6\u0001J\u0013\u0010\u001e\u001a\u00020\u001f2\u0008\u0010 \u001a\u0004\u0018\u00010!H\u00d6\u0003J\t\u0010\"\u001a\u00020\tH\u00d6\u0001J\t\u0010#\u001a\u00020$H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u001d\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00020\r0\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0010\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;",
        "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;",
        "city",
        "Lcom/squareup/workflow/text/WorkflowEditableText;",
        "state",
        "options",
        "",
        "Lcom/squareup/server/address/PostalLocation;",
        "selected",
        "",
        "onCityPicked",
        "Lkotlin/Function1;",
        "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;",
        "",
        "(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;ILkotlin/jvm/functions/Function1;)V",
        "getCity",
        "()Lcom/squareup/workflow/text/WorkflowEditableText;",
        "getOnCityPicked",
        "()Lkotlin/jvm/functions/Function1;",
        "getOptions",
        "()Ljava/util/List;",
        "getSelected",
        "()I",
        "getState",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final city:Lcom/squareup/workflow/text/WorkflowEditableText;

.field private final onCityPicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/address/PostalLocation;",
            ">;"
        }
    .end annotation
.end field

.field private final selected:I

.field private final state:Lcom/squareup/workflow/text/WorkflowEditableText;


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;ILkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Ljava/util/List<",
            "Lcom/squareup/server/address/PostalLocation;",
            ">;I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "city"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "options"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCityPicked"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, v0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->city:Lcom/squareup/workflow/text/WorkflowEditableText;

    iput-object p2, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->state:Lcom/squareup/workflow/text/WorkflowEditableText;

    iput-object p3, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->options:Ljava/util/List;

    iput p4, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->selected:I

    iput-object p5, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->onCityPicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;ILkotlin/jvm/functions/Function1;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    .line 33
    sget-object p5, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput$1;->INSTANCE:Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput$1;

    check-cast p5, Lkotlin/jvm/functions/Function1;

    :cond_0
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;-><init>(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;ILkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;ILkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    invoke-virtual {p0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getCity()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object p1

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    invoke-virtual {p0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getState()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object p2

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->options:Ljava/util/List;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget p4, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->selected:I

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->onCityPicked:Lkotlin/jvm/functions/Function1;

    :cond_4
    move-object v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move p6, v1

    move-object p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->copy(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;ILkotlin/jvm/functions/Function1;)Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getCity()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getState()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/address/PostalLocation;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->options:Ljava/util/List;

    return-object v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->selected:I

    return v0
.end method

.method public final component5()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->onCityPicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;ILkotlin/jvm/functions/Function1;)Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Lcom/squareup/workflow/text/WorkflowEditableText;",
            "Ljava/util/List<",
            "Lcom/squareup/server/address/PostalLocation;",
            ">;I",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;"
        }
    .end annotation

    const-string v0, "city"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "options"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onCityPicked"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;-><init>(Lcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;ILkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;

    invoke-virtual {p0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getCity()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getCity()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getState()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getState()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->options:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->options:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->selected:I

    iget v1, p1, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->selected:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->onCityPicked:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->onCityPicked:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getCity()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->city:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public final getOnCityPicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityResult;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->onCityPicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOptions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/address/PostalLocation;",
            ">;"
        }
    .end annotation

    .line 31
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->options:Ljava/util/List;

    return-object v0
.end method

.method public final getSelected()I
    .locals 1

    .line 32
    iget v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->selected:I

    return v0
.end method

.method public getState()Lcom/squareup/workflow/text/WorkflowEditableText;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->state:Lcom/squareup/workflow/text/WorkflowEditableText;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getCity()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getState()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->options:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->selected:I

    invoke-static {v2}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v2

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->onCityPicked:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PickCityInput(city="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getCity()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->getState()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->options:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->selected:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", onCityPicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/address/workflow/AddressBodyScreen$LocationInput$PickCityInput;->onCityPicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
