.class public final enum Lcom/squareup/Card$PanWarning;
.super Ljava/lang/Enum;
.source "Card.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/Card;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PanWarning"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/Card$PanWarning;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/Card$PanWarning;

.field public static final enum VISA_INVALID_16_DIGIT_PAN:Lcom/squareup/Card$PanWarning;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 365
    new-instance v0, Lcom/squareup/Card$PanWarning;

    const/4 v1, 0x0

    const-string v2, "VISA_INVALID_16_DIGIT_PAN"

    invoke-direct {v0, v2, v1}, Lcom/squareup/Card$PanWarning;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/Card$PanWarning;->VISA_INVALID_16_DIGIT_PAN:Lcom/squareup/Card$PanWarning;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/Card$PanWarning;

    .line 359
    sget-object v2, Lcom/squareup/Card$PanWarning;->VISA_INVALID_16_DIGIT_PAN:Lcom/squareup/Card$PanWarning;

    aput-object v2, v0, v1

    sput-object v0, Lcom/squareup/Card$PanWarning;->$VALUES:[Lcom/squareup/Card$PanWarning;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 359
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/Card$PanWarning;
    .locals 1

    .line 359
    const-class v0, Lcom/squareup/Card$PanWarning;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/Card$PanWarning;

    return-object p0
.end method

.method public static values()[Lcom/squareup/Card$PanWarning;
    .locals 1

    .line 359
    sget-object v0, Lcom/squareup/Card$PanWarning;->$VALUES:[Lcom/squareup/Card$PanWarning;

    invoke-virtual {v0}, [Lcom/squareup/Card$PanWarning;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/Card$PanWarning;

    return-object v0
.end method
