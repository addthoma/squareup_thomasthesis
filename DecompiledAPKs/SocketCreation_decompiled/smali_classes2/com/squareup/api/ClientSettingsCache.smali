.class public Lcom/squareup/api/ClientSettingsCache;
.super Ljava/lang/Object;
.source "ClientSettingsCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;
    }
.end annotation


# static fields
.field private static final CLIENT_SETTINGS_PREFERENCES_NAME:Ljava/lang/String; = "ClientSettings"

.field private static final ONE_DAY_MILLIS:J

.field private static final cacheEntryToClientSettings:Lrx/functions/Func1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func1<",
            "Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;",
            "Lcom/squareup/server/api/ClientSettings;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final clientSettingsCache:Landroid/content/SharedPreferences;

.field private final clientSettingsMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final clock:Lcom/squareup/util/Clock;

.field private final fileScheduler:Lrx/Scheduler;

.field private final gson:Lcom/google/gson/Gson;

.field private final mainScheduler:Lrx/Scheduler;

.field private final nullOrExpired:Lrx/functions/Func1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func1<",
            "Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 28
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/api/ClientSettingsCache;->ONE_DAY_MILLIS:J

    .line 30
    sget-object v0, Lcom/squareup/api/-$$Lambda$ClientSettingsCache$2CGEMd4g1O2lfEPOkU6uH6R7Fwk;->INSTANCE:Lcom/squareup/api/-$$Lambda$ClientSettingsCache$2CGEMd4g1O2lfEPOkU6uH6R7Fwk;

    sput-object v0, Lcom/squareup/api/ClientSettingsCache;->cacheEntryToClientSettings:Lrx/functions/Func1;

    return-void
.end method

.method public constructor <init>(Landroid/app/Application;Lcom/google/gson/Gson;Lcom/squareup/util/Clock;Lrx/Scheduler;Lrx/Scheduler;)V
    .locals 2
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p5    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Lcom/squareup/api/-$$Lambda$ClientSettingsCache$LR198xOxfoe_yWtVcj3Di3i8cZw;

    invoke-direct {v0, p0}, Lcom/squareup/api/-$$Lambda$ClientSettingsCache$LR198xOxfoe_yWtVcj3Di3i8cZw;-><init>(Lcom/squareup/api/ClientSettingsCache;)V

    iput-object v0, p0, Lcom/squareup/api/ClientSettingsCache;->nullOrExpired:Lrx/functions/Func1;

    .line 45
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/api/ClientSettingsCache;->clientSettingsMap:Ljava/util/concurrent/ConcurrentHashMap;

    const-string v0, "ClientSettings"

    const/4 v1, 0x0

    .line 47
    invoke-virtual {p1, v0, v1}, Landroid/app/Application;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/ClientSettingsCache;->clientSettingsCache:Landroid/content/SharedPreferences;

    .line 48
    iput-object p3, p0, Lcom/squareup/api/ClientSettingsCache;->clock:Lcom/squareup/util/Clock;

    .line 49
    iput-object p2, p0, Lcom/squareup/api/ClientSettingsCache;->gson:Lcom/google/gson/Gson;

    .line 50
    iput-object p4, p0, Lcom/squareup/api/ClientSettingsCache;->mainScheduler:Lrx/Scheduler;

    .line 51
    iput-object p5, p0, Lcom/squareup/api/ClientSettingsCache;->fileScheduler:Lrx/Scheduler;

    return-void
.end method

.method private getFromFile(Ljava/lang/String;)Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/api/ClientSettingsCache;->clientSettingsCache:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 118
    iget-object v0, p0, Lcom/squareup/api/ClientSettingsCache;->gson:Lcom/google/gson/Gson;

    const-class v1, Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    move-object v1, p1

    check-cast v1, Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;

    :cond_0
    return-object v1
.end method

.method private getFromMemory(Ljava/lang/String;)Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/api/ClientSettingsCache;->clientSettingsMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;

    return-object p1
.end method

.method static synthetic lambda$static$0(Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;)Lcom/squareup/server/api/ClientSettings;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;->clientSettings:Lcom/squareup/server/api/ClientSettings;

    return-object p0
.end method

.method private putToFile(Ljava/lang/String;Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;)V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/api/ClientSettingsCache;->clientSettingsCache:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/api/ClientSettingsCache;->gson:Lcom/google/gson/Gson;

    invoke-virtual {v1, p2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private putToMemory(Ljava/lang/String;Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;)V
    .locals 1

    .line 103
    iget-object v0, p0, Lcom/squareup/api/ClientSettingsCache;->clientSettingsMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public expired(Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;)Z
    .locals 4

    .line 124
    iget-object v0, p0, Lcom/squareup/api/ClientSettingsCache;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v0

    .line 125
    iget-wide v2, p1, Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;->updatedAt:J

    sub-long/2addr v0, v2

    sget-wide v2, Lcom/squareup/api/ClientSettingsCache;->ONE_DAY_MILLIS:J

    cmp-long p1, v0, v2

    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public get(Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/server/api/ClientSettings;",
            ">;"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/squareup/api/ClientSettingsCache;->clientSettingsMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    invoke-direct {p0, p1}, Lcom/squareup/api/ClientSettingsCache;->getFromMemory(Ljava/lang/String;)Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;

    move-result-object p1

    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/api/ClientSettingsCache;->nullOrExpired:Lrx/functions/Func1;

    .line 78
    invoke-virtual {p1, v0}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/api/ClientSettingsCache;->cacheEntryToClientSettings:Lrx/functions/Func1;

    .line 79
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 81
    :cond_0
    new-instance v0, Lcom/squareup/api/-$$Lambda$ClientSettingsCache$dXZ8jdzfzw-_P-pVy9GlA7D_MFo;

    invoke-direct {v0, p0, p1}, Lcom/squareup/api/-$$Lambda$ClientSettingsCache$dXZ8jdzfzw-_P-pVy9GlA7D_MFo;-><init>(Lcom/squareup/api/ClientSettingsCache;Ljava/lang/String;)V

    invoke-static {v0}, Lrx/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/api/ClientSettingsCache;->nullOrExpired:Lrx/functions/Func1;

    .line 82
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/api/ClientSettingsCache;->fileScheduler:Lrx/Scheduler;

    .line 83
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/api/ClientSettingsCache;->mainScheduler:Lrx/Scheduler;

    .line 84
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/api/-$$Lambda$ClientSettingsCache$AAC-8dX4LR7zgTsgixoWm_UbMyU;

    invoke-direct {v1, p0, p1}, Lcom/squareup/api/-$$Lambda$ClientSettingsCache$AAC-8dX4LR7zgTsgixoWm_UbMyU;-><init>(Lcom/squareup/api/ClientSettingsCache;Ljava/lang/String;)V

    .line 85
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnNext(Lrx/functions/Action1;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/api/ClientSettingsCache;->cacheEntryToClientSettings:Lrx/functions/Func1;

    .line 86
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$get$2$ClientSettingsCache(Ljava/lang/String;)Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 81
    invoke-direct {p0, p1}, Lcom/squareup/api/ClientSettingsCache;->getFromFile(Ljava/lang/String;)Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$get$3$ClientSettingsCache(Ljava/lang/String;Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;)V
    .locals 0

    .line 85
    invoke-direct {p0, p1, p2}, Lcom/squareup/api/ClientSettingsCache;->putToMemory(Ljava/lang/String;Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;)V

    return-void
.end method

.method public synthetic lambda$new$1$ClientSettingsCache(Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;)Ljava/lang/Boolean;
    .locals 0

    if-eqz p1, :cond_0

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/api/ClientSettingsCache;->expired(Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public put(Ljava/lang/String;Lcom/squareup/server/api/ClientSettings;)V
    .locals 3

    .line 91
    new-instance v0, Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;

    iget-object v1, p0, Lcom/squareup/api/ClientSettingsCache;->clock:Lcom/squareup/util/Clock;

    .line 92
    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, p2, v1, v2}, Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;-><init>(Lcom/squareup/server/api/ClientSettings;J)V

    .line 93
    invoke-direct {p0, p1, v0}, Lcom/squareup/api/ClientSettingsCache;->putToFile(Ljava/lang/String;Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;)V

    .line 94
    invoke-direct {p0, p1, v0}, Lcom/squareup/api/ClientSettingsCache;->putToMemory(Ljava/lang/String;Lcom/squareup/api/ClientSettingsCache$ClientSettingsCacheEntry;)V

    return-void
.end method

.method public remove(Ljava/lang/String;)V
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/api/ClientSettingsCache;->clientSettingsMap:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    iget-object v0, p0, Lcom/squareup/api/ClientSettingsCache;->clientSettingsCache:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method
