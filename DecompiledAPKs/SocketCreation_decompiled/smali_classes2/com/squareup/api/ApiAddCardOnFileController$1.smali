.class Lcom/squareup/api/ApiAddCardOnFileController$1;
.super Lcom/squareup/mortar/BundlerAdapter;
.source "ApiAddCardOnFileController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/api/ApiAddCardOnFileController;->getBundler()Lmortar/bundler/Bundler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/api/ApiAddCardOnFileController;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiAddCardOnFileController;Ljava/lang/String;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-direct {p0, p2}, Lcom/squareup/mortar/BundlerAdapter;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onEnterScope$0$ApiAddCardOnFileController$1(Lkotlin/Unit;)V
    .locals 7

    .line 92
    iget-object p1, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-virtual {p1}, Lcom/squareup/api/ApiAddCardOnFileController;->isApiAddCustomerCardRequest()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 93
    iget-object p1, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-static {p1}, Lcom/squareup/api/ApiAddCardOnFileController;->access$400(Lcom/squareup/api/ApiAddCardOnFileController;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    new-instance v6, Lcom/squareup/api/StoreCardCancelledEvent;

    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-static {v0}, Lcom/squareup/api/ApiAddCardOnFileController;->access$300(Lcom/squareup/api/ApiAddCardOnFileController;)Lcom/squareup/util/Clock;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-static {v0}, Lcom/squareup/api/ApiAddCardOnFileController;->access$000(Lcom/squareup/api/ApiAddCardOnFileController;)Lcom/squareup/api/ApiRequestController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->sequenceUuid()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    .line 94
    invoke-static {v0}, Lcom/squareup/api/ApiAddCardOnFileController;->access$000(Lcom/squareup/api/ApiAddCardOnFileController;)Lcom/squareup/api/ApiRequestController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->clientId()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-static {v0}, Lcom/squareup/api/ApiAddCardOnFileController;->access$000(Lcom/squareup/api/ApiAddCardOnFileController;)Lcom/squareup/api/ApiRequestController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/api/ApiRequestController;->requestStartTime()J

    move-result-wide v4

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/StoreCardCancelledEvent;-><init>(Lcom/squareup/util/Clock;Ljava/lang/String;Ljava/lang/String;J)V

    .line 93
    invoke-interface {p1, v6}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 95
    iget-object p1, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-static {p1}, Lcom/squareup/api/ApiAddCardOnFileController;->access$000(Lcom/squareup/api/ApiAddCardOnFileController;)Lcom/squareup/api/ApiRequestController;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/ApiRequestController;->getActivity()Landroid/app/Activity;

    move-result-object p1

    .line 96
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->ADD_CARD_ON_FILE_CANCELED:Lcom/squareup/api/ApiErrorResult;

    .line 97
    invoke-static {v0, v1}, Lcom/squareup/api/ApiAddCardOnFileController;->access$500(Lcom/squareup/api/ApiAddCardOnFileController;Lcom/squareup/api/ApiErrorResult;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    .line 96
    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 98
    iget-object p1, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-static {p1}, Lcom/squareup/api/ApiAddCardOnFileController;->access$000(Lcom/squareup/api/ApiAddCardOnFileController;)Lcom/squareup/api/ApiRequestController;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/ApiRequestController;->finish()V

    .line 99
    iget-object p1, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-static {p1, v1}, Lcom/squareup/api/ApiAddCardOnFileController;->access$202(Lcom/squareup/api/ApiAddCardOnFileController;Z)Z

    :cond_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 91
    iget-object p1, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-static {p1}, Lcom/squareup/api/ApiAddCardOnFileController;->access$000(Lcom/squareup/api/ApiAddCardOnFileController;)Lcom/squareup/api/ApiRequestController;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/api/ApiRequestController;->onStaleApiRequest()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/api/-$$Lambda$ApiAddCardOnFileController$1$Ujb-zge_L0V7FBBbsEUqMfkeR3E;

    invoke-direct {v0, p0}, Lcom/squareup/api/-$$Lambda$ApiAddCardOnFileController$1$Ujb-zge_L0V7FBBbsEUqMfkeR3E;-><init>(Lcom/squareup/api/ApiAddCardOnFileController$1;)V

    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-static {v0}, Lcom/squareup/api/ApiAddCardOnFileController;->access$100(Lcom/squareup/api/ApiAddCardOnFileController;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/api/ApiAddCardOnFileController;->access$102(Lcom/squareup/api/ApiAddCardOnFileController;Z)Z

    if-eqz p1, :cond_1

    .line 111
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    const-string v1, "AddCustomerCardApiRequestInFlight"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/api/ApiAddCardOnFileController;->access$202(Lcom/squareup/api/ApiAddCardOnFileController;Z)Z

    :cond_1
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/api/ApiAddCardOnFileController$1;->this$0:Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-static {v0}, Lcom/squareup/api/ApiAddCardOnFileController;->access$200(Lcom/squareup/api/ApiAddCardOnFileController;)Z

    move-result v0

    const-string v1, "AddCustomerCardApiRequestInFlight"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method
