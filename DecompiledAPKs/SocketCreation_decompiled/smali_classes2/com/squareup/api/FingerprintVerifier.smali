.class public Lcom/squareup/api/FingerprintVerifier;
.super Ljava/lang/Object;
.source "FingerprintVerifier.java"


# instance fields
.field private final packageManager:Landroid/content/pm/PackageManager;


# direct methods
.method constructor <init>(Landroid/content/pm/PackageManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/api/FingerprintVerifier;->packageManager:Landroid/content/pm/PackageManager;

    return-void
.end method

.method private bytesToHexString([B)Ljava/lang/String;
    .locals 5

    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    .line 121
    aget-byte v3, p1, v2

    and-int/lit16 v3, v3, 0xff

    const/16 v4, 0x10

    if-ge v3, v4, :cond_0

    const/16 v4, 0x30

    .line 123
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 125
    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v1, -0x1

    if-eq v2, v3, :cond_1

    const/16 v3, 0x3a

    .line 127
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 130
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public computeAppFingerprints(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/squareup/api/ApiValidationException;
        }
    .end annotation

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 73
    :try_start_0
    iget-object v1, p0, Lcom/squareup/api/FingerprintVerifier;->packageManager:Landroid/content/pm/PackageManager;

    const/16 v2, 0x40

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_4

    .line 78
    iget-object p1, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz p1, :cond_1

    .line 79
    array-length v1, p1

    if-eqz v1, :cond_1

    :try_start_1
    const-string v1, "X509"

    .line 85
    invoke-static {v1}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1
    :try_end_1
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_3

    :try_start_2
    const-string v2, "SHA1"

    .line 91
    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_2

    .line 95
    array-length v3, p1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    aget-object v5, p1, v4

    .line 96
    invoke-virtual {v5}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v5

    .line 97
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 100
    :try_start_3
    invoke-virtual {v1, v6}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v5

    check-cast v5, Ljava/security/cert/X509Certificate;
    :try_end_3
    .catch Ljava/security/cert/CertificateException; {:try_start_3 .. :try_end_3} :catch_1

    .line 106
    :try_start_4
    invoke-virtual {v5}, Ljava/security/cert/X509Certificate;->getEncoded()[B

    move-result-object v5
    :try_end_4
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_4 .. :try_end_4} :catch_0

    .line 110
    invoke-virtual {v2, v5}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v5

    .line 111
    invoke-direct {p0, v5}, Lcom/squareup/api/FingerprintVerifier;->bytesToHexString([B)Ljava/lang/String;

    move-result-object v5

    .line 112
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 108
    :catch_0
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->CANNOT_ENCODE_CERTIFICATE:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 102
    :catch_1
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->CANNOT_PARSE_CERTIFICATE:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    :cond_0
    return-object v0

    .line 93
    :catch_2
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->SHA1_NOT_AVAILABLE:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 87
    :catch_3
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->X509_NOT_AVAILABLE:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 80
    :cond_1
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_SIGNATURES:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 75
    :catch_4
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->APPLICATION_NOT_FOUND:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1
.end method

.method public validateFingerprint(Ljava/lang/String;Lcom/squareup/server/api/ClientSettings;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/squareup/api/ApiValidationException;
        }
    .end annotation

    .line 39
    iget-object p2, p2, Lcom/squareup/server/api/ClientSettings;->commerce_api:Lcom/squareup/server/api/ClientSettings$CommerceApi;

    iget-object p2, p2, Lcom/squareup/server/api/ClientSettings$CommerceApi;->android_packages:Ljava/util/List;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 42
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/api/ClientSettings$AndroidPackage;

    .line 43
    iget-object v2, v1, Lcom/squareup/server/api/ClientSettings$AndroidPackage;->package_name:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 44
    iget-object v1, v1, Lcom/squareup/server/api/ClientSettings$AndroidPackage;->fingerprint:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 48
    :cond_1
    invoke-virtual {p0, p1}, Lcom/squareup/api/FingerprintVerifier;->computeAppFingerprints(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    .line 52
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    .line 53
    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_2

    const/4 p1, 0x0

    return p1

    :cond_3
    const/4 p1, 0x1

    return p1
.end method
