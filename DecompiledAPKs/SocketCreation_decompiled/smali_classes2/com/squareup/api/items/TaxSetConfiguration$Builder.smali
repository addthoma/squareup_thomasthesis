.class public final Lcom/squareup/api/items/TaxSetConfiguration$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TaxSetConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/TaxSetConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/TaxSetConfiguration;",
        "Lcom/squareup/api/items/TaxSetConfiguration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public filter:Lcom/squareup/api/items/ObjectFilter;

.field public tax:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 103
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/TaxSetConfiguration$Builder;->tax:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/TaxSetConfiguration;
    .locals 4

    .line 122
    new-instance v0, Lcom/squareup/api/items/TaxSetConfiguration;

    iget-object v1, p0, Lcom/squareup/api/items/TaxSetConfiguration$Builder;->filter:Lcom/squareup/api/items/ObjectFilter;

    iget-object v2, p0, Lcom/squareup/api/items/TaxSetConfiguration$Builder;->tax:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/api/items/TaxSetConfiguration;-><init>(Lcom/squareup/api/items/ObjectFilter;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/api/items/TaxSetConfiguration$Builder;->build()Lcom/squareup/api/items/TaxSetConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public filter(Lcom/squareup/api/items/ObjectFilter;)Lcom/squareup/api/items/TaxSetConfiguration$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/api/items/TaxSetConfiguration$Builder;->filter:Lcom/squareup/api/items/ObjectFilter;

    return-object p0
.end method

.method public tax(Ljava/util/List;)Lcom/squareup/api/items/TaxSetConfiguration$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)",
            "Lcom/squareup/api/items/TaxSetConfiguration$Builder;"
        }
    .end annotation

    .line 115
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 116
    iput-object p1, p0, Lcom/squareup/api/items/TaxSetConfiguration$Builder;->tax:Ljava/util/List;

    return-object p0
.end method
