.class public final Lcom/squareup/api/items/Item$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Item.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/Item;",
        "Lcom/squareup/api/items/Item$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public abbreviation:Ljava/lang/String;

.field public archetype:Lcom/squareup/api/sync/ObjectId;

.field public available_electronically:Ljava/lang/Boolean;

.field public available_for_pickup:Ljava/lang/Boolean;

.field public available_online:Ljava/lang/Boolean;

.field public buyer_facing_name:Ljava/lang/String;

.field public catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

.field public color:Ljava/lang/String;

.field public custom_attribute_values:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
            ">;"
        }
    .end annotation
.end field

.field public description:Ljava/lang/String;

.field public ecom_available:Ljava/lang/Boolean;

.field public ecom_buy_button_text:Ljava/lang/String;

.field public ecom_image_uris:Ljava/lang/String;

.field public ecom_uri:Ljava/lang/String;

.field public ecom_visibility:Lcom/squareup/api/items/Item$EcomVisibility;

.field public id:Ljava/lang/String;

.field public image:Lcom/squareup/api/sync/ObjectId;

.field public item_options:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemOptionForItem;",
            ">;"
        }
    .end annotation
.end field

.field public menu_category:Lcom/squareup/api/sync/ObjectId;

.field public name:Ljava/lang/String;

.field public online_store_item_data:Lcom/squareup/protos/items/OnlineStoreItemData;

.field public ordinal:Ljava/lang/Integer;

.field public pricing_type:Lcom/squareup/api/items/PricingType;

.field public skips_modifier_screen:Ljava/lang/Boolean;

.field public straight_fire_override:Lcom/squareup/api/items/StraightFireType;

.field public tag_membership:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;"
        }
    .end annotation
.end field

.field public taxable:Ljava/lang/Boolean;

.field public type:Lcom/squareup/api/items/Item$Type;

.field public v2_id:Ljava/lang/String;

.field public visibility:Lcom/squareup/api/items/Visibility;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 578
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 579
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/Item$Builder;->tag_membership:Ljava/util/List;

    .line 580
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/Item$Builder;->item_options:Ljava/util/List;

    .line 581
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/items/Item$Builder;->custom_attribute_values:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public abbreviation(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 624
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->abbreviation:Ljava/lang/String;

    return-object p0
.end method

.method public archetype(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 655
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->archetype:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public available_electronically(Ljava/lang/Boolean;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 680
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->available_electronically:Ljava/lang/Boolean;

    return-object p0
.end method

.method public available_for_pickup(Ljava/lang/Boolean;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 672
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->available_for_pickup:Ljava/lang/Boolean;

    return-object p0
.end method

.method public available_online(Ljava/lang/Boolean;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 663
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->available_online:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/api/items/Item;
    .locals 2

    .line 813
    new-instance v0, Lcom/squareup/api/items/Item;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/api/items/Item;-><init>(Lcom/squareup/api/items/Item$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 517
    invoke-virtual {p0}, Lcom/squareup/api/items/Item$Builder;->build()Lcom/squareup/api/items/Item;

    move-result-object v0

    return-object v0
.end method

.method public buyer_facing_name(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 611
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->buyer_facing_name:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_object_reference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 721
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    return-object p0
.end method

.method public color(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 593
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->color:Ljava/lang/String;

    return-object p0
.end method

.method public custom_attribute_values(Ljava/util/List;)Lcom/squareup/api/items/Item$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/merchant_catalog/resources/CatalogCustomAttributeValue;",
            ">;)",
            "Lcom/squareup/api/items/Item$Builder;"
        }
    .end annotation

    .line 793
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 794
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->custom_attribute_values:Ljava/util/List;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 598
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public ecom_available(Ljava/lang/Boolean;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 775
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->ecom_available:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ecom_buy_button_text(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 802
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->ecom_buy_button_text:Ljava/lang/String;

    return-object p0
.end method

.method public ecom_image_uris(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 767
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->ecom_image_uris:Ljava/lang/String;

    return-object p0
.end method

.method public ecom_uri(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 759
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->ecom_uri:Ljava/lang/String;

    return-object p0
.end method

.method public ecom_visibility(Lcom/squareup/api/items/Item$EcomVisibility;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 807
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->ecom_visibility:Lcom/squareup/api/items/Item$EcomVisibility;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 585
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public image(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 642
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->image:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public item_options(Ljava/util/List;)Lcom/squareup/api/items/Item$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/ItemOptionForItem;",
            ">;)",
            "Lcom/squareup/api/items/Item$Builder;"
        }
    .end annotation

    .line 783
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 784
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->item_options:Ljava/util/List;

    return-object p0
.end method

.method public menu_category(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 637
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->menu_category:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 603
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public online_store_item_data(Lcom/squareup/protos/items/OnlineStoreItemData;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 703
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->online_store_item_data:Lcom/squareup/protos/items/OnlineStoreItemData;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 690
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public pricing_type(Lcom/squareup/api/items/PricingType;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 650
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->pricing_type:Lcom/squareup/api/items/PricingType;

    return-object p0
.end method

.method public skips_modifier_screen(Ljava/lang/Boolean;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 715
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->skips_modifier_screen:Ljava/lang/Boolean;

    return-object p0
.end method

.method public straight_fire_override(Lcom/squareup/api/items/StraightFireType;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 741
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->straight_fire_override:Lcom/squareup/api/items/StraightFireType;

    return-object p0
.end method

.method public tag_membership(Ljava/util/List;)Lcom/squareup/api/items/Item$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectId;",
            ">;)",
            "Lcom/squareup/api/items/Item$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 730
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 731
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->tag_membership:Ljava/util/List;

    return-object p0
.end method

.method public taxable(Ljava/lang/Boolean;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 616
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->taxable:Ljava/lang/Boolean;

    return-object p0
.end method

.method public type(Lcom/squareup/api/items/Item$Type;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 698
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->type:Lcom/squareup/api/items/Item$Type;

    return-object p0
.end method

.method public v2_id(Ljava/lang/String;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 751
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->v2_id:Ljava/lang/String;

    return-object p0
.end method

.method public visibility(Lcom/squareup/api/items/Visibility;)Lcom/squareup/api/items/Item$Builder;
    .locals 0

    .line 632
    iput-object p1, p0, Lcom/squareup/api/items/Item$Builder;->visibility:Lcom/squareup/api/items/Visibility;

    return-object p0
.end method
