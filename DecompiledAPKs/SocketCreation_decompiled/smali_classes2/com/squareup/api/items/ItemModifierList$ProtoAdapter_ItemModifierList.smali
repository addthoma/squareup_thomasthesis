.class final Lcom/squareup/api/items/ItemModifierList$ProtoAdapter_ItemModifierList;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ItemModifierList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/ItemModifierList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ItemModifierList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/api/items/ItemModifierList;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 297
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/api/items/ItemModifierList;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/items/ItemModifierList;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 326
    new-instance v0, Lcom/squareup/api/items/ItemModifierList$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemModifierList$Builder;-><init>()V

    .line 327
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 328
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 345
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 343
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/ItemModifierList$Builder;->v2_id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    goto :goto_0

    .line 342
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/ItemModifierList$Builder;->is_conversational(Ljava/lang/Boolean;)Lcom/squareup/api/items/ItemModifierList$Builder;

    goto :goto_0

    .line 341
    :pswitch_2
    sget-object v3, Lcom/squareup/api/items/MerchantCatalogObjectReference;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/MerchantCatalogObjectReference;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/ItemModifierList$Builder;->catalog_object_reference(Lcom/squareup/api/items/MerchantCatalogObjectReference;)Lcom/squareup/api/items/ItemModifierList$Builder;

    goto :goto_0

    .line 340
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/ItemModifierList$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    goto :goto_0

    .line 334
    :pswitch_4
    :try_start_0
    sget-object v4, Lcom/squareup/api/items/ItemModifierList$SelectionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/ItemModifierList$SelectionType;

    invoke-virtual {v0, v4}, Lcom/squareup/api/items/ItemModifierList$Builder;->selection_type(Lcom/squareup/api/items/ItemModifierList$SelectionType;)Lcom/squareup/api/items/ItemModifierList$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 336
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/api/items/ItemModifierList$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 331
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/ItemModifierList$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/ItemModifierList$Builder;

    goto :goto_0

    .line 330
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/api/items/ItemModifierList$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/ItemModifierList$Builder;

    goto :goto_0

    .line 349
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/ItemModifierList$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 350
    invoke-virtual {v0}, Lcom/squareup/api/items/ItemModifierList$Builder;->build()Lcom/squareup/api/items/ItemModifierList;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 295
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ItemModifierList$ProtoAdapter_ItemModifierList;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/api/items/ItemModifierList;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/items/ItemModifierList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 314
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemModifierList;->id:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 315
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemModifierList;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 316
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemModifierList;->ordinal:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 317
    sget-object v0, Lcom/squareup/api/items/ItemModifierList$SelectionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemModifierList;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 318
    sget-object v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemModifierList;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 319
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemModifierList;->is_conversational:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 320
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/api/items/ItemModifierList;->v2_id:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 321
    invoke-virtual {p2}, Lcom/squareup/api/items/ItemModifierList;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 295
    check-cast p2, Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/api/items/ItemModifierList$ProtoAdapter_ItemModifierList;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/api/items/ItemModifierList;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/api/items/ItemModifierList;)I
    .locals 4

    .line 302
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/ItemModifierList;->id:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/ItemModifierList;->name:Ljava/lang/String;

    const/4 v3, 0x1

    .line 303
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/ItemModifierList;->ordinal:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 304
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/ItemModifierList$SelectionType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/ItemModifierList;->selection_type:Lcom/squareup/api/items/ItemModifierList$SelectionType;

    const/4 v3, 0x3

    .line 305
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/api/items/MerchantCatalogObjectReference;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/ItemModifierList;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    const/4 v3, 0x5

    .line 306
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/ItemModifierList;->is_conversational:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 307
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/api/items/ItemModifierList;->v2_id:Ljava/lang/String;

    const/4 v3, 0x7

    .line 308
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 309
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemModifierList;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 295
    check-cast p1, Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ItemModifierList$ProtoAdapter_ItemModifierList;->encodedSize(Lcom/squareup/api/items/ItemModifierList;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/api/items/ItemModifierList;
    .locals 2

    .line 355
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemModifierList;->newBuilder()Lcom/squareup/api/items/ItemModifierList$Builder;

    move-result-object p1

    .line 356
    iget-object v0, p1, Lcom/squareup/api/items/ItemModifierList$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/api/items/ItemModifierList$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iput-object v0, p1, Lcom/squareup/api/items/ItemModifierList$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 357
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemModifierList$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 358
    invoke-virtual {p1}, Lcom/squareup/api/items/ItemModifierList$Builder;->build()Lcom/squareup/api/items/ItemModifierList;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 295
    check-cast p1, Lcom/squareup/api/items/ItemModifierList;

    invoke-virtual {p0, p1}, Lcom/squareup/api/items/ItemModifierList$ProtoAdapter_ItemModifierList;->redact(Lcom/squareup/api/items/ItemModifierList;)Lcom/squareup/api/items/ItemModifierList;

    move-result-object p1

    return-object p1
.end method
