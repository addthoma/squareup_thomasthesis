.class public final Lcom/squareup/api/items/TryToUsePromoRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TryToUsePromoRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/TryToUsePromoRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/TryToUsePromoRequest;",
        "Lcom/squareup/api/items/TryToUsePromoRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public delta:Ljava/lang/Integer;

.field public merchant_token:Ljava/lang/String;

.field public promo_code:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 117
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/TryToUsePromoRequest;
    .locals 5

    .line 140
    new-instance v0, Lcom/squareup/api/items/TryToUsePromoRequest;

    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest$Builder;->promo_code:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/TryToUsePromoRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/TryToUsePromoRequest$Builder;->delta:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/api/items/TryToUsePromoRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 110
    invoke-virtual {p0}, Lcom/squareup/api/items/TryToUsePromoRequest$Builder;->build()Lcom/squareup/api/items/TryToUsePromoRequest;

    move-result-object v0

    return-object v0
.end method

.method public delta(Ljava/lang/Integer;)Lcom/squareup/api/items/TryToUsePromoRequest$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lcom/squareup/api/items/TryToUsePromoRequest$Builder;->delta:Ljava/lang/Integer;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/api/items/TryToUsePromoRequest$Builder;
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/api/items/TryToUsePromoRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public promo_code(Ljava/lang/String;)Lcom/squareup/api/items/TryToUsePromoRequest$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lcom/squareup/api/items/TryToUsePromoRequest$Builder;->promo_code:Ljava/lang/String;

    return-object p0
.end method
