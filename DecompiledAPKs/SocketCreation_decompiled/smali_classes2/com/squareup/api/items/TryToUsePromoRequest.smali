.class public final Lcom/squareup/api/items/TryToUsePromoRequest;
.super Lcom/squareup/wire/Message;
.source "TryToUsePromoRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/TryToUsePromoRequest$ProtoAdapter_TryToUsePromoRequest;,
        Lcom/squareup/api/items/TryToUsePromoRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/TryToUsePromoRequest;",
        "Lcom/squareup/api/items/TryToUsePromoRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/TryToUsePromoRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DELTA:Ljava/lang/Integer;

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_PROMO_CODE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final delta:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final promo_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/api/items/TryToUsePromoRequest$ProtoAdapter_TryToUsePromoRequest;

    invoke-direct {v0}, Lcom/squareup/api/items/TryToUsePromoRequest$ProtoAdapter_TryToUsePromoRequest;-><init>()V

    sput-object v0, Lcom/squareup/api/items/TryToUsePromoRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x1

    .line 32
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/TryToUsePromoRequest;->DEFAULT_DELTA:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 1

    .line 56
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/api/items/TryToUsePromoRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 61
    sget-object v0, Lcom/squareup/api/items/TryToUsePromoRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 62
    iput-object p1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->promo_code:Ljava/lang/String;

    .line 63
    iput-object p2, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->merchant_token:Ljava/lang/String;

    .line 64
    iput-object p3, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->delta:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 80
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/TryToUsePromoRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 81
    :cond_1
    check-cast p1, Lcom/squareup/api/items/TryToUsePromoRequest;

    .line 82
    invoke-virtual {p0}, Lcom/squareup/api/items/TryToUsePromoRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/TryToUsePromoRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->promo_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/TryToUsePromoRequest;->promo_code:Ljava/lang/String;

    .line 83
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/TryToUsePromoRequest;->merchant_token:Ljava/lang/String;

    .line 84
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->delta:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/api/items/TryToUsePromoRequest;->delta:Ljava/lang/Integer;

    .line 85
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 90
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 92
    invoke-virtual {p0}, Lcom/squareup/api/items/TryToUsePromoRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 93
    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->promo_code:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 94
    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->delta:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 96
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/TryToUsePromoRequest$Builder;
    .locals 2

    .line 69
    new-instance v0, Lcom/squareup/api/items/TryToUsePromoRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/TryToUsePromoRequest$Builder;-><init>()V

    .line 70
    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->promo_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/TryToUsePromoRequest$Builder;->promo_code:Ljava/lang/String;

    .line 71
    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/TryToUsePromoRequest$Builder;->merchant_token:Ljava/lang/String;

    .line 72
    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->delta:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/TryToUsePromoRequest$Builder;->delta:Ljava/lang/Integer;

    .line 73
    invoke-virtual {p0}, Lcom/squareup/api/items/TryToUsePromoRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TryToUsePromoRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/api/items/TryToUsePromoRequest;->newBuilder()Lcom/squareup/api/items/TryToUsePromoRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->promo_code:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", promo_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->promo_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->delta:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", delta="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TryToUsePromoRequest;->delta:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TryToUsePromoRequest{"

    .line 107
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
