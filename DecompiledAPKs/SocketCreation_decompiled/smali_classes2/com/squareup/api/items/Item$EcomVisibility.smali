.class public final enum Lcom/squareup/api/items/Item$EcomVisibility;
.super Ljava/lang/Enum;
.source "Item.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EcomVisibility"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Item$EcomVisibility$ProtoAdapter_EcomVisibility;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/Item$EcomVisibility;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/Item$EcomVisibility;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Item$EcomVisibility;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum HIDDEN:Lcom/squareup/api/items/Item$EcomVisibility;

.field public static final enum UNAVAILABLE:Lcom/squareup/api/items/Item$EcomVisibility;

.field public static final enum UNINDEXED:Lcom/squareup/api/items/Item$EcomVisibility;

.field public static final enum VISIBLE:Lcom/squareup/api/items/Item$EcomVisibility;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 888
    new-instance v0, Lcom/squareup/api/items/Item$EcomVisibility;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "UNINDEXED"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/api/items/Item$EcomVisibility;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Item$EcomVisibility;->UNINDEXED:Lcom/squareup/api/items/Item$EcomVisibility;

    .line 893
    new-instance v0, Lcom/squareup/api/items/Item$EcomVisibility;

    const/4 v3, 0x2

    const-string v4, "UNAVAILABLE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/api/items/Item$EcomVisibility;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Item$EcomVisibility;->UNAVAILABLE:Lcom/squareup/api/items/Item$EcomVisibility;

    .line 898
    new-instance v0, Lcom/squareup/api/items/Item$EcomVisibility;

    const/4 v4, 0x3

    const-string v5, "HIDDEN"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/api/items/Item$EcomVisibility;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Item$EcomVisibility;->HIDDEN:Lcom/squareup/api/items/Item$EcomVisibility;

    .line 903
    new-instance v0, Lcom/squareup/api/items/Item$EcomVisibility;

    const/4 v5, 0x4

    const-string v6, "VISIBLE"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/api/items/Item$EcomVisibility;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Item$EcomVisibility;->VISIBLE:Lcom/squareup/api/items/Item$EcomVisibility;

    new-array v0, v5, [Lcom/squareup/api/items/Item$EcomVisibility;

    .line 884
    sget-object v5, Lcom/squareup/api/items/Item$EcomVisibility;->UNINDEXED:Lcom/squareup/api/items/Item$EcomVisibility;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/api/items/Item$EcomVisibility;->UNAVAILABLE:Lcom/squareup/api/items/Item$EcomVisibility;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/api/items/Item$EcomVisibility;->HIDDEN:Lcom/squareup/api/items/Item$EcomVisibility;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/api/items/Item$EcomVisibility;->VISIBLE:Lcom/squareup/api/items/Item$EcomVisibility;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/api/items/Item$EcomVisibility;->$VALUES:[Lcom/squareup/api/items/Item$EcomVisibility;

    .line 905
    new-instance v0, Lcom/squareup/api/items/Item$EcomVisibility$ProtoAdapter_EcomVisibility;

    invoke-direct {v0}, Lcom/squareup/api/items/Item$EcomVisibility$ProtoAdapter_EcomVisibility;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Item$EcomVisibility;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 909
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 910
    iput p3, p0, Lcom/squareup/api/items/Item$EcomVisibility;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/Item$EcomVisibility;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 921
    :cond_0
    sget-object p0, Lcom/squareup/api/items/Item$EcomVisibility;->VISIBLE:Lcom/squareup/api/items/Item$EcomVisibility;

    return-object p0

    .line 920
    :cond_1
    sget-object p0, Lcom/squareup/api/items/Item$EcomVisibility;->HIDDEN:Lcom/squareup/api/items/Item$EcomVisibility;

    return-object p0

    .line 919
    :cond_2
    sget-object p0, Lcom/squareup/api/items/Item$EcomVisibility;->UNAVAILABLE:Lcom/squareup/api/items/Item$EcomVisibility;

    return-object p0

    .line 918
    :cond_3
    sget-object p0, Lcom/squareup/api/items/Item$EcomVisibility;->UNINDEXED:Lcom/squareup/api/items/Item$EcomVisibility;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/Item$EcomVisibility;
    .locals 1

    .line 884
    const-class v0, Lcom/squareup/api/items/Item$EcomVisibility;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/Item$EcomVisibility;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/Item$EcomVisibility;
    .locals 1

    .line 884
    sget-object v0, Lcom/squareup/api/items/Item$EcomVisibility;->$VALUES:[Lcom/squareup/api/items/Item$EcomVisibility;

    invoke-virtual {v0}, [Lcom/squareup/api/items/Item$EcomVisibility;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/Item$EcomVisibility;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 928
    iget v0, p0, Lcom/squareup/api/items/Item$EcomVisibility;->value:I

    return v0
.end method
