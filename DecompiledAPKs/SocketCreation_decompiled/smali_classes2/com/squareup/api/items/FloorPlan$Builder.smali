.class public final Lcom/squareup/api/items/FloorPlan$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FloorPlan.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/FloorPlan;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/FloorPlan;",
        "Lcom/squareup/api/items/FloorPlan$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public height:Ljava/lang/Integer;

.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public ordinal:Ljava/lang/Integer;

.field public width:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 155
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/FloorPlan;
    .locals 8

    .line 194
    new-instance v7, Lcom/squareup/api/items/FloorPlan;

    iget-object v1, p0, Lcom/squareup/api/items/FloorPlan$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/FloorPlan$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/FloorPlan$Builder;->width:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/api/items/FloorPlan$Builder;->height:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/api/items/FloorPlan$Builder;->ordinal:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/api/items/FloorPlan;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 144
    invoke-virtual {p0}, Lcom/squareup/api/items/FloorPlan$Builder;->build()Lcom/squareup/api/items/FloorPlan;

    move-result-object v0

    return-object v0
.end method

.method public height(Ljava/lang/Integer;)Lcom/squareup/api/items/FloorPlan$Builder;
    .locals 0

    .line 179
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlan$Builder;->height:Ljava/lang/Integer;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/FloorPlan$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlan$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/FloorPlan$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlan$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/FloorPlan$Builder;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlan$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public width(Ljava/lang/Integer;)Lcom/squareup/api/items/FloorPlan$Builder;
    .locals 0

    .line 174
    iput-object p1, p0, Lcom/squareup/api/items/FloorPlan$Builder;->width:Ljava/lang/Integer;

    return-object p0
.end method
