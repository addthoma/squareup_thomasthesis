.class public final Lcom/squareup/api/items/ItemModifierOption;
.super Lcom/squareup/wire/Message;
.source "ItemModifierOption.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/ItemModifierOption$ProtoAdapter_ItemModifierOption;,
        Lcom/squareup/api/items/ItemModifierOption$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/ItemModifierOption;",
        "Lcom/squareup/api/items/ItemModifierOption$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/ItemModifierOption;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ON_BY_DEFAULT:Ljava/lang/Boolean;

.field public static final DEFAULT_ORDINAL:Ljava/lang/Integer;

.field public static final DEFAULT_V2_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.MerchantCatalogObjectReference#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final modifier_list:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final on_by_default:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final ordinal:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final price:Lcom/squareup/protos/common/dinero/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.dinero.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final v2_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 24
    new-instance v0, Lcom/squareup/api/items/ItemModifierOption$ProtoAdapter_ItemModifierOption;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemModifierOption$ProtoAdapter_ItemModifierOption;-><init>()V

    sput-object v0, Lcom/squareup/api/items/ItemModifierOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 30
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/api/items/ItemModifierOption;->DEFAULT_ON_BY_DEFAULT:Ljava/lang/Boolean;

    .line 34
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/items/ItemModifierOption;->DEFAULT_ORDINAL:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/String;)V
    .locals 10

    .line 100
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/api/items/ItemModifierOption;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/dinero/Money;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/sync/ObjectId;Lcom/squareup/api/items/MerchantCatalogObjectReference;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 107
    sget-object v0, Lcom/squareup/api/items/ItemModifierOption;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 108
    iput-object p1, p0, Lcom/squareup/api/items/ItemModifierOption;->id:Ljava/lang/String;

    .line 109
    iput-object p2, p0, Lcom/squareup/api/items/ItemModifierOption;->price:Lcom/squareup/protos/common/dinero/Money;

    .line 110
    iput-object p3, p0, Lcom/squareup/api/items/ItemModifierOption;->on_by_default:Ljava/lang/Boolean;

    .line 111
    iput-object p4, p0, Lcom/squareup/api/items/ItemModifierOption;->name:Ljava/lang/String;

    .line 112
    iput-object p5, p0, Lcom/squareup/api/items/ItemModifierOption;->ordinal:Ljava/lang/Integer;

    .line 113
    iput-object p6, p0, Lcom/squareup/api/items/ItemModifierOption;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    .line 114
    iput-object p7, p0, Lcom/squareup/api/items/ItemModifierOption;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 115
    iput-object p8, p0, Lcom/squareup/api/items/ItemModifierOption;->v2_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 136
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/ItemModifierOption;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 137
    :cond_1
    check-cast p1, Lcom/squareup/api/items/ItemModifierOption;

    .line 138
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierOption;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/ItemModifierOption;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ItemModifierOption;->id:Ljava/lang/String;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->price:Lcom/squareup/protos/common/dinero/Money;

    iget-object v3, p1, Lcom/squareup/api/items/ItemModifierOption;->price:Lcom/squareup/protos/common/dinero/Money;

    .line 140
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->on_by_default:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/items/ItemModifierOption;->on_by_default:Ljava/lang/Boolean;

    .line 141
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/ItemModifierOption;->name:Ljava/lang/String;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->ordinal:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/items/ItemModifierOption;->ordinal:Ljava/lang/Integer;

    .line 143
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/api/items/ItemModifierOption;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    .line 144
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iget-object v3, p1, Lcom/squareup/api/items/ItemModifierOption;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 145
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->v2_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/api/items/ItemModifierOption;->v2_id:Ljava/lang/String;

    .line 146
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 151
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 153
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierOption;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 154
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 155
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->price:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/dinero/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->on_by_default:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 159
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 160
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/api/items/MerchantCatalogObjectReference;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 161
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->v2_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 162
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/ItemModifierOption$Builder;
    .locals 2

    .line 120
    new-instance v0, Lcom/squareup/api/items/ItemModifierOption$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/ItemModifierOption$Builder;-><init>()V

    .line 121
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierOption$Builder;->id:Ljava/lang/String;

    .line 122
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->price:Lcom/squareup/protos/common/dinero/Money;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierOption$Builder;->price:Lcom/squareup/protos/common/dinero/Money;

    .line 123
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->on_by_default:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierOption$Builder;->on_by_default:Ljava/lang/Boolean;

    .line 124
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierOption$Builder;->name:Ljava/lang/String;

    .line 125
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->ordinal:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierOption$Builder;->ordinal:Ljava/lang/Integer;

    .line 126
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierOption$Builder;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    .line 127
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierOption$Builder;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    .line 128
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->v2_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/ItemModifierOption$Builder;->v2_id:Ljava/lang/String;

    .line 129
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierOption;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/ItemModifierOption$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/api/items/ItemModifierOption;->newBuilder()Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 169
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 170
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->price:Lcom/squareup/protos/common/dinero/Money;

    if-eqz v1, :cond_1

    const-string v1, ", price="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->price:Lcom/squareup/protos/common/dinero/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 172
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->on_by_default:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", on_by_default="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->on_by_default:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 173
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->ordinal:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", ordinal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->ordinal:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 175
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_5

    const-string v1, ", modifier_list="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->modifier_list:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 176
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    if-eqz v1, :cond_6

    const-string v1, ", catalog_object_reference="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->catalog_object_reference:Lcom/squareup/api/items/MerchantCatalogObjectReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 177
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->v2_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", v2_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/ItemModifierOption;->v2_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ItemModifierOption{"

    .line 178
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
