.class final Lcom/squareup/api/items/Fee$AdjustmentType$ProtoAdapter_AdjustmentType;
.super Lcom/squareup/wire/EnumAdapter;
.source "Fee.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Fee$AdjustmentType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AdjustmentType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/api/items/Fee$AdjustmentType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 462
    const-class v0, Lcom/squareup/api/items/Fee$AdjustmentType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/api/items/Fee$AdjustmentType;
    .locals 0

    .line 467
    invoke-static {p1}, Lcom/squareup/api/items/Fee$AdjustmentType;->fromValue(I)Lcom/squareup/api/items/Fee$AdjustmentType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 460
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/Fee$AdjustmentType$ProtoAdapter_AdjustmentType;->fromValue(I)Lcom/squareup/api/items/Fee$AdjustmentType;

    move-result-object p1

    return-object p1
.end method
