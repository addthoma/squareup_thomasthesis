.class final Lcom/squareup/api/items/Visibility$ProtoAdapter_Visibility;
.super Lcom/squareup/wire/EnumAdapter;
.source "Visibility.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Visibility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Visibility"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/api/items/Visibility;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 58
    const-class v0, Lcom/squareup/api/items/Visibility;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/api/items/Visibility;
    .locals 0

    .line 63
    invoke-static {p1}, Lcom/squareup/api/items/Visibility;->fromValue(I)Lcom/squareup/api/items/Visibility;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 56
    invoke-virtual {p0, p1}, Lcom/squareup/api/items/Visibility$ProtoAdapter_Visibility;->fromValue(I)Lcom/squareup/api/items/Visibility;

    move-result-object p1

    return-object p1
.end method
