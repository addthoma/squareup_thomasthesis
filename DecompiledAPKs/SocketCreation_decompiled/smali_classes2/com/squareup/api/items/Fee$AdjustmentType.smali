.class public final enum Lcom/squareup/api/items/Fee$AdjustmentType;
.super Ljava/lang/Enum;
.source "Fee.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/Fee;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AdjustmentType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/Fee$AdjustmentType$ProtoAdapter_AdjustmentType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/Fee$AdjustmentType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/Fee$AdjustmentType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/Fee$AdjustmentType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum OBSOLETE_SURCHARGE:Lcom/squareup/api/items/Fee$AdjustmentType;

.field public static final enum TAX:Lcom/squareup/api/items/Fee$AdjustmentType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 432
    new-instance v0, Lcom/squareup/api/items/Fee$AdjustmentType;

    const/4 v1, 0x0

    const-string v2, "TAX"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/api/items/Fee$AdjustmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Fee$AdjustmentType;->TAX:Lcom/squareup/api/items/Fee$AdjustmentType;

    .line 434
    new-instance v0, Lcom/squareup/api/items/Fee$AdjustmentType;

    const/4 v2, 0x1

    const-string v3, "OBSOLETE_SURCHARGE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/api/items/Fee$AdjustmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/Fee$AdjustmentType;->OBSOLETE_SURCHARGE:Lcom/squareup/api/items/Fee$AdjustmentType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/api/items/Fee$AdjustmentType;

    .line 431
    sget-object v3, Lcom/squareup/api/items/Fee$AdjustmentType;->TAX:Lcom/squareup/api/items/Fee$AdjustmentType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/api/items/Fee$AdjustmentType;->OBSOLETE_SURCHARGE:Lcom/squareup/api/items/Fee$AdjustmentType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/items/Fee$AdjustmentType;->$VALUES:[Lcom/squareup/api/items/Fee$AdjustmentType;

    .line 436
    new-instance v0, Lcom/squareup/api/items/Fee$AdjustmentType$ProtoAdapter_AdjustmentType;

    invoke-direct {v0}, Lcom/squareup/api/items/Fee$AdjustmentType$ProtoAdapter_AdjustmentType;-><init>()V

    sput-object v0, Lcom/squareup/api/items/Fee$AdjustmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 440
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 441
    iput p3, p0, Lcom/squareup/api/items/Fee$AdjustmentType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/Fee$AdjustmentType;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 450
    :cond_0
    sget-object p0, Lcom/squareup/api/items/Fee$AdjustmentType;->OBSOLETE_SURCHARGE:Lcom/squareup/api/items/Fee$AdjustmentType;

    return-object p0

    .line 449
    :cond_1
    sget-object p0, Lcom/squareup/api/items/Fee$AdjustmentType;->TAX:Lcom/squareup/api/items/Fee$AdjustmentType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/Fee$AdjustmentType;
    .locals 1

    .line 431
    const-class v0, Lcom/squareup/api/items/Fee$AdjustmentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/Fee$AdjustmentType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/Fee$AdjustmentType;
    .locals 1

    .line 431
    sget-object v0, Lcom/squareup/api/items/Fee$AdjustmentType;->$VALUES:[Lcom/squareup/api/items/Fee$AdjustmentType;

    invoke-virtual {v0}, [Lcom/squareup/api/items/Fee$AdjustmentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/Fee$AdjustmentType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 457
    iget v0, p0, Lcom/squareup/api/items/Fee$AdjustmentType;->value:I

    return v0
.end method
