.class public final Lcom/squareup/api/items/TaxRule;
.super Lcom/squareup/wire/Message;
.source "TaxRule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/TaxRule$ProtoAdapter_TaxRule;,
        Lcom/squareup/api/items/TaxRule$Condition;,
        Lcom/squareup/api/items/TaxRule$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/items/TaxRule;",
        "Lcom/squareup/api/items/TaxRule$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/TaxRule;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTION_TYPE:Lcom/squareup/api/items/EnablingActionType;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final action_type:Lcom/squareup/api/items/EnablingActionType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.EnablingActionType#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final condition:Lcom/squareup/api/items/TaxRule$Condition;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.TaxRule$Condition#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final exempt_product_set:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final item_config:Lcom/squareup/api/items/ItemSetConfiguration;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.ItemSetConfiguration#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final tax_config:Lcom/squareup/api/items/TaxSetConfiguration;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.items.TaxSetConfiguration#ADAPTER"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/api/items/TaxRule$ProtoAdapter_TaxRule;

    invoke-direct {v0}, Lcom/squareup/api/items/TaxRule$ProtoAdapter_TaxRule;-><init>()V

    sput-object v0, Lcom/squareup/api/items/TaxRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 36
    sget-object v0, Lcom/squareup/api/items/EnablingActionType;->UNKNOWN_ENABLING_ACTION_TYPE:Lcom/squareup/api/items/EnablingActionType;

    sput-object v0, Lcom/squareup/api/items/TaxRule;->DEFAULT_ACTION_TYPE:Lcom/squareup/api/items/EnablingActionType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TaxRule$Condition;Lcom/squareup/api/items/TaxSetConfiguration;Lcom/squareup/api/items/EnablingActionType;Lcom/squareup/api/items/ItemSetConfiguration;Lcom/squareup/api/sync/ObjectId;)V
    .locals 9

    .line 106
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/api/items/TaxRule;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TaxRule$Condition;Lcom/squareup/api/items/TaxSetConfiguration;Lcom/squareup/api/items/EnablingActionType;Lcom/squareup/api/items/ItemSetConfiguration;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TaxRule$Condition;Lcom/squareup/api/items/TaxSetConfiguration;Lcom/squareup/api/items/EnablingActionType;Lcom/squareup/api/items/ItemSetConfiguration;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V
    .locals 1

    .line 112
    sget-object v0, Lcom/squareup/api/items/TaxRule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 113
    iput-object p1, p0, Lcom/squareup/api/items/TaxRule;->id:Ljava/lang/String;

    .line 114
    iput-object p2, p0, Lcom/squareup/api/items/TaxRule;->name:Ljava/lang/String;

    .line 115
    iput-object p3, p0, Lcom/squareup/api/items/TaxRule;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    .line 116
    iput-object p4, p0, Lcom/squareup/api/items/TaxRule;->tax_config:Lcom/squareup/api/items/TaxSetConfiguration;

    .line 117
    iput-object p5, p0, Lcom/squareup/api/items/TaxRule;->action_type:Lcom/squareup/api/items/EnablingActionType;

    .line 118
    iput-object p6, p0, Lcom/squareup/api/items/TaxRule;->item_config:Lcom/squareup/api/items/ItemSetConfiguration;

    .line 119
    iput-object p7, p0, Lcom/squareup/api/items/TaxRule;->exempt_product_set:Lcom/squareup/api/sync/ObjectId;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 139
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/items/TaxRule;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 140
    :cond_1
    check-cast p1, Lcom/squareup/api/items/TaxRule;

    .line 141
    invoke-virtual {p0}, Lcom/squareup/api/items/TaxRule;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/items/TaxRule;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/TaxRule;->id:Ljava/lang/String;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/items/TaxRule;->name:Ljava/lang/String;

    .line 143
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    iget-object v3, p1, Lcom/squareup/api/items/TaxRule;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    .line 144
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->tax_config:Lcom/squareup/api/items/TaxSetConfiguration;

    iget-object v3, p1, Lcom/squareup/api/items/TaxRule;->tax_config:Lcom/squareup/api/items/TaxSetConfiguration;

    .line 145
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->action_type:Lcom/squareup/api/items/EnablingActionType;

    iget-object v3, p1, Lcom/squareup/api/items/TaxRule;->action_type:Lcom/squareup/api/items/EnablingActionType;

    .line 146
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->item_config:Lcom/squareup/api/items/ItemSetConfiguration;

    iget-object v3, p1, Lcom/squareup/api/items/TaxRule;->item_config:Lcom/squareup/api/items/ItemSetConfiguration;

    .line 147
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->exempt_product_set:Lcom/squareup/api/sync/ObjectId;

    iget-object p1, p1, Lcom/squareup/api/items/TaxRule;->exempt_product_set:Lcom/squareup/api/sync/ObjectId;

    .line 148
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 153
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 155
    invoke-virtual {p0}, Lcom/squareup/api/items/TaxRule;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 156
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 157
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/api/items/TaxRule$Condition;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 159
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->tax_config:Lcom/squareup/api/items/TaxSetConfiguration;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/api/items/TaxSetConfiguration;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 160
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->action_type:Lcom/squareup/api/items/EnablingActionType;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/api/items/EnablingActionType;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 161
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->item_config:Lcom/squareup/api/items/ItemSetConfiguration;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/api/items/ItemSetConfiguration;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 162
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->exempt_product_set:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 163
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/items/TaxRule$Builder;
    .locals 2

    .line 124
    new-instance v0, Lcom/squareup/api/items/TaxRule$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/TaxRule$Builder;-><init>()V

    .line 125
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/TaxRule$Builder;->id:Ljava/lang/String;

    .line 126
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/items/TaxRule$Builder;->name:Ljava/lang/String;

    .line 127
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    iput-object v1, v0, Lcom/squareup/api/items/TaxRule$Builder;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    .line 128
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->tax_config:Lcom/squareup/api/items/TaxSetConfiguration;

    iput-object v1, v0, Lcom/squareup/api/items/TaxRule$Builder;->tax_config:Lcom/squareup/api/items/TaxSetConfiguration;

    .line 129
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->action_type:Lcom/squareup/api/items/EnablingActionType;

    iput-object v1, v0, Lcom/squareup/api/items/TaxRule$Builder;->action_type:Lcom/squareup/api/items/EnablingActionType;

    .line 130
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->item_config:Lcom/squareup/api/items/ItemSetConfiguration;

    iput-object v1, v0, Lcom/squareup/api/items/TaxRule$Builder;->item_config:Lcom/squareup/api/items/ItemSetConfiguration;

    .line 131
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->exempt_product_set:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/api/items/TaxRule$Builder;->exempt_product_set:Lcom/squareup/api/sync/ObjectId;

    .line 132
    invoke-virtual {p0}, Lcom/squareup/api/items/TaxRule;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TaxRule$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/api/items/TaxRule;->newBuilder()Lcom/squareup/api/items/TaxRule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 171
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    if-eqz v1, :cond_2

    const-string v1, ", condition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->condition:Lcom/squareup/api/items/TaxRule$Condition;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 174
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->tax_config:Lcom/squareup/api/items/TaxSetConfiguration;

    if-eqz v1, :cond_3

    const-string v1, ", tax_config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->tax_config:Lcom/squareup/api/items/TaxSetConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 175
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->action_type:Lcom/squareup/api/items/EnablingActionType;

    if-eqz v1, :cond_4

    const-string v1, ", action_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->action_type:Lcom/squareup/api/items/EnablingActionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 176
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->item_config:Lcom/squareup/api/items/ItemSetConfiguration;

    if-eqz v1, :cond_5

    const-string v1, ", item_config="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->item_config:Lcom/squareup/api/items/ItemSetConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 177
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->exempt_product_set:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_6

    const-string v1, ", exempt_product_set="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/items/TaxRule;->exempt_product_set:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TaxRule{"

    .line 178
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
