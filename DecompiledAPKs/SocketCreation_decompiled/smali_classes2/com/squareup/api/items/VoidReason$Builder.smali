.class public final Lcom/squareup/api/items/VoidReason$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VoidReason.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/VoidReason;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/VoidReason;",
        "Lcom/squareup/api/items/VoidReason$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public ordinal:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 122
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/VoidReason;
    .locals 5

    .line 151
    new-instance v0, Lcom/squareup/api/items/VoidReason;

    iget-object v1, p0, Lcom/squareup/api/items/VoidReason$Builder;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/VoidReason$Builder;->id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/VoidReason$Builder;->ordinal:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/api/items/VoidReason;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 115
    invoke-virtual {p0}, Lcom/squareup/api/items/VoidReason$Builder;->build()Lcom/squareup/api/items/VoidReason;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/VoidReason$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/api/items/VoidReason$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/VoidReason$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/api/items/VoidReason$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/VoidReason$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/api/items/VoidReason$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method
