.class public final enum Lcom/squareup/api/items/PricingRule$DiscountTargetScope;
.super Ljava/lang/Enum;
.source "PricingRule.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/PricingRule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DiscountTargetScope"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/items/PricingRule$DiscountTargetScope$ProtoAdapter_DiscountTargetScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/api/items/PricingRule$DiscountTargetScope;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/items/PricingRule$DiscountTargetScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum LINE_ITEM:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

.field public static final enum WHOLE_PURCHASE:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 655
    new-instance v0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "LINE_ITEM"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->LINE_ITEM:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    .line 660
    new-instance v0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    const/4 v3, 0x2

    const-string v4, "WHOLE_PURCHASE"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->WHOLE_PURCHASE:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    new-array v0, v3, [Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    .line 651
    sget-object v3, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->LINE_ITEM:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->WHOLE_PURCHASE:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->$VALUES:[Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    .line 662
    new-instance v0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope$ProtoAdapter_DiscountTargetScope;

    invoke-direct {v0}, Lcom/squareup/api/items/PricingRule$DiscountTargetScope$ProtoAdapter_DiscountTargetScope;-><init>()V

    sput-object v0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 666
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 667
    iput p3, p0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/api/items/PricingRule$DiscountTargetScope;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 676
    :cond_0
    sget-object p0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->WHOLE_PURCHASE:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    return-object p0

    .line 675
    :cond_1
    sget-object p0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->LINE_ITEM:Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/api/items/PricingRule$DiscountTargetScope;
    .locals 1

    .line 651
    const-class v0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    return-object p0
.end method

.method public static values()[Lcom/squareup/api/items/PricingRule$DiscountTargetScope;
    .locals 1

    .line 651
    sget-object v0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->$VALUES:[Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    invoke-virtual {v0}, [Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/api/items/PricingRule$DiscountTargetScope;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 683
    iget v0, p0, Lcom/squareup/api/items/PricingRule$DiscountTargetScope;->value:I

    return v0
.end method
