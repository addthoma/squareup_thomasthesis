.class public final Lcom/squareup/api/items/TicketTemplate$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TicketTemplate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/items/TicketTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/items/TicketTemplate;",
        "Lcom/squareup/api/items/TicketTemplate$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public ordinal:Ljava/lang/Integer;

.field public ticket_group:Lcom/squareup/api/sync/ObjectId;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 143
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/items/TicketTemplate;
    .locals 7

    .line 177
    new-instance v6, Lcom/squareup/api/items/TicketTemplate;

    iget-object v1, p0, Lcom/squareup/api/items/TicketTemplate$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/items/TicketTemplate$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/api/items/TicketTemplate$Builder;->ordinal:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/squareup/api/items/TicketTemplate$Builder;->ticket_group:Lcom/squareup/api/sync/ObjectId;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/items/TicketTemplate;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/sync/ObjectId;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 134
    invoke-virtual {p0}, Lcom/squareup/api/items/TicketTemplate$Builder;->build()Lcom/squareup/api/items/TicketTemplate;

    move-result-object v0

    return-object v0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/items/TicketTemplate$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/api/items/TicketTemplate$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/api/items/TicketTemplate$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lcom/squareup/api/items/TicketTemplate$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/TicketTemplate$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/api/items/TicketTemplate$Builder;->ordinal:Ljava/lang/Integer;

    return-object p0
.end method

.method public ticket_group(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/TicketTemplate$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/api/items/TicketTemplate$Builder;->ticket_group:Lcom/squareup/api/sync/ObjectId;

    return-object p0
.end method
