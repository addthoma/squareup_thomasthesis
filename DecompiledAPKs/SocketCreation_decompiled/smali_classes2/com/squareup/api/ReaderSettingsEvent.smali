.class public Lcom/squareup/api/ReaderSettingsEvent;
.super Lcom/squareup/api/RegisterApiEvent;
.source "ReaderSettingsEvent.java"


# instance fields
.field public final cold_start:Z

.field public final request_start_Time:J

.field public final sequenceUuid:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "api_sequence_uuid"
    .end annotation
.end field

.field public final startup_duration_millis:J


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;Ljava/lang/String;Ljava/lang/String;ZJJ)V
    .locals 6

    .line 17
    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->API_NEW_READER_SETTINGS:Lcom/squareup/analytics/RegisterActionName;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-wide v4, p7

    invoke-direct/range {v0 .. v5}, Lcom/squareup/api/RegisterApiEvent;-><init>(Lcom/squareup/util/Clock;Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;J)V

    .line 18
    iput-object p2, p0, Lcom/squareup/api/ReaderSettingsEvent;->sequenceUuid:Ljava/lang/String;

    .line 19
    iput-boolean p4, p0, Lcom/squareup/api/ReaderSettingsEvent;->cold_start:Z

    .line 20
    iput-wide p5, p0, Lcom/squareup/api/ReaderSettingsEvent;->startup_duration_millis:J

    .line 21
    iput-wide p7, p0, Lcom/squareup/api/ReaderSettingsEvent;->request_start_Time:J

    return-void
.end method
