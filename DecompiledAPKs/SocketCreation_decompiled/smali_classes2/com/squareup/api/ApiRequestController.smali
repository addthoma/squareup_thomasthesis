.class public Lcom/squareup/api/ApiRequestController;
.super Ljava/lang/Object;
.source "ApiRequestController.java"


# static fields
.field private static final CLIENT_ID_KEY:Ljava/lang/String; = "registerApiClientId"

.field private static final REQUEST_START_TIME_KEY:Ljava/lang/String; = "REQUEST_START_TIME_KEY"

.field private static final SEQUENCE_UUID_KEY:Ljava/lang/String; = "SEQUENCE_UUID"


# instance fields
.field private activity:Landroid/app/Activity;

.field private final apiSessionLogger:Lcom/squareup/api/ApiSessionLogger;

.field private clientId:Ljava/lang/String;

.field private final latestApiSequenceUuid:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private loaded:Z

.field private final onStaleApiRequest:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

.field private requestStartTime:J

.field private sequenceUuid:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiSessionLogger;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/settings/LocalSetting;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/ApiSessionLogger;",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/ApiRequestController;->onStaleApiRequest:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 56
    iput-object p1, p0, Lcom/squareup/api/ApiRequestController;->apiSessionLogger:Lcom/squareup/api/ApiSessionLogger;

    .line 57
    iput-object p2, p0, Lcom/squareup/api/ApiRequestController;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    .line 58
    iput-object p3, p0, Lcom/squareup/api/ApiRequestController;->latestApiSequenceUuid:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/api/ApiRequestController;)Z
    .locals 0

    .line 32
    iget-boolean p0, p0, Lcom/squareup/api/ApiRequestController;->loaded:Z

    return p0
.end method

.method static synthetic access$002(Lcom/squareup/api/ApiRequestController;Z)Z
    .locals 0

    .line 32
    iput-boolean p1, p0, Lcom/squareup/api/ApiRequestController;->loaded:Z

    return p1
.end method

.method static synthetic access$100(Lcom/squareup/api/ApiRequestController;)Ljava/lang/String;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/api/ApiRequestController;->clientId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$102(Lcom/squareup/api/ApiRequestController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/api/ApiRequestController;->clientId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/api/ApiRequestController;)Ljava/lang/String;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/api/ApiRequestController;->sequenceUuid:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$202(Lcom/squareup/api/ApiRequestController;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/api/ApiRequestController;->sequenceUuid:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/squareup/api/ApiRequestController;)Lcom/squareup/api/ApiSessionLogger;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/api/ApiRequestController;->apiSessionLogger:Lcom/squareup/api/ApiSessionLogger;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/api/ApiRequestController;)J
    .locals 2

    .line 32
    iget-wide v0, p0, Lcom/squareup/api/ApiRequestController;->requestStartTime:J

    return-wide v0
.end method

.method static synthetic access$402(Lcom/squareup/api/ApiRequestController;J)J
    .locals 0

    .line 32
    iput-wide p1, p0, Lcom/squareup/api/ApiRequestController;->requestStartTime:J

    return-wide p1
.end method

.method static synthetic access$500(Lcom/squareup/api/ApiRequestController;)Lcom/squareup/settings/LocalSetting;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/api/ApiRequestController;->latestApiSequenceUuid:Lcom/squareup/settings/LocalSetting;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/api/ApiRequestController;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/api/ApiRequestController;->onStaleApiRequest:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/api/ApiRequestController;)Lcom/squareup/pauses/PauseAndResumeRegistrar;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/api/ApiRequestController;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    return-object p0
.end method


# virtual methods
.method public clientId()Ljava/lang/String;
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController;->clientId:Ljava/lang/String;

    return-object v0
.end method

.method public finish()V
    .locals 3

    .line 142
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController;->apiSessionLogger:Lcom/squareup/api/ApiSessionLogger;

    invoke-virtual {v0}, Lcom/squareup/api/ApiSessionLogger;->clearApiLogProperty()V

    .line 144
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController;->activity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 147
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController;->activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 149
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController;->activity:Landroid/app/Activity;

    const/4 v1, 0x0

    sget v2, Lcom/squareup/widgets/R$anim;->tooltip_dialog_exit:I

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    :cond_0
    const/4 v0, 0x0

    .line 151
    iput-object v0, p0, Lcom/squareup/api/ApiRequestController;->sequenceUuid:Ljava/lang/String;

    const-wide/16 v0, 0x0

    .line 152
    iput-wide v0, p0, Lcom/squareup/api/ApiRequestController;->requestStartTime:J

    return-void
.end method

.method public getActivity()Landroid/app/Activity;
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method public getBundler()Lmortar/bundler/Bundler;
    .locals 2

    .line 77
    new-instance v0, Lcom/squareup/api/ApiRequestController$1;

    const-class v1, Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/api/ApiRequestController$1;-><init>(Lcom/squareup/api/ApiRequestController;Ljava/lang/String;)V

    return-object v0
.end method

.method public isApiRequest()Z
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController;->clientId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onActivityCreated(Landroid/app/Activity;)V
    .locals 1

    const-string v0, "activity"

    .line 132
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/squareup/api/ApiRequestController;->activity:Landroid/app/Activity;

    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController;->activity:Landroid/app/Activity;

    const-string v1, "activity"

    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    if-ne v0, p1, :cond_0

    const/4 p1, 0x0

    .line 137
    iput-object p1, p0, Lcom/squareup/api/ApiRequestController;->activity:Landroid/app/Activity;

    :cond_0
    return-void
.end method

.method public onStaleApiRequest()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 128
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController;->onStaleApiRequest:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public requestStartTime()J
    .locals 2

    .line 156
    iget-wide v0, p0, Lcom/squareup/api/ApiRequestController;->requestStartTime:J

    return-wide v0
.end method

.method public sequenceUuid()Ljava/lang/String;
    .locals 1

    .line 160
    iget-object v0, p0, Lcom/squareup/api/ApiRequestController;->sequenceUuid:Ljava/lang/String;

    return-object v0
.end method

.method public startApiSession(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "API session already started"

    .line 64
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 65
    iput-object p2, p0, Lcom/squareup/api/ApiRequestController;->sequenceUuid:Ljava/lang/String;

    .line 66
    iput-wide p3, p0, Lcom/squareup/api/ApiRequestController;->requestStartTime:J

    .line 67
    iget-object p3, p0, Lcom/squareup/api/ApiRequestController;->apiSessionLogger:Lcom/squareup/api/ApiSessionLogger;

    invoke-virtual {p3, p2}, Lcom/squareup/api/ApiSessionLogger;->setApiLogProperty(Ljava/lang/String;)V

    .line 68
    iget-object p3, p0, Lcom/squareup/api/ApiRequestController;->latestApiSequenceUuid:Lcom/squareup/settings/LocalSetting;

    invoke-interface {p3, p2}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 69
    iput-object p1, p0, Lcom/squareup/api/ApiRequestController;->clientId:Ljava/lang/String;

    return-void
.end method
