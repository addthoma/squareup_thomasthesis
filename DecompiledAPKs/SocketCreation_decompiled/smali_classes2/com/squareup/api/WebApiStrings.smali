.class public final Lcom/squareup/api/WebApiStrings;
.super Ljava/lang/Object;
.source "WebApiStrings.java"


# static fields
.field public static final ERROR_CUSTOMER_MANAGEMENT_NOT_SUPPORTED:Ljava/lang/String; = "customer_management_not_supported"

.field public static final ERROR_DISABLED:Ljava/lang/String; = "error_disabled"

.field public static final ERROR_GIFT_CARDS_NOT_SUPPORTED:Ljava/lang/String; = "gift_cards_not_supported"

.field public static final ERROR_ILLEGAL_LOCATION_ID:Ljava/lang/String; = "illegal_location_id"

.field public static final ERROR_INVALID_CUSTOMER_ID:Ljava/lang/String; = "invalid_customer_id"

.field public static final ERROR_INVALID_REQUEST:Ljava/lang/String; = "invalid_request"

.field public static final ERROR_NOT_A_WEB_ERROR:Ljava/lang/String; = "unexpected_web_error"

.field public static final ERROR_NO_EMPLOYEE_LOGGED_IN:Ljava/lang/String; = "no_employee_logged_in"

.field public static final ERROR_NO_NETWORK:Ljava/lang/String; = "no_network"

.field public static final ERROR_NO_RESULT:Ljava/lang/String; = "no_result"

.field public static final ERROR_TRANSACTION_ALREADY_IN_PROGRESS:Ljava/lang/String; = "transaction_already_in_progress"

.field public static final ERROR_TRANSACTION_CANCELED:Ljava/lang/String; = "transaction_canceled"

.field public static final ERROR_UNAUTHORIZED_CLIENT_ID:Ljava/lang/String; = "unauthorized_client_id"

.field public static final ERROR_UNEXPECTED:Ljava/lang/String; = "unexpected"

.field public static final ERROR_UNSUPPORTED_API_VERSION:Ljava/lang/String; = "unsupported_api_version"

.field public static final ERROR_UNSUPPORTED_WEB_API_VERSION:Ljava/lang/String; = "unsupported_web_api_version"

.field public static final ERROR_USER_NOT_ACTIVATED:Ljava/lang/String; = "user_not_activated"

.field public static final ERROR_USER_NOT_LOGGED_IN:Ljava/lang/String; = "user_not_logged_in"

.field public static final EXTRA_ALLOW_SPLIT_TENDER:Ljava/lang/String; = "allow_split_tender"

.field public static final EXTRA_API_VERSION:Ljava/lang/String; = "api_version"

.field public static final EXTRA_AUTO_RETURN:Ljava/lang/String; = "auto_return"

.field public static final EXTRA_CLIENT_ID:Ljava/lang/String; = "client_id"

.field public static final EXTRA_CURRENCY_CODE:Ljava/lang/String; = "currency_code"

.field public static final EXTRA_CUSTOMER_ID:Ljava/lang/String; = "customer_id"

.field public static final EXTRA_LOCATION_ID:Ljava/lang/String; = "location_id"

.field public static final EXTRA_NOTE:Ljava/lang/String; = "note"

.field public static final EXTRA_REQUEST_STATE:Ljava/lang/String; = "state"

.field public static final EXTRA_SDK_VERSION:Ljava/lang/String; = "sdk_version"

.field public static final EXTRA_SKIP_RECEIPT:Ljava/lang/String; = "skip_receipt"

.field public static final EXTRA_TENDER_CARD_FROM_READER:Ljava/lang/String; = "card_from_reader"

.field public static final EXTRA_TENDER_CARD_ON_FILE:Ljava/lang/String; = "card_on_file"

.field public static final EXTRA_TENDER_CASH:Ljava/lang/String; = "cash"

.field public static final EXTRA_TENDER_KEYED_IN_CARD:Ljava/lang/String; = "keyed_in_card"

.field public static final EXTRA_TENDER_OTHER:Ljava/lang/String; = "other"

.field public static final EXTRA_TENDER_TYPES:Ljava/lang/String; = "tender_types"

.field public static final EXTRA_TOTAL_AMOUNT:Ljava/lang/String; = "amount"

.field public static final EXTRA_WEB_CALLBACK_URI:Ljava/lang/String; = "callback_url"

.field public static final FAILURE:Ljava/lang/String; = "false"

.field public static final RESULT_CLIENT_TRANSACTION_ID:Ljava/lang/String; = "client_transaction_id"

.field public static final RESULT_ERROR_CODE:Ljava/lang/String; = "error_code"

.field public static final RESULT_ERROR_DESCRIPTION:Ljava/lang/String; = "debug_description"

.field public static final RESULT_LOCATION_ID:Ljava/lang/String; = "location_id"

.field public static final RESULT_SERVER_TRANSACTION_ID:Ljava/lang/String; = "server_transaction_id"

.field public static final RESULT_STATE:Ljava/lang/String; = "state"

.field public static final RESULT_SUCCESS:Ljava/lang/String; = "success"

.field public static final SUCCESS:Ljava/lang/String; = "true"

.field public static final WEB_ACTION_CHARGE:Ljava/lang/String; = "com.squareup.pos.action.CHARGE"

.field public static final WEB_ERROR_KEY_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/api/ApiErrorResult;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final WEB_REQUEST_KEYS_V2_TO_V3:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final WEB_RESPONSE_KEYS_V3_TO_V2:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 19

    .line 73
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const-string v1, "com.squareup.pos.action.CHARGE"

    .line 74
    invoke-interface {v0, v1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.squareup.pos.API_VERSION"

    const-string v2, "api_version"

    .line 75
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.squareup.pos.SDK_VERSION"

    const-string v2, "sdk_version"

    .line 76
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.squareup.pos.CLIENT_ID"

    const-string v2, "client_id"

    .line 77
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.squareup.pos.CURRENCY_CODE"

    const-string v2, "currency_code"

    .line 78
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.squareup.pos.TOTAL_AMOUNT"

    const-string v2, "amount"

    .line 80
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.squareup.pos.NOTE"

    const-string v2, "note"

    .line 81
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.squareup.pos.LOCATION_ID"

    const-string v2, "location_id"

    .line 82
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.squareup.pos.TENDER_TYPES"

    const-string v2, "tender_types"

    .line 83
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.squareup.pos.TENDER_CASH"

    const-string v2, "cash"

    .line 84
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.squareup.pos.TENDER_OTHER"

    const-string v2, "other"

    .line 85
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.squareup.pos.TENDER_CARD_ON_FILE"

    const-string v2, "card_on_file"

    .line 86
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "com.squareup.pos.CUSTOMER_ID"

    const-string v2, "customer_id"

    .line 87
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "state"

    const-string v2, "com.squareup.pos.REQUEST_METADATA"

    .line 88
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "com.squareup.pos.AUTO_RETURN_TIMEOUT_MS"

    const-string v4, "V3_AUTO_RETURN"

    .line 89
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "com.squareup.pos.TENDER_CARD"

    const-string v4, "V3_TENDER_CARD"

    .line 91
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "com.squareup.pos.WEB_CALLBACK_URI"

    const-string v4, "callback_url"

    .line 92
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/WebApiStrings;->WEB_REQUEST_KEYS_V2_TO_V3:Ljava/util/Map;

    .line 98
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const-string v3, "error_code"

    const-string v4, "com.squareup.pos.ERROR_CODE"

    .line 99
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "debug_description"

    const-string v4, "com.squareup.pos.ERROR_DESCRIPTION"

    .line 100
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "client_transaction_id"

    const-string v2, "com.squareup.pos.CLIENT_TRANSACTION_ID"

    .line 102
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "server_transaction_id"

    const-string v2, "com.squareup.pos.SERVER_TRANSACTION_ID"

    .line 104
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "no_employee_logged_in"

    const-string v2, "com.squareup.pos.ERROR_NO_EMPLOYEE_LOGGED_IN"

    .line 106
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "gift_cards_not_supported"

    const-string v3, "com.squareup.pos.ERROR_GIFT_CARDS_NOT_SUPPORTED"

    .line 107
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "invalid_request"

    const-string v3, "com.squareup.pos.ERROR_INVALID_REQUEST"

    .line 108
    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "error_disabled"

    const-string v4, "com.squareup.pos.ERROR_DISABLED"

    .line 109
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "illegal_location_id"

    const-string v4, "com.squareup.pos.ERROR_ILLEGAL_LOCATION_ID"

    .line 110
    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "no_result"

    const-string v5, "com.squareup.pos.ERROR_NO_RESULT"

    .line 111
    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "no_network"

    const-string v6, "com.squareup.pos.ERROR_NO_NETWORK"

    .line 112
    invoke-interface {v0, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "transaction_canceled"

    const-string v7, "com.squareup.pos.ERROR_TRANSACTION_CANCELED"

    .line 113
    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "transaction_already_in_progress"

    const-string v8, "com.squareup.pos.ERROR_TRANSACTION_ALREADY_IN_PROGRESS"

    .line 114
    invoke-interface {v0, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v8, "unauthorized_client_id"

    const-string v9, "com.squareup.pos.ERROR_UNAUTHORIZED_CLIENT_ID"

    .line 116
    invoke-interface {v0, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v8, "unexpected"

    const-string v9, "com.squareup.pos.ERROR_UNEXPECTED"

    .line 117
    invoke-interface {v0, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v9, "unsupported_api_version"

    const-string v10, "com.squareup.pos.UNSUPPORTED_API_VERSION"

    .line 118
    invoke-interface {v0, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v10, "user_not_logged_in"

    const-string v11, "com.squareup.pos.ERROR_USER_NOT_LOGGED_IN"

    .line 119
    invoke-interface {v0, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v11, "user_not_activated"

    const-string v12, "com.squareup.pos.ERROR_USER_NOT_ACTIVATED"

    .line 120
    invoke-interface {v0, v11, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v12, "customer_management_not_supported"

    const-string v13, "com.squareup.pos.ERROR_CUSTOMER_MANAGEMENT_NOT_SUPPORTED"

    .line 121
    invoke-interface {v0, v12, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v13, "invalid_customer_id"

    const-string v14, "com.squareup.pos.ERROR_INVALID_CUSTOMER_ID"

    .line 123
    invoke-interface {v0, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v13, "unsupported_web_api_version"

    const-string v14, "com.squareup.pos.UNSUPPORTED_WEB_API_VERSION"

    .line 124
    invoke-interface {v0, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/WebApiStrings;->WEB_RESPONSE_KEYS_V3_TO_V2:Ljava/util/Map;

    .line 131
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 132
    invoke-static {}, Lcom/squareup/api/ApiErrorResult;->values()[Lcom/squareup/api/ApiErrorResult;

    move-result-object v13

    array-length v14, v13

    const/4 v15, 0x0

    :goto_0
    if-ge v15, v14, :cond_0

    move/from16 v16, v14

    aget-object v14, v13, v15

    .line 133
    sget-object v17, Lcom/squareup/api/WebApiStrings$1;->$SwitchMap$com$squareup$api$ApiErrorResult:[I

    invoke-virtual {v14}, Lcom/squareup/api/ApiErrorResult;->ordinal()I

    move-result v18

    aget v17, v17, v18

    packed-switch v17, :pswitch_data_0

    :goto_1
    move-object/from16 v17, v8

    goto/16 :goto_2

    .line 223
    :pswitch_0
    invoke-interface {v0, v14, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 207
    :pswitch_1
    invoke-interface {v0, v14, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 196
    :pswitch_2
    invoke-interface {v0, v14, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :pswitch_3
    move-object/from16 v17, v8

    const-string v8, "unsupported_web_api_version"

    .line 193
    invoke-interface {v0, v14, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :pswitch_4
    move-object/from16 v17, v8

    .line 190
    invoke-interface {v0, v14, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :pswitch_5
    move-object/from16 v17, v8

    .line 187
    invoke-interface {v0, v14, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :pswitch_6
    move-object/from16 v17, v8

    .line 184
    invoke-interface {v0, v14, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :pswitch_7
    move-object/from16 v17, v8

    .line 181
    invoke-interface {v0, v14, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :pswitch_8
    move-object/from16 v17, v8

    .line 178
    invoke-interface {v0, v14, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :pswitch_9
    move-object/from16 v17, v8

    .line 175
    invoke-interface {v0, v14, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :pswitch_a
    move-object/from16 v17, v8

    .line 172
    invoke-interface {v0, v14, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :pswitch_b
    move-object/from16 v17, v8

    const-string v8, "invalid_customer_id"

    .line 169
    invoke-interface {v0, v14, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :pswitch_c
    move-object/from16 v17, v8

    .line 166
    invoke-interface {v0, v14, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :pswitch_d
    move-object/from16 v17, v8

    .line 162
    invoke-interface {v0, v14, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :pswitch_e
    move-object/from16 v17, v8

    .line 150
    invoke-interface {v0, v14, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    add-int/lit8 v15, v15, 0x1

    move/from16 v14, v16

    move-object/from16 v8, v17

    goto :goto_0

    .line 228
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/WebApiStrings;->WEB_ERROR_KEY_MAP:Ljava/util/Map;

    .line 229
    invoke-static {}, Lcom/squareup/api/ApiErrorResult;->values()[Lcom/squareup/api/ApiErrorResult;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    .line 230
    sget-object v4, Lcom/squareup/api/WebApiStrings;->WEB_ERROR_KEY_MAP:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 231
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Api ErrorResult "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " is not mapped to a Web API error code. If you added this error result, map it to an error code in WebApiStrings."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_e
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_d
        :pswitch_c
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method constructor <init>()V
    .locals 1

    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 240
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method
