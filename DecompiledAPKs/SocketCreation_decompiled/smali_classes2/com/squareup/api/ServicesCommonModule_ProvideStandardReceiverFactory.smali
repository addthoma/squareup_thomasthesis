.class public final Lcom/squareup/api/ServicesCommonModule_ProvideStandardReceiverFactory;
.super Ljava/lang/Object;
.source "ServicesCommonModule_ProvideStandardReceiverFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/receiving/StandardReceiver;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainThreadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionExpiredHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/api/ServicesCommonModule_ProvideStandardReceiverFactory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/api/ServicesCommonModule_ProvideStandardReceiverFactory;->sessionExpiredHandlerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/api/ServicesCommonModule_ProvideStandardReceiverFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;",
            ">;)",
            "Lcom/squareup/api/ServicesCommonModule_ProvideStandardReceiverFactory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/api/ServicesCommonModule_ProvideStandardReceiverFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/api/ServicesCommonModule_ProvideStandardReceiverFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideStandardReceiver(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;)Lcom/squareup/receiving/StandardReceiver;
    .locals 0

    .line 43
    invoke-static {p0, p1}, Lcom/squareup/api/ServicesCommonModule;->provideStandardReceiver(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;)Lcom/squareup/receiving/StandardReceiver;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/receiving/StandardReceiver;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/receiving/StandardReceiver;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/api/ServicesCommonModule_ProvideStandardReceiverFactory;->mainThreadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v1, p0, Lcom/squareup/api/ServicesCommonModule_ProvideStandardReceiverFactory;->sessionExpiredHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;

    invoke-static {v0, v1}, Lcom/squareup/api/ServicesCommonModule_ProvideStandardReceiverFactory;->provideStandardReceiver(Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/receiving/StandardReceiver$SessionExpiredHandler;)Lcom/squareup/receiving/StandardReceiver;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/api/ServicesCommonModule_ProvideStandardReceiverFactory;->get()Lcom/squareup/receiving/StandardReceiver;

    move-result-object v0

    return-object v0
.end method
