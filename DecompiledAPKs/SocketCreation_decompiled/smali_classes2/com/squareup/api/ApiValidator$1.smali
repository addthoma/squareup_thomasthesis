.class Lcom/squareup/api/ApiValidator$1;
.super Ljava/lang/Object;
.source "ApiValidator.java"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/ApiValidator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/api/RequestParams;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/api/ApiValidator;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiValidator;)V
    .locals 0

    .line 123
    iput-object p1, p0, Lcom/squareup/api/ApiValidator$1;->this$0:Lcom/squareup/api/ApiValidator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/api/RequestParams;)V
    .locals 12

    .line 125
    iget-object v0, p0, Lcom/squareup/api/ApiValidator$1;->this$0:Lcom/squareup/api/ApiValidator;

    invoke-static {v0}, Lcom/squareup/api/ApiValidator;->access$000(Lcom/squareup/api/ApiValidator;)Lcom/squareup/account/LegacyAuthenticator;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/account/LegacyAuthenticator;->isLoggedIn()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 130
    iget-object v0, p0, Lcom/squareup/api/ApiValidator$1;->this$0:Lcom/squareup/api/ApiValidator;

    invoke-static {v0}, Lcom/squareup/api/ApiValidator;->access$100(Lcom/squareup/api/ApiValidator;)Lcom/squareup/AppDelegate;

    move-result-object v0

    const-class v1, Lcom/squareup/api/ApiValidationLoggedInComponent;

    invoke-interface {v0, v1}, Lcom/squareup/AppDelegate;->getLoggedInComponent(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiValidationLoggedInComponent;

    .line 131
    invoke-interface {v0}, Lcom/squareup/api/ApiValidationLoggedInComponent;->userToken()Ljava/lang/String;

    move-result-object v1

    .line 133
    iget-object v2, p1, Lcom/squareup/api/RequestParams;->locationId:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/squareup/api/RequestParams;->locationId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 134
    :cond_0
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_MERCHANT_ID:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 138
    :cond_1
    :goto_0
    invoke-interface {v0}, Lcom/squareup/api/ApiValidationLoggedInComponent;->passcodeEmployeeManagement()Lcom/squareup/permissions/PasscodeEmployeeManagement;

    move-result-object v1

    .line 139
    invoke-virtual {v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isUnlocked()Z

    move-result v1

    if-nez v1, :cond_3

    .line 140
    iget-object p1, p1, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    sget-object v0, Lcom/squareup/api/ApiVersion;->V1_1:Lcom/squareup/api/ApiVersion;

    invoke-virtual {p1, v0}, Lcom/squareup/api/ApiVersion;->isAtLeast(Lcom/squareup/api/ApiVersion;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 141
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->NO_EMPLOYEE_LOGGED_IN:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 143
    :cond_2
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->USER_NOT_LOGGED_IN:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 147
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/api/RequestParams;->isChargeRequest()Z

    move-result v1

    if-eqz v1, :cond_13

    .line 148
    iget-object v1, p1, Lcom/squareup/api/RequestParams;->transactionParams:Lcom/squareup/api/TransactionParams;

    .line 149
    invoke-interface {v0}, Lcom/squareup/api/ApiValidationLoggedInComponent;->transaction()Lcom/squareup/payment/Transaction;

    move-result-object v2

    .line 150
    iget-object v3, p1, Lcom/squareup/api/RequestParams;->transactionParams:Lcom/squareup/api/TransactionParams;

    iget-object v3, v3, Lcom/squareup/api/TransactionParams;->tenderTypes:Ljava/util/Set;

    .line 152
    iget-object v4, p0, Lcom/squareup/api/ApiValidator$1;->this$0:Lcom/squareup/api/ApiValidator;

    invoke-static {v4}, Lcom/squareup/api/ApiValidator;->access$200(Lcom/squareup/api/ApiValidator;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, v1, Lcom/squareup/api/TransactionParams;->tenderTypes:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    goto :goto_1

    .line 153
    :cond_4
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_TENDER_TYPE:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 156
    :cond_5
    :goto_1
    iget-object v4, v1, Lcom/squareup/api/TransactionParams;->currencyCode:Ljava/lang/String;

    if-eqz v4, :cond_12

    .line 160
    invoke-interface {v0}, Lcom/squareup/api/ApiValidationLoggedInComponent;->currencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 171
    iget-object v0, v1, Lcom/squareup/api/TransactionParams;->amount:Ljava/lang/Long;

    if-eqz v0, :cond_10

    .line 175
    iget-object v0, v1, Lcom/squareup/api/TransactionParams;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_f

    .line 180
    iget-object v0, p0, Lcom/squareup/api/ApiValidator$1;->this$0:Lcom/squareup/api/ApiValidator;

    invoke-static {v0}, Lcom/squareup/api/ApiValidator;->access$200(Lcom/squareup/api/ApiValidator;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/api/ApiTenderType;->OTHER:Lcom/squareup/api/ApiTenderType;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    sget-object v0, Lcom/squareup/api/ApiTenderType;->CASH:Lcom/squareup/api/ApiTenderType;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 181
    iget-object v0, p0, Lcom/squareup/api/ApiValidator$1;->this$0:Lcom/squareup/api/ApiValidator;

    invoke-static {v0}, Lcom/squareup/api/ApiValidator;->access$400(Lcom/squareup/api/ApiValidator;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 184
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getTransactionMinimum()J

    move-result-wide v3

    .line 185
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getTransactionMaximum()J

    move-result-wide v8

    .line 186
    iget-object v0, v1, Lcom/squareup/api/TransactionParams;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v0, v10, v3

    if-ltz v0, :cond_7

    .line 189
    iget-object v0, v1, Lcom/squareup/api/TransactionParams;->amount:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, v8

    if-gtz v0, :cond_6

    goto :goto_2

    .line 190
    :cond_6
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_AMOUNT_TOO_HIGH:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 187
    :cond_7
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_AMOUNT_TOO_LOW:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 182
    :cond_8
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->USER_NOT_ACTIVATED:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 194
    :cond_9
    :goto_2
    iget-wide v3, v1, Lcom/squareup/api/TransactionParams;->timeout:J

    cmp-long v0, v3, v6

    if-ltz v0, :cond_e

    .line 200
    iget-object v0, p0, Lcom/squareup/api/ApiValidator$1;->this$0:Lcom/squareup/api/ApiValidator;

    invoke-static {v0}, Lcom/squareup/api/ApiValidator;->access$200(Lcom/squareup/api/ApiValidator;)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object p1, p1, Lcom/squareup/api/RequestParams;->apiVersion:Lcom/squareup/api/ApiVersion;

    invoke-virtual {p1}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result p1

    sget-object v0, Lcom/squareup/api/ApiVersion;->V3_0:Lcom/squareup/api/ApiVersion;

    invoke-virtual {v0}, Lcom/squareup/api/ApiVersion;->ordinal()I

    move-result v0

    if-ge p1, v0, :cond_c

    .line 201
    iget-wide v3, v1, Lcom/squareup/api/TransactionParams;->timeout:J

    cmp-long p1, v3, v6

    if-lez p1, :cond_c

    .line 202
    iget-wide v3, v1, Lcom/squareup/api/TransactionParams;->timeout:J

    const-wide/16 v5, 0xc80

    cmp-long p1, v3, v5

    if-ltz p1, :cond_b

    .line 205
    iget-wide v0, v1, Lcom/squareup/api/TransactionParams;->timeout:J

    const-wide/16 v3, 0x2710

    cmp-long p1, v0, v3

    if-gtz p1, :cond_a

    goto :goto_3

    .line 206
    :cond_a
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->TIMEOUT_TOO_HIGH:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 203
    :cond_b
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->TIMEOUT_TOO_LOW:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 211
    :cond_c
    :goto_3
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->hasPayment()Z

    move-result p1

    if-nez p1, :cond_d

    goto :goto_4

    .line 212
    :cond_d
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->TRANSACTION_IN_PROGRESS:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 195
    :cond_e
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_TIMEOUT:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 176
    :cond_f
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->INVALID_CHARGE_AMOUNT:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 172
    :cond_10
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_CHARGE_AMOUNT:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    .line 162
    :cond_11
    iget-object p1, p0, Lcom/squareup/api/ApiValidator$1;->this$0:Lcom/squareup/api/ApiValidator;

    .line 163
    invoke-static {p1}, Lcom/squareup/api/ApiValidator;->access$300(Lcom/squareup/api/ApiValidator;)Landroid/content/res/Resources;

    move-result-object p1

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->INVALID_CURRENCY:Lcom/squareup/api/ApiErrorResult;

    iget v1, v1, Lcom/squareup/api/ApiErrorResult;->errorDescriptionResourceId:I

    invoke-static {p1, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v1, "parameter"

    .line 164
    invoke-virtual {p1, v1, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 165
    invoke-virtual {v0}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object v0

    const-string v1, "merchant_currency"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 166
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 167
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 168
    new-instance v0, Lcom/squareup/api/ApiValidationException;

    sget-object v1, Lcom/squareup/api/ApiErrorResult;->INVALID_CURRENCY:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {v0, v1, p1}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;Ljava/lang/String;)V

    throw v0

    .line 158
    :cond_12
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->MISSING_CURRENCY:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1

    :cond_13
    :goto_4
    return-void

    .line 126
    :cond_14
    new-instance p1, Lcom/squareup/api/ApiValidationException;

    sget-object v0, Lcom/squareup/api/ApiErrorResult;->USER_NOT_LOGGED_IN:Lcom/squareup/api/ApiErrorResult;

    invoke-direct {p1, v0}, Lcom/squareup/api/ApiValidationException;-><init>(Lcom/squareup/api/ApiErrorResult;)V

    throw p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 123
    check-cast p1, Lcom/squareup/api/RequestParams;

    invoke-virtual {p0, p1}, Lcom/squareup/api/ApiValidator$1;->call(Lcom/squareup/api/RequestParams;)V

    return-void
.end method
