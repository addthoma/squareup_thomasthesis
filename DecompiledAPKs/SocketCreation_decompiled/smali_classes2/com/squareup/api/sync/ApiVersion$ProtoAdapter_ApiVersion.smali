.class final Lcom/squareup/api/sync/ApiVersion$ProtoAdapter_ApiVersion;
.super Lcom/squareup/wire/EnumAdapter;
.source "ApiVersion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/ApiVersion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ApiVersion"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/api/sync/ApiVersion;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 46
    const-class v0, Lcom/squareup/api/sync/ApiVersion;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/api/sync/ApiVersion;
    .locals 0

    .line 51
    invoke-static {p1}, Lcom/squareup/api/sync/ApiVersion;->fromValue(I)Lcom/squareup/api/sync/ApiVersion;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 44
    invoke-virtual {p0, p1}, Lcom/squareup/api/sync/ApiVersion$ProtoAdapter_ApiVersion;->fromValue(I)Lcom/squareup/api/sync/ApiVersion;

    move-result-object p1

    return-object p1
.end method
