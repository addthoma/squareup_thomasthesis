.class public final Lcom/squareup/api/sync/RequestScope$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RequestScope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/RequestScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/sync/RequestScope;",
        "Lcom/squareup/api/sync/RequestScope$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public supported_features:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;"
        }
    .end annotation
.end field

.field public supports_advanced_availability:Ljava/lang/Boolean;

.field public supports_cancelled_appointments:Ljava/lang/Boolean;

.field public supports_carts_in_appointments:Ljava/lang/Boolean;

.field public supports_confirmations:Ljava/lang/Boolean;

.field public supports_pagination:Ljava/lang/Boolean;

.field public tickets_role:Lcom/squareup/protos/tickets/TicketsRole;

.field public unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

.field public user_id:Ljava/lang/Integer;

.field public user_token:Ljava/lang/String;

.field public visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 288
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 289
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/api/sync/RequestScope$Builder;->supported_features:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/sync/RequestScope;
    .locals 14

    .line 387
    new-instance v13, Lcom/squareup/api/sync/RequestScope;

    iget-object v1, p0, Lcom/squareup/api/sync/RequestScope$Builder;->user_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/sync/RequestScope$Builder;->user_id:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/api/sync/RequestScope$Builder;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    iget-object v4, p0, Lcom/squareup/api/sync/RequestScope$Builder;->supported_features:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/api/sync/RequestScope$Builder;->tickets_role:Lcom/squareup/protos/tickets/TicketsRole;

    iget-object v6, p0, Lcom/squareup/api/sync/RequestScope$Builder;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    iget-object v7, p0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_cancelled_appointments:Ljava/lang/Boolean;

    iget-object v8, p0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_carts_in_appointments:Ljava/lang/Boolean;

    iget-object v9, p0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_pagination:Ljava/lang/Boolean;

    iget-object v10, p0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_advanced_availability:Ljava/lang/Boolean;

    iget-object v11, p0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_confirmations:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/squareup/api/sync/RequestScope;-><init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/api/sync/VisibleTypeSet;Ljava/util/List;Lcom/squareup/protos/tickets/TicketsRole;Lcom/squareup/protos/agenda/UnitIdentifier;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v13
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 265
    invoke-virtual {p0}, Lcom/squareup/api/sync/RequestScope$Builder;->build()Lcom/squareup/api/sync/RequestScope;

    move-result-object v0

    return-object v0
.end method

.method public supported_features(Ljava/util/List;)Lcom/squareup/api/sync/RequestScope$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/CatalogFeature;",
            ">;)",
            "Lcom/squareup/api/sync/RequestScope$Builder;"
        }
    .end annotation

    .line 324
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 325
    iput-object p1, p0, Lcom/squareup/api/sync/RequestScope$Builder;->supported_features:Ljava/util/List;

    return-object p0
.end method

.method public supports_advanced_availability(Ljava/lang/Boolean;)Lcom/squareup/api/sync/RequestScope$Builder;
    .locals 0

    .line 373
    iput-object p1, p0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_advanced_availability:Ljava/lang/Boolean;

    return-object p0
.end method

.method public supports_cancelled_appointments(Ljava/lang/Boolean;)Lcom/squareup/api/sync/RequestScope$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 349
    iput-object p1, p0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_cancelled_appointments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public supports_carts_in_appointments(Ljava/lang/Boolean;)Lcom/squareup/api/sync/RequestScope$Builder;
    .locals 0

    .line 357
    iput-object p1, p0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_carts_in_appointments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public supports_confirmations(Ljava/lang/Boolean;)Lcom/squareup/api/sync/RequestScope$Builder;
    .locals 0

    .line 381
    iput-object p1, p0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_confirmations:Ljava/lang/Boolean;

    return-object p0
.end method

.method public supports_pagination(Ljava/lang/Boolean;)Lcom/squareup/api/sync/RequestScope$Builder;
    .locals 0

    .line 365
    iput-object p1, p0, Lcom/squareup/api/sync/RequestScope$Builder;->supports_pagination:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tickets_role(Lcom/squareup/protos/tickets/TicketsRole;)Lcom/squareup/api/sync/RequestScope$Builder;
    .locals 0

    .line 330
    iput-object p1, p0, Lcom/squareup/api/sync/RequestScope$Builder;->tickets_role:Lcom/squareup/protos/tickets/TicketsRole;

    return-object p0
.end method

.method public unit_identifier(Lcom/squareup/protos/agenda/UnitIdentifier;)Lcom/squareup/api/sync/RequestScope$Builder;
    .locals 0

    .line 338
    iput-object p1, p0, Lcom/squareup/api/sync/RequestScope$Builder;->unit_identifier:Lcom/squareup/protos/agenda/UnitIdentifier;

    return-object p0
.end method

.method public user_id(Ljava/lang/Integer;)Lcom/squareup/api/sync/RequestScope$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 306
    iput-object p1, p0, Lcom/squareup/api/sync/RequestScope$Builder;->user_id:Ljava/lang/Integer;

    return-object p0
.end method

.method public user_token(Ljava/lang/String;)Lcom/squareup/api/sync/RequestScope$Builder;
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/squareup/api/sync/RequestScope$Builder;->user_token:Ljava/lang/String;

    return-object p0
.end method

.method public visible_type_set(Lcom/squareup/api/sync/VisibleTypeSet;)Lcom/squareup/api/sync/RequestScope$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/squareup/api/sync/RequestScope$Builder;->visible_type_set:Lcom/squareup/api/sync/VisibleTypeSet;

    return-object p0
.end method
