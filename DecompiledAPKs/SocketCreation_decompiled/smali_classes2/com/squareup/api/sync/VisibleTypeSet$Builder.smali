.class public final Lcom/squareup/api/sync/VisibleTypeSet$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VisibleTypeSet.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/VisibleTypeSet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/sync/VisibleTypeSet;",
        "Lcom/squareup/api/sync/VisibleTypeSet$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public all_types:Ljava/lang/Boolean;

.field public items_visible_type_set:Lcom/squareup/api/items/ItemsVisibleTypeSet;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public all_types(Ljava/lang/Boolean;)Lcom/squareup/api/sync/VisibleTypeSet$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->all_types:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/api/sync/VisibleTypeSet;
    .locals 4

    .line 125
    new-instance v0, Lcom/squareup/api/sync/VisibleTypeSet;

    iget-object v1, p0, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->all_types:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->items_visible_type_set:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/api/sync/VisibleTypeSet;-><init>(Ljava/lang/Boolean;Lcom/squareup/api/items/ItemsVisibleTypeSet;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->build()Lcom/squareup/api/sync/VisibleTypeSet;

    move-result-object v0

    return-object v0
.end method

.method public items_visible_type_set(Lcom/squareup/api/items/ItemsVisibleTypeSet;)Lcom/squareup/api/sync/VisibleTypeSet$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/api/sync/VisibleTypeSet$Builder;->items_visible_type_set:Lcom/squareup/api/items/ItemsVisibleTypeSet;

    return-object p0
.end method
