.class public final Lcom/squareup/api/sync/ObjectId$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ObjectId.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/api/sync/ObjectId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/api/sync/ObjectId;",
        "Lcom/squareup/api/sync/ObjectId$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public customer_token:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public merchant_token:Ljava/lang/String;

.field public ref_type:Lcom/squareup/api/items/Type;

.field public type:Lcom/squareup/api/sync/ObjectType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 158
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/api/sync/ObjectId;
    .locals 8

    .line 199
    new-instance v7, Lcom/squareup/api/sync/ObjectId;

    iget-object v1, p0, Lcom/squareup/api/sync/ObjectId$Builder;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/api/sync/ObjectId$Builder;->type:Lcom/squareup/api/sync/ObjectType;

    iget-object v3, p0, Lcom/squareup/api/sync/ObjectId$Builder;->merchant_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/api/sync/ObjectId$Builder;->customer_token:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/api/sync/ObjectId$Builder;->ref_type:Lcom/squareup/api/items/Type;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/api/sync/ObjectId;-><init>(Ljava/lang/String;Lcom/squareup/api/sync/ObjectType;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/Type;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 147
    invoke-virtual {p0}, Lcom/squareup/api/sync/ObjectId$Builder;->build()Lcom/squareup/api/sync/ObjectId;

    move-result-object v0

    return-object v0
.end method

.method public customer_token(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;
    .locals 0

    .line 187
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectId$Builder;->customer_token:Ljava/lang/String;

    return-object p0
.end method

.method public id(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectId$Builder;->id:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId$Builder;
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectId$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public ref_type(Lcom/squareup/api/items/Type;)Lcom/squareup/api/sync/ObjectId$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 193
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectId$Builder;->ref_type:Lcom/squareup/api/items/Type;

    return-object p0
.end method

.method public type(Lcom/squareup/api/sync/ObjectType;)Lcom/squareup/api/sync/ObjectId$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/api/sync/ObjectId$Builder;->type:Lcom/squareup/api/sync/ObjectType;

    return-object p0
.end method
