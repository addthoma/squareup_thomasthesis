.class public final Lcom/squareup/api/sync/GetResponse;
.super Lcom/squareup/wire/Message;
.source "GetResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/api/sync/GetResponse$ProtoAdapter_GetResponse;,
        Lcom/squareup/api/sync/GetResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/api/sync/GetResponse;",
        "Lcom/squareup/api/sync/GetResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/api/sync/GetResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATALOG_OBJECT_COUNT:Ljava/lang/Integer;

.field public static final DEFAULT_CURRENT_BAZAAR_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_CURRENT_SERVER_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_PAGINATION_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_SYNC_AGAIN_IMMEDIATELY:Ljava/lang/Boolean;

.field public static final DEFAULT_SYNC_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_TOTAL_OBJECT_COUNT:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final catalog_object_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x4
    .end annotation
.end field

.field public final current_bazaar_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3e8
    .end annotation
.end field

.field public final current_server_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final objects:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectWrapper#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;"
        }
    .end annotation
.end field

.field public final pagination_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final sync_again_immediately:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3ea
    .end annotation
.end field

.field public final sync_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3e9
    .end annotation
.end field

.field public final total_object_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 24
    new-instance v0, Lcom/squareup/api/sync/GetResponse$ProtoAdapter_GetResponse;

    invoke-direct {v0}, Lcom/squareup/api/sync/GetResponse$ProtoAdapter_GetResponse;-><init>()V

    sput-object v0, Lcom/squareup/api/sync/GetResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 28
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/api/sync/GetResponse;->DEFAULT_CURRENT_SERVER_VERSION:Ljava/lang/Long;

    const/4 v1, 0x0

    .line 30
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sput-object v2, Lcom/squareup/api/sync/GetResponse;->DEFAULT_TOTAL_OBJECT_COUNT:Ljava/lang/Integer;

    .line 32
    sput-object v2, Lcom/squareup/api/sync/GetResponse;->DEFAULT_CATALOG_OBJECT_COUNT:Ljava/lang/Integer;

    .line 38
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/api/sync/GetResponse;->DEFAULT_SYNC_AGAIN_IMMEDIATELY:Ljava/lang/Boolean;

    .line 40
    sput-object v0, Lcom/squareup/api/sync/GetResponse;->DEFAULT_CURRENT_BAZAAR_VERSION:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .line 114
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/api/sync/GetResponse;-><init>(Ljava/lang/Long;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/squareup/api/sync/ObjectWrapper;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Long;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 121
    sget-object v0, Lcom/squareup/api/sync/GetResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 122
    iput-object p1, p0, Lcom/squareup/api/sync/GetResponse;->current_server_version:Ljava/lang/Long;

    const-string p1, "objects"

    .line 123
    invoke-static {p1, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/api/sync/GetResponse;->objects:Ljava/util/List;

    .line 124
    iput-object p3, p0, Lcom/squareup/api/sync/GetResponse;->total_object_count:Ljava/lang/Integer;

    .line 125
    iput-object p4, p0, Lcom/squareup/api/sync/GetResponse;->catalog_object_count:Ljava/lang/Integer;

    .line 126
    iput-object p5, p0, Lcom/squareup/api/sync/GetResponse;->pagination_token:Ljava/lang/String;

    .line 127
    iput-object p6, p0, Lcom/squareup/api/sync/GetResponse;->sync_token:Ljava/lang/String;

    .line 128
    iput-object p7, p0, Lcom/squareup/api/sync/GetResponse;->sync_again_immediately:Ljava/lang/Boolean;

    .line 129
    iput-object p8, p0, Lcom/squareup/api/sync/GetResponse;->current_bazaar_version:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 150
    :cond_0
    instance-of v1, p1, Lcom/squareup/api/sync/GetResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 151
    :cond_1
    check-cast p1, Lcom/squareup/api/sync/GetResponse;

    .line 152
    invoke-virtual {p0}, Lcom/squareup/api/sync/GetResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/api/sync/GetResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->current_server_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/api/sync/GetResponse;->current_server_version:Ljava/lang/Long;

    .line 153
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->objects:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/api/sync/GetResponse;->objects:Ljava/util/List;

    .line 154
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->total_object_count:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/sync/GetResponse;->total_object_count:Ljava/lang/Integer;

    .line 155
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->catalog_object_count:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/api/sync/GetResponse;->catalog_object_count:Ljava/lang/Integer;

    .line 156
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->pagination_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/sync/GetResponse;->pagination_token:Ljava/lang/String;

    .line 157
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->sync_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/api/sync/GetResponse;->sync_token:Ljava/lang/String;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->sync_again_immediately:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/api/sync/GetResponse;->sync_again_immediately:Ljava/lang/Boolean;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->current_bazaar_version:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/api/sync/GetResponse;->current_bazaar_version:Ljava/lang/Long;

    .line 160
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 165
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 167
    invoke-virtual {p0}, Lcom/squareup/api/sync/GetResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 168
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->current_server_version:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 169
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->objects:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->total_object_count:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->catalog_object_count:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->pagination_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->sync_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->sync_again_immediately:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->current_bazaar_version:Ljava/lang/Long;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 176
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/api/sync/GetResponse$Builder;
    .locals 2

    .line 134
    new-instance v0, Lcom/squareup/api/sync/GetResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/api/sync/GetResponse$Builder;-><init>()V

    .line 135
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->current_server_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/sync/GetResponse$Builder;->current_server_version:Ljava/lang/Long;

    .line 136
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->objects:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/api/sync/GetResponse$Builder;->objects:Ljava/util/List;

    .line 137
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->total_object_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/sync/GetResponse$Builder;->total_object_count:Ljava/lang/Integer;

    .line 138
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->catalog_object_count:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/api/sync/GetResponse$Builder;->catalog_object_count:Ljava/lang/Integer;

    .line 139
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->pagination_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/sync/GetResponse$Builder;->pagination_token:Ljava/lang/String;

    .line 140
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->sync_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/api/sync/GetResponse$Builder;->sync_token:Ljava/lang/String;

    .line 141
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->sync_again_immediately:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/api/sync/GetResponse$Builder;->sync_again_immediately:Ljava/lang/Boolean;

    .line 142
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->current_bazaar_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/api/sync/GetResponse$Builder;->current_bazaar_version:Ljava/lang/Long;

    .line 143
    invoke-virtual {p0}, Lcom/squareup/api/sync/GetResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/sync/GetResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/api/sync/GetResponse;->newBuilder()Lcom/squareup/api/sync/GetResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 183
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->current_server_version:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", current_server_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->current_server_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 185
    :cond_0
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->objects:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, ", objects="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->objects:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 186
    :cond_1
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->total_object_count:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", total_object_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->total_object_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 187
    :cond_2
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->catalog_object_count:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    const-string v1, ", catalog_object_count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->catalog_object_count:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 188
    :cond_3
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->pagination_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", pagination_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->pagination_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    :cond_4
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->sync_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", sync_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->sync_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    :cond_5
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->sync_again_immediately:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", sync_again_immediately="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->sync_again_immediately:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 191
    :cond_6
    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->current_bazaar_version:Ljava/lang/Long;

    if-eqz v1, :cond_7

    const-string v1, ", current_bazaar_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/api/sync/GetResponse;->current_bazaar_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetResponse{"

    .line 192
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
