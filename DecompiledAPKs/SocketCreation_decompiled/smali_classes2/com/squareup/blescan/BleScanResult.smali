.class public final Lcom/squareup/blescan/BleScanResult;
.super Ljava/lang/Object;
.source "BleScanResult.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/blescan/BleScanResult;",
        "",
        "device",
        "Landroid/bluetooth/BluetoothDevice;",
        "lookingToPair",
        "",
        "(Landroid/bluetooth/BluetoothDevice;Z)V",
        "getDevice",
        "()Landroid/bluetooth/BluetoothDevice;",
        "getLookingToPair",
        "()Z",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Landroid/bluetooth/BluetoothDevice;

.field private final lookingToPair:Z


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothDevice;Z)V
    .locals 1

    const-string v0, "device"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/blescan/BleScanResult;->device:Landroid/bluetooth/BluetoothDevice;

    iput-boolean p2, p0, Lcom/squareup/blescan/BleScanResult;->lookingToPair:Z

    return-void
.end method


# virtual methods
.method public final getDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/blescan/BleScanResult;->device:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method public final getLookingToPair()Z
    .locals 1

    .line 7
    iget-boolean v0, p0, Lcom/squareup/blescan/BleScanResult;->lookingToPair:Z

    return v0
.end method
