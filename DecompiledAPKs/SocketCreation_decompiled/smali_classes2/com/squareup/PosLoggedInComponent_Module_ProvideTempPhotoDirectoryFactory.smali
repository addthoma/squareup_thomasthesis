.class public final Lcom/squareup/PosLoggedInComponent_Module_ProvideTempPhotoDirectoryFactory;
.super Ljava/lang/Object;
.source "PosLoggedInComponent_Module_ProvideTempPhotoDirectoryFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field private final userDirProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideTempPhotoDirectoryFactory;->userDirProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/PosLoggedInComponent_Module_ProvideTempPhotoDirectoryFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/io/File;",
            ">;)",
            "Lcom/squareup/PosLoggedInComponent_Module_ProvideTempPhotoDirectoryFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/PosLoggedInComponent_Module_ProvideTempPhotoDirectoryFactory;

    invoke-direct {v0, p0}, Lcom/squareup/PosLoggedInComponent_Module_ProvideTempPhotoDirectoryFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideTempPhotoDirectory(Ljava/io/File;)Ljava/io/File;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/PosLoggedInComponent$Module;->provideTempPhotoDirectory(Ljava/io/File;)Ljava/io/File;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/io/File;

    return-object p0
.end method


# virtual methods
.method public get()Ljava/io/File;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/PosLoggedInComponent_Module_ProvideTempPhotoDirectoryFactory;->userDirProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-static {v0}, Lcom/squareup/PosLoggedInComponent_Module_ProvideTempPhotoDirectoryFactory;->provideTempPhotoDirectory(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/PosLoggedInComponent_Module_ProvideTempPhotoDirectoryFactory;->get()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method
