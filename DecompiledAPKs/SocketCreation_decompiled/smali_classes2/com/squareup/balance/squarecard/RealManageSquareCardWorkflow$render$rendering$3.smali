.class final Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealManageSquareCardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->render(Lkotlin/Unit;Lcom/squareup/balance/squarecard/ManageSquareCardState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
        "+",
        "Lkotlin/Unit;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealManageSquareCardWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealManageSquareCardWorkflow.kt\ncom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$3\n*L\n1#1,325:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
        "",
        "it",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$3;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/ManageSquareCardState;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    instance-of v0, p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event$PrimaryAction;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$3;->this$0:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->access$finishOrRefetchCardStatus(Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 165
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event$GoBack;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatus;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/balance/squarecard/ManageSquareCardState$LeafState$FetchingCardStatus;-><init>(ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v1, 0x2

    invoke-static {p1, v0, v3, v1, v3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    .line 166
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected event "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 104
    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$render$rendering$3;->invoke(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
