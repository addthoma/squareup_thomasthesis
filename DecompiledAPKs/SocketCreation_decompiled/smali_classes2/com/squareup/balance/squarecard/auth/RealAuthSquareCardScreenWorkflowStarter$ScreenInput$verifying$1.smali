.class final Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$verifying$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAuthSquareCardScreenWorkflowStarter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;",
        "it",
        "Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$verifying$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$verifying$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$verifying$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$verifying$1;->INSTANCE:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$verifying$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    instance-of v0, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event$GoBack;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$AbortIdvRequest;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$AbortIdvRequest;

    check-cast p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;

    return-object p1

    .line 114
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event$PrimaryAction;

    if-nez v0, :cond_2

    .line 115
    instance-of p1, p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event$SecondaryAction;

    if-eqz p1, :cond_1

    goto :goto_0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :cond_2
    :goto_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 91
    check-cast p1, Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$verifying$1;->invoke(Lcom/squareup/balance/squarecard/common/SquareCardProgress$Event;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;

    move-result-object p1

    return-object p1
.end method
