.class public final Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AuthSquareCardPersonalInfoCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAuthSquareCardPersonalInfoCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AuthSquareCardPersonalInfoCoordinator.kt\ncom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,89:1\n1103#2,7:90\n*E\n*S KotlinDebug\n*F\n+ 1 AuthSquareCardPersonalInfoCoordinator.kt\ncom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator\n*L\n51#1,7:90\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u000cH\u0016J\u0010\u0010\u0016\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u000cH\u0002J\u0016\u0010\u0017\u001a\u00020\u00142\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0019H\u0002J\u0016\u0010\u001a\u001a\u00020\u00142\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0019H\u0002J(\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u000c2\u0016\u0010\u001c\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$ScreenData;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoScreen;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "continueButton",
        "Landroid/view/View;",
        "ownerAddress",
        "Lcom/squareup/address/AddressLayout;",
        "ownerBirthDate",
        "Lcom/squareup/register/widgets/LegacyNohoDatePickerView;",
        "ownerName",
        "Landroid/widget/EditText;",
        "attach",
        "",
        "view",
        "bindViews",
        "configureActionBar",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "handleBack",
        "update",
        "screen",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private continueButton:Landroid/view/View;

.field private ownerAddress:Lcom/squareup/address/AddressLayout;

.field private ownerBirthDate:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

.field private ownerName:Landroid/widget/EditText;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$ScreenData;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$ScreenData;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getOwnerAddress$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;)Lcom/squareup/address/AddressLayout;
    .locals 1

    .line 29
    iget-object p0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->ownerAddress:Lcom/squareup/address/AddressLayout;

    if-nez p0, :cond_0

    const-string v0, "ownerAddress"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getOwnerBirthDate$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;)Lcom/squareup/register/widgets/LegacyNohoDatePickerView;
    .locals 1

    .line 29
    iget-object p0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->ownerBirthDate:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    if-nez p0, :cond_0

    const-string v0, "ownerBirthDate"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getOwnerName$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;)Landroid/widget/EditText;
    .locals 1

    .line 29
    iget-object p0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->ownerName:Landroid/widget/EditText;

    if-nez p0, :cond_0

    const-string v0, "ownerName"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->handleBack(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$setOwnerAddress$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;Lcom/squareup/address/AddressLayout;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->ownerAddress:Lcom/squareup/address/AddressLayout;

    return-void
.end method

.method public static final synthetic access$setOwnerBirthDate$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;Lcom/squareup/register/widgets/LegacyNohoDatePickerView;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->ownerBirthDate:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    return-void
.end method

.method public static final synthetic access$setOwnerName$p(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;Landroid/widget/EditText;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->ownerName:Landroid/widget/EditText;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 82
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 83
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->owner_name:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->ownerName:Landroid/widget/EditText;

    .line 84
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->owner_address:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/address/AddressLayout;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->ownerAddress:Lcom/squareup/address/AddressLayout;

    .line 85
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->owner_birthdate:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->ownerBirthDate:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    .line 86
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->continue_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->continueButton:Landroid/view/View;

    return-void
.end method

.method private final configureActionBar(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;",
            ">;)V"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 75
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 76
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->auth_square_card_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 77
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$configureActionBar$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$configureActionBar$1;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 78
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final handleBack(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;",
            ">;)V"
        }
    .end annotation

    .line 71
    sget-object v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$DeclinePersonalInfo;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$DeclinePersonalInfo;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$ScreenData;",
            "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;",
            ">;)V"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$update$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->continueButton:Landroid/view/View;

    if-nez p1, :cond_0

    const-string v0, "continueButton"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 90
    :cond_0
    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;Lcom/squareup/workflow/legacy/Screen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    iget-object p1, p2, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->configureActionBar(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 63
    iget-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->ownerName:Landroid/widget/EditText;

    if-nez p1, :cond_1

    const-string v0, "ownerName"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$ScreenData;->getOwnerName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object p1, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$ScreenData;->getAddress()Lcom/squareup/address/Address;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->ownerAddress:Lcom/squareup/address/AddressLayout;

    if-nez v0, :cond_2

    const-string v1, "ownerAddress"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, p1}, Lcom/squareup/address/AddressLayout;->setAddress(Lcom/squareup/address/Address;)V

    .line 65
    :cond_3
    iget-object p1, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$ScreenData;->getBirthDate()Lorg/threeten/bp/LocalDate;

    move-result-object p1

    const-string p2, "ownerBirthDate"

    if-eqz p1, :cond_5

    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->ownerBirthDate:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    if-nez v0, :cond_4

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setDate(Lorg/threeten/bp/LocalDate;)V

    .line 67
    :cond_5
    iget-object p1, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->ownerBirthDate:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    if-nez p1, :cond_6

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->ownerBirthDate:Lcom/squareup/register/widgets/LegacyNohoDatePickerView;

    if-nez v0, :cond_7

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v0}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->getDateFormat()Ljava/text/DateFormat;

    move-result-object p2

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const-string v1, "Calendar.getInstance()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/register/widgets/LegacyNohoDatePickerView;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->bindViews(Landroid/view/View;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfoCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
