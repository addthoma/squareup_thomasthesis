.class final Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$personalInfo$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAuthSquareCardScreenWorkflowStarter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;",
        "it",
        "Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$personalInfo$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$personalInfo$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$personalInfo$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$personalInfo$1;->INSTANCE:Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$personalInfo$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    instance-of v0, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromPersonalInfo;

    check-cast p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;->getOwnerName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;->getAddress()Lcom/squareup/address/Address;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$SubmitPersonalInfo;->getBirthDate()Lorg/threeten/bp/LocalDate;

    move-result-object p1

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$ContinueFromPersonalInfo;-><init>(Ljava/lang/String;Lcom/squareup/address/Address;Lorg/threeten/bp/LocalDate;)V

    check-cast v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;

    goto :goto_0

    .line 95
    :cond_0
    instance-of p1, p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event$DeclinePersonalInfo;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$CancelPersonalInfo;->INSTANCE:Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent$CancelPersonalInfo;

    move-object v0, p1

    check-cast v0, Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 91
    check-cast p1, Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/auth/RealAuthSquareCardScreenWorkflowStarter$ScreenInput$personalInfo$1;->invoke(Lcom/squareup/balance/squarecard/auth/AuthSquareCardPersonalInfo$Event;)Lcom/squareup/balance/squarecard/auth/AuthSquareCardReactor$AuthSquareCardEvent;

    move-result-object p1

    return-object p1
.end method
