.class final Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forCardConfirmation$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSquareCardOrderedWorkflowStarter.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;",
        "it",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forCardConfirmation$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forCardConfirmation$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forCardConfirmation$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forCardConfirmation$1;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forCardConfirmation$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$GoBack;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$GoBack;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnBackFromCardConfirmationScreen;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnBackFromCardConfirmationScreen;

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;

    goto :goto_0

    .line 157
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationSwipe;

    if-eqz v0, :cond_1

    .line 158
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnCardConfirmationSwipe;

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationSwipe;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationSwipe;->getCard()Lcom/squareup/Card;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnCardConfirmationSwipe;-><init>(Lcom/squareup/Card;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;

    goto :goto_0

    .line 159
    :cond_1
    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationManualEntry;

    if-eqz v0, :cond_2

    .line 160
    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnCardConfirmationManualEntry;

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationManualEntry;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationManualEntry;->getCvv()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event$CardConfirmationManualEntry;->getExpiration()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnCardConfirmationManualEntry;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;

    :goto_0
    return-object p1

    :cond_2
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 127
    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$ScreenInput$forCardConfirmation$1;->invoke(Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCardConfirmation$Event;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent;

    move-result-object p1

    return-object p1
.end method
