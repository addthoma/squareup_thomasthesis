.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;
.super Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;
.source "SquareCardOrderedReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PromptingForSquareCardInfo"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0011\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\tH\u00c6\u0003J;\u0010\u0018\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\tH\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\t2\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u0005H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000cR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;",
        "card",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "activationToken",
        "",
        "expiration",
        "cvv",
        "readerConnected",
        "",
        "(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V",
        "getActivationToken",
        "()Ljava/lang/String;",
        "getCard",
        "()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "getCvv",
        "getExpiration",
        "getReaderConnected",
        "()Z",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final activationToken:Ljava/lang/String;

.field private final card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

.field private final cvv:Ljava/lang/String;

.field private final expiration:Ljava/lang/String;

.field private final readerConnected:Z


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activationToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "expiration"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cvv"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 129
    invoke-direct {p0, v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->activationToken:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->expiration:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->cvv:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->readerConnected:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p6, 0x4

    const-string v0, ""

    if-eqz p7, :cond_0

    move-object v4, v0

    goto :goto_0

    :cond_0
    move-object v4, p3

    :goto_0
    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_1

    move-object v5, v0

    goto :goto_1

    :cond_1
    move-object v5, p4

    :goto_1
    and-int/lit8 p3, p6, 0x10

    if-eqz p3, :cond_2

    const/4 p5, 0x0

    const/4 v6, 0x0

    goto :goto_2

    :cond_2
    move v6, p5

    :goto_2
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    .line 128
    invoke-direct/range {v1 .. v6}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->activationToken:Ljava/lang/String;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->expiration:Ljava/lang/String;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->cvv:Ljava/lang/String;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->readerConnected:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move-object p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->copy(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->activationToken:Ljava/lang/String;

    return-object v0
.end method

.method public final component3()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->expiration:Ljava/lang/String;

    return-object v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->cvv:Ljava/lang/String;

    return-object v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->readerConnected:Z

    return v0
.end method

.method public final copy(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;
    .locals 7

    const-string v0, "card"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activationToken"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "expiration"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cvv"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->activationToken:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->activationToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->expiration:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->expiration:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->cvv:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->cvv:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->readerConnected:Z

    iget-boolean p1, p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->readerConnected:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getActivationToken()Ljava/lang/String;
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->activationToken:Ljava/lang/String;

    return-object v0
.end method

.method public getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    return-object v0
.end method

.method public final getCvv()Ljava/lang/String;
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->cvv:Ljava/lang/String;

    return-object v0
.end method

.method public final getExpiration()Ljava/lang/String;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->expiration:Ljava/lang/String;

    return-object v0
.end method

.method public final getReaderConnected()Z
    .locals 1

    .line 128
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->readerConnected:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->activationToken:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->expiration:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->cvv:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->readerConnected:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PromptingForSquareCardInfo(card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", activationToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->activationToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", expiration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->expiration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", cvv="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->cvv:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", readerConnected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$PromptingForSquareCardInfo;->readerConnected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
