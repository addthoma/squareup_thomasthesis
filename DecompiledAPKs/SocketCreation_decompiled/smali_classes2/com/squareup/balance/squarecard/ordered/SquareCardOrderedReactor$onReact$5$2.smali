.class final Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5$2;
.super Lkotlin/jvm/internal/Lambda;
.source "SquareCardOrderedReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnCodeConfirmationData;",
        "Lcom/squareup/workflow/legacy/EnterState<",
        "+",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCode;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/EnterState;",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCode;",
        "it",
        "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnCodeConfirmationData;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5$2;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnCodeConfirmationData;)Lcom/squareup/workflow/legacy/EnterState;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnCodeConfirmationData;",
            ")",
            "Lcom/squareup/workflow/legacy/EnterState<",
            "Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCode;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 288
    iget-object v0, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5$2;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5;

    iget-object v0, v0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;

    invoke-static {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;->access$getAnalytics$p(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor;)Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedAnalytics;->logEnterActivationCodeContinueClick()V

    .line 289
    new-instance v0, Lcom/squareup/workflow/legacy/EnterState;

    new-instance v1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCode;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5$2;->this$0:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5;

    iget-object v2, v2, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5;->$state:Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;

    invoke-virtual {v2}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState;->getCard()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnCodeConfirmationData;->getCode()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, v2, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedState$VerifyingActivationCode;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/EnterState;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 93
    check-cast p1, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnCodeConfirmationData;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$onReact$5$2;->invoke(Lcom/squareup/balance/squarecard/ordered/SquareCardOrderedReactor$SquareCardOrderedEvent$OnCodeConfirmationData;)Lcom/squareup/workflow/legacy/EnterState;

    move-result-object p1

    return-object p1
.end method
