.class public final synthetic Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 7

    invoke-static {}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->values()[Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ISSUED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->REVIEW_PENDING:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->CUSTOMIZATION_APPROVED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->SHIPPED:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ACTIVE_PENDING_2FA:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData$CardState;->ordinal()I

    move-result v1

    const/4 v6, 0x5

    aput v6, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->values()[Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->VERIFIED:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->INVALID_TOKEN:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->USED:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->EXPIRED_TOKEN:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/RealSquareCardOrderedWorkflowStarter$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->FAILED:Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bizbank/VerifyCardActivationTokenResponse$Result;->ordinal()I

    move-result v1

    aput v6, v0, v1

    return-void
.end method
