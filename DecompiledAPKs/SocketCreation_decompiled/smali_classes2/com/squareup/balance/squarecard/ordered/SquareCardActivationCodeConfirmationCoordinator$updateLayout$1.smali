.class public final Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateLayout$1;
.super Lcom/squareup/ui/LinkSpan;
.source "SquareCardActivationCodeConfirmationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator;->updateLayout(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateLayout$1",
        "Lcom/squareup/ui/LinkSpan;",
        "onClick",
        "",
        "widget",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screen:Lcom/squareup/workflow/legacy/Screen;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Screen;I)V
    .locals 0

    .line 70
    iput-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateLayout$1;->$screen:Lcom/squareup/workflow/legacy/Screen;

    invoke-direct {p0, p2}, Lcom/squareup/ui/LinkSpan;-><init>(I)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "widget"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object p1, p0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmationCoordinator$updateLayout$1;->$screen:Lcom/squareup/workflow/legacy/Screen;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v0, Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event$ResendConfirmationCode;->INSTANCE:Lcom/squareup/balance/squarecard/ordered/SquareCardActivationCodeConfirmation$Event$ResendConfirmationCode;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method
