.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderSquareCardReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnSquareCardProgressPrimaryAction;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "+",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
        "it",
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnSquareCardProgressPrimaryAction;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnSquareCardProgressPrimaryAction;)Lcom/squareup/workflow/legacy/Reaction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnSquareCardProgressPrimaryAction;",
            ")",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;",
            "Lcom/squareup/balance/squarecard/order/OrderSquareCardResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 324
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4$1;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;

    iget-object p1, p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;->getErrorType()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError$ErrorType;

    move-result-object p1

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError$ErrorType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 326
    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$MissingOwnerNameError;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$MissingOwnerNameError;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 325
    :cond_1
    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$NoError;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardResult$NoError;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    check-cast p1, Lcom/squareup/workflow/legacy/Reaction;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 88
    check-cast p1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnSquareCardProgressPrimaryAction;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$onReact$4$1;->invoke(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardEvent$OnSquareCardProgressPrimaryAction;)Lcom/squareup/workflow/legacy/Reaction;

    move-result-object p1

    return-object p1
.end method
