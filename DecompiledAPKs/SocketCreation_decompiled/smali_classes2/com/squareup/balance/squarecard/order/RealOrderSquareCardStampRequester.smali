.class public final Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;
.super Ljava/lang/Object;
.source "RealOrderSquareCardStampRequester.kt"

# interfaces
.implements Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderSquareCardStampRequester.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderSquareCardStampRequester.kt\ncom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester\n*L\n1#1,75:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\r\u001a\u00020\u000eH\u0016J\u0008\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0008\u0010\u0014\u001a\u00020\u000eH\u0016J\u000e\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u0016H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0010\u0012\u000c\u0012\n \u000c*\u0004\u0018\u00010\u000b0\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;",
        "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester;",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/settings/server/Features;)V",
        "disposable",
        "Lio/reactivex/disposables/SerialDisposable;",
        "stampsRelay",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;",
        "kotlin.jvm.PlatformType",
        "fetchStamps",
        "",
        "isStampsCustomizationEnabled",
        "",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "stamps",
        "Lio/reactivex/Single;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

.field private final disposable:Lio/reactivex/disposables/SerialDisposable;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final stampsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizbankService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->features:Lcom/squareup/settings/server/Features;

    .line 24
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.create<StampsStatus>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->stampsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 25
    new-instance p1, Lio/reactivex/disposables/SerialDisposable;

    invoke-direct {p1}, Lio/reactivex/disposables/SerialDisposable;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->disposable:Lio/reactivex/disposables/SerialDisposable;

    return-void
.end method

.method private final isStampsCustomizationEnabled()Z
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BIZBANK_STAMPS_CUSTOMIZATION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public fetchStamps()V
    .locals 3

    .line 45
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->isStampsCustomizationEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->stampsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Error$FeatureDisabled;->INSTANCE:Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus$Error$FeatureDisabled;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 48
    :cond_0
    new-instance v0, Lcom/squareup/protos/client/bizbank/GetAllStampsRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bizbank/GetAllStampsRequest$Builder;-><init>()V

    .line 49
    invoke-virtual {v0}, Lcom/squareup/protos/client/bizbank/GetAllStampsRequest$Builder;->build()Lcom/squareup/protos/client/bizbank/GetAllStampsRequest;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v2, "request"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->getAllStamps(Lcom/squareup/protos/client/bizbank/GetAllStampsRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    .line 53
    sget-object v1, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester$fetchStamps$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester$fetchStamps$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->stampsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->disposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v1, v0}, Lio/reactivex/disposables/SerialDisposable;->set(Lio/reactivex/disposables/Disposable;)Z

    :goto_0
    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->disposable:Lio/reactivex/disposables/SerialDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/SerialDisposable;->dispose()V

    return-void
.end method

.method public stamps()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->stampsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->hasValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->fetchStamps()V

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardStampRequester;->stampsRelay:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->firstOrError()Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "stampsRelay.firstOrError()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
