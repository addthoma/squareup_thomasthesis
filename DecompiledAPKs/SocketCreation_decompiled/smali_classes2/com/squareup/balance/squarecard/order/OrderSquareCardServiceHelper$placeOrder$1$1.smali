.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$placeOrder$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderSquareCardServiceHelper.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$placeOrder$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/client/bizbank/ConfirmIssueCardResponse;",
        "Lcom/squareup/server/shipping/UpdateAddressResponse;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderSquareCardServiceHelper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderSquareCardServiceHelper.kt\ncom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$placeOrder$1$1\n*L\n1#1,119:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/server/shipping/UpdateAddressResponse;",
        "it",
        "Lcom/squareup/protos/client/bizbank/ConfirmIssueCardResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$placeOrder$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$placeOrder$1$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$placeOrder$1$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$placeOrder$1$1;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$placeOrder$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/protos/client/bizbank/ConfirmIssueCardResponse;)Lcom/squareup/server/shipping/UpdateAddressResponse;
    .locals 4

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    iget-object v0, p1, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->success:Ljava/lang/Boolean;

    const-string v1, "it.status.success"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p1, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v1, v1, Lcom/squareup/protos/client/Status;->localized_title:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    const/4 v2, 0x0

    new-array v2, v2, [Lcom/squareup/server/shipping/UpdateAddressResponse$Error;

    .line 111
    new-instance v3, Lcom/squareup/server/shipping/UpdateAddressResponse;

    invoke-direct {v3, v0, v1, p1, v2}, Lcom/squareup/server/shipping/UpdateAddressResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;[Lcom/squareup/server/shipping/UpdateAddressResponse$Error;)V

    return-object v3
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Lcom/squareup/protos/client/bizbank/ConfirmIssueCardResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardServiceHelper$placeOrder$1$1;->invoke(Lcom/squareup/protos/client/bizbank/ConfirmIssueCardResponse;)Lcom/squareup/server/shipping/UpdateAddressResponse;

    move-result-object p1

    return-object p1
.end method
