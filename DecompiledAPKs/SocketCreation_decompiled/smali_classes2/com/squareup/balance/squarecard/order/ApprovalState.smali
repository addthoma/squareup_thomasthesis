.class public final enum Lcom/squareup/balance/squarecard/order/ApprovalState;
.super Ljava/lang/Enum;
.source "SquareCardCustomizationSettings.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/balance/squarecard/order/ApprovalState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/ApprovalState;",
        "",
        "(Ljava/lang/String;I)V",
        "requiresSsn",
        "",
        "APPROVED",
        "INCOMPLETE",
        "INCOMPLETE_NEEDS_SSN",
        "REJECTED",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/balance/squarecard/order/ApprovalState;

.field public static final enum APPROVED:Lcom/squareup/balance/squarecard/order/ApprovalState;

.field public static final enum INCOMPLETE:Lcom/squareup/balance/squarecard/order/ApprovalState;

.field public static final enum INCOMPLETE_NEEDS_SSN:Lcom/squareup/balance/squarecard/order/ApprovalState;

.field public static final enum REJECTED:Lcom/squareup/balance/squarecard/order/ApprovalState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/balance/squarecard/order/ApprovalState;

    new-instance v1, Lcom/squareup/balance/squarecard/order/ApprovalState;

    const/4 v2, 0x0

    const-string v3, "APPROVED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/balance/squarecard/order/ApprovalState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/balance/squarecard/order/ApprovalState;->APPROVED:Lcom/squareup/balance/squarecard/order/ApprovalState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/balance/squarecard/order/ApprovalState;

    const/4 v2, 0x1

    const-string v3, "INCOMPLETE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/balance/squarecard/order/ApprovalState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/balance/squarecard/order/ApprovalState;->INCOMPLETE:Lcom/squareup/balance/squarecard/order/ApprovalState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/balance/squarecard/order/ApprovalState;

    const/4 v2, 0x2

    const-string v3, "INCOMPLETE_NEEDS_SSN"

    invoke-direct {v1, v3, v2}, Lcom/squareup/balance/squarecard/order/ApprovalState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/balance/squarecard/order/ApprovalState;->INCOMPLETE_NEEDS_SSN:Lcom/squareup/balance/squarecard/order/ApprovalState;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/balance/squarecard/order/ApprovalState;

    const/4 v2, 0x3

    const-string v3, "REJECTED"

    invoke-direct {v1, v3, v2}, Lcom/squareup/balance/squarecard/order/ApprovalState;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/balance/squarecard/order/ApprovalState;->REJECTED:Lcom/squareup/balance/squarecard/order/ApprovalState;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/balance/squarecard/order/ApprovalState;->$VALUES:[Lcom/squareup/balance/squarecard/order/ApprovalState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/balance/squarecard/order/ApprovalState;
    .locals 1

    const-class v0, Lcom/squareup/balance/squarecard/order/ApprovalState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/balance/squarecard/order/ApprovalState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/balance/squarecard/order/ApprovalState;
    .locals 1

    sget-object v0, Lcom/squareup/balance/squarecard/order/ApprovalState;->$VALUES:[Lcom/squareup/balance/squarecard/order/ApprovalState;

    invoke-virtual {v0}, [Lcom/squareup/balance/squarecard/order/ApprovalState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/balance/squarecard/order/ApprovalState;

    return-object v0
.end method


# virtual methods
.method public final requiresSsn()Z
    .locals 2

    .line 30
    move-object v0, p0

    check-cast v0, Lcom/squareup/balance/squarecard/order/ApprovalState;

    sget-object v1, Lcom/squareup/balance/squarecard/order/ApprovalState;->INCOMPLETE_NEEDS_SSN:Lcom/squareup/balance/squarecard/order/ApprovalState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
