.class final Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderSquareCardSerializer.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion;->snapshotState(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

.field final synthetic $subSnapshot:Lcom/squareup/workflow/Snapshot;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;Lcom/squareup/workflow/Snapshot;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$subSnapshot:Lcom/squareup/workflow/Snapshot;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 56
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "state.javaClass.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 73
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    .line 74
    sget-object v1, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingSplash;->INSTANCE:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingSplash;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto/16 :goto_0

    .line 75
    :cond_0
    instance-of v1, v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;

    if-eqz v1, :cond_2

    .line 76
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;->getSkippedInitialScreens()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 77
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$CheckingAuthorization;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->writeSquareCardCustomizationSettings(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;)Lokio/BufferedSink;

    .line 78
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$subSnapshot:Lcom/squareup/workflow/Snapshot;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 80
    :cond_2
    instance-of v1, v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;

    if-eqz v1, :cond_3

    .line 81
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;->getSkippedInitialScreens()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 82
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError;->getErrorType()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$FetchingCustomizationSettingsError$ErrorType;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 84
    :cond_3
    instance-of v1, v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;

    if-eqz v1, :cond_4

    .line 85
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;->getSkippedInitialScreens()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 86
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$DisplayingBusinessName;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->writeSquareCardCustomizationSettings(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 88
    :cond_4
    instance-of v1, v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    if-eqz v1, :cond_5

    .line 89
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;->getSkippedInitialScreens()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 90
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    .line 91
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->writeSquareCardCustomizationSettings(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;)Lokio/BufferedSink;

    .line 92
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 93
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$CapturingSignature;->getStampsToRestore()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->access$writeStampsToRestore(Lokio/BufferedSink;Ljava/util/List;)V

    goto/16 :goto_0

    .line 95
    :cond_5
    instance-of v1, v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    if-eqz v1, :cond_6

    .line 96
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;->getInkLevel()Lcom/squareup/cardcustomizations/signature/InkLevel;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    .line 97
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;->getSkippedInitialScreens()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 98
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    .line 99
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->writeSquareCardCustomizationSettings(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;)Lokio/BufferedSink;

    .line 100
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 101
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;->getStamp()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeOptionalProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 102
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$SendingSignatureFailed;->getStampsToRestore()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->access$writeStampsToRestore(Lokio/BufferedSink;Ljava/util/List;)V

    goto/16 :goto_0

    .line 104
    :cond_6
    instance-of v1, v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    if-eqz v1, :cond_8

    .line 105
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->getSkippedInitialScreens()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 106
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->writeSquareCardCustomizationSettings(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;)Lokio/BufferedSink;

    .line 107
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 108
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$HostsWorkflow$OrderingSquareCard;->getStampsToRestore()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->access$writeStampsToRestore(Lokio/BufferedSink;Ljava/util/List;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$subSnapshot:Lcom/squareup/workflow/Snapshot;

    if-nez v0, :cond_7

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_7
    invoke-virtual {v0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-interface {p1, v0}, Lokio/BufferedSink;->write(Lokio/ByteString;)Lokio/BufferedSink;

    goto/16 :goto_0

    .line 111
    :cond_8
    instance-of v1, v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    if-eqz v1, :cond_9

    .line 112
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;->getSkippedInitialScreens()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 113
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    .line 114
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->writeSquareCardCustomizationSettings(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;)Lokio/BufferedSink;

    .line 115
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 116
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;->getStamp()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeOptionalProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 117
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$LoadingStamps;->getStampsToRestore()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->access$writeStampsToRestore(Lokio/BufferedSink;Ljava/util/List;)V

    goto :goto_0

    .line 119
    :cond_9
    instance-of v0, v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    if-eqz v0, :cond_a

    .line 120
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSkippedInitialScreens()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 121
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getAllowStampsCustomization()Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsCustomization;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    .line 122
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSquareCardCustomizationSettings()Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettingsKt;->writeSquareCardCustomizationSettings(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/order/SquareCardCustomizationSettings;)Lokio/BufferedSink;

    .line 123
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getSignatureAsJson()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 124
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStamp()Lcom/squareup/protos/client/bizbank/Stamp;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeOptionalProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 125
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStampsToRestore()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->access$writeStampsToRestore(Lokio/BufferedSink;Ljava/util/List;)V

    .line 126
    iget-object v0, p0, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializer$Companion$snapshotState$2;->$state:Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState;

    check-cast v0, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;

    invoke-virtual {v0}, Lcom/squareup/balance/squarecard/order/OrderSquareCardReactor$OrderSquareCardWorkflowState$LeafState$SignatureState$StampsState$StampsLoaded;->getStatus()Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/balance/squarecard/order/RealOrderSquareCardSerializerKt;->access$writeStampsStatus(Lokio/BufferedSink;Lcom/squareup/balance/squarecard/OrderSquareCardStampRequester$StampsStatus;)V

    :cond_a
    :goto_0
    return-void
.end method
