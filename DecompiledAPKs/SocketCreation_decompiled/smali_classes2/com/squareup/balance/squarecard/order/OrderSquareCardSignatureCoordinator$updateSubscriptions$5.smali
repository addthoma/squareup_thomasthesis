.class final Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$5;
.super Ljava/lang/Object;
.source "OrderSquareCardSignatureCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Predicate;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->updateSubscriptions(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Predicate<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "test",
        "(Lkotlin/Unit;)Z"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$5;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic test(Ljava/lang/Object;)Z
    .locals 0

    .line 80
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$5;->test(Lkotlin/Unit;)Z

    move-result p1

    return p1
.end method

.method public final test(Lkotlin/Unit;)Z
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    iget-object p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator$updateSubscriptions$5;->this$0:Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;->access$getShowingCardPreview$p(Lcom/squareup/balance/squarecard/order/OrderSquareCardSignatureCoordinator;)Z

    move-result p1

    return p1
.end method
