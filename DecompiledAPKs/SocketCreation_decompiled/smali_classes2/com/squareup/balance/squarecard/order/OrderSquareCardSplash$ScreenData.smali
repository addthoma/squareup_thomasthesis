.class public final Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;
.super Ljava/lang/Object;
.source "OrderSquareCardSplashScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;",
        "",
        "allowBack",
        "",
        "(Z)V",
        "getAllowBack",
        "()Z",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final allowBack:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;->allowBack:Z

    return-void
.end method


# virtual methods
.method public final getAllowBack()Z
    .locals 1

    .line 14
    iget-boolean v0, p0, Lcom/squareup/balance/squarecard/order/OrderSquareCardSplash$ScreenData;->allowBack:Z

    return v0
.end method
