.class public final Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EnterPhoneNumberCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEnterPhoneNumberCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EnterPhoneNumberCoordinator.kt\ncom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator\n+ 2 _Strings.kt\nkotlin/text/StringsKt___StringsKt\n+ 3 Strings.kt\nkotlin/text/StringsKt__StringsKt\n*L\n1#1,126:1\n371#2:127\n437#2,5:128\n17#3,22:133\n*E\n*S KotlinDebug\n*F\n+ 1 EnterPhoneNumberCoordinator.kt\ncom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator\n*L\n95#1:127\n95#1,5:128\n100#1,22:133\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001:\u0001$B3\u0008\u0002\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0010\u0010\u0016\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u0005H\u0002J\u0008\u0010\u0018\u001a\u00020\u0012H\u0002J\u0008\u0010\u0019\u001a\u00020\u0012H\u0002J\u0008\u0010\u001a\u001a\u00020\u001bH\u0002J\u0008\u0010\u001c\u001a\u00020\u001dH\u0002J\u0008\u0010\u001e\u001a\u00020\u001fH\u0002J\u0018\u0010 \u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0008\u0010!\u001a\u00020\u0012H\u0002J\u0010\u0010\"\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u0005H\u0002J\u0010\u0010#\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\u0005H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "phoneNumberScrubber",
        "Lcom/squareup/text/InsertingScrubber;",
        "(Lio/reactivex/Observable;Lcom/squareup/text/InsertingScrubber;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "phoneNumberText",
        "Lcom/squareup/noho/NohoEditText;",
        "submitButton",
        "Landroid/widget/Button;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "handleBack",
        "screen",
        "initPhoneNumberField",
        "initToolbar",
        "isPhoneNumberValid",
        "",
        "phoneNumberTrimmed",
        "",
        "phoneNumberUnformatted",
        "",
        "update",
        "updateActionState",
        "updateSubmitButtonHandler",
        "updateToolbar",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

.field private phoneNumberText:Lcom/squareup/noho/NohoEditText;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private submitButton:Landroid/widget/Button;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/InsertingScrubber;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/text/InsertingScrubber;",
            ")V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/text/InsertingScrubber;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/text/InsertingScrubber;)V

    return-void
.end method

.method public static final synthetic access$handleBack(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->handleBack(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;)V

    return-void
.end method

.method public static final synthetic access$phoneNumberUnformatted(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;)Ljava/lang/String;
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->phoneNumberUnformatted()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$update(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;Landroid/view/View;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->update(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$updateActionState(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->updateActionState()V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 121
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 122
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_enter_phone_number:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.s\u2026_card_enter_phone_number)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->phoneNumberText:Lcom/squareup/noho/NohoEditText;

    .line 123
    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->square_card_enter_phone_number_submit:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.s\u2026nter_phone_number_submit)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->submitButton:Landroid/widget/Button;

    return-void
.end method

.method private final handleBack(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;)V
    .locals 1

    .line 117
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object v0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen$Event$OnBack;->INSTANCE:Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen$Event$OnBack;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final initPhoneNumberField()V
    .locals 5

    .line 71
    iget-object v0, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->phoneNumberText:Lcom/squareup/noho/NohoEditText;

    const-string v1, "phoneNumberText"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$initPhoneNumberField$1;

    .line 72
    iget-object v3, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    iget-object v4, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->phoneNumberText:Lcom/squareup/noho/NohoEditText;

    if-nez v4, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v4, Lcom/squareup/text/HasSelectableText;

    invoke-direct {v2, p0, v3, v4}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$initPhoneNumberField$1;-><init>(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    check-cast v2, Landroid/text/TextWatcher;

    .line 71
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 80
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->updateActionState()V

    return-void
.end method

.method private final initToolbar()V
    .locals 4

    .line 106
    iget-object v0, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 104
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 105
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->square_card_enter_phone_number_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 106
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final isPhoneNumberValid()Z
    .locals 1

    .line 89
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->phoneNumberTrimmed()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private final phoneNumberTrimmed()Ljava/lang/CharSequence;
    .locals 8

    .line 100
    iget-object v0, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->phoneNumberText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_0

    const-string v1, "phoneNumberText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_7

    check-cast v0, Ljava/lang/CharSequence;

    .line 134
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    const/4 v3, 0x0

    move v4, v1

    const/4 v1, 0x0

    const/4 v5, 0x0

    :goto_0
    if-gt v1, v4, :cond_6

    if-nez v5, :cond_1

    move v6, v1

    goto :goto_1

    :cond_1
    move v6, v4

    .line 139
    :goto_1
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    const/16 v7, 0x20

    if-gt v6, v7, :cond_2

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    if-nez v5, :cond_4

    if-nez v6, :cond_3

    const/4 v5, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    if-nez v6, :cond_5

    goto :goto_3

    :cond_5
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    :cond_6
    :goto_3
    add-int/2addr v4, v2

    .line 154
    invoke-interface {v0, v1, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_7

    goto :goto_4

    :cond_7
    const-string v0, ""

    .line 100
    check-cast v0, Ljava/lang/CharSequence;

    :goto_4
    return-object v0
.end method

.method private final phoneNumberUnformatted()Ljava/lang/String;
    .locals 6

    .line 94
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->phoneNumberTrimmed()Ljava/lang/CharSequence;

    move-result-object v0

    .line 127
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    check-cast v1, Ljava/lang/Appendable;

    .line 128
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    .line 129
    invoke-interface {v0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    .line 95
    invoke-static {v4}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1, v4}, Ljava/lang/Appendable;->append(C)Ljava/lang/Appendable;

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 132
    :cond_1
    check-cast v1, Ljava/lang/CharSequence;

    .line 96
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final update(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;Landroid/view/View;)V
    .locals 1

    .line 59
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->updateToolbar(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;)V

    .line 60
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->updateSubmitButtonHandler(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;)V

    .line 62
    new-instance v0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$update$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$update$1;-><init>(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final updateActionState()V
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->submitButton:Landroid/widget/Button;

    if-nez v0, :cond_0

    const-string v1, "submitButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->isPhoneNumberValid()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method private final updateSubmitButtonHandler(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;)V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->submitButton:Landroid/widget/Button;

    if-nez v0, :cond_0

    const-string v1, "submitButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 67
    :cond_0
    new-instance v1, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$updateSubmitButtonHandler$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$updateSubmitButtonHandler$1;-><init>(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object p1

    check-cast p1, Landroid/view/View$OnClickListener;

    .line 66
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final updateToolbar(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;)V
    .locals 4

    .line 113
    iget-object v0, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 110
    :cond_0
    iget-object v2, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    .line 111
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 112
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$updateToolbar$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$updateToolbar$1;-><init>(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 113
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->bindViews(Landroid/view/View;)V

    .line 49
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->initToolbar()V

    .line 50
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->initPhoneNumberField()V

    .line 52
    iget-object v0, p0, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator$attach$1;-><init>(Lcom/squareup/balance/squarecard/addnumber/EnterPhoneNumberCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
