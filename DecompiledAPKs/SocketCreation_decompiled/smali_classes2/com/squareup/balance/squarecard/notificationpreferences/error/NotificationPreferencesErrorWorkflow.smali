.class public final Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "NotificationPreferencesErrorWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationPreferencesErrorWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationPreferencesErrorWorkflow.kt\ncom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,58:1\n149#2,5:59\n*E\n*S KotlinDebug\n*F\n+ 1 NotificationPreferencesErrorWorkflow.kt\ncom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow\n*L\n54#1,5:59\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0018\u000026\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012&\u0012$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00080\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\tJF\u0010\n\u001a$\u0012\u0004\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00082\u0006\u0010\u000b\u001a\u00020\u00022\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00030\rH\u0016\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;",
        "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "()V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 30
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow;->render(Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    sget-object v0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow$render$sink$1;->INSTANCE:Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow$render$sink$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p2, v0}, Lcom/squareup/workflow/RenderContextKt;->makeEventSink(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Sink;

    move-result-object p2

    .line 41
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->getAllowRetry()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorScreen$WithRetry;

    .line 43
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->getTitle()Lcom/squareup/resources/TextModel;

    move-result-object p1

    .line 44
    new-instance v1, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow$render$screen$1;

    invoke-direct {v1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow$render$screen$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 45
    new-instance v2, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow$render$screen$2;

    invoke-direct {v2, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow$render$screen$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 42
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorScreen$WithRetry;-><init>(Lcom/squareup/resources/TextModel;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorScreen;

    goto :goto_0

    .line 48
    :cond_0
    new-instance v0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorScreen$OnlyMessage;

    .line 49
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorProps;->getTitle()Lcom/squareup/resources/TextModel;

    move-result-object p1

    .line 50
    new-instance v1, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow$render$screen$3;

    invoke-direct {v1, p2}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorWorkflow$render$screen$3;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 48
    invoke-direct {v0, p1, v1}, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorScreen$OnlyMessage;-><init>(Lcom/squareup/resources/TextModel;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorScreen;

    .line 54
    :goto_0
    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 60
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 61
    const-class p2, Lcom/squareup/balance/squarecard/notificationpreferences/error/NotificationPreferencesErrorScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 62
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 60
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 55
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
