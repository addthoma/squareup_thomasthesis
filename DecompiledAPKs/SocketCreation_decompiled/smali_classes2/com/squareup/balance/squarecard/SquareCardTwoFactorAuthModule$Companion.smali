.class public final Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule$Companion;
.super Ljava/lang/Object;
.source "SquareCardTwoFactorAuthModule.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\u0001J\u0012\u0010\u0007\u001a\u00020\u00042\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\u0001\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule$Companion;",
        "",
        "()V",
        "providesResetPasscodeSquareCardTwoFactorAuthWorkflow",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;",
        "dataStore",
        "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;",
        "providesShowPrivateSquareCardDataTwoFactorAuthWorkflow",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/SquareCardTwoFactorAuthModule$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final providesResetPasscodeSquareCardTwoFactorAuthWorkflow(Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;)Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;
    .locals 1
    .param p1    # Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;
        .annotation runtime Lcom/squareup/balance/squarecard/twofactorauth/datastore/qualifiers/ResetPasswordTwoFactor;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/balance/squarecard/twofactorauth/datastore/qualifiers/ResetPasswordTwoFactor;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "dataStore"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v0, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;-><init>(Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;)V

    check-cast v0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;

    return-object v0
.end method

.method public final providesShowPrivateSquareCardDataTwoFactorAuthWorkflow(Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;)Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;
    .locals 1
    .param p1    # Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;
        .annotation runtime Lcom/squareup/balance/squarecard/twofactorauth/datastore/qualifiers/ShowPrivateCardDataTwoFactor;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/balance/squarecard/twofactorauth/datastore/qualifiers/ShowPrivateCardDataTwoFactor;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "dataStore"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    new-instance v0, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;-><init>(Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore;)V

    check-cast v0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;

    return-object v0
.end method
