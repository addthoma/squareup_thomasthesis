.class final Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$3;
.super Ljava/lang/Object;
.source "SquareCardAddToGooglePayHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->addCardToGooglePay(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a$\u0012 \u0012\u001e\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00050\u00050\u00020\u00012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lkotlin/Pair;",
        "",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/googlepay/GooglePayAddress;",
        "<name for destructuring parameter 0>",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$3;->this$0:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$3;->$card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lkotlin/Pair<",
            "Ljava/lang/String;",
            "Lcom/squareup/googlepay/GooglePayAddress;",
            ">;>;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 54
    sget-object v1, Lcom/squareup/util/rx2/Singles;->INSTANCE:Lcom/squareup/util/rx2/Singles;

    .line 55
    iget-object v2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$3;->this$0:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    const-string/jumbo v3, "walletId"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "deviceId"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$3;->$card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iget-object v3, v3, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    const-string v4, "card.card_token"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2, v0, p1, v3}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->access$provisionToken(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    .line 56
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$3;->this$0:Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$3;->$card:Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    iget-object v2, v2, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->name_on_card:Ljava/lang/String;

    const-string v3, "card.name_on_card"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;->access$googlePayAddress(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    check-cast v0, Lio/reactivex/SingleSource;

    .line 54
    invoke-virtual {v1, p1, v0}, Lcom/squareup/util/rx2/Singles;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayHelper$addCardToGooglePay$3;->apply(Lkotlin/Pair;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
