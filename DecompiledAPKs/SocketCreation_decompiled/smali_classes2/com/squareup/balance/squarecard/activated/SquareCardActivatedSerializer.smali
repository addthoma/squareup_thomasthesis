.class public final Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer;
.super Ljava/lang/Object;
.source "SquareCardActivatedSerializer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSquareCardActivatedSerializer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SquareCardActivatedSerializer.kt\ncom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,51:1\n180#2:52\n*E\n*S KotlinDebug\n*F\n+ 1 SquareCardActivatedSerializer.kt\ncom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer\n*L\n33#1:52\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\u0004\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer;",
        "",
        "()V",
        "deserializeState",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "snapshotState",
        "state",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;
    .locals 8

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    .line 52
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 34
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 35
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Class.forName(className)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/jvm/JvmClassMappingKt;->getKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    .line 39
    const-class v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingCardDetails;-><init>(ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    goto :goto_0

    .line 40
    :cond_0
    const-class v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLearnMoreAboutSuspendedCardDialog;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLearnMoreAboutSuspendedCardDialog;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLearnMoreAboutSuspendedCardDialog;

    move-object v0, p1

    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    goto :goto_0

    .line 41
    :cond_1
    const-class v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLoadingGooglePay;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object p1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLoadingGooglePay;->INSTANCE:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$ShowingLoadingGooglePay;

    move-object v0, p1

    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    goto :goto_0

    .line 42
    :cond_2
    const-class v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;

    .line 43
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v1

    .line 44
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result v2

    .line 45
    invoke-interface {p1}, Lokio/BufferedSource;->readInt()I

    move-result p1

    .line 42
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState$GooglePayResult;-><init>(ZII)V

    check-cast v0, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    :goto_0
    return-object v0

    .line 47
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final snapshotState(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/workflow/Snapshot;
    .locals 2

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer$snapshotState$1;

    invoke-direct {v1, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedSerializer$snapshotState$1;-><init>(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
