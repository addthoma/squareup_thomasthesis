.class final Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$8;
.super Lkotlin/jvm/internal/Lambda;
.source "SquareCardActivatedWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->render(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedInput;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
        "+",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
        "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult;",
        "result",
        "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$8;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$8;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;",
            "Lcom/squareup/balance/squarecard/activated/SquareCardActivatedResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$8;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->access$logResult(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;)V

    .line 249
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$8;->$state:Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    iget-object v2, p0, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$8;->this$0:Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;

    invoke-static {v2, p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;->access$googlePayResponseState(Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow;Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;

    move-result-object p1

    invoke-static {v1, p1}, Lcom/squareup/balance/squarecard/activated/SquareCardActivatedWorkflowKt;->access$plus(Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState$ScreenState;)Lcom/squareup/balance/squarecard/activated/SquareCardActivatedState;

    move-result-object p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p1, v1, v2, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 107
    check-cast p1, Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/activated/RealSquareCardActivatedWorkflow$render$8;->invoke(Lcom/squareup/balance/squarecard/activated/SquareCardAddToGooglePayResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
