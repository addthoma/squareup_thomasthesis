.class public final Lcom/squareup/balance/squarecard/activated/SecureSessionPrivateDataWorker;
.super Lcom/squareup/workflow/LifecycleWorker;
.source "SecureSessionPrivateDataWorker.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0008\u001a\u00020\tH\u0016J\u0008\u0010\n\u001a\u00020\tH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/activated/SecureSessionPrivateDataWorker;",
        "Lcom/squareup/workflow/LifecycleWorker;",
        "secureScopeManager",
        "Lcom/squareup/secure/SecureScopeManager;",
        "(Lcom/squareup/secure/SecureScopeManager;)V",
        "sessionKey",
        "",
        "kotlin.jvm.PlatformType",
        "onStarted",
        "",
        "onStopped",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final secureScopeManager:Lcom/squareup/secure/SecureScopeManager;

.field private final sessionKey:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/secure/SecureScopeManager;)V
    .locals 1

    const-string v0, "secureScopeManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Lcom/squareup/workflow/LifecycleWorker;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/SecureSessionPrivateDataWorker;->secureScopeManager:Lcom/squareup/secure/SecureScopeManager;

    .line 18
    invoke-static {}, Lcom/squareup/shared/catalog/utils/UUID;->generateId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/activated/SecureSessionPrivateDataWorker;->sessionKey:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onStarted()V
    .locals 3

    .line 21
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SecureSessionPrivateDataWorker;->secureScopeManager:Lcom/squareup/secure/SecureScopeManager;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/SecureSessionPrivateDataWorker;->sessionKey:Ljava/lang/String;

    const-string v2, "sessionKey"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/secure/SecureScopeManager;->enterSecureSession(Ljava/lang/String;)V

    return-void
.end method

.method public onStopped()V
    .locals 3

    .line 25
    iget-object v0, p0, Lcom/squareup/balance/squarecard/activated/SecureSessionPrivateDataWorker;->secureScopeManager:Lcom/squareup/secure/SecureScopeManager;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/activated/SecureSessionPrivateDataWorker;->sessionKey:Ljava/lang/String;

    const-string v2, "sessionKey"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/secure/SecureScopeManager;->leaveSecureSession(Ljava/lang/String;)V

    return-void
.end method
