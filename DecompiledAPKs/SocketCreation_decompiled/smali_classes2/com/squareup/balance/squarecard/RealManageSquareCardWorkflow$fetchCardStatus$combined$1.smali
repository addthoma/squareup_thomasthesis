.class final Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$combined$1;
.super Ljava/lang/Object;
.source "RealManageSquareCardWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow;->fetchCardStatus(Z)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/BiFunction<",
        "Lcom/squareup/balance/squarecard/CardStatus;",
        "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
        "Lcom/squareup/balance/squarecard/CardStatus;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/CardStatus;",
        "status",
        "<anonymous parameter 1>",
        "Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$combined$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$combined$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$combined$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$combined$1;->INSTANCE:Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$combined$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/balance/squarecard/CardStatus;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/balance/squarecard/CardStatus;
    .locals 1

    const-string v0, "status"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<anonymous parameter 1>"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 104
    check-cast p1, Lcom/squareup/balance/squarecard/CardStatus;

    check-cast p2, Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/RealManageSquareCardWorkflow$fetchCardStatus$combined$1;->apply(Lcom/squareup/balance/squarecard/CardStatus;Lcom/squareup/instantdeposit/InstantDepositRunner$Snapshot;)Lcom/squareup/balance/squarecard/CardStatus;

    move-result-object p1

    return-object p1
.end method
