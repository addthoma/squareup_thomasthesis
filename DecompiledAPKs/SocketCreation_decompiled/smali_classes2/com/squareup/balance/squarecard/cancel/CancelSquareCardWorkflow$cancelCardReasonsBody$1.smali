.class final Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$cancelCardReasonsBody$1;
.super Lkotlin/jvm/internal/Lambda;
.source "CancelSquareCardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->cancelCardReasonsBody(Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsOutput;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
        "+",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
        "output",
        "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$cancelCardReasonsBody$1;->this$0:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsOutput;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardState;",
            "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    instance-of v0, p1, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsOutput$SelectedReason;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReasonSelected;

    iget-object v1, p0, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$cancelCardReasonsBody$1;->this$0:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;

    invoke-static {v1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;->access$getAnalytics$p(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow;)Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;

    move-result-object v1

    check-cast p1, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsOutput$SelectedReason;

    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsOutput$SelectedReason;->getReason()Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$ReasonSelected;-><init>(Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;Lcom/squareup/balance/squarecard/cancel/DeactivationReason;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    goto :goto_0

    .line 192
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsOutput$OnBack;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsOutput$OnBack;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$Exit;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$Action$Exit;

    move-object v0, p1

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    :goto_0
    return-object v0

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/cancel/CancelSquareCardWorkflow$cancelCardReasonsBody$1;->invoke(Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
