.class public final Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;
.super Ljava/lang/Object;
.source "PromptForCardFeedbackLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackRendering;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPromptForCardFeedbackLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PromptForCardFeedbackLayoutRunner.kt\ncom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,73:1\n1103#2,7:74\n*E\n*S KotlinDebug\n*F\n+ 1 PromptForCardFeedbackLayoutRunner.kt\ncom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner\n*L\n51#1,7:74\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u00132\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002H\u0002J\u0008\u0010\u0012\u001a\u00020\rH\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n \n*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackRendering;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "feedbackField",
        "Landroid/widget/TextView;",
        "kotlin.jvm.PlatformType",
        "submitFeedback",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "updateFeedbackButton",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final feedbackField:Landroid/widget/TextView;

.field private final submitFeedback:Landroid/view/View;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->Companion:Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->view:Landroid/view/View;

    .line 30
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 31
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->canceled_card_feedback:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->feedbackField:Landroid/widget/TextView;

    .line 32
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->canceled_card_submit_feedback:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->submitFeedback:Landroid/view/View;

    .line 35
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->updateFeedbackButton()V

    .line 37
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->feedbackField:Landroid/widget/TextView;

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner$1;-><init>(Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;)V

    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public static final synthetic access$getFeedbackField$p(Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;)Landroid/widget/TextView;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->feedbackField:Landroid/widget/TextView;

    return-object p0
.end method

.method public static final synthetic access$updateFeedbackButton(Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;)V
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->updateFeedbackButton()V

    return-void
.end method

.method private final updateActionBar(Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackRendering;)V
    .locals 4

    .line 60
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 57
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 58
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->canceled_card_leave_feedback:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 59
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner$updateActionBar$1;

    invoke-direct {v3, p1}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner$updateActionBar$1;-><init>(Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackRendering;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 60
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final updateFeedbackButton()V
    .locals 3

    .line 64
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->submitFeedback:Landroid/view/View;

    const-string v1, "submitFeedback"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->feedbackField:Landroid/widget/TextView;

    const-string v2, "feedbackField"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackRendering;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    iget-object p2, p0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner$showRendering$1;-><init>(Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackRendering;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->updateActionBar(Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackRendering;)V

    .line 51
    iget-object p2, p0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->submitFeedback:Landroid/view/View;

    .line 74
    new-instance v0, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner$showRendering$$inlined$onClickDebounced$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner$showRendering$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackRendering;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackRendering;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackLayoutRunner;->showRendering(Lcom/squareup/balance/squarecard/cancel/feedback/prompt/PromptForCardFeedbackRendering;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
