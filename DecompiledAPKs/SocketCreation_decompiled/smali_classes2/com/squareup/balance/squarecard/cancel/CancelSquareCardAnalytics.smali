.class public interface abstract Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;
.super Ljava/lang/Object;
.source "CancelSquareCardAnalytics.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0014\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0003H&J\u0008\u0010\u0006\u001a\u00020\u0003H&J\u0008\u0010\u0007\u001a\u00020\u0003H&J\u0008\u0010\u0008\u001a\u00020\u0003H&J\u0008\u0010\t\u001a\u00020\u0003H&J\u0008\u0010\n\u001a\u00020\u0003H&J\u0008\u0010\u000b\u001a\u00020\u0003H&J\u0008\u0010\u000c\u001a\u00020\u0003H&J\u0008\u0010\r\u001a\u00020\u0003H&J\u0008\u0010\u000e\u001a\u00020\u0003H&J\u0008\u0010\u000f\u001a\u00020\u0003H&J\u0008\u0010\u0010\u001a\u00020\u0003H&J\u0008\u0010\u0011\u001a\u00020\u0003H&J\u0008\u0010\u0012\u001a\u00020\u0003H&J\u0008\u0010\u0013\u001a\u00020\u0003H&J\u0008\u0010\u0014\u001a\u00020\u0003H&J\u0008\u0010\u0015\u001a\u00020\u0003H&J\u0008\u0010\u0016\u001a\u00020\u0003H&\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/CancelSquareCardAnalytics;",
        "",
        "logCancelCardErrorScreen",
        "",
        "logDeactivateCardGeneric",
        "logDeactivateLostCard",
        "logDeactivateNeverReceivedCard",
        "logDeactivateStolenCard",
        "logGenericCardDeactivatedScreen",
        "logGenericDeactivatedOkay",
        "logGenericDeactivatedQuit",
        "logLostCardDeactivatedScreen",
        "logNeverReceivedCardDeactivatedScreen",
        "logReplaceLostCard",
        "logReplaceNeverReceivedCard",
        "logReplaceStolenCard",
        "logReportCancelCardForGenericReason",
        "logReportLostCard",
        "logReportNeverReceivedCard",
        "logReportStolenCard",
        "logStolenCardDeactivatedScreen",
        "logSwitchToDepositsError",
        "logSwitchToRegularDeposits",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract logCancelCardErrorScreen()V
.end method

.method public abstract logDeactivateCardGeneric()V
.end method

.method public abstract logDeactivateLostCard()V
.end method

.method public abstract logDeactivateNeverReceivedCard()V
.end method

.method public abstract logDeactivateStolenCard()V
.end method

.method public abstract logGenericCardDeactivatedScreen()V
.end method

.method public abstract logGenericDeactivatedOkay()V
.end method

.method public abstract logGenericDeactivatedQuit()V
.end method

.method public abstract logLostCardDeactivatedScreen()V
.end method

.method public abstract logNeverReceivedCardDeactivatedScreen()V
.end method

.method public abstract logReplaceLostCard()V
.end method

.method public abstract logReplaceNeverReceivedCard()V
.end method

.method public abstract logReplaceStolenCard()V
.end method

.method public abstract logReportCancelCardForGenericReason()V
.end method

.method public abstract logReportLostCard()V
.end method

.method public abstract logReportNeverReceivedCard()V
.end method

.method public abstract logReportStolenCard()V
.end method

.method public abstract logStolenCardDeactivatedScreen()V
.end method

.method public abstract logSwitchToDepositsError()V
.end method

.method public abstract logSwitchToRegularDeposits()V
.end method
