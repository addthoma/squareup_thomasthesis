.class public final Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "CancelSquareCardConfirmWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/balance/squarecard/cancel/DeactivationReason;",
        "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmOutput;",
        "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0018\u00002\u0018\u0012\u0008\u0012\u00060\u0002j\u0002`\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0006J(\u0010\u0007\u001a\u00020\u00052\n\u0010\u0008\u001a\u00060\u0002j\u0002`\u00032\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00040\nH\u0016\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/balance/squarecard/cancel/DeactivationReason;",
        "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmProps;",
        "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmOutput;",
        "Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;",
        "()V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public render(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;
    .locals 4

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow$render$sink$1;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow$render$sink$1;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {p2, v0}, Lcom/squareup/workflow/RenderContextKt;->makeEventSink(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Sink;

    move-result-object p2

    .line 35
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Lost;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lkotlin/Pair;

    .line 36
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_lost_card_action_bar:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 37
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_card_message:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 35
    invoke-direct {p1, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 39
    :cond_0
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Stolen;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Lkotlin/Pair;

    .line 40
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_stolen_card_action_bar:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 41
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_card_message:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 39
    invoke-direct {p1, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 43
    :cond_1
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$NeverReceived;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance p1, Lkotlin/Pair;

    .line 44
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_never_received_card_action_bar:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 45
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_card_never_received_message:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 43
    invoke-direct {p1, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    .line 47
    :cond_2
    sget-object v0, Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;->INSTANCE:Lcom/squareup/balance/squarecard/cancel/DeactivationReason$Generic;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    new-instance p1, Lkotlin/Pair;

    .line 48
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_card_action_bar:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 49
    sget v1, Lcom/squareup/balance/squarecard/impl/R$string;->cancel_card_only_message:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 47
    invoke-direct {p1, v0, v1}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 34
    :goto_0
    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    .line 53
    new-instance v1, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;

    .line 56
    new-instance v2, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow$render$1;

    invoke-direct {v2, p2}, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow$render$1;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    .line 57
    new-instance v3, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow$render$2;

    invoke-direct {v3, p2}, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow$render$2;-><init>(Lcom/squareup/workflow/Sink;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    .line 53
    invoke-direct {v1, v0, p1, v2, v3}, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;-><init>(IILkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-object v1

    .line 47
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/balance/squarecard/cancel/DeactivationReason;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmWorkflow;->render(Lcom/squareup/balance/squarecard/cancel/DeactivationReason;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/balance/squarecard/cancel/confirm/CancelSquareCardConfirmRendering;

    move-result-object p1

    return-object p1
.end method
