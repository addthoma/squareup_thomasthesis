.class public final Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;
.super Ljava/lang/Object;
.source "CancelSquareCardReasonsLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCancelSquareCardReasonsLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CancelSquareCardReasonsLayoutRunner.kt\ncom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,66:1\n1103#2,7:67\n1103#2,7:74\n1103#2,7:81\n1103#2,7:88\n*E\n*S KotlinDebug\n*F\n+ 1 CancelSquareCardReasonsLayoutRunner.kt\ncom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner\n*L\n46#1,7:67\n47#1,7:74\n48#1,7:81\n49#1,7:88\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00132\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0013B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0002H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0008\u001a\n \t*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\n \t*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n \t*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000c\u001a\n \t*\u0004\u0018\u00010\u00040\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "cancelChoice",
        "kotlin.jvm.PlatformType",
        "lostChoice",
        "neverReceivedChoice",
        "stolenChoice",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$Companion;


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final cancelChoice:Landroid/view/View;

.field private final lostChoice:Landroid/view/View;

.field private final neverReceivedChoice:Landroid/view/View;

.field private final stolenChoice:Landroid/view/View;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->Companion:Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->view:Landroid/view/View;

    .line 32
    sget-object p1, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 33
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->lost_square_card_choice:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->lostChoice:Landroid/view/View;

    .line 34
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->stolen_square_card_choice:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->stolenChoice:Landroid/view/View;

    .line 35
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->never_received_square_card_choice:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->neverReceivedChoice:Landroid/view/View;

    .line 36
    iget-object p1, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/squarecard/impl/R$id;->cancel_square_card_choice:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->cancelChoice:Landroid/view/View;

    return-void
.end method

.method private final updateActionBar(Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;)V
    .locals 4

    .line 56
    iget-object v0, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 53
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 54
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/balance/squarecard/impl/R$string;->problem_with_card:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 55
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$updateActionBar$1;

    invoke-direct {v3, p1}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$updateActionBar$1;-><init>(Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    iget-object p2, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$showRendering$1;-><init>(Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->updateActionBar(Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;)V

    .line 46
    iget-object p2, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->lostChoice:Landroid/view/View;

    .line 67
    new-instance v0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$showRendering$$inlined$onClickDebounced$1;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$showRendering$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    iget-object p2, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->stolenChoice:Landroid/view/View;

    .line 74
    new-instance v0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$showRendering$$inlined$onClickDebounced$2;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$showRendering$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    iget-object p2, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->neverReceivedChoice:Landroid/view/View;

    .line 81
    new-instance v0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$showRendering$$inlined$onClickDebounced$3;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$showRendering$$inlined$onClickDebounced$3;-><init>(Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    iget-object p2, p0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->cancelChoice:Landroid/view/View;

    .line 88
    new-instance v0, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$showRendering$$inlined$onClickDebounced$4;

    invoke-direct {v0, p1}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner$showRendering$$inlined$onClickDebounced$4;-><init>(Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonsLayoutRunner;->showRendering(Lcom/squareup/balance/squarecard/cancel/reason/CancelSquareCardReasonRendering;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
