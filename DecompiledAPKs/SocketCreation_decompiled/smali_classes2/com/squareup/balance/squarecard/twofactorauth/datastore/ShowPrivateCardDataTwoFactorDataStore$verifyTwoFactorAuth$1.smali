.class final Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore$verifyTwoFactorAuth$1;
.super Ljava/lang/Object;
.source "ShowPrivateCardDataTwoFactorDataStore.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;->verifyTwoFactorAuth(Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;",
        "successOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore$verifyTwoFactorAuth$1;->this$0:Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;",
            ">;)",
            "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore$verifyTwoFactorAuth$1;->this$0:Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse;->result:Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;

    const-string v1, "successOrFailure.response.result"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;->access$convertToVerificationResult(Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore;Lcom/squareup/protos/bizbank/CreateUserAuthorizationResponse$Result;)Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    move-result-object p1

    goto :goto_0

    .line 52
    :cond_0
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz p1, :cond_1

    sget-object p1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;->UNKNOWN_FAILURE:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/twofactorauth/datastore/ShowPrivateCardDataTwoFactorDataStore$verifyTwoFactorAuth$1;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$VerificationResult;

    move-result-object p1

    return-object p1
.end method
