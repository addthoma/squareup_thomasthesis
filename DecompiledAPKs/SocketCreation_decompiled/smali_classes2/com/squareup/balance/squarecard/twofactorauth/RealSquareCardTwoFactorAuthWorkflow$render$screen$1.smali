.class final Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$screen$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSquareCardTwoFactorAuthWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->render(Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$StartTwoFactorResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;",
        "+",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult;",
        "result",
        "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$StartTwoFactorResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$screen$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$screen$1;

    invoke-direct {v0}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$screen$1;-><init>()V

    sput-object v0, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$screen$1;->INSTANCE:Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$screen$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$StartTwoFactorResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$StartTwoFactorResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$StartTwoFactorResult;->SUCCESS:Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$StartTwoFactorResult;

    if-ne p1, v1, :cond_0

    sget-object p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$EnterConfirmationCode;->INSTANCE:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$EnterConfirmationCode;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode$GenericFailure;->INSTANCE:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode$GenericFailure;

    :goto_0
    check-cast p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$StartTwoFactorResult;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$render$screen$1;->invoke(Lcom/squareup/balance/squarecard/twofactorauth/datastore/SquareCardTwoFactorDataStore$StartTwoFactorResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
