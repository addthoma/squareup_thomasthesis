.class final Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$errorScreen$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSquareCardTwoFactorAuthWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->errorScreen(Lcom/squareup/workflow/RenderContext;Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;",
        "+",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSquareCardTwoFactorAuthWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSquareCardTwoFactorAuthWorkflow.kt\ncom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$errorScreen$1\n*L\n1#1,190:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult;",
        "event",
        "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode;

.field final synthetic this$0:Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$errorScreen$1;->this$0:Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;

    iput-object p2, p0, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$errorScreen$1;->$state:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState;",
            "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    sget-object v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event$PrimaryAction;->INSTANCE:Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event$PrimaryAction;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event$GoBack;->INSTANCE:Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event$GoBack;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 156
    :goto_0
    iget-object p1, p0, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$errorScreen$1;->$state:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode;

    .line 157
    instance-of v0, p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode$ExpiredToken;

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    instance-of v0, p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode$InvalidToken;

    if-eqz v0, :cond_2

    .line 158
    :goto_1
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 159
    sget-object v0, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$EnterConfirmationCode;->INSTANCE:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$EnterConfirmationCode;

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 158
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 161
    :cond_2
    instance-of p1, p1, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthState$ErrorVerifyingConfirmationCode$GenericFailure;

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$errorScreen$1;->this$0:Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;

    invoke-static {p1}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;->access$finishAndGoBack(Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_2
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 164
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported event :: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/balance/squarecard/twofactorauth/RealSquareCardTwoFactorAuthWorkflow$errorScreen$1;->invoke(Lcom/squareup/balance/core/progressscreen/BizBankProgressScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
