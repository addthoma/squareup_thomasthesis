.class public final Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;
.super Ljava/lang/Object;
.source "SquareCardPrivateCardDetailsDataStore.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u000eB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0008\u001a\u00020\tJ\u0012\u0010\n\u001a\u00020\u0007*\u0008\u0012\u0004\u0012\u00020\u000c0\u000bH\u0002J\u0012\u0010\n\u001a\u00020\u0007*\u0008\u0012\u0004\u0012\u00020\u000c0\rH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;",
        "",
        "bizbankService",
        "Lcom/squareup/balance/core/server/bizbank/BizbankService;",
        "(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V",
        "privateCardDetailsState",
        "Lio/reactivex/Single;",
        "Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;",
        "cardData",
        "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
        "toOutput",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;",
        "Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;",
        "SquareCardPrivateDataResult",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/core/server/bizbank/BizbankService;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "bizbankService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    return-void
.end method

.method public static final synthetic access$toOutput(Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;)Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;->toOutput(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;)Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$toOutput(Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;->toOutput(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;

    move-result-object p0

    return-object p0
.end method

.method private final toOutput(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;)Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess<",
            "Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;",
            ">;)",
            "Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;"
        }
    .end annotation

    .line 39
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->private_card_info:Lcom/squareup/protos/bizbank/PrivateCardData;

    .line 41
    new-instance v0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult$DataRetrieved;

    .line 42
    new-instance v1, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;

    .line 43
    iget-object v2, p1, Lcom/squareup/protos/bizbank/PrivateCardData;->full_pan:Ljava/lang/String;

    const-string v3, "cardData.full_pan"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    iget-object v3, p1, Lcom/squareup/protos/bizbank/PrivateCardData;->cvc:Ljava/lang/String;

    const-string v4, "cardData.cvc"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v4, p1, Lcom/squareup/protos/bizbank/PrivateCardData;->expiration:Lcom/squareup/protos/common/time/YearMonth;

    iget-object v4, v4, Lcom/squareup/protos/common/time/YearMonth;->month_of_year:Ljava/lang/Integer;

    const-string v5, "cardData.expiration.month_of_year"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 46
    iget-object p1, p1, Lcom/squareup/protos/bizbank/PrivateCardData;->expiration:Lcom/squareup/protos/common/time/YearMonth;

    iget-object p1, p1, Lcom/squareup/protos/common/time/YearMonth;->year:Ljava/lang/Integer;

    const-string v5, "cardData.expiration.year"

    invoke-static {p1, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    .line 42
    invoke-direct {v1, v2, v3, v4, p1}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 41
    invoke-direct {v0, v1}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult$DataRetrieved;-><init>(Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetails;)V

    check-cast v0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;

    return-object v0
.end method

.method private final toOutput(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure<",
            "Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;",
            ">;)",
            "Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;"
        }
    .end annotation

    .line 53
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p1, v1

    :cond_0
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;

    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse;->error:Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;

    :cond_1
    if-nez v1, :cond_2

    goto :goto_0

    :cond_2
    sget-object p1, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v1}, Lcom/squareup/protos/bizbank/GetPrivateCardDataResponse$Error;->ordinal()I

    move-result v0

    aget p1, p1, v0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    .line 55
    :goto_0
    sget-object p1, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult$CouldNotRetrieveData;->INSTANCE:Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult$CouldNotRetrieveData;

    check-cast p1, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;

    goto :goto_1

    .line 54
    :cond_3
    sget-object p1, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult$TwoFactorAuthRequired;->INSTANCE:Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult$TwoFactorAuthRequired;

    check-cast p1, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;

    :goto_1
    return-object p1
.end method


# virtual methods
.method public final privateCardDetailsState(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$SquareCardPrivateDataResult;",
            ">;"
        }
    .end annotation

    const-string v0, "cardData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    new-instance v0, Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest$Builder;-><init>()V

    .line 22
    iget-object p1, p1, Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;->card_token:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest$Builder;->card_token(Ljava/lang/String;)Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest$Builder;

    move-result-object p1

    .line 23
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest$Builder;->build()Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest;

    move-result-object p1

    .line 25
    iget-object v0, p0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;->bizbankService:Lcom/squareup/balance/core/server/bizbank/BizbankService;

    const-string v1, "request"

    .line 26
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService;->getPrivateCardData(Lcom/squareup/protos/bizbank/GetPrivateCardDataRequest;)Lcom/squareup/balance/core/server/bizbank/BizbankService$GetPrivateCardDataStandardResponse;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lcom/squareup/balance/core/server/bizbank/BizbankService$GetPrivateCardDataStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 28
    new-instance v0, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$privateCardDetailsState$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore$privateCardDetailsState$1;-><init>(Lcom/squareup/balance/squarecard/carddetails/SquareCardPrivateCardDetailsDataStore;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "bizbankService\n        .\u2026t()\n          }\n        }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
