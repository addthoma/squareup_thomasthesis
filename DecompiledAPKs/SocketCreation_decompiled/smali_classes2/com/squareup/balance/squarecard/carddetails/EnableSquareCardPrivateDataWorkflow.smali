.class public final Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "EnableSquareCardPrivateDataWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataProps;",
        "Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataOutput;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0018\u00002:\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00080\u0001B\u0011\u0008\u0007\u0012\u0008\u0008\u0001\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJJ\u0010\u000c\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0005\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u00070\u0004j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0005`\u00082\u0006\u0010\r\u001a\u00020\u00022\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00030\u000fH\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataProps;",
        "Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataOutput;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "twoFactorAuthWorkflow",
        "Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;",
        "(Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;)V",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final twoFactorAuthWorkflow:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;)V
    .locals 1
    .param p1    # Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;
        .annotation runtime Lcom/squareup/balance/squarecard/twofactorauth/datastore/qualifiers/ShowPrivateCardDataTwoFactor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "twoFactorAuthWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow;->twoFactorAuthWorkflow:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataProps;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow;->render(Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataProps;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataProps;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataProps;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow;->twoFactorAuthWorkflow:Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 41
    new-instance v3, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;

    .line 42
    invoke-virtual {p1}, Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataProps;->getCardData()Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;

    move-result-object p1

    .line 43
    sget v0, Lcom/squareup/balance/squarecard/impl/R$string;->show_private_data_two_factor_title:I

    .line 41
    invoke-direct {v3, p1, v0}, Lcom/squareup/balance/squarecard/twofactorauth/SquareCardTwoFactorAuthInput;-><init>(Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;I)V

    .line 45
    new-instance p1, Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow$render$1;-><init>(Lcom/squareup/balance/squarecard/carddetails/EnableSquareCardPrivateDataWorkflow;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p2

    .line 39
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    return-object p1
.end method
