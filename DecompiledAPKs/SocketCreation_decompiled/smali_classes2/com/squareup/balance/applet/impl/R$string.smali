.class public final Lcom/squareup/balance/applet/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/applet/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final account_freeze_applet_alert:I = 0x7f120037

.field public static final add_amount_add_funding_source:I = 0x7f120071

.field public static final add_amount_button_text:I = 0x7f120072

.field public static final add_amount_help_text:I = 0x7f120073

.field public static final add_amount_help_text_deposit_settings:I = 0x7f120074

.field public static final add_amount_source:I = 0x7f120075

.field public static final add_amount_success_message:I = 0x7f120076

.field public static final add_amount_title:I = 0x7f120077

.field public static final add_money_button_label:I = 0x7f120085

.field public static final add_money_debit_card_required_message:I = 0x7f120086

.field public static final add_money_debit_card_required_primary_button:I = 0x7f120087

.field public static final add_money_debit_card_required_title:I = 0x7f120088

.field public static final add_money_loading_message:I = 0x7f12008a

.field public static final add_money_pending_card_message:I = 0x7f12008b

.field public static final add_money_pending_card_primary_button:I = 0x7f12008c

.field public static final add_money_pending_card_title:I = 0x7f12008d

.field public static final available_balance_uppercase:I = 0x7f120111

.field public static final balance_sections_error_title:I = 0x7f120136

.field public static final balance_transaction_detail_at_uppercase:I = 0x7f120138

.field public static final balance_transaction_detail_business:I = 0x7f120139

.field public static final balance_transaction_detail_date:I = 0x7f12013a

.field public static final balance_transaction_detail_expense_type_uppercase:I = 0x7f12013b

.field public static final balance_transaction_detail_personal:I = 0x7f12013c

.field public static final balance_transaction_detail_time:I = 0x7f12013d

.field public static final balance_transaction_detail_total_titlecase:I = 0x7f12013e

.field public static final balance_transaction_detail_total_uppercase:I = 0x7f12013f

.field public static final balance_transaction_details_uppercase:I = 0x7f120140

.field public static final bank_account:I = 0x7f120141

.field public static final bank_account_hint:I = 0x7f120146

.field public static final bank_account_required_message:I = 0x7f12014d

.field public static final bank_account_required_primary_button:I = 0x7f12014e

.field public static final bank_account_required_title:I = 0x7f12014f

.field public static final bank_account_settings:I = 0x7f120150

.field public static final card_activity_declined:I = 0x7f1202ef

.field public static final card_activity_load_more_error:I = 0x7f1202f0

.field public static final card_activity_load_more_retry_button:I = 0x7f1202f1

.field public static final card_activity_pending:I = 0x7f1202f2

.field public static final card_spend:I = 0x7f12033c

.field public static final card_spend_detail:I = 0x7f12033d

.field public static final card_suspended_dialog_message:I = 0x7f12033e

.field public static final card_suspended_dialog_title:I = 0x7f12033f

.field public static final card_upsell_message:I = 0x7f120344

.field public static final confirm_transfer_button:I = 0x7f120485

.field public static final confirm_transfer_title:I = 0x7f120486

.field public static final debit_card_hint:I = 0x7f1207e1

.field public static final debit_card_required_message:I = 0x7f1207e2

.field public static final deposit_to_bank:I = 0x7f120807

.field public static final deposits_frozen_help_link_url:I = 0x7f120808

.field public static final deposits_frozen_info:I = 0x7f120809

.field public static final deposits_frozen_info_link:I = 0x7f12080a

.field public static final deposits_frozen_title_uppercase:I = 0x7f12080b

.field public static final deposits_frozen_verification_url:I = 0x7f12080c

.field public static final deposits_frozen_verify_account:I = 0x7f12080d

.field public static final empty_card_activity_message:I = 0x7f120a3b

.field public static final error_card_activity_message:I = 0x7f120a83

.field public static final free:I = 0x7f120ae0

.field public static final hide_instant_deposit_hint:I = 0x7f120b52

.field public static final instant_deposits_unavailable_troubleshooting:I = 0x7f120c38

.field public static final load_balance_error_message_body:I = 0x7f120ee1

.field public static final load_balance_error_message_title:I = 0x7f120ee2

.field public static final load_card_activity_error_message_body:I = 0x7f120ee4

.field public static final load_card_activity_error_message_title:I = 0x7f120ee5

.field public static final order_card_label:I = 0x7f1211f1

.field public static final recent_activity_active_sales:I = 0x7f1215d4

.field public static final recent_activity_balance_title_uppercase:I = 0x7f1215d5

.field public static final recent_activity_card_activity_date_format:I = 0x7f1215d6

.field public static final recent_activity_deposits_title_uppercase:I = 0x7f1215d7

.field public static final recent_activity_empty_message:I = 0x7f1215d8

.field public static final recent_activity_empty_title:I = 0x7f1215d9

.field public static final recent_activity_error_message:I = 0x7f1215da

.field public static final recent_activity_error_title:I = 0x7f1215db

.field public static final recent_activity_pending_deposit:I = 0x7f1215dc

.field public static final recent_activity_sending_date:I = 0x7f1215dd

.field public static final recent_activity_unified_activity_title:I = 0x7f1215de

.field public static final reload_balance:I = 0x7f121660

.field public static final send_square_card_confirmation_subheading:I = 0x7f1217c2

.field public static final send_square_card_subheading:I = 0x7f1217c3

.field public static final sending_to_bank_uppercase:I = 0x7f1217c4

.field public static final square_capital:I = 0x7f12186f

.field public static final square_capital_flex_status_current_plan:I = 0x7f121873

.field public static final square_capital_flex_status_financing_request:I = 0x7f121874

.field public static final square_capital_flex_status_no_offer:I = 0x7f121875

.field public static final square_capital_flex_status_previous_plan:I = 0x7f121877

.field public static final square_card:I = 0x7f121878

.field public static final titlecase_balance_applet_name:I = 0x7f1219da

.field public static final transfer_arrives_instantly:I = 0x7f1219f9

.field public static final transfer_arrives_one_to_two_business_days:I = 0x7f1219fa

.field public static final transfer_arrives_title:I = 0x7f1219fb

.field public static final transfer_available_balance:I = 0x7f1219fc

.field public static final transfer_breakdown_uppercase:I = 0x7f1219fd

.field public static final transfer_complete:I = 0x7f1219fe

.field public static final transfer_deposit_amount:I = 0x7f1219ff

.field public static final transfer_deposit_summary_uppercase:I = 0x7f121a00

.field public static final transfer_deposit_to:I = 0x7f121a01

.field public static final transfer_ending_balance:I = 0x7f121a02

.field public static final transfer_in_progress:I = 0x7f121a03

.field public static final transfer_instant_deposit_fee:I = 0x7f121a04

.field public static final transfer_result_error_message:I = 0x7f121a06

.field public static final transfer_result_error_title:I = 0x7f121a07

.field public static final transfer_result_instant_message_with_bank:I = 0x7f121a08

.field public static final transfer_result_instant_message_with_card:I = 0x7f121a09

.field public static final transfer_result_instant_title:I = 0x7f121a0a

.field public static final transfer_result_processing:I = 0x7f121a0b

.field public static final transfer_result_standard_message:I = 0x7f121a0c

.field public static final transfer_result_standard_title:I = 0x7f121a0d

.field public static final transfer_speed_hint:I = 0x7f121a0e

.field public static final transfer_speed_instant:I = 0x7f121a0f

.field public static final transfer_speed_instant_fee_amount:I = 0x7f121a10

.field public static final transfer_speed_instant_fee_rate:I = 0x7f121a11

.field public static final transfer_speed_standard:I = 0x7f121a12

.field public static final transfer_speed_uppercase:I = 0x7f121a13

.field public static final unable_to_change_expense_body:I = 0x7f121ade

.field public static final unable_to_change_expense_title:I = 0x7f121adf

.field public static final unified_activity_section_label:I = 0x7f121ae8

.field public static final upcoming_deposits_uppercase:I = 0x7f121af0

.field public static final uppercase_balance_applet_name:I = 0x7f121b08


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
