.class public final Lcom/squareup/balance/core/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/core/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final SquareCardPreview:[I

.field public static final SquareCardPreview_sqCardCornerRadius:I = 0x0

.field public static final SquareCardPreview_sqCardOrientation:I = 0x1

.field public static final SquareCardPreview_sqMaxCardWidth:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [I

    .line 137
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/balance/core/R$styleable;->SquareCardPreview:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f04036c
        0x7f04036d
        0x7f040399
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
