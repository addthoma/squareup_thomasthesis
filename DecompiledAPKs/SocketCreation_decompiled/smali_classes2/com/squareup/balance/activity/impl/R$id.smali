.class public final Lcom/squareup/balance/activity/impl/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/activity/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final balance_activities_child_container:I = 0x7f0a01f4

.field public static final balance_activities_list:I = 0x7f0a01f5

.field public static final balance_activities_list_container:I = 0x7f0a01f6

.field public static final balance_activities_loading:I = 0x7f0a01f7

.field public static final balance_activities_pull_to_refresh:I = 0x7f0a01f8

.field public static final balance_activities_title:I = 0x7f0a01f9

.field public static final balance_activity_action_bar:I = 0x7f0a01fa

.field public static final balance_activity_business_loading:I = 0x7f0a01fb

.field public static final balance_activity_description_list_state:I = 0x7f0a01fc

.field public static final balance_activity_details_amount:I = 0x7f0a01fd

.field public static final balance_activity_details_checkable_expense_type:I = 0x7f0a01fe

.field public static final balance_activity_details_expense_type_business:I = 0x7f0a01ff

.field public static final balance_activity_details_expense_type_personal:I = 0x7f0a0200

.field public static final balance_activity_details_expense_type_section:I = 0x7f0a0201

.field public static final balance_activity_details_expense_type_title:I = 0x7f0a0202

.field public static final balance_activity_details_footnote:I = 0x7f0a0203

.field public static final balance_activity_details_image:I = 0x7f0a0204

.field public static final balance_activity_details_list:I = 0x7f0a0205

.field public static final balance_activity_details_loaded_container:I = 0x7f0a0206

.field public static final balance_activity_details_main_description:I = 0x7f0a0207

.field public static final balance_activity_details_retry_button:I = 0x7f0a0208

.field public static final balance_activity_empty_text:I = 0x7f0a0209

.field public static final balance_activity_personal_loading:I = 0x7f0a020a

.field public static final balance_activity_retry:I = 0x7f0a020b

.field public static final balance_activity_tab_layout:I = 0x7f0a020c

.field public static final balance_activity_text:I = 0x7f0a020d

.field public static final community_reward_amount_label:I = 0x7f0a0367

.field public static final community_reward_amount_value:I = 0x7f0a0368

.field public static final community_reward_description:I = 0x7f0a0369

.field public static final community_reward_description_link:I = 0x7f0a036a

.field public static final community_reward_instant_discount_label:I = 0x7f0a036b

.field public static final community_reward_instant_discount_value:I = 0x7f0a036c

.field public static final community_reward_total_label:I = 0x7f0a036d

.field public static final community_reward_total_value:I = 0x7f0a036e

.field public static final tab_item_all:I = 0x7f0a0f5f

.field public static final tab_item_card_spend:I = 0x7f0a0f60

.field public static final tab_item_transfers:I = 0x7f0a0f61


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
