.class public final Lcom/squareup/balance/activity/impl/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/activity/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final balance_activity_action_bar_title:I = 0x7f12011e

.field public static final balance_activity_all_tab_bottom_message:I = 0x7f12011f

.field public static final balance_activity_details_error_message:I = 0x7f120120

.field public static final balance_activity_details_error_retry:I = 0x7f120121

.field public static final balance_activity_details_expense_type_title:I = 0x7f120122

.field public static final balance_activity_empty_message_all:I = 0x7f120123

.field public static final balance_activity_empty_message_card_spend:I = 0x7f120124

.field public static final balance_activity_empty_message_transfers:I = 0x7f120125

.field public static final balance_activity_error:I = 0x7f120126

.field public static final balance_activity_loading_message:I = 0x7f120127

.field public static final balance_activity_navigate_to_transfer_reports:I = 0x7f120128

.field public static final balance_activity_retry:I = 0x7f120129

.field public static final balance_activity_sub_label_card_payment:I = 0x7f12012a

.field public static final balance_activity_sub_label_card_processing:I = 0x7f12012b

.field public static final balance_activity_sub_label_dispute_lost:I = 0x7f12012c

.field public static final balance_activity_sub_label_instant_transfer_out:I = 0x7f12012d

.field public static final balance_activity_sub_label_payroll_cancellation:I = 0x7f12012e

.field public static final balance_activity_sub_label_transfer_in:I = 0x7f12012f

.field public static final balance_activity_sub_label_transfer_out:I = 0x7f120130

.field public static final balance_activity_tab_all:I = 0x7f120131

.field public static final balance_activity_tab_card_spend:I = 0x7f120132

.field public static final balance_activity_tab_card_transfers:I = 0x7f120133

.field public static final balance_activity_title_completed:I = 0x7f120134

.field public static final balance_activity_title_pending:I = 0x7f120135

.field public static final community_reward_browser_url:I = 0x7f120454

.field public static final community_reward_description:I = 0x7f120455

.field public static final community_reward_description_empty:I = 0x7f120456

.field public static final community_reward_description_link:I = 0x7f120457

.field public static final community_reward_label_amount:I = 0x7f120458

.field public static final community_reward_label_instant_discount:I = 0x7f120459

.field public static final community_reward_label_total:I = 0x7f12045a

.field public static final community_reward_title:I = 0x7f12045b

.field public static final error_updating_expense_category:I = 0x7f120a86

.field public static final expense_type_business:I = 0x7f120a96

.field public static final expense_type_personal:I = 0x7f120a97

.field public static final footnote_message_adjustments:I = 0x7f120ad6

.field public static final gross_amount_atm_withdrawal:I = 0x7f120b47

.field public static final gross_amount_cashback:I = 0x7f120b48

.field public static final gross_amount_instant_transfer_fee:I = 0x7f120b49

.field public static final modifier_atm_withdrawal_fee:I = 0x7f120ffd

.field public static final modifier_cash_back:I = 0x7f120ffe

.field public static final modifier_community_reward:I = 0x7f120fff

.field public static final modifier_instant_transfer_fee:I = 0x7f121004

.field public static final state_completed:I = 0x7f1218bf

.field public static final state_declined:I = 0x7f1218c0

.field public static final state_dispute_reversed:I = 0x7f1218c1

.field public static final state_dispute_won:I = 0x7f1218c2

.field public static final state_disputed:I = 0x7f1218c3

.field public static final state_pending:I = 0x7f1218c6

.field public static final state_refunded:I = 0x7f1218c7

.field public static final state_voided:I = 0x7f1218c8

.field public static final title_atm_withdrawal:I = 0x7f1219ce

.field public static final title_card_payment:I = 0x7f1219cf

.field public static final title_card_processing_adjustment:I = 0x7f1219d0

.field public static final title_card_processing_sales:I = 0x7f1219d1

.field public static final title_debit_card_transfer_in:I = 0x7f1219d2

.field public static final title_instant_transfer_out:I = 0x7f1219d4

.field public static final title_payroll:I = 0x7f1219d5

.field public static final title_promotional_reward:I = 0x7f1219d6

.field public static final title_standard_transfer_out:I = 0x7f1219d7

.field public static final title_unknown:I = 0x7f1219d8


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
