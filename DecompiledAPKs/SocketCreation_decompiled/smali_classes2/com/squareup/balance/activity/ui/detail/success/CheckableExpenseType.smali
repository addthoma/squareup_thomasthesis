.class public final Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;
.super Lcom/squareup/noho/NohoLinearLayout;
.source "CheckableExpenseType.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001\u001cB%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0006\u0010\u0011\u001a\u00020\u0012J\u001a\u0010\u0013\u001a\u00020\u00122\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u00120\u0015J\u0008\u0010\u0017\u001a\u00020\u0012H\u0002J\u0018\u0010\u0018\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u00162\u0008\u0008\u0002\u0010\u001a\u001a\u00020\u001bR\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;",
        "Lcom/squareup/noho/NohoLinearLayout;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "expenseTypeBusiness",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "expenseTypeBusinessLoading",
        "Landroid/view/View;",
        "expenseTypeCheckableGroup",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "expenseTypePersonal",
        "expenseTypePersonalLoading",
        "clearOnUpdateListener",
        "",
        "setOnUpdateListener",
        "onSelected",
        "Lkotlin/Function1;",
        "Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;",
        "showAndResetSections",
        "updateSelection",
        "selectionStatus",
        "isLoading",
        "",
        "SelectionStatus",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final expenseTypeBusiness:Lcom/squareup/noho/NohoCheckableRow;

.field private final expenseTypeBusinessLoading:Landroid/view/View;

.field private final expenseTypeCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

.field private final expenseTypePersonal:Lcom/squareup/noho/NohoCheckableRow;

.field private final expenseTypePersonalLoading:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/noho/NohoLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    sget p2, Lcom/squareup/balance/activity/impl/R$layout;->balance_activity_details_expense_type_checkable:I

    move-object p3, p0

    check-cast p3, Landroid/view/ViewGroup;

    invoke-static {p1, p2, p3}, Lcom/squareup/noho/NohoLinearLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 31
    sget p1, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_details_checkable_expense_type:I

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.balanc\u2026s_checkable_expense_type)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    .line 32
    sget p1, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_details_expense_type_personal:I

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.balanc\u2026ls_expense_type_personal)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypePersonal:Lcom/squareup/noho/NohoCheckableRow;

    .line 33
    sget p1, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_details_expense_type_business:I

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.balanc\u2026ls_expense_type_business)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeBusiness:Lcom/squareup/noho/NohoCheckableRow;

    .line 34
    sget p1, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_personal_loading:I

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.balanc\u2026ctivity_personal_loading)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypePersonalLoading:Landroid/view/View;

    .line 35
    sget p1, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_business_loading:I

    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "findViewById(R.id.balanc\u2026ctivity_business_loading)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeBusinessLoading:Landroid/view/View;

    const/4 p1, 0x1

    .line 37
    invoke-virtual {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->setOrientation(I)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 18
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 19
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static final synthetic access$getExpenseTypeBusiness$p(Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;)Lcom/squareup/noho/NohoCheckableRow;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeBusiness:Lcom/squareup/noho/NohoCheckableRow;

    return-object p0
.end method

.method public static final synthetic access$getExpenseTypePersonal$p(Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;)Lcom/squareup/noho/NohoCheckableRow;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypePersonal:Lcom/squareup/noho/NohoCheckableRow;

    return-object p0
.end method

.method private final showAndResetSections()V
    .locals 1

    .line 94
    invoke-static {p0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableGroup;->clearChecked()V

    return-void
.end method

.method public static synthetic updateSelection$default(Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 42
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->updateSelection(Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;Z)V

    return-void
.end method


# virtual methods
.method public final clearOnUpdateListener()V
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    const/4 v1, 0x0

    check-cast v1, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method public final setOnUpdateListener(Lkotlin/jvm/functions/Function1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onSelected"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    new-instance v1, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$setOnUpdateListener$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$setOnUpdateListener$1;-><init>(Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method public final updateSelection(Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;Z)V
    .locals 1

    const-string v0, "selectionStatus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->showAndResetSections()V

    .line 47
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeBusinessLoading:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypePersonalLoading:Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeBusiness:Lcom/squareup/noho/NohoCheckableRow;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypePersonal:Lcom/squareup/noho/NohoCheckableRow;

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 52
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 63
    :cond_0
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypePersonal:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableRow;->getId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    if-nez p2, :cond_1

    .line 65
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypePersonal:Lcom/squareup/noho/NohoCheckableRow;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 67
    :cond_1
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypePersonal:Lcom/squareup/noho/NohoCheckableRow;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 68
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypePersonalLoading:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 54
    :cond_2
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeCheckableGroup:Lcom/squareup/noho/NohoCheckableGroup;

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeBusiness:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableRow;->getId()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    if-nez p2, :cond_3

    .line 56
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeBusiness:Lcom/squareup/noho/NohoCheckableRow;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto :goto_0

    .line 58
    :cond_3
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeBusiness:Lcom/squareup/noho/NohoCheckableRow;

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 59
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->expenseTypeBusinessLoading:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    :goto_0
    return-void
.end method
