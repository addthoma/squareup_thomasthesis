.class public final Lcom/squareup/balance/activity/ui/RealBalanceActivityViewFactory$Factory;
.super Ljava/lang/Object;
.source "RealBalanceActivityViewFactory.kt"

# interfaces
.implements Lcom/squareup/balance/activity/ui/BalanceActivityViewFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/balance/activity/ui/RealBalanceActivityViewFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\u00020\u00082\u000c\u0010\t\u001a\u0008\u0012\u0002\u0008\u0003\u0018\u00010\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/RealBalanceActivityViewFactory$Factory;",
        "Lcom/squareup/balance/activity/ui/BalanceActivityViewFactory;",
        "balanceActivityInternalViewFactoryFactory",
        "Lcom/squareup/balance/activity/ui/BalanceActivityInternalViewFactory$Factory;",
        "transferReportsViewFactoryFactory",
        "Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;",
        "(Lcom/squareup/balance/activity/ui/BalanceActivityInternalViewFactory$Factory;Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;)V",
        "create",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "section",
        "Ljava/lang/Class;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final balanceActivityInternalViewFactoryFactory:Lcom/squareup/balance/activity/ui/BalanceActivityInternalViewFactory$Factory;

.field private final transferReportsViewFactoryFactory:Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;


# direct methods
.method public constructor <init>(Lcom/squareup/balance/activity/ui/BalanceActivityInternalViewFactory$Factory;Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "balanceActivityInternalViewFactoryFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transferReportsViewFactoryFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/RealBalanceActivityViewFactory$Factory;->balanceActivityInternalViewFactoryFactory:Lcom/squareup/balance/activity/ui/BalanceActivityInternalViewFactory$Factory;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/RealBalanceActivityViewFactory$Factory;->transferReportsViewFactoryFactory:Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;

    return-void
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Lcom/squareup/workflow/WorkflowViewFactory;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "Lcom/squareup/workflow/WorkflowViewFactory;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/balance/activity/ui/RealBalanceActivityViewFactory;

    .line 28
    iget-object v1, p0, Lcom/squareup/balance/activity/ui/RealBalanceActivityViewFactory$Factory;->balanceActivityInternalViewFactoryFactory:Lcom/squareup/balance/activity/ui/BalanceActivityInternalViewFactory$Factory;

    invoke-virtual {v1, p1}, Lcom/squareup/balance/activity/ui/BalanceActivityInternalViewFactory$Factory;->create(Ljava/lang/Class;)Lcom/squareup/balance/activity/ui/BalanceActivityInternalViewFactory;

    move-result-object v1

    .line 29
    iget-object v2, p0, Lcom/squareup/balance/activity/ui/RealBalanceActivityViewFactory$Factory;->transferReportsViewFactoryFactory:Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;

    invoke-virtual {v2, p1}, Lcom/squareup/transferreports/TransferReportsViewFactory$Factory;->create(Ljava/lang/Class;)Lcom/squareup/transferreports/TransferReportsViewFactory;

    move-result-object p1

    .line 27
    invoke-direct {v0, v1, p1}, Lcom/squareup/balance/activity/ui/RealBalanceActivityViewFactory;-><init>(Lcom/squareup/balance/activity/ui/BalanceActivityInternalViewFactory;Lcom/squareup/transferreports/TransferReportsViewFactory;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowViewFactory;

    return-object v0
.end method
