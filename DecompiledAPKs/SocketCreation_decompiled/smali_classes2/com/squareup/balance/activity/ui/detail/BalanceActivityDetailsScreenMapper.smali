.class public final Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;
.super Ljava/lang/Object;
.source "BalanceActivityDetailsScreenMapper.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBalanceActivityDetailsScreenMapper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BalanceActivityDetailsScreenMapper.kt\ncom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,245:1\n704#2:246\n777#2,2:247\n1360#2:249\n1429#2,3:250\n1360#2:253\n1429#2,3:254\n950#2:257\n*E\n*S KotlinDebug\n*F\n+ 1 BalanceActivityDetailsScreenMapper.kt\ncom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper\n*L\n143#1:246\n143#1,2:247\n147#1:249\n147#1,3:250\n181#1:253\n181#1,3:254\n202#1:257\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000~\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\r\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B=\u0008\u0007\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000e\u0008\u0001\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u00a2\u0006\u0002\u0010\u000bJ\u000e\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u000e\u001a\u00020\u000fJ\u0016\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015J\u000e\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u000fJ\u001c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001b0\u001a2\u0006\u0010\u0018\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015J\u000e\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u0018\u001a\u00020\u000fJ\u000e\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0018\u001a\u00020\u000fJ\u0012\u0010\u001f\u001a\u0008\u0012\u0004\u0012\u00020!0 *\u00020\"H\u0002J$\u0010#\u001a\u00020\u001b*\u0008\u0012\u0004\u0012\u00020!0 2\u0008\u0008\u0002\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'H\u0002J\u001c\u0010(\u001a\u00020\u001b*\u0008\u0012\u0004\u0012\u00020!0 2\u0008\u0008\u0002\u0010$\u001a\u00020%H\u0002R\u0014\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;",
        "",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "amountMoneyFormatter",
        "dateFormatter",
        "Ljava/text/DateFormat;",
        "locale",
        "Ljavax/inject/Provider;",
        "Ljava/util/Locale;",
        "(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljavax/inject/Provider;)V",
        "actionBarTitle",
        "Lcom/squareup/util/ViewString;",
        "unifiedActivity",
        "Lcom/squareup/protos/bizbank/UnifiedActivity;",
        "amount",
        "Lcom/squareup/balance/activity/ui/common/Amount;",
        "expenseType",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;",
        "details",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails;",
        "footnoteMessage",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;",
        "activity",
        "itemizedDescriptions",
        "",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;",
        "mainDescription",
        "merchantImage",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;",
        "toDescriptionText",
        "Lcom/squareup/resources/TextModel;",
        "",
        "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;",
        "toPillDescription",
        "priority",
        "",
        "onClick",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;",
        "toSimpleDescription",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amountMoneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatter:Ljava/text/DateFormat;

.field private final locale:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/text/Formatter;Lcom/squareup/text/Formatter;Ljava/text/DateFormat;Ljavax/inject/Provider;)V
    .locals 1
    .param p2    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/money/PositiveNegativeFormat;
        .end annotation
    .end param
    .param p3    # Ljava/text/DateFormat;
        .annotation runtime Lcom/squareup/text/MediumFormNoYear;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/text/DateFormat;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "moneyFormatter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amountMoneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->amountMoneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->dateFormatter:Ljava/text/DateFormat;

    iput-object p4, p0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->locale:Ljavax/inject/Provider;

    return-void
.end method

.method private final toDescriptionText(Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;)Lcom/squareup/resources/TextModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;",
            ")",
            "Lcom/squareup/resources/TextModel<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .line 233
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    .line 241
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    :pswitch_0
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->state_pending:I

    goto :goto_0

    .line 240
    :pswitch_1
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->state_refunded:I

    goto :goto_0

    .line 239
    :pswitch_2
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->state_voided:I

    goto :goto_0

    .line 238
    :pswitch_3
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->state_completed:I

    goto :goto_0

    .line 237
    :pswitch_4
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->state_disputed:I

    goto :goto_0

    .line 236
    :pswitch_5
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->state_dispute_won:I

    goto :goto_0

    .line 235
    :pswitch_6
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->state_dispute_reversed:I

    goto :goto_0

    .line 234
    :pswitch_7
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->state_declined:I

    .line 242
    :goto_0
    new-instance v0, Lcom/squareup/resources/ResourceCharSequence;

    invoke-direct {v0, p1}, Lcom/squareup/resources/ResourceCharSequence;-><init>(I)V

    check-cast v0, Lcom/squareup/resources/TextModel;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private final toPillDescription(Lcom/squareup/resources/TextModel;ILcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;I",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;",
            ")",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;"
        }
    .end annotation

    .line 229
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description$PillDecorationDescription;

    invoke-direct {v0, p2, p1, p3}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description$PillDecorationDescription;-><init>(ILcom/squareup/resources/TextModel;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;)V

    check-cast v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    return-object v0
.end method

.method static synthetic toPillDescription$default(Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;Lcom/squareup/resources/TextModel;ILcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;ILjava/lang/Object;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;
    .locals 0

    and-int/lit8 p4, p4, 0x1

    if-eqz p4, :cond_0

    const/16 p2, 0xa

    .line 226
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->toPillDescription(Lcom/squareup/resources/TextModel;ILcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    move-result-object p0

    return-object p0
.end method

.method private final toSimpleDescription(Lcom/squareup/resources/TextModel;I)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/resources/TextModel<",
            "+",
            "Ljava/lang/CharSequence;",
            ">;I)",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;"
        }
    .end annotation

    .line 222
    new-instance v6, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description$SimpleLineDescription;

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, v6

    move v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description$SimpleLineDescription;-><init>(ILcom/squareup/resources/TextModel;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v6, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    return-object v6
.end method

.method static synthetic toSimpleDescription$default(Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;Lcom/squareup/resources/TextModel;IILjava/lang/Object;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const/16 p2, 0xa

    .line 220
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->toSimpleDescription(Lcom/squareup/resources/TextModel;I)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final actionBarTitle(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/util/ViewString;
    .locals 1

    const-string v0, "unifiedActivity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 93
    :pswitch_0
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->title_payroll:I

    goto :goto_1

    .line 91
    :pswitch_1
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->title_promotional_reward:I

    goto :goto_1

    .line 90
    :pswitch_2
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->title_standard_transfer_out:I

    goto :goto_1

    .line 89
    :pswitch_3
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->title_instant_transfer_out:I

    goto :goto_1

    .line 88
    :pswitch_4
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->title_debit_card_transfer_in:I

    goto :goto_1

    .line 87
    :pswitch_5
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->title_card_processing_adjustment:I

    goto :goto_1

    .line 86
    :pswitch_6
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->title_card_processing_sales:I

    goto :goto_1

    .line 85
    :pswitch_7
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->title_card_payment:I

    goto :goto_1

    .line 84
    :pswitch_8
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->title_atm_withdrawal:I

    goto :goto_1

    .line 94
    :goto_0
    sget p1, Lcom/squareup/balance/activity/impl/R$string;->title_unknown:I

    .line 97
    :goto_1
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v0, Lcom/squareup/util/ViewString;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final amount(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/common/Amount;
    .locals 3

    const-string v0, "unifiedActivity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    new-instance v0, Lcom/squareup/resources/FixedText;

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->amountMoneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v2, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v1, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-direct {v0, v1}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    .line 104
    iget-object v1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const-string v2, "unifiedActivity.activity_state"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/balance/activity/ui/common/MapperHelperKt;->shouldCrossOut(Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance p1, Lcom/squareup/balance/activity/ui/common/Amount$CrossedOutAmount;

    check-cast v0, Lcom/squareup/resources/TextModel;

    invoke-direct {p1, v0}, Lcom/squareup/balance/activity/ui/common/Amount$CrossedOutAmount;-><init>(Lcom/squareup/resources/TextModel;)V

    check-cast p1, Lcom/squareup/balance/activity/ui/common/Amount;

    goto :goto_0

    .line 105
    :cond_0
    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->amount:Lcom/squareup/protos/common/Money;

    invoke-static {p1}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lcom/squareup/balance/activity/ui/common/Amount$PositiveAmount;

    check-cast v0, Lcom/squareup/resources/TextModel;

    invoke-direct {p1, v0}, Lcom/squareup/balance/activity/ui/common/Amount$PositiveAmount;-><init>(Lcom/squareup/resources/TextModel;)V

    check-cast p1, Lcom/squareup/balance/activity/ui/common/Amount;

    goto :goto_0

    .line 106
    :cond_1
    new-instance p1, Lcom/squareup/balance/activity/ui/common/Amount$UndecoratedAmount;

    check-cast v0, Lcom/squareup/resources/TextModel;

    invoke-direct {p1, v0}, Lcom/squareup/balance/activity/ui/common/Amount$UndecoratedAmount;-><init>(Lcom/squareup/resources/TextModel;)V

    check-cast p1, Lcom/squareup/balance/activity/ui/common/Amount;

    :goto_0
    return-object p1
.end method

.method public final expenseType(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;
    .locals 1

    const-string v0, "unifiedActivity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "details"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    .line 116
    :goto_0
    sget-object p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType$None;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType$None;

    check-cast p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;

    goto :goto_2

    .line 115
    :cond_1
    iget-object p1, p2, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->is_personal_expense:Ljava/lang/Boolean;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    sget-object p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType$PersonalSelected;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType$PersonalSelected;

    goto :goto_1

    :cond_2
    sget-object p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType$BusinessSelected;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType$BusinessSelected;

    :goto_1
    check-cast p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;

    :goto_2
    return-object p1
.end method

.method public final footnoteMessage(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;
    .locals 2

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_type:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    .line 210
    :goto_0
    sget-object p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage$None;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage$None;

    check-cast p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;

    goto :goto_1

    .line 207
    :cond_1
    new-instance p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage$SimpleMessage;

    .line 208
    new-instance v0, Lcom/squareup/resources/ResourceCharSequence;

    sget v1, Lcom/squareup/balance/activity/impl/R$string;->footnote_message_adjustments:I

    invoke-direct {v0, v1}, Lcom/squareup/resources/ResourceCharSequence;-><init>(I)V

    check-cast v0, Lcom/squareup/resources/TextModel;

    .line 207
    invoke-direct {p1, v0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage$SimpleMessage;-><init>(Lcom/squareup/resources/TextModel;)V

    check-cast p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;

    :goto_1
    return-object p1
.end method

.method public final itemizedDescriptions(Lcom/squareup/protos/bizbank/UnifiedActivity;Lcom/squareup/protos/bizbank/UnifiedActivityDetails;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/bizbank/UnifiedActivity;",
            "Lcom/squareup/protos/bizbank/UnifiedActivityDetails;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;",
            ">;"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v0, p1

    move-object/from16 v7, p2

    const-string v1, "activity"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "details"

    invoke-static {v7, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object v8, v1

    check-cast v8, Ljava/util/List;

    .line 131
    iget-object v1, v0, Lcom/squareup/protos/bizbank/UnifiedActivity;->latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v9, 0x0

    const/4 v2, 0x0

    const/4 v10, 0x1

    if-eqz v1, :cond_0

    .line 132
    move-object v1, v8

    check-cast v1, Ljava/util/Collection;

    new-instance v3, Lcom/squareup/resources/FixedText;

    .line 133
    iget-object v4, v6, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->dateFormatter:Ljava/text/DateFormat;

    iget-object v5, v0, Lcom/squareup/protos/bizbank/UnifiedActivity;->latest_event_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v11, v6, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->locale:Ljavax/inject/Provider;

    invoke-interface {v11}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Locale;

    invoke-static {v5, v11}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "dateFormatter.format(asD\u2026_event_at, locale.get()))"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/lang/CharSequence;

    .line 132
    invoke-direct {v3, v4}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/resources/TextModel;

    .line 134
    invoke-static {v6, v3, v2, v10, v9}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->toSimpleDescription$default(Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;Lcom/squareup/resources/TextModel;IILjava/lang/Object;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    move-result-object v3

    .line 132
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 138
    :cond_0
    move-object v11, v8

    check-cast v11, Ljava/util/Collection;

    iget-object v0, v0, Lcom/squareup/protos/bizbank/UnifiedActivity;->activity_state:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const-string v1, "activity.activity_state"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    invoke-direct {v6, v0}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->toDescriptionText(Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;)Lcom/squareup/resources/TextModel;

    move-result-object v0

    .line 140
    invoke-static {v6, v0, v2, v10, v9}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->toSimpleDescription$default(Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;Lcom/squareup/resources/TextModel;IILjava/lang/Object;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    move-result-object v0

    .line 138
    invoke-interface {v11, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 142
    iget-object v0, v7, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->modifiers:Ljava/util/List;

    const-string v1, "details.modifiers"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 246
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 247
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    .line 143
    iget-object v4, v4, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_1

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 248
    :cond_3
    check-cast v1, Ljava/util/List;

    .line 146
    move-object v12, v1

    check-cast v12, Ljava/lang/Iterable;

    .line 249
    new-instance v0, Ljava/util/ArrayList;

    const/16 v13, 0xa

    invoke-static {v12, v13}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    move-object v14, v0

    check-cast v14, Ljava/util/Collection;

    .line 250
    invoke-interface {v12}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    const-string v1, "amount"

    if-eqz v0, :cond_b

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 251
    check-cast v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    .line 148
    iget-object v2, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    if-nez v2, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    sget-object v3, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {v2}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ordinal()I

    move-result v2

    aget v2, v3, v2

    if-eq v2, v10, :cond_a

    const/4 v3, 0x2

    if-eq v2, v3, :cond_8

    const/4 v3, 0x3

    const/16 v4, 0x9

    if-eq v2, v3, :cond_7

    const/4 v3, 0x4

    if-eq v2, v3, :cond_6

    const/4 v3, 0x5

    if-ne v2, v3, :cond_5

    .line 172
    new-instance v2, Lcom/squareup/resources/PhraseModel;

    sget v3, Lcom/squareup/balance/activity/impl/R$string;->modifier_cash_back:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 173
    iget-object v3, v6, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v0, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v3, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object v0

    check-cast v0, Lcom/squareup/resources/TextModel;

    .line 174
    invoke-direct {v6, v0, v4}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->toSimpleDescription(Lcom/squareup/resources/TextModel;I)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    move-result-object v0

    goto/16 :goto_4

    :cond_5
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 167
    :cond_6
    new-instance v2, Lcom/squareup/resources/PhraseModel;

    sget v3, Lcom/squareup/balance/activity/impl/R$string;->modifier_instant_transfer_fee:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 168
    iget-object v3, v6, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v0, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v3, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object v0

    check-cast v0, Lcom/squareup/resources/TextModel;

    .line 169
    invoke-direct {v6, v0, v4}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->toSimpleDescription(Lcom/squareup/resources/TextModel;I)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    move-result-object v0

    goto :goto_4

    .line 162
    :cond_7
    new-instance v2, Lcom/squareup/resources/PhraseModel;

    sget v3, Lcom/squareup/balance/activity/impl/R$string;->modifier_atm_withdrawal_fee:I

    invoke-direct {v2, v3}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 163
    iget-object v3, v6, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v0, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v3, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object v0

    check-cast v0, Lcom/squareup/resources/TextModel;

    .line 164
    invoke-direct {v6, v0, v4}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->toSimpleDescription(Lcom/squareup/resources/TextModel;I)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    move-result-object v0

    goto :goto_4

    .line 151
    :cond_8
    iget-object v2, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount_description:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 152
    iget-object v2, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount_description:Ljava/lang/String;

    check-cast v2, Ljava/lang/CharSequence;

    goto :goto_3

    .line 154
    :cond_9
    iget-object v2, v6, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount:Lcom/squareup/protos/common/Money;

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 156
    :goto_3
    new-instance v3, Lcom/squareup/resources/PhraseModel;

    sget v4, Lcom/squareup/balance/activity/impl/R$string;->modifier_community_reward:I

    invoke-direct {v3, v4}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 157
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object v1

    check-cast v1, Lcom/squareup/resources/TextModel;

    const/4 v2, 0x0

    .line 159
    new-instance v3, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick$CommunityReward;

    const-string v4, "modifier"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v3, v0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick$CommunityReward;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;)V

    check-cast v3, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-static/range {v0 .. v5}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->toPillDescription$default(Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;Lcom/squareup/resources/TextModel;ILcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;ILjava/lang/Object;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    move-result-object v0

    .line 176
    :goto_4
    invoke-interface {v14, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 149
    :cond_a
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Modifier type is not allowed"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 252
    :cond_b
    check-cast v14, Ljava/util/List;

    check-cast v14, Ljava/lang/Iterable;

    .line 146
    invoke-static {v11, v14}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 181
    iget-object v0, v7, Lcom/squareup/protos/bizbank/UnifiedActivityDetails;->gross_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_10

    .line 253
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v12, v13}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 254
    invoke-interface {v12}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 255
    check-cast v4, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    .line 182
    iget-object v4, v4, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 256
    :cond_c
    check-cast v2, Ljava/util/List;

    .line 187
    sget-object v3, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ATM_WITHDRAWAL_FEE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    sget v2, Lcom/squareup/balance/activity/impl/R$string;->gross_amount_atm_withdrawal:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    goto :goto_6

    .line 188
    :cond_d
    sget-object v3, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->INSTANT_TRANSFER_FEE:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    sget v2, Lcom/squareup/balance/activity/impl/R$string;->gross_amount_instant_transfer_fee:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    goto :goto_6

    .line 189
    :cond_e
    sget-object v3, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->CASHBACK:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    sget v2, Lcom/squareup/balance/activity/impl/R$string;->gross_amount_cashback:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    :cond_f
    :goto_6
    if-eqz v9, :cond_10

    .line 194
    new-instance v2, Lcom/squareup/resources/PhraseModel;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/squareup/resources/PhraseModel;-><init>(I)V

    .line 195
    iget-object v3, v6, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {v3, v0}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/squareup/resources/PhraseModel;->with(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/resources/PhraseModel;

    move-result-object v0

    check-cast v0, Lcom/squareup/resources/TextModel;

    const/16 v1, 0x8

    .line 196
    invoke-direct {v6, v0, v1}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper;->toSimpleDescription(Lcom/squareup/resources/TextModel;I)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    move-result-object v0

    .line 194
    invoke-interface {v11, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 202
    :cond_10
    check-cast v8, Ljava/lang/Iterable;

    .line 257
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$itemizedDescriptions$$inlined$sortedBy$1;

    invoke-direct {v0}, Lcom/squareup/balance/activity/ui/detail/BalanceActivityDetailsScreenMapper$itemizedDescriptions$$inlined$sortedBy$1;-><init>()V

    check-cast v0, Ljava/util/Comparator;

    invoke-static {v8, v0}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final mainDescription(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;
    .locals 7

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    new-instance v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description$SimpleLineDescription;

    new-instance v1, Lcom/squareup/resources/FixedText;

    iget-object p1, p1, Lcom/squareup/protos/bizbank/UnifiedActivity;->description:Ljava/lang/String;

    const-string v2, "activity.description"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {v1, p1}, Lcom/squareup/resources/FixedText;-><init>(Ljava/lang/CharSequence;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/resources/TextModel;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description$SimpleLineDescription;-><init>(ILcom/squareup/resources/TextModel;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    return-object v0
.end method

.method public final merchantImage(Lcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;
    .locals 1

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    sget-object p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage$NotAvailable;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage$NotAvailable;

    check-cast p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;

    return-object p1
.end method
