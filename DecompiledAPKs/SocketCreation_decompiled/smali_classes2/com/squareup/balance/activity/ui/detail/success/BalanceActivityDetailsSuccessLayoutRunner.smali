.class public final Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;
.super Ljava/lang/Object;
.source "BalanceActivityDetailsSuccessLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$Binding;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBalanceActivityDetailsSuccessLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BalanceActivityDetailsSuccessLayoutRunner.kt\ncom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,260:1\n1360#2:261\n1429#2,3:262\n1642#2,2:265\n*E\n*S KotlinDebug\n*F\n+ 1 BalanceActivityDetailsSuccessLayoutRunner.kt\ncom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner\n*L\n190#1:261\n190#1,3:262\n212#1,2:265\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u00015B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u001c\u001a\u00020\u001dH\u0002J\u0008\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u001e\u001a\u00020\u001d2\u0006\u0010\u001f\u001a\u00020 H\u0002J\u001e\u0010!\u001a\u00020\u001d2\u0006\u0010\"\u001a\u00020\u00022\u000c\u0010#\u001a\u0008\u0012\u0004\u0012\u00020%0$H\u0002J\u0010\u0010&\u001a\u00020\u001d2\u0006\u0010\'\u001a\u00020(H\u0002J\u0010\u0010)\u001a\u00020\u001d2\u0006\u0010*\u001a\u00020\u0002H\u0002J\u0010\u0010+\u001a\u00020\u001d2\u0006\u0010,\u001a\u00020-H\u0002J\u0010\u0010.\u001a\u00020\u001d2\u0006\u0010*\u001a\u00020\u0002H\u0002J\u0008\u0010/\u001a\u00020\u001dH\u0002J\u0018\u00100\u001a\u00020\u001d2\u0006\u00101\u001a\u00020\u00022\u0006\u00102\u001a\u000203H\u0016J\u0010\u00104\u001a\u00020\u001d2\u0006\u0010\"\u001a\u00020\u0002H\u0002R\u0016\u0010\u0006\u001a\n \u0008*\u0004\u0018\u00010\u00070\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n \u0008*\u0004\u0018\u00010\n0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n \u0008*\u0004\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\n \u0008*\u0004\u0018\u00010\u00110\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0014\u001a\n \u0008*\u0004\u0018\u00010\u00150\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0016\u001a\n \u0008*\u0004\u0018\u00010\n0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0017\u001a\n \u0008*\u0004\u0018\u00010\n0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0018\u001a\n \u0008*\u0004\u0018\u00010\u00190\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00066"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "kotlin.jvm.PlatformType",
        "amount",
        "Lcom/squareup/noho/NohoLabel;",
        "descriptionBottomMargin",
        "",
        "descriptionTopMargin",
        "descriptions",
        "Landroid/view/ViewGroup;",
        "detailImage",
        "Landroid/widget/ImageView;",
        "errorUpdatingSnackbar",
        "Lcom/google/android/material/snackbar/Snackbar;",
        "expenseTypeSection",
        "Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;",
        "footnote",
        "mainDescription",
        "res",
        "Landroid/content/res/Resources;",
        "showBackButton",
        "",
        "dismissError",
        "",
        "populateAmountText",
        "amountData",
        "Lcom/squareup/balance/activity/ui/common/Amount;",
        "populateDescriptions",
        "screen",
        "itemizedDescriptions",
        "",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;",
        "populateDetailImage",
        "merchantImage",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;",
        "populateExpenseType",
        "data",
        "populateFootnoteMessage",
        "footnoteMessage",
        "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;",
        "showBody",
        "showError",
        "showRendering",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "updateActionBar",
        "Binding",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final amount:Lcom/squareup/noho/NohoLabel;

.field private final descriptionBottomMargin:I

.field private final descriptionTopMargin:I

.field private final descriptions:Landroid/view/ViewGroup;

.field private final detailImage:Landroid/widget/ImageView;

.field private errorUpdatingSnackbar:Lcom/google/android/material/snackbar/Snackbar;

.field private final expenseTypeSection:Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;

.field private final footnote:Lcom/squareup/noho/NohoLabel;

.field private final mainDescription:Lcom/squareup/noho/NohoLabel;

.field private final res:Landroid/content/res/Resources;

.field private final showBackButton:Z

.field private final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    .line 50
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_details_image:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->detailImage:Landroid/widget/ImageView;

    .line 51
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_details_amount:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->amount:Lcom/squareup/noho/NohoLabel;

    .line 53
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_details_main_description:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->mainDescription:Lcom/squareup/noho/NohoLabel;

    .line 54
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_details_list:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->descriptions:Landroid/view/ViewGroup;

    .line 56
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_details_expense_type_section:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->expenseTypeSection:Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;

    .line 57
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 59
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    sget v0, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_details_footnote:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoLabel;

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->footnote:Lcom/squareup/noho/NohoLabel;

    .line 61
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->res:Landroid/content/res/Resources;

    .line 63
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->res:Landroid/content/res/Resources;

    sget v0, Lcom/squareup/balance/activity/impl/R$dimen;->balance_activity_details_description_spacing_top:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->descriptionTopMargin:I

    .line 65
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->res:Landroid/content/res/Resources;

    sget v0, Lcom/squareup/balance/activity/impl/R$dimen;->balance_activity_details_description_spacing_bottom:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->descriptionBottomMargin:I

    .line 66
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getScreenSize(Landroid/view/View;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isMasterDetail()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->showBackButton:Z

    .line 69
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$1;

    invoke-direct {v0, p0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$1;-><init>(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->onDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static final synthetic access$dismissError(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->dismissError()V

    return-void
.end method

.method private final dismissError()V
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->errorUpdatingSnackbar:Lcom/google/android/material/snackbar/Snackbar;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/material/snackbar/Snackbar;->dismiss()V

    :cond_0
    const/4 v0, 0x0

    .line 164
    check-cast v0, Lcom/google/android/material/snackbar/Snackbar;

    iput-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->errorUpdatingSnackbar:Lcom/google/android/material/snackbar/Snackbar;

    return-void
.end method

.method private final errorUpdatingSnackbar()Lcom/google/android/material/snackbar/Snackbar;
    .locals 3

    .line 172
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->errorUpdatingSnackbar:Lcom/google/android/material/snackbar/Snackbar;

    if-nez v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    sget v1, Lcom/squareup/balance/activity/impl/R$string;->error_updating_expense_category:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/material/snackbar/Snackbar;->make(Landroid/view/View;II)Lcom/google/android/material/snackbar/Snackbar;

    move-result-object v0

    .line 175
    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/balance/activity/impl/R$color;->snackbar_text_color:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/snackbar/Snackbar;->setTextColor(I)Lcom/google/android/material/snackbar/Snackbar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->errorUpdatingSnackbar:Lcom/google/android/material/snackbar/Snackbar;

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->errorUpdatingSnackbar:Lcom/google/android/material/snackbar/Snackbar;

    if-nez v0, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    return-object v0
.end method

.method private final populateAmountText(Lcom/squareup/balance/activity/ui/common/Amount;)V
    .locals 5

    .line 99
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->amount:Lcom/squareup/noho/NohoLabel;

    const-string v1, "amount"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/common/Amount;->getText()Lcom/squareup/resources/TextModel;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->amount:Lcom/squareup/noho/NohoLabel;

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/noho/NohoLabel;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "amount.context"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->amount:Lcom/squareup/noho/NohoLabel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/noho/NohoLabel;->getPaintFlags()I

    .line 104
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->amount:Lcom/squareup/noho/NohoLabel;

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->res:Landroid/content/res/Resources;

    sget v3, Lcom/squareup/balance/activity/impl/R$color;->amount_text_color_default:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoLabel;->setTextColor(I)V

    .line 107
    instance-of v0, p1, Lcom/squareup/balance/activity/ui/common/Amount$CrossedOutAmount;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->amount:Lcom/squareup/noho/NohoLabel;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->amount:Lcom/squareup/noho/NohoLabel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/noho/NohoLabel;->getPaintFlags()I

    move-result v0

    or-int/lit8 v0, v0, 0x10

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoLabel;->setPaintFlags(I)V

    goto :goto_0

    .line 108
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/activity/ui/common/Amount$PositiveAmount;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->amount:Lcom/squareup/noho/NohoLabel;

    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->res:Landroid/content/res/Resources;

    sget v1, Lcom/squareup/balance/activity/impl/R$color;->amount_text_color_positive:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoLabel;->setTextColor(I)V

    goto :goto_0

    .line 109
    :cond_1
    instance-of p1, p1, Lcom/squareup/balance/activity/ui/common/Amount$UndecoratedAmount;

    :goto_0
    return-void
.end method

.method private final populateDescriptions(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;Ljava/util/List;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;",
            ">;)V"
        }
    .end annotation

    move-object/from16 v6, p0

    move-object/from16 v0, p2

    .line 185
    iget-object v1, v6, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->descriptions:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_description_list_state:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 187
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_6

    .line 188
    :cond_0
    iget-object v1, v6, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->descriptions:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 189
    check-cast v0, Ljava/lang/Iterable;

    .line 261
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    move-object v7, v1

    check-cast v7, Ljava/util/Collection;

    .line 262
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 263
    move-object v2, v0

    check-cast v2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    .line 193
    instance-of v0, v2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description$SimpleLineDescription;

    if-eqz v0, :cond_1

    sget v0, Lcom/squareup/balance/activity/impl/R$style;->BalanceActivityDescription_Simple:I

    goto :goto_1

    .line 194
    :cond_1
    instance-of v0, v2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description$PillDecorationDescription;

    if-eqz v0, :cond_3

    sget v0, Lcom/squareup/balance/activity/impl/R$style;->BalanceActivityDescription_Pill:I

    .line 197
    :goto_1
    iget-object v1, v6, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->descriptions:Landroid/view/ViewGroup;

    const-string v3, "descriptions"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 199
    new-instance v5, Lcom/squareup/noho/NohoLabel;

    new-instance v1, Landroidx/appcompat/view/ContextThemeWrapper;

    invoke-direct {v1, v3, v0}, Landroidx/appcompat/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    move-object v10, v1

    check-cast v10, Landroid/content/Context;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0xe

    const/4 v15, 0x0

    move-object v9, v5

    invoke-direct/range {v9 .. v15}, Lcom/squareup/noho/NohoLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 200
    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;->getText()Lcom/squareup/resources/TextModel;

    move-result-object v0

    const-string v1, "context"

    invoke-static {v3, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 202
    invoke-virtual {v2}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;->getOnClick()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;

    move-result-object v1

    .line 203
    instance-of v0, v1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick$CommunityReward;

    if-eqz v0, :cond_2

    new-instance v9, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateDescriptions$$inlined$map$lambda$1;

    move-object v0, v9

    move-object/from16 v4, p0

    move-object v10, v5

    move-object/from16 v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateDescriptions$$inlined$map$lambda$1;-><init>(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;Landroid/content/Context;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;)V

    check-cast v9, Landroid/view/View$OnClickListener;

    invoke-virtual {v10, v9}, Lcom/squareup/noho/NohoLabel;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    :cond_2
    move-object v10, v5

    .line 206
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick$NoAction;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$OnClick$NoAction;

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 210
    :goto_2
    invoke-interface {v7, v10}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 194
    :cond_3
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v0

    .line 264
    :cond_4
    check-cast v7, Ljava/util/List;

    check-cast v7, Ljava/lang/Iterable;

    .line 265
    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/noho/NohoLabel;

    .line 213
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 215
    iget v3, v6, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->descriptionTopMargin:I

    iget v4, v6, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->descriptionBottomMargin:I

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v3, v5, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 217
    iget-object v3, v6, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->descriptions:Landroid/view/ViewGroup;

    check-cast v1, Landroid/view/View;

    check-cast v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v3, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    .line 220
    :cond_5
    iget-object v0, v6, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->descriptions:Landroid/view/ViewGroup;

    sget v1, Lcom/squareup/balance/activity/impl/R$id;->balance_activity_description_list_state:I

    iget-object v2, v6, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->descriptions:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    :cond_6
    return-void
.end method

.method private final populateDetailImage(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;)V
    .locals 1

    .line 117
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage$NotAvailable;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage$NotAvailable;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->detailImage:Landroid/widget/ImageView;

    const-string v0, "detailImage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method private final populateExpenseType(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;)V
    .locals 3

    .line 122
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->expenseTypeSection:Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;

    .line 124
    invoke-virtual {v0}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->clearOnUpdateListener()V

    .line 127
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->isUpdatingCategories()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->setEnabled(Z)V

    .line 129
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->getExpenseType()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType;

    move-result-object v1

    .line 130
    sget-object v2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType$None;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType$None;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    goto :goto_0

    .line 131
    :cond_0
    sget-object v2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType$PersonalSelected;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType$PersonalSelected;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 132
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 133
    sget-object v1, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;->PERSONAL:Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->isUpdatingCategories()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->updateSelection(Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;Z)V

    goto :goto_0

    .line 135
    :cond_1
    sget-object v2, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType$BusinessSelected;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$ExpenseType$BusinessSelected;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 136
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 137
    sget-object v1, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;->BUSINESS:Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->isUpdatingCategories()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->updateSelection(Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;Z)V

    .line 141
    :cond_2
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->getDisplayExpenseTypeError()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 142
    invoke-direct {p0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->showError()V

    .line 144
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->getOnErrorDisplayed()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    invoke-interface {v1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 148
    :cond_3
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/util/Views;->isVisible(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 149
    new-instance v1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateExpenseType$$inlined$run$lambda$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$populateExpenseType$$inlined$run$lambda$1;-><init>(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->setOnUpdateListener(Lkotlin/jvm/functions/Function1;)V

    :cond_4
    return-void
.end method

.method private final populateFootnoteMessage(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;)V
    .locals 3

    .line 228
    sget-object v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage$None;->INSTANCE:Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage$None;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "footnote"

    if-eqz v0, :cond_0

    .line 229
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->footnote:Lcom/squareup/noho/NohoLabel;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    goto :goto_0

    .line 231
    :cond_0
    instance-of v0, p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage$SimpleMessage;

    if-eqz v0, :cond_1

    .line 232
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->footnote:Lcom/squareup/noho/NohoLabel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 233
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->footnote:Lcom/squareup/noho/NohoLabel;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage$SimpleMessage;

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage$SimpleMessage;->getMessage()Lcom/squareup/resources/TextModel;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "view.context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v1}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method

.method private final showBody(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;)V
    .locals 4

    .line 87
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->mainDescription:Lcom/squareup/noho/NohoLabel;

    const-string v1, "mainDescription"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->getMainDescription()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description;

    move-result-object v1

    if-eqz v1, :cond_0

    check-cast v1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description$SimpleLineDescription;

    invoke-virtual {v1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$Description$SimpleLineDescription;->getText()Lcom/squareup/resources/TextModel;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string/jumbo v3, "view.context"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/squareup/resources/TextModel;->evaluate(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 89
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->getAmount()Lcom/squareup/balance/activity/ui/common/Amount;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->populateAmountText(Lcom/squareup/balance/activity/ui/common/Amount;)V

    .line 90
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->getMerchantImage()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->populateDetailImage(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$MerchantImage;)V

    .line 91
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->getItemizedDescriptions()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->populateDescriptions(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;Ljava/util/List;)V

    .line 92
    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->populateExpenseType(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;)V

    .line 93
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->getFootnoteMessage()Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->populateFootnoteMessage(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen$FootnoteMessage;)V

    return-void

    .line 87
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.balance.activity.ui.detail.success.BalanceActivityDetailsSuccessScreen.Description.SimpleLineDescription"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final showError()V
    .locals 1

    .line 168
    invoke-direct {p0}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->errorUpdatingSnackbar()Lcom/google/android/material/snackbar/Snackbar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/material/snackbar/Snackbar;->show()V

    return-void
.end method

.method private final updateActionBar(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;)V
    .locals 4

    .line 248
    iget-object v0, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 239
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 240
    invoke-virtual {p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;->getTitle()Lcom/squareup/util/ViewString;

    move-result-object v2

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 242
    iget-boolean v2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->showBackButton:Z

    if-eqz v2, :cond_0

    .line 243
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$updateActionBar$$inlined$apply$lambda$1;

    invoke-direct {v3, p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$updateActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 245
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 248
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public showRendering(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->showBody(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;)V

    .line 81
    iget-object p2, p0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner$showRendering$1;-><init>(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 82
    invoke-direct {p0, p1}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->updateActionBar(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessLayoutRunner;->showRendering(Lcom/squareup/balance/activity/ui/detail/success/BalanceActivityDetailsSuccessScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
