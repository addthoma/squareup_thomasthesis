.class final Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$setOnUpdateListener$1;
.super Ljava/lang/Object;
.source "CheckableExpenseType.kt"

# interfaces
.implements Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->setOnUpdateListener(Lkotlin/jvm/functions/Function1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "kotlin.jvm.PlatformType",
        "checkedId",
        "",
        "previousId",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $onSelected:Lkotlin/jvm/functions/Function1;

.field final synthetic this$0:Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$setOnUpdateListener$1;->this$0:Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$setOnUpdateListener$1;->$onSelected:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Lcom/squareup/noho/NohoCheckableGroup;II)V
    .locals 0

    if-ne p2, p3, :cond_0

    return-void

    .line 87
    :cond_0
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$setOnUpdateListener$1;->this$0:Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;

    invoke-static {p1}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->access$getExpenseTypePersonal$p(Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;)Lcom/squareup/noho/NohoCheckableRow;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/noho/NohoCheckableRow;->getId()I

    move-result p1

    if-ne p2, p1, :cond_1

    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$setOnUpdateListener$1;->$onSelected:Lkotlin/jvm/functions/Function1;

    sget-object p2, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;->PERSONAL:Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 88
    :cond_1
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$setOnUpdateListener$1;->this$0:Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;

    invoke-static {p1}, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;->access$getExpenseTypeBusiness$p(Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType;)Lcom/squareup/noho/NohoCheckableRow;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/noho/NohoCheckableRow;->getId()I

    move-result p1

    if-ne p2, p1, :cond_2

    iget-object p1, p0, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$setOnUpdateListener$1;->$onSelected:Lkotlin/jvm/functions/Function1;

    sget-object p2, Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;->BUSINESS:Lcom/squareup/balance/activity/ui/detail/success/CheckableExpenseType$SelectionStatus;

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    :goto_0
    return-void
.end method
