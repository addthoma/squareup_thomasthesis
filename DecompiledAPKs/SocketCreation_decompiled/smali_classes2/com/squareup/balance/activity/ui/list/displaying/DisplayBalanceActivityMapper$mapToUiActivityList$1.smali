.class final Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DisplayBalanceActivityMapper.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;->mapToUiActivityList(Lcom/squareup/balance/activity/data/BalanceActivityType;Lcom/squareup/balance/activity/data/BalanceActivity;)Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Ljava/lang/Boolean;",
        "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDisplayBalanceActivityMapper.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DisplayBalanceActivityMapper.kt\ncom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,141:1\n1360#2:142\n1429#2,3:143\n*E\n*S KotlinDebug\n*F\n+ 1 DisplayBalanceActivityMapper.kt\ncom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1\n*L\n52#1:142\n52#1,3:143\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "mapUnifiedActivity",
        "",
        "isPending",
        "",
        "unifiedActivityResult",
        "Lcom/squareup/balance/activity/data/UnifiedActivityResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $tabType:Lcom/squareup/balance/activity/data/BalanceActivityType;

.field final synthetic $uiActivity:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;


# direct methods
.method constructor <init>(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;Ljava/util/List;Lcom/squareup/balance/activity/data/BalanceActivityType;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;

    iput-object p2, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->$uiActivity:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->$tabType:Lcom/squareup/balance/activity/data/BalanceActivityType;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    check-cast p2, Lcom/squareup/balance/activity/data/UnifiedActivityResult;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->invoke(ZLcom/squareup/balance/activity/data/UnifiedActivityResult;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ZLcom/squareup/balance/activity/data/UnifiedActivityResult;)V
    .locals 6

    const-string v0, "unifiedActivityResult"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    instance-of v0, p2, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    if-eqz v0, :cond_4

    .line 49
    move-object v0, p2

    check-cast v0, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;

    invoke-virtual {v0}, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;->getActivities()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_5

    .line 50
    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->$uiActivity:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$Title;

    if-eqz p1, :cond_0

    sget-object v3, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$Title$TitleType;->PENDING:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$Title$TitleType;

    goto :goto_0

    :cond_0
    sget-object v3, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$Title$TitleType;->COMPLETED:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$Title$TitleType;

    :goto_0
    invoke-direct {v2, v3}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$Title;-><init>(Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$Title$TitleType;)V

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 51
    iget-object v1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->$uiActivity:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {v0}, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData;->getActivities()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 142
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v0, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 143
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 144
    check-cast v3, Lcom/squareup/protos/bizbank/UnifiedActivity;

    .line 53
    iget-object v4, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->this$0:Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;

    .line 55
    iget-object v5, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->$tabType:Lcom/squareup/balance/activity/data/BalanceActivityType;

    invoke-static {v4, v5}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;->access$shouldShowRunningBalance(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;Lcom/squareup/balance/activity/data/BalanceActivityType;)Z

    move-result v5

    .line 53
    invoke-static {v4, p1, v5, v3}, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;->access$mapToUiBalanceActivity(Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper;ZZLcom/squareup/protos/bizbank/UnifiedActivity;)Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity;

    move-result-object v3

    .line 57
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 145
    :cond_1
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 51
    invoke-static {v1, v2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;Ljava/lang/Iterable;)Z

    .line 60
    instance-of v0, p2, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$Success;

    if-eqz v0, :cond_2

    invoke-static {p2}, Lcom/squareup/balance/activity/data/UnifiedActivityResultKt;->hasBatchToken(Lcom/squareup/balance/activity/data/UnifiedActivityResult;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 61
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->$uiActivity:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    sget-object p2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$LoadMore;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$LoadMore;

    invoke-interface {p1, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 62
    :cond_2
    instance-of p2, p2, Lcom/squareup/balance/activity/data/UnifiedActivityResult$HasData$ErrorFetchingMore;

    if-eqz p2, :cond_3

    .line 63
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->$uiActivity:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    sget-object p2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$ErrorRow;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$ErrorRow;

    invoke-interface {p1, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    if-nez p1, :cond_5

    .line 64
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->$tabType:Lcom/squareup/balance/activity/data/BalanceActivityType;

    sget-object p2, Lcom/squareup/balance/activity/data/BalanceActivityType;->ALL:Lcom/squareup/balance/activity/data/BalanceActivityType;

    if-ne p1, p2, :cond_5

    if-eqz v0, :cond_5

    .line 67
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->$uiActivity:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    new-instance p2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;

    sget v0, Lcom/squareup/balance/activity/impl/R$string;->balance_activity_all_tab_bottom_message:I

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {p2, v0, v2, v1, v2}, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow;-><init>(ILcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$MessageRow$ClickAction;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {p1, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 71
    :cond_4
    sget-object p1, Lcom/squareup/balance/activity/data/UnifiedActivityResult$Error;->INSTANCE:Lcom/squareup/balance/activity/data/UnifiedActivityResult$Error;

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_5

    .line 72
    iget-object p1, p0, Lcom/squareup/balance/activity/ui/list/displaying/DisplayBalanceActivityMapper$mapToUiActivityList$1;->$uiActivity:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    sget-object p2, Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$ErrorRow;->INSTANCE:Lcom/squareup/balance/activity/ui/list/displaying/UiBalanceActivity$ErrorRow;

    invoke-interface {p1, p2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_5
    :goto_2
    return-void
.end method
