.class public abstract Lcom/squareup/balance/activity/ui/list/BalanceActivityState;
.super Ljava/lang/Object;
.source "BalanceActivityState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceActivity;,
        Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceDetails;,
        Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingTransferReports;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u0007\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0003\n\u000b\u000c\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityState;",
        "Landroid/os/Parcelable;",
        "()V",
        "type",
        "Lcom/squareup/balance/activity/data/BalanceActivityType;",
        "getType",
        "()Lcom/squareup/balance/activity/data/BalanceActivityType;",
        "DisplayingBalanceActivity",
        "DisplayingBalanceDetails",
        "DisplayingTransferReports",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceActivity;",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingBalanceDetails;",
        "Lcom/squareup/balance/activity/ui/list/BalanceActivityState$DisplayingTransferReports;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 9
    invoke-direct {p0}, Lcom/squareup/balance/activity/ui/list/BalanceActivityState;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getType()Lcom/squareup/balance/activity/data/BalanceActivityType;
.end method
