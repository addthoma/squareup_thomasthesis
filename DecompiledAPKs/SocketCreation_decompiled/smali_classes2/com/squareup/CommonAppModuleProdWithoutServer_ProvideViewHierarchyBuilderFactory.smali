.class public final Lcom/squareup/CommonAppModuleProdWithoutServer_ProvideViewHierarchyBuilderFactory;
.super Ljava/lang/Object;
.source "CommonAppModuleProdWithoutServer_ProvideViewHierarchyBuilderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/CommonAppModuleProdWithoutServer_ProvideViewHierarchyBuilderFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/radiography/FocusedActivityScanner;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/CommonAppModuleProdWithoutServer_ProvideViewHierarchyBuilderFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvideViewHierarchyBuilderFactory$InstanceHolder;->access$000()Lcom/squareup/CommonAppModuleProdWithoutServer_ProvideViewHierarchyBuilderFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideViewHierarchyBuilder()Lcom/squareup/radiography/FocusedActivityScanner;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/CommonAppModuleProdWithoutServer;->provideViewHierarchyBuilder()Lcom/squareup/radiography/FocusedActivityScanner;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/radiography/FocusedActivityScanner;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/radiography/FocusedActivityScanner;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvideViewHierarchyBuilderFactory;->provideViewHierarchyBuilder()Lcom/squareup/radiography/FocusedActivityScanner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/CommonAppModuleProdWithoutServer_ProvideViewHierarchyBuilderFactory;->get()Lcom/squareup/radiography/FocusedActivityScanner;

    move-result-object v0

    return-object v0
.end method
