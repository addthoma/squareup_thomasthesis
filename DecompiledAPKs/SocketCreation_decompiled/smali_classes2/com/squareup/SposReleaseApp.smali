.class public Lcom/squareup/SposReleaseApp;
.super Lcom/squareup/CommonPosApp;
.source "SposReleaseApp.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/squareup/CommonPosApp;-><init>()V

    return-void
.end method


# virtual methods
.method protected createAppDelegate()Lcom/squareup/RegisterAppDelegate;
    .locals 1

    .line 5
    new-instance v0, Lcom/squareup/SposReleaseAppDelegate;

    invoke-direct {v0, p0}, Lcom/squareup/SposReleaseAppDelegate;-><init>(Landroid/app/Application;)V

    return-object v0
.end method

.method protected bridge synthetic createAppDelegate()Lcom/squareup/app/HasOnCreate;
    .locals 1

    .line 3
    invoke-virtual {p0}, Lcom/squareup/SposReleaseApp;->createAppDelegate()Lcom/squareup/RegisterAppDelegate;

    move-result-object v0

    return-object v0
.end method
