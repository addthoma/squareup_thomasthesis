.class public final Lcom/squareup/backgroundjob/BackgroundJobs;
.super Ljava/lang/Object;
.source "BackgroundJobs.java"


# static fields
.field static DEFAULT_WAKE_LOCK_TIMEOUT_MS:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 21
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/squareup/backgroundjob/BackgroundJobs;->DEFAULT_WAKE_LOCK_TIMEOUT_MS:J

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static asBackgroundJobs(Ljava/util/Collection;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/evernote/android/job/Job;",
            ">;)",
            "Ljava/util/Set<",
            "Lcom/squareup/backgroundjob/BackgroundJob;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_0

    .line 71
    new-instance p0, Ljava/util/LinkedHashSet;

    invoke-direct {p0}, Ljava/util/LinkedHashSet;-><init>()V

    return-object p0

    .line 74
    :cond_0
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(I)V

    .line 75
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/evernote/android/job/Job;

    .line 76
    check-cast v1, Lcom/squareup/backgroundjob/BackgroundJob;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static executeWithWakeLock(Landroid/content/Context;Ljava/util/concurrent/Executor;JLjava/lang/String;Lrx/functions/Action0;)V
    .locals 2

    const-string v0, "power"

    .line 50
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/os/PowerManager;

    const/4 v0, 0x1

    .line 51
    invoke-virtual {p0, v0, p4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object p0

    const-wide/16 v0, 0x0

    cmp-long p4, p2, v0

    if-lez p4, :cond_0

    .line 53
    invoke-virtual {p0, p2, p3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    goto :goto_0

    .line 55
    :cond_0
    invoke-virtual {p0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 58
    :goto_0
    new-instance p2, Lcom/squareup/backgroundjob/-$$Lambda$BackgroundJobs$09AKDTv_f2cqyqYAV2RItHV84z4;

    invoke-direct {p2, p5, p0}, Lcom/squareup/backgroundjob/-$$Lambda$BackgroundJobs$09AKDTv_f2cqyqYAV2RItHV84z4;-><init>(Lrx/functions/Action0;Landroid/os/PowerManager$WakeLock;)V

    invoke-interface {p1, p2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static executeWithWakeLock(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/lang/String;Lrx/functions/Action0;)V
    .locals 6

    .line 37
    sget-wide v2, Lcom/squareup/backgroundjob/BackgroundJobs;->DEFAULT_WAKE_LOCK_TIMEOUT_MS:J

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/squareup/backgroundjob/BackgroundJobs;->executeWithWakeLock(Landroid/content/Context;Ljava/util/concurrent/Executor;JLjava/lang/String;Lrx/functions/Action0;)V

    return-void
.end method

.method static synthetic lambda$executeWithWakeLock$0(Lrx/functions/Action0;Landroid/os/PowerManager$WakeLock;)V
    .locals 1

    .line 60
    :try_start_0
    invoke-interface {p0}, Lrx/functions/Action0;->call()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    invoke-virtual {p1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 63
    invoke-virtual {p1}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    return-void

    :catchall_0
    move-exception p0

    .line 62
    invoke-virtual {p1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    invoke-virtual {p1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 65
    :cond_1
    throw p0
.end method
