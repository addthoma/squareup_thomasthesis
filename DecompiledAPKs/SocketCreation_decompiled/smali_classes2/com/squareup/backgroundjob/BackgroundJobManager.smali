.class public interface abstract Lcom/squareup/backgroundjob/BackgroundJobManager;
.super Ljava/lang/Object;
.source "BackgroundJobManager.java"


# virtual methods
.method public abstract addJobCreator(Lcom/squareup/backgroundjob/BackgroundJobCreator;)V
.end method

.method public abstract cancel(I)Z
.end method

.method public abstract cancelAll()I
.end method

.method public abstract cancelAllForTag(Ljava/lang/String;)I
.end method

.method public abstract getAllJobRequests()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/evernote/android/job/JobRequest;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAllJobRequestsForTag(Ljava/lang/String;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set<",
            "Lcom/evernote/android/job/JobRequest;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAllJobs()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/backgroundjob/BackgroundJob;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getAllJobsForTag(Ljava/lang/String;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/backgroundjob/BackgroundJob;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getJob(I)Lcom/evernote/android/job/Job;
.end method

.method public abstract getJobRequest(I)Lcom/evernote/android/job/JobRequest;
.end method

.method public abstract removeJobCreator(Lcom/squareup/backgroundjob/BackgroundJobCreator;)V
.end method

.method public abstract schedule(Lcom/evernote/android/job/JobRequest;)V
.end method
