.class public Lcom/squareup/backgroundjob/RealBackgroundJobManager;
.super Ljava/lang/Object;
.source "RealBackgroundJobManager.java"

# interfaces
.implements Lcom/squareup/backgroundjob/BackgroundJobManager;


# instance fields
.field private final delegate:Lcom/evernote/android/job/JobManager;

.field private final jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;


# direct methods
.method public constructor <init>(Landroid/app/Application;Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 30
    invoke-static {v0}, Lcom/evernote/android/job/JobConfig;->setLogcatEnabled(Z)V

    const/4 v1, 0x1

    .line 37
    invoke-static {v1}, Lcom/evernote/android/job/JobConfig;->setForceAllowApi14(Z)V

    .line 43
    sget-object v1, Lcom/evernote/android/job/JobApi;->GCM:Lcom/evernote/android/job/JobApi;

    invoke-static {v1, v0}, Lcom/evernote/android/job/JobConfig;->setApiEnabled(Lcom/evernote/android/job/JobApi;Z)V

    .line 49
    sget-object v1, Lcom/evernote/android/job/JobApi;->WORK_MANAGER:Lcom/evernote/android/job/JobApi;

    invoke-static {v1, v0}, Lcom/evernote/android/job/JobConfig;->setApiEnabled(Lcom/evernote/android/job/JobApi;Z)V

    .line 52
    invoke-static {p1}, Lcom/evernote/android/job/JobManager;->create(Landroid/content/Context;)Lcom/evernote/android/job/JobManager;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->delegate:Lcom/evernote/android/job/JobManager;

    .line 53
    iput-object p2, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    return-void
.end method


# virtual methods
.method public addJobCreator(Lcom/squareup/backgroundjob/BackgroundJobCreator;)V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->delegate:Lcom/evernote/android/job/JobManager;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/JobManager;->addJobCreator(Lcom/evernote/android/job/JobCreator;)V

    return-void
.end method

.method public cancel(I)Z
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->delegate:Lcom/evernote/android/job/JobManager;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/JobManager;->getJobRequest(I)Lcom/evernote/android/job/JobRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v1, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    invoke-interface {v1, v0}, Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;->hideNotificationFor(Lcom/evernote/android/job/JobRequest;)V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->delegate:Lcom/evernote/android/job/JobManager;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/JobManager;->cancel(I)Z

    move-result p1

    return p1
.end method

.method public cancelAll()I
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    invoke-interface {v0}, Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;->hideAllNotifications()V

    .line 101
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->delegate:Lcom/evernote/android/job/JobManager;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobManager;->cancelAll()I

    move-result v0

    return v0
.end method

.method public cancelAllForTag(Ljava/lang/String;)I
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    invoke-interface {v0, p1}, Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;->hideAllNotificationsForTag(Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->delegate:Lcom/evernote/android/job/JobManager;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/JobManager;->cancelAllForTag(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getAllJobRequests()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/evernote/android/job/JobRequest;",
            ">;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->delegate:Lcom/evernote/android/job/JobManager;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobManager;->getAllJobRequests()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getAllJobRequestsForTag(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set<",
            "Lcom/evernote/android/job/JobRequest;",
            ">;"
        }
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->delegate:Lcom/evernote/android/job/JobManager;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/JobManager;->getAllJobRequestsForTag(Ljava/lang/String;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method public getAllJobs()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/backgroundjob/BackgroundJob;",
            ">;"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->delegate:Lcom/evernote/android/job/JobManager;

    invoke-virtual {v0}, Lcom/evernote/android/job/JobManager;->getAllJobs()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/backgroundjob/BackgroundJobs;->asBackgroundJobs(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getAllJobsForTag(Ljava/lang/String;)Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set<",
            "Lcom/squareup/backgroundjob/BackgroundJob;",
            ">;"
        }
    .end annotation

    .line 82
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->delegate:Lcom/evernote/android/job/JobManager;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/JobManager;->getAllJobsForTag(Ljava/lang/String;)Ljava/util/Set;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/backgroundjob/BackgroundJobs;->asBackgroundJobs(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method public getJob(I)Lcom/evernote/android/job/Job;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->delegate:Lcom/evernote/android/job/JobManager;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/JobManager;->getJob(I)Lcom/evernote/android/job/Job;

    move-result-object p1

    return-object p1
.end method

.method public getJobRequest(I)Lcom/evernote/android/job/JobRequest;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->delegate:Lcom/evernote/android/job/JobManager;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/JobManager;->getJobRequest(I)Lcom/evernote/android/job/JobRequest;

    move-result-object p1

    return-object p1
.end method

.method public removeJobCreator(Lcom/squareup/backgroundjob/BackgroundJobCreator;)V
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->delegate:Lcom/evernote/android/job/JobManager;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/JobManager;->removeJobCreator(Lcom/evernote/android/job/JobCreator;)V

    return-void
.end method

.method public schedule(Lcom/evernote/android/job/JobRequest;)V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->jobNotificationManager:Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    invoke-interface {v0, p1}, Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;->showNotificationFor(Lcom/evernote/android/job/JobRequest;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/backgroundjob/RealBackgroundJobManager;->delegate:Lcom/evernote/android/job/JobManager;

    invoke-virtual {v0, p1}, Lcom/evernote/android/job/JobManager;->schedule(Lcom/evernote/android/job/JobRequest;)V

    return-void
.end method
