.class public abstract Lcom/squareup/account/ImpersonationHelper;
.super Lmortar/Presenter;
.source "ImpersonationHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/account/ImpersonationHelper$NoImpersonationHelper;,
        Lcom/squareup/account/ImpersonationHelper$View;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/account/ImpersonationHelper$View;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/account/ImpersonationHelper$View;)Lmortar/bundler/BundleService;
    .locals 0

    .line 22
    invoke-interface {p1}, Lcom/squareup/account/ImpersonationHelper$View;->getBundleService()Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/account/ImpersonationHelper$View;

    invoke-virtual {p0, p1}, Lcom/squareup/account/ImpersonationHelper;->extractBundleService(Lcom/squareup/account/ImpersonationHelper$View;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public abstract onActivityResult(IILandroid/content/Intent;)V
.end method
