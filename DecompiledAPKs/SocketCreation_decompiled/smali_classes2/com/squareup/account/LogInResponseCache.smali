.class public interface abstract Lcom/squareup/account/LogInResponseCache;
.super Ljava/lang/Object;
.source "LogInResponseCache.java"


# virtual methods
.method public abstract clearCache()V
.end method

.method public abstract getCanonicalStatus()Lcom/squareup/server/account/protos/AccountStatusResponse;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract getSessionToken()Ljava/lang/String;
.end method

.method public abstract init(Lcom/squareup/accountstatus/QuietServerPreferences;)V
.end method

.method public abstract onLoggedIn()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract replaceCache(Ljava/lang/String;)V
.end method

.method public abstract replaceCache(Ljava/lang/String;Lcom/squareup/server/account/protos/AccountStatusResponse;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract setPreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/Preferences;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method
