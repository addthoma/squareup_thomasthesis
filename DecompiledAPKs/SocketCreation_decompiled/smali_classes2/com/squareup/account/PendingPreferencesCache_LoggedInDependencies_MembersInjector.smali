.class public final Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;
.super Ljava/lang/Object;
.source "PendingPreferencesCache_LoggedInDependencies_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;",
        ">;"
    }
.end annotation


# instance fields
.field private final preferencesInProgressProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;>;"
        }
    .end annotation
.end field

.field private final preferencesOnDeckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;>;"
        }
    .end annotation
.end field

.field private final taskQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final userIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;->preferencesInProgressProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;->preferencesOnDeckProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;->taskQueueProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;->userIdProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;",
            ">;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPreferencesInProgress(Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;)V"
        }
    .end annotation

    .line 61
    iput-object p1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->preferencesInProgress:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public static injectPreferencesOnDeck(Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;)V"
        }
    .end annotation

    .line 68
    iput-object p1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->preferencesOnDeck:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public static injectTaskQueue(Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;Lcom/squareup/queue/retrofit/RetrofitQueue;)V
    .locals 0

    .line 75
    iput-object p1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-void
.end method

.method public static injectUserId(Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;Ljava/lang/String;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;->userId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;->preferencesInProgressProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;->injectPreferencesInProgress(Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;Lcom/squareup/settings/LocalSetting;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;->preferencesOnDeckProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;->injectPreferencesOnDeck(Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;Lcom/squareup/settings/LocalSetting;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;->taskQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-static {p1, v0}, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;->injectTaskQueue(Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;Lcom/squareup/queue/retrofit/RetrofitQueue;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;->userIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;->injectUserId(Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;

    invoke-virtual {p0, p1}, Lcom/squareup/account/PendingPreferencesCache_LoggedInDependencies_MembersInjector;->injectMembers(Lcom/squareup/account/PendingPreferencesCache$LoggedInDependencies;)V

    return-void
.end method
