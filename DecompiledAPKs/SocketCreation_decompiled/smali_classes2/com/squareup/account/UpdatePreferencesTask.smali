.class public Lcom/squareup/account/UpdatePreferencesTask;
.super Ljava/lang/Object;
.source "UpdatePreferencesTask.java"

# interfaces
.implements Lcom/squareup/queue/LoggedInTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/account/UpdatePreferencesTask$Component;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/LoggedInTask<",
        "Lcom/squareup/account/UpdatePreferencesTask$Component;",
        ">;"
    }
.end annotation


# instance fields
.field public transient pendingPreferencesCache:Lcom/squareup/account/PendingPreferencesCache;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public transient preferencesInProgress:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public transient preferencesOnDeck:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/server/account/protos/PreferencesRequest;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public transient quietServerPreferences:Lcom/squareup/accountstatus/QuietServerPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static asSimpleResponse(Lcom/squareup/server/account/protos/Preferences;)Lcom/squareup/server/SimpleResponse;
    .locals 3

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/Preferences;->title:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/server/account/protos/Preferences;->error_title:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/Preferences;->title:Ljava/lang/String;

    .line 133
    :goto_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->message:Ljava/lang/String;

    .line 134
    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->error_message:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->message:Ljava/lang/String;

    .line 135
    :goto_1
    new-instance v2, Lcom/squareup/server/SimpleResponse;

    iget-object p0, p0, Lcom/squareup/server/account/protos/Preferences;->success:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    invoke-direct {v2, p0, v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method private getPreferencesFromResponse(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/server/account/protos/Preferences;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/Preferences;",
            ">;)",
            "Lcom/squareup/server/account/protos/Preferences;"
        }
    .end annotation

    .line 79
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 80
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/account/protos/Preferences;

    return-object p1

    .line 83
    :cond_0
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    .line 84
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 85
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    if-eqz v0, :cond_1

    .line 86
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Rejected;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/account/protos/Preferences;

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private invokeErrorCallback(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/server/SquareCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/server/account/protos/Preferences;",
            ">;",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 93
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_3

    .line 94
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 96
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/squareup/account/UpdatePreferencesTask;->preferencesInProgress:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->remove()V

    .line 100
    iget-object v0, p0, Lcom/squareup/account/UpdatePreferencesTask;->pendingPreferencesCache:Lcom/squareup/account/PendingPreferencesCache;

    invoke-virtual {v0}, Lcom/squareup/account/PendingPreferencesCache;->pendingPreferencesChanged()V

    .line 101
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    .line 103
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/Preferences;

    invoke-static {v0}, Lcom/squareup/account/UpdatePreferencesTask;->asSimpleResponse(Lcom/squareup/server/account/protos/Preferences;)Lcom/squareup/server/SimpleResponse;

    move-result-object v0

    .line 104
    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getStatusCode()I

    move-result p1

    .line 103
    invoke-virtual {p2, v0, p1}, Lcom/squareup/server/SquareCallback;->clientError(Ljava/lang/Object;I)V

    return-void

    .line 107
    :cond_0
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_1

    .line 108
    invoke-virtual {p2}, Lcom/squareup/server/SquareCallback;->sessionExpired()V

    return-void

    .line 111
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_2

    .line 112
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->getStatusCode()I

    move-result p1

    invoke-virtual {p2, p1}, Lcom/squareup/server/SquareCallback;->serverError(I)V

    return-void

    .line 115
    :cond_2
    instance-of p1, p1, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    if-eqz p1, :cond_3

    .line 116
    invoke-virtual {p2}, Lcom/squareup/server/SquareCallback;->networkError()V

    return-void

    .line 121
    :cond_3
    new-instance p1, Lkotlin/NotImplementedError;

    const-string p2, "Shouldn\'t happen"

    invoke-direct {p1, p2}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 47
    iget-object v0, p0, Lcom/squareup/account/UpdatePreferencesTask;->preferencesInProgress:Lcom/squareup/settings/LocalSetting;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/PreferencesRequest;

    .line 48
    iget-object v2, p0, Lcom/squareup/account/UpdatePreferencesTask;->preferencesOnDeck:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v2, v1}, Lcom/squareup/settings/LocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/account/protos/PreferencesRequest;

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    .line 52
    :cond_0
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/PreferencesRequest;->overlay(Lcom/squareup/server/account/protos/PreferencesRequest;)Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object v0

    .line 53
    :goto_0
    iget-object v1, p0, Lcom/squareup/account/UpdatePreferencesTask;->preferencesInProgress:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 54
    iget-object v1, p0, Lcom/squareup/account/UpdatePreferencesTask;->preferencesOnDeck:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v1}, Lcom/squareup/settings/LocalSetting;->remove()V

    :cond_1
    if-nez v0, :cond_2

    .line 59
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    return-void

    .line 64
    :cond_2
    iget-object v1, p0, Lcom/squareup/account/UpdatePreferencesTask;->quietServerPreferences:Lcom/squareup/accountstatus/QuietServerPreferences;

    invoke-interface {v1, v0}, Lcom/squareup/accountstatus/QuietServerPreferences;->setPreferencesQuietly(Lcom/squareup/server/account/protos/PreferencesRequest;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/account/-$$Lambda$UpdatePreferencesTask$BbMMFLwGsRo9n-4dCP7-cZ8CZLM;

    invoke-direct {v1, p0, p1}, Lcom/squareup/account/-$$Lambda$UpdatePreferencesTask$BbMMFLwGsRo9n-4dCP7-cZ8CZLM;-><init>(Lcom/squareup/account/UpdatePreferencesTask;Lcom/squareup/server/SquareCallback;)V

    .line 65
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/account/UpdatePreferencesTask;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public inject(Lcom/squareup/account/UpdatePreferencesTask$Component;)V
    .locals 0

    .line 125
    invoke-interface {p1, p0}, Lcom/squareup/account/UpdatePreferencesTask$Component;->inject(Lcom/squareup/account/UpdatePreferencesTask;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/account/UpdatePreferencesTask$Component;

    invoke-virtual {p0, p1}, Lcom/squareup/account/UpdatePreferencesTask;->inject(Lcom/squareup/account/UpdatePreferencesTask$Component;)V

    return-void
.end method

.method public synthetic lambda$execute$0$UpdatePreferencesTask(Lcom/squareup/server/SquareCallback;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 66
    invoke-direct {p0, p2}, Lcom/squareup/account/UpdatePreferencesTask;->getPreferencesFromResponse(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    if-nez v0, :cond_0

    .line 68
    invoke-direct {p0, p2, p1}, Lcom/squareup/account/UpdatePreferencesTask;->invokeErrorCallback(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/server/SquareCallback;)V

    return-void

    .line 72
    :cond_0
    iget-object p2, p0, Lcom/squareup/account/UpdatePreferencesTask;->preferencesInProgress:Lcom/squareup/settings/LocalSetting;

    invoke-interface {p2}, Lcom/squareup/settings/LocalSetting;->remove()V

    .line 73
    iget-object p2, p0, Lcom/squareup/account/UpdatePreferencesTask;->pendingPreferencesCache:Lcom/squareup/account/PendingPreferencesCache;

    invoke-virtual {p2}, Lcom/squareup/account/PendingPreferencesCache;->pendingPreferencesChanged()V

    .line 74
    invoke-static {v0}, Lcom/squareup/account/UpdatePreferencesTask;->asSimpleResponse(Lcom/squareup/server/account/protos/Preferences;)Lcom/squareup/server/SimpleResponse;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method
