.class final Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl$SCVPCS_ComponentImpl;
.super Ljava/lang/Object;
.source "DaggerSposReleaseAppComponent.java"

# interfaces
.implements Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Component;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SCVPCS_ComponentImpl"
.end annotation


# instance fields
.field private errorsBarPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$6:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl;


# direct methods
.method private constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl;)V
    .locals 0

    .line 39090
    iput-object p1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl$SCVPCS_ComponentImpl;->this$6:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39092
    invoke-direct {p0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl$SCVPCS_ComponentImpl;->initialize()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl;Lcom/squareup/DaggerSposReleaseAppComponent$1;)V
    .locals 0

    .line 39085
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl$SCVPCS_ComponentImpl;-><init>(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl;)V

    return-void
.end method

.method private initialize()V
    .locals 3

    .line 39097
    invoke-static {}, Lcom/squareup/ui/ErrorsBarPresenter_Factory;->create()Lcom/squareup/ui/ErrorsBarPresenter_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl$SCVPCS_ComponentImpl;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    .line 39098
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl$SCVPCS_ComponentImpl;->this$6:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl;

    invoke-static {v0}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl;->access$139500(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl;)Ljavax/inject/Provider;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl$SCVPCS_ComponentImpl;->this$6:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl;->this$5:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    iget-object v1, v1, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->this$0:Lcom/squareup/DaggerSposReleaseAppComponent;

    invoke-static {v1}, Lcom/squareup/DaggerSposReleaseAppComponent;->access$4300(Lcom/squareup/DaggerSposReleaseAppComponent;)Ljavax/inject/Provider;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl$SCVPCS_ComponentImpl;->this$6:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl;->this$5:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl;->this$4:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl;->this$3:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl;->this$2:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;

    iget-object v2, v2, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl;->this$1:Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;

    invoke-static {v2}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;->access$17600(Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl;)Ljavax/inject/Provider;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen_Presenter_Factory;->create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen_Presenter_Factory;

    move-result-object v0

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->provider(Ljavax/inject/Provider;)Ljavax/inject/Provider;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl$SCVPCS_ComponentImpl;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method private injectErrorsBarView(Lcom/squareup/ui/ErrorsBarView;)Lcom/squareup/ui/ErrorsBarView;
    .locals 1

    .line 39110
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl$SCVPCS_ComponentImpl;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ErrorsBarPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/ErrorsBarView_MembersInjector;->injectPresenter(Lcom/squareup/ui/ErrorsBarView;Lcom/squareup/ui/ErrorsBarPresenter;)V

    return-object p1
.end method

.method private injectSaveCardVerifyPostalCodeView(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;)Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;
    .locals 1

    .line 39116
    iget-object v0, p0, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl$SCVPCS_ComponentImpl;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;)V

    return-object p1
.end method


# virtual methods
.method public inject(Lcom/squareup/ui/ErrorsBarView;)V
    .locals 0

    .line 39103
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl$SCVPCS_ComponentImpl;->injectErrorsBarView(Lcom/squareup/ui/ErrorsBarView;)Lcom/squareup/ui/ErrorsBarView;

    return-void
.end method

.method public inject(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;)V
    .locals 0

    .line 39107
    invoke-direct {p0, p1}, Lcom/squareup/DaggerSposReleaseAppComponent$SposReleaseLoggedInComponentImpl$SposReleaseMainActivityComponentImpl$SS_TabletComponentImpl$TS_ComponentImpl$STS_ComponentImpl$ViewInTransCImpl$SCVPCS_ComponentImpl;->injectSaveCardVerifyPostalCodeView(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;)Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;

    return-void
.end method
