.class public Lcom/squareup/analytics/GooglePlayServicesAvailabilityEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "GooglePlayServicesAvailabilityEvent.java"


# static fields
.field private static final CATALOG_NAME:Ljava/lang/String; = "mobile_collaborators"


# instance fields
.field private final mobile_collaborators_google_play_services_availability:Ljava/lang/String;

.field private final mobile_collaborators_google_play_services_client_library_version:J

.field private mobile_collaborators_google_play_services_installed_version:J

.field private mobile_collaborators_google_play_services_last_updated_time:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-string v0, "mobile_collaborators"

    .line 33
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    .line 34
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    .line 35
    invoke-direct {p0, v0}, Lcom/squareup/analytics/GooglePlayServicesAvailabilityEvent;->availabilityMessage(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/analytics/GooglePlayServicesAvailabilityEvent;->mobile_collaborators_google_play_services_availability:Ljava/lang/String;

    .line 36
    sget v0, Lcom/google/android/gms/common/GoogleApiAvailability;->GOOGLE_PLAY_SERVICES_VERSION_CODE:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/analytics/GooglePlayServicesAvailabilityEvent;->mobile_collaborators_google_play_services_client_library_version:J

    .line 39
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    const-string v0, "com.google.android.gms"

    const/4 v1, 0x0

    .line 40
    invoke-virtual {p1, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object p1

    .line 41
    iget v0, p1, Landroid/content/pm/PackageInfo;->versionCode:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/analytics/GooglePlayServicesAvailabilityEvent;->mobile_collaborators_google_play_services_installed_version:J

    .line 42
    iget-wide v0, p1, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    iput-wide v0, p0, Lcom/squareup/analytics/GooglePlayServicesAvailabilityEvent;->mobile_collaborators_google_play_services_last_updated_time:J
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-wide/16 v0, -0x1

    .line 44
    iput-wide v0, p0, Lcom/squareup/analytics/GooglePlayServicesAvailabilityEvent;->mobile_collaborators_google_play_services_installed_version:J

    .line 45
    iput-wide v0, p0, Lcom/squareup/analytics/GooglePlayServicesAvailabilityEvent;->mobile_collaborators_google_play_services_last_updated_time:J

    :goto_0
    return-void
.end method

.method private availabilityMessage(I)Ljava/lang/String;
    .locals 2

    if-eqz p1, :cond_5

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/16 v0, 0x9

    if-eq p1, v0, :cond_1

    const/16 v0, 0x12

    if-eq p1, v0, :cond_0

    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "unknown connection result value: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, "service updating"

    return-object p1

    :cond_1
    const-string p1, "service invalid"

    return-object p1

    :cond_2
    const-string p1, "service disabled"

    return-object p1

    :cond_3
    const-string p1, "service version update required"

    return-object p1

    :cond_4
    const-string p1, "service missing"

    return-object p1

    :cond_5
    const-string p1, "available"

    return-object p1
.end method
