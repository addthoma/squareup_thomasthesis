.class public Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;
.super Lcom/squareup/analytics/event/v1/ViewEvent;
.source "CardOnFileViewEvent.java"


# instance fields
.field public final contactToken:Ljava/lang/String;

.field public final entryFlow:Ljava/lang/String;

.field public final entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public final instrumentToken:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/RegisterViewName;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ViewEvent;-><init>(Lcom/squareup/analytics/EventNamedView;)V

    .line 15
    iput-object p2, p0, Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;->contactToken:Ljava/lang/String;

    const/4 p1, 0x0

    .line 16
    iput-object p1, p0, Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;->instrumentToken:Ljava/lang/String;

    .line 17
    iput-object p3, p0, Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;->entryFlow:Ljava/lang/String;

    .line 18
    iput-object p4, p0, Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/RegisterViewName;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ViewEvent;-><init>(Lcom/squareup/analytics/EventNamedView;)V

    .line 24
    iput-object p2, p0, Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;->contactToken:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;->instrumentToken:Ljava/lang/String;

    .line 26
    iput-object p4, p0, Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;->entryFlow:Ljava/lang/String;

    .line 27
    iput-object p5, p0, Lcom/squareup/analytics/event/v1/CardOnFileViewEvent;->entryMethod:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-void
.end method
