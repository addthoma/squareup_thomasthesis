.class public Lcom/squareup/analytics/event/ViewEvent;
.super Lcom/squareup/eventstream/v2/AppEvent;
.source "ViewEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/analytics/event/ViewEvent$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0016\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u000f\u0008\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B#\u0008\u0002\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0008\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0013\u0010\r\u001a\u00020\n2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0096\u0002J\u0008\u0010\u0010\u001a\u00020\u0011H\u0016J\u0008\u0010\u0012\u001a\u00020\u0003H\u0016R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u000cR\u0010\u0010\u0007\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0008\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/analytics/event/ViewEvent;",
        "Lcom/squareup/eventstream/v2/AppEvent;",
        "toScreenName",
        "",
        "(Ljava/lang/String;)V",
        "Lcom/squareup/analytics/RegisterViewName;",
        "(Lcom/squareup/analytics/RegisterViewName;)V",
        "mobile_view_event_from",
        "mobile_view_event_to",
        "isNavigationEvent",
        "",
        "(Ljava/lang/String;Ljava/lang/String;Z)V",
        "()Z",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "Companion",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CATALOG_NAME:Ljava/lang/String; = "mobile_view_event"

.field public static final Companion:Lcom/squareup/analytics/event/ViewEvent$Companion;

.field private static final NO_FROM:Ljava/lang/String; = "<unknown>"


# instance fields
.field private final transient isNavigationEvent:Z

.field public final mobile_view_event_from:Ljava/lang/String;

.field public final mobile_view_event_to:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/analytics/event/ViewEvent$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/analytics/event/ViewEvent$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/analytics/event/ViewEvent;->Companion:Lcom/squareup/analytics/event/ViewEvent$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/RegisterViewName;)V
    .locals 7

    const-string v0, "toScreenName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    iget-object v3, p1, Lcom/squareup/analytics/RegisterViewName;->value:Ljava/lang/String;

    const-string p1, "toScreenName.value"

    invoke-static {v3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 7

    const-string v0, "toScreenName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x5

    const/4 v6, 0x0

    move-object v1, p0

    move-object v3, p1

    .line 27
    invoke-direct/range {v1 .. v6}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "mobile_view_event"

    .line 25
    invoke-direct {p0, v0}, Lcom/squareup/eventstream/v2/AppEvent;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/analytics/event/ViewEvent;->mobile_view_event_from:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/analytics/event/ViewEvent;->mobile_view_event_to:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/squareup/analytics/event/ViewEvent;->isNavigationEvent:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    const-string p1, "<unknown>"

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 24
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/analytics/event/ViewEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public static final createNavigationEvent(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/analytics/event/ViewEvent;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/analytics/event/ViewEvent;->Companion:Lcom/squareup/analytics/event/ViewEvent$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/analytics/event/ViewEvent$Companion;->createNavigationEvent(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/analytics/event/ViewEvent;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 35
    move-object v0, p0

    check-cast v0, Lcom/squareup/analytics/event/ViewEvent;

    const/4 v1, 0x1

    if-ne v0, p1, :cond_0

    return v1

    .line 36
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    return v2

    :cond_2
    if-eqz p1, :cond_6

    .line 38
    check-cast p1, Lcom/squareup/analytics/event/ViewEvent;

    .line 40
    iget-object v0, p0, Lcom/squareup/analytics/event/ViewEvent;->mobile_view_event_from:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/analytics/event/ViewEvent;->mobile_view_event_from:Ljava/lang/String;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_3

    return v2

    .line 41
    :cond_3
    iget-object v0, p0, Lcom/squareup/analytics/event/ViewEvent;->mobile_view_event_to:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/analytics/event/ViewEvent;->mobile_view_event_to:Ljava/lang/String;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-eqz v0, :cond_4

    return v2

    .line 42
    :cond_4
    iget-boolean v0, p0, Lcom/squareup/analytics/event/ViewEvent;->isNavigationEvent:Z

    iget-boolean p1, p1, Lcom/squareup/analytics/event/ViewEvent;->isNavigationEvent:Z

    if-eq v0, p1, :cond_5

    return v2

    :cond_5
    return v1

    .line 38
    :cond_6
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.analytics.event.ViewEvent"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public hashCode()I
    .locals 2

    .line 48
    iget-object v0, p0, Lcom/squareup/analytics/event/ViewEvent;->mobile_view_event_from:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    .line 49
    iget-object v1, p0, Lcom/squareup/analytics/event/ViewEvent;->mobile_view_event_to:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    .line 50
    iget-boolean v1, p0, Lcom/squareup/analytics/event/ViewEvent;->isNavigationEvent:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final isNavigationEvent()Z
    .locals 1

    .line 24
    iget-boolean v0, p0, Lcom/squareup/analytics/event/ViewEvent;->isNavigationEvent:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "viewAppeared(["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/analytics/event/ViewEvent;->mobile_view_event_from:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "], ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/analytics/event/ViewEvent;->mobile_view_event_to:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "])"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
