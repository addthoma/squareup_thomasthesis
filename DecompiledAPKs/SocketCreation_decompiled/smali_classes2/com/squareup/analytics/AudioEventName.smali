.class public final enum Lcom/squareup/analytics/AudioEventName;
.super Ljava/lang/Enum;
.source "AudioEventName.kt"

# interfaces
.implements Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/analytics/AudioEventName;",
        ">;",
        "Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0008\u001a\u00020\u0004H\u0016J\u0012\u0010\t\u001a\u00020\u00042\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0016R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007j\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/analytics/AudioEventName;",
        "",
        "Lcom/squareup/cardreader/ReaderEventLogger$CardReaderEvent;",
        "eventValue",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getEventValue",
        "()Ljava/lang/String;",
        "getValue",
        "getValueForCardReader",
        "cardReaderInfo",
        "Lcom/squareup/cardreader/CardReaderInfo;",
        "SET_INITIAL_AUDIO_STATE",
        "SET_VOLUME",
        "SET_VOLUME_ERROR",
        "SET_RINGER_ERROR",
        "FW_UPDATE_STAY_AWAKE",
        "FW_UPDATE_ALREADY_AWAKE",
        "AUDIOTRACK_NOT_CREATED",
        "START_RECORDING",
        "STOP_RECORDING_NOT_RUNNING",
        "STOP_RECORDING_NOT_CONNECTED",
        "dipper_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/analytics/AudioEventName;

.field public static final enum AUDIOTRACK_NOT_CREATED:Lcom/squareup/analytics/AudioEventName;

.field public static final enum FW_UPDATE_ALREADY_AWAKE:Lcom/squareup/analytics/AudioEventName;

.field public static final enum FW_UPDATE_STAY_AWAKE:Lcom/squareup/analytics/AudioEventName;

.field public static final enum SET_INITIAL_AUDIO_STATE:Lcom/squareup/analytics/AudioEventName;

.field public static final enum SET_RINGER_ERROR:Lcom/squareup/analytics/AudioEventName;

.field public static final enum SET_VOLUME:Lcom/squareup/analytics/AudioEventName;

.field public static final enum SET_VOLUME_ERROR:Lcom/squareup/analytics/AudioEventName;

.field public static final enum START_RECORDING:Lcom/squareup/analytics/AudioEventName;

.field public static final enum STOP_RECORDING_NOT_CONNECTED:Lcom/squareup/analytics/AudioEventName;

.field public static final enum STOP_RECORDING_NOT_RUNNING:Lcom/squareup/analytics/AudioEventName;


# instance fields
.field private final eventValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/analytics/AudioEventName;

    new-instance v1, Lcom/squareup/analytics/AudioEventName;

    const/4 v2, 0x0

    const-string v3, "SET_INITIAL_AUDIO_STATE"

    const-string v4, "Initial Audio State"

    .line 7
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/analytics/AudioEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/analytics/AudioEventName;->SET_INITIAL_AUDIO_STATE:Lcom/squareup/analytics/AudioEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/analytics/AudioEventName;

    const/4 v2, 0x1

    const-string v3, "SET_VOLUME"

    const-string v4, "Setting Volume"

    .line 8
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/analytics/AudioEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/analytics/AudioEventName;->SET_VOLUME:Lcom/squareup/analytics/AudioEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/analytics/AudioEventName;

    const/4 v2, 0x2

    const-string v3, "SET_VOLUME_ERROR"

    const-string v4, "Set Volume Error"

    .line 9
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/analytics/AudioEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/analytics/AudioEventName;->SET_VOLUME_ERROR:Lcom/squareup/analytics/AudioEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/analytics/AudioEventName;

    const/4 v2, 0x3

    const-string v3, "SET_RINGER_ERROR"

    const-string v4, "Unable To Set Ringer"

    .line 10
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/analytics/AudioEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/analytics/AudioEventName;->SET_RINGER_ERROR:Lcom/squareup/analytics/AudioEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/analytics/AudioEventName;

    const/4 v2, 0x4

    const-string v3, "FW_UPDATE_STAY_AWAKE"

    const-string v4, "Firmware Updating: Staying Awake"

    .line 11
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/analytics/AudioEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/analytics/AudioEventName;->FW_UPDATE_STAY_AWAKE:Lcom/squareup/analytics/AudioEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/analytics/AudioEventName;

    const/4 v2, 0x5

    const-string v3, "FW_UPDATE_ALREADY_AWAKE"

    const-string v4, "Firmware Updating: Already Awake"

    .line 12
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/analytics/AudioEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/analytics/AudioEventName;->FW_UPDATE_ALREADY_AWAKE:Lcom/squareup/analytics/AudioEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/analytics/AudioEventName;

    const/4 v2, 0x6

    const-string v3, "AUDIOTRACK_NOT_CREATED"

    const-string v4, "Audiotrack Not Created"

    .line 13
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/analytics/AudioEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/analytics/AudioEventName;->AUDIOTRACK_NOT_CREATED:Lcom/squareup/analytics/AudioEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/analytics/AudioEventName;

    const/4 v2, 0x7

    const-string v3, "START_RECORDING"

    const-string v4, "Starting Recording"

    .line 14
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/analytics/AudioEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/analytics/AudioEventName;->START_RECORDING:Lcom/squareup/analytics/AudioEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/analytics/AudioEventName;

    const/16 v2, 0x8

    const-string v3, "STOP_RECORDING_NOT_RUNNING"

    const-string v4, "Stopping Recording: Scope Not Running"

    .line 15
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/analytics/AudioEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/analytics/AudioEventName;->STOP_RECORDING_NOT_RUNNING:Lcom/squareup/analytics/AudioEventName;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/analytics/AudioEventName;

    const/16 v2, 0x9

    const-string v3, "STOP_RECORDING_NOT_CONNECTED"

    const-string v4, "Stopping Recording: Headset Not Connected"

    .line 16
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/analytics/AudioEventName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/analytics/AudioEventName;->STOP_RECORDING_NOT_CONNECTED:Lcom/squareup/analytics/AudioEventName;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/analytics/AudioEventName;->$VALUES:[Lcom/squareup/analytics/AudioEventName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/analytics/AudioEventName;->eventValue:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/analytics/AudioEventName;
    .locals 1

    const-class v0, Lcom/squareup/analytics/AudioEventName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/analytics/AudioEventName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/analytics/AudioEventName;
    .locals 1

    sget-object v0, Lcom/squareup/analytics/AudioEventName;->$VALUES:[Lcom/squareup/analytics/AudioEventName;

    invoke-virtual {v0}, [Lcom/squareup/analytics/AudioEventName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/analytics/AudioEventName;

    return-object v0
.end method


# virtual methods
.method public final getEventValue()Ljava/lang/String;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/analytics/AudioEventName;->eventValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/analytics/AudioEventName;->eventValue:Ljava/lang/String;

    return-object v0
.end method

.method public getValueForCardReader(Lcom/squareup/cardreader/CardReaderInfo;)Ljava/lang/String;
    .locals 1

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 p1, 0x20

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/analytics/AudioEventName;->eventValue:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
