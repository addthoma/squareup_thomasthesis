.class Lcom/squareup/cashdrawer/CashDrawerTracker$1;
.super Ljava/lang/Object;
.source "CashDrawerTracker.java"

# interfaces
.implements Lcom/squareup/cashdrawer/CashDrawer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cashdrawer/CashDrawerTracker;-><init>(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterStations;Ljava/util/concurrent/Executor;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/thread/executor/MainThread;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cashdrawer/CashDrawerTracker;

.field final synthetic val$apgVasarioCashDrawer:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;


# direct methods
.method constructor <init>(Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/cashdrawer/CashDrawerTracker$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerTracker;

    iput-object p2, p0, Lcom/squareup/cashdrawer/CashDrawerTracker$1;->val$apgVasarioCashDrawer:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public drawerAvailable()V
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerTracker;

    iget-object v1, p0, Lcom/squareup/cashdrawer/CashDrawerTracker$1;->val$apgVasarioCashDrawer:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-static {v0, v1}, Lcom/squareup/cashdrawer/CashDrawerTracker;->access$000(Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/cashdrawer/CashDrawer;)V

    return-void
.end method

.method public drawerUnavailable(Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/cashdrawer/CashDrawerTracker$1;->this$0:Lcom/squareup/cashdrawer/CashDrawerTracker;

    iget-object v1, p0, Lcom/squareup/cashdrawer/CashDrawerTracker$1;->val$apgVasarioCashDrawer:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-static {v0, v1, p1}, Lcom/squareup/cashdrawer/CashDrawerTracker;->access$100(Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/cashdrawer/CashDrawer;Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V

    return-void
.end method
