.class Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$DiscovererListener;
.super Ljava/lang/Object;
.source "ApgVasarioCashDrawer.java"

# interfaces
.implements Lcom/squareup/usb/UsbDiscoverer$DeviceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DiscovererListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;


# direct methods
.method constructor <init>(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)V
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$DiscovererListener;->this$0:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public deviceAvailable(Landroid/hardware/usb/UsbDevice;)V
    .locals 5

    .line 138
    iget-object v0, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$DiscovererListener;->this$0:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-static {v0}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->access$300(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)Lcom/squareup/hardware/usb/UsbManager;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/hardware/usb/UsbManager;->openDevice(Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDeviceConnection;

    move-result-object v0

    if-nez v0, :cond_1

    .line 142
    iget-object p1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$DiscovererListener;->this$0:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-static {p1}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->access$400(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)Lcom/squareup/cashdrawer/CashDrawer$Listener;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$DiscovererListener;->this$0:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-static {p1}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->access$400(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)Lcom/squareup/cashdrawer/CashDrawer$Listener;

    move-result-object p1

    sget-object v0, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;->FAILED_TO_CONNECT:Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

    invoke-interface {p1, v0}, Lcom/squareup/cashdrawer/CashDrawer$Listener;->drawerUnavailable(Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    .line 146
    invoke-virtual {p1, v1}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object p1

    const/4 v2, 0x1

    .line 148
    invoke-virtual {v0, p1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 152
    invoke-virtual {p1, v1}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object p1

    .line 154
    iget-object v1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$DiscovererListener;->this$0:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    new-instance v2, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;

    invoke-static {v1}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->access$600(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)Lcom/squareup/thread/enforcer/ThreadEnforcer;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v0, p1, v3, v4}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;-><init>(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$1;)V

    invoke-static {v1, v2}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->access$502(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;)Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;

    .line 155
    iget-object p1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$DiscovererListener;->this$0:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-static {p1}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->access$400(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)Lcom/squareup/cashdrawer/CashDrawer$Listener;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$DiscovererListener;->this$0:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-static {p1}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->access$400(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)Lcom/squareup/cashdrawer/CashDrawer$Listener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/cashdrawer/CashDrawer$Listener;->drawerAvailable()V

    :cond_2
    return-void

    .line 150
    :cond_3
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Failed to claim interface"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

.method public deviceUnavailable(Landroid/hardware/usb/UsbDevice;)V
    .locals 1

    .line 159
    iget-object p1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$DiscovererListener;->this$0:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->access$502(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;)Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$Device;

    .line 160
    iget-object p1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$DiscovererListener;->this$0:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-static {p1}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->access$400(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)Lcom/squareup/cashdrawer/CashDrawer$Listener;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer$DiscovererListener;->this$0:Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;

    invoke-static {p1}, Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;->access$400(Lcom/squareup/cashdrawer/ApgVasarioCashDrawer;)Lcom/squareup/cashdrawer/CashDrawer$Listener;

    move-result-object p1

    sget-object v0, Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;->DISCONNECTED:Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;

    invoke-interface {p1, v0}, Lcom/squareup/cashdrawer/CashDrawer$Listener;->drawerUnavailable(Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;)V

    :cond_0
    return-void
.end method
