.class public interface abstract Lcom/squareup/cashdrawer/CashDrawer;
.super Ljava/lang/Object;
.source "CashDrawer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cashdrawer/CashDrawer$DisconnectReason;,
        Lcom/squareup/cashdrawer/CashDrawer$Listener;
    }
.end annotation


# virtual methods
.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract isAvailable()Z
.end method

.method public abstract isDrawerOpen()Z
.end method

.method public abstract openDrawer()V
.end method

.method public abstract setListener(Lcom/squareup/cashdrawer/CashDrawer$Listener;)V
.end method
