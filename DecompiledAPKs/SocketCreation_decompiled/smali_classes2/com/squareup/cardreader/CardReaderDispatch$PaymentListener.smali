.class public interface abstract Lcom/squareup/cardreader/CardReaderDispatch$PaymentListener;
.super Ljava/lang/Object;
.source "CardReaderDispatch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderDispatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PaymentListener"
.end annotation


# virtual methods
.method public abstract onAccountSelectionRequired(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onAudioRequest(Lcom/squareup/cardreader/lcr/CrsTmnAudio;)V
.end method

.method public abstract onAudioVisualRequest(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V
.end method

.method public abstract onCardError()V
.end method

.method public abstract onCardRemovedDuringPayment()V
.end method

.method public abstract onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
.end method

.method public abstract onDisplayRequest(Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onHardwarePinRequested(Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
.end method

.method public abstract onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
.end method

.method public abstract onMagSwipeFailed(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
.end method

.method public abstract onMagSwipePassthrough(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
.end method

.method public abstract onMagSwipeSuccess(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
.end method

.method public abstract onMagswipeFallbackSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
.end method

.method public abstract onPaymentContinuingDueToSwipe()V
.end method

.method public abstract onSecureTouchDisabled()V
.end method

.method public abstract onSecureTouchEnabled()V
.end method

.method public abstract onSecureTouchPinPadEvent(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)V
.end method

.method public abstract onSigRequested()V
.end method

.method public abstract onSoftwarePinRequested(Lcom/squareup/cardreader/PinRequestData;)V
.end method

.method public abstract onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
.end method

.method public abstract onTmnDataToTmn(Ljava/lang/String;[B)V
.end method

.method public abstract onTmnTransactionComplete(Lcom/squareup/cardreader/lcr/TmnTransactionResult;Lcom/squareup/cardreader/PaymentTimings;)V
.end method

.method public abstract onTmnWriteNotify(II[B)V
.end method

.method public abstract onUseChipCard()V
.end method

.method public abstract sendAuthorization([BZLcom/squareup/cardreader/CardInfo;)V
.end method

.method public abstract sendContactlessAuthorization([BLcom/squareup/cardreader/CardInfo;)V
.end method

.method public abstract sendTmnAuthorization([B)V
.end method
