.class public interface abstract Lcom/squareup/cardreader/SecureTouchFeature;
.super Ljava/lang/Object;
.source "SecureTouchFeature.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u0008\u0010\u0005\u001a\u00020\u0003H&J \u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\u00082\u0006\u0010\n\u001a\u00020\u0008H&J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u000c\u001a\u00020\u0008H&J\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u0008H&J\u0018\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0008H&\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/cardreader/SecureTouchFeature;",
        "",
        "disableSquidTouchDriver",
        "",
        "enableSquidTouchDriver",
        "hideSecureTouchPinPad",
        "onSecureTouchAccessibilityPinPadCenter",
        "x",
        "",
        "y",
        "pinPadType",
        "onSecureTouchKeepaliveFailed",
        "keepAliveErrorCode",
        "onSecureTouchPinPadEvent",
        "buttonType",
        "showSecureTouchPinPad",
        "cardInfo",
        "Lcom/squareup/cardreader/CardInfo;",
        "pinTryValue",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract disableSquidTouchDriver()V
.end method

.method public abstract enableSquidTouchDriver()V
.end method

.method public abstract hideSecureTouchPinPad()V
.end method

.method public abstract onSecureTouchAccessibilityPinPadCenter(III)V
.end method

.method public abstract onSecureTouchKeepaliveFailed(I)V
.end method

.method public abstract onSecureTouchPinPadEvent(I)V
.end method

.method public abstract showSecureTouchPinPad(Lcom/squareup/cardreader/CardInfo;I)V
.end method
