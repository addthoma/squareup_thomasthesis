.class public final Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnEventLogReceived"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u000e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0008H\u00c6\u0003J1\u0010\u0015\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u0008H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput;",
        "timestamp",
        "",
        "event",
        "",
        "sourceCode",
        "message",
        "",
        "(JIILjava/lang/String;)V",
        "getEvent",
        "()I",
        "getMessage",
        "()Ljava/lang/String;",
        "getSourceCode",
        "getTimestamp",
        "()J",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final event:I

.field private final message:Ljava/lang/String;

.field private final sourceCode:I

.field private final timestamp:J


# direct methods
.method public constructor <init>(JIILjava/lang/String;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 305
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->timestamp:J

    iput p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->event:I

    iput p4, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->sourceCode:I

    iput-object p5, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->message:Ljava/lang/String;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;JIILjava/lang/String;ILjava/lang/Object;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;
    .locals 6

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-wide p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->timestamp:J

    :cond_0
    move-wide v1, p1

    and-int/lit8 p1, p6, 0x2

    if-eqz p1, :cond_1

    iget p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->event:I

    :cond_1
    move v3, p3

    and-int/lit8 p1, p6, 0x4

    if-eqz p1, :cond_2

    iget p4, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->sourceCode:I

    :cond_2
    move v4, p4

    and-int/lit8 p1, p6, 0x8

    if-eqz p1, :cond_3

    iget-object p5, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->message:Ljava/lang/String;

    :cond_3
    move-object v5, p5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->copy(JIILjava/lang/String;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->timestamp:J

    return-wide v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->event:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->sourceCode:I

    return v0
.end method

.method public final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final copy(JIILjava/lang/String;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;
    .locals 7

    const-string v0, "message"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;

    move-object v1, v0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;-><init>(JIILjava/lang/String;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->timestamp:J

    iget-wide v2, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->timestamp:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->event:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->event:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->sourceCode:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->sourceCode:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->message:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->message:Ljava/lang/String;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getEvent()I
    .locals 1

    .line 302
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->event:I

    return v0
.end method

.method public final getMessage()Ljava/lang/String;
    .locals 1

    .line 304
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final getSourceCode()I
    .locals 1

    .line 303
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->sourceCode:I

    return v0
.end method

.method public final getTimestamp()J
    .locals 2

    .line 301
    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->timestamp:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->timestamp:J

    invoke-static {v0, v1}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->event:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->sourceCode:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->message:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnEventLogReceived(timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->timestamp:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->event:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", sourceCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->sourceCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$EventLogFeatureOutput$OnEventLogReceived;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
