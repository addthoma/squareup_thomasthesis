.class public Lcom/squareup/cardreader/GlobalCardReaderModule$Prod;
.super Ljava/lang/Object;
.source "GlobalCardReaderModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/cardreader/GlobalCardReaderModule$ProdWithoutCardReaderFactory;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/GlobalCardReaderModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Prod"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideCardReaderFactory(Lcom/squareup/cardreader/RealCardReaderFactory;)Lcom/squareup/cardreader/CardReaderFactory;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    return-object p1
.end method
