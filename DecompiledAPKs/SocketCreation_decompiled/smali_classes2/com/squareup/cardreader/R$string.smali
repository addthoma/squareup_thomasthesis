.class public final Lcom/squareup/cardreader/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final ble_pairing_already_bonded_message:I = 0x7f120183

.field public static final ble_pairing_failed_message:I = 0x7f120184

.field public static final call_your_bank_message:I = 0x7f12027e

.field public static final call_your_bank_title:I = 0x7f12027f

.field public static final cancel_refund:I = 0x7f120297

.field public static final cannot_process_refund_title:I = 0x7f1202b1

.field public static final card_not_read_message:I = 0x7f120311

.field public static final card_not_read_title:I = 0x7f120312

.field public static final card_not_supported_message:I = 0x7f120316

.field public static final cards_required_to_be_present_plural:I = 0x7f120346

.field public static final cards_required_to_be_present_singular:I = 0x7f120347

.field public static final contactless_action_required_message:I = 0x7f12049a

.field public static final contactless_action_required_title:I = 0x7f12049b

.field public static final contactless_card_declined_message:I = 0x7f12049c

.field public static final contactless_card_declined_title:I = 0x7f12049d

.field public static final contactless_chip_card_insertion_required_description:I = 0x7f12049e

.field public static final contactless_chip_card_insertion_required_title:I = 0x7f12049f

.field public static final contactless_interface_unavailable_message:I = 0x7f1204a0

.field public static final contactless_interface_unavailable_title:I = 0x7f1204a1

.field public static final contactless_limit_exceeded_insert_card_message:I = 0x7f1204a2

.field public static final contactless_limit_exceeded_insert_card_title:I = 0x7f1204a3

.field public static final contactless_limit_exceeded_try_another_card_message:I = 0x7f1204a4

.field public static final contactless_limit_exceeded_try_another_card_title:I = 0x7f1204a5

.field public static final contactless_one_card_message:I = 0x7f1204a6

.field public static final contactless_one_card_title:I = 0x7f1204a7

.field public static final contactless_reader_disconnected_title:I = 0x7f1204ad

.field public static final contactless_reader_required:I = 0x7f1204ae

.field public static final contactless_ready_message:I = 0x7f1204af

.field public static final contactless_ready_title:I = 0x7f1204b0

.field public static final contactless_tap_again_message:I = 0x7f1204b2

.field public static final contactless_tap_again_title:I = 0x7f1204b3

.field public static final contactless_too_many_taps_message:I = 0x7f1204b4

.field public static final contactless_unable_to_process_message:I = 0x7f1204b5

.field public static final contactless_unable_to_process_refund_message:I = 0x7f1204b6

.field public static final contactless_unlock_phone_and_try_again_message:I = 0x7f1204b8

.field public static final contactless_unlock_phone_to_pay_title:I = 0x7f1204b9

.field public static final contactless_waiting_for_tap_message:I = 0x7f1204bb

.field public static final contactless_waiting_for_tap_title:I = 0x7f1204bc

.field public static final emv_canceled:I = 0x7f120a47

.field public static final emv_declined:I = 0x7f120a4e

.field public static final emv_request_tap_refund:I = 0x7f120a66

.field public static final emv_std_msg_try_again_message:I = 0x7f120a6c

.field public static final emv_std_msg_try_again_title:I = 0x7f120a6d

.field public static final firmware_update_notification_title:I = 0x7f120ac0

.field public static final no_reader_connected:I = 0x7f12108b

.field public static final pairing_confirmation_message:I = 0x7f121309

.field public static final pairing_confirmation_title:I = 0x7f12130a

.field public static final pairing_list_row_text:I = 0x7f121313

.field public static final pairing_progress_title:I = 0x7f121314

.field public static final pairing_screen_bluetooth_enable_action:I = 0x7f121315

.field public static final pairing_screen_bluetooth_required_message:I = 0x7f121316

.field public static final pairing_screen_bluetooth_required_title:I = 0x7f121317

.field public static final payment_failed:I = 0x7f1213bd

.field public static final please_contact_support:I = 0x7f121441

.field public static final please_remove_card_title:I = 0x7f121443

.field public static final processing_payments_expiring_text:I = 0x7f1214f4

.field public static final reader_detail_accepts_legacy:I = 0x7f121569

.field public static final reader_detail_accepts_r12_value:I = 0x7f12156b

.field public static final reader_detail_accepts_r6_value:I = 0x7f12156c

.field public static final reader_detail_connection_bluetooth:I = 0x7f12156f

.field public static final reader_detail_connection_headset:I = 0x7f121570

.field public static final reader_detail_status_connecting_value:I = 0x7f121578

.field public static final reader_detail_status_failed_value:I = 0x7f121579

.field public static final reader_detail_status_permission_needed:I = 0x7f12157b

.field public static final reader_detail_status_ready_value:I = 0x7f12157c

.field public static final reader_detail_status_unavailable_value:I = 0x7f12157d

.field public static final reader_detail_status_updating_value:I = 0x7f12157e

.field public static final reader_failed_to_connect:I = 0x7f121580

.field public static final smart_reader_contactless_and_chip_name:I = 0x7f121826

.field public static final square_reader_bluetooth:I = 0x7f121895

.field public static final square_reader_chip:I = 0x7f121898

.field public static final square_reader_contactless_chip:I = 0x7f121899

.field public static final square_reader_magstripe:I = 0x7f12189b

.field public static final youtube_url_template:I = 0x7f121bf5


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
