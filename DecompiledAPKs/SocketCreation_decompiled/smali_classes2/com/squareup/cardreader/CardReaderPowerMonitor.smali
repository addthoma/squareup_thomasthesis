.class public Lcom/squareup/cardreader/CardReaderPowerMonitor;
.super Ljava/lang/Object;
.source "CardReaderPowerMonitor.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;
    }
.end annotation


# static fields
.field private static POLLING_INTERVAL:J = 0xdbba0L


# instance fields
.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final clock:Lcom/squareup/util/Clock;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final powerRequesters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/cardreader/CardReaderId;",
            "Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method constructor <init>(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/util/Clock;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 31
    iput-object p2, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 32
    iput-object p3, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->clock:Lcom/squareup/util/Clock;

    .line 33
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->powerRequesters:Ljava/util/Map;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/cardreader/CardReaderPowerMonitor;)Lcom/squareup/util/Clock;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->clock:Lcom/squareup/util/Clock;

    return-object p0
.end method

.method private readerCanRequestPowerStatus(Lcom/squareup/cardreader/CardReader;)Z
    .locals 0

    if-eqz p1, :cond_0

    .line 112
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isSmartReader()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private scheduleNextUpdate(Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;)V
    .locals 3

    .line 103
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->access$000(Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isAudio()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    sget-wide v1, Lcom/squareup/cardreader/CardReaderPowerMonitor;->POLLING_INTERVAL:J

    invoke-interface {v0, p1, v1, v2}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 2

    .line 92
    new-instance v0, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;-><init>(Lcom/squareup/cardreader/CardReaderPowerMonitor;Lcom/squareup/cardreader/CardReader;)V

    .line 93
    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->powerRequesters:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->scheduleNextUpdate(Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;)V

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 3

    .line 98
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->powerRequesters:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->powerRequesters:Ljava/util/Map;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 77
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {p1, p0}, Lcom/squareup/cardreader/CardReaderHub;->addCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReader;

    .line 80
    invoke-virtual {p0, v0}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 3

    .line 85
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p0}, Lcom/squareup/cardreader/CardReaderHub;->removeCardReaderAttachListener(Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->powerRequesters:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;

    .line 87
    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v2, v1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public requestPowerStatus(Lcom/squareup/cardreader/CardReaderId;)Z
    .locals 3

    .line 57
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->readerCanRequestPowerStatus(Lcom/squareup/cardreader/CardReader;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v1

    const-string v2, "Stopping power status polling for CardReader %s. Smart payments not supported."

    .line 58
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->powerRequesters:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return v1

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->powerRequesters:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;

    if-eqz p1, :cond_1

    .line 69
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v0, p1}, Lcom/squareup/thread/executor/MainThread;->cancel(Ljava/lang/Runnable;)V

    .line 70
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;->requestCardReaderPowerStatus()Z

    move-result v1

    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->scheduleNextUpdate(Lcom/squareup/cardreader/CardReaderPowerMonitor$PowerRequester;)V

    :cond_1
    return v1
.end method

.method public requestPowerStatusForAllReaders()V
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderPowerMonitor;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    .line 38
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/squareup/cardreader/CardReaderPowerMonitor;->requestPowerStatus(Lcom/squareup/cardreader/CardReaderId;)Z

    goto :goto_0

    :cond_0
    return-void
.end method
