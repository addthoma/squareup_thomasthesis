.class Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_isReaderSdk;
.super Ljava/lang/Object;
.source "DaggerT2CardReaderContextComponent.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "com_squareup_cardreader_CardReaderContextParent_isReaderSdk"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljavax/inject/Provider<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderContextParent:Lcom/squareup/cardreader/CardReaderContextParent;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CardReaderContextParent;)V
    .locals 0

    .line 530
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 531
    iput-object p1, p0, Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_isReaderSdk;->cardReaderContextParent:Lcom/squareup/cardreader/CardReaderContextParent;

    return-void
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 1

    .line 536
    iget-object v0, p0, Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_isReaderSdk;->cardReaderContextParent:Lcom/squareup/cardreader/CardReaderContextParent;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderContextParent;->isReaderSdk()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 526
    invoke-virtual {p0}, Lcom/squareup/cardreader/squid/t2/DaggerT2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_isReaderSdk;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
