.class public final Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideX2BackendFactory;
.super Ljava/lang/Object;
.source "SpeCardReaderModule_ProvideX2BackendFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/squid/common/SpeBackend;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardreaderNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideX2BackendFactory;->cardreaderNativeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideX2BackendFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;",
            ">;)",
            "Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideX2BackendFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideX2BackendFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideX2BackendFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideX2Backend(Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;)Lcom/squareup/cardreader/squid/common/SpeBackend;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule;->provideX2Backend(Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;)Lcom/squareup/cardreader/squid/common/SpeBackend;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/squid/common/SpeBackend;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/squid/common/SpeBackend;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideX2BackendFactory;->cardreaderNativeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;

    invoke-static {v0}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideX2BackendFactory;->provideX2Backend(Lcom/squareup/cardreader/lcr/CardreaderNativeInterface;)Lcom/squareup/cardreader/squid/common/SpeBackend;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/squid/common/SpeCardReaderModule_ProvideX2BackendFactory;->get()Lcom/squareup/cardreader/squid/common/SpeBackend;

    move-result-object v0

    return-object v0
.end method
