.class public Lcom/squareup/cardreader/squid/common/SpeCardReaderModule$NoOpBluetoothUtils;
.super Ljava/lang/Object;
.source "SpeCardReaderModule.java"

# interfaces
.implements Lcom/squareup/cardreader/BluetoothUtils;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/squid/common/SpeCardReaderModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NoOpBluetoothUtils"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bondedDevicesCount(Ljava/lang/String;)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public disable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public enable()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public hasPermission()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isConnectedBle(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public isEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public supportsBle()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public supportsBluetooth()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public unpairDevice(Ljava/lang/String;)V
    .locals 0

    return-void
.end method
