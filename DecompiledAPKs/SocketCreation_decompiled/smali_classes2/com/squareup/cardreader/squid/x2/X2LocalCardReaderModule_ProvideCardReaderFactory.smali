.class public final Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideCardReaderFactory;
.super Ljava/lang/Object;
.source "X2LocalCardReaderModule_ProvideCardReaderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReader;",
        ">;"
    }
.end annotation


# instance fields
.field private final localCardReaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LocalCardReader;",
            ">;"
        }
    .end annotation
.end field

.field private final x2SystemFeatureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/X2SystemFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LocalCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/X2SystemFeatureLegacy;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideCardReaderFactory;->localCardReaderProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideCardReaderFactory;->x2SystemFeatureProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideCardReaderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/LocalCardReader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/X2SystemFeatureLegacy;",
            ">;)",
            "Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideCardReaderFactory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideCardReaderFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideCardReaderFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCardReader(Lcom/squareup/cardreader/LocalCardReader;Lcom/squareup/cardreader/X2SystemFeatureLegacy;)Lcom/squareup/cardreader/CardReader;
    .locals 0

    .line 44
    invoke-static {p0, p1}, Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule;->provideCardReader(Lcom/squareup/cardreader/LocalCardReader;Lcom/squareup/cardreader/X2SystemFeatureLegacy;)Lcom/squareup/cardreader/CardReader;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/CardReader;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReader;
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideCardReaderFactory;->localCardReaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/LocalCardReader;

    iget-object v1, p0, Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideCardReaderFactory;->x2SystemFeatureProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/X2SystemFeatureLegacy;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideCardReaderFactory;->provideCardReader(Lcom/squareup/cardreader/LocalCardReader;Lcom/squareup/cardreader/X2SystemFeatureLegacy;)Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/cardreader/squid/x2/X2LocalCardReaderModule_ProvideCardReaderFactory;->get()Lcom/squareup/cardreader/CardReader;

    move-result-object v0

    return-object v0
.end method
