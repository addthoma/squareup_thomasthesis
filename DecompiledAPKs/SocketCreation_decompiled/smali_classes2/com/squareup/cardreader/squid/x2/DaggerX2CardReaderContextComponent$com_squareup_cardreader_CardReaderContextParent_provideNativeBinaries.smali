.class Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_provideNativeBinaries;
.super Ljava/lang/Object;
.source "DaggerX2CardReaderContextComponent.java"

# interfaces
.implements Ljavax/inject/Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "com_squareup_cardreader_CardReaderContextParent_provideNativeBinaries"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljavax/inject/Provider<",
        "Lcom/squareup/cardreader/NativeBinaries;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderContextParent:Lcom/squareup/cardreader/CardReaderContextParent;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CardReaderContextParent;)V
    .locals 0

    .line 566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 567
    iput-object p1, p0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_provideNativeBinaries;->cardReaderContextParent:Lcom/squareup/cardreader/CardReaderContextParent;

    return-void
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/NativeBinaries;
    .locals 2

    .line 572
    iget-object v0, p0, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_provideNativeBinaries;->cardReaderContextParent:Lcom/squareup/cardreader/CardReaderContextParent;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderContextParent;->provideNativeBinaries()Lcom/squareup/cardreader/NativeBinaries;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable component method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/NativeBinaries;

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 562
    invoke-virtual {p0}, Lcom/squareup/cardreader/squid/x2/DaggerX2CardReaderContextComponent$com_squareup_cardreader_CardReaderContextParent_provideNativeBinaries;->get()Lcom/squareup/cardreader/NativeBinaries;

    move-result-object v0

    return-object v0
.end method
