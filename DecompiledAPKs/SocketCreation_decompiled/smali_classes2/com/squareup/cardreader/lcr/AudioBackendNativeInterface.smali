.class public interface abstract Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;
.super Ljava/lang/Object;
.source "AudioBackendNativeInterface.java"


# virtual methods
.method public abstract cr_comms_backend_audio_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;
.end method

.method public abstract cr_comms_backend_audio_enable_tx_for_connection(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V
.end method

.method public abstract cr_comms_backend_audio_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;
.end method

.method public abstract cr_comms_backend_audio_notify_phy_tx_complete(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V
.end method

.method public abstract cr_comms_backend_audio_shutdown(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V
.end method

.method public abstract decode_r4_packet(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;I[S)Ljava/lang/Object;
.end method

.method public abstract feed_audio_samples(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Ljava/lang/Object;II)V
.end method

.method public abstract initialize_backend_audio(IILcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;
.end method

.method public abstract set_legacy_reader_type(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;I)V
.end method
