.class public interface abstract Lcom/squareup/cardreader/lcr/UserInteractionFeatureNativeInterface;
.super Ljava/lang/Object;
.source "UserInteractionFeatureNativeInterface.java"


# virtual methods
.method public abstract cr_user_interaction_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;
.end method

.method public abstract cr_user_interaction_identify_reader(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;
.end method

.method public abstract cr_user_interaction_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;)Lcom/squareup/cardreader/lcr/CrUserInteractionResult;
.end method

.method public abstract user_interaction_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_user_interaction_t;
.end method
