.class public final enum Lcom/squareup/cardreader/lcr/CrSecureTouchResult;
.super Ljava/lang/Enum;
.source "CrSecureTouchResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrSecureTouchResult$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrSecureTouchResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

.field public static final enum CR_SECURE_TOUCH_MODE_FEATURE_RESULT_ALREADY_INITIALIZED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

.field public static final enum CR_SECURE_TOUCH_MODE_FEATURE_RESULT_ALREADY_TERMINATED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

.field public static final enum CR_SECURE_TOUCH_MODE_FEATURE_RESULT_CALL_UNEXPECTED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

.field public static final enum CR_SECURE_TOUCH_MODE_FEATURE_RESULT_INVALID_PARAMETER:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

.field public static final enum CR_SECURE_TOUCH_MODE_FEATURE_RESULT_NOT_INITIALIZED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

.field public static final enum CR_SECURE_TOUCH_MODE_FEATURE_RESULT_NOT_TERMINATED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

.field public static final enum CR_SECURE_TOUCH_MODE_FEATURE_RESULT_SESSION_ERROR:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

.field public static final enum CR_SECURE_TOUCH_MODE_FEATURE_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    const/4 v1, 0x0

    const-string v2, "CR_SECURE_TOUCH_MODE_FEATURE_RESULT_SUCCESS"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    const/4 v2, 0x1

    const-string v3, "CR_SECURE_TOUCH_MODE_FEATURE_RESULT_INVALID_PARAMETER"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_INVALID_PARAMETER:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    const/4 v3, 0x2

    const-string v4, "CR_SECURE_TOUCH_MODE_FEATURE_RESULT_NOT_INITIALIZED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_NOT_INITIALIZED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    const/4 v4, 0x3

    const-string v5, "CR_SECURE_TOUCH_MODE_FEATURE_RESULT_ALREADY_INITIALIZED"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_ALREADY_INITIALIZED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    const/4 v5, 0x4

    const-string v6, "CR_SECURE_TOUCH_MODE_FEATURE_RESULT_NOT_TERMINATED"

    invoke-direct {v0, v6, v5}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_NOT_TERMINATED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    const/4 v6, 0x5

    const-string v7, "CR_SECURE_TOUCH_MODE_FEATURE_RESULT_ALREADY_TERMINATED"

    invoke-direct {v0, v7, v6}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_ALREADY_TERMINATED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    const/4 v7, 0x6

    const-string v8, "CR_SECURE_TOUCH_MODE_FEATURE_RESULT_SESSION_ERROR"

    invoke-direct {v0, v8, v7}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_SESSION_ERROR:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    const/4 v8, 0x7

    const-string v9, "CR_SECURE_TOUCH_MODE_FEATURE_RESULT_CALL_UNEXPECTED"

    invoke-direct {v0, v9, v8}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_CALL_UNEXPECTED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    .line 11
    sget-object v9, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    aput-object v9, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_INVALID_PARAMETER:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_NOT_INITIALIZED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_ALREADY_INITIALIZED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_NOT_TERMINATED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_ALREADY_TERMINATED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_SESSION_ERROR:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->CR_SECURE_TOUCH_MODE_FEATURE_RESULT_CALL_UNEXPECTED:Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->$VALUES:[Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 43
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrSecureTouchResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrSecureTouchResult;",
            ")V"
        }
    .end annotation

    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->swigValue:I

    .line 49
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrSecureTouchResult$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;
    .locals 6

    .line 26
    const-class v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    .line 27
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 28
    aget-object p0, v1, p0

    return-object p0

    .line 29
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 30
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 32
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrSecureTouchResult;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrSecureTouchResult;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->$VALUES:[Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrSecureTouchResult;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 22
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrSecureTouchResult;->swigValue:I

    return v0
.end method
