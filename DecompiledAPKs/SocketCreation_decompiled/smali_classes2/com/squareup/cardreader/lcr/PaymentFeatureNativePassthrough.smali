.class public Lcom/squareup/cardreader/lcr/PaymentFeatureNativePassthrough;
.super Ljava/lang/Object;
.source "PaymentFeatureNativePassthrough.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cr_payment_cancel_payment(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 20
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->cr_payment_cancel_payment(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_payment_enable_swipe_passthrough(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Z)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 31
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->cr_payment_enable_swipe_passthrough(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Z)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_payment_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 15
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->cr_payment_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_payment_request_card_presence(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 25
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->cr_payment_request_card_presence(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public cr_payment_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 10
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->cr_payment_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public payment_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;II)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;
    .locals 0

    .line 37
    invoke-static {p1, p2, p3, p4}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;II)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    move-result-object p1

    return-object p1
.end method

.method public payment_process_server_response(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 56
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_process_server_response(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public payment_select_account_type(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 68
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_select_account_type(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public payment_select_application(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 50
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_select_application(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public payment_send_powerup_hint(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;I)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 62
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_send_powerup_hint(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public payment_start_payment(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;JIIIIIIII)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 44
    invoke-static/range {p1 .. p11}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_start_payment(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;JIIIIIIII)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public payment_tmn_cancel_request(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 91
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_tmn_cancel_request(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public payment_tmn_send_bytes_to_reader(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 81
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_tmn_send_bytes_to_reader(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public payment_tmn_start_miryo(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 86
    invoke-static {p1, p2}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_tmn_start_miryo(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public payment_tmn_start_transaction(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 75
    invoke-static/range {p1 .. p6}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_tmn_start_transaction(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method

.method public payment_tmn_write_notify_ack(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 0

    .line 96
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNative;->payment_tmn_write_notify_ack(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    return-object p1
.end method
