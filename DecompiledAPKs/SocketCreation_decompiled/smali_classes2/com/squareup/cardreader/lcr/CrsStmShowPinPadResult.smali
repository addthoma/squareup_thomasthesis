.class public Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;
.super Ljava/lang/Object;
.source "CrsStmShowPinPadResult.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 73
    invoke-static {}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->new_CrsStmShowPinPadResult()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;-><init>(JZ)V

    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-boolean p3, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCMemOwn:Z

    .line 17
    iput-wide p1, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCPtr:J

    return-void
.end method

.method protected static getCPtr(Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;)J
    .locals 2

    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    goto :goto_0

    .line 21
    :cond_0
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCPtr:J

    :goto_0
    return-wide v0
.end method


# virtual methods
.method public declared-synchronized delete()V
    .locals 5

    monitor-enter p0

    .line 29
    :try_start_0
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCPtr:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 31
    iput-boolean v0, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->delete_CrsStmShowPinPadResult(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCPtr:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected finalize()V
    .locals 0

    .line 25
    invoke-virtual {p0}, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->delete()V

    return-void
.end method

.method public getAccess_pin_pad_config()Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;
    .locals 5

    .line 68
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmShowPinPadResult_access_pin_pad_config_get(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 69
    :cond_0
    new-instance v2, Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;-><init>(JZ)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method

.method public getButtons()Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;
    .locals 5

    .line 51
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmShowPinPadResult_buttons_get(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 52
    :cond_0
    new-instance v2, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;-><init>(JZ)V

    move-object v0, v2

    :goto_0
    return-object v0
.end method

.method public getConfig_mode()S
    .locals 2

    .line 60
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmShowPinPadResult_config_mode_get(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;)S

    move-result v0

    return v0
.end method

.method public getNumber_of_buttons()S
    .locals 2

    .line 43
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmShowPinPadResult_number_of_buttons_get(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;)S

    move-result v0

    return v0
.end method

.method public setAccess_pin_pad_config(Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)V
    .locals 6

    .line 64
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCPtr:J

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;->getCPtr(Lcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmShowPinPadResult_access_pin_pad_config_set(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;JLcom/squareup/cardreader/lcr/CrsStmAccessibilityPinPadConfig;)V

    return-void
.end method

.method public setButtons(Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V
    .locals 6

    .line 47
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCPtr:J

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;->getCPtr(Lcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmShowPinPadResult_buttons_set(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;JLcom/squareup/cardreader/lcr/CrsStmPinPadButtonInfo;)V

    return-void
.end method

.method public setConfig_mode(S)V
    .locals 2

    .line 56
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmShowPinPadResult_config_mode_set(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;S)V

    return-void
.end method

.method public setNumber_of_buttons(S)V
    .locals 2

    .line 39
    iget-wide v0, p0, Lcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/squareup/cardreader/lcr/SecureTouchFeatureNativeJNI;->CrsStmShowPinPadResult_number_of_buttons_set(JLcom/squareup/cardreader/lcr/CrsStmShowPinPadResult;S)V

    return-void
.end method
