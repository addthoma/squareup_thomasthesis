.class public final enum Lcom/squareup/cardreader/lcr/CrCardreaderType;
.super Ljava/lang/Enum;
.source "CrCardreaderType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrCardreaderType$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrCardreaderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrCardreaderType;

.field public static final enum CR_CARDREADER_READER_TYPE_A10:Lcom/squareup/cardreader/lcr/CrCardreaderType;

.field public static final enum CR_CARDREADER_READER_TYPE_GEN2:Lcom/squareup/cardreader/lcr/CrCardreaderType;

.field public static final enum CR_CARDREADER_READER_TYPE_O1:Lcom/squareup/cardreader/lcr/CrCardreaderType;

.field public static final enum CR_CARDREADER_READER_TYPE_R12:Lcom/squareup/cardreader/lcr/CrCardreaderType;

.field public static final enum CR_CARDREADER_READER_TYPE_R12C:Lcom/squareup/cardreader/lcr/CrCardreaderType;

.field public static final enum CR_CARDREADER_READER_TYPE_R4:Lcom/squareup/cardreader/lcr/CrCardreaderType;

.field public static final enum CR_CARDREADER_READER_TYPE_R6:Lcom/squareup/cardreader/lcr/CrCardreaderType;

.field public static final enum CR_CARDREADER_READER_TYPE_T2:Lcom/squareup/cardreader/lcr/CrCardreaderType;

.field public static final enum CR_CARDREADER_READER_TYPE_T2B:Lcom/squareup/cardreader/lcr/CrCardreaderType;

.field public static final enum CR_CARDREADER_READER_TYPE_UNKNOWN:Lcom/squareup/cardreader/lcr/CrCardreaderType;

.field public static final enum CR_CARDREADER_READER_TYPE_X2:Lcom/squareup/cardreader/lcr/CrCardreaderType;

.field public static final enum CR_CARDREADER_READER_TYPE_X2B:Lcom/squareup/cardreader/lcr/CrCardreaderType;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    const/4 v1, 0x0

    const-string v2, "CR_CARDREADER_READER_TYPE_UNKNOWN"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/lcr/CrCardreaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_UNKNOWN:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    const/4 v2, 0x1

    const-string v3, "CR_CARDREADER_READER_TYPE_GEN2"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/lcr/CrCardreaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_GEN2:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    const/4 v3, 0x2

    const-string v4, "CR_CARDREADER_READER_TYPE_O1"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/lcr/CrCardreaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_O1:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    const/4 v4, 0x3

    const-string v5, "CR_CARDREADER_READER_TYPE_R4"

    invoke-direct {v0, v5, v4}, Lcom/squareup/cardreader/lcr/CrCardreaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_R4:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    const/4 v5, 0x4

    const-string v6, "CR_CARDREADER_READER_TYPE_R6"

    invoke-direct {v0, v6, v5}, Lcom/squareup/cardreader/lcr/CrCardreaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_R6:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    const/4 v6, 0x5

    const-string v7, "CR_CARDREADER_READER_TYPE_R12"

    invoke-direct {v0, v7, v6}, Lcom/squareup/cardreader/lcr/CrCardreaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_R12:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    const/4 v7, 0x6

    const-string v8, "CR_CARDREADER_READER_TYPE_A10"

    invoke-direct {v0, v8, v7}, Lcom/squareup/cardreader/lcr/CrCardreaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_A10:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    const/4 v8, 0x7

    const-string v9, "CR_CARDREADER_READER_TYPE_X2"

    invoke-direct {v0, v9, v8}, Lcom/squareup/cardreader/lcr/CrCardreaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_X2:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    const/16 v9, 0x8

    const-string v10, "CR_CARDREADER_READER_TYPE_R12C"

    invoke-direct {v0, v10, v9}, Lcom/squareup/cardreader/lcr/CrCardreaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_R12C:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    .line 21
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    const/16 v10, 0x9

    const-string v11, "CR_CARDREADER_READER_TYPE_T2"

    invoke-direct {v0, v11, v10}, Lcom/squareup/cardreader/lcr/CrCardreaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_T2:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    .line 22
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    const/16 v11, 0xa

    const-string v12, "CR_CARDREADER_READER_TYPE_X2B"

    invoke-direct {v0, v12, v11}, Lcom/squareup/cardreader/lcr/CrCardreaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_X2B:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    .line 23
    new-instance v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    const/16 v12, 0xb

    const-string v13, "CR_CARDREADER_READER_TYPE_T2B"

    invoke-direct {v0, v13, v12}, Lcom/squareup/cardreader/lcr/CrCardreaderType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_T2B:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrCardreaderType;

    .line 11
    sget-object v13, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_UNKNOWN:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_GEN2:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_O1:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_R4:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_R6:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_R12:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_A10:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_X2:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_R12C:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_T2:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_X2B:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/cardreader/lcr/CrCardreaderType;->CR_CARDREADER_READER_TYPE_T2B:Lcom/squareup/cardreader/lcr/CrCardreaderType;

    aput-object v1, v0, v12

    sput-object v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->$VALUES:[Lcom/squareup/cardreader/lcr/CrCardreaderType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrCardreaderType$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 47
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrCardreaderType$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrCardreaderType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrCardreaderType;",
            ")V"
        }
    .end annotation

    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrCardreaderType;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->swigValue:I

    .line 53
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrCardreaderType$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCardreaderType;
    .locals 6

    .line 30
    const-class v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrCardreaderType;

    .line 31
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrCardreaderType;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 32
    aget-object p0, v1, p0

    return-object p0

    .line 33
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 34
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrCardreaderType;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 36
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrCardreaderType;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrCardreaderType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrCardreaderType;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->$VALUES:[Lcom/squareup/cardreader/lcr/CrCardreaderType;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrCardreaderType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrCardreaderType;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 26
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrCardreaderType;->swigValue:I

    return v0
.end method
