.class public final enum Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;
.super Ljava/lang/Enum;
.source "CrPaymentTransactionType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/lcr/CrPaymentTransactionType$SwigNext;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

.field public static final enum CR_PAYMENT_TRANSACTION_TYPE_PURCHASE:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

.field public static final enum CR_PAYMENT_TRANSACTION_TYPE_REFUND:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;


# instance fields
.field private final swigValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 12
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    const/4 v1, 0x0

    const-string v2, "CR_PAYMENT_TRANSACTION_TYPE_PURCHASE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->CR_PAYMENT_TRANSACTION_TYPE_PURCHASE:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    const/4 v2, 0x1

    const-string v3, "CR_PAYMENT_TRANSACTION_TYPE_REFUND"

    const/16 v4, 0x20

    invoke-direct {v0, v3, v2, v4}, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->CR_PAYMENT_TRANSACTION_TYPE_REFUND:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    .line 11
    sget-object v3, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->CR_PAYMENT_TRANSACTION_TYPE_PURCHASE:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->CR_PAYMENT_TRANSACTION_TYPE_REFUND:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->$VALUES:[Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType$SwigNext;->access$008()I

    move-result p1

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->swigValue:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 36
    iput p3, p0, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->swigValue:I

    add-int/lit8 p3, p3, 0x1

    .line 37
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType$SwigNext;->access$002(I)I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/cardreader/lcr/CrPaymentTransactionType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;",
            ")V"
        }
    .end annotation

    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    iget p1, p3, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->swigValue:I

    iput p1, p0, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->swigValue:I

    .line 43
    iget p1, p0, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->swigValue:I

    add-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType$SwigNext;->access$002(I)I

    return-void
.end method

.method public static swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;
    .locals 6

    .line 20
    const-class v0, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    .line 21
    array-length v2, v1

    if-ge p0, v2, :cond_0

    if-ltz p0, :cond_0

    aget-object v2, v1, p0

    iget v2, v2, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->swigValue:I

    if-ne v2, p0, :cond_0

    .line 22
    aget-object p0, v1, p0

    return-object p0

    .line 23
    :cond_0
    array-length v2, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 24
    iget v5, v4, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->swigValue:I

    if-ne v5, p0, :cond_1

    return-object v4

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 26
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No enum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " with value "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->$VALUES:[Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    return-object v0
.end method


# virtual methods
.method public final swigValue()I
    .locals 1

    .line 16
    iget v0, p0, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->swigValue:I

    return v0
.end method
