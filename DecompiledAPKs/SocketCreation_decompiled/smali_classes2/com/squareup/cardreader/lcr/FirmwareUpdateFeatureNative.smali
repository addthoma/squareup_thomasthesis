.class public Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNative;
.super Ljava/lang/Object;
.source "FirmwareUpdateFeatureNative.java"

# interfaces
.implements Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeConstants;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cr_firmware_update_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
    .locals 2

    .line 17
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeJNI;->cr_firmware_update_feature_free(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_firmware_update_feature_get_manifest(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
    .locals 2

    .line 21
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeJNI;->cr_firmware_update_feature_get_manifest(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_firmware_update_feature_stop_sending_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
    .locals 2

    .line 25
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeJNI;->cr_firmware_update_feature_stop_sending_data(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object p0

    return-object p0
.end method

.method public static cr_firmware_update_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
    .locals 2

    .line 13
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeJNI;->cr_firmware_update_feature_term(J)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object p0

    return-object p0
.end method

.method public static firmware_send_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;[B[B[B)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
    .locals 2

    .line 34
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1, p2, p3}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeJNI;->firmware_send_data(J[B[B[B)I

    move-result p0

    invoke-static {p0}, Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object p0

    return-object p0
.end method

.method public static firmware_update_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;
    .locals 3

    .line 29
    invoke-static {p0}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;->getCPtr(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeJNI;->firmware_update_initialize(JLjava/lang/Object;)J

    move-result-wide p0

    const-wide/16 v0, 0x0

    cmp-long v2, p0, v0

    if-nez v2, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 30
    :cond_0
    new-instance v0, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;-><init>(JZ)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method
