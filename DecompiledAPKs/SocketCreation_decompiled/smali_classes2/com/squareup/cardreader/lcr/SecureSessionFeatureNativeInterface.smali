.class public interface abstract Lcom/squareup/cardreader/lcr/SecureSessionFeatureNativeInterface;
.super Ljava/lang/Object;
.source "SecureSessionFeatureNativeInterface.java"


# virtual methods
.method public abstract cr_securesession_feature_establish_session(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
.end method

.method public abstract cr_securesession_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
.end method

.method public abstract cr_securesession_feature_notify_server_error(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
.end method

.method public abstract cr_securesession_feature_pin_bypass(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
.end method

.method public abstract cr_securesession_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
.end method

.method public abstract pin_add_digit(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;I)Z
.end method

.method public abstract pin_reset(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)V
.end method

.method public abstract pin_submit(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;)Lcom/squareup/cardreader/lcr/CrSecureSessionResult;
.end method

.method public abstract securesession_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;
.end method

.method public abstract securesession_recv_server_message(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;[B)Lcom/squareup/cardreader/lcr/CrSecureSessionResultError;
.end method

.method public abstract set_kb(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_securesession_feature_t;[B)Z
.end method
