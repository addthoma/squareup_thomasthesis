.class public final Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;
.super Ljava/lang/Object;
.source "HeadsetStateDispatcher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/HeadsetStateDispatcher;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final headsetConnectionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final rootBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->headsetConnectionStateProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->libraryLoaderProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->rootBusProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;"
        }
    .end annotation

    .line 57
    new-instance v7, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/cardreader/loader/LibraryLoader;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/Features;)Lcom/squareup/cardreader/HeadsetStateDispatcher;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/wavpool/swipe/HeadsetConnectionState;",
            ">;",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            "Lcom/squareup/badbus/BadEventSink;",
            "Lcom/squareup/settings/server/Features;",
            ")",
            "Lcom/squareup/cardreader/HeadsetStateDispatcher;"
        }
    .end annotation

    .line 64
    new-instance v7, Lcom/squareup/cardreader/HeadsetStateDispatcher;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/cardreader/HeadsetStateDispatcher;-><init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/loader/LibraryLoader;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/Features;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/HeadsetStateDispatcher;
    .locals 6

    .line 48
    iget-object v0, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->headsetConnectionStateProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->libraryLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/loader/LibraryLoader;

    iget-object v2, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v3, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v4, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->rootBusProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/badbus/BadEventSink;

    iget-object v5, p0, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/settings/server/Features;

    invoke-static/range {v0 .. v5}, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/cardreader/loader/LibraryLoader;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/settings/server/Features;)Lcom/squareup/cardreader/HeadsetStateDispatcher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/cardreader/HeadsetStateDispatcher_Factory;->get()Lcom/squareup/cardreader/HeadsetStateDispatcher;

    move-result-object v0

    return-object v0
.end method
