.class Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;
.super Lcom/squareup/cardreader/CardReaderInfoUpdater;
.source "CardReaderInitializer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderInitializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InternalListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/CardReaderInitializer;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/CardReaderInitializer;)V
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    .line 173
    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$000(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/CardReaderInfoUpdater;-><init>(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method private continuePostTamperInit()V
    .locals 4

    .line 474
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$600(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->requestFirmwareManifest()V

    .line 475
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isFirmwareUpdateBlocking()Z

    move-result v0

    if-nez v0, :cond_1

    .line 481
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$1200(Lcom/squareup/cardreader/CardReaderInitializer;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object v2, Lcom/squareup/cardreader/CardReaderEventName;->INIT_SECURE_SESSION:Lcom/squareup/cardreader/CardReaderEventName;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/CardReaderInitializer;->access$400(Lcom/squareup/cardreader/CardReaderInitializer;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V

    .line 483
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$1202(Lcom/squareup/cardreader/CardReaderInitializer;Z)Z

    .line 484
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$902(Lcom/squareup/cardreader/CardReaderInitializer;Z)Z

    .line 485
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$600(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->initializeSecureSession()V

    .line 486
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderStatusListener;->initializingSecureSession(Lcom/squareup/cardreader/CardReaderInfo;)V

    goto :goto_0

    .line 488
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Why are you calling initializeSecureSession another time than once?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 494
    :goto_0
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$1400(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/thread/executor/MainThread;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$1300(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderInitializer$CoreDumpRunner;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    return-void
.end method

.method private setReaderFeatureFlags()V
    .locals 2

    .line 231
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->supportsFeatureFlags()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$500(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    const-string v1, "CardReaderInitializer::setReaderFeatureFlags readerFeatureFlags is null"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$600(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$500(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderDispatch;->setReaderFeatureFlags(Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V

    return-void
.end method


# virtual methods
.method public logEvent(Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;)V
    .locals 2

    .line 469
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$300(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/ReaderEventLogger;->logEvent(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;)V

    return-void
.end method

.method public onAudioReaderFailedToConnect()V
    .locals 2

    .line 282
    invoke-super {p0}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onAudioReaderFailedToConnect()V

    .line 283
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onAudioReaderFailedToConnect(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 285
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderInfo;->setTimedOutDuringInit(Z)V

    .line 287
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$700(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderFactory;->destroy(Lcom/squareup/cardreader/CardReaderId;)V

    :cond_0
    return-void
.end method

.method public onCapabilitiesReceived(ZLjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrsCapability;",
            ">;)V"
        }
    .end annotation

    .line 447
    invoke-super {p0, p1, p2}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onCapabilitiesReceived(ZLjava/util/List;)V

    .line 448
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$300(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    .line 449
    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/ReaderEventLogger;->logSystemCapabilities(ZLjava/util/List;)V

    return-void
.end method

.method public onCardPresenceChange(Z)V
    .locals 1

    .line 453
    invoke-super {p0, p1}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onCardPresenceChange(Z)V

    if-eqz p1, :cond_0

    .line 456
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$1100(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/FirmwareUpdater;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/FirmwareUpdater;->pauseUpdates()V

    .line 457
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderStatusListener;->onCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V

    goto :goto_0

    .line 459
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isCardPresenceRequired()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 462
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$1100(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/FirmwareUpdater;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/FirmwareUpdater;->resumeUpdates()V

    .line 464
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderStatusListener;->onCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V

    :goto_0
    return-void
.end method

.method public onCardReaderBackendInitialized()V
    .locals 2

    .line 177
    invoke-super {p0}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onCardReaderBackendInitialized()V

    .line 178
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$100(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onCardReaderBackendInitialized(Lcom/squareup/cardreader/CardReader;)V

    return-void
.end method

.method public onChargeCycleCountReceived(I)V
    .locals 0

    .line 442
    invoke-super {p0, p1}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onChargeCycleCountReceived(I)V

    return-void
.end method

.method public onCommsRateUpdated(Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;)V
    .locals 1

    .line 240
    invoke-super {p0, p1}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onCommsRateUpdated(Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;)V

    .line 241
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$300(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/ReaderEventLogger;->logCommsRateUpdated(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onCommsVersionAcquired(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$300(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 202
    invoke-interface {v0, v1, p1, p2, p3}, Lcom/squareup/cardreader/ReaderEventLogger;->logCommsVersionAcquired(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V

    .line 205
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getCommsStatus()Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    move-result-object v0

    if-eqz v0, :cond_0

    return-void

    .line 209
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onCommsVersionAcquired(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V

    .line 211
    sget-object p2, Lcom/squareup/cardreader/CardReaderInitializer$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCommsVersionResult:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x1

    if-eq p1, p2, :cond_3

    const/4 p3, 0x2

    if-eq p1, p3, :cond_2

    const/4 p3, 0x3

    if-ne p1, p3, :cond_1

    .line 221
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object p3, Lcom/squareup/cardreader/CardReaderEventName;->APP_UPDATE_REQUIRED:Lcom/squareup/cardreader/CardReaderEventName;

    invoke-static {p1, p2, p3}, Lcom/squareup/cardreader/CardReaderInitializer;->access$400(Lcom/squareup/cardreader/CardReaderInitializer;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V

    .line 222
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {p1, p2}, Lcom/squareup/cardreader/CardReaderStatusListener;->onInitRegisterUpdateRequired(Lcom/squareup/cardreader/CardReaderInfo;)V

    goto :goto_0

    .line 225
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 226
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getCommsStatus()Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    move-result-object v0

    aput-object v0, p2, p3

    const-string p3, "Unknown CommsVersionResult: %s"

    invoke-static {p3, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 216
    :cond_2
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object p3, Lcom/squareup/cardreader/CardReaderEventName;->FW_UPDATE_REQUIRED:Lcom/squareup/cardreader/CardReaderEventName;

    invoke-static {p1, p2, p3}, Lcom/squareup/cardreader/CardReaderInitializer;->access$400(Lcom/squareup/cardreader/CardReaderInitializer;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V

    .line 217
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {p1, p2}, Lcom/squareup/cardreader/CardReaderStatusListener;->onInitFirmwareUpdateRequired(Lcom/squareup/cardreader/CardReaderInfo;)V

    goto :goto_0

    .line 213
    :cond_3
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {p1, p2}, Lcom/squareup/cardreader/CardReaderStatusListener;->onFullCommsEstablished(Lcom/squareup/cardreader/CardReaderInfo;)V

    :goto_0
    return-void
.end method

.method public onCoreDumpExists(Z)V
    .locals 2

    .line 383
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$300(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/ReaderEventLogger;->logCoreDumpResult(Lcom/squareup/cardreader/CardReaderInfo;Z)V

    if-eqz p1, :cond_0

    .line 385
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$600(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderDispatch;->retrieveCoreDump()V

    :cond_0
    return-void
.end method

.method public onCoreDumpReceived([B[B)V
    .locals 2

    .line 390
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$100(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, v1, p1, p2}, Lcom/squareup/cardreader/CardReaderStatusListener;->onCoreDump(Lcom/squareup/cardreader/CardReader;[B[B)V

    return-void
.end method

.method public onPowerStatus(Lcom/squareup/cardreader/CardReaderBatteryInfo;)V
    .locals 2

    .line 293
    invoke-super {p0, p1}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onPowerStatus(Lcom/squareup/cardreader/CardReaderBatteryInfo;)V

    .line 294
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$300(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    .line 295
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {p1, v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger;->logBatteryInfo(ILcom/squareup/cardreader/CardReaderInfo;)V

    .line 297
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderStatusListener;->onBatteryUpdate(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 298
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isBatteryDead()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 299
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$600(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderDispatch;->powerOff()V

    .line 300
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$700(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderFactory;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderFactory;->destroy(Lcom/squareup/cardreader/CardReaderId;)V

    .line 301
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderStatusListener;->onBatteryDead(Lcom/squareup/cardreader/CardReaderInfo;)V

    goto :goto_0

    .line 303
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->requestTamperStatus()V

    :goto_0
    return-void
.end method

.method public onReaderError(Lcom/squareup/cardreader/lcr/CrsReaderError;)V
    .locals 2

    .line 353
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$300(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/ReaderEventLogger;->logReaderError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrsReaderError;)V

    .line 365
    sget-object v0, Lcom/squareup/cardreader/lcr/CrsReaderError;->READER_ERROR_TOUCH_PANEL_SECURITY_EVENT:Lcom/squareup/cardreader/lcr/CrsReaderError;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "Received a touch panel security event (RA-34496)"

    .line 366
    invoke-static {v0, p1}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 367
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-void

    .line 371
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$100(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReader;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderStatusListener;->onReaderError(Lcom/squareup/cardreader/CardReader;)V

    return-void
.end method

.method public onReaderReady(Lcom/squareup/cardreader/lcr/CrCardreaderType;)V
    .locals 3

    .line 249
    invoke-super {p0, p1}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onReaderReady(Lcom/squareup/cardreader/lcr/CrCardreaderType;)V

    .line 250
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object v1, Lcom/squareup/cardreader/CardReaderEventName;->READER_READY:Lcom/squareup/cardreader/CardReaderEventName;

    invoke-static {p1, v0, v1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$400(Lcom/squareup/cardreader/CardReaderInitializer;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V

    .line 252
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getCommsStatus()Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    move-result-object p1

    if-eqz p1, :cond_4

    .line 255
    sget-object p1, Lcom/squareup/cardreader/CardReaderInitializer$1;->$SwitchMap$com$squareup$cardreader$lcr$CrCommsVersionResult:[I

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getCommsStatus()Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/lcr/CrCommsVersionResult;->ordinal()I

    move-result v0

    aget p1, p1, v0

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v1, 0x2

    if-eq p1, v1, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    goto :goto_0

    .line 274
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 275
    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->getCommsStatus()Lcom/squareup/cardreader/lcr/CrCommsVersionResult;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "Unknown CommsVersionResult: %s"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 268
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->requestTamperStatus()V

    goto :goto_0

    .line 257
    :cond_2
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->setReaderFeatureFlags()V

    .line 258
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->hasBattery()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 259
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->requestPowerStatus()V

    goto :goto_0

    .line 261
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->requestTamperStatus()V

    .line 278
    :goto_0
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onReaderReady()V

    return-void

    .line 253
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "#getCommsStatus() cannot return null in #onReaderReady()"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onSecureSessionDenied(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
    .locals 2

    .line 421
    invoke-super {p0, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onSecureSessionDenied(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    .line 422
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/squareup/cardreader/CardReaderStatusListener;->onSecureSessionDenied(Lcom/squareup/cardreader/CardReaderInfo;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V

    .line 424
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->requestSystemPropertiesAndFirmwareManifest()V

    return-void
.end method

.method public onSecureSessionError(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
    .locals 2

    .line 428
    invoke-super {p0, p1}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onSecureSessionError(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V

    .line 429
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$300(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 430
    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/ReaderEventLogger;->logSecureSessionResult(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V

    .line 431
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onSecureSessionError(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V

    .line 432
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->requestSystemPropertiesAndFirmwareManifest()V

    return-void
.end method

.method public onSecureSessionInvalid()V
    .locals 2

    .line 414
    invoke-super {p0}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onSecureSessionInvalid()V

    .line 415
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onSecureSessionInvalid(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 416
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->requestSystemPropertiesAndFirmwareManifest()V

    return-void
.end method

.method public onSecureSessionSendToServer([B)V
    .locals 2

    .line 394
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$902(Lcom/squareup/cardreader/CardReaderInitializer;Z)Z

    .line 396
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$100(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->sendSecureSessionMessageToServer(Lcom/squareup/cardreader/CardReader;[B)V

    return-void
.end method

.method public onSecureSessionValid()V
    .locals 2

    .line 400
    invoke-super {p0}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onSecureSessionValid()V

    .line 402
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onSecureSessionValid(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 404
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$1000(Lcom/squareup/cardreader/CardReaderInitializer;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$1002(Lcom/squareup/cardreader/CardReaderInitializer;Z)Z

    .line 406
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onInitializationComplete(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 408
    invoke-virtual {p0}, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->requestSystemPropertiesAndFirmwareManifest()V

    .line 409
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$600(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->checkCardPresence()V

    :cond_0
    return-void
.end method

.method public onTamperData([B)V
    .locals 3

    .line 377
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$300(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    .line 378
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/cardreader/ReaderEventLogger;->logTamperData(ILcom/squareup/cardreader/CardReaderInfo;[B)V

    .line 379
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$100(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object v1

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReader;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/CardReaderStatusListener;->onTamperData(Lcom/squareup/cardreader/CardReader;[B)V

    return-void
.end method

.method public onTamperStatus(Lcom/squareup/cardreader/lcr/CrTamperStatus;)V
    .locals 3

    .line 326
    invoke-super {p0, p1}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onTamperStatus(Lcom/squareup/cardreader/lcr/CrTamperStatus;)V

    .line 327
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$300(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    .line 328
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/cardreader/ReaderEventLogger;->logTamperResult(ILcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrTamperStatus;)V

    .line 330
    sget-object v0, Lcom/squareup/cardreader/CardReaderInitializer$1;->$SwitchMap$com$squareup$cardreader$lcr$CrTamperStatus:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrTamperStatus;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "Received a CR_TAMPER_STATUS_UNKNOWN"

    .line 346
    invoke-static {v0, p1}, Ltimber/log/Timber;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 347
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$600(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderDispatch;->requestTamperData()V

    goto :goto_0

    .line 342
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$600(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderDispatch;->requestTamperData()V

    goto :goto_0

    .line 339
    :cond_2
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->continuePostTamperInit()V

    goto :goto_0

    .line 332
    :cond_3
    invoke-direct {p0}, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->continuePostTamperInit()V

    .line 335
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$600(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object p1

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderDispatch;->requestTamperData()V

    :goto_0
    return-void
.end method

.method public onTmsCountryCode(Ljava/lang/String;)V
    .locals 1

    .line 313
    invoke-super {p0, p1}, Lcom/squareup/cardreader/CardReaderInfoUpdater;->onTmsCountryCode(Ljava/lang/String;)V

    .line 314
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$200(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderStatusListener;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderStatusListener;->onTmsCountryCode(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public onVersionInfo([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V
    .locals 1

    .line 308
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$500(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->onVersionInfo([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)V

    .line 309
    iget-object p1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {p1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$300(Lcom/squareup/cardreader/CardReaderInitializer;)Lcom/squareup/cardreader/CardReaderListeners;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/ReaderEventLogger;->logFirmwareAssetVersionInfo(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method requestSystemPropertiesAndFirmwareManifest()V
    .locals 1

    .line 437
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$600(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->requestSystemInfo()V

    .line 438
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$600(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->requestFirmwareManifest()V

    return-void
.end method

.method requestTamperStatus()V
    .locals 3

    .line 318
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$800(Lcom/squareup/cardreader/CardReaderInitializer;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 319
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    iget-object v1, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    sget-object v2, Lcom/squareup/cardreader/CardReaderEventName;->REQUEST_TAMPER_STATUS:Lcom/squareup/cardreader/CardReaderEventName;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/CardReaderInitializer;->access$400(Lcom/squareup/cardreader/CardReaderInitializer;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderEventName;)V

    .line 320
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    invoke-static {v0}, Lcom/squareup/cardreader/CardReaderInitializer;->access$600(Lcom/squareup/cardreader/CardReaderInitializer;)Ljavax/inject/Provider;

    move-result-object v0

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderDispatch;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderDispatch;->requestTamperStatus()V

    .line 321
    iget-object v0, p0, Lcom/squareup/cardreader/CardReaderInitializer$InternalListener;->this$0:Lcom/squareup/cardreader/CardReaderInitializer;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/cardreader/CardReaderInitializer;->access$802(Lcom/squareup/cardreader/CardReaderInitializer;Z)Z

    :cond_0
    return-void
.end method
