.class public final Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StartEmvPaymentInteraction"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u001e\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001BM\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\u0007\u0012\u0006\u0010\n\u001a\u00020\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u0007\u0012\u0006\u0010\u000c\u001a\u00020\u0007\u0012\u0006\u0010\r\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u000eJ\t\u0010\u001b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0007H\u00c6\u0003J\t\u0010 \u001a\u00020\u0007H\u00c6\u0003J\t\u0010!\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0007H\u00c6\u0003J\t\u0010#\u001a\u00020\u0003H\u00c6\u0003Jc\u0010$\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\u00072\u0008\u0008\u0002\u0010\n\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u00072\u0008\u0008\u0002\u0010\r\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010%\u001a\u00020&2\u0008\u0010\'\u001a\u0004\u0018\u00010(H\u00d6\u0003J\t\u0010)\u001a\u00020\u0007H\u00d6\u0001J\t\u0010*\u001a\u00020+H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\t\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\n\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0012R\u0011\u0010\u000b\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0012R\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0012R\u0011\u0010\u000c\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0012R\u0011\u0010\r\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0012\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage;",
        "amountAuthorized",
        "",
        "transactionType",
        "Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;",
        "year",
        "",
        "month",
        "day",
        "hour",
        "minute",
        "second",
        "timeMillis",
        "(JLcom/squareup/cardreader/lcr/CrPaymentTransactionType;IIIIIIJ)V",
        "getAmountAuthorized",
        "()J",
        "getDay",
        "()I",
        "getHour",
        "getMinute",
        "getMonth",
        "getSecond",
        "getTimeMillis",
        "getTransactionType",
        "()Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;",
        "getYear",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final amountAuthorized:J

.field private final day:I

.field private final hour:I

.field private final minute:I

.field private final month:I

.field private final second:I

.field private final timeMillis:J

.field private final transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

.field private final year:I


# direct methods
.method public constructor <init>(JLcom/squareup/cardreader/lcr/CrPaymentTransactionType;IIIIIIJ)V
    .locals 1

    const-string v0, "transactionType"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 110
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-wide p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->amountAuthorized:J

    iput-object p3, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    iput p4, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->year:I

    iput p5, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->month:I

    iput p6, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->day:I

    iput p7, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->hour:I

    iput p8, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->minute:I

    iput p9, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->second:I

    iput-wide p10, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->timeMillis:J

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;JLcom/squareup/cardreader/lcr/CrPaymentTransactionType;IIIIIIJILjava/lang/Object;)Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;
    .locals 13

    move-object v0, p0

    move/from16 v1, p12

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-wide v2, v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->amountAuthorized:J

    goto :goto_0

    :cond_0
    move-wide v2, p1

    :goto_0
    and-int/lit8 v4, v1, 0x2

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    goto :goto_1

    :cond_1
    move-object/from16 v4, p3

    :goto_1
    and-int/lit8 v5, v1, 0x4

    if-eqz v5, :cond_2

    iget v5, v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->year:I

    goto :goto_2

    :cond_2
    move/from16 v5, p4

    :goto_2
    and-int/lit8 v6, v1, 0x8

    if-eqz v6, :cond_3

    iget v6, v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->month:I

    goto :goto_3

    :cond_3
    move/from16 v6, p5

    :goto_3
    and-int/lit8 v7, v1, 0x10

    if-eqz v7, :cond_4

    iget v7, v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->day:I

    goto :goto_4

    :cond_4
    move/from16 v7, p6

    :goto_4
    and-int/lit8 v8, v1, 0x20

    if-eqz v8, :cond_5

    iget v8, v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->hour:I

    goto :goto_5

    :cond_5
    move/from16 v8, p7

    :goto_5
    and-int/lit8 v9, v1, 0x40

    if-eqz v9, :cond_6

    iget v9, v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->minute:I

    goto :goto_6

    :cond_6
    move/from16 v9, p8

    :goto_6
    and-int/lit16 v10, v1, 0x80

    if-eqz v10, :cond_7

    iget v10, v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->second:I

    goto :goto_7

    :cond_7
    move/from16 v10, p9

    :goto_7
    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    iget-wide v11, v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->timeMillis:J

    goto :goto_8

    :cond_8
    move-wide/from16 v11, p10

    :goto_8
    move-wide p1, v2

    move-object/from16 p3, v4

    move/from16 p4, v5

    move/from16 p5, v6

    move/from16 p6, v7

    move/from16 p7, v8

    move/from16 p8, v9

    move/from16 p9, v10

    move-wide/from16 p10, v11

    invoke-virtual/range {p0 .. p11}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->copy(JLcom/squareup/cardreader/lcr/CrPaymentTransactionType;IIIIIIJ)Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->amountAuthorized:J

    return-wide v0
.end method

.method public final component2()Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->year:I

    return v0
.end method

.method public final component4()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->month:I

    return v0
.end method

.method public final component5()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->day:I

    return v0
.end method

.method public final component6()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->hour:I

    return v0
.end method

.method public final component7()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->minute:I

    return v0
.end method

.method public final component8()I
    .locals 1

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->second:I

    return v0
.end method

.method public final component9()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->timeMillis:J

    return-wide v0
.end method

.method public final copy(JLcom/squareup/cardreader/lcr/CrPaymentTransactionType;IIIIIIJ)Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;
    .locals 13

    const-string v0, "transactionType"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;

    move-object v1, v0

    move-wide v2, p1

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move-wide/from16 v11, p10

    invoke-direct/range {v1 .. v12}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;-><init>(JLcom/squareup/cardreader/lcr/CrPaymentTransactionType;IIIIIIJ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->amountAuthorized:J

    iget-wide v2, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->amountAuthorized:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    iget-object v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->year:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->year:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->month:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->month:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->day:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->day:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->hour:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->hour:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->minute:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->minute:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->second:I

    iget v1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->second:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->timeMillis:J

    iget-wide v2, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->timeMillis:J

    cmp-long p1, v0, v2

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAmountAuthorized()J
    .locals 2

    .line 101
    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->amountAuthorized:J

    return-wide v0
.end method

.method public final getDay()I
    .locals 1

    .line 105
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->day:I

    return v0
.end method

.method public final getHour()I
    .locals 1

    .line 106
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->hour:I

    return v0
.end method

.method public final getMinute()I
    .locals 1

    .line 107
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->minute:I

    return v0
.end method

.method public final getMonth()I
    .locals 1

    .line 104
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->month:I

    return v0
.end method

.method public final getSecond()I
    .locals 1

    .line 108
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->second:I

    return v0
.end method

.method public final getTimeMillis()J
    .locals 2

    .line 109
    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->timeMillis:J

    return-wide v0
.end method

.method public final getTransactionType()Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    return-object v0
.end method

.method public final getYear()I
    .locals 1

    .line 103
    iget v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->year:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->amountAuthorized:J

    invoke-static {v0, v1}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->year:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->month:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->day:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->hour:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->minute:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->second:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->timeMillis:J

    invoke-static {v1, v2}, L$r8$java8methods$utility$Long$hashCode$IJ;->hashCode(J)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "StartEmvPaymentInteraction(amountAuthorized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->amountAuthorized:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", transactionType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->transactionType:Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", year="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->year:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", month="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->month:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", day="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->day:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", hour="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->hour:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", minute="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->minute:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", second="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->second:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", timeMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$PaymentFeatureMessage$StartEmvPaymentInteraction;->timeMillis:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
