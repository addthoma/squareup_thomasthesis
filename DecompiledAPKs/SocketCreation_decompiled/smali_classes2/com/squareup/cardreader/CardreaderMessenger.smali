.class public interface abstract Lcom/squareup/cardreader/CardreaderMessenger;
.super Ljava/lang/Object;
.source "CardreaderMessengerInterface.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/CardreaderMessenger$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH&J\u0016\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00032\u0006\u0010\u000c\u001a\u00020\u000bH\u0016J\u0016\u0010\r\u001a\u00020\u000e2\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0004H&R\u001e\u0010\u0002\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00050\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/cardreader/CardreaderMessenger;",
        "",
        "responses",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/cardreader/ReaderPayload;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;",
        "getResponses",
        "()Lio/reactivex/Observable;",
        "asSingleCardreaderMessenger",
        "Lcom/squareup/cardreader/SingleCardreaderMessenger;",
        "connectionId",
        "Lcom/squareup/cardreader/CardreaderConnectionId;",
        "cardreaderId",
        "send",
        "",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract asSingleCardreaderMessenger(Lcom/squareup/cardreader/CardreaderConnectionId;)Lcom/squareup/cardreader/SingleCardreaderMessenger;
.end method

.method public abstract getResponses()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/cardreader/ReaderPayload<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract responses(Lcom/squareup/cardreader/CardreaderConnectionId;)Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/CardreaderConnectionId;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;",
            ">;"
        }
    .end annotation
.end method

.method public abstract send(Lcom/squareup/cardreader/ReaderPayload;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ReaderPayload<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderInput;",
            ">;)V"
        }
    .end annotation
.end method
