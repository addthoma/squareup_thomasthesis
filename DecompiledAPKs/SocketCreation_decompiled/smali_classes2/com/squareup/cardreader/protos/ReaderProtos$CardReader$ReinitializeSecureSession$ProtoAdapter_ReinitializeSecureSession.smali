.class final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$ProtoAdapter_ReinitializeSecureSession;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReinitializeSecureSession"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 929
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 944
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$Builder;-><init>()V

    .line 945
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 946
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 949
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 953
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 954
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 927
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$ProtoAdapter_ReinitializeSecureSession;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 939
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 927
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$ProtoAdapter_ReinitializeSecureSession;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;)I
    .locals 0

    .line 934
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 927
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$ProtoAdapter_ReinitializeSecureSession;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;
    .locals 0

    .line 959
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$Builder;

    move-result-object p1

    .line 960
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 961
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 927
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession$ProtoAdapter_ReinitializeSecureSession;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ReinitializeSecureSession;

    move-result-object p1

    return-object p1
.end method
