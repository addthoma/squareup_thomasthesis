.class final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer$ProtoAdapter_ProcessSecureSessionMessageFromServer;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ProcessSecureSessionMessageFromServer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 836
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 854
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer$Builder;-><init>()V

    .line 855
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 856
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 860
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 858
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer$Builder;->payload(Lokio/ByteString;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer$Builder;

    goto :goto_0

    .line 864
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 865
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 834
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer$ProtoAdapter_ProcessSecureSessionMessageFromServer;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 848
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;->payload:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 849
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 834
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer$ProtoAdapter_ProcessSecureSessionMessageFromServer;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;)I
    .locals 3

    .line 841
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;->payload:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 842
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 834
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer$ProtoAdapter_ProcessSecureSessionMessageFromServer;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;
    .locals 0

    .line 871
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer$Builder;

    move-result-object p1

    .line 872
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 873
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 834
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer$ProtoAdapter_ProcessSecureSessionMessageFromServer;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessSecureSessionMessageFromServer;

    move-result-object p1

    return-object p1
.end method
