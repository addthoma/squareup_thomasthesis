.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public currency_code:Ljava/lang/Integer;

.field public mcc:Ljava/lang/Integer;

.field public reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 217
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;
    .locals 5

    .line 237
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->mcc:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->currency_code:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 210
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures;

    move-result-object v0

    return-object v0
.end method

.method public currency_code(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;
    .locals 0

    .line 226
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->currency_code:Ljava/lang/Integer;

    return-object p0
.end method

.method public mcc(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;
    .locals 0

    .line 221
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->mcc:Ljava/lang/Integer;

    return-object p0
.end method

.method public reader_feature_flags(Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;
    .locals 0

    .line 231
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$InitializeCardReaderFeatures$Builder;->reader_feature_flags:Lcom/squareup/cardreader/protos/ReaderProtos$ReaderFeatureFlags;

    return-object p0
.end method
