.class final Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$ProtoAdapter_ProcessMessageFromReader;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ProcessMessageFromReader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 7760
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7829
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;-><init>()V

    .line 7830
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 7831
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_10

    const/4 v4, 0x1

    if-eq v3, v4, :cond_f

    const/4 v4, 0x3

    if-eq v3, v4, :cond_e

    const/16 v4, 0xd

    if-eq v3, v4, :cond_d

    const/16 v4, 0x25

    if-eq v3, v4, :cond_c

    const/16 v4, 0x32

    if-eq v3, v4, :cond_b

    const/16 v4, 0x36

    if-eq v3, v4, :cond_a

    const/16 v4, 0x38

    if-eq v3, v4, :cond_9

    const/16 v4, 0x41

    if-eq v3, v4, :cond_8

    const/16 v4, 0x4c

    if-eq v3, v4, :cond_7

    const/16 v4, 0x5d

    if-eq v3, v4, :cond_6

    const/16 v4, 0xf

    if-eq v3, v4, :cond_5

    const/16 v4, 0x10

    if-eq v3, v4, :cond_4

    const/16 v4, 0x16

    if-eq v3, v4, :cond_3

    const/16 v4, 0x17

    if-eq v3, v4, :cond_2

    const/16 v4, 0x2c

    if-eq v3, v4, :cond_1

    const/16 v4, 0x2d

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    packed-switch v3, :pswitch_data_1

    .line 7861
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 7858
    :pswitch_0
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_started(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto :goto_0

    .line 7857
    :pswitch_1
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_complete(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto :goto_0

    .line 7856
    :pswitch_2
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_asset_success(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto :goto_0

    .line 7855
    :pswitch_3
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_valid(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto :goto_0

    .line 7854
    :pswitch_4
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_initialization_complete(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7840
    :pswitch_5
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_tamper_data(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7839
    :pswitch_6
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_battery_dead(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7838
    :pswitch_7
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_full_comms_established(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7837
    :pswitch_8
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_init_register_update_required(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7836
    :pswitch_9
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_init_firmware_update_required(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7835
    :pswitch_a
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_reader_failed_to_connect(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7848
    :cond_0
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->initializing_secure_session(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7847
    :cond_1
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->send_firmware_manifest_to_server(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7845
    :cond_2
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_removed(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7844
    :cond_3
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_inserted(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7843
    :cond_4
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_denied(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7842
    :cond_5
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_invalid(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7859
    :cond_6
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_aborted(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7853
    :cond_7
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_core_dump(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7852
    :cond_8
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_libraries_failed_to_load(Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7851
    :cond_9
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_cardreader_backend_initialized(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7850
    :cond_a
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_error(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7849
    :cond_b
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_device_unsupported(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7846
    :cond_c
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_reader_info_changed(Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7841
    :cond_d
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->send_secure_session_message_to_server(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7834
    :cond_e
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_error(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7833
    :cond_f
    sget-object v3, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_progress(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    goto/16 :goto_0

    .line 7865
    :cond_10
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 7866
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x56
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7758
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$ProtoAdapter_ProcessMessageFromReader;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7797
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    const/16 v2, 0x2c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7798
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    const/16 v2, 0x5a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7799
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7800
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    const/16 v2, 0x58

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7801
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    const/16 v2, 0x59

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7802
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7803
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    const/16 v2, 0x38

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7804
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    const/16 v2, 0x32

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7805
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7806
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7807
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7808
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7809
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7810
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7811
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    const/16 v2, 0x4c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7812
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    const/16 v2, 0x2d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7813
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7814
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    const/16 v2, 0x5d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7815
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7816
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7817
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    const/16 v2, 0x36

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7818
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    const/16 v2, 0x57

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7819
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    const/16 v2, 0x56

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7820
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7821
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7822
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    const/16 v2, 0x25

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7823
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    const/16 v2, 0x41

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 7824
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 7758
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$ProtoAdapter_ProcessMessageFromReader;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;)I
    .locals 4

    .line 7765
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    const/16 v2, 0x2c

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    const/16 v3, 0x5a

    .line 7766
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    const/4 v3, 0x1

    .line 7767
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    const/16 v3, 0x58

    .line 7768
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    const/16 v3, 0x59

    .line 7769
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    const/4 v3, 0x3

    .line 7770
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    const/16 v3, 0x38

    .line 7771
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    const/16 v3, 0x32

    .line 7772
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    const/4 v3, 0x6

    .line 7773
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    const/4 v3, 0x7

    .line 7774
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    const/16 v3, 0x8

    .line 7775
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    const/16 v3, 0x9

    .line 7776
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    const/16 v3, 0xa

    .line 7777
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    const/16 v3, 0xb

    .line 7778
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    const/16 v3, 0x4c

    .line 7779
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    const/16 v3, 0x2d

    .line 7780
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    const/16 v3, 0xd

    .line 7781
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    const/16 v3, 0x5d

    .line 7782
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    const/16 v3, 0xf

    .line 7783
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    const/16 v3, 0x10

    .line 7784
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    const/16 v3, 0x36

    .line 7785
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    const/16 v3, 0x57

    .line 7786
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    const/16 v3, 0x56

    .line 7787
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    const/16 v3, 0x16

    .line 7788
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    const/16 v3, 0x17

    .line 7789
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    const/16 v3, 0x25

    .line 7790
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    const/16 v3, 0x41

    .line 7791
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7792
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 7758
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$ProtoAdapter_ProcessMessageFromReader;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;
    .locals 2

    .line 7871
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;

    move-result-object p1

    .line 7872
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    .line 7873
    :cond_0
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    .line 7874
    :cond_1
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    .line 7875
    :cond_2
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    .line 7876
    :cond_3
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    .line 7877
    :cond_4
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    .line 7878
    :cond_5
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    .line 7879
    :cond_6
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    .line 7880
    :cond_7
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    .line 7881
    :cond_8
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    .line 7882
    :cond_9
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    .line 7883
    :cond_a
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    .line 7884
    :cond_b
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    .line 7885
    :cond_c
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    .line 7886
    :cond_d
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    .line 7887
    :cond_e
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    .line 7888
    :cond_f
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    if-eqz v0, :cond_10

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    .line 7889
    :cond_10
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    if-eqz v0, :cond_11

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    .line 7890
    :cond_11
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    if-eqz v0, :cond_12

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    .line 7891
    :cond_12
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    if-eqz v0, :cond_13

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    .line 7892
    :cond_13
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    if-eqz v0, :cond_14

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    .line 7893
    :cond_14
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    if-eqz v0, :cond_15

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    .line 7894
    :cond_15
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    if-eqz v0, :cond_16

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    .line 7895
    :cond_16
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    if-eqz v0, :cond_17

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    .line 7896
    :cond_17
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    if-eqz v0, :cond_18

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    .line 7897
    :cond_18
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    if-eqz v0, :cond_19

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    .line 7898
    :cond_19
    iget-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    if-eqz v0, :cond_1a

    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    iput-object v0, p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    .line 7899
    :cond_1a
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 7900
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 7758
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$ProtoAdapter_ProcessMessageFromReader;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;

    move-result-object p1

    return-object p1
.end method
