.class final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent$ProtoAdapter_OnCoreDumpDataSent;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_OnCoreDumpDataSent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 606
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 621
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent$Builder;-><init>()V

    .line 622
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 623
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 626
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 630
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 631
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 604
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent$ProtoAdapter_OnCoreDumpDataSent;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 616
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 604
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent$ProtoAdapter_OnCoreDumpDataSent;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;)I
    .locals 0

    .line 611
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 604
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent$ProtoAdapter_OnCoreDumpDataSent;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;
    .locals 0

    .line 636
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent$Builder;

    move-result-object p1

    .line 637
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 638
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 604
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent$ProtoAdapter_OnCoreDumpDataSent;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$OnCoreDumpDataSent;

    move-result-object p1

    return-object p1
.end method
