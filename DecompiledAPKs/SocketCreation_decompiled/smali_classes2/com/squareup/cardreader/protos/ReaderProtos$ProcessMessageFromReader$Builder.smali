.class public final Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

.field public on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

.field public on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

.field public on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

.field public on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

.field public on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

.field public on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

.field public on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

.field public on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

.field public on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

.field public on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

.field public on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

.field public on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

.field public on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

.field public on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

.field public on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

.field public on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

.field public on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

.field public on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

.field public on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

.field public on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

.field public on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

.field public on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

.field public on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

.field public on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

.field public send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

.field public send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7592
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;
    .locals 2

    .line 7754
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;-><init>(Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 7537
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader;

    move-result-object v0

    return-object v0
.end method

.method public initializing_secure_session(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7684
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->initializing_secure_session:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$InitializingSecureSession;

    return-object p0
.end method

.method public on_battery_dead(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7668
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_battery_dead:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnBatteryDead;

    return-object p0
.end method

.method public on_card_inserted(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7731
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_inserted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardInserted;

    return-object p0
.end method

.method public on_card_reader_info_changed(Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7742
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_reader_info_changed:Lcom/squareup/cardreader/protos/ReaderProtos$OnCardReaderInfoChanged;

    return-object p0
.end method

.method public on_card_removed(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7736
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_card_removed:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardRemoved;

    return-object p0
.end method

.method public on_cardreader_backend_initialized(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7633
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_cardreader_backend_initialized:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCardReaderBackendInitialized;

    return-object p0
.end method

.method public on_core_dump(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7678
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_core_dump:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnCoreDump;

    return-object p0
.end method

.method public on_device_unsupported(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7639
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_device_unsupported:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnDeviceUnsupported;

    return-object p0
.end method

.method public on_firmware_update_asset_success(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7615
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_asset_success:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateAssetSuccess;

    return-object p0
.end method

.method public on_firmware_update_complete(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7621
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_complete:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateComplete;

    return-object p0
.end method

.method public on_firmware_update_error(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7627
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_error:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateError;

    return-object p0
.end method

.method public on_firmware_update_progress(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7609
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_progress:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateProgress;

    return-object p0
.end method

.method public on_firmware_update_started(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7603
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_firmware_update_started:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$OnFirmwareUpdateStarted;

    return-object p0
.end method

.method public on_full_comms_established(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7663
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_full_comms_established:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnFullCommsEstablished;

    return-object p0
.end method

.method public on_init_firmware_update_required(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7651
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_init_firmware_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitFirmwareUpdateRequired;

    return-object p0
.end method

.method public on_init_register_update_required(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7657
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_init_register_update_required:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitRegisterUpdateRequired;

    return-object p0
.end method

.method public on_initialization_complete(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7726
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_initialization_complete:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnInitializationComplete;

    return-object p0
.end method

.method public on_libraries_failed_to_load(Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7748
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_libraries_failed_to_load:Lcom/squareup/cardreader/protos/ReaderProtos$LibraryLoadErrorListener$OnLibrariesFailedToLoad;

    return-object p0
.end method

.method public on_reader_failed_to_connect(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7645
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_reader_failed_to_connect:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnReaderFailedToConnect;

    return-object p0
.end method

.method public on_secure_session_aborted(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7696
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_aborted:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionAborted;

    return-object p0
.end method

.method public on_secure_session_denied(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7708
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_denied:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionDenied;

    return-object p0
.end method

.method public on_secure_session_error(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7714
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_error:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionError;

    return-object p0
.end method

.method public on_secure_session_invalid(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7702
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_invalid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionInvalid;

    return-object p0
.end method

.method public on_secure_session_valid(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7720
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_secure_session_valid:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnSecureSessionValid;

    return-object p0
.end method

.method public on_tamper_data(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7673
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->on_tamper_data:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$OnTamperData;

    return-object p0
.end method

.method public send_firmware_manifest_to_server(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7597
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->send_firmware_manifest_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    return-object p0
.end method

.method public send_secure_session_message_to_server(Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;)Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;
    .locals 0

    .line 7690
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$ProcessMessageFromReader$Builder;->send_secure_session_message_to_server:Lcom/squareup/cardreader/protos/ReaderProtos$PresenterListener$SendSecureSessionMessageToServer;

    return-object p0
.end method
