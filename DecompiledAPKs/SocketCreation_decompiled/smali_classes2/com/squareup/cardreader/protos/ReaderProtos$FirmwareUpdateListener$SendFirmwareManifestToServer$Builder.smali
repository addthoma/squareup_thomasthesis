.class public final Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public libcardreader_comms_version:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

.field public manifest:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 2866
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;
    .locals 4

    .line 2882
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;->manifest:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;->libcardreader_comms_version:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;-><init>(Lokio/ByteString;Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2861
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer;

    move-result-object v0

    return-object v0
.end method

.method public libcardreader_comms_version(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;
    .locals 0

    .line 2876
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;->libcardreader_comms_version:Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    return-object p0
.end method

.method public manifest(Lokio/ByteString;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;
    .locals 0

    .line 2870
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$SendFirmwareManifestToServer$Builder;->manifest:Lokio/ByteString;

    return-object p0
.end method
