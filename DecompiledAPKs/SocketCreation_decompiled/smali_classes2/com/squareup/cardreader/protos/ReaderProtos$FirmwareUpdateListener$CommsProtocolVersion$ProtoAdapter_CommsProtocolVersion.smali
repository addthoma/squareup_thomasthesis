.class final Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$ProtoAdapter_CommsProtocolVersion;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CommsProtocolVersion"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2745
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2766
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;-><init>()V

    .line 2767
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2768
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 2774
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2772
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;->ep(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;

    goto :goto_0

    .line 2771
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;->app(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;

    goto :goto_0

    .line 2770
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;->transport(Ljava/lang/Integer;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;

    goto :goto_0

    .line 2778
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2779
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2743
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$ProtoAdapter_CommsProtocolVersion;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2758
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->transport:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2759
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->app:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2760
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->ep:Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2761
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2743
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$ProtoAdapter_CommsProtocolVersion;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;)I
    .locals 4

    .line 2750
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->transport:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->app:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 2751
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->ep:Ljava/lang/Integer;

    const/4 v3, 0x3

    .line 2752
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2753
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2743
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$ProtoAdapter_CommsProtocolVersion;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;
    .locals 0

    .line 2784
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;

    move-result-object p1

    .line 2785
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2786
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2743
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion$ProtoAdapter_CommsProtocolVersion;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;)Lcom/squareup/cardreader/protos/ReaderProtos$FirmwareUpdateListener$CommsProtocolVersion;

    move-result-object p1

    return-object p1
.end method
