.class public final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public update_response:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 702
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;
    .locals 3

    .line 712
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    iget-object v1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse$Builder;->update_response:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;-><init>(Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 699
    invoke-virtual {p0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse;

    move-result-object v0

    return-object v0
.end method

.method public update_response(Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse$Builder;
    .locals 0

    .line 706
    iput-object p1, p0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$ProcessFirmwareUpdateResponse$Builder;->update_response:Lcom/squareup/cardreader/protos/ReaderProtos$AssetUpdateResponse;

    return-object p0
.end method
