.class public final enum Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;
.super Ljava/lang/Enum;
.source "ReaderProtos.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BatteryMode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode$ProtoAdapter_BatteryMode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CHARGED:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

.field public static final enum CHARGING:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

.field public static final enum DISCHARGING:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

.field public static final enum LOW_CRITICAL:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 5925
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    const/4 v1, 0x0

    const-string v2, "DISCHARGING"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->DISCHARGING:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    .line 5927
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    const/4 v2, 0x1

    const-string v3, "CHARGING"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->CHARGING:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    .line 5929
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    const/4 v3, 0x2

    const-string v4, "CHARGED"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->CHARGED:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    .line 5931
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    const/4 v4, 0x3

    const-string v5, "LOW_CRITICAL"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->LOW_CRITICAL:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    .line 5924
    sget-object v5, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->DISCHARGING:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->CHARGING:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->CHARGED:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->LOW_CRITICAL:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    .line 5933
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode$ProtoAdapter_BatteryMode;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode$ProtoAdapter_BatteryMode;-><init>()V

    sput-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 5937
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 5938
    iput p3, p0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 5949
    :cond_0
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->LOW_CRITICAL:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    return-object p0

    .line 5948
    :cond_1
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->CHARGED:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    return-object p0

    .line 5947
    :cond_2
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->CHARGING:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    return-object p0

    .line 5946
    :cond_3
    sget-object p0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->DISCHARGING:Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;
    .locals 1

    .line 5924
    const-class v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;
    .locals 1

    .line 5924
    sget-object v0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->$VALUES:[Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 5956
    iget v0, p0, Lcom/squareup/cardreader/protos/ReaderProtos$BatteryMode;->value:I

    return v0
.end method
