.class final Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$ProtoAdapter_Reset;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ReaderProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Reset"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 342
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 357
    new-instance v0, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$Builder;-><init>()V

    .line 358
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 359
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 362
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 366
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 367
    invoke-virtual {v0}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 340
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$ProtoAdapter_Reset;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 352
    invoke-virtual {p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 340
    check-cast p2, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$ProtoAdapter_Reset;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;)I
    .locals 0

    .line 347
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 340
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$ProtoAdapter_Reset;->encodedSize(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;
    .locals 0

    .line 372
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;->newBuilder()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$Builder;

    move-result-object p1

    .line 373
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 374
    invoke-virtual {p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$Builder;->build()Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 340
    check-cast p1, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset$ProtoAdapter_Reset;->redact(Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;)Lcom/squareup/cardreader/protos/ReaderProtos$CardReader$Reset;

    move-result-object p1

    return-object p1
.end method
