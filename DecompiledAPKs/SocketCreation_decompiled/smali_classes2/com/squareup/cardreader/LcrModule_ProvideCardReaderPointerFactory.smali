.class public final Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;
.super Ljava/lang/Object;
.source "LcrModule_ProvideCardReaderPointerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/CardReaderPointer;",
        ">;"
    }
.end annotation


# instance fields
.field private final serviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFeatureLegacy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFeatureLegacy;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;->serviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFeatureLegacy;",
            ">;)",
            "Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCardReaderPointer(Lcom/squareup/cardreader/CardReaderFeatureLegacy;)Lcom/squareup/cardreader/CardReaderPointer;
    .locals 0

    .line 37
    invoke-static {p0}, Lcom/squareup/cardreader/LcrModule;->provideCardReaderPointer(Lcom/squareup/cardreader/CardReaderFeatureLegacy;)Lcom/squareup/cardreader/CardReaderPointer;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/CardReaderPointer;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;->serviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderFeatureLegacy;

    invoke-static {v0}, Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;->provideCardReaderPointer(Lcom/squareup/cardreader/CardReaderFeatureLegacy;)Lcom/squareup/cardreader/CardReaderPointer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/LcrModule_ProvideCardReaderPointerFactory;->get()Lcom/squareup/cardreader/CardReaderPointer;

    move-result-object v0

    return-object v0
.end method
