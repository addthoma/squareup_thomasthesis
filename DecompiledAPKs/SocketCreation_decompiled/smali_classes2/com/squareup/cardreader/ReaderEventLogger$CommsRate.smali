.class public Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;
.super Ljava/lang/Object;
.source "ReaderEventLogger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderEventLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CommsRate"
.end annotation


# instance fields
.field public final inCommsRate:Lcom/squareup/cardreader/lcr/CrCommsRate;

.field public final inCommsRateValue:Ljava/lang/String;

.field public final outCommsRate:Lcom/squareup/cardreader/lcr/CrCommsRate;

.field public final outCommsRateValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/lcr/CrCommsRate;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrCommsRate;Ljava/lang/String;)V
    .locals 0

    .line 296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297
    iput-object p1, p0, Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;->inCommsRate:Lcom/squareup/cardreader/lcr/CrCommsRate;

    .line 298
    iput-object p2, p0, Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;->inCommsRateValue:Ljava/lang/String;

    .line 299
    iput-object p3, p0, Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;->outCommsRate:Lcom/squareup/cardreader/lcr/CrCommsRate;

    .line 300
    iput-object p4, p0, Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;->outCommsRateValue:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .line 304
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Comms rate in: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;->inCommsRate:Lcom/squareup/cardreader/lcr/CrCommsRate;

    invoke-static {v1}, Lcom/squareup/cardreader/CommsRateUtils;->getShortName(Lcom/squareup/cardreader/lcr/CrCommsRate;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", out: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;->outCommsRate:Lcom/squareup/cardreader/lcr/CrCommsRate;

    invoke-static {v1}, Lcom/squareup/cardreader/CommsRateUtils;->getShortName(Lcom/squareup/cardreader/lcr/CrCommsRate;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
