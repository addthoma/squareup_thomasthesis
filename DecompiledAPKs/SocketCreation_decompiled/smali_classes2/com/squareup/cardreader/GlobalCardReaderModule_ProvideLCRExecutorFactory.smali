.class public final Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideLCRExecutorFactory;
.super Ljava/lang/Object;
.source "GlobalCardReaderModule_ProvideLCRExecutorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideLCRExecutorFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/concurrent/ExecutorService;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideLCRExecutorFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideLCRExecutorFactory$InstanceHolder;->access$000()Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideLCRExecutorFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideLCRExecutor()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/cardreader/GlobalCardReaderModule;->provideLCRExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideLCRExecutorFactory;->get()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/cardreader/GlobalCardReaderModule_ProvideLCRExecutorFactory;->provideLCRExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method
