.class public Lcom/squareup/cardreader/SavedCardReader;
.super Ljava/lang/Object;
.source "SavedCardReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/SavedCardReader$SavedCardReaderStore;,
        Lcom/squareup/cardreader/SavedCardReader$Builder;
    }
.end annotation


# instance fields
.field public final firmwareVersion:Ljava/lang/String;

.field public final hardwareSerialNumber:Ljava/lang/String;

.field public final isBluetoothClassic:Z

.field public final lastConnectionSuccessUtcMillis:Ljava/lang/Long;

.field public final macAddress:Ljava/lang/String;

.field public final name:Ljava/lang/String;

.field public final serialNumberLast4:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/SavedCardReader$Builder;)V
    .locals 1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->access$000(Lcom/squareup/cardreader/SavedCardReader$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/SavedCardReader;->name:Ljava/lang/String;

    .line 38
    invoke-static {p1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->access$100(Lcom/squareup/cardreader/SavedCardReader$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/SavedCardReader;->serialNumberLast4:Ljava/lang/String;

    .line 39
    invoke-static {p1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->access$200(Lcom/squareup/cardreader/SavedCardReader$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/SavedCardReader;->macAddress:Ljava/lang/String;

    .line 40
    invoke-static {p1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->access$300(Lcom/squareup/cardreader/SavedCardReader$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/cardreader/SavedCardReader;->isBluetoothClassic:Z

    .line 41
    invoke-static {p1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->access$400(Lcom/squareup/cardreader/SavedCardReader$Builder;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/SavedCardReader;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    .line 42
    invoke-static {p1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->access$500(Lcom/squareup/cardreader/SavedCardReader$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/cardreader/SavedCardReader;->firmwareVersion:Ljava/lang/String;

    .line 43
    invoke-static {p1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->access$600(Lcom/squareup/cardreader/SavedCardReader$Builder;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/SavedCardReader;->hardwareSerialNumber:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/SavedCardReader$Builder;Lcom/squareup/cardreader/SavedCardReader$1;)V
    .locals 0

    .line 11
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/SavedCardReader;-><init>(Lcom/squareup/cardreader/SavedCardReader$Builder;)V

    return-void
.end method


# virtual methods
.method public buildUpon()Lcom/squareup/cardreader/SavedCardReader$Builder;
    .locals 2

    .line 47
    new-instance v0, Lcom/squareup/cardreader/SavedCardReader$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/SavedCardReader$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/cardreader/SavedCardReader;->name:Ljava/lang/String;

    .line 48
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->name(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/SavedCardReader;->serialNumberLast4:Ljava/lang/String;

    .line 49
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->serialNumberLast4(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/SavedCardReader;->macAddress:Ljava/lang/String;

    .line 50
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->macAddress(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/squareup/cardreader/SavedCardReader;->isBluetoothClassic:Z

    .line 51
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->isBluetoothClassic(Z)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/SavedCardReader;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    .line 52
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->lastConnectionSuccessUtcMillis(Ljava/lang/Long;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/SavedCardReader;->firmwareVersion:Ljava/lang/String;

    .line 53
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->firmwareVersion(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/SavedCardReader;->hardwareSerialNumber:Ljava/lang/String;

    .line 54
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/SavedCardReader$Builder;->hardwareSerialNumber(Ljava/lang/String;)Lcom/squareup/cardreader/SavedCardReader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SavedCardReader{name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/SavedCardReader;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', serialNumberLast4=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/SavedCardReader;->serialNumberLast4:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', macAddress=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/SavedCardReader;->macAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\', isBluetoothClassic=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/cardreader/SavedCardReader;->isBluetoothClassic:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, "\', lastConnectionSuccessUtcMillis=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/SavedCardReader;->lastConnectionSuccessUtcMillis:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\', firmwareVersion=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/SavedCardReader;->firmwareVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
