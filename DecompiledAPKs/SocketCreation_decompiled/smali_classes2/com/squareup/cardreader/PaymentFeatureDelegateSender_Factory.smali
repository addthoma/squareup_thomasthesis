.class public final Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;
.super Ljava/lang/Object;
.source "PaymentFeatureDelegateSender_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/PaymentFeatureDelegateSender;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;"
        }
    .end annotation
.end field

.field private final realCardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;->cardReaderIdProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/RealCardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderId;",
            ">;)",
            "Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/PaymentFeatureDelegateSender;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/cardreader/PaymentFeatureDelegateSender;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/PaymentFeatureDelegateSender;-><init>(Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/cardreader/CardReaderId;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/PaymentFeatureDelegateSender;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;->realCardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/RealCardReaderListeners;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;->cardReaderIdProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderId;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;->newInstance(Lcom/squareup/cardreader/RealCardReaderListeners;Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/PaymentFeatureDelegateSender;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/cardreader/PaymentFeatureDelegateSender_Factory;->get()Lcom/squareup/cardreader/PaymentFeatureDelegateSender;

    move-result-object v0

    return-object v0
.end method
