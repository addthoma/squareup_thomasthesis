.class final enum Lcom/squareup/cardreader/DippedCardTracker$CardState;
.super Ljava/lang/Enum;
.source "DippedCardTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/DippedCardTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "CardState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/DippedCardTracker$CardState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/DippedCardTracker$CardState;

.field public static final enum INSERTED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

.field public static final enum MUST_BE_REMOVED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

.field public static final enum REMOVED:Lcom/squareup/cardreader/DippedCardTracker$CardState;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 110
    new-instance v0, Lcom/squareup/cardreader/DippedCardTracker$CardState;

    const/4 v1, 0x0

    const-string v2, "REMOVED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/DippedCardTracker$CardState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/DippedCardTracker$CardState;->REMOVED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

    new-instance v0, Lcom/squareup/cardreader/DippedCardTracker$CardState;

    const/4 v2, 0x1

    const-string v3, "INSERTED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/DippedCardTracker$CardState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/DippedCardTracker$CardState;->INSERTED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

    new-instance v0, Lcom/squareup/cardreader/DippedCardTracker$CardState;

    const/4 v3, 0x2

    const-string v4, "MUST_BE_REMOVED"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/DippedCardTracker$CardState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/DippedCardTracker$CardState;->MUST_BE_REMOVED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/cardreader/DippedCardTracker$CardState;

    .line 109
    sget-object v4, Lcom/squareup/cardreader/DippedCardTracker$CardState;->REMOVED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/cardreader/DippedCardTracker$CardState;->INSERTED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/DippedCardTracker$CardState;->MUST_BE_REMOVED:Lcom/squareup/cardreader/DippedCardTracker$CardState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/cardreader/DippedCardTracker$CardState;->$VALUES:[Lcom/squareup/cardreader/DippedCardTracker$CardState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 109
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/DippedCardTracker$CardState;
    .locals 1

    .line 109
    const-class v0, Lcom/squareup/cardreader/DippedCardTracker$CardState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/DippedCardTracker$CardState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/DippedCardTracker$CardState;
    .locals 1

    .line 109
    sget-object v0, Lcom/squareup/cardreader/DippedCardTracker$CardState;->$VALUES:[Lcom/squareup/cardreader/DippedCardTracker$CardState;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/DippedCardTracker$CardState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/DippedCardTracker$CardState;

    return-object v0
.end method
