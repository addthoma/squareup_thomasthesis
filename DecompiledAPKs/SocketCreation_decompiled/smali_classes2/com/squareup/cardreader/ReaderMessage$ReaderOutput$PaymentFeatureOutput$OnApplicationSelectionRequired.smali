.class public final Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;
.super Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OnApplicationSelectionRequired"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0013\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0007J\u001e\u0010\n\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001\u00a2\u0006\u0002\u0010\u000bJ\u0013\u0010\u000c\u001a\u00020\r2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u00d6\u0003J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001R\u0019\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\n\n\u0002\u0010\u0008\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;",
        "applications",
        "",
        "Lcom/squareup/cardreader/EmvApplication;",
        "([Lcom/squareup/cardreader/EmvApplication;)V",
        "getApplications",
        "()[Lcom/squareup/cardreader/EmvApplication;",
        "[Lcom/squareup/cardreader/EmvApplication;",
        "component1",
        "copy",
        "([Lcom/squareup/cardreader/EmvApplication;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final applications:[Lcom/squareup/cardreader/EmvApplication;


# direct methods
.method public constructor <init>([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 1

    const-string v0, "applications"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 356
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;->applications:[Lcom/squareup/cardreader/EmvApplication;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;[Lcom/squareup/cardreader/EmvApplication;ILjava/lang/Object;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;->applications:[Lcom/squareup/cardreader/EmvApplication;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;->copy([Lcom/squareup/cardreader/EmvApplication;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()[Lcom/squareup/cardreader/EmvApplication;
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;->applications:[Lcom/squareup/cardreader/EmvApplication;

    return-object v0
.end method

.method public final copy([Lcom/squareup/cardreader/EmvApplication;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;
    .locals 1

    const-string v0, "applications"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;-><init>([Lcom/squareup/cardreader/EmvApplication;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;->applications:[Lcom/squareup/cardreader/EmvApplication;

    iget-object p1, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;->applications:[Lcom/squareup/cardreader/EmvApplication;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getApplications()[Lcom/squareup/cardreader/EmvApplication;
    .locals 1

    .line 355
    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;->applications:[Lcom/squareup/cardreader/EmvApplication;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;->applications:[Lcom/squareup/cardreader/EmvApplication;

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OnApplicationSelectionRequired(applications="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$OnApplicationSelectionRequired;->applications:[Lcom/squareup/cardreader/EmvApplication;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
