.class public final Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;
.super Ljava/lang/Object;
.source "LcrModule_ProvidePowerFeatureFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/PowerFeatureLegacy;",
        ">;"
    }
.end annotation


# instance fields
.field private final powerFeatureNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionPointerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;->sessionPointerProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;->powerFeatureNativeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;",
            ">;)",
            "Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providePowerFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;)Lcom/squareup/cardreader/PowerFeatureLegacy;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;",
            ")",
            "Lcom/squareup/cardreader/PowerFeatureLegacy;"
        }
    .end annotation

    .line 41
    invoke-static {p0, p1}, Lcom/squareup/cardreader/LcrModule;->providePowerFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;)Lcom/squareup/cardreader/PowerFeatureLegacy;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/PowerFeatureLegacy;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/PowerFeatureLegacy;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;->sessionPointerProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;->powerFeatureNativeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;

    invoke-static {v0, v1}, Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;->providePowerFeature(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;)Lcom/squareup/cardreader/PowerFeatureLegacy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/cardreader/LcrModule_ProvidePowerFeatureFactory;->get()Lcom/squareup/cardreader/PowerFeatureLegacy;

    move-result-object v0

    return-object v0
.end method
