.class Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;
.super Ljava/lang/Object;
.source "FirmwareUpdateFeatureLegacy.java"

# interfaces
.implements Lcom/squareup/cardreader/FirmwareUpdateFeature;


# instance fields
.field private apiListener:Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

.field private final cardReaderConstants:Lcom/squareup/cardreader/CardReaderConstants;

.field private final cardReaderPointer:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private feature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

.field private final firmwareUpdateFeatureNative:Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderConstants;Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderConstants;",
            "Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->cardReaderPointer:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->cardReaderConstants:Lcom/squareup/cardreader/CardReaderConstants;

    .line 32
    iput-object p3, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->firmwareUpdateFeatureNative:Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;

    return-void
.end method


# virtual methods
.method public initialize(Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->feature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    .line 46
    iput-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    .line 48
    iget-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->firmwareUpdateFeatureNative:Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;

    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->cardReaderPointer:Ljavax/inject/Provider;

    .line 49
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    .line 48
    invoke-interface {p1, v0, p0}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;->firmware_update_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->feature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    return-void

    .line 45
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "apiListener cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 42
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "FirmwareUpdateFeature is already initialized!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onFirmwareUpdateTmsCountryChanges(Ljava/lang/String;)V
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;->onTmsCountryCode(Ljava/lang/String;)V

    return-void
.end method

.method public onVersionInfo([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "updateFirmwareComponentVersions: received version info from reader"

    .line 99
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;->onVersionInfo([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V

    return-void
.end method

.method public pauseUpdates()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Pausing firmware updates"

    .line 90
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->firmwareUpdateFeatureNative:Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->feature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;->cr_firmware_update_feature_stop_sending_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    return-void
.end method

.method public receivedManifest([BZI)V
    .locals 2

    const/4 p3, 0x0

    new-array p3, p3, [Ljava/lang/Object;

    const-string v0, "Firmware update: received manifest from reader"

    .line 105
    invoke-static {v0, p3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 106
    iget-object p3, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    new-instance v0, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->cardReaderConstants:Lcom/squareup/cardreader/CardReaderConstants;

    .line 108
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReaderConstants;->transportProtocolVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->transport(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->cardReaderConstants:Lcom/squareup/cardreader/CardReaderConstants;

    .line 109
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReaderConstants;->appProtocolVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->app(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->cardReaderConstants:Lcom/squareup/cardreader/CardReaderConstants;

    .line 110
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReaderConstants;->epProtocolVersion()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->ep(Ljava/lang/Integer;)Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;

    move-result-object v0

    .line 111
    invoke-virtual {v0}, Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion$Builder;->build()Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;

    move-result-object v0

    .line 106
    invoke-interface {p3, p1, p2, v0}, Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;->onManifestReceived([BZLcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V

    return-void
.end method

.method public requestManifest()V
    .locals 3

    .line 62
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->firmwareUpdateFeatureNative:Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->feature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    .line 63
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;->cr_firmware_update_feature_get_manifest(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v0, "Requested firmware manifest. Result: %s"

    .line 64
    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public resetFirmwareFeature()V
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->feature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    if-eqz v0, :cond_0

    .line 54
    iget-object v1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->firmwareUpdateFeatureNative:Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;->cr_firmware_update_feature_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    .line 55
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->firmwareUpdateFeatureNative:Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->feature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;->cr_firmware_update_feature_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    const/4 v0, 0x0

    .line 56
    iput-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->feature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    .line 57
    iput-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    :cond_0
    return-void
.end method

.method setApiListener(Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;)V
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    return-void
.end method

.method public update(Lcom/squareup/protos/client/tarkin/Asset;)V
    .locals 6

    .line 69
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->feature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    if-nez v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 73
    iget-object v1, p1, Lcom/squareup/protos/client/tarkin/Asset;->is_blocking:Ljava/lang/Boolean;

    .line 74
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "blocking"

    goto :goto_0

    :cond_1
    const-string v1, "non-blocking"

    :goto_0
    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p1, Lcom/squareup/protos/client/tarkin/Asset;->encrypted_data:Lokio/ByteString;

    invoke-virtual {v1}, Lokio/ByteString;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v3, 0x1

    aput-object v1, v0, v3

    const/4 v1, 0x2

    iget-object v4, p1, Lcom/squareup/protos/client/tarkin/Asset;->header:Lokio/ByteString;

    .line 75
    invoke-virtual {v4}, Lokio/ByteString;->base64()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    const-string v1, "Firmware update, %s: Sending %d bytes body; header %s"

    .line 73
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    iget-object v0, p1, Lcom/squareup/protos/client/tarkin/Asset;->block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

    if-nez v0, :cond_2

    new-array v0, v2, [B

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/tarkin/Asset;->block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

    .line 79
    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object v0

    .line 81
    :goto_1
    iget-object v1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->firmwareUpdateFeatureNative:Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;

    iget-object v4, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->feature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;

    iget-object v5, p1, Lcom/squareup/protos/client/tarkin/Asset;->header:Lokio/ByteString;

    .line 82
    invoke-virtual {v5}, Lokio/ByteString;->toByteArray()[B

    move-result-object v5

    iget-object p1, p1, Lcom/squareup/protos/client/tarkin/Asset;->encrypted_data:Lokio/ByteString;

    invoke-virtual {p1}, Lokio/ByteString;->toByteArray()[B

    move-result-object p1

    .line 81
    invoke-interface {v1, v4, v5, p1, v0}, Lcom/squareup/cardreader/lcr/FirmwareUpdateFeatureNativeInterface;->firmware_send_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_firmware_update_feature_t;[B[B[B)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object p1

    .line 83
    sget-object v0, Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;->CR_FIRMWARE_UPDATE_FEATURE_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    if-eq p1, v0, :cond_3

    new-array v0, v3, [Ljava/lang/Object;

    aput-object p1, v0, v2

    const-string p1, "Firmware update feature error: %s"

    .line 84
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 85
    iget-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    sget-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_ERROR_UNKNOWN:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;->onFirmwareUpdateError(Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V

    :cond_3
    return-void
.end method

.method public updateComplete(I)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 122
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Firmware update complete with result %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 123
    sget-object v0, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->CRS_FWUP_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    invoke-virtual {v0}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->swigValue()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 124
    iget-object p1, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    invoke-interface {p1}, Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;->onFirmwareUpdateSuccess()V

    goto :goto_0

    .line 126
    :cond_0
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;

    move-result-object p1

    .line 127
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;->onFirmwareUpdateError(Lcom/squareup/cardreader/lcr/CrsFirmwareUpdateResult;)V

    :goto_0
    return-void
.end method

.method public updateProgress(I)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 116
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Firmware asset update progress: %d%%"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 117
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateFeatureLegacy;->apiListener:Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderDispatch$FirmwareUpdateListener;->onFirmwareUpdateProgress(I)V

    return-void
.end method
