.class interface abstract Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;
.super Ljava/lang/Object;
.source "PaymentFeatureLegacy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/PaymentFeatureLegacy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "PaymentFeatureListener"
.end annotation


# virtual methods
.method public abstract onAccountSelectionRequired(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract onAudioRequest(Lcom/squareup/cardreader/lcr/CrsTmnAudio;)V
.end method

.method public abstract onAudioVisualRequest(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V
.end method

.method public abstract onCardError()V
.end method

.method public abstract onCardPresenceChange(Z)V
.end method

.method public abstract onCardRemovedDuringPayment()V
.end method

.method public abstract onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
.end method

.method public abstract onDisplayRequest(Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onICCApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V
.end method

.method public abstract onICCDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
.end method

.method public abstract onICCReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
.end method

.method public abstract onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
.end method

.method public abstract onMagSwipePassthrough(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
.end method

.method public abstract onMagswipeFailure(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
.end method

.method public abstract onMagswipeFallbackSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V
.end method

.method public abstract onMagswipeSuccess(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
.end method

.method public abstract onNfcCollision()V
.end method

.method public abstract onNfcInterfaceUnavailable()V
.end method

.method public abstract onNfcLimitExceededInsertCard()V
.end method

.method public abstract onNfcLimitExceededTryAnotherCard()V
.end method

.method public abstract onNfcSeePaymentDeviceForInstructions()V
.end method

.method public abstract onNfcTryAgain()V
.end method

.method public abstract onNfcTryAnotherCard()V
.end method

.method public abstract onNfcUnlockPaymentDevice()V
.end method

.method public abstract onPaymentNfcTimedOut(Lcom/squareup/cardreader/PaymentTimings;)V
.end method

.method public abstract onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V
.end method

.method public abstract onRequestTapCard()V
.end method

.method public abstract onSendAuthorization([BZLcom/squareup/cardreader/CardInfo;)V
.end method

.method public abstract onSendContactlessAuthorization([BLcom/squareup/cardreader/CardInfo;)V
.end method

.method public abstract onSendTmnAuthorization([B)V
.end method

.method public abstract onSigRequested()V
.end method

.method public abstract onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V
.end method

.method public abstract onTmnDataToTmn(Ljava/lang/String;[B)V
.end method

.method public abstract onTmnTransactionComplete(Lcom/squareup/cardreader/lcr/TmnTransactionResult;Lcom/squareup/cardreader/PaymentTimings;)V
.end method

.method public abstract onTmnWriteNotify(II[B)V
.end method

.method public abstract onUseChipCard()V
.end method
