.class Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SkippingNotifyLcrException;
.super Ljava/lang/Exception;
.source "SecureTouchFeatureLegacy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/SecureTouchFeatureLegacy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SkippingNotifyLcrException"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/SecureTouchFeatureLegacy;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/SecureTouchFeatureLegacy;Ljava/lang/String;)V
    .locals 1

    .line 478
    iput-object p1, p0, Lcom/squareup/cardreader/SecureTouchFeatureLegacy$SkippingNotifyLcrException;->this$0:Lcom/squareup/cardreader/SecureTouchFeatureLegacy;

    .line 479
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "RA-35999: Skipping message to LCR: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    return-void
.end method
