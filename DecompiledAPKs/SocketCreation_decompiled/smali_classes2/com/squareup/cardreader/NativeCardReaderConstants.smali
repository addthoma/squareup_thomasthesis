.class public Lcom/squareup/cardreader/NativeCardReaderConstants;
.super Ljava/lang/Object;
.source "NativeCardReaderConstants.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderConstants;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public appProtocolVersion()I
    .locals 1

    .line 11
    sget v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CRS_APP_PROTOCOL_VERSION:I

    return v0
.end method

.method public epProtocolVersion()I
    .locals 1

    .line 15
    sget v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CRS_EP_PROTOCOL_VERSION:I

    return v0
.end method

.method public transportPauseTimeInMilliseconds()I
    .locals 1

    .line 23
    sget v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CRS_TRANSPORT_PAUSE_TIMEOUT_IN_MS:I

    return v0
.end method

.method public transportProtocolVersion()I
    .locals 1

    .line 19
    sget v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CRS_TRANSPORT_PROTOCOL_VERSION:I

    return v0
.end method

.method public transportSmallFrameThreshold()I
    .locals 1

    .line 27
    sget v0, Lcom/squareup/cardreader/lcr/CardreaderNativeConstants;->CRS_TRANSPORT_SMALL_FRAME_THRESHOLD:I

    return v0
.end method
