.class final Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "BleConnectionStateMachineV2.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->readRssiUntilCancelled(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u008a@\u00a2\u0006\u0004\u0008\u0003\u0010\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.cardreader.ble.BleConnectionStateMachineV2$readRssiUntilCancelled$2"
    f = "BleConnectionStateMachineV2.kt"
    i = {
        0x0,
        0x1,
        0x1
    }
    l = {
        0x72,
        0x75
    }
    m = "invokeSuspend"
    n = {
        "$this$async",
        "$this$async",
        "rssi"
    }
    s = {
        "L$0",
        "L$0",
        "L$1"
    }
.end annotation


# instance fields
.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field final synthetic this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p2}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    invoke-direct {v0, v1, p2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 111
    iget v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->label:I

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    if-eq v1, v3, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->L$1:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->L$0:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/CoroutineScope;

    :try_start_0
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 122
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 111
    :cond_1
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->L$0:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/CoroutineScope;

    :try_start_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v4, v0

    move-object v0, p0

    goto :goto_2

    :catch_0
    :goto_0
    move-object v4, v0

    move-object v0, p0

    goto :goto_3

    :cond_2
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->p$:Lkotlinx/coroutines/CoroutineScope;

    move-object v1, p1

    move-object p1, p0

    .line 112
    :goto_1
    invoke-static {v1}, Lkotlinx/coroutines/CoroutineScopeKt;->isActive(Lkotlinx/coroutines/CoroutineScope;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 114
    :try_start_2
    iget-object v4, p1, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    invoke-static {v4}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->access$getExecutionEnv$p(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;)Lcom/squareup/cardreader/ble/Timeouts;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/cardreader/ble/Timeouts;->getDefaultTimeoutMs()J

    move-result-wide v4

    new-instance v6, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2$rssi$1;

    const/4 v7, 0x0

    invoke-direct {v6, p1, v7}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2$rssi$1;-><init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;Lkotlin/coroutines/Continuation;)V

    check-cast v6, Lkotlin/jvm/functions/Function2;

    iput-object v1, p1, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->L$0:Ljava/lang/Object;

    iput v3, p1, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->label:I

    invoke-static {v4, v5, v6, p1}, Lkotlinx/coroutines/TimeoutKt;->withTimeout(JLkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v4
    :try_end_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_1

    if-ne v4, v0, :cond_3

    return-object v0

    :cond_3
    move-object v8, v0

    move-object v0, p1

    move-object p1, v4

    move-object v4, v8

    .line 111
    :goto_2
    :try_start_3
    check-cast p1, Ljava/lang/Integer;

    if-eqz p1, :cond_4

    .line 115
    iget-object v5, v0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    invoke-virtual {v5}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->getEvents()Lcom/squareup/cardreader/ble/RealConnectionEvents;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/squareup/cardreader/ble/RealConnectionEvents;->publishRssi$impl_release(I)V

    .line 117
    :cond_4
    iget-object v5, v0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->this$0:Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;

    invoke-static {v5}, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;->access$getExecutionEnv$p(Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2;)Lcom/squareup/cardreader/ble/Timeouts;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/cardreader/ble/Timeouts;->getRssiPeriodMs()J

    move-result-wide v5

    iput-object v1, v0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->L$0:Ljava/lang/Object;

    iput-object p1, v0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->L$1:Ljava/lang/Object;

    iput v2, v0, Lcom/squareup/cardreader/ble/BleConnectionStateMachineV2$readRssiUntilCancelled$2;->label:I

    invoke-static {v5, v6, v0}, Lkotlinx/coroutines/DelayKt;->delay(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1
    :try_end_3
    .catch Ljava/util/concurrent/CancellationException; {:try_start_3 .. :try_end_3} :catch_2

    if-ne p1, v4, :cond_5

    return-object v4

    :catch_1
    move-object v4, v0

    move-object v0, p1

    :catch_2
    :cond_5
    :goto_3
    move-object p1, v0

    move-object v0, v4

    goto :goto_1

    .line 122
    :cond_6
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method
