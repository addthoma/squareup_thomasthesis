.class public final enum Lcom/squareup/cardreader/ble/BleConnectType;
.super Ljava/lang/Enum;
.source "BleConnectType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/ble/BleConnectType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/ble/BleConnectType;

.field public static final enum CONNECT_IMMEDIATELY:Lcom/squareup/cardreader/ble/BleConnectType;

.field public static final enum CONNECT_WHEN_AVAILABLE:Lcom/squareup/cardreader/ble/BleConnectType;

.field public static final enum RECONNECT_SILENTLY:Lcom/squareup/cardreader/ble/BleConnectType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 17
    new-instance v0, Lcom/squareup/cardreader/ble/BleConnectType;

    const/4 v1, 0x0

    const-string v2, "CONNECT_IMMEDIATELY"

    invoke-direct {v0, v2, v1}, Lcom/squareup/cardreader/ble/BleConnectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/BleConnectType;->CONNECT_IMMEDIATELY:Lcom/squareup/cardreader/ble/BleConnectType;

    .line 24
    new-instance v0, Lcom/squareup/cardreader/ble/BleConnectType;

    const/4 v2, 0x1

    const-string v3, "CONNECT_WHEN_AVAILABLE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/cardreader/ble/BleConnectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/BleConnectType;->CONNECT_WHEN_AVAILABLE:Lcom/squareup/cardreader/ble/BleConnectType;

    .line 30
    new-instance v0, Lcom/squareup/cardreader/ble/BleConnectType;

    const/4 v3, 0x2

    const-string v4, "RECONNECT_SILENTLY"

    invoke-direct {v0, v4, v3}, Lcom/squareup/cardreader/ble/BleConnectType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/cardreader/ble/BleConnectType;->RECONNECT_SILENTLY:Lcom/squareup/cardreader/ble/BleConnectType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/cardreader/ble/BleConnectType;

    .line 12
    sget-object v4, Lcom/squareup/cardreader/ble/BleConnectType;->CONNECT_IMMEDIATELY:Lcom/squareup/cardreader/ble/BleConnectType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/cardreader/ble/BleConnectType;->CONNECT_WHEN_AVAILABLE:Lcom/squareup/cardreader/ble/BleConnectType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/ble/BleConnectType;->RECONNECT_SILENTLY:Lcom/squareup/cardreader/ble/BleConnectType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/cardreader/ble/BleConnectType;->$VALUES:[Lcom/squareup/cardreader/ble/BleConnectType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static fromAutoConnect(Z)Lcom/squareup/cardreader/ble/BleConnectType;
    .locals 0

    if-eqz p0, :cond_0

    .line 38
    sget-object p0, Lcom/squareup/cardreader/ble/BleConnectType;->CONNECT_WHEN_AVAILABLE:Lcom/squareup/cardreader/ble/BleConnectType;

    return-object p0

    .line 40
    :cond_0
    sget-object p0, Lcom/squareup/cardreader/ble/BleConnectType;->CONNECT_IMMEDIATELY:Lcom/squareup/cardreader/ble/BleConnectType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/ble/BleConnectType;
    .locals 1

    .line 12
    const-class v0, Lcom/squareup/cardreader/ble/BleConnectType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/BleConnectType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/ble/BleConnectType;
    .locals 1

    .line 12
    sget-object v0, Lcom/squareup/cardreader/ble/BleConnectType;->$VALUES:[Lcom/squareup/cardreader/ble/BleConnectType;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/ble/BleConnectType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/ble/BleConnectType;

    return-object v0
.end method


# virtual methods
.method public isAutoConnect()Z
    .locals 1

    .line 33
    sget-object v0, Lcom/squareup/cardreader/ble/BleConnectType;->CONNECT_WHEN_AVAILABLE:Lcom/squareup/cardreader/ble/BleConnectType;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
