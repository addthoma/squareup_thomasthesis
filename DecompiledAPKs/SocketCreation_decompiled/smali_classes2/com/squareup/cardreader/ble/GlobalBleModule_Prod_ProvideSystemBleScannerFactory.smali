.class public final Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;
.super Ljava/lang/Object;
.source "GlobalBleModule_Prod_ProvideSystemBleScannerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/ble/SystemBleScanner;",
        ">;"
    }
.end annotation


# instance fields
.field private final bleScanFilterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final bleScannerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanner;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final leScannerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/le/BluetoothLeScanner;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/le/BluetoothLeScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanFilter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)V"
        }
    .end annotation

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

    .line 41
    iput-object p2, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p3, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p4, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->leScannerProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p5, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->bleScannerProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p6, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->bleScanFilterProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p7, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->mainThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/le/BluetoothLeScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanFilter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)",
            "Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;"
        }
    .end annotation

    .line 59
    new-instance v8, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;-><init>(Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static provideSystemBleScanner(Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;Lcom/squareup/cardreader/BluetoothUtils;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/ble/BleScanFilter;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/ble/SystemBleScanner;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/le/BluetoothLeScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanner;",
            ">;",
            "Lcom/squareup/cardreader/ble/BleScanFilter;",
            "Lcom/squareup/thread/executor/MainThread;",
            ")",
            "Lcom/squareup/cardreader/ble/SystemBleScanner;"
        }
    .end annotation

    .line 66
    invoke-virtual/range {p0 .. p6}, Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;->provideSystemBleScanner(Lcom/squareup/cardreader/BluetoothUtils;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/ble/BleScanFilter;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/ble/SystemBleScanner;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/SystemBleScanner;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/ble/SystemBleScanner;
    .locals 7

    .line 51
    iget-object v0, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->module:Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->leScannerProvider:Ljavax/inject/Provider;

    iget-object v4, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->bleScannerProvider:Ljavax/inject/Provider;

    iget-object v5, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->bleScanFilterProvider:Ljavax/inject/Provider;

    invoke-interface {v5}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/cardreader/ble/BleScanFilter;

    iget-object v6, p0, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v6}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/thread/executor/MainThread;

    invoke-static/range {v0 .. v6}, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->provideSystemBleScanner(Lcom/squareup/cardreader/ble/GlobalBleModule$Prod;Lcom/squareup/cardreader/BluetoothUtils;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/ble/BleScanFilter;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/cardreader/ble/SystemBleScanner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/GlobalBleModule_Prod_ProvideSystemBleScannerFactory;->get()Lcom/squareup/cardreader/ble/SystemBleScanner;

    move-result-object v0

    return-object v0
.end method
