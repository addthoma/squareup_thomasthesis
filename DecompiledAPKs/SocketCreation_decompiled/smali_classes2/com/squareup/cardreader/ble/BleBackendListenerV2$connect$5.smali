.class final Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$5;
.super Ljava/lang/Object;
.source "BleBackendListenerV2.kt"

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/ble/BleBackendListenerV2;->connect()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/BiFunction<",
        "Lcom/squareup/dipper/events/BleConnectionState;",
        "Lcom/squareup/dipper/events/BleConnectionState;",
        "Lcom/squareup/dipper/events/BleConnectionState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/dipper/events/BleConnectionState;",
        "old",
        "new",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$5;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/dipper/events/BleConnectionState;)Lcom/squareup/dipper/events/BleConnectionState;
    .locals 7

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$5;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderListeners$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v1

    .line 148
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$5;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderId$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/CardReaderId;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$5;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getWirelessConnection$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v3

    const/4 v6, 0x0

    move-object v4, p1

    move-object v5, p2

    .line 147
    invoke-interface/range {v1 .. v6}, Lcom/squareup/cardreader/ReaderEventLogger;->logBleConnectionStateChange(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/dipper/events/BleConnectionState;Ljava/lang/String;)V

    return-object p2
.end method

.method public bridge synthetic apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/dipper/events/BleConnectionState;

    check-cast p2, Lcom/squareup/dipper/events/BleConnectionState;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$5;->apply(Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/dipper/events/BleConnectionState;)Lcom/squareup/dipper/events/BleConnectionState;

    move-result-object p1

    return-object p1
.end method
