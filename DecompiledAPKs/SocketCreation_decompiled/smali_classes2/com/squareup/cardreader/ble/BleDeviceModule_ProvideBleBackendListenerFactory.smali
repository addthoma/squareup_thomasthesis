.class public final Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;
.super Ljava/lang/Object;
.source "BleDeviceModule_ProvideBleBackendListenerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;",
        ">;"
    }
.end annotation


# instance fields
.field private final bleBackendListenerV2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendListenerV2;",
            ">;"
        }
    .end annotation
.end field

.field private final bleSenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleSender;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/cardreader/ble/BleDeviceModule;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/BleDeviceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/BleDeviceModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendListenerV2;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;->module:Lcom/squareup/cardreader/ble/BleDeviceModule;

    .line 27
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;->bleSenderProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;->bleBackendListenerV2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/cardreader/ble/BleDeviceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/BleDeviceModule;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBackendListenerV2;",
            ">;)",
            "Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;-><init>(Lcom/squareup/cardreader/ble/BleDeviceModule;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideBleBackendListener(Lcom/squareup/cardreader/ble/BleDeviceModule;Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;
    .locals 0

    .line 44
    invoke-virtual {p0, p1, p2}, Lcom/squareup/cardreader/ble/BleDeviceModule;->provideBleBackendListener(Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;->module:Lcom/squareup/cardreader/ble/BleDeviceModule;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;->bleSenderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/ble/BleSender;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;->bleBackendListenerV2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v0, v1, v2}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;->provideBleBackendListener(Lcom/squareup/cardreader/ble/BleDeviceModule;Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/BleDeviceModule_ProvideBleBackendListenerFactory;->get()Lcom/squareup/cardreader/ble/BleBackendLegacy$Listener;

    move-result-object v0

    return-object v0
.end method
