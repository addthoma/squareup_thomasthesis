.class public interface abstract Lcom/squareup/cardreader/ble/BleBackend;
.super Ljava/lang/Object;
.source "BleBackend.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0012\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0008\u0010\u0004\u001a\u00020\u0003H&J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/BleBackend;",
        "",
        "readFromCharacteristicAckVector",
        "",
        "readFromCharacteristicMTU",
        "writeToCharacteristic",
        "data",
        "",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract readFromCharacteristicAckVector()V
.end method

.method public abstract readFromCharacteristicMTU()V
.end method

.method public abstract writeToCharacteristic([B)V
.end method
