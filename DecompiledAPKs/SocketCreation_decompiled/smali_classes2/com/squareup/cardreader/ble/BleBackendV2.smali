.class public final Lcom/squareup/cardreader/ble/BleBackendV2;
.super Ljava/lang/Object;
.source "BleBackendV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/BackendPointerProvider;
.implements Lcom/squareup/cardreader/CanReset;
.implements Lcom/squareup/cardreader/ble/BleBackend;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0012\n\u0002\u0008\u0008\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0013\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007J\u0008\u0010\u000c\u001a\u00020\u000bH\u0016J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\u0013H\u0002J\u0008\u0010\u0016\u001a\u00020\u000eH\u0016J\u0008\u0010\u0017\u001a\u00020\u000eH\u0016J\u0008\u0010\u0018\u001a\u00020\u000eH\u0016J\u0010\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u0013H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/cardreader/ble/BleBackendV2;",
        "Lcom/squareup/cardreader/BackendPointerProvider;",
        "Lcom/squareup/cardreader/CanReset;",
        "Lcom/squareup/cardreader/ble/BleBackend;",
        "posSender",
        "Lcom/squareup/cardreader/SendsToPos;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$BleBackendOutput;",
        "(Lcom/squareup/cardreader/SendsToPos;)V",
        "bleAlloc",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;",
        "bleBackend",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;",
        "backendPointer",
        "handleMessage",
        "",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$BleBackendMessage;",
        "initialize",
        "commsVersion",
        "",
        "onDataReceived",
        "value",
        "readFromCharacteristicAckVector",
        "readFromCharacteristicMTU",
        "resetIfInitilized",
        "writeToCharacteristic",
        "data",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private bleAlloc:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

.field private bleBackend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

.field private final posSender:Lcom/squareup/cardreader/SendsToPos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$BleBackendOutput;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SendsToPos;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$BleBackendOutput;",
            ">;)V"
        }
    .end annotation

    const-string v0, "posSender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    return-void
.end method

.method public static final synthetic access$getBleAlloc$p(Lcom/squareup/cardreader/ble/BleBackendV2;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;
    .locals 1

    .line 20
    iget-object p0, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->bleAlloc:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    if-nez p0, :cond_0

    const-string v0, "bleAlloc"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setBleAlloc$p(Lcom/squareup/cardreader/ble/BleBackendV2;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)V
    .locals 0

    .line 20
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->bleAlloc:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    return-void
.end method

.method private final initialize([B)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "initialize %s"

    .line 45
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    invoke-static {}, Lcom/squareup/cardreader/lcr/BleBackendNative;->cr_comms_backend_ble_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    move-result-object v0

    const-string v1, "BleBackendNative.cr_comms_backend_ble_alloc()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->bleAlloc:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    .line 48
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->bleAlloc:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    if-nez v0, :cond_0

    const-string v1, "bleAlloc"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0, p1, p0}, Lcom/squareup/cardreader/lcr/BleBackendNative;->initialize_backend_ble(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;[BLjava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    move-result-object p1

    const-string v0, "BleBackendNative.initial\u2026lloc, commsVersion, this)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->bleBackend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    return-void
.end method

.method private final onDataReceived([B)V
    .locals 3

    .line 59
    array-length v0, p1

    new-array v0, v0, [B

    .line 60
    array-length v1, p1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 61
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->bleAlloc:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    if-nez p1, :cond_0

    const-string v1, "bleAlloc"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {p1, v0}, Lcom/squareup/cardreader/lcr/BleBackendNative;->ble_received_data_from_characteristic_data(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;[B)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    return-void
.end method


# virtual methods
.method public backendPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->bleBackend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    if-nez v0, :cond_0

    const-string v1, "bleBackend"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final handleMessage(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$BleBackendMessage;)V
    .locals 2

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$BleBackendMessage$Initialize;

    if-eqz v0, :cond_0

    .line 31
    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$BleBackendMessage$Initialize;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$BleBackendMessage$Initialize;->getCommsVersion()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleBackendV2;->initialize([B)V

    .line 32
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$BleBackendOutput$OnInitialized;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$BleBackendOutput$OnInitialized;

    check-cast v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    goto :goto_0

    .line 34
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$BleBackendMessage$OnDataReceived;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$BleBackendMessage$OnDataReceived;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$BleBackendMessage$OnDataReceived;->getValue()[B

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/cardreader/ble/BleBackendV2;->onDataReceived([B)V

    goto :goto_0

    .line 35
    :cond_1
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$BleBackendMessage$OnAckVectorReceived;

    const-string v1, "bleAlloc"

    if-eqz v0, :cond_3

    .line 36
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->bleAlloc:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$BleBackendMessage$OnAckVectorReceived;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$BleBackendMessage$OnAckVectorReceived;->getAckVector()I

    move-result p1

    .line 35
    invoke-static {v0, p1}, Lcom/squareup/cardreader/lcr/BleBackendNative;->ble_received_data_from_characteristic_ack_vector(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;I)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    goto :goto_0

    .line 38
    :cond_3
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$BleBackendMessage$OnMtuReceived;

    if-eqz v0, :cond_5

    .line 39
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->bleAlloc:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$BleBackendMessage$OnMtuReceived;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$BleBackendMessage$OnMtuReceived;->getMtu()I

    move-result p1

    invoke-static {v0, p1}, Lcom/squareup/cardreader/lcr/BleBackendNative;->ble_received_data_from_characteristic_mtu(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;I)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    :cond_5
    :goto_0
    return-void
.end method

.method public readFromCharacteristicAckVector()V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    sget-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$BleBackendOutput$ReadAckVector;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$BleBackendOutput$ReadAckVector;

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public readFromCharacteristicMTU()V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    sget-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$BleBackendOutput$ReadMtu;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$BleBackendOutput$ReadMtu;

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public resetIfInitilized()V
    .locals 2

    .line 52
    move-object v0, p0

    check-cast v0, Lcom/squareup/cardreader/ble/BleBackendV2;

    iget-object v0, v0, Lcom/squareup/cardreader/ble/BleBackendV2;->bleAlloc:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    if-eqz v0, :cond_2

    .line 53
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->bleAlloc:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    const-string v1, "bleAlloc"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/BleBackendNative;->cr_comms_backend_ble_shutdown(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->bleAlloc:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/BleBackendNative;->cr_comms_backend_ble_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_ble_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    :cond_2
    return-void
.end method

.method public writeToCharacteristic([B)V
    .locals 2

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$BleBackendOutput$WriteToCharacteristic;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$BleBackendOutput$WriteToCharacteristic;-><init>([B)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method
