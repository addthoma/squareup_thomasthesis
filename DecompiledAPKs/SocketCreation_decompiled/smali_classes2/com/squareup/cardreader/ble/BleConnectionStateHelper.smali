.class public final Lcom/squareup/cardreader/ble/BleConnectionStateHelper;
.super Ljava/lang/Object;
.source "BleConnectionStateHelper.java"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method static getPairingTimeout(Lcom/squareup/dipper/events/BleConnectionState;)I
    .locals 1

    .line 48
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_DISCONNECT_TO_SILENTLY_RECONNECT:Lcom/squareup/dipper/events/BleConnectionState;

    if-eq p0, v0, :cond_2

    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_RECONNECT_AFTER_GATT_ERROR:Lcom/squareup/dipper/events/BleConnectionState;

    if-ne p0, v0, :cond_0

    goto :goto_0

    .line 51
    :cond_0
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_CONNECTION_TO_READER:Lcom/squareup/dipper/events/BleConnectionState;

    if-ne p0, v0, :cond_1

    const/16 p0, 0x7530

    return p0

    :cond_1
    const/16 p0, 0x1f40

    return p0

    :cond_2
    :goto_0
    const/16 p0, 0x2710

    return p0
.end method

.method static isDestroying(Lcom/squareup/dipper/events/BleConnectionState;)Z
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/dipper/events/BleConnectionState;->name()Ljava/lang/String;

    move-result-object p0

    const-string v0, "DESTROY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method static shouldRestartTimer(Lcom/squareup/dipper/events/BleConnectionState;Lcom/squareup/cardreader/ble/BleConnectType;)Z
    .locals 2

    .line 35
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->READY:Lcom/squareup/dipper/events/BleConnectionState;

    const/4 v1, 0x0

    if-ne p0, v0, :cond_0

    return v1

    .line 36
    :cond_0
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->CREATED:Lcom/squareup/dipper/events/BleConnectionState;

    if-ne p0, v0, :cond_1

    return v1

    .line 37
    :cond_1
    invoke-static {p0}, Lcom/squareup/cardreader/ble/BleConnectionStateHelper;->isDestroying(Lcom/squareup/dipper/events/BleConnectionState;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v1

    .line 39
    :cond_2
    sget-object v0, Lcom/squareup/dipper/events/BleConnectionState;->WAITING_FOR_CONNECTION_TO_READER:Lcom/squareup/dipper/events/BleConnectionState;

    if-ne p0, v0, :cond_3

    sget-object p0, Lcom/squareup/cardreader/ble/BleConnectType;->CONNECT_WHEN_AVAILABLE:Lcom/squareup/cardreader/ble/BleConnectType;

    if-ne p1, p0, :cond_3

    return v1

    :cond_3
    const/4 p0, 0x1

    return p0
.end method
