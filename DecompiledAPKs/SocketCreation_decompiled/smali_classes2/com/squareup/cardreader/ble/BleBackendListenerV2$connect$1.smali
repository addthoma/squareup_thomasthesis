.class final Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;
.super Ljava/lang/Object;
.source "BleBackendListenerV2.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/cardreader/ble/BleBackendListenerV2;->connect()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/cardreader/ble/ConnectionState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "state",
        "Lcom/squareup/cardreader/ble/ConnectionState;",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/cardreader/ble/ConnectionState;)V
    .locals 12

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "state: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    instance-of v0, p1, Lcom/squareup/cardreader/ble/ConnectionState$Connecting;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderListeners$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getWirelessConnection$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v1

    check-cast p1, Lcom/squareup/cardreader/ble/ConnectionState$Connecting;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/ConnectionState$Connecting;->getAutoconnect()Z

    move-result p1

    invoke-static {p1}, Lcom/squareup/cardreader/ble/BleConnectType;->fromAutoConnect(Z)Lcom/squareup/cardreader/ble/BleConnectType;

    move-result-object p1

    .line 87
    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/ReaderEventLogger;->logBleConnectionEnqueued(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/BleConnectType;)V

    goto/16 :goto_5

    .line 90
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/ble/ConnectionState$Connected;

    if-eqz v0, :cond_2

    .line 91
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getBleBackend$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/ble/BleBackendLegacy;

    move-result-object v0

    check-cast p1, Lcom/squareup/cardreader/ble/ConnectionState$Connected;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/ConnectionState$Connected;->getCommsVersion()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ble/BleBackendLegacy;->setCommsVersion([B)V

    .line 92
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {p1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getBleBackend$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/ble/BleBackendLegacy;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleBackendLegacy;->initializeCardReader()V

    .line 93
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {p1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderListeners$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getBlePairingListeners()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/BlePairingListener;

    .line 94
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getWirelessConnection$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/BlePairingListener;->onPairingSuccess(Lcom/squareup/cardreader/WirelessConnection;)V

    goto :goto_0

    .line 96
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {p1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderListeners$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object p1

    new-instance v0, Lcom/squareup/dipper/events/DipperEvent$BleConnectionSuccess;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getBleDevice$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/dipper/events/BleDevice;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionSuccess;-><init>(Lcom/squareup/dipper/events/BleDevice;)V

    check-cast v0, Lcom/squareup/dipper/events/DipperEvent;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishDipperEvent(Lcom/squareup/dipper/events/DipperEvent;)V

    goto/16 :goto_5

    .line 98
    :cond_2
    instance-of v0, p1, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    if-eqz v0, :cond_9

    .line 101
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getDisconnectRequested$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Z

    move-result v5

    .line 102
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getDisposable$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lio/reactivex/disposables/CompositeDisposable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->dispose()V

    .line 105
    check-cast p1, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getErrorBeforeConnection()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 108
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getAutoConnect$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getConnectionError()Lcom/squareup/cardreader/ble/ConnectionError;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/ConnectionError;->getConnectionEvent()Lcom/squareup/cardreader/ble/Error;

    move-result-object v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    sget-object v1, Lcom/squareup/cardreader/ble/Error;->REMOVED_BOND:Lcom/squareup/cardreader/ble/Error;

    if-eq v0, v1, :cond_5

    .line 109
    :cond_4
    invoke-static {p1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2Kt;->access$decodeErrorType(Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;)Lcom/squareup/dipper/events/BleErrorType;

    move-result-object v0

    .line 110
    new-instance v1, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;

    .line 111
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getBleDevice$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/dipper/events/BleDevice;

    move-result-object v7

    const/4 v9, 0x1

    const/4 v10, 0x1

    move-object v6, v1

    move-object v8, v0

    move-object v11, p1

    .line 110
    invoke-direct/range {v6 .. v11}, Lcom/squareup/dipper/events/DipperEvent$BleConnectionFailure;-><init>(Lcom/squareup/dipper/events/BleDevice;Lcom/squareup/dipper/events/BleErrorType;IZLcom/squareup/cardreader/ble/ConnectionState$Disconnected;)V

    .line 114
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderListeners$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v2

    check-cast v1, Lcom/squareup/dipper/events/DipperEvent;

    invoke-virtual {v2, v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->publishDipperEvent(Lcom/squareup/dipper/events/DipperEvent;)V

    .line 115
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderListeners$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getBlePairingListeners()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/cardreader/BlePairingListener;

    .line 116
    iget-object v3, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v3}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getWirelessConnection$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Lcom/squareup/cardreader/BlePairingListener;->onPairingFailed(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/dipper/events/BleErrorType;)V

    goto :goto_2

    .line 121
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;->getDisconnectStatus()Ljava/lang/Integer;

    move-result-object v0

    const/16 v1, 0x13

    if-nez v0, :cond_6

    goto :goto_4

    :cond_6
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_8

    .line 122
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderListeners$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getBlePairingListeners()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/BlePairingListener;

    .line 123
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getWirelessConnection$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/cardreader/BlePairingListener;->onReaderForceUnPair(Lcom/squareup/cardreader/WirelessConnection;)V

    goto :goto_3

    .line 125
    :cond_7
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderListeners$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getWirelessConnection$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger;->logBleReaderForceUnpaired(Lcom/squareup/cardreader/WirelessConnection;)V

    .line 128
    :cond_8
    :goto_4
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderFactory$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/CardReaderFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderId$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderFactory;->destroy(Lcom/squareup/cardreader/CardReaderId;)V

    .line 129
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v0, v5, p1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$reconnectMode(Lcom/squareup/cardreader/ble/BleBackendListenerV2;ZLcom/squareup/cardreader/ble/ConnectionState$Disconnected;)Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;

    move-result-object v0

    .line 131
    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderListeners$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/RealCardReaderListeners;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/cardreader/RealCardReaderListeners;->getReaderEventLogger()Lcom/squareup/cardreader/ReaderEventLogger;

    move-result-object v1

    .line 132
    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v2}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderId$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/CardReaderId;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v3}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getWirelessConnection$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v3

    move-object v4, p1

    move-object v6, v0

    .line 131
    invoke-interface/range {v1 .. v6}, Lcom/squareup/cardreader/ReaderEventLogger;->logBleDisconnectedEvent(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/cardreader/ble/ConnectionState$Disconnected;ZLcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;)V

    .line 136
    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$ReconnectMode;->getShouldReconnect()Z

    move-result p1

    if-eqz p1, :cond_9

    .line 137
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {p1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getCardReaderFactory$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/CardReaderFactory;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->this$0:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-static {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->access$getWirelessConnection$p(Lcom/squareup/cardreader/ble/BleBackendListenerV2;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/cardreader/CardReaderFactory;->forBleAutoConnect(Lcom/squareup/cardreader/WirelessConnection;)V

    :cond_9
    :goto_5
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 60
    check-cast p1, Lcom/squareup/cardreader/ble/ConnectionState;

    invoke-virtual {p0, p1}, Lcom/squareup/cardreader/ble/BleBackendListenerV2$connect$1;->accept(Lcom/squareup/cardreader/ble/ConnectionState;)V

    return-void
.end method
