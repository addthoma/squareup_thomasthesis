.class public Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;
.super Ljava/lang/Object;
.source "BleCardReaderGraphInitializer.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;


# instance fields
.field private final bleBackendListenerV2:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

.field private final bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

.field private final bleConnectionStateMachine:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

.field private final useV2StateMachine:Z


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/ble/BleConnectionStateMachine;Lcom/squareup/cardreader/ble/BleBackendListenerV2;Lcom/squareup/cardreader/ble/BleConnectType;Z)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;->bleConnectionStateMachine:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    .line 17
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;->bleBackendListenerV2:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    .line 18
    iput-object p3, p0, Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    .line 19
    iput-boolean p4, p0, Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;->useV2StateMachine:Z

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .line 31
    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;->useV2StateMachine:Z

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;->bleBackendListenerV2:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->disconnect()V

    goto :goto_0

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;->bleConnectionStateMachine:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    new-instance v1, Lcom/squareup/cardreader/ble/BleAction$DestroyReader;

    invoke-direct {v1}, Lcom/squareup/cardreader/ble/BleAction$DestroyReader;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    :goto_0
    return-void
.end method

.method public initialize()V
    .locals 3

    .line 23
    iget-boolean v0, p0, Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;->useV2StateMachine:Z

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;->bleBackendListenerV2:Lcom/squareup/cardreader/ble/BleBackendListenerV2;

    invoke-virtual {v0}, Lcom/squareup/cardreader/ble/BleBackendListenerV2;->connect()V

    goto :goto_0

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;->bleConnectionStateMachine:Lcom/squareup/cardreader/ble/BleConnectionStateMachine;

    new-instance v1, Lcom/squareup/cardreader/ble/BleAction$InitializeBle;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/BleCardReaderGraphInitializer;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    invoke-direct {v1, v2}, Lcom/squareup/cardreader/ble/BleAction$InitializeBle;-><init>(Lcom/squareup/cardreader/ble/BleConnectType;)V

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ble/BleConnectionStateMachine;->onBleAction(Lcom/squareup/cardreader/ble/BleAction;)V

    :goto_0
    return-void
.end method
