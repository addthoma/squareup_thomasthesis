.class Lcom/squareup/cardreader/ble/BleSender$CreateConnection;
.super Ljava/lang/Object;
.source "BleSender.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/BleSender$GattAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ble/BleSender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CreateConnection"
.end annotation


# instance fields
.field final initializeHelper:Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;

.field final synthetic this$0:Lcom/squareup/cardreader/ble/BleSender;


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;)V
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleSender$CreateConnection;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    iput-object p2, p0, Lcom/squareup/cardreader/ble/BleSender$CreateConnection;->initializeHelper:Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;Lcom/squareup/cardreader/ble/BleSender$1;)V
    .locals 0

    .line 198
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/ble/BleSender$CreateConnection;-><init>(Lcom/squareup/cardreader/ble/BleSender;Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;)V

    return-void
.end method


# virtual methods
.method public perform()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Starting to initialize connection"

    .line 206
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleSender$CreateConnection;->this$0:Lcom/squareup/cardreader/ble/BleSender;

    iget-object v1, p0, Lcom/squareup/cardreader/ble/BleSender$CreateConnection;->initializeHelper:Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;

    invoke-interface {v1}, Lcom/squareup/cardreader/ble/BleSender$InitializeHelper;->initialize()Landroid/bluetooth/BluetoothGatt;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/cardreader/ble/BleSender;->access$602(Lcom/squareup/cardreader/ble/BleSender;Landroid/bluetooth/BluetoothGatt;)Landroid/bluetooth/BluetoothGatt;

    return-void
.end method
