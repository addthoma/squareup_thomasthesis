.class public Lcom/squareup/cardreader/ble/BleConnector;
.super Ljava/lang/Object;
.source "BleConnector.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final bleAutoConnector:Lcom/squareup/cardreader/ble/BleAutoConnector;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/ble/BleAutoConnector;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/cardreader/ble/BleConnector;->bleAutoConnector:Lcom/squareup/cardreader/ble/BleAutoConnector;

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 16
    iget-object p1, p0, Lcom/squareup/cardreader/ble/BleConnector;->bleAutoConnector:Lcom/squareup/cardreader/ble/BleAutoConnector;

    invoke-interface {p1}, Lcom/squareup/cardreader/ble/BleAutoConnector;->initialize()V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/cardreader/ble/BleConnector;->bleAutoConnector:Lcom/squareup/cardreader/ble/BleAutoConnector;

    invoke-interface {v0}, Lcom/squareup/cardreader/ble/BleAutoConnector;->destroy()V

    return-void
.end method
