.class Lcom/squareup/cardreader/ble/SystemBleScannerLollipop;
.super Landroid/bluetooth/le/ScanCallback;
.source "SystemBleScannerLollipop.java"

# interfaces
.implements Lcom/squareup/cardreader/ble/SystemBleScanner;


# instance fields
.field private final bleScanFilter:Lcom/squareup/cardreader/ble/BleScanFilter;

.field private final bleScanner:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanner;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothLeScanner:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/le/BluetoothLeScanner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Lcom/squareup/cardreader/ble/BleScanFilter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/le/BluetoothLeScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleScanner;",
            ">;",
            "Lcom/squareup/cardreader/ble/BleScanFilter;",
            ")V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Landroid/bluetooth/le/ScanCallback;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/cardreader/ble/SystemBleScannerLollipop;->bluetoothLeScanner:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/cardreader/ble/SystemBleScannerLollipop;->bleScanner:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/cardreader/ble/SystemBleScannerLollipop;->bleScanFilter:Lcom/squareup/cardreader/ble/BleScanFilter;

    return-void
.end method

.method private static buildBleScanResult(Landroid/bluetooth/le/ScanResult;)Lcom/squareup/cardreader/ble/RealBleScanResult;
    .locals 3

    .line 73
    invoke-virtual {p0}, Landroid/bluetooth/le/ScanResult;->getScanRecord()Landroid/bluetooth/le/ScanRecord;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const v2, 0x827e

    .line 75
    invoke-virtual {v0, v2}, Landroid/bluetooth/le/ScanRecord;->getManufacturerSpecificData(I)[B

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    aget-byte v0, v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    const/4 v1, 0x1

    .line 79
    :cond_1
    new-instance v0, Lcom/squareup/cardreader/ble/RealBleScanResult;

    invoke-virtual {p0}, Landroid/bluetooth/le/ScanResult;->getDevice()Landroid/bluetooth/BluetoothDevice;

    move-result-object p0

    invoke-direct {v0, p0, v1}, Lcom/squareup/cardreader/ble/RealBleScanResult;-><init>(Landroid/bluetooth/BluetoothDevice;Z)V

    return-object v0
.end method


# virtual methods
.method public onBatchScanResults(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Landroid/bluetooth/le/ScanResult;",
            ">;)V"
        }
    .end annotation

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 58
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/le/ScanResult;

    .line 59
    invoke-static {v1}, Lcom/squareup/cardreader/ble/SystemBleScannerLollipop;->buildBleScanResult(Landroid/bluetooth/le/ScanResult;)Lcom/squareup/cardreader/ble/RealBleScanResult;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 62
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardreader/ble/SystemBleScannerLollipop;->bleScanner:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/ble/BleScanner;

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ble/BleScanner;->onBleDeviceFound(Ljava/util/List;)V

    return-void
.end method

.method public onScanFailed(I)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 66
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "BLE Scan failed, code: %d"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    iget-object p1, p0, Lcom/squareup/cardreader/ble/SystemBleScannerLollipop;->bleScanner:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/ble/BleScanner;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ble/BleScanner;->onScanFailed()V

    return-void
.end method

.method public onScanResult(ILandroid/bluetooth/le/ScanResult;)V
    .locals 0

    .line 52
    iget-object p1, p0, Lcom/squareup/cardreader/ble/SystemBleScannerLollipop;->bleScanner:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/cardreader/ble/BleScanner;

    invoke-static {p2}, Lcom/squareup/cardreader/ble/SystemBleScannerLollipop;->buildBleScanResult(Landroid/bluetooth/le/ScanResult;)Lcom/squareup/cardreader/ble/RealBleScanResult;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ble/BleScanner;->onBleDeviceFound(Lcom/squareup/cardreader/ble/BleScanResult;)V

    return-void
.end method

.method public declared-synchronized startScan()V
    .locals 4

    monitor-enter p0

    .line 34
    :try_start_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/SystemBleScannerLollipop;->bluetoothLeScanner:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/le/BluetoothLeScanner;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 35
    monitor-exit p0

    return-void

    .line 37
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/squareup/cardreader/ble/SystemBleScannerLollipop;->bleScanFilter:Lcom/squareup/cardreader/ble/BleScanFilter;

    invoke-virtual {v1}, Lcom/squareup/cardreader/ble/BleScanFilter;->createScanFiltersForSquarePos()Ljava/util/List;

    move-result-object v1

    .line 38
    new-instance v2, Landroid/bluetooth/le/ScanSettings$Builder;

    invoke-direct {v2}, Landroid/bluetooth/le/ScanSettings$Builder;-><init>()V

    const/4 v3, 0x2

    .line 39
    invoke-virtual {v2, v3}, Landroid/bluetooth/le/ScanSettings$Builder;->setScanMode(I)Landroid/bluetooth/le/ScanSettings$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/le/ScanSettings$Builder;->build()Landroid/bluetooth/le/ScanSettings;

    move-result-object v2

    .line 41
    invoke-virtual {v0, v1, v2, p0}, Landroid/bluetooth/le/BluetoothLeScanner;->startScan(Ljava/util/List;Landroid/bluetooth/le/ScanSettings;Landroid/bluetooth/le/ScanCallback;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 42
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stopScan()V
    .locals 1

    monitor-enter p0

    .line 45
    :try_start_0
    iget-object v0, p0, Lcom/squareup/cardreader/ble/SystemBleScannerLollipop;->bluetoothLeScanner:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/le/BluetoothLeScanner;

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v0, p0}, Landroid/bluetooth/le/BluetoothLeScanner;->stopScan(Landroid/bluetooth/le/ScanCallback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
