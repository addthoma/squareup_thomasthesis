.class public final Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;
.super Ljava/lang/Object;
.source "RealBleAutoConnector_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/cardreader/ble/RealBleAutoConnector;",
        ">;"
    }
.end annotation


# instance fields
.field private final bleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final bleEventLogFilterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleEventLogFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothStatusReceiverProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderListenersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final libraryLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final storedCardReadersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleEventLogFilter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ">;)V"
        }
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->bleEventLogFilterProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p2, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p3, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->bleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p4, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p5, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->bluetoothStatusReceiverProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p6, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p7, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p8, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p9, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p10, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->libraryLoaderProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p11, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->storedCardReadersProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleEventLogFilter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ">;)",
            "Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;"
        }
    .end annotation

    .line 85
    new-instance v12, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/cardreader/ble/BleEventLogFilter;Ljavax/inject/Provider;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/loader/LibraryLoader;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;)Lcom/squareup/cardreader/ble/RealBleAutoConnector;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/ble/BleEventLogFilter;",
            "Ljavax/inject/Provider<",
            "Landroid/bluetooth/BluetoothAdapter;",
            ">;",
            "Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/cardreader/loader/LibraryLoader;",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ")",
            "Lcom/squareup/cardreader/ble/RealBleAutoConnector;"
        }
    .end annotation

    .line 94
    new-instance v12, Lcom/squareup/cardreader/ble/RealBleAutoConnector;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/cardreader/ble/RealBleAutoConnector;-><init>(Lcom/squareup/cardreader/ble/BleEventLogFilter;Ljavax/inject/Provider;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/loader/LibraryLoader;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/cardreader/ble/RealBleAutoConnector;
    .locals 12

    .line 71
    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->bleEventLogFilterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/cardreader/ble/BleEventLogFilter;

    iget-object v2, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->bluetoothAdapterProvider:Ljavax/inject/Provider;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->bleBondingBroadcastReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->bluetoothStatusReceiverProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->cardReaderListenersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/cardreader/CardReaderListeners;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->libraryLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/cardreader/loader/LibraryLoader;

    iget-object v0, p0, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->storedCardReadersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    invoke-static/range {v1 .. v11}, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->newInstance(Lcom/squareup/cardreader/ble/BleEventLogFilter;Ljavax/inject/Provider;Lcom/squareup/cardreader/ble/BleBondingBroadcastReceiver;Lcom/squareup/cardreader/BluetoothUtils;Lcom/squareup/cardreader/ble/BluetoothStatusReceiver;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/loader/LibraryLoader;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;)Lcom/squareup/cardreader/ble/RealBleAutoConnector;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/cardreader/ble/RealBleAutoConnector_Factory;->get()Lcom/squareup/cardreader/ble/RealBleAutoConnector;

    move-result-object v0

    return-object v0
.end method
