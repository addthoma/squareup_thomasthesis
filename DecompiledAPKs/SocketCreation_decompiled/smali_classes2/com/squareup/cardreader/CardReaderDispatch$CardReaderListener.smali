.class public interface abstract Lcom/squareup/cardreader/CardReaderDispatch$CardReaderListener;
.super Ljava/lang/Object;
.source "CardReaderDispatch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/CardReaderDispatch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CardReaderListener"
.end annotation


# virtual methods
.method public abstract logEvent(Lcom/squareup/cardreader/ReaderEventLogger$FirmwareEventLog;)V
.end method

.method public abstract onAudioReaderFailedToConnect()V
.end method

.method public abstract onCapabilitiesReceived(ZLjava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrsCapability;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onCardPresenceChange(Z)V
.end method

.method public abstract onCardReaderBackendInitialized()V
.end method

.method public abstract onChargeCycleCountReceived(I)V
.end method

.method public abstract onCommsRateUpdated(Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;)V
.end method

.method public abstract onCommsVersionAcquired(Lcom/squareup/cardreader/lcr/CrCommsVersionResult;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;Lcom/squareup/protos/client/tarkin/AssetUpdateRequest$CommsProtocolVersion;)V
.end method

.method public abstract onCoreDumpErased()V
.end method

.method public abstract onCoreDumpExists(Z)V
.end method

.method public abstract onCoreDumpProgress(III)V
.end method

.method public abstract onCoreDumpReceived([B[B)V
.end method

.method public abstract onCoreDumpTriggered(Z)V
.end method

.method public abstract onFirmwareVersionReceived(Ljava/lang/String;)V
.end method

.method public abstract onHardwareSerialNumberReceived(Ljava/lang/String;)V
.end method

.method public abstract onPowerStatus(Lcom/squareup/cardreader/CardReaderBatteryInfo;)V
.end method

.method public abstract onReaderError(Lcom/squareup/cardreader/lcr/CrsReaderError;)V
.end method

.method public abstract onReaderReady(Lcom/squareup/cardreader/lcr/CrCardreaderType;)V
.end method

.method public abstract onSecureSessionDenied(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrSecureSessionUxHint;)V
.end method

.method public abstract onSecureSessionError(Lcom/squareup/cardreader/lcr/CrSecureSessionResult;)V
.end method

.method public abstract onSecureSessionInvalid()V
.end method

.method public abstract onSecureSessionSendToServer([B)V
.end method

.method public abstract onSecureSessionValid()V
.end method

.method public abstract onTamperData([B)V
.end method

.method public abstract onTamperStatus(Lcom/squareup/cardreader/lcr/CrTamperStatus;)V
.end method

.method public abstract onTmsCountryCode(Ljava/lang/String;)V
.end method

.method public abstract onVersionInfo([Lcom/squareup/cardreader/FirmwareAssetVersionInfo;)V
.end method
