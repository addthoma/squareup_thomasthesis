.class public final enum Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;
.super Ljava/lang/Enum;
.source "ReaderMessage.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StandardPaymentMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008%\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015j\u0002\u0008\u0016j\u0002\u0008\u0017j\u0002\u0008\u0018j\u0002\u0008\u0019j\u0002\u0008\u001aj\u0002\u0008\u001bj\u0002\u0008\u001cj\u0002\u0008\u001dj\u0002\u0008\u001ej\u0002\u0008\u001fj\u0002\u0008 j\u0002\u0008!j\u0002\u0008\"j\u0002\u0008#j\u0002\u0008$j\u0002\u0008%\u00a8\u0006&"
    }
    d2 = {
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;",
        "",
        "(Ljava/lang/String;I)V",
        "None",
        "Amount",
        "AmountOk",
        "Approved",
        "CallYourBank",
        "CancelOrEnter",
        "CardError",
        "Declined",
        "EnterAmount",
        "EnterPin",
        "IncorrectPin",
        "InsertCard",
        "NotAccepted",
        "PinOk",
        "PleaseWait",
        "ProcessingError",
        "RemoveCard",
        "UseChipReader",
        "UseMagStrip",
        "TryAgain",
        "Welcome",
        "PresentCard",
        "Processing",
        "CardReadOkPlsRemoveCard",
        "PlsInsertOrSwipeCard",
        "PlsPresentOneCard",
        "ApprovedPlsSign",
        "AuthorisingPlsWait",
        "InsertSwipeOrTryAnotherCard",
        "PlsInsertCard",
        "NoMsg",
        "SeePhoneForInstructions",
        "PresentCardAgain",
        "UnlockPhoneToPay",
        "TooManyTap",
        "lcr-data-models_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum Amount:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum AmountOk:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum Approved:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum ApprovedPlsSign:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum AuthorisingPlsWait:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum CallYourBank:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum CancelOrEnter:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum CardError:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum CardReadOkPlsRemoveCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum Declined:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum EnterAmount:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum EnterPin:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum IncorrectPin:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum InsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum InsertSwipeOrTryAnotherCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum NoMsg:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum None:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum NotAccepted:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum PinOk:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum PleaseWait:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum PlsInsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum PlsInsertOrSwipeCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum PlsPresentOneCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum PresentCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum PresentCardAgain:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum Processing:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum ProcessingError:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum RemoveCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum SeePhoneForInstructions:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum TooManyTap:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum TryAgain:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum UnlockPhoneToPay:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum UseChipReader:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum UseMagStrip:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

.field public static final enum Welcome:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v0, 0x23

    new-array v0, v0, [Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/4 v2, 0x0

    const-string v3, "None"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->None:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/4 v2, 0x1

    const-string v3, "Amount"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->Amount:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/4 v2, 0x2

    const-string v3, "AmountOk"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->AmountOk:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/4 v2, 0x3

    const-string v3, "Approved"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->Approved:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/4 v2, 0x4

    const-string v3, "CallYourBank"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->CallYourBank:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/4 v2, 0x5

    const-string v3, "CancelOrEnter"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->CancelOrEnter:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/4 v2, 0x6

    const-string v3, "CardError"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->CardError:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/4 v2, 0x7

    const-string v3, "Declined"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->Declined:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x8

    const-string v3, "EnterAmount"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->EnterAmount:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x9

    const-string v3, "EnterPin"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->EnterPin:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0xa

    const-string v3, "IncorrectPin"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->IncorrectPin:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0xb

    const-string v3, "InsertCard"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->InsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0xc

    const-string v3, "NotAccepted"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->NotAccepted:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0xd

    const-string v3, "PinOk"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->PinOk:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0xe

    const-string v3, "PleaseWait"

    invoke-direct {v1, v3, v2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->PleaseWait:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "ProcessingError"

    const/16 v3, 0xf

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->ProcessingError:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "RemoveCard"

    const/16 v3, 0x10

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->RemoveCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "UseChipReader"

    const/16 v3, 0x11

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->UseChipReader:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "UseMagStrip"

    const/16 v3, 0x12

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->UseMagStrip:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "TryAgain"

    const/16 v3, 0x13

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->TryAgain:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "Welcome"

    const/16 v3, 0x14

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->Welcome:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "PresentCard"

    const/16 v3, 0x15

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->PresentCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "Processing"

    const/16 v3, 0x16

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->Processing:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "CardReadOkPlsRemoveCard"

    const/16 v3, 0x17

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->CardReadOkPlsRemoveCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "PlsInsertOrSwipeCard"

    const/16 v3, 0x18

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->PlsInsertOrSwipeCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "PlsPresentOneCard"

    const/16 v3, 0x19

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->PlsPresentOneCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "ApprovedPlsSign"

    const/16 v3, 0x1a

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->ApprovedPlsSign:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "AuthorisingPlsWait"

    const/16 v3, 0x1b

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->AuthorisingPlsWait:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "InsertSwipeOrTryAnotherCard"

    const/16 v3, 0x1c

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->InsertSwipeOrTryAnotherCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "PlsInsertCard"

    const/16 v3, 0x1d

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->PlsInsertCard:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "NoMsg"

    const/16 v3, 0x1e

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->NoMsg:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "SeePhoneForInstructions"

    const/16 v3, 0x1f

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->SeePhoneForInstructions:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "PresentCardAgain"

    const/16 v3, 0x20

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->PresentCardAgain:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "UnlockPhoneToPay"

    const/16 v3, 0x21

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->UnlockPhoneToPay:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const-string v2, "TooManyTap"

    const/16 v3, 0x22

    invoke-direct {v1, v2, v3}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->TooManyTap:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->$VALUES:[Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 432
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;
    .locals 1

    const-class v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;
    .locals 1

    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->$VALUES:[Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$PaymentFeatureOutput$StandardPaymentMessage;

    return-object v0
.end method
