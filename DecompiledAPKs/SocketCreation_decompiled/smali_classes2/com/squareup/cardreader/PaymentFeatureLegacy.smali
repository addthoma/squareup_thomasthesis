.class Lcom/squareup/cardreader/PaymentFeatureLegacy;
.super Ljava/lang/Object;
.source "PaymentFeatureLegacy.java"

# interfaces
.implements Lcom/squareup/cardreader/PaymentFeature;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;
    }
.end annotation


# instance fields
.field private apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

.field private final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field private final cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

.field private final delegate:Lcom/squareup/cardreader/PaymentFeature;

.field private featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private final paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

.field private final sessionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field

.field private final tmnTimings:Lcom/squareup/tmn/TmnTimings;


# direct methods
.method constructor <init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderListeners;Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/tmn/TmnTimings;Lcom/squareup/cardreader/PaymentFeature;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/CardReaderInfo;",
            "Lcom/squareup/cardreader/CardReaderListeners;",
            "Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/tmn/TmnTimings;",
            "Lcom/squareup/cardreader/PaymentFeature;",
            ")V"
        }
    .end annotation

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->sessionProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 66
    iput-object p3, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    .line 67
    iput-object p4, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    .line 68
    iput-object p5, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 69
    iput-object p6, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    .line 70
    iput-object p7, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    return-void
.end method

.method static accountTypesFromInt([I)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List<",
            "Lcom/squareup/cardreader/lcr/CrPaymentAccountType;",
            ">;"
        }
    .end annotation

    .line 354
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 355
    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 356
    aget v2, p0, v1

    invoke-static {v2}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private createSuccessfulSwipe([BLcom/squareup/cardreader/CardInfo;)Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;
    .locals 3

    .line 574
    invoke-static {p1}, Lcom/squareup/squarewave/util/Base64;->encode([B)[C

    move-result-object p1

    .line 575
    invoke-static {p1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object p1

    .line 577
    new-instance v0, Lcom/squareup/Card$Builder;

    invoke-direct {v0}, Lcom/squareup/Card$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 578
    invoke-direct {p0, v1}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->inputTypeFromReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/Card$InputType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/Card$Builder;->inputType(Lcom/squareup/Card$InputType;)Lcom/squareup/Card$Builder;

    move-result-object v0

    .line 579
    invoke-virtual {v0, p1}, Lcom/squareup/Card$Builder;->trackData(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 580
    invoke-virtual {p2}, Lcom/squareup/cardreader/CardInfo;->getLast4()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/Card$Builder;->pan(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 581
    invoke-virtual {p2}, Lcom/squareup/cardreader/CardInfo;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/Card$Builder;->name(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 582
    invoke-virtual {p2}, Lcom/squareup/cardreader/CardInfo;->getBrand()Lcom/squareup/Card$Brand;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/Card$Builder;->brand(Lcom/squareup/Card$Brand;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 583
    invoke-virtual {p1}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object p1

    .line 585
    new-instance v0, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderInfo;->getCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/cardreader/CardInfo;->hasTrack1Data()Z

    move-result v2

    .line 586
    invoke-virtual {p2}, Lcom/squareup/cardreader/CardInfo;->hasTrack2Data()Z

    move-result p2

    invoke-direct {v0, v1, p1, v2, p2}, Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;-><init>(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/Card;ZZ)V

    return-object v0
.end method

.method private inputTypeFromReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/Card$InputType;
    .locals 3

    .line 599
    sget-object v0, Lcom/squareup/cardreader/PaymentFeatureLegacy$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 605
    sget-object p1, Lcom/squareup/Card$InputType;->R6_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    return-object p1

    .line 607
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Reader not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 603
    :cond_1
    sget-object p1, Lcom/squareup/Card$InputType;->T2_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    return-object p1

    .line 601
    :cond_2
    sget-object p1, Lcom/squareup/Card$InputType;->X2_ENCRYPTED_TRACK:Lcom/squareup/Card$InputType;

    return-object p1
.end method

.method static languagePrefsFromBytes([B)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 370
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p0, :cond_0

    return-object v0

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 374
    :goto_0
    array-length v3, p0

    const/4 v4, 0x1

    sub-int/2addr v3, v4

    if-ge v2, v3, :cond_2

    .line 375
    aget-byte v3, p0, v2

    if-nez v3, :cond_1

    add-int/lit8 v3, v2, 0x1

    aget-byte v3, p0, v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x2

    new-array v3, v3, [B

    .line 378
    aget-byte v5, p0, v2

    aput-byte v5, v3, v1

    add-int/lit8 v5, v2, 0x1

    aget-byte v5, p0, v5

    aput-byte v5, v3, v4

    .line 380
    :try_start_0
    new-instance v4, Ljava/lang/String;

    const-string v5, "UTF-8"

    invoke-direct {v4, v3, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 382
    :catch_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Couldn\'t decode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v3, v4}, Ltimber/log/Timber;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    add-int/lit8 v2, v2, 0x2

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private logPaymentEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V
    .locals 2

    .line 590
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v1, Lcom/squareup/cardreader/-$$Lambda$PaymentFeatureLegacy$aVqRqxKGlMEHV0wTCL5L99Z1Hfw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/cardreader/-$$Lambda$PaymentFeatureLegacy$aVqRqxKGlMEHV0wTCL5L99Z1Hfw;-><init>(Lcom/squareup/cardreader/PaymentFeatureLegacy;Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V

    invoke-interface {v0, v1}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method private startEmvPaymentInteraction(Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;JJ)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 16

    move-object/from16 v0, p0

    .line 133
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    move-wide/from16 v2, p4

    .line 134
    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v2, 0x1

    .line 136
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/lit16 v10, v3, -0x7d0

    const/4 v3, 0x2

    .line 137
    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/lit8 v11, v3, 0x1

    const/4 v2, 0x5

    .line 138
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v12

    const/16 v2, 0xb

    .line 139
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v13

    const/16 v2, 0xc

    .line 140
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v14

    const/16 v2, 0xd

    .line 141
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v15

    .line 143
    iget-object v4, v0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v5, v0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    sget-object v1, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->CR_PAYMENT_ACCOUNT_TYPE_DEFAULT:Lcom/squareup/cardreader/lcr/CrPaymentAccountType;

    .line 145
    invoke-virtual {v1}, Lcom/squareup/cardreader/lcr/CrPaymentAccountType;->swigValue()I

    move-result v8

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;->swigValue()I

    move-result v9

    move-wide/from16 v6, p2

    .line 144
    invoke-interface/range {v4 .. v15}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->payment_start_payment(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;JIIIIIIII)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v1

    .line 148
    new-instance v2, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    invoke-direct {v2}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;-><init>()V

    sget-object v3, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->START_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 149
    invoke-virtual {v2, v3}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->event(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object v2

    iget-object v3, v0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 150
    invoke-virtual {v2, v3}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object v2

    move-object/from16 v3, p1

    .line 151
    invoke-virtual {v2, v3}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->transactionType(Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object v2

    move-wide/from16 v3, p2

    .line 152
    invoke-virtual {v2, v3, v4}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->amountAuthorized(J)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object v2

    .line 153
    invoke-virtual {v2, v1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->startPaymentResult(Lcom/squareup/cardreader/lcr/CrPaymentResult;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object v2

    .line 154
    invoke-virtual {v2}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    move-result-object v2

    .line 155
    invoke-direct {v0, v2}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->logPaymentEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V

    return-object v1
.end method

.method private startTmnPaymentInteraction(Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)Lcom/squareup/cardreader/lcr/CrPaymentResult;
    .locals 9

    .line 113
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_TMN_START:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 114
    iget-object v2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v3, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-wide v7, p4

    .line 115
    invoke-interface/range {v2 .. v8}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->payment_tmn_start_transaction(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object v0

    .line 118
    new-instance v1, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    invoke-direct {v1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;-><init>()V

    sget-object v2, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->START_TMN_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 119
    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->event(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 120
    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object v1

    .line 121
    invoke-virtual {v1, p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->requestType(Lcom/squareup/cardreader/lcr/CrsTmnRequestType;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p1

    .line 122
    invoke-virtual {p1, p3}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->brandId(Lcom/squareup/cardreader/lcr/CrsTmnBrandId;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p1

    .line 123
    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->transactionId(Ljava/lang/String;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p1

    .line 124
    invoke-virtual {p1, p4, p5}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->amountAuthorized(J)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p1

    .line 125
    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    move-result-object p1

    .line 126
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->logPaymentEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V

    return-object v0
.end method


# virtual methods
.method public ackTmnWriteNotify()V
    .locals 2

    .line 215
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_TMN_ACK_WRITE_NOTIFY:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 216
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->payment_tmn_write_notify_ack(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    return-void
.end method

.method public cancelPayment()V
    .locals 2

    .line 161
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->CANCEL_PAYMENT:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 162
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->event(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 163
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object v0

    .line 164
    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    move-result-object v0

    .line 165
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->logPaymentEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "PaymentFeature: Calling cr_payment_cancel_payment"

    .line 166
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 167
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->cr_payment_cancel_payment(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    return-void
.end method

.method public cancelTmnRequest()V
    .locals 2

    .line 220
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->payment_tmn_cancel_request(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    return-void
.end method

.method public checkCardPresence()V
    .locals 2

    .line 183
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->cr_payment_request_card_presence(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    return-void
.end method

.method public enableSwipePassthrough(Z)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    if-eqz p1, :cond_0

    const-string v1, "En"

    goto :goto_0

    :cond_0
    const-string v1, "Dis"

    :goto_0
    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Swipe Passthrough: %sabled"

    .line 187
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 188
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->cr_payment_enable_swipe_passthrough(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Z)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    return-void
.end method

.method public initializePayment(Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;II)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    .line 79
    iput-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    .line 81
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->sessionProvider:Ljavax/inject/Provider;

    .line 82
    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    invoke-interface {p1, v0, p0, p2, p3}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->payment_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;II)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    return-void

    .line 78
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "apiListener cannot be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 75
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "PaymentFeature is already initialized!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic lambda$logPaymentEvent$0$PaymentFeatureLegacy(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V
    .locals 1

    .line 590
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderListeners:Lcom/squareup/cardreader/CardReaderListeners;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/CardReaderListeners;->logPaymentFeatureEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V

    return-void
.end method

.method public onAccountSelectionRequired([I[BLjava/lang/String;)V
    .locals 2

    .line 346
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/PaymentFeature;->onAccountSelectionRequired([I[BLjava/lang/String;)V

    .line 347
    invoke-static {p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->accountTypesFromInt([I)Ljava/util/List;

    move-result-object p1

    .line 348
    invoke-static {p2}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->languagePrefsFromBytes([B)Ljava/util/List;

    move-result-object p2

    .line 349
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Account selection required: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " languages: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onAccountSelectionRequired(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method public onAudioRequest(I)V
    .locals 2

    .line 559
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/PaymentFeature;->onAudioRequest(I)V

    .line 560
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsTmnAudio;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsTmnAudio;

    move-result-object p1

    .line 561
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onAudioRequest: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 562
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onAudioRequest(Lcom/squareup/cardreader/lcr/CrsTmnAudio;)V

    return-void
.end method

.method public onAudioVisualRequest(I)V
    .locals 2

    .line 567
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/PaymentFeature;->onAudioVisualRequest(I)V

    .line 568
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrAudioVisualId;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrAudioVisualId;

    move-result-object p1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "onAudioVisualRequest: %s"

    .line 569
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 570
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onAudioVisualRequest(Lcom/squareup/cardreader/lcr/CrAudioVisualId;)V

    return-void
.end method

.method public onCardActionRequired(II)V
    .locals 4

    .line 261
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/PaymentFeature;->onCardActionRequired(II)V

    .line 262
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    move-result-object p1

    .line 263
    invoke-static {p2}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    move-result-object p2

    .line 265
    new-instance v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->ON_CARD_ACTION_REQUIRED:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 266
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->event(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 267
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object v0

    .line 268
    invoke-virtual {v0, p2}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->standardMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object v0

    .line 269
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardAction(Lcom/squareup/cardreader/lcr/CrPaymentCardAction;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object v0

    .line 270
    invoke-virtual {v0}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    move-result-object v0

    .line 271
    invoke-direct {p0, v0}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->logPaymentEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "Card Action Code:   %s"

    .line 273
    invoke-static {v3, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    new-array v1, v0, [Ljava/lang/Object;

    aput-object p2, v1, v2

    const-string v2, "Card Action StdMsg: %s"

    .line 274
    invoke-static {v2, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    sget-object v1, Lcom/squareup/cardreader/PaymentFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrPaymentCardAction:[I

    invoke-virtual {p1}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 332
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown action: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 329
    :pswitch_0
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onRequestTapCard()V

    goto/16 :goto_0

    .line 326
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onNfcLimitExceededInsertCard()V

    goto/16 :goto_0

    .line 323
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onNfcLimitExceededTryAnotherCard()V

    goto :goto_0

    .line 320
    :pswitch_3
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onNfcCollision()V

    goto :goto_0

    .line 317
    :pswitch_4
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onNfcUnlockPaymentDevice()V

    goto :goto_0

    .line 314
    :pswitch_5
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onNfcSeePaymentDeviceForInstructions()V

    goto :goto_0

    .line 306
    :pswitch_6
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onNfcTryAnotherCard()V

    goto :goto_0

    .line 303
    :pswitch_7
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onNfcInterfaceUnavailable()V

    goto :goto_0

    .line 297
    :pswitch_8
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    iget-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 298
    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReaderInfo;->failedSwipesDefaultToSwipeStraight()Z

    move-result p2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;->fromSwipeStraight(Ljava/lang/Boolean;)Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;

    move-result-object p2

    .line 297
    invoke-interface {p1, p2}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onMagswipeFailure(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V

    goto :goto_0

    .line 294
    :pswitch_9
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    sget-object p2, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->SCHEME:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    invoke-interface {p1, p2}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V

    goto :goto_0

    .line 291
    :pswitch_a
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    sget-object p2, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->TECHNICAL:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    invoke-interface {p1, p2}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onSwipeForFallback(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;)V

    goto :goto_0

    .line 288
    :pswitch_b
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onCardError()V

    goto :goto_0

    .line 278
    :pswitch_c
    sget-object p1, Lcom/squareup/cardreader/PaymentFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrPaymentStandardMessage:[I

    invoke-virtual {p2}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->ordinal()I

    move-result p2

    aget p1, p1, p2

    if-eq p1, v0, :cond_1

    const/4 p2, 0x2

    if-eq p1, p2, :cond_0

    goto :goto_0

    .line 283
    :cond_0
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onUseChipCard()V

    goto :goto_0

    .line 280
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onCardRemovedDuringPayment()V

    :goto_0
    :pswitch_d
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_d
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCardPresenceChanged(ZZZ)V
    .locals 2

    .line 240
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/PaymentFeature;->onCardPresenceChanged(ZZZ)V

    if-eqz p3, :cond_0

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/Object;

    const/4 v0, 0x0

    .line 248
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, p3, v0

    const-string v0, "Card Present: %s"

    invoke-static {v0, p3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 249
    new-instance p3, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    invoke-direct {p3}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;-><init>()V

    sget-object v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->ON_CARD_PRESENCE_CHANGED:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 250
    invoke-virtual {p3, v0}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->event(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p3

    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 251
    invoke-virtual {p3, v0}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p3

    .line 252
    invoke-virtual {p3, p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->present(Z)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p3

    .line 253
    invoke-virtual {p3, p2}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->willContinuePayment(Z)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p2

    .line 254
    invoke-virtual {p2}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    move-result-object p2

    .line 255
    invoke-direct {p0, p2}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->logPaymentEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V

    .line 256
    iget-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p2, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onCardPresenceChange(Z)V

    return-void
.end method

.method public onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V
    .locals 1

    .line 499
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/PaymentFeature;->onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V

    .line 500
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onCardholderNameReceived(Lcom/squareup/cardreader/CardInfo;)V

    return-void
.end method

.method public onContactlessEmvAuthRequest([BZLcom/squareup/cardreader/CardInfo;)V
    .locals 1

    .line 478
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/PaymentFeature;->onContactlessEmvAuthRequest([BZLcom/squareup/cardreader/CardInfo;)V

    const-string v0, "NFC should not overlap with quick chip"

    .line 479
    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 480
    iget-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p2, p1, p3}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onSendContactlessAuthorization([BLcom/squareup/cardreader/CardInfo;)V

    return-void
.end method

.method public onDisplayRequest(ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 550
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/PaymentFeature;->onDisplayRequest(ILjava/lang/String;Ljava/lang/String;)V

    .line 551
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrsTmnMessage;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsTmnMessage;

    move-result-object p1

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v2, 0x1

    aput-object p2, v0, v2

    const/4 v2, 0x2

    aput-object p3, v0, v2

    const-string v2, "onDisplayRequest - message: %s amount: %s balance: %s"

    .line 552
    invoke-static {v2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 554
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onDisplayRequest(Lcom/squareup/cardreader/lcr/CrsTmnMessage;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onEmvAuthRequest([BZLcom/squareup/cardreader/CardInfo;)V
    .locals 1

    .line 471
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/PaymentFeature;->onEmvAuthRequest([BZLcom/squareup/cardreader/CardInfo;)V

    .line 472
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onSendAuthorization([BZLcom/squareup/cardreader/CardInfo;)V

    return-void
.end method

.method public onListApplications([Lcom/squareup/cardreader/EmvApplication;)V
    .locals 3

    .line 338
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/PaymentFeature;->onListApplications([Lcom/squareup/cardreader/EmvApplication;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 339
    array-length v1, p1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Listing %d Applications"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 340
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onListApplications([Lcom/squareup/cardreader/EmvApplication;)V

    return-void
.end method

.method public onPaymentComplete([BIZLcom/squareup/cardreader/CardInfo;I[Lcom/squareup/cardreader/PaymentTiming;I)V
    .locals 8

    .line 392
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Lcom/squareup/cardreader/PaymentFeature;->onPaymentComplete([BIZLcom/squareup/cardreader/CardInfo;I[Lcom/squareup/cardreader/PaymentTiming;I)V

    .line 396
    invoke-static {p2}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;

    move-result-object p2

    .line 397
    invoke-static {p5}, Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;

    move-result-object v3

    .line 400
    invoke-static {p7}, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    move-result-object p5

    .line 402
    new-instance p7, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    invoke-direct {p7}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;-><init>()V

    sget-object v0, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->ON_PAYMENT_COMPLETE:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 403
    invoke-virtual {p7, v0}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->event(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p7

    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 404
    invoke-virtual {p7, v0}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p7

    .line 405
    invoke-virtual {p7, p2}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->paymentResult(Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p7

    .line 406
    invoke-virtual {p7, v3}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->standardMessage(Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p7

    .line 407
    invoke-virtual {p7, p5}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardAction(Lcom/squareup/cardreader/lcr/CrPaymentCardAction;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p7

    .line 408
    invoke-virtual {p7, p3}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->approvedOffline(Z)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p7

    .line 409
    invoke-virtual {p7, p4}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardInfo(Lcom/squareup/cardreader/CardInfo;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p7

    .line 410
    invoke-virtual {p7}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    move-result-object p7

    .line 411
    invoke-direct {p0, p7}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->logPaymentEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V

    const/4 p7, 0x1

    new-array v0, p7, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const-string v2, "Payment Complete, Status: %s"

    .line 413
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    new-array v0, p7, [Ljava/lang/Object;

    aput-object v3, v0, v1

    const-string v2, "Payment Complete, StdMsg: %s"

    .line 414
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    new-array v0, p7, [Ljava/lang/Object;

    aput-object p5, v0, v1

    const-string v2, "Payment Complete, Action: %s"

    .line 415
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 417
    new-instance v5, Lcom/squareup/cardreader/PaymentTimings;

    invoke-direct {v5, p6}, Lcom/squareup/cardreader/PaymentTimings;-><init>([Lcom/squareup/cardreader/PaymentTiming;)V

    .line 418
    invoke-virtual {v5}, Lcom/squareup/cardreader/PaymentTimings;->iterator()Ljava/util/Iterator;

    move-result-object p6

    :goto_0
    invoke-interface {p6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/PaymentTiming;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 419
    invoke-virtual {v0}, Lcom/squareup/cardreader/PaymentTiming;->getLabel()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-virtual {v0}, Lcom/squareup/cardreader/PaymentTiming;->getDeltaMS()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, p7

    const-string v0, "Payment Timing: %s %d"

    invoke-static {v0, v2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 422
    :cond_0
    sget-object p6, Lcom/squareup/cardreader/PaymentFeatureLegacy$1;->$SwitchMap$com$squareup$cardreader$lcr$CrPaymentPaymentResult:[I

    invoke-virtual {p2}, Lcom/squareup/cardreader/lcr/CrPaymentPaymentResult;->ordinal()I

    move-result p2

    aget p2, p6, p2

    packed-switch p2, :pswitch_data_0

    goto/16 :goto_1

    .line 455
    :pswitch_0
    sget-object p1, Lcom/squareup/cardreader/lcr/CrPaymentCardAction;->CR_PAYMENT_CARD_ACTION_CONTACTLESS_CARD_ERROR_TRY_AGAIN:Lcom/squareup/cardreader/lcr/CrPaymentCardAction;

    if-ne p5, p1, :cond_1

    .line 456
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onNfcTryAgain()V

    goto :goto_1

    .line 463
    :cond_1
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    iget-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {p1, p2, v3, v5}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onPaymentTerminated(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    goto :goto_1

    .line 450
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p1, v5}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onPaymentNfcTimedOut(Lcom/squareup/cardreader/PaymentTimings;)V

    goto :goto_1

    .line 446
    :pswitch_2
    iget-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    sget-object p3, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->TECHNICAL:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    invoke-direct {p0, p1, p4}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->createSuccessfulSwipe([BLcom/squareup/cardreader/CardInfo;)Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;

    move-result-object p1

    invoke-interface {p2, p3, p1, v3}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onMagswipeFallbackSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    goto :goto_1

    .line 442
    :pswitch_3
    iget-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    sget-object p3, Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;->SCHEME:Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;

    invoke-direct {p0, p1, p4}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->createSuccessfulSwipe([BLcom/squareup/cardreader/CardInfo;)Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;

    move-result-object p1

    invoke-interface {p2, p3, p1, v3}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onMagswipeFallbackSuccess(Lcom/squareup/protos/client/bills/CardTender$Card$ChipCardFallbackIndicator;Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;Lcom/squareup/cardreader/lcr/CrPaymentStandardMessage;)V

    goto :goto_1

    .line 439
    :pswitch_4
    iget-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-direct {p0, p1, p4}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->createSuccessfulSwipe([BLcom/squareup/cardreader/CardInfo;)Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;

    move-result-object p1

    invoke-interface {p2, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onMagswipeSuccess(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    goto :goto_1

    .line 434
    :pswitch_5
    iget-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p2}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onSigRequested()V

    .line 435
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    move-object v2, p1

    move v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onICCApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V

    goto :goto_1

    .line 430
    :pswitch_6
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    move-object v2, p1

    move v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onICCApproved(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;ZLcom/squareup/cardreader/PaymentTimings;)V

    goto :goto_1

    .line 427
    :pswitch_7
    iget-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    iget-object p3, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {p2, p3, p1, v3, v5}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onICCReversed(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    goto :goto_1

    .line 424
    :pswitch_8
    iget-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    iget-object p3, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-interface {p2, p3, p1, v3, v5}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onICCDeclined(Lcom/squareup/cardreader/CardReaderInfo;[BLcom/squareup/cardreader/lcr/CrPaymentStandardMessage;Lcom/squareup/cardreader/PaymentTimings;)V

    :goto_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onSwipePassthrough([BLcom/squareup/cardreader/CardInfo;)V
    .locals 1

    .line 493
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/PaymentFeature;->onSwipePassthrough([BLcom/squareup/cardreader/CardInfo;)V

    .line 494
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->createSuccessfulSwipe([BLcom/squareup/cardreader/CardInfo;)Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onMagSwipePassthrough(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    return-void
.end method

.method public onTmnAuthRequest([B)V
    .locals 2

    .line 485
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/PaymentFeature;->onTmnAuthRequest([B)V

    .line 486
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    invoke-virtual {v0}, Lcom/squareup/tmn/TmnTimings;->tapped()V

    .line 487
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_ON_TMN_AUTH_REQUEST:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 488
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onSendTmnAuthorization([B)V

    return-void
.end method

.method public onTmnDataToTmn(Ljava/lang/String;[B)V
    .locals 2

    .line 505
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/PaymentFeature;->onTmnDataToTmn(Ljava/lang/String;[B)V

    .line 506
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_ON_TMN_DATA_TO_TMN:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    const/4 v0, 0x0

    const/16 v1, 0x20

    .line 511
    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 512
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onTmnDataToTmn(Ljava/lang/String;[B)V

    return-void
.end method

.method public onTmnTransactionComplete(I[Lcom/squareup/cardreader/PaymentTiming;)V
    .locals 6

    .line 527
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1, p2}, Lcom/squareup/cardreader/PaymentFeature;->onTmnTransactionComplete(I[Lcom/squareup/cardreader/PaymentTiming;)V

    .line 528
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_ON_TMN_TRANSACTION_COMPLETE:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 530
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/TmnTransactionResult;->swigToEnum(I)Lcom/squareup/cardreader/lcr/TmnTransactionResult;

    move-result-object p1

    .line 531
    new-instance v0, Lcom/squareup/cardreader/PaymentTimings;

    invoke-direct {v0, p2}, Lcom/squareup/cardreader/PaymentTimings;-><init>([Lcom/squareup/cardreader/PaymentTiming;)V

    const/4 p2, 0x1

    new-array v1, p2, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "TMN: onTmnTransactionComplete %s"

    .line 532
    invoke-static {v3, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 533
    invoke-virtual {v0}, Lcom/squareup/cardreader/PaymentTimings;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/cardreader/PaymentTiming;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 534
    invoke-virtual {v3}, Lcom/squareup/cardreader/PaymentTiming;->getLabel()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v3}, Lcom/squareup/cardreader/PaymentTiming;->getDeltaMS()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v4, p2

    const-string v3, "TMN: Payment Timing %s %d"

    invoke-static {v3, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 537
    :cond_0
    new-instance p2, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    invoke-direct {p2}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;-><init>()V

    sget-object v1, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;->ON_TMN_PAYMENT_COMPLETE:Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;

    .line 538
    invoke-virtual {p2, v1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->event(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Event;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p2

    .line 539
    invoke-virtual {p2, p1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->tmnTransactionResult(Lcom/squareup/cardreader/lcr/TmnTransactionResult;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p2

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 540
    invoke-virtual {p2, v1}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->cardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;

    move-result-object p2

    .line 541
    invoke-virtual {p2}, Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent$Builder;->build()Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;

    move-result-object p2

    .line 543
    invoke-direct {p0, p2}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->logPaymentEvent(Lcom/squareup/cardreader/ReaderEventLogger$PaymentFeatureEvent;)V

    .line 544
    iget-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReaderInfo;->setNotInPayment()V

    .line 545
    iget-object p2, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {p2, p1, v0}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onTmnTransactionComplete(Lcom/squareup/cardreader/lcr/TmnTransactionResult;Lcom/squareup/cardreader/PaymentTimings;)V

    return-void
.end method

.method public onTmnWriteNotify(II[B)V
    .locals 2

    .line 517
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->delegate:Lcom/squareup/cardreader/PaymentFeature;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/PaymentFeature;->onTmnWriteNotify(II[B)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "TMN ontmnwritenotify"

    .line 518
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 519
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_ON_TMN_WRITE_NOTIFY:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 520
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;->onTmnWriteNotify(II[B)V

    return-void
.end method

.method public processARPC([B)V
    .locals 2

    .line 176
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->payment_process_server_response(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    return-void
.end method

.method public resetEmvFeature()V
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    if-eqz v0, :cond_0

    .line 197
    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->cr_payment_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    .line 198
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->cr_payment_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    const/4 v0, 0x0

    .line 199
    iput-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    .line 200
    iput-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PaymentFeatureLegacy$PaymentFeatureListener;

    :cond_0
    return-void
.end method

.method public selectAccountType(Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)V
    .locals 2

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Selected account type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 206
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->payment_select_account_type(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;Lcom/squareup/cardreader/lcr/CrPaymentAccountType;)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    return-void
.end method

.method public selectApplication(Lcom/squareup/cardreader/EmvApplication;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 171
    invoke-virtual {p1}, Lcom/squareup/cardreader/EmvApplication;->getLabel()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Selected Application: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 172
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    invoke-virtual {p1}, Lcom/squareup/cardreader/EmvApplication;->getAdfName()[B

    move-result-object p1

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->payment_select_application(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    return-void
.end method

.method public sendReaderPowerupHint(I)V
    .locals 2

    .line 192
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->payment_send_powerup_hint(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;I)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    return-void
.end method

.method public sendTmnBytesToReader([B)V
    .locals 2

    .line 210
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_TMN_BYTES_TO_READER:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 211
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->payment_tmn_send_bytes_to_reader(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    return-void
.end method

.method public startPaymentInteraction(Lcom/squareup/cardreader/PaymentInteraction;)V
    .locals 8

    .line 88
    instance-of v0, p1, Lcom/squareup/cardreader/TmnPaymentInteraction;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-array v0, v1, [Ljava/lang/Object;

    const-string v2, "TMN: startPaymentInteraction"

    .line 89
    invoke-static {v2, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    check-cast p1, Lcom/squareup/cardreader/TmnPaymentInteraction;

    .line 91
    invoke-virtual {p1}, Lcom/squareup/cardreader/TmnPaymentInteraction;->getRequestType()Lcom/squareup/cardreader/lcr/CrsTmnRequestType;

    move-result-object v3

    .line 92
    invoke-virtual {p1}, Lcom/squareup/cardreader/TmnPaymentInteraction;->getTransactionId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/squareup/cardreader/TmnPaymentInteraction;->getBrandId()Lcom/squareup/cardreader/lcr/CrsTmnBrandId;

    move-result-object v5

    .line 93
    invoke-virtual {p1}, Lcom/squareup/cardreader/TmnPaymentInteraction;->getAmountAuthorized()J

    move-result-wide v6

    move-object v2, p0

    .line 91
    invoke-direct/range {v2 .. v7}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->startTmnPaymentInteraction(Lcom/squareup/cardreader/lcr/CrsTmnRequestType;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrsTmnBrandId;J)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    goto :goto_0

    .line 94
    :cond_0
    instance-of v0, p1, Lcom/squareup/cardreader/EmvPaymentInteraction;

    if-eqz v0, :cond_2

    .line 95
    check-cast p1, Lcom/squareup/cardreader/EmvPaymentInteraction;

    .line 96
    invoke-virtual {p1}, Lcom/squareup/cardreader/EmvPaymentInteraction;->getTransactionType()Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;

    move-result-object v3

    .line 97
    invoke-virtual {p1}, Lcom/squareup/cardreader/EmvPaymentInteraction;->getAmountAuthorized()J

    move-result-wide v4

    .line 98
    invoke-virtual {p1}, Lcom/squareup/cardreader/EmvPaymentInteraction;->getCurrentTimeMillis()J

    move-result-wide v6

    move-object v2, p0

    .line 96
    invoke-direct/range {v2 .. v7}, Lcom/squareup/cardreader/PaymentFeatureLegacy;->startEmvPaymentInteraction(Lcom/squareup/cardreader/lcr/CrPaymentTransactionType;JJ)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    move-result-object p1

    .line 104
    :goto_0
    sget-object v0, Lcom/squareup/cardreader/lcr/CrPaymentResult;->CR_PAYMENT_RESULT_SUCCESS:Lcom/squareup/cardreader/lcr/CrPaymentResult;

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v1

    const-string p1, "PaymentFeature.startPayment failed with %s"

    .line 107
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    return-void

    .line 100
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported class type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public startTmnMiryo([B)V
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->tmnTimings:Lcom/squareup/tmn/TmnTimings;

    sget-object v1, Lcom/squareup/tmn/What;->PAYMENT_FEATURE_TMN_START_MIRYO:Lcom/squareup/tmn/What;

    invoke-virtual {v0, v1}, Lcom/squareup/tmn/TmnTimings;->event(Lcom/squareup/tmn/What;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->paymentFeatureNative:Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PaymentFeatureLegacy;->featurePointer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cardreader/lcr/PaymentFeatureNativeInterface;->payment_tmn_start_miryo(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_payment_t;[B)Lcom/squareup/cardreader/lcr/CrPaymentResult;

    return-void
.end method
