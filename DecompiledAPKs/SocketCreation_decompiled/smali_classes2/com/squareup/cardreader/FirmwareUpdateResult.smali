.class public final enum Lcom/squareup/cardreader/FirmwareUpdateResult;
.super Ljava/lang/Enum;
.source "FirmwareUpdateResult.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/cardreader/FirmwareUpdateResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum BAD_ARGUMENT:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum BAD_ENCRYPTION_KEY:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum BAD_HEADER:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum BAD_WRITE_ALIGNMENT:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum DECRYPTION_FAILURE:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum DUPLICATE_UPDATE_TO_SLOT:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum ENCRYPTED_UPDATE_REQUIRED:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum ERROR_UNKNOWN:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum FLASH_FAILURE:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum INVALID_IMAGE:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum INVALID_IMAGE_VERSION:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum NONE:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum NOT_INITIALIZED:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum SEND_DATA:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum SEND_DATA_INVALID_IMAGE:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum SEND_DATA_INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum SEND_DATA_INVALID_IMAGE_WILL_RETRY:Lcom/squareup/cardreader/FirmwareUpdateResult;

.field public static final enum SUCCESS:Lcom/squareup/cardreader/FirmwareUpdateResult;


# instance fields
.field private final message:Ljava/lang/String;

.field private final nativeIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 8
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/4 v1, 0x0

    const-string v2, "SUCCESS"

    const-string v3, "Success"

    invoke-direct {v0, v2, v1, v1, v3}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->SUCCESS:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 9
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/4 v2, 0x1

    const-string v3, "SEND_DATA"

    const-string v4, "Send data"

    invoke-direct {v0, v3, v2, v2, v4}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->SEND_DATA:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 10
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/4 v3, 0x2

    const-string v4, "SEND_DATA_INVALID_IMAGE_HEADER"

    const-string v5, "Send data; Invalid image header"

    invoke-direct {v0, v4, v3, v3, v5}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 11
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/4 v4, 0x3

    const-string v5, "SEND_DATA_INVALID_IMAGE"

    const-string v6, "Send data; Invalid image"

    invoke-direct {v0, v5, v4, v4, v6}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 12
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/4 v5, 0x4

    const-string v6, "SEND_DATA_INVALID_IMAGE_WILL_RETRY"

    const-string v7, "Send data; Invalid image; Will retry"

    invoke-direct {v0, v6, v5, v5, v7}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE_WILL_RETRY:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 13
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/4 v6, 0x5

    const-string v7, "NOT_INITIALIZED"

    const-string v8, "Not initialized"

    invoke-direct {v0, v7, v6, v6, v8}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->NOT_INITIALIZED:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 14
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/4 v7, 0x6

    const-string v8, "INVALID_IMAGE_HEADER"

    const-string v9, "Invalid image header"

    invoke-direct {v0, v8, v7, v7, v9}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 15
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/4 v8, 0x7

    const-string v9, "INVALID_IMAGE"

    const-string v10, "Invalid image"

    invoke-direct {v0, v9, v8, v8, v10}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->INVALID_IMAGE:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 16
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v9, 0x8

    const-string v10, "DECRYPTION_FAILURE"

    const-string v11, "Decryption failure"

    invoke-direct {v0, v10, v9, v9, v11}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->DECRYPTION_FAILURE:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 17
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v10, 0x9

    const-string v11, "FLASH_FAILURE"

    const-string v12, "Flash failure"

    invoke-direct {v0, v11, v10, v10, v12}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->FLASH_FAILURE:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 18
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v11, 0xa

    const-string v12, "BAD_ARGUMENT"

    const-string v13, "Bad argument"

    invoke-direct {v0, v12, v11, v11, v13}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->BAD_ARGUMENT:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 19
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v12, 0xb

    const-string v13, "BAD_HEADER"

    const-string v14, "Bad header"

    invoke-direct {v0, v13, v12, v12, v14}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->BAD_HEADER:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 20
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v13, 0xc

    const-string v14, "BAD_WRITE_ALIGNMENT"

    const-string v15, "Bad write alignment"

    invoke-direct {v0, v14, v13, v13, v15}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->BAD_WRITE_ALIGNMENT:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 21
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v14, 0xd

    const-string v15, "BAD_ENCRYPTION_KEY"

    const-string v13, "Bad encryption key"

    invoke-direct {v0, v15, v14, v14, v13}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->BAD_ENCRYPTION_KEY:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 22
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v13, 0xe

    const-string v15, "DUPLICATE_UPDATE_TO_SLOT"

    const-string v14, "Duplicate update to slot"

    invoke-direct {v0, v15, v13, v13, v14}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->DUPLICATE_UPDATE_TO_SLOT:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 23
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const-string v14, "ENCRYPTED_UPDATE_REQUIRED"

    const/16 v15, 0xf

    const/16 v13, 0xf

    const-string v12, "Attempting to update a production unit with a plaintext image"

    invoke-direct {v0, v14, v15, v13, v12}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->ENCRYPTED_UPDATE_REQUIRED:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 24
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const-string v12, "ERROR_UNKNOWN"

    const/16 v13, 0x10

    const/16 v14, 0x10

    const-string v15, "Unknown error"

    invoke-direct {v0, v12, v13, v14, v15}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->ERROR_UNKNOWN:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 25
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const-string v12, "INVALID_IMAGE_VERSION"

    const/16 v13, 0x11

    const/16 v14, 0x11

    const-string v15, "Invalid image version"

    invoke-direct {v0, v12, v13, v14, v15}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->INVALID_IMAGE_VERSION:Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 26
    new-instance v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    const-string v12, "NONE"

    const/16 v13, 0x12

    const/16 v14, 0xff

    const-string v15, "None"

    invoke-direct {v0, v12, v13, v14, v15}, Lcom/squareup/cardreader/FirmwareUpdateResult;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->NONE:Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v0, 0x13

    new-array v0, v0, [Lcom/squareup/cardreader/FirmwareUpdateResult;

    .line 7
    sget-object v12, Lcom/squareup/cardreader/FirmwareUpdateResult;->SUCCESS:Lcom/squareup/cardreader/FirmwareUpdateResult;

    aput-object v12, v0, v1

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->SEND_DATA:Lcom/squareup/cardreader/FirmwareUpdateResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/FirmwareUpdateResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE:Lcom/squareup/cardreader/FirmwareUpdateResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->SEND_DATA_INVALID_IMAGE_WILL_RETRY:Lcom/squareup/cardreader/FirmwareUpdateResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->NOT_INITIALIZED:Lcom/squareup/cardreader/FirmwareUpdateResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->INVALID_IMAGE_HEADER:Lcom/squareup/cardreader/FirmwareUpdateResult;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->INVALID_IMAGE:Lcom/squareup/cardreader/FirmwareUpdateResult;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->DECRYPTION_FAILURE:Lcom/squareup/cardreader/FirmwareUpdateResult;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->FLASH_FAILURE:Lcom/squareup/cardreader/FirmwareUpdateResult;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->BAD_ARGUMENT:Lcom/squareup/cardreader/FirmwareUpdateResult;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->BAD_HEADER:Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v2, 0xb

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->BAD_WRITE_ALIGNMENT:Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->BAD_ENCRYPTION_KEY:Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->DUPLICATE_UPDATE_TO_SLOT:Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->ENCRYPTED_UPDATE_REQUIRED:Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->ERROR_UNKNOWN:Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->INVALID_IMAGE_VERSION:Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/cardreader/FirmwareUpdateResult;->NONE:Lcom/squareup/cardreader/FirmwareUpdateResult;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->$VALUES:[Lcom/squareup/cardreader/FirmwareUpdateResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput p3, p0, Lcom/squareup/cardreader/FirmwareUpdateResult;->nativeIndex:I

    .line 33
    iput-object p4, p0, Lcom/squareup/cardreader/FirmwareUpdateResult;->message:Ljava/lang/String;

    return-void
.end method

.method public static getByIntegerValue(I)Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;
    .locals 6

    .line 49
    invoke-static {}, Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;->values()[Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    .line 50
    invoke-virtual {v4}, Lcom/squareup/cardreader/lcr/CrFirmwareFeatureResult;->swigValue()I

    move-result v5

    if-ne p0, v5, :cond_0

    return-object v4

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 56
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v0, v2

    const-string p0, "Unknown firmware update result from reader: %d"

    invoke-static {p0, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 p0, 0x0

    return-object p0
.end method

.method static isSuccess(I)Z
    .locals 0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/cardreader/FirmwareUpdateResult;
    .locals 1

    .line 7
    const-class v0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/cardreader/FirmwareUpdateResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/cardreader/FirmwareUpdateResult;
    .locals 1

    .line 7
    sget-object v0, Lcom/squareup/cardreader/FirmwareUpdateResult;->$VALUES:[Lcom/squareup/cardreader/FirmwareUpdateResult;

    invoke-virtual {v0}, [Lcom/squareup/cardreader/FirmwareUpdateResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/cardreader/FirmwareUpdateResult;

    return-object v0
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/cardreader/FirmwareUpdateResult;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getNativeIndex()I
    .locals 1

    .line 37
    iget v0, p0, Lcom/squareup/cardreader/FirmwareUpdateResult;->nativeIndex:I

    return v0
.end method
