.class public Lcom/squareup/cardreader/PowerFeatureLegacy;
.super Ljava/lang/Object;
.source "PowerFeatureLegacy.java"

# interfaces
.implements Lcom/squareup/cardreader/PowerFeature;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/cardreader/PowerFeatureLegacy$PowerListener;
    }
.end annotation


# instance fields
.field private apiListener:Lcom/squareup/cardreader/PowerFeatureLegacy$PowerListener;

.field private powerFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

.field private final powerFeatureNative:Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;

.field private final sessionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;",
            ")V"
        }
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->sessionProvider:Ljavax/inject/Provider;

    .line 18
    iput-object p2, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->powerFeatureNative:Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;

    return-void
.end method


# virtual methods
.method public initialize(Lcom/squareup/cardreader/PowerFeatureLegacy$PowerListener;)V
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->powerFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    .line 27
    iput-object p1, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PowerFeatureLegacy$PowerListener;

    .line 29
    iget-object p1, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->powerFeatureNative:Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;

    iget-object v0, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->sessionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v0

    invoke-interface {p1, v0, p0}, Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;->power_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->powerFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    return-void

    .line 26
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "apiListener cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 23
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "PowerService is already initialized!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onPowerStatus(IIIIZI)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 61
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Power Status is at %d"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    new-instance v0, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;-><init>()V

    .line 64
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->setPercentage(Ljava/lang/Integer;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    move-result-object p1

    .line 65
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->setCurrent(Ljava/lang/Integer;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    move-result-object p1

    .line 66
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->setVoltage(Ljava/lang/Integer;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    move-result-object p1

    .line 67
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->setTemperature(Ljava/lang/Integer;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    move-result-object p1

    .line 68
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->setCritical(Ljava/lang/Boolean;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    move-result-object p1

    .line 69
    invoke-static {p6}, Lcom/squareup/cardreader/lcr/CrsBatteryMode;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrsBatteryMode;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->setBatteryMode(Lcom/squareup/cardreader/lcr/CrsBatteryMode;)Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;

    move-result-object p1

    .line 70
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderBatteryInfo$Builder;->build()Lcom/squareup/cardreader/CardReaderBatteryInfo;

    move-result-object p1

    .line 71
    iget-object p2, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->apiListener:Lcom/squareup/cardreader/PowerFeatureLegacy$PowerListener;

    invoke-interface {p2, p1}, Lcom/squareup/cardreader/PowerFeatureLegacy$PowerListener;->onPowerReceived(Lcom/squareup/cardreader/CardReaderBatteryInfo;)V

    return-void
.end method

.method public powerOff()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->powerFeatureNative:Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->powerFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;->cr_power_off(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;

    return-void
.end method

.method public requestPowerStatus()V
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->powerFeatureNative:Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->powerFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;->cr_power_get_battery_voltage(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;

    return-void
.end method

.method public resetPowerFeature()V
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->powerFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    if-eqz v0, :cond_0

    .line 34
    iget-object v1, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->powerFeatureNative:Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;

    invoke-interface {v1, v0}, Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;->cr_power_term(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;

    .line 35
    iget-object v0, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->powerFeatureNative:Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;

    iget-object v1, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->powerFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/lcr/PowerFeatureNativeInterface;->cr_power_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;)Lcom/squareup/cardreader/lcr/CrPowerResult;

    const/4 v0, 0x0

    .line 36
    iput-object v0, p0, Lcom/squareup/cardreader/PowerFeatureLegacy;->powerFeature:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_power_t;

    :cond_0
    return-void
.end method
