.class Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;
.super Ljava/lang/Object;
.source "ReaderBatteryStatusHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Holder"
.end annotation


# instance fields
.field private final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

.field private final isCharging:Z


# direct methods
.method private constructor <init>(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 164
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->isBatteryCharging()Z

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;-><init>(Lcom/squareup/cardreader/CardReaderInfo;Z)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$1;)V
    .locals 0

    .line 154
    invoke-direct {p0, p1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;-><init>(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method private constructor <init>(Lcom/squareup/cardreader/CardReaderInfo;Z)V
    .locals 0

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 160
    iput-boolean p2, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;->isCharging:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/cardreader/CardReaderInfo;ZLcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$1;)V
    .locals 0

    .line 154
    invoke-direct {p0, p1, p2}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;-><init>(Lcom/squareup/cardreader/CardReaderInfo;Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;)Z
    .locals 0

    .line 154
    iget-boolean p0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;->isCharging:Z

    return p0
.end method

.method static synthetic access$400(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;)Lcom/squareup/cardreader/CardReaderInfo;
    .locals 0

    .line 154
    iget-object p0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$Holder;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-object p0
.end method
