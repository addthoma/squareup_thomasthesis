.class Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$2;
.super Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;
.source "ReaderBatteryStatusHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;


# direct methods
.method constructor <init>(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;)V
    .locals 1

    .line 270
    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$2;->this$0:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$HudBatteryHelper;-><init>(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$1;)V

    return-void
.end method


# virtual methods
.method public getBatteryNormalText()Ljava/lang/CharSequence;
    .locals 2

    .line 296
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler$2;->this$0:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    invoke-static {v0}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->access$000(Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;)Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/ui/R$string;->hud_contactless_chip_reader_connected:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getCharging()I
    .locals 1

    .line 272
    sget v0, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_r12_reader_charging_battery_120:I

    return v0
.end method

.method getErrorScreenType()Lcom/squareup/cardreader/ui/api/ReaderWarningType;
    .locals 1

    .line 292
    sget-object v0, Lcom/squareup/cardreader/ui/api/ReaderWarningType;->R12_LOW_BATTERY:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    return-object v0
.end method

.method getFullBattery()I
    .locals 1

    .line 276
    sget v0, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_r12_reader_full_battery_120:I

    return v0
.end method

.method getHighBattery()I
    .locals 1

    .line 280
    sget v0, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_r12_reader_high_battery_120:I

    return v0
.end method

.method getLowBattery()I
    .locals 1

    .line 288
    sget v0, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_r12_reader_low_battery_120:I

    return v0
.end method

.method getMidBattery()I
    .locals 1

    .line 284
    sget v0, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_r12_reader_mid_battery_120:I

    return v0
.end method
