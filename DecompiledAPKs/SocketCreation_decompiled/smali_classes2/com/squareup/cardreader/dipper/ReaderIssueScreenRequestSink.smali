.class public final Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;
.super Ljava/lang/Object;
.source "ReaderIssueScreenRequestSink.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\n\u001a\u00020\u0007J-\u0010\u0010\u001a\u00020\u000b2%\u0010\u0011\u001a!\u0012\u0013\u0012\u00110\u0007\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\n\u0012\u0004\u0012\u00020\u000b0\u0006j\u0002`\u000cJ-\u0010\u0012\u001a\u00020\u000b2%\u0010\u0013\u001a!\u0012\u0013\u0012\u00110\u0007\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\n\u0012\u0004\u0012\u00020\u000b0\u0006j\u0002`\u000cR-\u0010\u0005\u001a!\u0012\u0013\u0012\u00110\u0007\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\n\u0012\u0004\u0012\u00020\u000b0\u0006j\u0002`\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R3\u0010\r\u001a\'\u0012#\u0012!\u0012\u0013\u0012\u00110\u0007\u00a2\u0006\u000c\u0008\u0008\u0012\u0008\u0008\t\u0012\u0004\u0008\u0008(\n\u0012\u0004\u0012\u00020\u000b0\u0006j\u0002`\u000c0\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
        "",
        "navigator",
        "Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;",
        "(Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;)V",
        "defaultIssueHandler",
        "Lkotlin/Function1;",
        "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;",
        "Lkotlin/ParameterName;",
        "name",
        "issueScreen",
        "",
        "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestHandler;",
        "issueHandler",
        "Lcom/squareup/ui/StackedDelegate;",
        "requestReaderIssueScreen",
        "setReaderIssueScreenRequestHandler",
        "newHandlerIssue",
        "unsetReaderIssueScreenRequestHandler",
        "unsetHandlerIssue",
        "cardreader-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final defaultIssueHandler:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private issueHandler:Lcom/squareup/ui/StackedDelegate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/StackedDelegate<",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end field

.field private final navigator:Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "navigator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->navigator:Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;

    .line 19
    new-instance p1, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink$defaultIssueHandler$1;

    invoke-direct {p1, p0}, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink$defaultIssueHandler$1;-><init>(Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->defaultIssueHandler:Lkotlin/jvm/functions/Function1;

    .line 22
    new-instance p1, Lcom/squareup/ui/StackedDelegate;

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/jvm/functions/Function1;

    iget-object v1, p0, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->defaultIssueHandler:Lkotlin/jvm/functions/Function1;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p1, v0}, Lcom/squareup/ui/StackedDelegate;-><init>([Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->issueHandler:Lcom/squareup/ui/StackedDelegate;

    return-void
.end method

.method public static final synthetic access$getNavigator$p(Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;)Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->navigator:Lcom/squareup/cardreader/dipper/ReaderIssueNavigator;

    return-object p0
.end method


# virtual methods
.method public final requestReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V
    .locals 1

    const-string v0, "issueScreen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->issueHandler:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {v0}, Lcom/squareup/ui/StackedDelegate;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final setReaderIssueScreenRequestHandler(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "newHandlerIssue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->issueHandler:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StackedDelegate;->registerDelegate(Ljava/lang/Object;)V

    return-void
.end method

.method public final unsetReaderIssueScreenRequestHandler(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "unsetHandlerIssue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;->issueHandler:Lcom/squareup/ui/StackedDelegate;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StackedDelegate;->unregisterDelegate(Ljava/lang/Object;)V

    return-void
.end method
