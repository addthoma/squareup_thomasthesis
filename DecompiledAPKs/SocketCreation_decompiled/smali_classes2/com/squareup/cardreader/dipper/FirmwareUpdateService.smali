.class public interface abstract Lcom/squareup/cardreader/dipper/FirmwareUpdateService;
.super Ljava/lang/Object;
.source "FirmwareUpdateService.java"


# virtual methods
.method public abstract send(Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/tarkin/AssetUpdateRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/tarkin/AssetUpdateResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/reader/asset-update"
    .end annotation
.end method
