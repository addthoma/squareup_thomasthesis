.class public abstract Lcom/squareup/cardreader/dagger/CardReaderCompleteModule;
.super Ljava/lang/Object;
.source "CardReaderCompleteModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/cardreader/ble/GlobalBleModule;,
        Lcom/squareup/cardreader/GlobalCardReaderModule;,
        Lcom/squareup/cardreader/GlobalHeadsetModule;,
        Lcom/squareup/cardreader/RemoteCardReaderModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract isReaderSdkApp(Z)Z
    .annotation runtime Lcom/squareup/cardreader/dagger/IsReaderSdkAppForDipper;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
