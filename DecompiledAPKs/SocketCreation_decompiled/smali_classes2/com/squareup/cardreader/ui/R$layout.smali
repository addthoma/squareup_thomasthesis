.class public final Lcom/squareup/cardreader/ui/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/cardreader/ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final audio_permission_card_view:I = 0x7f0d0067

.field public static final audio_permission_layout:I = 0x7f0d0068

.field public static final audio_permission_screen_view:I = 0x7f0d0069

.field public static final card_reader_detail_card_view:I = 0x7f0d00bb

.field public static final card_reader_detail_layout:I = 0x7f0d00bc

.field public static final card_reader_status_row:I = 0x7f0d00be

.field public static final card_readers_card_view:I = 0x7f0d00bf

.field public static final cardreaders_ble_footer_view:I = 0x7f0d00c1

.field public static final cardreaders_header_view:I = 0x7f0d00c2

.field public static final dark_warning_content:I = 0x7f0d01b0

.field public static final dark_warning_view:I = 0x7f0d01b1

.field public static final pairing_header_view:I = 0x7f0d041e

.field public static final pairing_help_row_view:I = 0x7f0d041f

.field public static final pairing_help_view:I = 0x7f0d0420

.field public static final pairing_view:I = 0x7f0d0421

.field public static final r12_pairing_header_content_view:I = 0x7f0d0463

.field public static final reader_battery_hud:I = 0x7f0d0468

.field public static final reader_warning_view:I = 0x7f0d046b

.field public static final warning_content:I = 0x7f0d058b

.field public static final warning_view:I = 0x7f0d058c


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
