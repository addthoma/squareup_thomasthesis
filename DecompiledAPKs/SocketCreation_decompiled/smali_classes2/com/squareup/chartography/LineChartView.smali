.class public final Lcom/squareup/chartography/LineChartView;
.super Lcom/squareup/chartography/ChartView;
.source "LineChartView.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLineChartView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LineChartView.kt\ncom/squareup/chartography/LineChartView\n*L\n1#1,137:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\n\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0018\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0007H\u0002J(\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001c\u001a\u00020\u000c2\u0006\u0010\u001d\u001a\u00020\u000c2\u0006\u0010\u001e\u001a\u00020\u0007H\u0002J\u0010\u0010\u001f\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0014R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u00020\u000c8TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0012\u001a\u00020\u000c2\u0006\u0010\u0011\u001a\u00020\u000c8B@BX\u0082\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0013\u0010\u000f\"\u0004\u0008\u0014\u0010\u0015\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/chartography/LineChartView;",
        "Lcom/squareup/chartography/ChartView;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "defStyleAttr",
        "",
        "(Landroid/content/Context;Landroid/util/AttributeSet;I)V",
        "dotPaint",
        "Landroid/graphics/Paint;",
        "dotRadius",
        "",
        "headerSpacing",
        "getHeaderSpacing",
        "()F",
        "linePaint",
        "value",
        "lineWidth",
        "getLineWidth",
        "setLineWidth",
        "(F)V",
        "drawData",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "dimension",
        "drawDot",
        "x",
        "y",
        "color",
        "onDrawData",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final dotPaint:Landroid/graphics/Paint;

.field private dotRadius:F

.field private final linePaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/chartography/LineChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/chartography/LineChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/chartography/ChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/chartography/LineChartView;->dotPaint:Landroid/graphics/Paint;

    .line 24
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/chartography/LineChartView;->linePaint:Landroid/graphics/Paint;

    .line 27
    iget-object v0, p0, Lcom/squareup/chartography/LineChartView;->dotPaint:Landroid/graphics/Paint;

    .line 28
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getPixel$public_release()Lcom/squareup/chartography/util/Pixel;

    move-result-object v2

    const/high16 v3, 0x3fc00000    # 1.5f

    invoke-virtual {v2, v3}, Lcom/squareup/chartography/util/Pixel;->dp(F)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 29
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/chartography/LineChartView;->linePaint:Landroid/graphics/Paint;

    .line 33
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 34
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 35
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getPixel$public_release()Lcom/squareup/chartography/util/Pixel;

    move-result-object v1

    const v2, 0x3f4ccccd    # 0.8f

    invoke-virtual {v1, v2}, Lcom/squareup/chartography/util/Pixel;->dp(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    .line 40
    sget-object v0, Lcom/squareup/chartography/R$styleable;->LineChartView:[I

    const/4 v1, 0x0

    .line 38
    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 45
    :try_start_0
    sget p2, Lcom/squareup/chartography/R$styleable;->LineChartView_dotSize:I

    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getPixel$public_release()Lcom/squareup/chartography/util/Pixel;

    move-result-object p3

    const/high16 v0, 0x40c00000    # 6.0f

    invoke-virtual {p3, v0}, Lcom/squareup/chartography/util/Pixel;->dp(F)F

    move-result p3

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    const/4 p3, 0x2

    int-to-float p3, p3

    div-float/2addr p2, p3

    iput p2, p0, Lcom/squareup/chartography/LineChartView;->dotRadius:F

    .line 46
    sget p2, Lcom/squareup/chartography/R$styleable;->LineChartView_lineSize:I

    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getPixel$public_release()Lcom/squareup/chartography/util/Pixel;

    move-result-object p3

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p3, v0}, Lcom/squareup/chartography/util/Pixel;->dp(F)F

    move-result p3

    invoke-virtual {p1, p2, p3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result p2

    invoke-direct {p0, p2}, Lcom/squareup/chartography/LineChartView;->setLineWidth(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :catchall_0
    move-exception p2

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw p2
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 12
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 13
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/chartography/LineChartView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final drawData(Landroid/graphics/Canvas;I)V
    .locals 9

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 77
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getAdapter()Lcom/squareup/chartography/ChartAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartAdapter;->getDomainNumSteps()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getAdapter()Lcom/squareup/chartography/ChartAdapter;

    move-result-object v4

    invoke-virtual {v4, v3, p2}, Lcom/squareup/chartography/ChartAdapter;->rangePercentage(II)D

    move-result-wide v4

    double-to-float v4, v4

    .line 80
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getHeight()F

    move-result v5

    mul-float v5, v5, v4

    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getRangeScalingFactor()F

    move-result v4

    mul-float v5, v5, v4

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getX()F

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v3

    const/4 v4, 0x2

    int-to-float v4, v4

    div-float/2addr v3, v4

    add-float/2addr v1, v3

    .line 84
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getY()F

    move-result v3

    .line 87
    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 88
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->floatValue()F

    move-result v6

    sub-float v6, v3, v6

    invoke-virtual {v5, v1, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 90
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v6

    add-float/2addr v1, v6

    .line 91
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getAdapter()Lcom/squareup/chartography/ChartAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/chartography/ChartAdapter;->getDomainNumSteps()I

    move-result v6

    const/4 v7, 0x1

    :goto_1
    if-ge v7, v6, :cond_1

    .line 92
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Number;

    invoke-virtual {v8}, Ljava/lang/Number;->floatValue()F

    move-result v8

    sub-float v8, v3, v8

    invoke-virtual {v5, v1, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 93
    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Number;

    invoke-virtual {v8}, Ljava/lang/Number;->floatValue()F

    move-result v8

    sub-float v8, v3, v8

    invoke-virtual {v5, v1, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 94
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v8

    invoke-virtual {v8}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v8

    add-float/2addr v1, v8

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 98
    :cond_1
    iget-object v1, p0, Lcom/squareup/chartography/LineChartView;->linePaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getColorWheel$public_release()Lcom/squareup/chartography/util/ColorWheel;

    move-result-object v6

    invoke-virtual {v6, p2}, Lcom/squareup/chartography/util/ColorWheel;->color(I)I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 100
    iget-object v1, p0, Lcom/squareup/chartography/LineChartView;->linePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 101
    invoke-virtual {v5}, Landroid/graphics/Path;->close()V

    .line 105
    iget v1, p0, Lcom/squareup/chartography/LineChartView;->dotRadius:F

    mul-float v1, v1, v4

    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v5

    cmpg-float v1, v1, v5

    if-gez v1, :cond_3

    .line 108
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getX()F

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v5

    div-float/2addr v5, v4

    add-float/2addr v1, v5

    .line 109
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getAdapter()Lcom/squareup/chartography/ChartAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/chartography/ChartAdapter;->getDomainNumSteps()I

    move-result v4

    :goto_2
    if-ge v2, v4, :cond_3

    .line 110
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getSelectedDomain()I

    move-result v5

    if-ne v2, v5, :cond_2

    .line 111
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v5

    sub-float v5, v3, v5

    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getSelectedColorWheel$public_release()Lcom/squareup/chartography/util/ColorWheel;

    move-result-object v6

    invoke-virtual {v6, p2}, Lcom/squareup/chartography/util/ColorWheel;->color(I)I

    move-result v6

    invoke-direct {p0, p1, v1, v5, v6}, Lcom/squareup/chartography/LineChartView;->drawDot(Landroid/graphics/Canvas;FFI)V

    goto :goto_3

    .line 113
    :cond_2
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v5

    sub-float v5, v3, v5

    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getColorWheel$public_release()Lcom/squareup/chartography/util/ColorWheel;

    move-result-object v6

    invoke-virtual {v6, p2}, Lcom/squareup/chartography/util/ColorWheel;->color(I)I

    move-result v6

    invoke-direct {p0, p1, v1, v5, v6}, Lcom/squareup/chartography/LineChartView;->drawDot(Landroid/graphics/Canvas;FFI)V

    .line 115
    :goto_3
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v5

    add-float/2addr v1, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    return-void
.end method

.method private final drawDot(Landroid/graphics/Canvas;FFI)V
    .locals 2

    .line 127
    iget-object v0, p0, Lcom/squareup/chartography/LineChartView;->dotPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 128
    iget-object v0, p0, Lcom/squareup/chartography/LineChartView;->dotPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 129
    iget v0, p0, Lcom/squareup/chartography/LineChartView;->dotRadius:F

    iget-object v1, p0, Lcom/squareup/chartography/LineChartView;->dotPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p3, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 132
    iget-object v0, p0, Lcom/squareup/chartography/LineChartView;->dotPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p4}, Landroid/graphics/Paint;->setColor(I)V

    .line 133
    iget-object p4, p0, Lcom/squareup/chartography/LineChartView;->dotPaint:Landroid/graphics/Paint;

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 134
    iget p4, p0, Lcom/squareup/chartography/LineChartView;->dotRadius:F

    iget-object v0, p0, Lcom/squareup/chartography/LineChartView;->dotPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p3, p4, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method private final getLineWidth()F
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/chartography/LineChartView;->linePaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    return v0
.end method

.method private final setLineWidth(F)V
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/chartography/LineChartView;->linePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method


# virtual methods
.method protected getHeaderSpacing()F
    .locals 2

    .line 69
    iget v0, p0, Lcom/squareup/chartography/LineChartView;->dotRadius:F

    iget-object v1, p0, Lcom/squareup/chartography/LineChartView;->dotPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method protected onDrawData(Landroid/graphics/Canvas;)V
    .locals 8

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getSelectedDomain()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 56
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getX()F

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v1

    const/4 v2, 0x2

    int-to-float v2, v2

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getXStep()F

    move-result v1

    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getSelectedDomain()I

    move-result v2

    int-to-float v2, v2

    mul-float v1, v1, v2

    add-float v5, v0, v1

    .line 58
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getY()F

    move-result v4

    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getY()F

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getDataDrawRegion()Lcom/squareup/chartography/ChartView$DataDrawRegion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/chartography/ChartView$DataDrawRegion;->getHeight()F

    move-result v1

    sub-float v6, v0, v1

    .line 59
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getAxisPaint()Landroid/graphics/Paint;

    move-result-object v7

    move-object v2, p1

    move v3, v5

    .line 57
    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 63
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/chartography/LineChartView;->getAdapter()Lcom/squareup/chartography/ChartAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/chartography/ChartAdapter;->getNumDimensions()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 64
    invoke-direct {p0, p1, v0}, Lcom/squareup/chartography/LineChartView;->drawData(Landroid/graphics/Canvas;I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method
