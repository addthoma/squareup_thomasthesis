.class final Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAccessiblePinTutorialWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow;->render(Lkotlin/Unit;Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen$AccessiblePinTutorialDialogEvent;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "event",
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen$AccessiblePinTutorialDialogEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;

.field final synthetic $state:Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/RenderContext;Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$3;->$context:Lcom/squareup/workflow/RenderContext;

    iput-object p2, p0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$3;->$state:Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen$AccessiblePinTutorialDialogEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$3;->invoke(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen$AccessiblePinTutorialDialogEvent;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen$AccessiblePinTutorialDialogEvent;)V
    .locals 2

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    sget-object v0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen$AccessiblePinTutorialDialogEvent$ResumeTutorial;->INSTANCE:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen$AccessiblePinTutorialDialogEvent$ResumeTutorial;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$3;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {p1}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    new-instance v0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;

    iget-object v1, p0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$3;->$state:Lcom/squareup/accessibility/pin/AccessiblePinTutorialState;

    check-cast v1, Lcom/squareup/accessibility/pin/DisplayingQuitDialog;

    invoke-virtual {v1}, Lcom/squareup/accessibility/pin/DisplayingQuitDialog;->getPage()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$DisplayTutorialPage;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    goto :goto_0

    .line 71
    :cond_0
    sget-object v0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen$AccessiblePinTutorialDialogEvent$SkipTutorial;->INSTANCE:Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen$AccessiblePinTutorialDialogEvent$SkipTutorial;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$render$3;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {p1}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object p1

    sget-object v0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$ExitTutorial;->INSTANCE:Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialWorkflow$Action$ExitTutorial;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method
