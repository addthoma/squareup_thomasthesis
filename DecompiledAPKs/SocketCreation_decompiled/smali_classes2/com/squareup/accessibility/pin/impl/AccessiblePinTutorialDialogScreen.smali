.class public final Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen;
.super Ljava/lang/Object;
.source "AccessiblePinTutorialDialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen$AccessiblePinTutorialDialogEvent;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0001\tB\u0019\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0002\u0010\u0006R\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "onEvent",
        "Lkotlin/Function1;",
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen$AccessiblePinTutorialDialogEvent;",
        "",
        "(Lkotlin/jvm/functions/Function1;)V",
        "getOnEvent",
        "()Lkotlin/jvm/functions/Function1;",
        "AccessiblePinTutorialDialogEvent",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final onEvent:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen$AccessiblePinTutorialDialogEvent;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen$AccessiblePinTutorialDialogEvent;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onEvent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-void
.end method


# virtual methods
.method public final getOnEvent()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen$AccessiblePinTutorialDialogEvent;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 9
    iget-object v0, p0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen;->onEvent:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method
