.class public final Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialViewFactory;
.super Lcom/squareup/accessibility/pin/AccessiblePinTutorialViewFactory;
.source "RealAccessiblePinTutorialViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialViewFactory;",
        "Lcom/squareup/accessibility/pin/AccessiblePinTutorialViewFactory;",
        "accessiblePinTutorialDoneCoordinatorFactory",
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator$Factory;",
        "accessiblePinTutorialStepCoordinatorFactory",
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$Factory;",
        "accessiblePinTutorialDialogFactory",
        "Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$Factory;",
        "(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator$Factory;Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$Factory;Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator$Factory;Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$Factory;Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$Factory;)V
    .locals 26
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    const-string v3, "accessiblePinTutorialDoneCoordinatorFactory"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "accessiblePinTutorialStepCoordinatorFactory"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "accessiblePinTutorialDialogFactory"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x3

    new-array v3, v3, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 21
    sget-object v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 22
    const-class v5, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneScreen;

    invoke-static {v5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v5

    const/4 v12, 0x1

    const/4 v13, 0x0

    invoke-static {v5, v13, v12, v13}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 23
    sget v6, Lcom/squareup/accessibility/pin/impl/R$layout;->accessible_pin_tutorial_done_layout:I

    .line 24
    new-instance v7, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialViewFactory$1;

    invoke-direct {v7, v0}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialViewFactory$1;-><init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDoneCoordinator$Factory;)V

    move-object v9, v7

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 25
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v20, Lcom/squareup/container/spot/Spots;->RIGHT:Lcom/squareup/container/spot/Spot;

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const/16 v24, 0x1df

    const/16 v25, 0x0

    move-object v14, v0

    invoke-direct/range {v14 .. v25}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v8, 0x0

    const/16 v10, 0x8

    const/4 v11, 0x0

    move-object v7, v0

    .line 21
    invoke-static/range {v4 .. v11}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 28
    sget-object v14, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 29
    const-class v0, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {v0, v13, v12, v13}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v15

    .line 30
    sget v16, Lcom/squareup/accessibility/pin/impl/R$layout;->accessible_pin_tutorial_step_layout:I

    .line 31
    new-instance v0, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialViewFactory$2;

    invoke-direct {v0, v1}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialViewFactory$2;-><init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialStepCoordinator$Factory;)V

    move-object/from16 v19, v0

    check-cast v19, Lkotlin/jvm/functions/Function1;

    const/16 v17, 0x0

    const/16 v20, 0xc

    const/16 v21, 0x0

    .line 28
    invoke-static/range {v14 .. v21}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    aput-object v0, v3, v12

    .line 34
    sget-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 35
    const-class v1, Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogScreen;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v1, v13, v12, v13}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    .line 36
    new-instance v4, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialViewFactory$3;

    invoke-direct {v4, v2}, Lcom/squareup/accessibility/pin/impl/RealAccessiblePinTutorialViewFactory$3;-><init>(Lcom/squareup/accessibility/pin/impl/AccessiblePinTutorialDialogFactory$Factory;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 34
    invoke-virtual {v0, v1, v4}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x2

    aput-object v0, v3, v1

    move-object/from16 v0, p0

    .line 20
    invoke-direct {v0, v3}, Lcom/squareup/accessibility/pin/AccessiblePinTutorialViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
