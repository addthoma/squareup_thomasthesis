.class final Lcom/squareup/accessibility/AccessibilitySettings$talkBackState$1;
.super Ljava/lang/Object;
.source "AccessibilitySettings.kt"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/accessibility/AccessibilitySettings;-><init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/accessibility/AccessibilitySettings;


# direct methods
.method constructor <init>(Lcom/squareup/accessibility/AccessibilitySettings;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/accessibility/AccessibilitySettings$talkBackState$1;->this$0:Lcom/squareup/accessibility/AccessibilitySettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    new-instance v0, Lcom/squareup/accessibility/AccessibilitySettings$talkBackState$1$listener$1;

    invoke-direct {v0, p1}, Lcom/squareup/accessibility/AccessibilitySettings$talkBackState$1$listener$1;-><init>(Lio/reactivex/ObservableEmitter;)V

    check-cast v0, Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;

    .line 23
    iget-object v1, p0, Lcom/squareup/accessibility/AccessibilitySettings$talkBackState$1;->this$0:Lcom/squareup/accessibility/AccessibilitySettings;

    invoke-static {v1}, Lcom/squareup/accessibility/AccessibilitySettings;->access$getAccessibilityManager$p(Lcom/squareup/accessibility/AccessibilitySettings;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/accessibility/AccessibilityManager;->addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    .line 25
    new-instance v1, Lcom/squareup/accessibility/AccessibilitySettings$talkBackState$1$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/accessibility/AccessibilitySettings$talkBackState$1$1;-><init>(Lcom/squareup/accessibility/AccessibilitySettings$talkBackState$1;Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)V

    check-cast v1, Lio/reactivex/functions/Cancellable;

    invoke-interface {p1, v1}, Lio/reactivex/ObservableEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    return-void
.end method
