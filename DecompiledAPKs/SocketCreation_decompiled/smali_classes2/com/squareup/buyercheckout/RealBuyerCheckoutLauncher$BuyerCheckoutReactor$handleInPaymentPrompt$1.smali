.class final Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealBuyerCheckoutLauncher.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;->handleInPaymentPrompt(Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;Lcom/squareup/workflow/legacy/rx2/EventChannel;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/workflow/legacy/Reaction<",
        "+",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "+",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        ">;>;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealBuyerCheckoutLauncher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealBuyerCheckoutLauncher.kt\ncom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1\n+ 2 EventSelectBuilder.kt\ncom/squareup/workflow/legacy/rx2/EventSelectBuilder\n*L\n1#1,641:1\n85#2,2:642\n85#2,2:644\n85#2,2:646\n85#2,2:648\n*E\n*S KotlinDebug\n*F\n+ 1 RealBuyerCheckoutLauncher.kt\ncom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1\n*L\n319#1,2:642\n321#1,2:644\n328#1,2:646\n334#1,2:648\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u001e\u0012\u0004\u0012\u00020\u0003\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0002H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
        "Lcom/squareup/workflow/legacy/Reaction;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutWorkflowReaction;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $inputHandlerResult:Lio/reactivex/Single;

.field final synthetic $state:Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;

.field final synthetic this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;


# direct methods
.method constructor <init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;Lio/reactivex/Single;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor;

    iput-object p2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;->$state:Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;

    iput-object p3, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;->$inputHandlerResult:Lio/reactivex/Single;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 176
    check-cast p1, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;

    invoke-virtual {p0, p1}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutEvent;",
            "Lcom/squareup/workflow/legacy/Reaction<",
            "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
            "Lcom/squareup/tenderpayment/TenderPaymentResult;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 319
    new-instance v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$1;-><init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 642
    sget-object v1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$$special$$inlined$onEvent$1;->INSTANCE:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$$special$$inlined$onEvent$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 321
    new-instance v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$2;

    invoke-direct {v0, p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$2;-><init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 644
    sget-object v1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$$special$$inlined$onEvent$2;->INSTANCE:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$$special$$inlined$onEvent$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 328
    new-instance v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$3;

    invoke-direct {v0, p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$3;-><init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 646
    sget-object v1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$$special$$inlined$onEvent$3;->INSTANCE:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$$special$$inlined$onEvent$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 334
    new-instance v0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$4;

    invoke-direct {v0, p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$4;-><init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 648
    sget-object v1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$$special$$inlined$onEvent$4;->INSTANCE:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$$special$$inlined$onEvent$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->addEventCase(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 339
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;->$inputHandlerResult:Lio/reactivex/Single;

    new-instance v1, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$5;

    invoke-direct {v1, p0}, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1$5;-><init>(Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$BuyerCheckoutReactor$handleInPaymentPrompt$1;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;->onSuccess(Lio/reactivex/Single;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
