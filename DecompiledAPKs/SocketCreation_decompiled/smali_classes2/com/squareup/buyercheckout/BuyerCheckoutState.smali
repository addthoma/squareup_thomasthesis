.class public abstract Lcom/squareup/buyercheckout/BuyerCheckoutState;
.super Ljava/lang/Object;
.source "BuyerCheckoutState.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;,
        Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;,
        Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;,
        Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;,
        Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;,
        Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;,
        Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;,
        Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;,
        Lcom/squareup/buyercheckout/BuyerCheckoutState$BuyerCheckoutStateData;,
        Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00062\u00020\u0001:\n\u0005\u0006\u0007\u0008\t\n\u000b\u000c\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u0082\u0001\u0008\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/buyercheckout/BuyerCheckoutState;",
        "",
        "()V",
        "toSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "BuyerCheckoutStateData",
        "Companion",
        "InBuyerCart",
        "InLanguageSelection",
        "InPaymentPrompt",
        "InTipping",
        "StartTipping",
        "Starting",
        "Stopping",
        "WaitingForCancelPermission",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$Starting;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$Stopping;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$InBuyerCart;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$InPaymentPrompt;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$StartTipping;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$InTipping;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$InLanguageSelection;",
        "Lcom/squareup/buyercheckout/BuyerCheckoutState$WaitingForCancelPermission;",
        "tender-payment_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/buyercheckout/BuyerCheckoutState;-><init>()V

    return-void
.end method


# virtual methods
.method public final toSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 1

    .line 81
    sget-object v0, Lcom/squareup/buyercheckout/BuyerCheckoutState;->Companion:Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;

    invoke-static {v0, p0}, Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;->access$snapshotState(Lcom/squareup/buyercheckout/BuyerCheckoutState$Companion;Lcom/squareup/buyercheckout/BuyerCheckoutState;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    return-object v0
.end method
