.class final Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$$inlined$also$lambda$1;
.super Ljava/lang/Object;
.source "RealBuyerCheckoutLauncher.kt"

# interfaces
.implements Lio/reactivex/functions/Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;->launch(Lcom/squareup/buyercheckout/BuyerCheckoutState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "run",
        "com/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$2$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $paymentInputHandler$inlined:Lcom/squareup/ui/main/errors/PaymentInputHandler;

.field final synthetic $paymentInputSub:Lio/reactivex/disposables/Disposable;

.field final synthetic $workflows$inlined:Lcom/squareup/workflow/legacy/WorkflowPool;

.field final synthetic this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;


# direct methods
.method constructor <init>(Lio/reactivex/disposables/Disposable;Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/workflow/legacy/WorkflowPool;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$$inlined$also$lambda$1;->$paymentInputSub:Lio/reactivex/disposables/Disposable;

    iput-object p2, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$$inlined$also$lambda$1;->this$0:Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher;

    iput-object p3, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$$inlined$also$lambda$1;->$paymentInputHandler$inlined:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    iput-object p4, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$$inlined$also$lambda$1;->$workflows$inlined:Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$$inlined$also$lambda$1;->$workflows$inlined:Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool;->abandonAll()V

    .line 170
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$$inlined$also$lambda$1;->$paymentInputSub:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 171
    iget-object v0, p0, Lcom/squareup/buyercheckout/RealBuyerCheckoutLauncher$launch$$inlined$also$lambda$1;->$paymentInputHandler$inlined:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->destroy()V

    return-void
.end method
