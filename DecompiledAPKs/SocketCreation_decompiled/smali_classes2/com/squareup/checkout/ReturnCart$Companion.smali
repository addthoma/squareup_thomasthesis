.class public final Lcom/squareup/checkout/ReturnCart$Companion;
.super Ljava/lang/Object;
.source "ReturnCart.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/ReturnCart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReturnCart.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReturnCart.kt\ncom/squareup/checkout/ReturnCart$Companion\n*L\n1#1,206:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J*\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0007\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/checkout/ReturnCart$Companion;",
        "",
        "()V",
        "fromBill",
        "Lcom/squareup/checkout/ReturnCart;",
        "bill",
        "Lcom/squareup/protos/client/bills/Bill;",
        "res",
        "Lcom/squareup/util/Res;",
        "namer",
        "Lcom/squareup/checkout/OrderVariationNamer;",
        "discountApplicationIdEnabled",
        "",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 159
    invoke-direct {p0}, Lcom/squareup/checkout/ReturnCart$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromBill(Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;Z)Lcom/squareup/checkout/ReturnCart;
    .locals 15
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "bill"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "res"

    move-object/from16 v4, p2

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v1, "namer"

    move-object/from16 v5, p3

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_1

    .line 171
    :cond_0
    iget-object v1, v0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v1, v1, Lcom/squareup/protos/client/bills/Cart;->return_line_items:Ljava/util/List;

    const-string v2, "bill.cart.return_line_items"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->first(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;

    .line 172
    sget-object v2, Lcom/squareup/checkout/ReturnCartItem;->Companion:Lcom/squareup/checkout/ReturnCartItem$Companion;

    .line 173
    iget-object v3, v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_itemization:Ljava/util/List;

    const-string v6, "returnLineItems.return_itemization"

    invoke-static {v3, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    sget-object v6, Lcom/squareup/checkout/OrderDestination;->Companion:Lcom/squareup/checkout/OrderDestination$Companion;

    iget-object v7, v0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v7, v7, Lcom/squareup/protos/client/bills/Cart;->feature_details:Lcom/squareup/protos/client/bills/Cart$FeatureDetails;

    invoke-virtual {v6, v7}, Lcom/squareup/checkout/OrderDestination$Companion;->seatsFromFeatureDetails(Lcom/squareup/protos/client/bills/Cart$FeatureDetails;)Ljava/util/List;

    move-result-object v7

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move/from16 v6, p4

    .line 172
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/checkout/ReturnCartItem$Companion;->fromProtos(Ljava/util/List;Lcom/squareup/util/Res;Lcom/squareup/checkout/OrderVariationNamer;ZLjava/util/List;)Ljava/util/List;

    move-result-object v12

    .line 182
    iget-object v2, v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->return_tip_line_item:Ljava/util/List;

    if-eqz v2, :cond_1

    sget-object v3, Lcom/squareup/checkout/ReturnTip;->Companion:Lcom/squareup/checkout/ReturnTip$Companion;

    invoke-virtual {v3, v2}, Lcom/squareup/checkout/ReturnTip$Companion;->fromReturnTipLineItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 183
    :cond_1
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v2

    :goto_0
    move-object v13, v2

    .line 185
    new-instance v2, Lcom/squareup/checkout/ReturnCart;

    .line 186
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Bill;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v9, v3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    const-string v3, "bill.bill_id_pair.server_id"

    invoke-static {v9, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    iget-object v3, v0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->net:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v3, v3, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    iget-object v10, v3, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    const-string v3, "bill.cart.amount_details\u2026total_money.currency_code"

    invoke-static {v10, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 188
    iget-object v0, v0, Lcom/squareup/protos/client/bills/Bill;->cart:Lcom/squareup/protos/client/bills/Cart;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart;->amount_details:Lcom/squareup/protos/client/bills/Cart$AmountDetails;

    iget-object v0, v0, Lcom/squareup/protos/client/bills/Cart$AmountDetails;->return_:Lcom/squareup/protos/client/bills/Cart$Amounts;

    iget-object v11, v0, Lcom/squareup/protos/client/bills/Cart$Amounts;->total_money:Lcom/squareup/protos/common/Money;

    const-string v0, "bill.cart.amount_details.return_.total_money"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    iget-object v14, v1, Lcom/squareup/protos/client/bills/Cart$ReturnLineItems;->rounding_adjustment:Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;

    move-object v8, v2

    .line 185
    invoke-direct/range {v8 .. v14}, Lcom/squareup/checkout/ReturnCart;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/bills/RoundingAdjustmentLineItem;)V

    return-object v2

    :cond_2
    :goto_1
    const/4 v0, 0x0

    return-object v0
.end method
