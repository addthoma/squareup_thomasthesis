.class public final Lcom/squareup/checkout/v2/launcher/CheckoutAppletBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "CheckoutAppletBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\u0002\u001a\u00020\tH\u0016J\u0008\u0010\n\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/checkout/v2/launcher/CheckoutAppletBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "scope",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "(Lcom/squareup/ui/main/InMainActivityScope;)V",
        "getScope",
        "()Lcom/squareup/ui/main/InMainActivityScope;",
        "doRegister",
        "",
        "Lmortar/MortarScope;",
        "getParentKey",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final scope:Lcom/squareup/ui/main/InMainActivityScope;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/InMainActivityScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkout/v2/launcher/CheckoutAppletBootstrapScreen;->scope:Lcom/squareup/ui/main/InMainActivityScope;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget-object v0, Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner;->Companion:Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner$Companion;->get$public_release(Lmortar/MortarScope;)Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner;

    move-result-object p1

    .line 17
    invoke-interface {p1}, Lcom/squareup/checkout/v2/launcher/CheckoutAppletWorkflowRunner;->startWorkflow()V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/main/InMainActivityScope;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/checkout/v2/launcher/CheckoutAppletBootstrapScreen;->scope:Lcom/squareup/ui/main/InMainActivityScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/checkout/v2/launcher/CheckoutAppletBootstrapScreen;->getParentKey()Lcom/squareup/ui/main/InMainActivityScope;

    move-result-object v0

    return-object v0
.end method

.method public final getScope()Lcom/squareup/ui/main/InMainActivityScope;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/checkout/v2/launcher/CheckoutAppletBootstrapScreen;->scope:Lcom/squareup/ui/main/InMainActivityScope;

    return-object v0
.end method
