.class public Lcom/squareup/checkout/Cart$Builder;
.super Ljava/lang/Object;
.source "Cart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkout/Cart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final deletedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation
.end field

.field private final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->deletedItems:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/checkout/Cart$Builder;)Ljava/util/List;
    .locals 0

    .line 87
    iget-object p0, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/checkout/Cart$Builder;)Ljava/util/List;
    .locals 0

    .line 87
    iget-object p0, p0, Lcom/squareup/checkout/Cart$Builder;->deletedItems:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/checkout/Cart$Builder;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 87
    iget-object p0, p0, Lcom/squareup/checkout/Cart$Builder;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method


# virtual methods
.method public addItem(ILcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/Cart$Builder;
    .locals 1

    .line 286
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addItems(Ljava/lang/Iterable;)Lcom/squareup/checkout/Cart$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Lcom/squareup/checkout/Cart$Builder;"
        }
    .end annotation

    .line 114
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 115
    iget-object v1, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    const-string v2, "item"

    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public varargs addItems([Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/Cart$Builder;
    .locals 5

    .line 102
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    .line 103
    iget-object v3, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    const-string v4, "item"

    invoke-static {v2, v4}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public build()Lcom/squareup/checkout/Cart;
    .locals 2

    .line 366
    new-instance v0, Lcom/squareup/checkout/Cart;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/checkout/Cart;-><init>(Lcom/squareup/checkout/Cart$Builder;Lcom/squareup/checkout/Cart$1;)V

    return-object v0
.end method

.method public clearItems()Lcom/squareup/checkout/Cart$Builder;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-object p0
.end method

.method public compItem(ILcom/squareup/protos/client/Employee;Lcom/squareup/checkout/Discount;Ljava/util/List;)Lcom/squareup/checkout/Cart$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/protos/client/Employee;",
            "Lcom/squareup/checkout/Discount;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;)",
            "Lcom/squareup/checkout/Cart$Builder;"
        }
    .end annotation

    .line 240
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 241
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 242
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 243
    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->id()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->removeAppliedDiscount(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    goto :goto_0

    .line 246
    :cond_0
    invoke-virtual {v0, p3}, Lcom/squareup/checkout/CartItem$Builder;->addAppliedDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p4

    iget-object p3, p3, Lcom/squareup/checkout/Discount;->name:Ljava/lang/String;

    .line 247
    invoke-static {p2, p3}, Lcom/squareup/checkout/util/ItemizationEvents;->compEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p2

    invoke-virtual {p4, p2}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    .line 248
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p2

    .line 250
    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/Cart$Builder;

    return-object p0
.end method

.method public currencyCode(Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/checkout/Cart$Builder;
    .locals 1

    const-string v0, "currencyCode"

    .line 349
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/common/CurrencyCode;

    iput-object p1, p0, Lcom/squareup/checkout/Cart$Builder;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method public deletedItems(Ljava/util/List;)Lcom/squareup/checkout/Cart$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Lcom/squareup/checkout/Cart$Builder;"
        }
    .end annotation

    .line 127
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->deletedItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 128
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 129
    iget-object v1, p0, Lcom/squareup/checkout/Cart$Builder;->deletedItems:Ljava/util/List;

    const-string v2, "deletedItem"

    invoke-static {v0, v2}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public deletedItemsView()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 299
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->deletedItems:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    .line 354
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 361
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public itemsListIterator()Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 93
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public itemsView()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 295
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public notVoidedItemsView()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    .line 307
    invoke-virtual {p0, v0}, Lcom/squareup/checkout/Cart$Builder;->notVoidedItemsView(Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public notVoidedItemsView(Z)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 319
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 320
    :goto_0
    iget-object v3, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 321
    iget-object v3, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkout/CartItem;

    .line 322
    iget-object v4, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    if-ne v2, v4, :cond_0

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz p1, :cond_1

    if-eqz v5, :cond_1

    .line 323
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->isInteresting()Z

    move-result v4

    if-nez v4, :cond_1

    goto :goto_2

    .line 326
    :cond_1
    invoke-virtual {v3}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v4

    if-nez v4, :cond_2

    .line 327
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 330
    :cond_3
    :goto_2
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public rememberDeletedItem(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)Lcom/squareup/checkout/Cart$Builder;
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->deletedItems:Ljava/util/List;

    invoke-static {p2}, Lcom/squareup/checkout/util/ItemizationEvents;->deleteEvent(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/checkout/util/ItemizationEvents;->itemWithEvent(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public removeItem(IZLcom/squareup/protos/client/Employee;)Lcom/squareup/checkout/Cart$Builder;
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/CartItem;

    if-eqz p2, :cond_0

    .line 222
    invoke-virtual {p0, p1, p3}, Lcom/squareup/checkout/Cart$Builder;->rememberDeletedItem(Lcom/squareup/checkout/CartItem;Lcom/squareup/protos/client/Employee;)Lcom/squareup/checkout/Cart$Builder;

    :cond_0
    return-object p0
.end method

.method public replaceDeletedItems(Lcom/squareup/checkout/CartItem$Transform;)Lcom/squareup/checkout/Cart$Builder;
    .locals 7

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 165
    :goto_0
    iget-object v2, p0, Lcom/squareup/checkout/Cart$Builder;->deletedItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 166
    iget-object v2, p0, Lcom/squareup/checkout/Cart$Builder;->deletedItems:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    invoke-interface {p1, v2}, Lcom/squareup/checkout/CartItem$Transform;->apply(Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/CartItem;

    move-result-object v2

    .line 168
    iget-object v3, v2, Lcom/squareup/checkout/CartItem;->events:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v4, 0x0

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/protos/client/bills/Itemization$Event;

    .line 169
    iget-object v5, v5, Lcom/squareup/protos/client/bills/Itemization$Event;->event_type:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    sget-object v6, Lcom/squareup/protos/client/bills/Itemization$Event$EventType;->DELETE:Lcom/squareup/protos/client/bills/Itemization$Event$EventType;

    if-ne v5, v6, :cond_0

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    if-eqz v4, :cond_2

    .line 176
    iget-object v3, p0, Lcom/squareup/checkout/Cart$Builder;->deletedItems:Ljava/util/List;

    invoke-interface {v3, v1, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 174
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Replacement must still be deleted"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    return-object p0
.end method

.method public replaceItem(ILcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/Cart$Builder;
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    const-string v1, "item"

    invoke-static {p2, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public replaceItem(ILrx/functions/Func1;)Lcom/squareup/checkout/Cart$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lrx/functions/Func1<",
            "Lcom/squareup/checkout/CartItem$Builder;",
            "Lcom/squareup/checkout/CartItem$Builder;",
            ">;)",
            "Lcom/squareup/checkout/Cart$Builder;"
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    invoke-virtual {v1}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v1

    invoke-interface {p2, v1}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/checkout/CartItem$Builder;

    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public replaceItems(Lcom/squareup/checkout/CartItem$Transform;)Lcom/squareup/checkout/Cart$Builder;
    .locals 2

    const/4 v0, 0x0

    .line 155
    :goto_0
    iget-object v1, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 156
    iget-object v1, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    invoke-interface {p1, v1}, Lcom/squareup/checkout/CartItem$Transform;->apply(Lcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/CartItem;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/Cart$Builder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public scanItems(Lrx/functions/Action2;)Lcom/squareup/checkout/Cart$Builder;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/Action2<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Lcom/squareup/checkout/Cart$Builder;"
        }
    .end annotation

    .line 186
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 187
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p1, v3, v2}, Lrx/functions/Action2;->call(Ljava/lang/Object;Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public scanSingle(Lrx/functions/Func2;Lrx/functions/Action2;)Lcom/squareup/checkout/Cart$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/Func2<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lrx/functions/Action2<",
            "Ljava/lang/Integer;",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Lcom/squareup/checkout/Cart$Builder;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 200
    :goto_0
    iget-object v1, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 201
    iget-object v1, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 202
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2, v1}, Lrx/functions/Func2;->call(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 203
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p2, p1, v1}, Lrx/functions/Action2;->call(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object p0
.end method

.method public uncompItem(ILcom/squareup/protos/client/Employee;Ljava/util/List;)Lcom/squareup/checkout/Cart$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/protos/client/Employee;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/Discount;",
            ">;)",
            "Lcom/squareup/checkout/Cart$Builder;"
        }
    .end annotation

    .line 256
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 258
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->getCompDiscount()Lcom/squareup/checkout/Discount;

    move-result-object v1

    .line 259
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 260
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Discount;

    .line 261
    invoke-virtual {v0, v2}, Lcom/squareup/checkout/CartItem$Builder;->addAppliedDiscount(Lcom/squareup/checkout/Discount;)Lcom/squareup/checkout/CartItem$Builder;

    goto :goto_0

    .line 263
    :cond_0
    iget-object p3, v1, Lcom/squareup/checkout/Discount;->id:Ljava/lang/String;

    .line 264
    invoke-virtual {v0, p3}, Lcom/squareup/checkout/CartItem$Builder;->removeAppliedDiscount(Ljava/lang/String;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p3

    .line 265
    invoke-static {p2}, Lcom/squareup/checkout/util/ItemizationEvents;->uncompEvent(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p2

    invoke-virtual {p3, p2}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    .line 266
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p2

    .line 268
    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/Cart$Builder;

    return-object p0
.end method

.method public voidItem(ILcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/checkout/Cart$Builder;
    .locals 2

    .line 273
    iget-object v0, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkout/CartItem;

    .line 275
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 276
    invoke-virtual {v0, v1}, Lcom/squareup/checkout/CartItem$Builder;->isVoided(Z)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 277
    invoke-virtual {v0}, Lcom/squareup/checkout/CartItem$Builder;->clearAppliedDiscounts()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 278
    invoke-static {p2, p3}, Lcom/squareup/checkout/util/ItemizationEvents;->voidEvent(Lcom/squareup/protos/client/Employee;Ljava/lang/String;)Lcom/squareup/protos/client/bills/Itemization$Event;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/checkout/CartItem$Builder;->addEvent(Lcom/squareup/protos/client/bills/Itemization$Event;)Lcom/squareup/checkout/CartItem$Builder;

    move-result-object p2

    .line 279
    invoke-virtual {p2}, Lcom/squareup/checkout/CartItem$Builder;->build()Lcom/squareup/checkout/CartItem;

    move-result-object p2

    .line 281
    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkout/Cart$Builder;->replaceItem(ILcom/squareup/checkout/CartItem;)Lcom/squareup/checkout/Cart$Builder;

    return-object p0
.end method

.method public voidedItemsView()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .line 338
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 339
    iget-object v1, p0, Lcom/squareup/checkout/Cart$Builder;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 340
    invoke-virtual {v2}, Lcom/squareup/checkout/CartItem;->isVoided()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 341
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 344
    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
