.class public final enum Lcom/squareup/PaymentType;
.super Ljava/lang/Enum;
.source "PaymentType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/PaymentType;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/PaymentType;

.field public static final enum BILL:Lcom/squareup/PaymentType;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum BILL2:Lcom/squareup/PaymentType;

.field public static final enum CARD:Lcom/squareup/PaymentType;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum CASH:Lcom/squareup/PaymentType;

.field public static final enum OTHER:Lcom/squareup/PaymentType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 17
    new-instance v0, Lcom/squareup/PaymentType;

    const/4 v1, 0x0

    const-string v2, "CASH"

    invoke-direct {v0, v2, v1}, Lcom/squareup/PaymentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/PaymentType;->CASH:Lcom/squareup/PaymentType;

    .line 23
    new-instance v0, Lcom/squareup/PaymentType;

    const/4 v2, 0x1

    const-string v3, "CARD"

    invoke-direct {v0, v3, v2}, Lcom/squareup/PaymentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/PaymentType;->CARD:Lcom/squareup/PaymentType;

    .line 31
    new-instance v0, Lcom/squareup/PaymentType;

    const/4 v3, 0x2

    const-string v4, "BILL"

    invoke-direct {v0, v4, v3}, Lcom/squareup/PaymentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/PaymentType;->BILL:Lcom/squareup/PaymentType;

    .line 35
    new-instance v0, Lcom/squareup/PaymentType;

    const/4 v4, 0x3

    const-string v5, "OTHER"

    invoke-direct {v0, v5, v4}, Lcom/squareup/PaymentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/PaymentType;->OTHER:Lcom/squareup/PaymentType;

    .line 41
    new-instance v0, Lcom/squareup/PaymentType;

    const/4 v5, 0x4

    const-string v6, "BILL2"

    invoke-direct {v0, v6, v5}, Lcom/squareup/PaymentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/PaymentType;->BILL2:Lcom/squareup/PaymentType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/PaymentType;

    .line 14
    sget-object v6, Lcom/squareup/PaymentType;->CASH:Lcom/squareup/PaymentType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/PaymentType;->CARD:Lcom/squareup/PaymentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/PaymentType;->BILL:Lcom/squareup/PaymentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/PaymentType;->OTHER:Lcom/squareup/PaymentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/PaymentType;->BILL2:Lcom/squareup/PaymentType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/PaymentType;->$VALUES:[Lcom/squareup/PaymentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/PaymentType;
    .locals 1

    .line 14
    const-class v0, Lcom/squareup/PaymentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/PaymentType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/PaymentType;
    .locals 1

    .line 14
    sget-object v0, Lcom/squareup/PaymentType;->$VALUES:[Lcom/squareup/PaymentType;

    invoke-virtual {v0}, [Lcom/squareup/PaymentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/PaymentType;

    return-object v0
.end method
