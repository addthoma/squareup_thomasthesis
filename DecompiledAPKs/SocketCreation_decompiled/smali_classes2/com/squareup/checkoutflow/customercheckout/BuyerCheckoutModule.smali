.class public abstract Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutModule;
.super Ljava/lang/Object;
.source "BuyerCheckoutModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindBuyerCheckoutTenderOptionFactoryIntoMainActivity(Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideBuyerCheckoutAdditionalTenderOptions(Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutAdditionalTenderOptions;)Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutAdditionalTenderOptions;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideBuyerCheckoutTenderOptionFactory(Lcom/squareup/checkoutflow/customercheckout/RealBuyerCheckoutTenderOptionFactory;)Lcom/squareup/checkoutflow/customercheckout/BuyerCheckoutTenderOptionFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
