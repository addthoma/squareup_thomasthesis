.class public final Lcom/squareup/checkoutflow/selecttender/RealSelectTenderViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealSelectTenderViewFactory.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u001c\u0010\u0006\u001a\u00020\u00072\u0012\u0010\u0008\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\nH\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/selecttender/RealSelectTenderViewFactory;",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "selectTenderFactory",
        "Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$Factory;",
        "(Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$Factory;)V",
        "isInScreen",
        "",
        "screenKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$Factory;)V
    .locals 18
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    const-string v1, "selectTenderFactory"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 14
    sget-object v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 15
    sget-object v3, Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;->Companion:Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData$Companion;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    .line 16
    sget v4, Lcom/squareup/checkoutflow/selecttender/impl/R$layout;->select_tender_view:I

    .line 17
    new-instance v17, Lcom/squareup/workflow/ScreenHint;

    sget-object v11, Lcom/squareup/container/spot/Spots;->RIGHT:Lcom/squareup/container/spot/Spot;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0x1df

    const/16 v16, 0x0

    move-object/from16 v5, v17

    invoke-direct/range {v5 .. v16}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 18
    new-instance v5, Lcom/squareup/checkoutflow/selecttender/RealSelectTenderViewFactory$1;

    invoke-direct {v5, v0}, Lcom/squareup/checkoutflow/selecttender/RealSelectTenderViewFactory$1;-><init>(Lcom/squareup/checkoutflow/selecttender/SelectTenderCoordinator$Factory;)V

    move-object v7, v5

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/16 v8, 0x8

    move-object/from16 v5, v17

    .line 14
    invoke-static/range {v2 .. v9}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v2, 0x0

    aput-object v0, v1, v2

    move-object/from16 v0, p0

    .line 13
    invoke-direct {v0, v1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method


# virtual methods
.method public isInScreen(Lcom/squareup/workflow/legacy/Screen$Key;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)Z"
        }
    .end annotation

    const-string v0, "screenKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    sget-object v0, Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData;->Companion:Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData$Companion;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/selecttender/SelectTenderScreenData$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method
