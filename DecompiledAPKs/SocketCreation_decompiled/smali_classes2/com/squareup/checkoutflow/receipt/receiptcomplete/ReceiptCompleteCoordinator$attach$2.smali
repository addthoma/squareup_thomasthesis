.class final Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$attach$2;
.super Lkotlin/jvm/internal/Lambda;
.source "ReceiptCompleteCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReceiptCompleteCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReceiptCompleteCoordinator.kt\ncom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$attach$2\n*L\n1#1,268:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012<\u0010\u0002\u001a8\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0007*\u001c\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u0004\u0018\u0001`\u00060\u0003j\u0008\u0012\u0004\u0012\u00020\u0004`\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$attach$2;->this$0:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 54
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$attach$2;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2

    .line 90
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator$attach$2;->this$0:Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;

    const-string v1, "screen"

    .line 91
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;

    invoke-static {v0, v1}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->access$setCurrentScreen$p(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;)V

    .line 92
    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteScreen;->getRequestCreator()Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;->access$updateBackground(Lcom/squareup/checkoutflow/receipt/receiptcomplete/ReceiptCompleteCoordinator;Lcom/squareup/picasso/RequestCreator;)V

    return-void
.end method
