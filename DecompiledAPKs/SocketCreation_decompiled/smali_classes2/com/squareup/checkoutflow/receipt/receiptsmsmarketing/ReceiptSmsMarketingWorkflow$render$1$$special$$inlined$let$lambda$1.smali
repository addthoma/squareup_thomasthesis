.class final Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1$$special$$inlined$let$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ReceiptSmsMarketingWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1;->invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/WorkflowAction$Updater<",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;",
        "-",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001*\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002H\n\u00a2\u0006\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
        "invoke",
        "com/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

.field final synthetic this$0:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1;


# direct methods
.method constructor <init>(Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1$$special$$inlined$let$lambda$1;->$it:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1$$special$$inlined$let$lambda$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Updater;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1$$special$$inlined$let$lambda$1;->invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Updater;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Updater<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState;",
            "-",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingPrompt;

    .line 124
    new-instance v1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;

    .line 125
    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1$$special$$inlined$let$lambda$1;->$it:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    iget-object v2, v2, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    iget-object v2, v2, Lcom/squareup/protos/postoffice/sms/Subscriber;->obfuscated_phone_number:Ljava/lang/String;

    const-string v3, "it.subscriber.obfuscated_phone_number"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1$$special$$inlined$let$lambda$1;->$it:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    iget-object v3, v3, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->title:Ljava/lang/String;

    const-string v4, "it.title"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1$$special$$inlined$let$lambda$1;->$it:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    iget-object v4, v4, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->subtitle:Ljava/lang/String;

    const-string v5, "it.subtitle"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    iget-object v5, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1$$special$$inlined$let$lambda$1;->$it:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    iget-object v5, v5, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->terms:Ljava/lang/String;

    const-string v6, "it.terms"

    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    invoke-direct {v1, v2, v3, v4, v5}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1$$special$$inlined$let$lambda$1;->this$0:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1;

    iget-object v2, v2, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1;->$subscriber:Lcom/squareup/protos/postoffice/sms/Subscriber;

    .line 129
    iget-object v3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow$render$1$$special$$inlined$let$lambda$1;->$it:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    iget-object v3, v3, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->id:Ljava/lang/String;

    const-string v4, "it.id"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingState$SmsMarketingPrompt;-><init>(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;Lcom/squareup/protos/postoffice/sms/Subscriber;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Updater;->setNextState(Ljava/lang/Object;)V

    return-void
.end method
