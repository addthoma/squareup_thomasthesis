.class public final Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;
.super Ljava/lang/Object;
.source "ReceiptSmsMarketingPromptScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;,
        Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u001b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0086\u0008\u0018\u0000 02\u00020\u0001:\u000201Bi\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0012\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e0\r\u0012\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0002\u0010\u0011J\t\u0010 \u001a\u00020\u0003H\u00c6\u0003J\t\u0010!\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0007H\u00c6\u0003J\t\u0010#\u001a\u00020\tH\u00c6\u0003J\t\u0010$\u001a\u00020\u000bH\u00c6\u0003J\u0015\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003J\u0015\u0010&\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003J\u0015\u0010\'\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0003J}\u0010(\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0014\u0008\u0002\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e0\r2\u0014\u0008\u0002\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e0\r2\u0014\u0008\u0002\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e0\rH\u00c6\u0001J\u0013\u0010)\u001a\u00020*2\u0008\u0010+\u001a\u0004\u0018\u00010,H\u00d6\u0003J\t\u0010-\u001a\u00020.H\u00d6\u0001J\t\u0010/\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u001d\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u001bR\u001d\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001bR\u001d\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000e0\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001bR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001f\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "businessName",
        "",
        "couponInfo",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;",
        "buyerLanguageState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;",
        "updateCustomerState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;",
        "addCardState",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;",
        "onNewSaleClicked",
        "Lkotlin/Function1;",
        "",
        "onDeclineCouponClicked",
        "onAcceptCouponClicked",
        "(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V",
        "getAddCardState",
        "()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;",
        "getBusinessName",
        "()Ljava/lang/String;",
        "getBuyerLanguageState",
        "()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;",
        "getCouponInfo",
        "()Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;",
        "getOnAcceptCouponClicked",
        "()Lkotlin/jvm/functions/Function1;",
        "getOnDeclineCouponClicked",
        "getOnNewSaleClicked",
        "getUpdateCustomerState",
        "()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "Companion",
        "CouponInfo",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;


# instance fields
.field private final addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

.field private final businessName:Ljava/lang/String;

.field private final buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

.field private final couponInfo:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;

.field private final onAcceptCouponClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onDeclineCouponClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onNewSaleClicked:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->Companion:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$Companion;

    .line 29
    const-class v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "businessName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "couponInfo"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLanguageState"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "updateCustomerState"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "addCardState"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onNewSaleClicked"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDeclineCouponClicked"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onAcceptCouponClicked"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->businessName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->couponInfo:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;

    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    iput-object p5, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    iput-object p6, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    iput-object p7, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onDeclineCouponClicked:Lkotlin/jvm/functions/Function1;

    iput-object p8, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onAcceptCouponClicked:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 9
    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->businessName:Ljava/lang/String;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->couponInfo:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onDeclineCouponClicked:Lkotlin/jvm/functions/Function1;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onAcceptCouponClicked:Lkotlin/jvm/functions/Function1;

    goto :goto_7

    :cond_7
    move-object/from16 v1, p8

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->copy(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;

    move-result-object v0

    return-object v0
.end method

.method public static final getKEY()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->Companion:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method


# virtual methods
.method public final component1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->businessName:Ljava/lang/String;

    return-object v0
.end method

.method public final component2()Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->couponInfo:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;

    return-object v0
.end method

.method public final component3()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    return-object v0
.end method

.method public final component4()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    return-object v0
.end method

.method public final component5()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    return-object v0
.end method

.method public final component6()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component7()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onDeclineCouponClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component8()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onAcceptCouponClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;"
        }
    .end annotation

    const-string v0, "businessName"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "couponInfo"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerLanguageState"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "updateCustomerState"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "addCardState"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onNewSaleClicked"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onDeclineCouponClicked"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onAcceptCouponClicked"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;-><init>(Ljava/lang/String;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->businessName:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->businessName:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->couponInfo:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->couponInfo:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onDeclineCouponClicked:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onDeclineCouponClicked:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onAcceptCouponClicked:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onAcceptCouponClicked:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAddCardState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    return-object v0
.end method

.method public final getBusinessName()Ljava/lang/String;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->businessName:Ljava/lang/String;

    return-object v0
.end method

.method public final getBuyerLanguageState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;
    .locals 1

    .line 12
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    return-object v0
.end method

.method public final getCouponInfo()Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->couponInfo:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;

    return-object v0
.end method

.method public final getOnAcceptCouponClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onAcceptCouponClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnDeclineCouponClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 16
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onDeclineCouponClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnNewSaleClicked()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 15
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getUpdateCustomerState()Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;
    .locals 1

    .line 13
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->businessName:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->couponInfo:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onDeclineCouponClicked:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onAcceptCouponClicked:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReceiptSmsMarketingPromptScreen(businessName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->businessName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", couponInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->couponInfo:Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen$CouponInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", buyerLanguageState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->buyerLanguageState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$BuyerLanguageState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", updateCustomerState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->updateCustomerState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$UpdateCustomerState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", addCardState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->addCardState:Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$AddCardState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onNewSaleClicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onNewSaleClicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onDeclineCouponClicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onDeclineCouponClicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", onAcceptCouponClicked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingprompt/ReceiptSmsMarketingPromptScreen;->onAcceptCouponClicked:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
