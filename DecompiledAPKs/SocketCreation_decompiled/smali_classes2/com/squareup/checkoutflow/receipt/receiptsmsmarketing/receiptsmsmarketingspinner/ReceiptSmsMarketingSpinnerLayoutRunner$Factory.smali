.class public final Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$Factory;
.super Ljava/lang/Object;
.source "ReceiptSmsMarketingSpinnerLayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner$Factory;",
        "",
        "()V",
        "create",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;",
        "view",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final create(Landroid/view/View;)Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/receiptsmsmarketingspinner/ReceiptSmsMarketingSpinnerLayoutRunner;-><init>(Landroid/view/View;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
