.class public abstract Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;
.super Ljava/lang/Object;
.source "ReceiptSmsMarketingResult.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$NoSmsMarketing;,
        Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$SmsMarketingDeclined;,
        Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$SmsMarketingAccepted;,
        Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$BuyerLanguageClicked;,
        Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$UpdateCustomerClicked;,
        Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$AddCardClicked;,
        Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$ExitedManually;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0007\u0003\u0004\u0005\u0006\u0007\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0007\n\u000b\u000c\r\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;",
        "",
        "()V",
        "AddCardClicked",
        "BuyerLanguageClicked",
        "ExitedManually",
        "NoSmsMarketing",
        "SmsMarketingAccepted",
        "SmsMarketingDeclined",
        "UpdateCustomerClicked",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$NoSmsMarketing;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$SmsMarketingDeclined;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$SmsMarketingAccepted;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$BuyerLanguageClicked;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$UpdateCustomerClicked;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$AddCardClicked;",
        "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult$ExitedManually;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingResult;-><init>()V

    return-void
.end method
