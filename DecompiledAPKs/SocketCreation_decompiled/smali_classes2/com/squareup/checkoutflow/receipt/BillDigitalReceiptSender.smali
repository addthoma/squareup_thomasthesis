.class public final Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;
.super Ljava/lang/Object;
.source "BillDigitalReceiptSender.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\n\u0010\u0017\u001a\u0004\u0018\u00010\nH\u0016J\n\u0010\u0018\u001a\u0004\u0018\u00010\nH\u0016J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\nH\u0002J\u0012\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u001e\u001a\u00020\nH\u0016J\u0012\u0010\u001f\u001a\u0004\u0018\u00010 2\u0006\u0010\u001b\u001a\u00020\nH\u0016R\u0016\u0010\t\u001a\u0004\u0018\u00010\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u0016\u0010\r\u001a\u0004\u0018\u00010\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000cR\u0016\u0010\u000f\u001a\u0004\u0018\u00010\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u000cR\u0016\u0010\u0011\u001a\u0004\u0018\u00010\n8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u000cR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u00020\u00148BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;",
        "Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;",
        "receiptForLastPayment",
        "Lcom/squareup/payment/ReceiptForLastPayment;",
        "receiptSender",
        "Lcom/squareup/receipt/ReceiptSender;",
        "phoneNumbers",
        "Lcom/squareup/text/PhoneNumberHelper;",
        "(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/text/PhoneNumberHelper;)V",
        "contactEmail",
        "",
        "getContactEmail",
        "()Ljava/lang/String;",
        "contactSms",
        "getContactSms",
        "defaultEmail",
        "getDefaultEmail",
        "defaultSms",
        "getDefaultSms",
        "receipt",
        "Lcom/squareup/payment/PaymentReceipt;",
        "getReceipt",
        "()Lcom/squareup/payment/PaymentReceipt;",
        "getPrefilledEmail",
        "getPrefilledSms",
        "hasValidSms",
        "",
        "phoneNumber",
        "trySendEmailReceipt",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;",
        "emailAddress",
        "trySendSmsReceipt",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;",
        "impl-bill_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

.field private final receiptForLastPayment:Lcom/squareup/payment/ReceiptForLastPayment;

.field private final receiptSender:Lcom/squareup/receipt/ReceiptSender;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/text/PhoneNumberHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "receiptForLastPayment"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "receiptSender"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneNumbers"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->receiptForLastPayment:Lcom/squareup/payment/ReceiptForLastPayment;

    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    return-void
.end method

.method private final getContactEmail()Ljava/lang/String;
    .locals 2

    .line 27
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    const-string v1, "receipt.payment"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getEmail(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private final getContactSms()Ljava/lang/String;
    .locals 2

    .line 33
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getPayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    const-string v1, "receipt.payment"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getPhone(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private final getDefaultSms()Ljava/lang/String;
    .locals 1

    .line 30
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getDefaultSms()Lcom/squareup/payment/Obfuscated;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/payment/Obfuscated;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private final getReceipt()Lcom/squareup/payment/PaymentReceipt;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->receiptForLastPayment:Lcom/squareup/payment/ReceiptForLastPayment;

    invoke-interface {v0}, Lcom/squareup/payment/ReceiptForLastPayment;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    return-object v0
.end method

.method private final hasValidSms(Ljava/lang/String;)Z
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    invoke-interface {v0, p1}, Lcom/squareup/text/PhoneNumberHelper;->isValid(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method


# virtual methods
.method public getDefaultEmail()Ljava/lang/String;
    .locals 1

    .line 24
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getDefaultEmail()Lcom/squareup/payment/Obfuscated;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/payment/Obfuscated;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getPrefilledEmail()Ljava/lang/String;
    .locals 1

    .line 75
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getDefaultEmail()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getContactEmail()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getPrefilledSms()Ljava/lang/String;
    .locals 1

    .line 38
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getDefaultSms()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getContactSms()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public trySendEmailReceipt(Ljava/lang/String;)Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;
    .locals 4

    const-string v0, "emailAddress"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getPrefilledEmail()Ljava/lang/String;

    move-result-object v0

    .line 81
    move-object v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 83
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/receipt/ReceiptSender;->trySendEmailReceipt(Lcom/squareup/payment/PaymentReceipt;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    new-instance v3, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;

    invoke-direct {v3, p1}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 89
    :cond_0
    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    if-eqz v0, :cond_2

    .line 91
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getDefaultEmail()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 93
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/squareup/receipt/ReceiptSender;->sendEmailReceiptToDefault(Lcom/squareup/payment/PaymentReceipt;)V

    .line 94
    new-instance v3, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;

    invoke-direct {v3, v0}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 96
    :cond_1
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getContactEmail()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 98
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-interface {p1, v1, v0}, Lcom/squareup/receipt/ReceiptSender;->trySendEmailReceipt(Lcom/squareup/payment/PaymentReceipt;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 99
    new-instance v3, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;

    invoke-direct {v3, v0}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;-><init>(Ljava/lang/String;)V

    :cond_2
    :goto_0
    return-object v3
.end method

.method public trySendSmsReceipt(Ljava/lang/String;)Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;
    .locals 4

    const-string v0, "phoneNumber"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getPrefilledSms()Ljava/lang/String;

    move-result-object v0

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->hasValidSms(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    .line 46
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/receipt/ReceiptSender;->trySendSmsReceipt(Lcom/squareup/payment/PaymentReceipt;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 47
    new-instance v2, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;

    const/4 v0, 0x1

    invoke-direct {v2, p1, v0}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    .line 52
    :cond_0
    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    if-eqz v0, :cond_2

    .line 54
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getDefaultSms()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    .line 56
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/squareup/receipt/ReceiptSender;->sendSmsReceiptToDefault(Lcom/squareup/payment/PaymentReceipt;)V

    .line 57
    new-instance v2, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;

    invoke-direct {v2, v0, v1}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    .line 59
    :cond_1
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getContactSms()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 61
    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v3

    invoke-interface {p1, v3, v0}, Lcom/squareup/receipt/ReceiptSender;->trySendSmsReceipt(Lcom/squareup/payment/PaymentReceipt;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 62
    new-instance v2, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;

    invoke-direct {v2, v0, v1}, Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;-><init>(Ljava/lang/String;Z)V

    :cond_2
    :goto_0
    return-object v2
.end method
