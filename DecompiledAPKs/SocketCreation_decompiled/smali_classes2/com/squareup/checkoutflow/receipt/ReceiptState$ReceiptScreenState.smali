.class public abstract Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;
.super Ljava/lang/Object;
.source "ReceiptState.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/receipt/ReceiptState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ReceiptScreenState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptSelectionState;,
        Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;,
        Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;,
        Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InvalidInputState;,
        Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingState;,
        Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingResult;,
        Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00032\u00020\u0001:\u0007\u0003\u0004\u0005\u0006\u0007\u0008\tB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0005\n\u000b\u000c\r\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;",
        "Landroid/os/Parcelable;",
        "()V",
        "Companion",
        "InputState",
        "InvalidInputState",
        "ReceiptCompleteState",
        "ReceiptSelectionState",
        "SmsMarketingResult",
        "SmsMarketingState",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptSelectionState;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InputState;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$ReceiptCompleteState;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$InvalidInputState;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$SmsMarketingState;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;->Companion:Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptState$ReceiptScreenState;-><init>()V

    return-void
.end method
