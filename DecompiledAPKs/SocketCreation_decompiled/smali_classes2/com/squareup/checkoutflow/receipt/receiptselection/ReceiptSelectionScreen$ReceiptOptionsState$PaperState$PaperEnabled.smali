.class public final Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;
.super Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState;
.source "ReceiptSelectionScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PaperEnabled"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u0019\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0015\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u001f\u0010\t\u001a\u00020\u00002\u0014\u0008\u0002\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0010\u001a\u00020\u0011H\u00d6\u0001R\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;",
        "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState;",
        "onPaperSelected",
        "Lkotlin/Function1;",
        "",
        "(Lkotlin/jvm/functions/Function1;)V",
        "getOnPaperSelected",
        "()Lkotlin/jvm/functions/Function1;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final onPaperSelected:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "onPaperSelected"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 84
    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;->onPaperSelected:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;->onPaperSelected:Lkotlin/jvm/functions/Function1;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;->copy(Lkotlin/jvm/functions/Function1;)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;->onPaperSelected:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final copy(Lkotlin/jvm/functions/Function1;)Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;"
        }
    .end annotation

    const-string v0, "onPaperSelected"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;

    invoke-direct {v0, p1}, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;-><init>(Lkotlin/jvm/functions/Function1;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;->onPaperSelected:Lkotlin/jvm/functions/Function1;

    iget-object p1, p1, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;->onPaperSelected:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getOnPaperSelected()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;->onPaperSelected:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;->onPaperSelected:Lkotlin/jvm/functions/Function1;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PaperEnabled(onPaperSelected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/receiptselection/ReceiptSelectionScreen$ReceiptOptionsState$PaperState$PaperEnabled;->onPaperSelected:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
