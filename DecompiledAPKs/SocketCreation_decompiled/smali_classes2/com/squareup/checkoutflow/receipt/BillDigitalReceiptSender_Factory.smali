.class public final Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender_Factory;
.super Ljava/lang/Object;
.source "BillDigitalReceiptSender_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ReceiptForLastPayment;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ReceiptForLastPayment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ReceiptForLastPayment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;)",
            "Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender_Factory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/text/PhoneNumberHelper;)Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;-><init>(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/text/PhoneNumberHelper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ReceiptForLastPayment;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/receipt/ReceiptSender;

    iget-object v2, p0, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/text/PhoneNumberHelper;

    invoke-static {v0, v1, v2}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender_Factory;->newInstance(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/text/PhoneNumberHelper;)Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender_Factory;->get()Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;

    move-result-object v0

    return-object v0
.end method
