.class public abstract Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;
.super Ljava/lang/Object;
.source "ReceiptInput.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/receipt/ReceiptInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "PaymentTypeInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo;,
        Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;,
        Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u000b\u000c\rB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u0082\u0001\u0002\u000e\u000f\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;",
        "",
        "()V",
        "remainingAmountConfig",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;",
        "getRemainingAmountConfig",
        "()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;",
        "tenderedAmount",
        "Lcom/squareup/protos/common/Money;",
        "getTenderedAmount",
        "()Lcom/squareup/protos/common/Money;",
        "CashPaymentInfo",
        "PaymentInfo",
        "RemainingAmountConfig",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$CashPaymentInfo;",
        "Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$PaymentInfo;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getRemainingAmountConfig()Lcom/squareup/checkoutflow/receipt/ReceiptInput$PaymentTypeInfo$RemainingAmountConfig;
.end method

.method public abstract getTenderedAmount()Lcom/squareup/protos/common/Money;
.end method
