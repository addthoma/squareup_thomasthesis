.class public final Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;
.super Ljava/lang/Object;
.source "BillReceiptModule_ProvideReceiptWorkflowFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerFlowReceiptManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final curatedImageProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;"
        }
    .end annotation
.end field

.field private final digitalReceiptSenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;",
            ">;"
        }
    .end annotation
.end field

.field private final paperReceiptSenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptDeclinerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;",
            ">;"
        }
    .end annotation
.end field

.field private final receiptReaderHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final smsMarketingWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final yieldToFlowWorkerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;",
            ">;)V"
        }
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->receiptReaderHandlerProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p3, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->buyerFlowReceiptManagerProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p4, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p5, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->curatedImageProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p6, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->analyticsProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p7, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->smsMarketingWorkflowProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p8, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->yieldToFlowWorkerProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p9, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->digitalReceiptSenderProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p10, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->paperReceiptSenderProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p11, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->receiptDeclinerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;",
            ">;)",
            "Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;"
        }
    .end annotation

    .line 85
    new-instance v12, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static provideReceiptWorkflow(Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/analytics/Analytics;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;)Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;
    .locals 0

    .line 94
    invoke-static/range {p0 .. p10}, Lcom/squareup/checkoutflow/receipt/BillReceiptModule;->provideReceiptWorkflow(Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/analytics/Analytics;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;)Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;
    .locals 12

    .line 71
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->receiptReaderHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->buyerFlowReceiptManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/log/CheckoutInformationEventLogger;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->curatedImageProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/merchantimages/CuratedImage;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->smsMarketingWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->yieldToFlowWorkerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->digitalReceiptSenderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->paperReceiptSenderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;

    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->receiptDeclinerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;

    invoke-static/range {v1 .. v11}, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->provideReceiptWorkflow(Lcom/squareup/checkoutflow/receipt/ReceiptReaderHandler;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/analytics/Analytics;Lcom/squareup/checkoutflow/receipt/receiptsmsmarketing/ReceiptSmsMarketingWorkflow;Lcom/squareup/checkoutflow/receipt/YieldToFlowWorker;Lcom/squareup/checkoutflow/receipt/BillDigitalReceiptSender;Lcom/squareup/checkoutflow/receipt/BillPaperReceiptSender;Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;)Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/BillReceiptModule_ProvideReceiptWorkflowFactory;->get()Lcom/squareup/checkoutflow/receipt/ReceiptWorkflow;

    move-result-object v0

    return-object v0
.end method
