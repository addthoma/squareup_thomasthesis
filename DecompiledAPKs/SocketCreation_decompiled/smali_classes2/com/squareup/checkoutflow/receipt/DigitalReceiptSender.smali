.class public interface abstract Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;
.super Ljava/lang/Object;
.source "DigitalReceiptSender.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\n\u0010\u0006\u001a\u0004\u0018\u00010\u0003H&J\n\u0010\u0007\u001a\u0004\u0018\u00010\u0003H&J\u0012\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00020\u0003H&J\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u000c2\u0006\u0010\r\u001a\u00020\u0003H&R\u0014\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/receipt/DigitalReceiptSender;",
        "",
        "defaultEmail",
        "",
        "getDefaultEmail",
        "()Ljava/lang/String;",
        "getPrefilledEmail",
        "getPrefilledSms",
        "trySendEmailReceipt",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;",
        "emailAddress",
        "trySendSmsReceipt",
        "Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;",
        "phoneNumber",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getDefaultEmail()Ljava/lang/String;
.end method

.method public abstract getPrefilledEmail()Ljava/lang/String;
.end method

.method public abstract getPrefilledSms()Ljava/lang/String;
.end method

.method public abstract trySendEmailReceipt(Ljava/lang/String;)Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$EmailDestination;
.end method

.method public abstract trySendSmsReceipt(Ljava/lang/String;)Lcom/squareup/checkoutflow/receipt/ReceiptResult$ReceiptSelection$DigitalReceipt$DigitalDestination$SmsDestination;
.end method
