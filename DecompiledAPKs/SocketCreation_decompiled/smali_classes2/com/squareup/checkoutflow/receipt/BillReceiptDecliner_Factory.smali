.class public final Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner_Factory;
.super Ljava/lang/Object;
.source "BillReceiptDecliner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ReceiptForLastPayment;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ReceiptForLastPayment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ReceiptForLastPayment;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receipt/ReceiptSender;",
            ">;)",
            "Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/receipt/ReceiptSender;)Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;-><init>(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/receipt/ReceiptSender;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;
    .locals 2

    .line 26
    iget-object v0, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ReceiptForLastPayment;

    iget-object v1, p0, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/receipt/ReceiptSender;

    invoke-static {v0, v1}, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner_Factory;->newInstance(Lcom/squareup/payment/ReceiptForLastPayment;Lcom/squareup/receipt/ReceiptSender;)Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner_Factory;->get()Lcom/squareup/checkoutflow/receipt/BillReceiptDecliner;

    move-result-object v0

    return-object v0
.end method
