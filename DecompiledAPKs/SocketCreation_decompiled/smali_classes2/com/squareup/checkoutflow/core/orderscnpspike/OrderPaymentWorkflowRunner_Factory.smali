.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "OrderPaymentWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p4, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;",
            ">;)",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;-><init>(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;
    .locals 4

    .line 33
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/PosContainer;

    iget-object v2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;

    iget-object v3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;->newInstance(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflow;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingCompleter;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentViewFactory;)Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner_Factory;->get()Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
