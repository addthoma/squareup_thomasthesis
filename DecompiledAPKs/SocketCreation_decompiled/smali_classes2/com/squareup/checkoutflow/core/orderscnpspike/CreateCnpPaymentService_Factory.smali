.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;
.super Ljava/lang/Object;
.source "CreateCnpPaymentService_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/services/CheckoutFeService;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/CardConverter;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/services/CheckoutFeService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/CardConverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 29
    iput-object p4, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/services/CheckoutFeService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/CardConverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/checkoutflow/core/services/CheckoutFeService;Lcom/squareup/payment/CardConverter;Ljavax/inject/Provider;Ljava/lang/String;)Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/core/services/CheckoutFeService;",
            "Lcom/squareup/payment/CardConverter;",
            "Ljavax/inject/Provider<",
            "Landroid/location/Location;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;"
        }
    .end annotation

    .line 45
    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;-><init>(Lcom/squareup/checkoutflow/core/services/CheckoutFeService;Lcom/squareup/payment/CardConverter;Ljavax/inject/Provider;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;
    .locals 4

    .line 34
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/core/services/CheckoutFeService;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/CardConverter;

    iget-object v2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;->arg2Provider:Ljavax/inject/Provider;

    iget-object v3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;->newInstance(Lcom/squareup/checkoutflow/core/services/CheckoutFeService;Lcom/squareup/payment/CardConverter;Ljavax/inject/Provider;Ljava/lang/String;)Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService_Factory;->get()Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentService;

    move-result-object v0

    return-object v0
.end method
