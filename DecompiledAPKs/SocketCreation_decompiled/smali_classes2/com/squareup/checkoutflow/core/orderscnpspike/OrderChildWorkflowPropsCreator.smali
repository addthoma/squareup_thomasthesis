.class public final Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;
.super Ljava/lang/Object;
.source "OrderChildWorkflowPropsCreator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection;,
        Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0002)*B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\"\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u000eH\u0002J \u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\u000eH\u0002J\u0018\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0014\u001a\u00020\u000eH\u0002J\u0018\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u000eJ,\u0010\u001a\u001a\u0014\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u000e0\u001b2\u0006\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u0013\u001a\u0004\u0018\u00010\u000eH\u0002J\u0016\u0010\u001e\u001a\u00020\u001f2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010 \u001a\u00020!J\"\u0010\"\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u000e2\u0006\u0010#\u001a\u00020\u001c2\u0006\u0010$\u001a\u00020\u001dH\u0002J\u0010\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020\u000eH\u0002J\u0018\u0010(\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u000eH\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;",
        "",
        "buyerLocaleOverride",
        "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "createCnpPaymentWorkerFactory",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;",
        "(Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/CountryCode;Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;)V",
        "authNetworking",
        "Lcom/squareup/checkoutflow/core/auth/AuthNetworking;",
        "card",
        "Lcom/squareup/Card;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "tip",
        "autoGratuityAndTipText",
        "",
        "tenderAmount",
        "tipAmount",
        "autoGratuityAmount",
        "autoGratuityNoTipText",
        "createAuthProps",
        "Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;",
        "props",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;",
        "createBuyerActionBarProps",
        "Lkotlin/Triple;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection;",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity;",
        "createErrorProps",
        "Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;",
        "state",
        "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;",
        "createSubtitle",
        "tipSelection",
        "autoGratuity",
        "createTitle",
        "",
        "totalBeingAuthorized",
        "tipText",
        "AutoGratuity",
        "TipSelection",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

.field private final countryCode:Lcom/squareup/CountryCode;

.field private final createCnpPaymentWorkerFactory:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;


# direct methods
.method public constructor <init>(Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/CountryCode;Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "buyerLocaleOverride"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "createCnpPaymentWorkerFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iput-object p2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->countryCode:Lcom/squareup/CountryCode;

    iput-object p3, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->createCnpPaymentWorkerFactory:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;

    return-void
.end method

.method private final authNetworking(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/core/auth/AuthNetworking;
    .locals 2

    .line 175
    new-instance v0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->createCnpPaymentWorkerFactory:Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrdersCnpAuthNetworking;-><init>(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/core/orderscnpspike/CreateCnpPaymentWorker$Factory;)V

    check-cast v0, Lcom/squareup/checkoutflow/core/auth/AuthNetworking;

    return-object v0
.end method

.method private final autoGratuityAndTipText(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;
    .locals 4

    .line 127
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v0

    .line 128
    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v1

    .line 129
    invoke-static {p1, p3}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 130
    iget-object v2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->countryCode:Lcom/squareup/CountryCode;

    sget-object v3, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v2}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v2

    aget v2, v3, v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 132
    sget v2, Lcom/squareup/checkoutflow/core/orderscnpspike/impl/R$string;->buyer_tip_plus_auto_grat_plus_amount:I

    goto :goto_0

    .line 131
    :cond_0
    sget v2, Lcom/squareup/checkoutflow/core/orderscnpspike/impl/R$string;->buyer_tip_plus_service_charge_plus_amount:I

    .line 134
    :goto_0
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 135
    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v2, "amount"

    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 136
    invoke-interface {v1, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p3

    const-string v0, "auto_grat"

    invoke-virtual {p1, v0, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 137
    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string p3, "tip"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 138
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "buyerRes.phrase(patternR\u2026mount))\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final autoGratuityNoTipText(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;
    .locals 4

    .line 145
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v0

    .line 146
    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v1

    .line 147
    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMath;->subtract(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 148
    iget-object v2, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->countryCode:Lcom/squareup/CountryCode;

    sget-object v3, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v2}, Lcom/squareup/CountryCode;->ordinal()I

    move-result v2

    aget v2, v3, v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 150
    sget v2, Lcom/squareup/checkoutflow/core/orderscnpspike/impl/R$string;->buyer_tip_no_tip_amount_plus_auto_grat:I

    goto :goto_0

    .line 149
    :cond_0
    sget v2, Lcom/squareup/checkoutflow/core/orderscnpspike/impl/R$string;->buyer_tip_no_tip_amount_plus_service_charge:I

    .line 152
    :goto_0
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 153
    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    const-string v2, "amount"

    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 154
    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v0, "auto_grat"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 155
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "buyerRes.phrase(patternR\u2026mount))\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final createBuyerActionBarProps(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;Lcom/squareup/protos/common/Money;)Lkotlin/Triple;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;",
            "Lcom/squareup/protos/common/Money;",
            ")",
            "Lkotlin/Triple<",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection;",
            "Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity;",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 70
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz p2, :cond_0

    .line 71
    iget-object v1, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_0

    .line 72
    new-instance v1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection$TipAmount;

    invoke-direct {v1, p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection$TipAmount;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast v1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection;

    goto :goto_0

    .line 74
    :cond_0
    sget-object p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection$NoTip;->INSTANCE:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection$NoTip;

    move-object v1, p2

    check-cast v1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection;

    .line 77
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;->getTipConfiguration()Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration;

    move-result-object p1

    .line 79
    instance-of p2, p1, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipEnabled;

    if-eqz p2, :cond_2

    check-cast p1, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipEnabled;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipEnabled;->getAutoGratuity()Lcom/squareup/protos/common/Money;

    move-result-object p2

    if-eqz p2, :cond_2

    .line 80
    new-instance p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity$AutoGratuityEnabled;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/TipConfiguration$TipEnabled;->getAutoGratuity()Lcom/squareup/protos/common/Money;

    move-result-object p1

    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    invoke-direct {p2, p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity$AutoGratuityEnabled;-><init>(Lcom/squareup/protos/common/Money;)V

    check-cast p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity;

    goto :goto_1

    .line 82
    :cond_2
    sget-object p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity$AutoGratuityDisabled;->INSTANCE:Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity$AutoGratuityDisabled;

    move-object p2, p1

    check-cast p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity;

    .line 85
    :goto_1
    instance-of p1, v1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection$TipAmount;

    if-eqz p1, :cond_3

    .line 86
    move-object p1, v1

    check-cast p1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection$TipAmount;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection$TipAmount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/money/MoneyMath;->sum(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 90
    :cond_3
    new-instance p1, Lkotlin/Triple;

    invoke-direct {p1, v1, p2, v0}, Lkotlin/Triple;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method

.method private final createSubtitle(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity;)Ljava/lang/CharSequence;
    .locals 2

    .line 104
    instance-of v0, p3, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity$AutoGratuityEnabled;

    if-eqz v0, :cond_0

    instance-of v1, p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection$TipAmount;

    if-eqz v1, :cond_0

    .line 106
    check-cast p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection$TipAmount;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection$TipAmount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 107
    check-cast p3, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity$AutoGratuityEnabled;

    invoke-virtual {p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity$AutoGratuityEnabled;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p3

    .line 104
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->autoGratuityAndTipText(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    .line 111
    check-cast p3, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity$AutoGratuityEnabled;

    invoke-virtual {p3}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity$AutoGratuityEnabled;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 109
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->autoGratuityNoTipText(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 113
    :cond_1
    instance-of p3, p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection$TipAmount;

    if-eqz p3, :cond_2

    .line 115
    check-cast p2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection$TipAmount;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection$TipAmount;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 113
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->tipText(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private final createTitle(Lcom/squareup/protos/common/Money;)Ljava/lang/String;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 95
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final tipText(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Ljava/lang/CharSequence;
    .locals 3

    .line 162
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v0}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerRes()Lcom/squareup/util/Res;

    move-result-object v0

    .line 163
    iget-object v1, p0, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->buyerLocaleOverride:Lcom/squareup/buyer/language/BuyerLocaleOverride;

    invoke-interface {v1}, Lcom/squareup/buyer/language/BuyerLocaleOverride;->getBuyerMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v1

    .line 164
    sget v2, Lcom/squareup/checkoutflow/core/orderscnpspike/impl/R$string;->buyer_tip_plus_amount:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 165
    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v2, "amount"

    invoke-virtual {v0, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 166
    invoke-interface {v1, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    const-string v0, "tip"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 167
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "buyerRes.phrase(R.string\u2026mount))\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public final createAuthProps(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;
    .locals 8

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;->getPaymentType()Lcom/squareup/checkoutflow/core/orderscnpspike/PaymentType;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/squareup/checkoutflow/core/orderscnpspike/PaymentType$ManualCardEntry;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/PaymentType$ManualCardEntry;->getCard()Lcom/squareup/Card;

    move-result-object v6

    .line 35
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->createBuyerActionBarProps(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;Lcom/squareup/protos/common/Money;)Lkotlin/Triple;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/Triple;->component1()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection;

    invoke-virtual {v0}, Lkotlin/Triple;->component2()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity;

    invoke-virtual {v0}, Lkotlin/Triple;->component3()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    .line 37
    new-instance v7, Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;

    const/4 v3, 0x0

    .line 41
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v4

    .line 39
    invoke-direct {p0, v6, v4, p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->authNetworking(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/checkoutflow/core/auth/AuthNetworking;

    move-result-object p2

    .line 44
    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->createTitle(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v4

    .line 46
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 45
    invoke-direct {p0, p1, v1, v2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->createSubtitle(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity;)Ljava/lang/CharSequence;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 47
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move-object v5, p1

    move-object v1, v7

    move v2, v3

    move-object v3, p2

    .line 37
    invoke-direct/range {v1 .. v6}, Lcom/squareup/checkoutflow/core/auth/CardProcessingProps;-><init>(ZLcom/squareup/checkoutflow/core/auth/AuthNetworking;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/Card;)V

    return-object v7

    .line 33
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.checkoutflow.core.orderscnpspike.PaymentType.ManualCardEntry"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final createErrorProps(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;)Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;
    .locals 4

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->getTipAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->createBuyerActionBarProps(Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;Lcom/squareup/protos/common/Money;)Lkotlin/Triple;

    move-result-object v0

    invoke-virtual {v0}, Lkotlin/Triple;->component1()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection;

    invoke-virtual {v0}, Lkotlin/Triple;->component2()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity;

    invoke-virtual {v0}, Lkotlin/Triple;->component3()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    .line 58
    new-instance v3, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;

    .line 59
    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->createTitle(Lcom/squareup/protos/common/Money;)Ljava/lang/String;

    move-result-object v0

    .line 60
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentProps;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-direct {p0, p1, v1, v2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator;->createSubtitle(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$TipSelection;Lcom/squareup/checkoutflow/core/orderscnpspike/OrderChildWorkflowPropsCreator$AutoGratuity;)Ljava/lang/CharSequence;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 61
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 62
    invoke-virtual {p2}, Lcom/squareup/checkoutflow/core/orderscnpspike/OrderPaymentState$Error;->getMessage()Ljava/lang/String;

    move-result-object p2

    .line 58
    invoke-direct {v3, v0, p1, v1, p2}, Lcom/squareup/checkoutflow/core/error/OrderErrorWorkflowProps;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v3
.end method
