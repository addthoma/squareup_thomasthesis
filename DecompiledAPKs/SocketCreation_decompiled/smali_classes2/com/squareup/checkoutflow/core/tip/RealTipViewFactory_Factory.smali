.class public final Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory_Factory;
.super Ljava/lang/Object;
.source "RealTipViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$Factory;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 19
    iput-object p2, p0, Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$Factory;",
            ">;)",
            "Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/checkoutflow/core/tip/TipCoordinator$Factory;Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$Factory;)Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory;
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory;-><init>(Lcom/squareup/checkoutflow/core/tip/TipCoordinator$Factory;Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory;
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/core/tip/TipCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$Factory;

    invoke-static {v0, v1}, Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory_Factory;->newInstance(Lcom/squareup/checkoutflow/core/tip/TipCoordinator$Factory;Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$Factory;)Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory_Factory;->get()Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory;

    move-result-object v0

    return-object v0
.end method
