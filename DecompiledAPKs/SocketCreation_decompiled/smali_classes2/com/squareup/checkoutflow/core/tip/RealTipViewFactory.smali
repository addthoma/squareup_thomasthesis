.class public final Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "RealTipViewFactory.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/core/tip/TipViewFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0017\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory;",
        "Lcom/squareup/checkoutflow/core/tip/TipViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "tipFactory",
        "Lcom/squareup/checkoutflow/core/tip/TipCoordinator$Factory;",
        "tipInputFactory",
        "Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$Factory;",
        "(Lcom/squareup/checkoutflow/core/tip/TipCoordinator$Factory;Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/core/tip/TipCoordinator$Factory;Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$Factory;)V
    .locals 22
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    const-string v2, "tipFactory"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "tipInputFactory"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 13
    sget-object v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 14
    sget-object v4, Lcom/squareup/checkoutflow/core/tip/TipScreen;->Companion:Lcom/squareup/checkoutflow/core/tip/TipScreen$Companion;

    invoke-virtual {v4}, Lcom/squareup/checkoutflow/core/tip/TipScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    .line 15
    sget v5, Lcom/squareup/checkoutflow/core/tip/impl/R$layout;->tip_view:I

    .line 16
    new-instance v6, Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory$1;

    invoke-direct {v6, v0}, Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory$1;-><init>(Lcom/squareup/checkoutflow/core/tip/TipCoordinator$Factory;)V

    move-object v8, v6

    check-cast v8, Lkotlin/jvm/functions/Function1;

    .line 17
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v15, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x0

    const/16 v19, 0x15f

    const/16 v20, 0x0

    move-object v9, v0

    invoke-direct/range {v9 .. v20}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v7, 0x0

    const/16 v9, 0x8

    move-object v6, v0

    .line 13
    invoke-static/range {v3 .. v10}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 19
    sget-object v4, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 20
    sget-object v0, Lcom/squareup/checkoutflow/core/tip/TipInputScreen;->Companion:Lcom/squareup/checkoutflow/core/tip/TipInputScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/core/tip/TipInputScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v5

    .line 21
    sget v6, Lcom/squareup/checkout/R$layout;->buyer_input_view:I

    .line 22
    new-instance v0, Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory$2;

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/core/tip/RealTipViewFactory$2;-><init>(Lcom/squareup/checkoutflow/core/tip/TipInputCoordinator$Factory;)V

    move-object v9, v0

    check-cast v9, Lkotlin/jvm/functions/Function1;

    .line 23
    new-instance v0, Lcom/squareup/workflow/ScreenHint;

    sget-object v16, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x1df

    const/16 v21, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v21}, Lcom/squareup/workflow/ScreenHint;-><init>(Lcom/squareup/workflow/WorkflowViewFactory$Orientation;Lcom/squareup/workflow/WorkflowViewFactory$Orientation;ZLjava/lang/Class;Lcom/squareup/workflow/SoftInputMode;Lcom/squareup/container/spot/Spot;ZZLjava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v8, 0x0

    const/16 v10, 0x8

    move-object v7, v0

    .line 19
    invoke-static/range {v4 .. v11}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, v2, v1

    move-object/from16 v0, p0

    .line 12
    invoke-direct {v0, v2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
