.class public final Lcom/squareup/checkoutflow/RealCheckoutWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealCheckoutWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/CheckoutWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;",
        "Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;",
        "Lcom/squareup/checkoutflow/CheckoutResult;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/checkoutflow/CheckoutWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealCheckoutWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealCheckoutWorkflow.kt\ncom/squareup/checkoutflow/RealCheckoutWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,153:1\n85#2:154\n240#3:155\n276#4:156\n*E\n*S KotlinDebug\n*F\n+ 1 RealCheckoutWorkflow.kt\ncom/squareup/checkoutflow/RealCheckoutWorkflow\n*L\n136#1:154\n136#1:155\n136#1:156\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0080\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012@\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n0\u0002BK\u0008\u0007\u0012\u000c\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u000c\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u00a2\u0006\u0002\u0010\u001aJ\u001a\u0010\'\u001a\u00020\u00042\u0006\u0010(\u001a\u00020\u00032\u0008\u0010)\u001a\u0004\u0018\u00010*H\u0016JR\u0010+\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0007\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0008j\u0002`\t0\u0006j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0007`\n2\u0006\u0010(\u001a\u00020\u00032\u0006\u0010,\u001a\u00020\u00042\u0012\u0010-\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050.H\u0016J\u0008\u0010/\u001a\u000200H\u0002J\u0010\u00101\u001a\u00020*2\u0006\u0010,\u001a\u00020\u0004H\u0016R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u001b\u001a\u00020\r8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001e\u0010\u001f\u001a\u0004\u0008\u001c\u0010\u001dR\u001b\u0010 \u001a\u00020\u00118BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008#\u0010\u001f\u001a\u0004\u0008!\u0010\"R\u0014\u0010$\u001a\u0008\u0012\u0004\u0012\u00020&0%X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/RealCheckoutWorkflow;",
        "Lcom/squareup/checkoutflow/CheckoutWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;",
        "Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;",
        "Lcom/squareup/checkoutflow/CheckoutResult;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "lazyTenderPaymentResultHandler",
        "Ldagger/Lazy;",
        "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "lazyTenderPaymentWorkflow",
        "Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "blockedByBuyerFacingDisplayWorkflow",
        "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;",
        "loyaltySellerCartBannerFormatter",
        "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
        "loyaltyFrontOfTransactionEvents",
        "Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;",
        "(Ldagger/Lazy;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;Lcom/squareup/settings/server/Features;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;)V",
        "tenderPaymentResultHandler",
        "getTenderPaymentResultHandler",
        "()Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
        "tenderPaymentResultHandler$delegate",
        "Ldagger/Lazy;",
        "tenderPaymentWorkflow",
        "getTenderPaymentWorkflow",
        "()Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;",
        "tenderPaymentWorkflow$delegate",
        "unexpectedNavigationWorker",
        "Lcom/squareup/workflow/Worker;",
        "Lcom/squareup/checkoutflow/CheckoutResult$CheckoutEndedUnexpectedly;",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "showSellerOverride",
        "",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final blockedByBuyerFacingDisplayWorkflow:Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final loyaltyFrontOfTransactionEvents:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

.field private final loyaltySellerCartBannerFormatter:Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;

.field private final tenderPaymentResultHandler$delegate:Ldagger/Lazy;

.field private final tenderPaymentWorkflow$delegate:Ldagger/Lazy;

.field private final unexpectedNavigationWorker:Lcom/squareup/workflow/Worker;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/Worker<",
            "Lcom/squareup/checkoutflow/CheckoutResult$CheckoutEndedUnexpectedly;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-class v0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;

    const/4 v1, 0x2

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "tenderPaymentResultHandler"

    const-string v5, "getTenderPaymentResultHandler()Lcom/squareup/tenderpayment/TenderPaymentResultHandler;"

    invoke-direct {v2, v3, v4, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-string v3, "tenderPaymentWorkflow"

    const-string v4, "getTenderPaymentWorkflow()Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;"

    invoke-direct {v2, v0, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Ldagger/Lazy;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;Lcom/squareup/settings/server/Features;Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;",
            "Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;",
            "Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "lazyTenderPaymentResultHandler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyTenderPaymentWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "blockedByBuyerFacingDisplayWorkflow"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltySellerCartBannerFormatter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "loyaltyFrontOfTransactionEvents"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p4, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->features:Lcom/squareup/settings/server/Features;

    iput-object p5, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->blockedByBuyerFacingDisplayWorkflow:Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;

    iput-object p6, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->loyaltySellerCartBannerFormatter:Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;

    iput-object p7, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->loyaltyFrontOfTransactionEvents:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    .line 55
    iput-object p1, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->tenderPaymentResultHandler$delegate:Ldagger/Lazy;

    .line 56
    iput-object p3, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->tenderPaymentWorkflow$delegate:Ldagger/Lazy;

    .line 128
    invoke-static {p2}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object p1

    const-wide/16 p2, 0x1

    .line 131
    invoke-virtual {p1, p2, p3}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object p1

    .line 132
    sget-object p4, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$unexpectedNavigationWorker$1;->INSTANCE:Lcom/squareup/checkoutflow/RealCheckoutWorkflow$unexpectedNavigationWorker$1;

    check-cast p4, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, p4}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    .line 133
    invoke-virtual {p1, p2, p3}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object p1

    .line 134
    invoke-virtual {p1}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object p1

    .line 135
    sget-object p2, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$unexpectedNavigationWorker$2;->INSTANCE:Lcom/squareup/checkoutflow/RealCheckoutWorkflow$unexpectedNavigationWorker$2;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "container.nextScreen()\n \u2026eckoutEndedUnexpectedly }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$$special$$inlined$asWorker$1;

    const/4 p3, 0x0

    invoke-direct {p2, p1, p3}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$$special$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 155
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 156
    const-class p2, Lcom/squareup/checkoutflow/CheckoutResult$CheckoutEndedUnexpectedly;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object p2

    new-instance p3, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p3, p2, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p3, Lcom/squareup/workflow/Worker;

    .line 154
    iput-object p3, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->unexpectedNavigationWorker:Lcom/squareup/workflow/Worker;

    return-void
.end method

.method public static final synthetic access$getLoyaltyFrontOfTransactionEvents$p(Lcom/squareup/checkoutflow/RealCheckoutWorkflow;)Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->loyaltyFrontOfTransactionEvents:Lcom/squareup/loyaltycheckin/LoyaltyFrontOfTransactionEvents;

    return-object p0
.end method

.method public static final synthetic access$getTenderPaymentResultHandler$p(Lcom/squareup/checkoutflow/RealCheckoutWorkflow;)Lcom/squareup/tenderpayment/TenderPaymentResultHandler;
    .locals 0

    .line 39
    invoke-direct {p0}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->getTenderPaymentResultHandler()Lcom/squareup/tenderpayment/TenderPaymentResultHandler;

    move-result-object p0

    return-object p0
.end method

.method private final getTenderPaymentResultHandler()Lcom/squareup/tenderpayment/TenderPaymentResultHandler;
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->tenderPaymentResultHandler$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResultHandler;

    return-object v0
.end method

.method private final getTenderPaymentWorkflow()Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->tenderPaymentWorkflow$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;

    return-object v0
.end method

.method private final showSellerOverride()Z
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->loyaltySellerCartBannerFormatter:Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;

    invoke-interface {v0}, Lcom/squareup/loyaltycheckin/LoyaltySellerCartBannerFormatter;->isCheckInFlowActiveSynchronous()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_X2_FRONT_OF_TRANSACTION_CHECKIN_SELLER_OVERRIDE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;
    .locals 1

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->showSellerOverride()Z

    move-result p1

    if-eqz p1, :cond_0

    new-instance p1, Lcom/squareup/checkoutflow/RealCheckoutWorkflowState$BlockedByBuyerFacingDisplay;

    const/4 p2, 0x1

    const/4 v0, 0x0

    invoke-direct {p1, v0, p2, v0}, Lcom/squareup/checkoutflow/RealCheckoutWorkflowState$BlockedByBuyerFacingDisplay;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;

    goto :goto_0

    .line 63
    :cond_0
    sget-object p1, Lcom/squareup/checkoutflow/RealCheckoutWorkflowState$InCheckout;->INSTANCE:Lcom/squareup/checkoutflow/RealCheckoutWorkflowState$InCheckout;

    check-cast p1, Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->initialState(Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;

    check-cast p2, Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->render(Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;",
            "Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;",
            "-",
            "Lcom/squareup/checkoutflow/CheckoutResult;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object v2, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->unexpectedNavigationWorker:Lcom/squareup/workflow/Worker;

    sget-object v0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$1;->INSTANCE:Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$1;

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, p3

    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 74
    instance-of v0, p2, Lcom/squareup/checkoutflow/RealCheckoutWorkflowState$BlockedByBuyerFacingDisplay;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 78
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v3, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$2;

    invoke-direct {v3, p0, v2}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$2;-><init>(Lcom/squareup/checkoutflow/RealCheckoutWorkflow;Lkotlin/coroutines/Continuation;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v3}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object v0

    invoke-static {p3, v0, v2, v1, v2}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->blockedByBuyerFacingDisplayWorkflow:Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayWorkflow;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 84
    new-instance v3, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayProps;

    .line 85
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->getPaymentConfig()Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/datamodels/payment/PaymentConfig;->getAmountToCollect()Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 86
    new-instance v0, Lcom/squareup/resources/ResourceCharSequence;

    sget v1, Lcom/squareup/checkoutflow/impl/R$string;->blocked_by_loyalty_checkin_title:I

    invoke-direct {v0, v1}, Lcom/squareup/resources/ResourceCharSequence;-><init>(I)V

    check-cast v0, Lcom/squareup/resources/TextModel;

    .line 87
    new-instance v1, Lcom/squareup/resources/ResourceCharSequence;

    sget v4, Lcom/squareup/checkoutflow/impl/R$string;->blocked_by_loyalty_checkin_body:I

    invoke-direct {v1, v4}, Lcom/squareup/resources/ResourceCharSequence;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    .line 88
    check-cast p2, Lcom/squareup/checkoutflow/RealCheckoutWorkflowState$BlockedByBuyerFacingDisplay;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/RealCheckoutWorkflowState$BlockedByBuyerFacingDisplay;->getUuid()Ljava/lang/String;

    move-result-object p2

    .line 84
    invoke-direct {v3, p1, v0, v1, p2}, Lcom/squareup/checkoutflow/BlockedByBuyerFacingDisplayProps;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/resources/TextModel;Lcom/squareup/resources/TextModel;Ljava/lang/String;)V

    const/4 v4, 0x0

    .line 90
    new-instance p1, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$3;-><init>(Lcom/squareup/checkoutflow/RealCheckoutWorkflow;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p3

    .line 82
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    goto :goto_0

    .line 105
    :cond_0
    sget-object v0, Lcom/squareup/checkoutflow/RealCheckoutWorkflowState$InCheckout;->INSTANCE:Lcom/squareup/checkoutflow/RealCheckoutWorkflowState$InCheckout;

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 106
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$4;

    invoke-direct {v0, p0, v2}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$4;-><init>(Lcom/squareup/checkoutflow/RealCheckoutWorkflow;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p2, v0}, Lcom/squareup/workflow/Worker$Companion;->createSideEffect(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;

    move-result-object p2

    invoke-static {p3, p2, v2, v1, v2}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 112
    invoke-direct {p0}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->getTenderPaymentWorkflow()Lcom/squareup/tenderpayment/TenderPaymentV2Workflow;

    move-result-object p2

    move-object v1, p2

    check-cast v1, Lcom/squareup/workflow/Workflow;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;->getTenderConfig()Lcom/squareup/tenderpayment/TenderPaymentConfig;

    move-result-object v2

    const/4 v3, 0x0

    new-instance p1, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$5;

    invoke-direct {p1, p0}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow$render$5;-><init>(Lcom/squareup/checkoutflow/RealCheckoutWorkflow;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p3

    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public snapshotState(Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 39
    check-cast p1, Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/RealCheckoutWorkflow;->snapshotState(Lcom/squareup/checkoutflow/RealCheckoutWorkflowState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
