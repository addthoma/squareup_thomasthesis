.class public final Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;
.super Ljava/lang/Object;
.source "EmoneyMiryoInput.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0013\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0014\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0008H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0008H\u00c6\u0003J;\u0010\u0018\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\u0008H\u00c6\u0001J\u0013\u0010\u0019\u001a\u00020\u00082\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\t\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0010R\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u000e\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;",
        "",
        "money",
        "Lcom/squareup/protos/common/Money;",
        "brand",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;",
        "previousBalance",
        "shouldShowUnresolved",
        "",
        "hasValidReaderConnected",
        "(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;Lcom/squareup/protos/common/Money;ZZ)V",
        "getBrand",
        "()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;",
        "getHasValidReaderConnected",
        "()Z",
        "getMoney",
        "()Lcom/squareup/protos/common/Money;",
        "getPreviousBalance",
        "getShouldShowUnresolved",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

.field private final hasValidReaderConnected:Z

.field private final money:Lcom/squareup/protos/common/Money;

.field private final previousBalance:Lcom/squareup/protos/common/Money;

.field private final shouldShowUnresolved:Z


# direct methods
.method public constructor <init>(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;Lcom/squareup/protos/common/Money;ZZ)V
    .locals 1

    const-string v0, "money"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "brand"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previousBalance"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->money:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    iput-object p3, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->previousBalance:Lcom/squareup/protos/common/Money;

    iput-boolean p4, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->shouldShowUnresolved:Z

    iput-boolean p5, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->hasValidReaderConnected:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;Lcom/squareup/protos/common/Money;ZZILjava/lang/Object;)Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->money:Lcom/squareup/protos/common/Money;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->previousBalance:Lcom/squareup/protos/common/Money;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->shouldShowUnresolved:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->hasValidReaderConnected:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->copy(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->money:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component2()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/common/Money;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->previousBalance:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->shouldShowUnresolved:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->hasValidReaderConnected:Z

    return v0
.end method

.method public final copy(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;Lcom/squareup/protos/common/Money;ZZ)Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;
    .locals 7

    const-string v0, "money"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "brand"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "previousBalance"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;Lcom/squareup/protos/common/Money;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->money:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->money:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    iget-object v1, p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->previousBalance:Lcom/squareup/protos/common/Money;

    iget-object v1, p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->previousBalance:Lcom/squareup/protos/common/Money;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->shouldShowUnresolved:Z

    iget-boolean v1, p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->shouldShowUnresolved:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->hasValidReaderConnected:Z

    iget-boolean p1, p1, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->hasValidReaderConnected:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    return-object v0
.end method

.method public final getHasValidReaderConnected()Z
    .locals 1

    .line 11
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->hasValidReaderConnected:Z

    return v0
.end method

.method public final getMoney()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 7
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->money:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getPreviousBalance()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 9
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->previousBalance:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public final getShouldShowUnresolved()Z
    .locals 1

    .line 10
    iget-boolean v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->shouldShowUnresolved:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->money:Lcom/squareup/protos/common/Money;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->previousBalance:Lcom/squareup/protos/common/Money;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->shouldShowUnresolved:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->hasValidReaderConnected:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EmoneyMiryoInput(money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->brand:Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", previousBalance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->previousBalance:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shouldShowUnresolved="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->shouldShowUnresolved:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", hasValidReaderConnected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/checkoutflow/emoney/miryo/EmoneyMiryoInput;->hasValidReaderConnected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
