.class public final Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;
.super Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;
.source "EmoneyPaymentProcessingScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReadyForTap"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B!\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0015\u0010\u000c\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0006H\u00c6\u0003J)\u0010\u000e\u001a\u00020\u00002\u0014\u0008\u0002\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\tR\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;",
        "onBackPressed",
        "Lkotlin/Function1;",
        "",
        "emoneyIconFactory",
        "Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;",
        "(Lkotlin/jvm/functions/Function1;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V",
        "getEmoneyIconFactory",
        "()Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;",
        "getOnBackPressed",
        "()Lkotlin/jvm/functions/Function1;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

.field private final onBackPressed:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;",
            ")V"
        }
    .end annotation

    const-string v0, "onBackPressed"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emoneyIconFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 27
    invoke-direct {p0, v0}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->onBackPressed:Lkotlin/jvm/functions/Function1;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;Lkotlin/jvm/functions/Function1;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;ILjava/lang/Object;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->onBackPressed:Lkotlin/jvm/functions/Function1;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->copy(Lkotlin/jvm/functions/Function1;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->onBackPressed:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component2()Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;
    .locals 1

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    return-object v0
.end method

.method public final copy(Lkotlin/jvm/functions/Function1;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;",
            ")",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;"
        }
    .end annotation

    const-string v0, "onBackPressed"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emoneyIconFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;

    invoke-direct {v0, p1, p2}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;-><init>(Lkotlin/jvm/functions/Function1;Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->onBackPressed:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->onBackPressed:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    iget-object p1, p1, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getEmoneyIconFactory()Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    return-object v0
.end method

.method public final getOnBackPressed()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lkotlin/Unit;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->onBackPressed:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->onBackPressed:Lkotlin/jvm/functions/Function1;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReadyForTap(onBackPressed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->onBackPressed:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", emoneyIconFactory="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingScreen$ScreenState$ReadyForTap;->emoneyIconFactory:Lcom/squareup/checkoutflow/emoney/EmoneyIconFactory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
