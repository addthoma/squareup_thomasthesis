.class public final Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyStateTransitionEventKt;
.super Ljava/lang/Object;
.source "EmoneyStateTransitionEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0014\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0007\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0008\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\t\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\n\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000b\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000c\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\r\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000e\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u000f\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0010\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0011\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0012\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0013\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0014\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "AUTH_FAILURE",
        "",
        "AUTH_SUCCESS",
        "MIRYO_AUTH_DATA",
        "MIRYO_MONEY_MOVED",
        "MIRYO_NETWORK_ERROR",
        "MIRYO_NO_MONEY_MOVED",
        "MIRYO_READER_CONNECTED",
        "MIRYO_READER_DISCONNECTED",
        "MIRYO_RESTART",
        "MIRYO_START_PAYMENT",
        "MIRYO_TMN_ERROR",
        "READER_DISCONNECTED",
        "READER_RECONNECTED",
        "RECEIVED_AUTH_DATA",
        "TMN_BALANCE_UPDATE",
        "TMN_WORKFLOW_FINISHED",
        "TMN_WORKFLOW_NETWORK_ERROR",
        "USER_CLICKED_CANCEL",
        "USER_CLICKED_RETRY",
        "USER_DISMISSED_CANCEL_DIALOG",
        "impl_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final AUTH_FAILURE:Ljava/lang/String; = "Auth Failure"

.field public static final AUTH_SUCCESS:Ljava/lang/String; = "Auth Success"

.field public static final MIRYO_AUTH_DATA:Ljava/lang/String; = "Miryo Received auth data from reader"

.field public static final MIRYO_MONEY_MOVED:Ljava/lang/String; = "Miryo money was moved"

.field public static final MIRYO_NETWORK_ERROR:Ljava/lang/String; = "Miryo Network Error"

.field public static final MIRYO_NO_MONEY_MOVED:Ljava/lang/String; = "Miryo No money was moved"

.field public static final MIRYO_READER_CONNECTED:Ljava/lang/String; = "Miryo Reader Connected"

.field public static final MIRYO_READER_DISCONNECTED:Ljava/lang/String; = "Miryo Reader Disconnected"

.field public static final MIRYO_RESTART:Ljava/lang/String; = "Miryo Restart"

.field public static final MIRYO_START_PAYMENT:Ljava/lang/String; = "Miryo Start Payment"

.field public static final MIRYO_TMN_ERROR:Ljava/lang/String; = "Miryo TMN Error"

.field public static final READER_DISCONNECTED:Ljava/lang/String; = "Reader Disconnected"

.field public static final READER_RECONNECTED:Ljava/lang/String; = "Reader Reconnected"

.field public static final RECEIVED_AUTH_DATA:Ljava/lang/String; = "Received auth data from reader"

.field public static final TMN_BALANCE_UPDATE:Ljava/lang/String; = "New balance returned"

.field public static final TMN_WORKFLOW_FINISHED:Ljava/lang/String; = "TMN Workflow Finished"

.field public static final TMN_WORKFLOW_NETWORK_ERROR:Ljava/lang/String; = "TMN Workflow Network Error"

.field public static final USER_CLICKED_CANCEL:Ljava/lang/String; = "User clicked Cancel"

.field public static final USER_CLICKED_RETRY:Ljava/lang/String; = "User clicked Retry"

.field public static final USER_DISMISSED_CANCEL_DIALOG:Ljava/lang/String; = "User dismissed cancel dialog"
