.class public interface abstract Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;
.super Ljava/lang/Object;
.source "EmoneyBrandSelectionWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/Workflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/Workflow<",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002&\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00050\u0004j\u0006\u0012\u0002\u0008\u0003`\u00060\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;",
        "Lcom/squareup/workflow/Workflow;",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation
