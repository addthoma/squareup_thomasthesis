.class public final Lcom/squareup/checkoutflow/emoney/brandselection/RealEmoneyBrandSelectionWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealEmoneyBrandSelectionWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealEmoneyBrandSelectionWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealEmoneyBrandSelectionWorkflow.kt\ncom/squareup/checkoutflow/emoney/brandselection/RealEmoneyBrandSelectionWorkflow\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,65:1\n1360#2:66\n1429#2,3:67\n149#3,5:70\n*E\n*S KotlinDebug\n*F\n+ 1 RealEmoneyBrandSelectionWorkflow.kt\ncom/squareup/checkoutflow/emoney/brandselection/RealEmoneyBrandSelectionWorkflow\n*L\n37#1:66\n37#1,3:67\n55#1,5:70\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012&\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u00070\u0002B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0008J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000cH\u0002J6\u0010\u000e\u001a\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u00072\u0006\u0010\u000f\u001a\u00020\u00032\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00040\u0011H\u0016\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/brandselection/RealEmoneyBrandSelectionWorkflow;",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionResult;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "()V",
        "isBrandSelectionEnabled",
        "",
        "amountToCollect",
        "Lcom/squareup/protos/common/Money;",
        "emoneyMin",
        "render",
        "input",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method

.method private final isBrandSelectionEnabled(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z
    .locals 0

    .line 63
    invoke-static {p1, p2}, Lcom/squareup/money/MoneyMath;->greaterThanOrEqualTo(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    return p1
.end method


# virtual methods
.method public render(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 7

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;->getAmountToCollect()Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;->getEmoneyMin()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/checkoutflow/emoney/brandselection/RealEmoneyBrandSelectionWorkflow;->isBrandSelectionEnabled(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    new-instance v0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection$Enabled;

    sget-object v1, Lcom/squareup/checkoutflow/emoney/brandselection/RealEmoneyBrandSelectionWorkflow$render$brandSelection$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/brandselection/RealEmoneyBrandSelectionWorkflow$render$brandSelection$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection$Enabled;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection;

    goto :goto_0

    .line 34
    :cond_0
    sget-object v0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection$Disabled;->INSTANCE:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection$Disabled;

    check-cast v0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection;

    :goto_0
    move-object v6, v0

    .line 37
    sget-object v0, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;->getSuicaMax()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->getBrandList(Lcom/squareup/protos/common/Money;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 66
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 67
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 68
    check-cast v2, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    .line 38
    new-instance v3, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;

    .line 39
    sget-object v4, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->INSTANCE:Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;

    invoke-virtual {v4, v2}, Lcom/squareup/checkoutflow/emoney/EmoneyBrandUtil;->getDisplayNameForBrand(Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)I

    move-result v4

    .line 40
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;->getAmountToCollect()Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;->enabled(Lcom/squareup/protos/common/Money;)Z

    move-result v5

    .line 38
    invoke-direct {v3, v4, v5, v2}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;-><init>(IZLcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V

    .line 42
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 69
    :cond_1
    move-object v3, v1

    check-cast v3, Ljava/util/List;

    .line 45
    new-instance v0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;

    .line 46
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;->getAmountToCollect()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 48
    sget-object p1, Lcom/squareup/checkoutflow/emoney/brandselection/RealEmoneyBrandSelectionWorkflow$render$1;->INSTANCE:Lcom/squareup/checkoutflow/emoney/brandselection/RealEmoneyBrandSelectionWorkflow$render$1;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v4

    .line 51
    sget-object p1, Lcom/squareup/checkoutflow/emoney/brandselection/RealEmoneyBrandSelectionWorkflow$render$2;->INSTANCE:Lcom/squareup/checkoutflow/emoney/brandselection/RealEmoneyBrandSelectionWorkflow$render$2;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v5

    move-object v1, v0

    .line 45
    invoke-direct/range {v1 .. v6}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;-><init>(Lcom/squareup/protos/common/Money;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection;)V

    check-cast v0, Lcom/squareup/workflow/legacy/V2Screen;

    .line 71
    new-instance p1, Lcom/squareup/workflow/legacy/Screen;

    .line 72
    const-class p2, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen;

    invoke-static {p2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p2

    const-string v1, ""

    invoke-static {p2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p2

    .line 73
    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v1

    .line 71
    invoke-direct {p1, p2, v0, v1}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/emoney/brandselection/RealEmoneyBrandSelectionWorkflow;->render(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
