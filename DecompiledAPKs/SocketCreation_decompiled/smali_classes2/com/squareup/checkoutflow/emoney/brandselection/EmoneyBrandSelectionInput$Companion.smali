.class public final Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput$Companion;
.super Ljava/lang/Object;
.source "EmoneyBrandSelectionInput.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEmoneyBrandSelectionInput.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EmoneyBrandSelectionInput.kt\ncom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput$Companion\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,39:1\n180#2:40\n56#3:41\n56#3:42\n56#3:43\n*E\n*S KotlinDebug\n*F\n+ 1 EmoneyBrandSelectionInput.kt\ncom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput$Companion\n*L\n30#1:40\n30#1:41\n30#1:42\n30#1:43\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput$Companion;",
        "",
        "()V",
        "fromByteStringSnapshot",
        "Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;",
        "byteString",
        "Lokio/ByteString;",
        "start",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromByteStringSnapshot(Lokio/ByteString;)Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;
    .locals 4

    const-string v0, "byteString"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 41
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v1, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    .line 42
    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v2, Lcom/squareup/protos/common/Money;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/common/Money;

    .line 43
    sget-object v2, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v2, v3}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/common/Money;

    .line 31
    new-instance v2, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;

    invoke-direct {v2, v0, v1, p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V

    return-object v2
.end method

.method public final start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;
    .locals 1

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget-object v0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;->Companion:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput$Companion;

    .line 24
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    .line 23
    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput$Companion;->fromByteStringSnapshot(Lokio/ByteString;)Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;

    move-result-object p1

    return-object p1
.end method
