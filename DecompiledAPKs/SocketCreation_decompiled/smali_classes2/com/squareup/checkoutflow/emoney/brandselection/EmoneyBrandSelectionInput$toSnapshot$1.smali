.class final Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput$toSnapshot$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EmoneyBrandSelectionInput.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;->toSnapshot()Lcom/squareup/workflow/Snapshot;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSink;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "buffer",
        "Lokio/BufferedSink;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput$toSnapshot$1;->this$0:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10
    check-cast p1, Lokio/BufferedSink;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput$toSnapshot$1;->invoke(Lokio/BufferedSink;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lokio/BufferedSink;)V
    .locals 1

    const-string v0, "buffer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput$toSnapshot$1;->this$0:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;->getAmountToCollect()Lcom/squareup/protos/common/Money;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 16
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput$toSnapshot$1;->this$0:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;->getEmoneyMin()Lcom/squareup/protos/common/Money;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    .line 17
    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput$toSnapshot$1;->this$0:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionInput;->getSuicaMax()Lcom/squareup/protos/common/Money;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    invoke-static {p1, v0}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    return-void
.end method
