.class public final Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$createEmoneyRow$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "EmoneyBrandSelectionCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator;->createEmoneyRow(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection;)Lcom/squareup/marin/widgets/BorderedLinearLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$createEmoneyRow$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $brandRow:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;

.field final synthetic $brandSelection:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection;Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;)V
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$createEmoneyRow$1;->$brandSelection:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection;

    iput-object p2, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$createEmoneyRow$1;->$brandRow:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    iget-object p1, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$createEmoneyRow$1;->$brandSelection:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection;

    check-cast p1, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection$Enabled;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$BrandSelection$Enabled;->getOnSelect()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionCoordinator$createEmoneyRow$1;->$brandRow:Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/emoney/brandselection/EmoneyBrandSelectionScreen$EmoneyBrandRow;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object v0

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
