.class public final Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;
.super Ljava/lang/Object;
.source "BillPaymentProcessingStarter.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u001a\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u000c2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;",
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingStarter;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "x2ScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "swipeHandler",
        "Lcom/squareup/ui/payment/SwipeHandler;",
        "activeCardReader",
        "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
        "(Lcom/squareup/payment/Transaction;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/cardreader/dipper/ActiveCardReader;)V",
        "startManualCardEntryBillsPayment",
        "",
        "amountDue",
        "Lcom/squareup/protos/common/Money;",
        "card",
        "Lcom/squareup/Card;",
        "startPayment",
        "paymentProcessingInput",
        "Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/cardreader/dipper/ActiveCardReader;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "x2ScreenRunner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "swipeHandler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activeCardReader"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p2, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iput-object p3, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    iput-object p4, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    return-void
.end method

.method private final startManualCardEntryBillsPayment(Lcom/squareup/protos/common/Money;Lcom/squareup/Card;)V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0, p1, p2}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->tenderKeyedCard(Lcom/squareup/protos/common/Money;Lcom/squareup/Card;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 35
    iget-object p1, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1, p2}, Lcom/squareup/payment/Transaction;->setCard(Lcom/squareup/Card;)V

    .line 36
    iget-object p1, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/payment/SwipeHandler;->readyToAuthorize()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 37
    iget-object p1, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {p1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->cancelPaymentsOnNonActiveCardReaders()V

    .line 38
    iget-object p1, p0, Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/payment/SwipeHandler;->authorize()V

    :cond_0
    return-void
.end method


# virtual methods
.method public startPayment(Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput;)V
    .locals 1

    const-string v0, "paymentProcessingInput"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    instance-of v0, p1, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput$ManualCardEntry;

    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    check-cast p1, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput$ManualCardEntry;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/orderbillpaymentfork/PaymentProcessingInput$ManualCardEntry;->getCard()Lcom/squareup/Card;

    move-result-object p1

    .line 24
    invoke-direct {p0, v0, p1}, Lcom/squareup/checkoutflow/orderbillpaymentfork/BillPaymentProcessingStarter;->startManualCardEntryBillsPayment(Lcom/squareup/protos/common/Money;Lcom/squareup/Card;)V

    :cond_0
    return-void
.end method
