.class final Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask$execute$1;
.super Ljava/lang/Object;
.source "InstallmentsSmsTask.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->execute(Lcom/squareup/server/SquareCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSResponse;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u0004\u0012\u00020\u0004 \u0005*\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "response",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSResponse;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $callback:Lcom/squareup/server/SquareCallback;

.field final synthetic this$0:Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;Lcom/squareup/server/SquareCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask$execute$1;->this$0:Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask$execute$1;->$callback:Lcom/squareup/server/SquareCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/capital/consumer/service/SendInstallmentsSMSResponse;",
            ">;)V"
        }
    .end annotation

    .line 33
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_2

    .line 34
    move-object v0, p1

    check-cast v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {v0}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object v0

    .line 36
    instance-of v1, v0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v1, :cond_0

    .line 37
    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    invoke-virtual {v0}, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->getStatusCode()I

    move-result v0

    goto :goto_0

    .line 39
    :cond_0
    instance-of v1, v0, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v1, :cond_1

    .line 40
    check-cast v0, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    invoke-virtual {v0}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getStatusCode()I

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    .line 45
    :goto_0
    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask$execute$1;->this$0:Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask;->getAnalytics()Lcom/squareup/analytics/Analytics;

    move-result-object v1

    new-instance v2, Lcheckoutflow/installments/impl/src/main/java/com/squareup/checkoutflow/installments/InstallmentsSmsErrorEvent;

    invoke-direct {v2, v0}, Lcheckoutflow/installments/impl/src/main/java/com/squareup/checkoutflow/installments/InstallmentsSmsErrorEvent;-><init>(I)V

    check-cast v2, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 48
    :cond_2
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask$execute$1;->$callback:Lcom/squareup/server/SquareCallback;

    sget-object v1, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask$execute$1$1;->INSTANCE:Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask$execute$1$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/SquareCallback;->accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsSmsTask$execute$1;->accept(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    return-void
.end method
