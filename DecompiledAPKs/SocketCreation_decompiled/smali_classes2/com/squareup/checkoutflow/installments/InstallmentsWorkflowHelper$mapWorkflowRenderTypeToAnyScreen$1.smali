.class final Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper$mapWorkflowRenderTypeToAnyScreen$1;
.super Lkotlin/jvm/internal/Lambda;
.source "InstallmentsWorkflowHelper.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper;->mapWorkflowRenderTypeToAnyScreen(Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;)Lcom/squareup/workflow/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u0006\u0008\u0001\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0002H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;",
        "",
        "it",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper$mapWorkflowRenderTypeToAnyScreen$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper$mapWorkflowRenderTypeToAnyScreen$1;

    invoke-direct {v0}, Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper$mapWorkflowRenderTypeToAnyScreen$1;-><init>()V

    sput-object v0, Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper$mapWorkflowRenderTypeToAnyScreen$1;->INSTANCE:Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper$mapWorkflowRenderTypeToAnyScreen$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;)Lcom/squareup/workflow/legacy/Screen;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    instance-of v0, p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;

    invoke-static {p1}, Lcom/squareup/checkoutflow/installments/options/InstallmentsOptionsScreenKt;->createInstallmentsOptionsScreen(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 24
    :cond_0
    instance-of v0, p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;

    invoke-static {p1}, Lcom/squareup/checkoutflow/installments/linkOptions/InstallmentsLinkOptionsScreenKt;->createInstallmentsLinkOptionsScreen(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 25
    :cond_1
    instance-of v0, p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;

    invoke-static {p1}, Lcom/squareup/checkoutflow/installments/qrCode/InstallmentsQRCodeScreenKt;->createInstallmentsQRCodeScreen(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 26
    :cond_2
    instance-of v0, p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;

    invoke-static {p1}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputScreenKt;->createInstallmentsSmsInputScreen(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    goto :goto_0

    .line 27
    :cond_3
    instance-of v0, p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsSentScreenData;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsSentScreenData;

    invoke-static {p1}, Lcom/squareup/checkoutflow/installments/SmsSent/InstallmentsSmsSentScreenKt;->createInstallmentsSmsSentScreen(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsSentScreenData;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsWorkflowHelper$mapWorkflowRenderTypeToAnyScreen$1;->invoke(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
