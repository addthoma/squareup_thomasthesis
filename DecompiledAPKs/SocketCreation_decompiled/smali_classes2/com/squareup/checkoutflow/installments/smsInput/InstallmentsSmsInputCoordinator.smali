.class public final Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "InstallmentsSmsInputCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nInstallmentsSmsInputCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 InstallmentsSmsInputCoordinator.kt\ncom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator\n*L\n1#1,102:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0001!B;\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0010\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0018\u0010\u001f\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u0011\u001a\u00020\u0005H\u0002J\u0008\u0010 \u001a\u00020\u001bH\u0002R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;",
        "",
        "Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "scrubber",
        "Lcom/squareup/text/InsertingScrubber;",
        "phoneNumbers",
        "Lcom/squareup/text/PhoneNumberHelper;",
        "(Lio/reactivex/Observable;Lcom/squareup/util/Res;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/PhoneNumberHelper;)V",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "data",
        "hint",
        "Landroid/widget/TextView;",
        "sendButton",
        "Landroid/widget/Button;",
        "smsInput",
        "Lcom/squareup/widgets/SelectableEditText;",
        "title",
        "Lcom/squareup/marketfont/MarketTextView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "update",
        "updateSendEnabled",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private final data:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;",
            ">;"
        }
    .end annotation
.end field

.field private hint:Landroid/widget/TextView;

.field private final phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final scrubber:Lcom/squareup/text/InsertingScrubber;

.field private sendButton:Landroid/widget/Button;

.field private smsInput:Lcom/squareup/widgets/SelectableEditText;

.field private title:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/util/Res;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/PhoneNumberHelper;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/InsertingScrubber;",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scrubber"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "phoneNumbers"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->scrubber:Lcom/squareup/text/InsertingScrubber;

    iput-object p4, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    .line 43
    sget-object p2, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$data$1;->INSTANCE:Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$data$1;

    check-cast p2, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "screens.map { it.data }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->data:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getPhoneNumbers$p(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;)Lcom/squareup/text/PhoneNumberHelper;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    return-object p0
.end method

.method public static final synthetic access$getSmsInput$p(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;)Lcom/squareup/widgets/SelectableEditText;
    .locals 1

    .line 25
    iget-object p0, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    if-nez p0, :cond_0

    const-string v0, "smsInput"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setSmsInput$p(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;Lcom/squareup/widgets/SelectableEditText;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->update(Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;)V

    return-void
.end method

.method public static final synthetic access$updateSendEnabled(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->updateSendEnabled()V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 95
    sget v0, Lcom/squareup/marin/R$id;->buyer_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(com.sq\u2026in.R.id.buyer_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 96
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_sms_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.installments_sms_title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    .line 97
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_sms_input:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.installments_sms_input)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    .line 98
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_send_button:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string/jumbo v1, "view.findViewById(R.id.installments_send_button)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->sendButton:Landroid/widget/Button;

    .line 99
    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$id;->installments_sms_hint:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.installments_sms_hint)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->hint:Landroid/widget/TextView;

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;)V
    .locals 3

    .line 70
    new-instance v0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$update$1;

    invoke-direct {v0, p2}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$update$1;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 73
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    if-nez p1, :cond_0

    const-string v0, "buyerActionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v1, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$update$2;

    invoke-direct {v1, p2}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$update$2;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 77
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->title:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_1

    const-string v0, "title"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_sms_prompt:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    if-nez p1, :cond_2

    const-string v0, "smsInput"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_sms_input_hint:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 80
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->sendButton:Landroid/widget/Button;

    const-string v0, "sendButton"

    if-nez p1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_sms_send:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->sendButton:Landroid/widget/Button;

    if-nez p1, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$update$3;

    invoke-direct {v0, p0, p2}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$update$3;-><init>(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    invoke-direct {p0}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->updateSendEnabled()V

    .line 87
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->hint:Landroid/widget/TextView;

    if-nez p1, :cond_5

    const-string p2, "hint"

    invoke-static {p2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    iget-object p2, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/checkoutflow/installments/impl/R$string;->installments_sms_hint:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateSendEnabled()V
    .locals 4

    .line 91
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->sendButton:Landroid/widget/Button;

    if-nez v0, :cond_0

    const-string v1, "sendButton"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->phoneNumbers:Lcom/squareup/text/PhoneNumberHelper;

    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    if-nez v2, :cond_1

    const-string v3, "smsInput"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lkotlin/text/StringsKt;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/text/PhoneNumberHelper;->isValid(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_2
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type kotlin.CharSequence"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->bindViews(Landroid/view/View;)V

    .line 54
    new-instance v0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$attach$smsTextWatcher$1;

    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->scrubber:Lcom/squareup/text/InsertingScrubber;

    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    const-string v3, "smsInput"

    if-nez v2, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v2, Lcom/squareup/text/HasSelectableText;

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$attach$smsTextWatcher$1;-><init>(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    .line 60
    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->smsInput:Lcom/squareup/widgets/SelectableEditText;

    if-nez v1, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;->data:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator$attach$1;-><init>(Lcom/squareup/checkoutflow/installments/smsInput/InstallmentsSmsInputCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "data.subscribe { update(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
