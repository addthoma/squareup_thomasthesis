.class public final Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper_Factory;
.super Ljava/lang/Object;
.source "RealInstallmentsCardEntryScreenDataHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;)",
            "Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper_Factory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)",
            "Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;

    invoke-direct {v0, p0, p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;-><init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;
    .locals 2

    .line 27
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/Formatter;

    invoke-static {v0, v1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper_Factory;->get()Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;

    move-result-object v0

    return-object v0
.end method
