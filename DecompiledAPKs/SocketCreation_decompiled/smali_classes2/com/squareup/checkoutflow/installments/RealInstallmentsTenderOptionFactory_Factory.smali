.class public final Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory_Factory;
.super Ljava/lang/Object;
.source "RealInstallmentsTenderOptionFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p3, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;",
            ">;)",
            "Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;)Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
            ">;",
            "Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;",
            ")",
            "Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;-><init>(Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;
    .locals 3

    .line 32
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;

    invoke-static {v0, v1, v2}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory_Factory;->newInstance(Lcom/squareup/util/Res;Ldagger/Lazy;Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;)Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory_Factory;->get()Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;

    move-result-object v0

    return-object v0
.end method
