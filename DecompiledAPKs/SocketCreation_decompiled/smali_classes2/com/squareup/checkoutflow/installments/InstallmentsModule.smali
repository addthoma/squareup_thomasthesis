.class public abstract Lcom/squareup/checkoutflow/installments/InstallmentsModule;
.super Ljava/lang/Object;
.source "InstallmentsModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindEmoneyTenderOptionFactoryIntoMainActivity(Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;)Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract bindInstallmentsIntoMainActivity(Lcom/squareup/checkoutflow/installments/InstallmentsViewFactory;)Lcom/squareup/workflow/WorkflowViewFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method abstract provideInstallmentsCardEntryScreenDataHelper(Lcom/squareup/checkoutflow/installments/RealInstallmentsCardEntryScreenDataHelper;)Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideInstallmentsTenderOptionFactory(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;)Lcom/squareup/checkoutflow/installments/InstallmentsTenderOptionFactory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideInstallmentsWorkflow(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;)Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
