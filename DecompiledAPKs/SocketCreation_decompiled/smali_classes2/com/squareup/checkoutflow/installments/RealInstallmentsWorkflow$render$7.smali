.class final Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$7;
.super Lkotlin/jvm/internal/Lambda;
.source "RealInstallmentsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->render(Lcom/squareup/checkoutflow/installments/InstallmentsInput;Lcom/squareup/checkoutflow/installments/InstallmentsState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
        "+",
        "Lcom/squareup/checkoutflow/installments/InstallmentsOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsOutput;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/checkoutflow/installments/InstallmentsState;

.field final synthetic this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;Lcom/squareup/checkoutflow/installments/InstallmentsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$7;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$7;->$state:Lcom/squareup/checkoutflow/installments/InstallmentsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
            "Lcom/squareup/checkoutflow/installments/InstallmentsOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 148
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;

    iget-object v0, v0, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;->qr_code_image_bytes:Lokio/ByteString;

    invoke-virtual {v0}, Lokio/ByteString;->toByteArray()[B

    move-result-object v0

    const/4 v3, 0x0

    .line 149
    array-length v4, v0

    invoke-static {v0, v3, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    .line 153
    iget-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$7;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;

    invoke-static {p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->access$getAnalytics$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object v0, Lcom/squareup/analytics/RegisterErrorName;->INSTALLMENTS_QR_FAILED_IMAGE_CONVERSION:Lcom/squareup/analytics/RegisterErrorName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logError(Lcom/squareup/analytics/RegisterErrorName;)V

    .line 154
    new-instance p1, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeError;

    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$7;->$state:Lcom/squareup/checkoutflow/installments/InstallmentsState;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/installments/InstallmentsState;->getToken()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeError;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsState;

    goto :goto_0

    .line 156
    :cond_0
    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoaded;

    iget-object v3, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$7;->$state:Lcom/squareup/checkoutflow/installments/InstallmentsState;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/installments/InstallmentsState;->getToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;

    iget-object p1, p1, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;->qr_code_image_bytes:Lokio/ByteString;

    const-string v4, "it.response.qr_code_image_bytes"

    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v3, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoaded;-><init>(Ljava/lang/String;Lokio/ByteString;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsState;

    .line 159
    :goto_0
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 161
    :cond_1
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    if-eqz v0, :cond_4

    .line 162
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 164
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    if-eqz v0, :cond_2

    .line 165
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ServerError;->getStatusCode()I

    move-result p1

    goto :goto_1

    .line 167
    :cond_2
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    if-eqz v0, :cond_3

    .line 168
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Error$ClientError;->getStatusCode()I

    move-result p1

    goto :goto_1

    :cond_3
    const/4 p1, -0x1

    .line 173
    :goto_1
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$7;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;

    invoke-static {v0}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->access$getAnalytics$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    new-instance v3, Lcheckoutflow/installments/impl/src/main/java/com/squareup/checkoutflow/installments/InstallmentsQrErrorEvent;

    invoke-direct {v3, p1}, Lcheckoutflow/installments/impl/src/main/java/com/squareup/checkoutflow/installments/InstallmentsQrErrorEvent;-><init>(I)V

    check-cast v3, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 175
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 176
    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeError;

    iget-object v3, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$7;->$state:Lcom/squareup/checkoutflow/installments/InstallmentsState;

    invoke-virtual {v3}, Lcom/squareup/checkoutflow/installments/InstallmentsState;->getToken()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeError;-><init>(Ljava/lang/String;)V

    .line 175
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_2
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$7;->invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
