.class final Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealInstallmentsTenderOptionFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;->render(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionInput;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/checkoutflow/installments/InstallmentsOutput;",
        "Lcom/squareup/workflow/WorkflowAction;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult;",
        "it",
        "Lcom/squareup/checkoutflow/installments/InstallmentsOutput;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1$render$2;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/checkoutflow/installments/InstallmentsOutput;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    instance-of v0, p1, Lcom/squareup/checkoutflow/installments/InstallmentsOutput$Canceled;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Canceled;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Canceled;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 75
    :cond_0
    instance-of v0, p1, Lcom/squareup/checkoutflow/installments/InstallmentsOutput$Finished$Unsuccessful;

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Finished$Unsuccessful;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Finished$Unsuccessful;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 76
    :cond_1
    instance-of v0, p1, Lcom/squareup/checkoutflow/installments/InstallmentsOutput$Finished$Successful;

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Finished$Successful;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$Finished$Successful;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 77
    :cond_2
    instance-of v0, p1, Lcom/squareup/checkoutflow/installments/InstallmentsOutput$ManualCardEntry;

    if-eqz v0, :cond_3

    .line 80
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1$render$2;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;

    iget-object v0, v0, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1;->this$0:Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;

    invoke-static {v0}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;->access$getTenderPaymentResultHandler$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory;)Ldagger/Lazy;

    move-result-object v0

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tenderpayment/TenderPaymentResultHandler;

    new-instance v1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayCard;

    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsOutput$ManualCardEntry;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/installments/InstallmentsOutput$ManualCardEntry;->getScreenData()Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$PayCard;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;)V

    check-cast v1, Lcom/squareup/tenderpayment/TenderPaymentResult;

    invoke-interface {v0, v1}, Lcom/squareup/tenderpayment/TenderPaymentResultHandler;->handlePaymentResult(Lcom/squareup/tenderpayment/TenderPaymentResult;)V

    .line 81
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$DoNothing;->INSTANCE:Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionResult$DoNothing;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 53
    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsTenderOptionFactory$workflow$1$render$2;->invoke(Lcom/squareup/checkoutflow/installments/InstallmentsOutput;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
