.class public final Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealInstallmentsWorkflow.kt"

# interfaces
.implements Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/checkoutflow/installments/InstallmentsInput;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsOutput;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;",
        ">;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealInstallmentsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealInstallmentsWorkflow.kt\ncom/squareup/checkoutflow/installments/RealInstallmentsWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/Worker$Companion\n+ 4 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,239:1\n85#2:240\n240#3:241\n276#4:242\n*E\n*S KotlinDebug\n*F\n+ 1 RealInstallmentsWorkflow.kt\ncom/squareup/checkoutflow/installments/RealInstallmentsWorkflow\n*L\n144#1:240\n144#1:241\n144#1:242\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u001a\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0002B;\u0008\u0007\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0008\u0008\u0001\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0008\u0008\u0001\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0002\u0010\u0013J\u001a\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u00032\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016J,\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\u00032\u0006\u0010\u0019\u001a\u00020\u00042\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u0004H\u0016R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsInput;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsOutput;",
        "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;",
        "manualCardEntryScreenDataHelper",
        "Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;",
        "res",
        "Lcom/squareup/util/Res;",
        "taskQueue",
        "Lcom/squareup/queue/retrofit/RetrofitQueue;",
        "installmentsService",
        "Lcom/squareup/checkoutflow/installments/InstallmentsService;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;Lcom/squareup/util/Res;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/checkoutflow/installments/InstallmentsService;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;)V",
        "initialState",
        "input",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "snapshotState",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final installmentsService:Lcom/squareup/checkoutflow/installments/InstallmentsService;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final manualCardEntryScreenDataHelper:Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;

.field private final res:Lcom/squareup/util/Res;

.field private final taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;


# direct methods
.method public constructor <init>(Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;Lcom/squareup/util/Res;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/checkoutflow/installments/InstallmentsService;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .param p3    # Lcom/squareup/queue/retrofit/RetrofitQueue;
        .annotation runtime Lcom/squareup/queue/Tasks;
        .end annotation
    .end param
    .param p5    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "manualCardEntryScreenDataHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taskQueue"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "installmentsService"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->manualCardEntryScreenDataHelper:Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;

    iput-object p2, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iput-object p4, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->installmentsService:Lcom/squareup/checkoutflow/installments/InstallmentsService;

    iput-object p5, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p6, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getManualCardEntryScreenDataHelper$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;)Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->manualCardEntryScreenDataHelper:Lcom/squareup/checkoutflow/installments/InstallmentsCardEntryScreenDataHelper;

    return-object p0
.end method

.method public static final synthetic access$getTaskQueue$p(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 0

    .line 57
    iget-object p0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-object p0
.end method


# virtual methods
.method public initialState(Lcom/squareup/checkoutflow/installments/InstallmentsInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/installments/InstallmentsState;
    .locals 1

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 76
    sget-object p1, Lcom/squareup/checkoutflow/installments/InstallmentsState;->Companion:Lcom/squareup/checkoutflow/installments/InstallmentsState$Companion;

    invoke-virtual {p2}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/checkoutflow/installments/InstallmentsState$Companion;->fromSnapshot(Lokio/ByteString;)Lcom/squareup/checkoutflow/installments/InstallmentsState;

    move-result-object p1

    return-object p1

    .line 77
    :cond_0
    new-instance p1, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingInstallmentsOptions;

    const/4 p2, 0x1

    const/4 v0, 0x0

    invoke-direct {p1, v0, p2, v0}, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingInstallmentsOptions;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsState;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->initialState(Lcom/squareup/checkoutflow/installments/InstallmentsInput;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/checkoutflow/installments/InstallmentsState;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/checkoutflow/installments/InstallmentsInput;Lcom/squareup/checkoutflow/installments/InstallmentsState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkoutflow/installments/InstallmentsInput;",
            "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/checkoutflow/installments/InstallmentsState;",
            "-",
            "Lcom/squareup/checkoutflow/installments/InstallmentsOutput;",
            ">;)",
            "Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    instance-of v0, p2, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingInstallmentsOptions;

    if-eqz v0, :cond_1

    .line 88
    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;

    .line 89
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/installments/InstallmentsInput;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 90
    sget-object v2, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$1;->INSTANCE:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 93
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/installments/InstallmentsInput;->isSplitTender()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 94
    sget-object p2, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData$LinkAction$Disabled;->INSTANCE:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData$LinkAction$Disabled;

    check-cast p2, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData$LinkAction;

    goto :goto_0

    .line 96
    :cond_0
    iget-object v3, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v4, Lcom/squareup/analytics/RegisterTapName;->INSTALLMENTS_START_APPLICATION_LINK:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v3, v4}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 98
    new-instance v3, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData$LinkAction$Enabled;

    .line 99
    new-instance v4, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$2;

    invoke-direct {v4, p2}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$2;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsState;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 98
    invoke-direct {v3, p2}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData$LinkAction$Enabled;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object p2, v3

    check-cast p2, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData$LinkAction;

    .line 104
    :goto_0
    new-instance v3, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$3;

    invoke-direct {v3, p0, p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$3;-><init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;Lcom/squareup/checkoutflow/installments/InstallmentsInput;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 88
    invoke-direct {v0, v1, v2, p2, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData;-><init>(Lcom/squareup/protos/common/Money;Lkotlin/jvm/functions/Function1;Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsOptionsScreenData$LinkAction;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;

    goto/16 :goto_1

    .line 117
    :cond_1
    instance-of v0, p2, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingInstallmentsLinkOptions;

    if-eqz v0, :cond_2

    .line 118
    new-instance p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;

    .line 119
    sget-object v0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$4;->INSTANCE:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$4;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v0}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v0

    .line 122
    new-instance v1, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$5;

    invoke-direct {v1, p0, p2}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$5;-><init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;Lcom/squareup/checkoutflow/installments/InstallmentsState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v1

    .line 127
    new-instance v2, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$6;

    invoke-direct {v2, p0, p2}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$6;-><init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;Lcom/squareup/checkoutflow/installments/InstallmentsState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 118
    invoke-direct {p1, v0, v1, p2}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsLinkOptionsScreenData;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;

    goto/16 :goto_1

    .line 134
    :cond_2
    instance-of v0, p2, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoading;

    if-eqz v0, :cond_3

    .line 135
    iget-object v0, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/checkoutflow/installments/impl/R$dimen;->installments_qr_code_size:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v0

    .line 137
    iget-object v1, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->installmentsService:Lcom/squareup/checkoutflow/installments/InstallmentsService;

    .line 138
    new-instance v2, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;

    invoke-virtual {p2}, Lcom/squareup/checkoutflow/installments/InstallmentsState;->getToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/installments/InstallmentsInput;->getCart()Lcom/squareup/protos/client/bills/Cart;

    move-result-object p1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v2, v3, p1, v4, v0}, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 137
    invoke-interface {v1, v2}, Lcom/squareup/checkoutflow/installments/InstallmentsService;->generateQr(Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 140
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 141
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "installmentsService.gene\u2026          .toObservable()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v0, 0x320

    .line 142
    sget-object v2, Lcom/squareup/register/widgets/GlassSpinner;->MIN_SPINNER_TIME_UNIT:Ljava/util/concurrent/TimeUnit;

    const-string v3, "MIN_SPINNER_TIME_UNIT"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-static {p1, v0, v1, v2, v3}, Lcom/squareup/util/rx2/Rx2TransformersKt;->delayEmission(Lio/reactivex/Observable;JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    .line 143
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "installmentsService.gene\u2026          .firstOrError()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 240
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$$inlined$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$$inlined$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 241
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 242
    const-class v0, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    sget-object v1, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v2, Lcom/squareup/protos/capital/consumer/service/GenerateInstallmentsQRCodeResponse;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v0

    new-instance v1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v1, v0, p1}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/Worker;

    const/4 v4, 0x0

    .line 145
    new-instance p1, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$7;

    invoke-direct {p1, p0, p2}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$7;-><init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;Lcom/squareup/checkoutflow/installments/InstallmentsState;)V

    move-object v5, p1

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object v2, p3

    .line 136
    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 182
    new-instance p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;

    .line 183
    sget-object p2, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState$QrLoading;->INSTANCE:Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState$QrLoading;

    check-cast p2, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;

    .line 182
    invoke-direct {p1, p2}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;

    goto/16 :goto_1

    .line 186
    :cond_3
    instance-of v0, p2, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoaded;

    if-eqz v0, :cond_4

    .line 187
    new-instance p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;

    .line 188
    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState$QrLoaded;

    .line 189
    move-object v1, p2

    check-cast v1, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoaded;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeLoaded;->getQrCode()Lokio/ByteString;

    move-result-object v1

    .line 190
    sget-object v2, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$8;->INSTANCE:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$8;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 193
    new-instance v3, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$9;

    invoke-direct {v3, p0, p2}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$9;-><init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;Lcom/squareup/checkoutflow/installments/InstallmentsState;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 188
    invoke-direct {v0, v1, v2, p2}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState$QrLoaded;-><init>(Lokio/ByteString;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;

    .line 187
    invoke-direct {p1, v0}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;

    goto :goto_1

    .line 201
    :cond_4
    instance-of v0, p2, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingQRCodeError;

    if-eqz v0, :cond_5

    .line 202
    new-instance p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;

    .line 203
    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState$QrError;

    .line 204
    sget-object v1, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$10;->INSTANCE:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$10;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v1

    .line 207
    new-instance v2, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$11;

    invoke-direct {v2, p0, p2}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$11;-><init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;Lcom/squareup/checkoutflow/installments/InstallmentsState;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 203
    invoke-direct {v0, v1, p2}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState$QrError;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;

    .line 202
    invoke-direct {p1, v0}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsQRCodeScreenData$QrState;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;

    goto :goto_1

    .line 215
    :cond_5
    instance-of v0, p2, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingSmsInput;

    if-eqz v0, :cond_6

    .line 216
    new-instance v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;

    .line 217
    new-instance v1, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$12;

    invoke-direct {v1, p2}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$12;-><init>(Lcom/squareup/checkoutflow/installments/InstallmentsState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v1

    .line 220
    new-instance v2, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$13;

    invoke-direct {v2, p0, p2, p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$13;-><init>(Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;Lcom/squareup/checkoutflow/installments/InstallmentsState;Lcom/squareup/checkoutflow/installments/InstallmentsInput;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p1

    .line 216
    invoke-direct {v0, v1, p1}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsInputScreenData;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;

    goto :goto_1

    .line 229
    :cond_6
    instance-of p1, p2, Lcom/squareup/checkoutflow/installments/InstallmentsState$ShowingSmsSent;

    if-eqz p1, :cond_7

    .line 231
    new-instance p1, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsSentScreenData;

    .line 232
    sget-object p2, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$14;->INSTANCE:Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow$render$14;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-interface {p3, p2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p2

    .line 231
    invoke-direct {p1, p2}, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData$InstallmentsSmsSentScreenData;-><init>(Lkotlin/jvm/functions/Function1;)V

    move-object v0, p1

    check-cast v0, Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;

    :goto_1
    return-object v0

    :cond_7
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsInput;

    check-cast p2, Lcom/squareup/checkoutflow/installments/InstallmentsState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->render(Lcom/squareup/checkoutflow/installments/InstallmentsInput;Lcom/squareup/checkoutflow/installments/InstallmentsState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/checkoutflow/installments/InstallmentsScreenData;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/checkoutflow/installments/InstallmentsState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p1}, Lcom/squareup/checkoutflow/installments/InstallmentsState;->toSnapshot()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 57
    check-cast p1, Lcom/squareup/checkoutflow/installments/InstallmentsState;

    invoke-virtual {p0, p1}, Lcom/squareup/checkoutflow/installments/RealInstallmentsWorkflow;->snapshotState(Lcom/squareup/checkoutflow/installments/InstallmentsState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
