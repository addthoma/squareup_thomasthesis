.class Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter$GiftCardTenderEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "PayGiftCardPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GiftCardTenderEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;)V
    .locals 1

    .line 214
    iput-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter$GiftCardTenderEvent;->this$0:Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    .line 215
    sget-object p1, Lcom/squareup/eventstream/v1/EventStream$Name;->TAP:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->GIFT_CARD_TENDER:Lcom/squareup/analytics/RegisterTapName;

    iget-object v0, v0, Lcom/squareup/analytics/RegisterTapName;->value:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    return-void
.end method
