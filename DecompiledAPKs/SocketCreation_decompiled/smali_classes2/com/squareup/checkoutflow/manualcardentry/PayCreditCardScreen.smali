.class public final Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;
.super Lcom/squareup/ui/tender/InTenderScope;
.source "PayCreditCardScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/HasSoftInputModeForPhone;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final SHOWN:Ljava/lang/String; = "Shown PayCardCreditScreen"


# instance fields
.field public final screenData:Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayCreditCardScreen$1qJnO8XQZy41I4_exhC_tSAYnKw;->INSTANCE:Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayCreditCardScreen$1qJnO8XQZy41I4_exhC_tSAYnKw;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/InTenderScope;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    .line 32
    iput-object p2, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;->screenData:Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;
    .locals 4

    .line 57
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 58
    invoke-static {p0}, Lcom/squareup/cnp/LinkSpanData;->deserialize(Landroid/os/Parcel;)Lcom/squareup/cnp/LinkSpanData;

    move-result-object v1

    .line 59
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 60
    :goto_0
    new-instance v3, Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    invoke-direct {v3, v0, v1, v2}, Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;-><init>(Ljava/lang/String;Lcom/squareup/cnp/LinkSpanData;Z)V

    .line 62
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 63
    check-cast p0, Lcom/squareup/ui/tender/TenderScopeTreeKey;

    .line 64
    new-instance v0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;

    invoke-direct {v0, p0, v3}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;->screenData:Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;->screenData:Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;->getLinkSpanData()Lcom/squareup/cnp/LinkSpanData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/cnp/LinkSpanData;->serialize(Landroid/os/Parcel;)V

    .line 52
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;->screenData:Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;->getIgnoreTransactionLimits()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYMENT_FLOW_CARD:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSoftInputMode()Lcom/squareup/workflow/SoftInputMode;
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/workflow/SoftInputMode;->ALWAYS_SHOW_RESIZE:Lcom/squareup/workflow/SoftInputMode;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 69
    sget v0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$layout;->pay_credit_card_screen_view:I

    return v0
.end method
