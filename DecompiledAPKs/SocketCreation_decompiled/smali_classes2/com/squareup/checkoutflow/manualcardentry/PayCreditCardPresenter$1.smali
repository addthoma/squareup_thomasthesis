.class Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter$1;
.super Ljava/lang/Object;
.source "PayCreditCardPresenter.java"

# interfaces
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;)V
    .locals 0

    .line 80
    iput-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter$1;->this$0:Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 88
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter$1;->this$0:Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;

    invoke-static {p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->access$000(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;)Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    move-result-object p1

    invoke-interface {p1, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->removeEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)Z

    .line 89
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter$1;->this$0:Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;

    invoke-static {p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;->access$100(Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;)Lcom/squareup/ui/main/errors/PaymentInputHandler;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter$1;->this$0:Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardPresenter;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->initializeWithNfcFieldOn(Lcom/squareup/ui/NfcProcessor$NfcStatusDisplay;)V

    return-void
.end method
