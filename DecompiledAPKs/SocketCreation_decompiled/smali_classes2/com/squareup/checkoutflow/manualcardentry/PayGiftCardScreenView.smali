.class public Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;
.super Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;
.source "PayGiftCardScreenView.java"


# instance fields
.field countryCode:Lcom/squareup/CountryCode;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private editor:Lcom/squareup/register/widgets/card/CardEditor;

.field private giftCardOnFileView:Lcom/squareup/ui/account/view/LineRow;

.field private giftCardOnFileViewContainer:Landroid/view/View;

.field presenter:Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private swipedCardContainter:Landroid/view/View;

.field private swipedCardView:Lcom/squareup/ui/TokenView;

.field thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/card/ThirdPartyGiftCardPanValidationStrategy;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 35
    invoke-direct {p0, p1, p2}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const-class p2, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen$Component;->inject(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 127
    sget v0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$id;->card_editor:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/card/CardEditor;

    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    .line 128
    sget v0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$id;->swiped_card_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->swipedCardContainter:Landroid/view/View;

    .line 129
    sget v0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$id;->swiped_card:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/TokenView;

    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->swipedCardView:Lcom/squareup/ui/TokenView;

    .line 130
    sget v0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$id;->gift_card_on_file:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/LineRow;

    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->giftCardOnFileView:Lcom/squareup/ui/account/view/LineRow;

    .line 131
    sget v0, Lcom/squareup/checkoutflow/manualcardentry/impl/R$id;->gift_card_on_file_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->giftCardOnFileViewContainer:Landroid/view/View;

    return-void
.end method


# virtual methods
.method public clearCard()V
    .locals 1

    .line 110
    invoke-super {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->clearCard()V

    .line 111
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->showCardEditor()V

    const/4 v0, 0x0

    .line 112
    invoke-virtual {p0, v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->updateChargeButton(Z)V

    return-void
.end method

.method getPresenter()Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/checkoutflow/manualcardentry/PayCardPresenter<",
            "*>;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    return-object v0
.end method

.method hideCardOnFileRow()V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->giftCardOnFileViewContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public hidePanWarning()V
    .locals 0

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$0$PayGiftCardScreenView()V
    .locals 1

    .line 54
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->showCardEditor()V

    const/4 v0, 0x0

    .line 55
    invoke-virtual {p0, v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->updateChargeButton(Z)V

    .line 56
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->clearStoredCardInCart()V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$1$PayGiftCardScreenView(Landroid/view/View;)V
    .locals 0

    .line 58
    iget-object p1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    invoke-virtual {p1}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->giftCardOnFileSelected()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .line 44
    invoke-super {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->onAttachedToWindow()V

    .line 45
    invoke-direct {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->bindViews()V

    .line 47
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->isThirdPartyGiftCardFeatureEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    iget-object v2, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->countryCode:Lcom/squareup/CountryCode;

    iget-object v3, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->thirdPartyGiftCardStrategyProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/register/widgets/card/PanValidationStrategy;

    invoke-virtual {v0, v2, v1, v3}, Lcom/squareup/register/widgets/card/CardEditor;->init(Lcom/squareup/CountryCode;ZLcom/squareup/register/widgets/card/PanValidationStrategy;)V

    goto :goto_0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    iget-object v2, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->countryCode:Lcom/squareup/CountryCode;

    new-instance v3, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;

    invoke-direct {v3}, Lcom/squareup/register/widgets/card/CardPanValidationStrategy;-><init>()V

    invoke-virtual {v0, v2, v1, v3}, Lcom/squareup/register/widgets/card/CardEditor;->init(Lcom/squareup/CountryCode;ZLcom/squareup/register/widgets/card/PanValidationStrategy;)V

    .line 53
    :goto_0
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->swipedCardView:Lcom/squareup/ui/TokenView;

    new-instance v1, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayGiftCardScreenView$E93UEG0FekJFxcpVW0zhKZQOyIg;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayGiftCardScreenView$E93UEG0FekJFxcpVW0zhKZQOyIg;-><init>(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/TokenView;->setListener(Lcom/squareup/ui/TokenView$Listener;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->giftCardOnFileView:Lcom/squareup/ui/account/view/LineRow;

    new-instance v1, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayGiftCardScreenView$wZB97gviWLHkzxT_f9bRukGX1IU;

    invoke-direct {v1, p0}, Lcom/squareup/checkoutflow/manualcardentry/-$$Lambda$PayGiftCardScreenView$wZB97gviWLHkzxT_f9bRukGX1IU;-><init>(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/account/view/LineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->takeView(Ljava/lang/Object;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    sget-object v1, Lcom/squareup/Card$Brand;->SQUARE_GIFT_CARD_V2:Lcom/squareup/Card$Brand;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/squareup/util/ProtoGlyphs;->card(Lcom/squareup/Card$Brand;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setCardGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    invoke-virtual {v0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->isShowPlasticGiftCardsFeatureEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    sget v1, Lcom/squareup/checkoutflow/manualcardentry/impl/R$string;->pay_gift_card_hint:I

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setCardInputHint(I)V

    goto :goto_1

    .line 65
    :cond_1
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    sget v1, Lcom/squareup/checkoutflow/manualcardentry/impl/R$string;->pay_gift_card_hint_no_swipe:I

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setCardInputHint(I)V

    :goto_1
    return-void
.end method

.method public onBackPressed()Z
    .locals 2

    .line 96
    invoke-super {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->onBackPressed()Z

    move-result v0

    .line 97
    iget-object v1, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    invoke-virtual {v1}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->clearStoredCardInCart()V

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->dropView(Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;)V

    .line 71
    invoke-super {p0}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->onDetachedFromWindow()V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->onWindowFocusChanged(Z)V

    .line 92
    invoke-super {p0, p1}, Lcom/squareup/checkoutflow/manualcardentry/AbstractPayCardScreenView;->onWindowFocusChanged(Z)V

    return-void
.end method

.method public setEditorDisabled(Ljava/lang/CharSequence;)V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setEnabled(Z)V

    .line 103
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/card/CardEditor;->setCardInputHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showCardEditor()V
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->swipedCardContainter:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setVisibility(I)V

    return-void
.end method

.method public showPanWarning(I)V
    .locals 0

    return-void
.end method

.method showSwipedCard(Ljava/lang/CharSequence;)V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->swipedCardContainter:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->editor:Lcom/squareup/register/widgets/card/CardEditor;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/card/CardEditor;->setVisibility(I)V

    .line 82
    invoke-virtual {p0}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->hideSoftKeyboard()V

    .line 83
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->swipedCardView:Lcom/squareup/ui/TokenView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/TokenView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method updateChargeButton(Z)V
    .locals 1

    .line 123
    iget-object v0, p0, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreenView;->presenter:Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardPresenter;->setChargeButtonEnabled(Z)V

    return-void
.end method
