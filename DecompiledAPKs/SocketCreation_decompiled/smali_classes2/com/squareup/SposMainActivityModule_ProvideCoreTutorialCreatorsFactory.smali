.class public final Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;
.super Ljava/lang/Object;
.source "SposMainActivityModule_ProvideCoreTutorialCreatorsFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/List<",
        "Lcom/squareup/tutorialv2/TutorialCreator;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final accountFreezeTutorialCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final createItemTutorialCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
            ">;"
        }
    .end annotation
.end field

.field private final disputesTutorialCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesTutorialCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final firstTutorialCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;",
            ">;"
        }
    .end annotation
.end field

.field private final messagingTutorialCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;",
            ">;"
        }
    .end annotation
.end field

.field private final placeholderCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;",
            ">;"
        }
    .end annotation
.end field

.field private final quickAmountsTutorialCreatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesTutorialCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->accountFreezeTutorialCreatorProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p2, p0, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->placeholderCreatorProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->firstTutorialCreatorProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->disputesTutorialCreatorProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->createItemTutorialCreatorProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->messagingTutorialCreatorProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->quickAmountsTutorialCreatorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/disputes/DisputesTutorialCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            ">;)",
            "Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;"
        }
    .end annotation

    .line 66
    new-instance v8, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static provideCoreTutorialCreators(Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;Lcom/squareup/disputes/DisputesTutorialCreator;Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;",
            "Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;",
            "Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;",
            "Lcom/squareup/disputes/DisputesTutorialCreator;",
            "Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;",
            "Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;",
            "Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/tutorialv2/TutorialCreator;",
            ">;"
        }
    .end annotation

    .line 77
    invoke-static/range {p0 .. p6}, Lcom/squareup/SposMainActivityModule;->provideCoreTutorialCreators(Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;Lcom/squareup/disputes/DisputesTutorialCreator;Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;)Ljava/util/List;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->get()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/tutorialv2/TutorialCreator;",
            ">;"
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->accountFreezeTutorialCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;

    iget-object v0, p0, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->placeholderCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;

    iget-object v0, p0, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->firstTutorialCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;

    iget-object v0, p0, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->disputesTutorialCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/disputes/DisputesTutorialCreator;

    iget-object v0, p0, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->createItemTutorialCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;

    iget-object v0, p0, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->messagingTutorialCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;

    iget-object v0, p0, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->quickAmountsTutorialCreatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;

    invoke-static/range {v1 .. v7}, Lcom/squareup/SposMainActivityModule_ProvideCoreTutorialCreatorsFactory;->provideCoreTutorialCreators(Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;Lcom/squareup/register/tutorial/PlaceholderTutorial$Creator;Lcom/squareup/register/tutorial/FirstCardPaymentTutorialV2$Creator;Lcom/squareup/disputes/DisputesTutorialCreator;Lcom/squareup/items/tutorial/CreateItemTutorial$Creator;Lcom/squareup/ui/help/messages/tutorials/MessagingTutorial$Creator;Lcom/squareup/quickamounts/ui/QuickAmountsTutorialCreator;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
