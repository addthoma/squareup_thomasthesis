.class public abstract Lcom/squareup/CommonAppModule$Real;
.super Ljava/lang/Object;
.source "CommonAppModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/CommonAppModule;,
        Lcom/squareup/systempermissions/RealSystemPermissionsCheckerModule;,
        Lcom/squareup/android/activity/ToastFactoryModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/CommonAppModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Real"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAutoCaptureControlTimer()Lcom/squareup/autocapture/AutoCaptureControlTimer;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 187
    sget-object v0, Lcom/squareup/autocapture/AutoCaptureControlTimer;->REAL:Lcom/squareup/autocapture/AutoCaptureControlTimer;

    return-object v0
.end method

.method static provideForegroundServiceStarter(Landroid/app/Application;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/util/Res;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/foregroundservice/ForegroundServiceStarter;
    .locals 8
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 194
    new-instance v7, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;

    const-class v6, Lcom/squareup/queue/QueueService;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/squareup/foregroundservice/RealForegroundServiceStarter;-><init>(Landroid/app/Application;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/util/Res;Lcom/squareup/notification/NotificationWrapper;Lcom/squareup/thread/executor/MainThread;Ljava/lang/Class;)V

    return-object v7
.end method
