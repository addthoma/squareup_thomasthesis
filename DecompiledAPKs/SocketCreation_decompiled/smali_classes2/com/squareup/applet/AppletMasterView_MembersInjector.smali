.class public final Lcom/squareup/applet/AppletMasterView_MembersInjector;
.super Ljava/lang/Object;
.source "AppletMasterView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/applet/AppletMasterView;",
        ">;"
    }
.end annotation


# instance fields
.field private final badgePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletMasterViewPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletMasterViewPresenter;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/applet/AppletMasterView_MembersInjector;->badgePresenterProvider:Ljavax/inject/Provider;

    .line 20
    iput-object p2, p0, Lcom/squareup/applet/AppletMasterView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletMasterViewPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/applet/AppletMasterView;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/applet/AppletMasterView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/applet/AppletMasterView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectBadgePresenter(Lcom/squareup/applet/AppletMasterView;Lcom/squareup/applet/BadgePresenter;)V
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/squareup/applet/AppletMasterView;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/applet/AppletMasterView;Lcom/squareup/applet/AppletMasterViewPresenter;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/applet/AppletMasterView;->presenter:Lcom/squareup/applet/AppletMasterViewPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/applet/AppletMasterView;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/applet/AppletMasterView_MembersInjector;->badgePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/BadgePresenter;

    invoke-static {p1, v0}, Lcom/squareup/applet/AppletMasterView_MembersInjector;->injectBadgePresenter(Lcom/squareup/applet/AppletMasterView;Lcom/squareup/applet/BadgePresenter;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/applet/AppletMasterView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletMasterViewPresenter;

    invoke-static {p1, v0}, Lcom/squareup/applet/AppletMasterView_MembersInjector;->injectPresenter(Lcom/squareup/applet/AppletMasterView;Lcom/squareup/applet/AppletMasterViewPresenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/applet/AppletMasterView;

    invoke-virtual {p0, p1}, Lcom/squareup/applet/AppletMasterView_MembersInjector;->injectMembers(Lcom/squareup/applet/AppletMasterView;)V

    return-void
.end method
