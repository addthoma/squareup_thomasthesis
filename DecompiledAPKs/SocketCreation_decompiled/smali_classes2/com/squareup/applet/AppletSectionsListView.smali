.class public Lcom/squareup/applet/AppletSectionsListView;
.super Landroid/widget/ListView;
.source "AppletSectionsListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;,
        Lcom/squareup/applet/AppletSectionsListView$Component;
    }
.end annotation


# instance fields
.field private adapter:Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;

.field private colorStateList:Landroid/content/res/ColorStateList;

.field presenter:Lcom/squareup/applet/AppletSectionsListPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    const-class v0, Lcom/squareup/applet/AppletSectionsListView$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletSectionsListView$Component;

    invoke-interface {v0, p0}, Lcom/squareup/applet/AppletSectionsListView$Component;->inject(Lcom/squareup/applet/AppletSectionsListView;)V

    .line 41
    sget-object v0, Lcom/squareup/widgets/pos/R$styleable;->AppletSectionsListView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 43
    sget p2, Lcom/squareup/widgets/pos/R$styleable;->AppletSectionsListView_sq_valueColor:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/applet/AppletSectionsListView;->colorStateList:Landroid/content/res/ColorStateList;

    .line 45
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/applet/AppletSectionsListView;)Landroid/content/res/ColorStateList;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/applet/AppletSectionsListView;->colorStateList:Landroid/content/res/ColorStateList;

    return-object p0
.end method


# virtual methods
.method protected appletListRow()I
    .locals 1

    .line 80
    sget v0, Lcom/squareup/pos/container/R$layout;->applet_list_row:I

    return v0
.end method

.method protected appletSidebarRow()I
    .locals 1

    .line 76
    sget v0, Lcom/squareup/pos/container/R$layout;->applet_sidebar_row:I

    return v0
.end method

.method public createAdapter()V
    .locals 2

    .line 67
    new-instance v0, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;

    iget-object v1, p0, Lcom/squareup/applet/AppletSectionsListView;->presenter:Lcom/squareup/applet/AppletSectionsListPresenter;

    invoke-virtual {v1}, Lcom/squareup/applet/AppletSectionsListPresenter;->useCheckableRows()Z

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;-><init>(Lcom/squareup/applet/AppletSectionsListView;Z)V

    iput-object v0, p0, Lcom/squareup/applet/AppletSectionsListView;->adapter:Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;

    .line 68
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListView;->adapter:Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;

    invoke-virtual {p0, v0}, Lcom/squareup/applet/AppletSectionsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$0$AppletSectionsListView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 51
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 52
    iget-object p1, p0, Lcom/squareup/applet/AppletSectionsListView;->presenter:Lcom/squareup/applet/AppletSectionsListPresenter;

    invoke-virtual {p1, p3}, Lcom/squareup/applet/AppletSectionsListPresenter;->onSectionClicked(I)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 49
    invoke-super {p0}, Landroid/widget/ListView;->onAttachedToWindow()V

    .line 50
    new-instance v0, Lcom/squareup/applet/-$$Lambda$AppletSectionsListView$v0Y8zoTG4NrA8BLh09OKCQWUkic;

    invoke-direct {v0, p0}, Lcom/squareup/applet/-$$Lambda$AppletSectionsListView$v0Y8zoTG4NrA8BLh09OKCQWUkic;-><init>(Lcom/squareup/applet/AppletSectionsListView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/applet/AppletSectionsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListView;->presenter:Lcom/squareup/applet/AppletSectionsListPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/applet/AppletSectionsListPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListView;->presenter:Lcom/squareup/applet/AppletSectionsListPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/applet/AppletSectionsListPresenter;->dropView(Ljava/lang/Object;)V

    .line 59
    invoke-super {p0}, Landroid/widget/ListView;->onDetachedFromWindow()V

    return-void
.end method

.method public refreshAdapter()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListView;->adapter:Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSectionsListView$SectionsAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public scrollToSection(I)V
    .locals 0

    .line 72
    invoke-virtual {p0, p1}, Lcom/squareup/applet/AppletSectionsListView;->setSelection(I)V

    return-void
.end method
