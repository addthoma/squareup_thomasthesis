.class public abstract Lcom/squareup/applet/Applet$Badge;
.super Ljava/lang/Object;
.source "Applet.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/applet/Applet;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Badge"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/applet/Applet$Badge$Hidden;,
        Lcom/squareup/applet/Applet$Badge$Visible;,
        Lcom/squareup/applet/Applet$Badge$Priority;,
        Lcom/squareup/applet/Applet$Badge$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u00032\u00020\u0001:\u0004\u0003\u0004\u0005\u0006B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0002\u0007\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/applet/Applet$Badge;",
        "",
        "()V",
        "Companion",
        "Hidden",
        "Priority",
        "Visible",
        "Lcom/squareup/applet/Applet$Badge$Hidden;",
        "Lcom/squareup/applet/Applet$Badge$Visible;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/applet/Applet$Badge$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/applet/Applet$Badge$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/applet/Applet$Badge$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/applet/Applet$Badge;->Companion:Lcom/squareup/applet/Applet$Badge$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 54
    invoke-direct {p0}, Lcom/squareup/applet/Applet$Badge;-><init>()V

    return-void
.end method

.method public static final toBadge(I)Lcom/squareup/applet/Applet$Badge;
    .locals 3
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/applet/Applet$Badge;->Companion:Lcom/squareup/applet/Applet$Badge$Companion;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p0, v1, v2, v1}, Lcom/squareup/applet/Applet$Badge$Companion;->toBadge$default(Lcom/squareup/applet/Applet$Badge$Companion;ILcom/squareup/applet/Applet$Badge$Priority;ILjava/lang/Object;)Lcom/squareup/applet/Applet$Badge;

    move-result-object p0

    return-object p0
.end method

.method public static final toBadge(ILcom/squareup/applet/Applet$Badge$Priority;)Lcom/squareup/applet/Applet$Badge;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/applet/Applet$Badge;->Companion:Lcom/squareup/applet/Applet$Badge$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/applet/Applet$Badge$Companion;->toBadge(ILcom/squareup/applet/Applet$Badge$Priority;)Lcom/squareup/applet/Applet$Badge;

    move-result-object p0

    return-object p0
.end method
