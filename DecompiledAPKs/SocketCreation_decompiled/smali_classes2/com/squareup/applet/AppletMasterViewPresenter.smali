.class public Lcom/squareup/applet/AppletMasterViewPresenter;
.super Lmortar/ViewPresenter;
.source "AppletMasterViewPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/applet/AppletMasterView;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

.field private final appletSelection:Lcom/squareup/applet/AppletSelection;

.field private final upButtonText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/applet/ActionBarNavigationHelper;Ljava/lang/String;Lcom/squareup/applet/AppletSelection;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/applet/AppletMasterViewPresenter;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    .line 19
    iput-object p2, p0, Lcom/squareup/applet/AppletMasterViewPresenter;->upButtonText:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/squareup/applet/AppletMasterViewPresenter;->appletSelection:Lcom/squareup/applet/AppletSelection;

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$0$AppletMasterViewPresenter(Lcom/squareup/applet/Applet;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/applet/AppletMasterViewPresenter;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    .line 27
    invoke-virtual {p0}, Lcom/squareup/applet/AppletMasterViewPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/applet/AppletMasterView;

    invoke-virtual {v1}, Lcom/squareup/applet/AppletMasterView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/applet/AppletMasterViewPresenter;->upButtonText:Ljava/lang/String;

    .line 26
    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/applet/ActionBarNavigationHelper;->makeActionBarForAppletActivationScreen(Lcom/squareup/marin/widgets/MarinActionBar;Ljava/lang/String;Lcom/squareup/applet/Applet;)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$AppletMasterViewPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/applet/AppletMasterViewPresenter;->appletSelection:Lcom/squareup/applet/AppletSelection;

    invoke-interface {v0}, Lcom/squareup/applet/AppletSelection;->selectedApplet()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/applet/-$$Lambda$AppletMasterViewPresenter$xOWzQ8OGmvVX0SGCmUXiO3saT5s;

    invoke-direct {v1, p0}, Lcom/squareup/applet/-$$Lambda$AppletMasterViewPresenter$xOWzQ8OGmvVX0SGCmUXiO3saT5s;-><init>(Lcom/squareup/applet/AppletMasterViewPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/applet/AppletMasterViewPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/applet/-$$Lambda$AppletMasterViewPresenter$1uC4L8GkTFcvz9iozMVyiGFAREc;

    invoke-direct {v0, p0}, Lcom/squareup/applet/-$$Lambda$AppletMasterViewPresenter$1uC4L8GkTFcvz9iozMVyiGFAREc;-><init>(Lcom/squareup/applet/AppletMasterViewPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
