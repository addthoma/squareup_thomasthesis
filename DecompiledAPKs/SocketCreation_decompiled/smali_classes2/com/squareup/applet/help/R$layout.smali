.class public final Lcom/squareup/applet/help/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/applet/help/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final about:I = 0x7f0d001c

.field public static final announcement_details:I = 0x7f0d0055

.field public static final announcements:I = 0x7f0d0056

.field public static final announcements_row:I = 0x7f0d0057

.field public static final help_applet_content_row:I = 0x7f0d0288

.field public static final help_applet_master_view:I = 0x7f0d0289

.field public static final help_content_row:I = 0x7f0d028a

.field public static final help_section:I = 0x7f0d028c

.field public static final jedi_help_section:I = 0x7f0d030d

.field public static final jedi_panel_recycler_view:I = 0x7f0d0311

.field public static final legal:I = 0x7f0d0325

.field public static final libraries:I = 0x7f0d0326

.field public static final libraries_footer:I = 0x7f0d0327

.field public static final license_group:I = 0x7f0d032c

.field public static final license_group_divider:I = 0x7f0d032d

.field public static final license_group_header:I = 0x7f0d032e

.field public static final licenses:I = 0x7f0d032f

.field public static final messages_view:I = 0x7f0d035a

.field public static final ntep:I = 0x7f0d03b1

.field public static final order_hardware:I = 0x7f0d03e5

.field public static final order_history_entry:I = 0x7f0d03e6

.field public static final order_history_more:I = 0x7f0d03e7

.field public static final order_history_view:I = 0x7f0d03e8

.field public static final order_reader_item_view:I = 0x7f0d03e9

.field public static final orders_section:I = 0x7f0d0418

.field public static final troubleshooting_section:I = 0x7f0d0572

.field public static final tutorials:I = 0x7f0d057a


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
