.class public final Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;
.super Ljava/lang/Object;
.source "AppletsDrawerActionBarNavigationHelper.kt"

# interfaces
.implements Lcom/squareup/applet/ActionBarNavigationHelper;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J \u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\u00020\u0006X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;",
        "Lcom/squareup/applet/ActionBarNavigationHelper;",
        "drawerRunner",
        "Lcom/squareup/applet/AppletsDrawerRunner;",
        "(Lcom/squareup/applet/AppletsDrawerRunner;)V",
        "hasDrawerButton",
        "",
        "getHasDrawerButton",
        "()Z",
        "makeActionBarForAppletActivationScreen",
        "",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "name",
        "",
        "applet",
        "Lcom/squareup/applet/Applet;",
        "pos-container_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final drawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

.field private final hasDrawerButton:Z


# direct methods
.method public constructor <init>(Lcom/squareup/applet/AppletsDrawerRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "drawerRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;->drawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    const/4 p1, 0x1

    .line 11
    iput-boolean p1, p0, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;->hasDrawerButton:Z

    return-void
.end method

.method public static final synthetic access$getDrawerRunner$p(Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;)Lcom/squareup/applet/AppletsDrawerRunner;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;->drawerRunner:Lcom/squareup/applet/AppletsDrawerRunner;

    return-object p0
.end method


# virtual methods
.method public getHasDrawerButton()Z
    .locals 1

    .line 11
    iget-boolean v0, p0, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;->hasDrawerButton:Z

    return v0
.end method

.method public makeActionBarForAppletActivationScreen(Lcom/squareup/marin/widgets/MarinActionBar;Ljava/lang/String;Lcom/squareup/applet/Applet;)V
    .locals 1

    const-string v0, "actionBar"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "applet"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p3

    .line 24
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BURGER:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p3, v0, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 25
    new-instance v0, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper$makeActionBarForAppletActivationScreen$1;

    invoke-direct {v0, p0}, Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper$makeActionBarForAppletActivationScreen$1;-><init>(Lcom/squareup/applet/AppletsDrawerActionBarNavigationHelper;)V

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 26
    invoke-virtual {p3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
