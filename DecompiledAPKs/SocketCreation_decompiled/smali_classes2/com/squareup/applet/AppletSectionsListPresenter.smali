.class public Lcom/squareup/applet/AppletSectionsListPresenter;
.super Lmortar/ViewPresenter;
.source "AppletSectionsListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/applet/AppletSectionsListView;",
        ">;"
    }
.end annotation


# instance fields
.field private final appletSectionsList:Lcom/squareup/applet/AppletSectionsList;

.field private final device:Lcom/squareup/util/Device;

.field private final flow:Lflow/Flow;

.field private final visibleEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/applet/AppletSectionsListEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/applet/AppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;)V
    .locals 1

    .line 22
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->visibleEntries:Ljava/util/List;

    .line 23
    iput-object p1, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->appletSectionsList:Lcom/squareup/applet/AppletSectionsList;

    .line 24
    iput-object p2, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->flow:Lflow/Flow;

    .line 25
    iput-object p3, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->device:Lcom/squareup/util/Device;

    .line 26
    iget-object p2, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->visibleEntries:Ljava/util/List;

    invoke-virtual {p1}, Lcom/squareup/applet/AppletSectionsList;->getVisibleEntries()Ljava/util/List;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method private snapToSelectedSection()V
    .locals 3

    .line 91
    invoke-virtual {p0}, Lcom/squareup/applet/AppletSectionsListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletSectionsListView;

    iget-object v1, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->visibleEntries:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->appletSectionsList:Lcom/squareup/applet/AppletSectionsList;

    invoke-virtual {v2}, Lcom/squareup/applet/AppletSectionsList;->getLastSelectedSection()Lcom/squareup/applet/AppletSection;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/applet/AppletSectionsListView;->scrollToSection(I)V

    return-void
.end method


# virtual methods
.method public getEntry(I)Lcom/squareup/applet/AppletSectionsListEntry;
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->visibleEntries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/applet/AppletSectionsListEntry;

    return-object p1
.end method

.method public getSectionCount()I
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->visibleEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSectionName(I)Ljava/lang/String;
    .locals 0

    .line 79
    invoke-virtual {p0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->getEntry(I)Lcom/squareup/applet/AppletSectionsListEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/AppletSectionsListEntry;->getTitleText()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getSectionValue(I)Lio/reactivex/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 83
    invoke-virtual {p0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->getEntry(I)Lcom/squareup/applet/AppletSectionsListEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/AppletSectionsListEntry;->getValueTextObservable()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public isSectionSelected(I)Z
    .locals 1

    .line 75
    invoke-virtual {p0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->getEntry(I)Lcom/squareup/applet/AppletSectionsListEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/AppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->appletSectionsList:Lcom/squareup/applet/AppletSectionsList;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSectionsList;->getLastSelectedSection()Lcom/squareup/applet/AppletSection;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 30
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 31
    invoke-virtual {p0}, Lcom/squareup/applet/AppletSectionsListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletSectionsListView;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSectionsListView;->createAdapter()V

    .line 32
    invoke-virtual {p0}, Lcom/squareup/applet/AppletSectionsListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletSectionsListView;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSectionsListView;->refreshAdapter()V

    if-nez p1, :cond_0

    .line 34
    invoke-direct {p0}, Lcom/squareup/applet/AppletSectionsListPresenter;->snapToSelectedSection()V

    :cond_0
    return-void
.end method

.method public onSectionClicked(I)V
    .locals 1

    .line 39
    invoke-virtual {p0}, Lcom/squareup/applet/AppletSectionsListPresenter;->useCheckableRows()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/squareup/applet/AppletSectionsListPresenter;->isSectionSelected(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->visibleEntries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/applet/AppletSectionsListEntry;

    invoke-virtual {p1}, Lcom/squareup/applet/AppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object p1

    .line 48
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->appletSectionsList:Lcom/squareup/applet/AppletSectionsList;

    invoke-virtual {v0, p1}, Lcom/squareup/applet/AppletSectionsList;->setSelectedSection(Lcom/squareup/applet/AppletSection;)V

    .line 49
    invoke-virtual {p1}, Lcom/squareup/applet/AppletSection;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    .line 50
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0, p1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 51
    iget-object p1, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-nez p1, :cond_1

    .line 52
    invoke-virtual {p0}, Lcom/squareup/applet/AppletSectionsListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/applet/AppletSectionsListView;

    invoke-virtual {p1}, Lcom/squareup/applet/AppletSectionsListView;->refreshAdapter()V

    :cond_1
    return-void
.end method

.method public refresh()V
    .locals 1

    .line 87
    invoke-virtual {p0}, Lcom/squareup/applet/AppletSectionsListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/applet/AppletSectionsListView;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSectionsListView;->refreshAdapter()V

    return-void
.end method

.method public rowDisplayed(I)V
    .locals 0

    return-void
.end method

.method public useCheckableRows()Z
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/applet/AppletSectionsListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
