.class public abstract Lcom/squareup/badbus/BadBusModule;
.super Ljava/lang/Object;
.source "BadBusModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideBus()Lcom/squareup/badbus/BadBus;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 18
    new-instance v0, Lcom/squareup/badbus/BadBus;

    invoke-direct {v0}, Lcom/squareup/badbus/BadBus;-><init>()V

    return-object v0
.end method


# virtual methods
.method abstract bindEventSink(Lcom/squareup/badbus/BadBus;)Lcom/squareup/badbus/BadEventSink;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
