.class public interface abstract Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;
.super Ljava/lang/Object;
.source "AppointmentLinkingHandler.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;",
        "",
        "handle",
        "",
        "target",
        "Lcom/squareup/appointmentsapi/AppointmentLinkingTarget;",
        "appointments_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract handle(Lcom/squareup/appointmentsapi/AppointmentLinkingTarget;)V
.end method
