.class public abstract Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule;
.super Ljava/lang/Object;
.source "AccountFreezeLoggedInModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\'\u0018\u0000 \u000c2\u00020\u0001:\u0001\u000cB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH!\u00a2\u0006\u0002\u0008\u000b\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule;",
        "",
        "()V",
        "bindAccountFreezeIntoLoginScope",
        "Lmortar/Scoped;",
        "accountFreezeLogoutListener",
        "Lcom/squareup/accountfreeze/AccountFreezeLogoutListener;",
        "bindAccountFreezeIntoLoginScope$impl_wiring_release",
        "bindAccountFreezeJobCreator",
        "accountFreezeNotificationScheduler",
        "Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;",
        "bindAccountFreezeJobCreator$impl_wiring_release",
        "Companion",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule;->Companion:Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideLastDismissedFreezeBanner(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p0    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/accountfreeze/LastDismissedFreezeBanner;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule;->Companion:Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule$Companion;->provideLastDismissedFreezeBanner(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method

.method public static final provideLastDismissedFreezeNotification(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 1
    .param p0    # Lcom/f2prateek/rx/preferences2/RxSharedPreferences;
        .annotation runtime Lcom/squareup/settings/LoggedInSettings;
        .end annotation
    .end param
    .annotation runtime Lcom/squareup/accountfreeze/LastDismissedFreezeNotification;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/f2prateek/rx/preferences2/RxSharedPreferences;",
            ")",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule;->Companion:Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/accountfreeze/wiring/AccountFreezeLoggedInModule$Companion;->provideLastDismissedFreezeNotification(Lcom/f2prateek/rx/preferences2/RxSharedPreferences;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract bindAccountFreezeIntoLoginScope$impl_wiring_release(Lcom/squareup/accountfreeze/AccountFreezeLogoutListener;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForLoggedIn;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method

.method public abstract bindAccountFreezeJobCreator$impl_wiring_release(Lcom/squareup/accountfreeze/AccountFreezeNotificationScheduler;)Lmortar/Scoped;
    .annotation runtime Lcom/squareup/ForLoggedIn;
    .end annotation

    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation runtime Ldagger/multibindings/IntoSet;
    .end annotation
.end method
