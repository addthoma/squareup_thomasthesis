.class public final Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;
.super Ljava/lang/Object;
.source "AccountFreezeNotificationManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/accountfreeze/AccountFreezeNotificationManager$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0006\u0010\r\u001a\u00020\u000eJ\u0006\u0010\u000f\u001a\u00020\u000eR\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;",
        "",
        "notificationManager",
        "Landroid/app/NotificationManager;",
        "notificationWrapper",
        "Lcom/squareup/notification/NotificationWrapper;",
        "application",
        "Landroid/app/Application;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "appNameFormatter",
        "Lcom/squareup/util/AppNameFormatter;",
        "(Landroid/app/NotificationManager;Lcom/squareup/notification/NotificationWrapper;Landroid/app/Application;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/AppNameFormatter;)V",
        "hideNotification",
        "",
        "showNotification",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/accountfreeze/AccountFreezeNotificationManager$Companion;

.field private static final DELETE_ID:I = 0xd31337


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appNameFormatter:Lcom/squareup/util/AppNameFormatter;

.field private final application:Landroid/app/Application;

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final notificationWrapper:Lcom/squareup/notification/NotificationWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->Companion:Lcom/squareup/accountfreeze/AccountFreezeNotificationManager$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/app/NotificationManager;Lcom/squareup/notification/NotificationWrapper;Landroid/app/Application;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/AppNameFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "notificationManager"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationWrapper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "application"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appNameFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    iput-object p2, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iput-object p3, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->application:Landroid/app/Application;

    iput-object p4, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p5, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    return-void
.end method


# virtual methods
.method public final hideNotification()V
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    sget v1, Lcom/squareup/accountfreeze/impl/R$id;->freeze_notification:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    return-void
.end method

.method public final showNotification()V
    .locals 8

    .line 33
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->application:Landroid/app/Application;

    check-cast v0, Landroid/content/Context;

    const-string v1, "square-register://account-freeze"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 32
    invoke-static {v0, v1}, Lcom/squareup/ui/PaymentActivity;->createPendingIntentForDeepLink(Landroid/content/Context;Landroid/net/Uri;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 36
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->application:Landroid/app/Application;

    check-cast v2, Landroid/content/Context;

    const-class v3, Lcom/squareup/accountfreeze/AccountFreezeDismissReceiver;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 37
    iget-object v2, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->application:Landroid/app/Application;

    check-cast v2, Landroid/content/Context;

    const/4 v3, 0x0

    const v4, 0xd31337

    invoke-static {v2, v4, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 39
    iget-object v2, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->application:Landroid/app/Application;

    sget v4, Lcom/squareup/accountfreeze/impl/R$string;->freeze_notification_title:I

    invoke-virtual {v2, v4}, Landroid/app/Application;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v4, "application.getString(R.\u2026reeze_notification_title)"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v4, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v5, Lcom/squareup/accountfreeze/impl/R$string;->freeze_notification_message:I

    invoke-interface {v4, v5}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 41
    iget-object v5, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->notificationWrapper:Lcom/squareup/notification/NotificationWrapper;

    iget-object v6, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->application:Landroid/app/Application;

    check-cast v6, Landroid/content/Context;

    sget-object v7, Lcom/squareup/notification/Channels;->ACCOUNT_ISSUE:Lcom/squareup/notification/Channels;

    check-cast v7, Lcom/squareup/notification/Channel;

    invoke-virtual {v5, v6, v7}, Lcom/squareup/notification/NotificationWrapper;->getNotificationBuilder(Landroid/content/Context;Lcom/squareup/notification/Channel;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v5

    .line 42
    invoke-virtual {v5, v1}, Landroidx/core/app/NotificationCompat$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    .line 43
    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    .line 44
    invoke-virtual {v1, v2}, Landroidx/core/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    .line 45
    invoke-virtual {v1, v4}, Landroidx/core/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v1

    .line 46
    invoke-virtual {v1, v0}, Landroidx/core/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x2

    .line 47
    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setPriority(I)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    .line 48
    new-instance v1, Landroidx/core/app/NotificationCompat$BigTextStyle;

    invoke-direct {v1}, Landroidx/core/app/NotificationCompat$BigTextStyle;-><init>()V

    check-cast v1, Landroidx/core/app/NotificationCompat$Style;

    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setStyle(Landroidx/core/app/NotificationCompat$Style;)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 49
    invoke-virtual {v0, v1}, Landroidx/core/app/NotificationCompat$Builder;->setOnlyAlertOnce(Z)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    .line 50
    invoke-virtual {v0, v3}, Landroidx/core/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroidx/core/app/NotificationCompat$Builder;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Landroidx/core/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    .line 53
    iget-object v1, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->notificationManager:Landroid/app/NotificationManager;

    sget v2, Lcom/squareup/accountfreeze/impl/R$id;->freeze_notification:I

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeNotificationManager;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {v0}, Lcom/squareup/accountfreeze/FreezeAnalyticsKt;->onCreatedNotification(Lcom/squareup/analytics/Analytics;)V

    return-void
.end method
