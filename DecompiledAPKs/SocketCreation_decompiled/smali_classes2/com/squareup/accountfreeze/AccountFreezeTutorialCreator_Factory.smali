.class public final Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator_Factory;
.super Ljava/lang/Object;
.source "AccountFreezeTutorialCreator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorial;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorial;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 19
    iput-object p2, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorial;",
            ">;)",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator_Factory;"
        }
    .end annotation

    .line 29
    new-instance v0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/accountfreeze/AccountFreeze;Ljavax/inject/Provider;)Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorial;",
            ">;)",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;

    invoke-direct {v0, p0, p1}, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;-><init>(Lcom/squareup/accountfreeze/AccountFreeze;Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountfreeze/AccountFreeze;

    iget-object v1, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-static {v0, v1}, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator_Factory;->newInstance(Lcom/squareup/accountfreeze/AccountFreeze;Ljavax/inject/Provider;)Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator_Factory;->get()Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;

    move-result-object v0

    return-object v0
.end method
