.class public final Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;
.super Ljava/lang/Object;
.source "AccountFreezeTutorial_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/accountfreeze/AccountFreezeTutorial;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 27
    iput-object p4, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;->arg3Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreeze;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/accountfreeze/AccountFreeze;Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;Lcom/squareup/analytics/Analytics;)Lcom/squareup/accountfreeze/AccountFreezeTutorial;
    .locals 1

    .line 43
    new-instance v0, Lcom/squareup/accountfreeze/AccountFreezeTutorial;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/accountfreeze/AccountFreezeTutorial;-><init>(Lcom/squareup/accountfreeze/AccountFreeze;Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/accountfreeze/AccountFreezeTutorial;
    .locals 4

    .line 32
    iget-object v0, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/accountfreeze/AccountFreeze;

    iget-object v1, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;

    iget-object v2, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;

    iget-object v3, p0, Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;->newInstance(Lcom/squareup/accountfreeze/AccountFreeze;Lcom/squareup/accountfreeze/AccountFreezeTutorialCreator;Lcom/squareup/accountfreeze/AccountFreezeTutorialRunner;Lcom/squareup/analytics/Analytics;)Lcom/squareup/accountfreeze/AccountFreezeTutorial;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/accountfreeze/AccountFreezeTutorial_Factory;->get()Lcom/squareup/accountfreeze/AccountFreezeTutorial;

    move-result-object v0

    return-object v0
.end method
