.class final Lcom/squareup/accountfreeze/RealAccountFreeze$frozen$1;
.super Ljava/lang/Object;
.source "RealAccountFreeze.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/accountfreeze/RealAccountFreeze;->frozen()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/accountfreeze/RealAccountFreeze;


# direct methods
.method constructor <init>(Lcom/squareup/accountfreeze/RealAccountFreeze;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/accountfreeze/RealAccountFreeze$frozen$1;->this$0:Lcom/squareup/accountfreeze/RealAccountFreeze;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p0, p1}, Lcom/squareup/accountfreeze/RealAccountFreeze$frozen$1;->apply(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lcom/squareup/settings/server/AccountStatusSettings;)Z
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    iget-object p1, p0, Lcom/squareup/accountfreeze/RealAccountFreeze$frozen$1;->this$0:Lcom/squareup/accountfreeze/RealAccountFreeze;

    invoke-virtual {p1}, Lcom/squareup/accountfreeze/RealAccountFreeze;->isFrozen()Z

    move-result p1

    return p1
.end method
