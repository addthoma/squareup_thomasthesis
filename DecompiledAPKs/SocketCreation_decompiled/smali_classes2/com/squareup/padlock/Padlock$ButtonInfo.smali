.class public abstract Lcom/squareup/padlock/Padlock$ButtonInfo;
.super Ljava/lang/Object;
.source "Padlock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/padlock/Padlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "ButtonInfo"
.end annotation


# instance fields
.field protected final background:Landroid/graphics/drawable/StateListDrawable;

.field private bottom:F

.field private final content:Ljava/lang/String;

.field protected final contentDisabledPaint:Landroid/text/TextPaint;

.field protected final contentPaint:Landroid/text/TextPaint;

.field private final contentType:Lcom/squareup/padlock/Padlock$ContentType;

.field private description:Ljava/lang/String;

.field private displayValue:Ljava/lang/String;

.field private enabled:Z

.field private left:F

.field private final letters:Ljava/lang/String;

.field protected final lettersDisabledPaint:Landroid/text/TextPaint;

.field protected final lettersPaint:Landroid/text/TextPaint;

.field private longPressed:Z

.field private pressed:Z

.field private right:F

.field final synthetic this$0:Lcom/squareup/padlock/Padlock;

.field private top:F


# direct methods
.method public constructor <init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V
    .locals 0

    .line 1342
    iput-object p1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x1

    .line 1343
    iput-boolean p1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->enabled:Z

    .line 1344
    iput-object p2, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->background:Landroid/graphics/drawable/StateListDrawable;

    .line 1345
    iput-object p5, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->content:Ljava/lang/String;

    .line 1346
    iput-object p8, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->letters:Ljava/lang/String;

    .line 1347
    iput-object p3, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->contentPaint:Landroid/text/TextPaint;

    .line 1348
    iput-object p4, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->contentDisabledPaint:Landroid/text/TextPaint;

    .line 1349
    iput-object p6, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->lettersPaint:Landroid/text/TextPaint;

    .line 1350
    iput-object p7, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->lettersDisabledPaint:Landroid/text/TextPaint;

    .line 1351
    iput-object p9, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->contentType:Lcom/squareup/padlock/Padlock$ContentType;

    .line 1352
    invoke-virtual {p10, p11}, Lcom/squareup/padlock/Padlock$Key;->getDisplayValue(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->displayValue:Ljava/lang/String;

    .line 1353
    invoke-virtual {p10, p11}, Lcom/squareup/padlock/Padlock$Key;->getDescription(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->description:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V
    .locals 12

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    .line 1336
    invoke-direct/range {v0 .. v11}, Lcom/squareup/padlock/Padlock$ButtonInfo;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/padlock/Padlock$ButtonInfo;)F
    .locals 0

    .line 1299
    iget p0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->left:F

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/padlock/Padlock$ButtonInfo;)F
    .locals 0

    .line 1299
    iget p0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->right:F

    return p0
.end method

.method static synthetic access$400(Lcom/squareup/padlock/Padlock$ButtonInfo;)F
    .locals 0

    .line 1299
    iget p0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->top:F

    return p0
.end method

.method static synthetic access$500(Lcom/squareup/padlock/Padlock$ButtonInfo;)F
    .locals 0

    .line 1299
    iget p0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->bottom:F

    return p0
.end method


# virtual methods
.method public centerX()F
    .locals 2

    .line 1378
    iget v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->left:F

    iget v1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->right:F

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public centerY()F
    .locals 2

    .line 1382
    iget v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->top:F

    iget v1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->bottom:F

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public clearPress()V
    .locals 1

    const/4 v0, 0x0

    .line 1464
    iput-boolean v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->pressed:Z

    .line 1465
    iput-boolean v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->longPressed:Z

    return-void
.end method

.method protected abstract click(FF)V
.end method

.method public contains(FF)Z
    .locals 1

    .line 1374
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->enabled:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->left:F

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->right:F

    cmpg-float p1, p1, v0

    if-gtz p1, :cond_0

    iget p1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->top:F

    cmpl-float p1, p2, p1

    if-ltz p1, :cond_0

    iget p1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->bottom:F

    cmpg-float p1, p2, p1

    if-gtz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 8

    .line 1394
    invoke-virtual {p0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->drawBackground(Landroid/graphics/Canvas;)V

    .line 1396
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->centerX()F

    move-result v0

    .line 1397
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->centerY()F

    move-result v1

    .line 1398
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->getContent()Ljava/lang/String;

    move-result-object v2

    const v3, 0x3d7ae148    # 0.06125f

    const/high16 v4, 0x40000000    # 2.0f

    if-eqz v2, :cond_3

    .line 1399
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->getContentPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 1402
    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v5

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v6

    div-float/2addr v6, v4

    add-float/2addr v5, v6

    sub-float v5, v1, v5

    .line 1405
    iget-object v6, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->contentType:Lcom/squareup/padlock/Padlock$ContentType;

    sget-object v7, Lcom/squareup/padlock/Padlock$ContentType;->TEXT:Lcom/squareup/padlock/Padlock$ContentType;

    if-ne v6, v7, :cond_0

    .line 1406
    iget-object v6, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v6}, Lcom/squareup/padlock/Padlock;->getTypefaceSize()I

    move-result v6

    int-to-float v6, v6

    mul-float v6, v6, v3

    :goto_0
    add-float/2addr v5, v6

    goto :goto_1

    .line 1407
    :cond_0
    iget-object v6, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->contentType:Lcom/squareup/padlock/Padlock$ContentType;

    sget-object v7, Lcom/squareup/padlock/Padlock$ContentType;->MARIN_GLYPH:Lcom/squareup/padlock/Padlock$ContentType;

    if-ne v6, v7, :cond_1

    .line 1409
    invoke-virtual {v2}, Landroid/text/TextPaint;->getTextSize()F

    move-result v6

    const/high16 v7, 0x40880000    # 4.25f

    div-float/2addr v6, v7

    goto :goto_0

    .line 1412
    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->letters:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 1413
    iget-object v6, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v6}, Lcom/squareup/padlock/Padlock;->getLettersTypefaceSize()I

    move-result v6

    int-to-float v6, v6

    iget-object v7, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v7}, Lcom/squareup/padlock/Padlock;->getLineSpaceExtra()F

    move-result v7

    add-float/2addr v6, v7

    div-float/2addr v6, v4

    sub-float/2addr v5, v6

    .line 1416
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->getContent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6, v0, v5, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1418
    :cond_3
    iget-object v2, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->letters:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 1419
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->getLettersPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 1422
    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v5

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v6

    div-float/2addr v6, v4

    add-float/2addr v5, v6

    sub-float/2addr v1, v5

    iget-object v5, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    .line 1424
    invoke-virtual {v5}, Lcom/squareup/padlock/Padlock;->getLettersTypefaceSize()I

    move-result v5

    int-to-float v5, v5

    mul-float v5, v5, v3

    add-float/2addr v1, v5

    .line 1425
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->getContent()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 1426
    iget-object v3, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v3}, Lcom/squareup/padlock/Padlock;->getTypefaceSize()I

    move-result v3

    int-to-float v3, v3

    iget-object v5, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v5}, Lcom/squareup/padlock/Padlock;->getLineSpaceExtra()F

    move-result v5

    add-float/2addr v3, v5

    div-float/2addr v3, v4

    add-float/2addr v1, v3

    .line 1428
    :cond_4
    iget-object v3, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->letters:Ljava/lang/String;

    invoke-virtual {p1, v3, v0, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_5
    return-void
.end method

.method protected drawBackground(Landroid/graphics/Canvas;)V
    .locals 2

    .line 1481
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->background:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    .line 1482
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->background:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/StateListDrawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_6

    .line 1504
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 1506
    :cond_1
    check-cast p1, Lcom/squareup/padlock/Padlock$ButtonInfo;

    .line 1508
    iget v2, p1, Lcom/squareup/padlock/Padlock$ButtonInfo;->left:F

    iget v3, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->left:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_2

    return v1

    .line 1509
    :cond_2
    iget v2, p1, Lcom/squareup/padlock/Padlock$ButtonInfo;->top:F

    iget v3, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->top:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_3

    return v1

    .line 1510
    :cond_3
    iget v2, p1, Lcom/squareup/padlock/Padlock$ButtonInfo;->right:F

    iget v3, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->right:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_4

    return v1

    .line 1511
    :cond_4
    iget p1, p1, Lcom/squareup/padlock/Padlock$ButtonInfo;->bottom:F

    iget v2, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->bottom:F

    invoke-static {p1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result p1

    if-nez p1, :cond_5

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_6
    :goto_1
    return v1
.end method

.method protected getContent()Ljava/lang/String;
    .locals 1

    .line 1486
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->content:Ljava/lang/String;

    return-object v0
.end method

.method protected getContentPaint()Landroid/text/TextPaint;
    .locals 1

    .line 1386
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->enabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->contentPaint:Landroid/text/TextPaint;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->contentDisabledPaint:Landroid/text/TextPaint;

    :goto_0
    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .line 1326
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayValue()Ljava/lang/String;
    .locals 1

    .line 1322
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->displayValue:Ljava/lang/String;

    return-object v0
.end method

.method protected getDrawableState()[I
    .locals 3

    .line 1471
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->enabled:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    new-array v0, v1, [I

    return-object v0

    .line 1473
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->pressed:Z

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-static {v0}, Lcom/squareup/padlock/Padlock;->access$900(Lcom/squareup/padlock/Padlock;)Z

    move-result v0

    if-nez v0, :cond_1

    new-array v0, v2, [I

    const v2, 0x10100a7

    aput v2, v0, v1

    return-object v0

    :cond_1
    new-array v0, v2, [I

    const v2, 0x101009e

    aput v2, v0, v1

    return-object v0
.end method

.method protected getLettersPaint()Landroid/text/TextPaint;
    .locals 1

    .line 1390
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->enabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->lettersPaint:Landroid/text/TextPaint;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->lettersDisabledPaint:Landroid/text/TextPaint;

    :goto_0
    return-object v0
.end method

.method public getLocation()Landroid/graphics/Rect;
    .locals 6

    .line 1370
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->left:F

    float-to-double v1, v1

    invoke-virtual {p0, v1, v2}, Lcom/squareup/padlock/Padlock$ButtonInfo;->round(D)I

    move-result v1

    iget v2, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->top:F

    float-to-double v2, v2

    invoke-virtual {p0, v2, v3}, Lcom/squareup/padlock/Padlock$ButtonInfo;->round(D)I

    move-result v2

    iget v3, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->right:F

    float-to-double v3, v3

    invoke-virtual {p0, v3, v4}, Lcom/squareup/padlock/Padlock$ButtonInfo;->round(D)I

    move-result v3

    iget v4, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->bottom:F

    float-to-double v4, v4

    invoke-virtual {p0, v4, v5}, Lcom/squareup/padlock/Padlock$ButtonInfo;->round(D)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .line 1499
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->enabled:Z

    return v0
.end method

.method protected longClick(FF)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public onClick(FF)V
    .locals 2

    .line 1443
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->pressed:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->longPressed:Z

    if-nez v0, :cond_1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/padlock/Padlock$ButtonInfo;->contains(FF)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1445
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-static {v0}, Lcom/squareup/padlock/Padlock;->access$900(Lcom/squareup/padlock/Padlock;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1446
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/padlock/Padlock;->performHapticFeedback(I)Z

    .line 1448
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/padlock/Padlock$ButtonInfo;->click(FF)V

    .line 1451
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->clearPress()V

    return-void
.end method

.method public onLongClick(FF)Z
    .locals 2

    .line 1455
    iget-boolean v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->pressed:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/squareup/padlock/Padlock$ButtonInfo;->contains(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/squareup/padlock/Padlock$ButtonInfo;->longClick(FF)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1456
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-virtual {p1, v1}, Lcom/squareup/padlock/Padlock;->performHapticFeedback(I)Z

    const/4 p1, 0x1

    .line 1457
    iput-boolean p1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->longPressed:Z

    return p1

    :cond_0
    return v1
.end method

.method public onPressed(FF)Z
    .locals 0

    .line 1433
    invoke-virtual {p0, p1, p2}, Lcom/squareup/padlock/Padlock$ButtonInfo;->contains(FF)Z

    move-result p1

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 1434
    iput-boolean p1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->pressed:Z

    .line 1435
    iput-boolean p2, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->longPressed:Z

    return p1

    :cond_0
    return p2
.end method

.method round(D)I
    .locals 2

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    add-double/2addr p1, v0

    double-to-int p1, p1

    return p1
.end method

.method public setEnabled(Z)V
    .locals 0

    .line 1495
    iput-boolean p1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->enabled:Z

    return-void
.end method

.method public setLocation(FFFF)V
    .locals 6

    .line 1362
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->background:Landroid/graphics/drawable/StateListDrawable;

    float-to-double v1, p1

    invoke-virtual {p0, v1, v2}, Lcom/squareup/padlock/Padlock$ButtonInfo;->round(D)I

    move-result v1

    float-to-double v2, p2

    invoke-virtual {p0, v2, v3}, Lcom/squareup/padlock/Padlock$ButtonInfo;->round(D)I

    move-result v2

    float-to-double v3, p3

    invoke-virtual {p0, v3, v4}, Lcom/squareup/padlock/Padlock$ButtonInfo;->round(D)I

    move-result v3

    float-to-double v4, p4

    invoke-virtual {p0, v4, v5}, Lcom/squareup/padlock/Padlock$ButtonInfo;->round(D)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    .line 1363
    iput p1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->left:F

    .line 1364
    iput p2, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->top:F

    .line 1365
    iput p3, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->right:F

    .line 1366
    iput p4, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->bottom:F

    return-void
.end method

.method public updateRes(Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V
    .locals 1

    .line 1357
    invoke-virtual {p1, p2}, Lcom/squareup/padlock/Padlock$Key;->getDisplayValue(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->displayValue:Ljava/lang/String;

    .line 1358
    invoke-virtual {p1, p2}, Lcom/squareup/padlock/Padlock$Key;->getDescription(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/padlock/Padlock$ButtonInfo;->description:Ljava/lang/String;

    return-void
.end method
