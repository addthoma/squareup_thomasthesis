.class Lcom/squareup/padlock/Padlock$ClearOrCancelButton;
.super Lcom/squareup/padlock/Padlock$ButtonInfo;
.source "Padlock.java"

# interfaces
.implements Lcom/squareup/padlock/Padlock$UpdatableEnabledState;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/padlock/Padlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClearOrCancelButton"
.end annotation


# instance fields
.field private res:Lcom/squareup/util/Res;

.field final synthetic this$0:Lcom/squareup/padlock/Padlock;


# direct methods
.method constructor <init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V
    .locals 10

    move-object v9, p0

    move-object v1, p1

    .line 1524
    iput-object v1, v9, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    .line 1525
    invoke-direct/range {v0 .. v8}, Lcom/squareup/padlock/Padlock$ButtonInfo;-><init>(Lcom/squareup/padlock/Padlock;Landroid/graphics/drawable/StateListDrawable;Landroid/text/TextPaint;Landroid/text/TextPaint;Ljava/lang/String;Lcom/squareup/padlock/Padlock$ContentType;Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V

    move-object/from16 v0, p7

    .line 1526
    iput-object v0, v9, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private isClear()Z
    .locals 2

    .line 1589
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CLEAR:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CLEAR_DISABLED:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method protected click(FF)V
    .locals 0

    .line 1537
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    if-eqz p1, :cond_1

    .line 1538
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    sget-object p2, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CLEAR:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    if-ne p1, p2, :cond_0

    .line 1539
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    invoke-interface {p1}, Lcom/squareup/padlock/Padlock$OnKeyPressListener;->onClearClicked()V

    goto :goto_0

    .line 1541
    :cond_0
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    invoke-interface {p1}, Lcom/squareup/padlock/Padlock$OnKeyPressListener;->onCancelClicked()V

    :cond_1
    :goto_0
    return-void
.end method

.method protected getContent()Ljava/lang/String;
    .locals 2

    .line 1555
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CANCEL:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CANCEL_DISABLED:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 1558
    :cond_0
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/padlock/R$string;->text_clear:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1556
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/padlock/R$string;->text_cancel:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 2

    .line 1547
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->res:Lcom/squareup/util/Res;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->isClear()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/padlock/R$string;->text_clear:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/padlock/R$string;->text_cancel:I

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayValue()Ljava/lang/String;
    .locals 2

    .line 1551
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->res:Lcom/squareup/util/Res;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->isClear()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/padlock/R$string;->button_clear:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/padlock/R$string;->button_cancel:I

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onLongClick(FF)Z
    .locals 0

    .line 1530
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    sget-object p2, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CLEAR:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    if-ne p1, p2, :cond_0

    .line 1531
    iget-object p1, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/padlock/Padlock;->onKeyPressListener:Lcom/squareup/padlock/Padlock$OnKeyPressListener;

    invoke-interface {p1}, Lcom/squareup/padlock/Padlock$OnKeyPressListener;->onClearLongpressed()V

    :cond_0
    const/4 p1, 0x1

    return p1
.end method

.method public setEnabled(Z)V
    .locals 2

    .line 1564
    sget-object v0, Lcom/squareup/padlock/Padlock$10;->$SwitchMap$com$squareup$padlock$Padlock$PinPadLeftButtonState:[I

    iget-object v1, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v1, v1, Lcom/squareup/padlock/Padlock;->pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    invoke-virtual {v1}, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    if-eqz p1, :cond_0

    .line 1572
    sget-object v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CANCEL:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CANCEL_DISABLED:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    .line 1567
    sget-object v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CLEAR:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CLEAR_DISABLED:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    .line 1575
    :goto_0
    iget-object v1, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iput-object v0, v1, Lcom/squareup/padlock/Padlock;->pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    .line 1576
    invoke-super {p0, p1}, Lcom/squareup/padlock/Padlock$ButtonInfo;->setEnabled(Z)V

    return-void
.end method

.method public updateEnabledState()V
    .locals 2

    .line 1580
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CLEAR:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    iget-object v0, v0, Lcom/squareup/padlock/Padlock;->pinPadLeftButtonState:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    sget-object v1, Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;->CANCEL:Lcom/squareup/padlock/Padlock$PinPadLeftButtonState;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->setEnabled(Z)V

    .line 1581
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0}, Lcom/squareup/padlock/Padlock;->invalidate()V

    return-void
.end method

.method public updateRes(Lcom/squareup/padlock/Padlock$Key;Lcom/squareup/util/Res;)V
    .locals 0

    .line 1585
    iput-object p2, p0, Lcom/squareup/padlock/Padlock$ClearOrCancelButton;->res:Lcom/squareup/util/Res;

    return-void
.end method
