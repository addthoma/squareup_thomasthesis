.class Lcom/squareup/padlock/Padlock$2;
.super Ljava/lang/Object;
.source "Padlock.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/padlock/Padlock;->createKeysAndLines(Lcom/squareup/util/Res;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/padlock/Padlock;


# direct methods
.method constructor <init>(Lcom/squareup/padlock/Padlock;)V
    .locals 0

    .line 580
    iput-object p1, p0, Lcom/squareup/padlock/Padlock$2;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 582
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$2;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-static {v0}, Lcom/squareup/padlock/Padlock;->access$100(Lcom/squareup/padlock/Padlock;)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/squareup/padlock/Padlock$Key;->BACKSPACE:Lcom/squareup/padlock/Padlock$Key;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock$ButtonInfo;

    .line 583
    invoke-static {v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->access$200(Lcom/squareup/padlock/Padlock$ButtonInfo;)F

    move-result v1

    invoke-static {v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->access$300(Lcom/squareup/padlock/Padlock$ButtonInfo;)F

    move-result v2

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    .line 584
    invoke-static {v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->access$400(Lcom/squareup/padlock/Padlock$ButtonInfo;)F

    move-result v3

    invoke-static {v0}, Lcom/squareup/padlock/Padlock$ButtonInfo;->access$500(Lcom/squareup/padlock/Padlock$ButtonInfo;)F

    move-result v4

    add-float/2addr v3, v4

    div-float/2addr v3, v2

    .line 583
    invoke-virtual {v0, v1, v3}, Lcom/squareup/padlock/Padlock$ButtonInfo;->click(FF)V

    .line 585
    iget-object v0, p0, Lcom/squareup/padlock/Padlock$2;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-static {v0}, Lcom/squareup/padlock/Padlock;->access$700(Lcom/squareup/padlock/Padlock;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/padlock/Padlock$2;->this$0:Lcom/squareup/padlock/Padlock;

    invoke-static {v1}, Lcom/squareup/padlock/Padlock;->access$600(Lcom/squareup/padlock/Padlock;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
