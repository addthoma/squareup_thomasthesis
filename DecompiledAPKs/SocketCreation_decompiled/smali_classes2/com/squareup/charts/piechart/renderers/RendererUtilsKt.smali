.class public final Lcom/squareup/charts/piechart/renderers/RendererUtilsKt;
.super Ljava/lang/Object;
.source "RendererUtils.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u001a \u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0001\u001a \u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u00012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "calculateAngle",
        "",
        "slice",
        "Lcom/squareup/charts/piechart/Slice;",
        "progress",
        "totalLength",
        "length",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final calculateAngle(FFF)F
    .locals 0

    mul-float p0, p0, p1

    const/high16 p1, 0x43b40000    # 360.0f

    mul-float p0, p0, p1

    div-float/2addr p0, p2

    return p0
.end method

.method public static final calculateAngle(Lcom/squareup/charts/piechart/Slice;FF)F
    .locals 1

    const-string v0, "slice"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-virtual {p0}, Lcom/squareup/charts/piechart/Slice;->getLength()F

    move-result p0

    invoke-static {p0, p1, p2}, Lcom/squareup/charts/piechart/renderers/RendererUtilsKt;->calculateAngle(FFF)F

    move-result p0

    return p0
.end method

.method public static synthetic calculateAngle$default(FFFILjava/lang/Object;)F
    .locals 0

    and-int/lit8 p3, p3, 0x4

    if-eqz p3, :cond_0

    const/high16 p2, 0x42c80000    # 100.0f

    .line 16
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/charts/piechart/renderers/RendererUtilsKt;->calculateAngle(FFF)F

    move-result p0

    return p0
.end method

.method public static synthetic calculateAngle$default(Lcom/squareup/charts/piechart/Slice;FFILjava/lang/Object;)F
    .locals 0

    and-int/lit8 p3, p3, 0x4

    if-eqz p3, :cond_0

    const/high16 p2, 0x42c80000    # 100.0f

    .line 9
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/charts/piechart/renderers/RendererUtilsKt;->calculateAngle(Lcom/squareup/charts/piechart/Slice;FF)F

    move-result p0

    return p0
.end method
