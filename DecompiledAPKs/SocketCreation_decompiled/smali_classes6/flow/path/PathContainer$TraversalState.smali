.class public final Lflow/path/PathContainer$TraversalState;
.super Ljava/lang/Object;
.source "PathContainer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/path/PathContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1c
    name = "TraversalState"
.end annotation


# instance fields
.field private fromPath:Lflow/path/Path;

.field private fromViewState:Lflow/ViewState;

.field private toPath:Lflow/path/Path;

.field private toViewState:Lflow/ViewState;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lflow/path/PathContainer$TraversalState;)Lflow/path/Path;
    .locals 0

    .line 36
    iget-object p0, p0, Lflow/path/PathContainer$TraversalState;->toPath:Lflow/path/Path;

    return-object p0
.end method


# virtual methods
.method public fromPath()Lflow/path/Path;
    .locals 1

    .line 50
    iget-object v0, p0, Lflow/path/PathContainer$TraversalState;->fromPath:Lflow/path/Path;

    return-object v0
.end method

.method public restoreViewState(Landroid/view/View;)V
    .locals 1

    .line 62
    iget-object v0, p0, Lflow/path/PathContainer$TraversalState;->toViewState:Lflow/ViewState;

    invoke-interface {v0, p1}, Lflow/ViewState;->restore(Landroid/view/View;)V

    return-void
.end method

.method public saveViewState(Landroid/view/View;)V
    .locals 1

    .line 58
    iget-object v0, p0, Lflow/path/PathContainer$TraversalState;->fromViewState:Lflow/ViewState;

    invoke-interface {v0, p1}, Lflow/ViewState;->save(Landroid/view/View;)V

    return-void
.end method

.method public setNextEntry(Lflow/path/Path;Lflow/ViewState;)V
    .locals 1

    .line 43
    iget-object v0, p0, Lflow/path/PathContainer$TraversalState;->toPath:Lflow/path/Path;

    iput-object v0, p0, Lflow/path/PathContainer$TraversalState;->fromPath:Lflow/path/Path;

    .line 44
    iget-object v0, p0, Lflow/path/PathContainer$TraversalState;->toViewState:Lflow/ViewState;

    iput-object v0, p0, Lflow/path/PathContainer$TraversalState;->fromViewState:Lflow/ViewState;

    .line 45
    iput-object p1, p0, Lflow/path/PathContainer$TraversalState;->toPath:Lflow/path/Path;

    .line 46
    iput-object p2, p0, Lflow/path/PathContainer$TraversalState;->toViewState:Lflow/ViewState;

    return-void
.end method

.method public toPath()Lflow/path/Path;
    .locals 1

    .line 54
    iget-object v0, p0, Lflow/path/PathContainer$TraversalState;->toPath:Lflow/path/Path;

    return-object v0
.end method
