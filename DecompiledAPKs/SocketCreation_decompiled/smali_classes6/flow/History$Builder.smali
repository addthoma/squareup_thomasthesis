.class public final Lflow/History$Builder;
.super Ljava/lang/Object;
.source "History.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/History;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final entryMemory:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Lflow/History$Entry;",
            ">;"
        }
    .end annotation
.end field

.field private final history:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lflow/History$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Lflow/History$Entry;",
            ">;)V"
        }
    .end annotation

    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lflow/History$Builder;->entryMemory:Ljava/util/Map;

    .line 221
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lflow/History$Builder;->history:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Collection;Lflow/History$1;)V
    .locals 0

    .line 216
    invoke-direct {p0, p1}, Lflow/History$Builder;-><init>(Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public addAll(Ljava/util/Collection;)Lflow/History$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "*>;)",
            "Lflow/History$Builder;"
        }
    .end annotation

    .line 267
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 268
    invoke-virtual {p0, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public build()Lflow/History;
    .locals 3

    .line 322
    new-instance v0, Lflow/History;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lflow/History$Builder;->history:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lflow/History;-><init>(Ljava/util/List;Lflow/History$1;)V

    return-object v0
.end method

.method public clear()Lflow/History$Builder;
    .locals 1

    .line 241
    :goto_0
    invoke-virtual {p0}, Lflow/History$Builder;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    invoke-virtual {p0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public framesFromBottom()Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/lang/Iterable<",
            "TT;>;"
        }
    .end annotation

    .line 229
    new-instance v0, Lflow/History$HistoryIterable;

    iget-object v1, p0, Lflow/History$Builder;->history:Ljava/util/List;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lflow/History$HistoryIterable;-><init>(Ljava/util/List;Z)V

    return-object v0
.end method

.method public framesFromTop()Ljava/lang/Iterable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Ljava/lang/Iterable<",
            "TT;>;"
        }
    .end annotation

    .line 225
    new-instance v0, Lflow/History$HistoryIterable;

    iget-object v1, p0, Lflow/History$Builder;->history:Ljava/util/List;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lflow/History$HistoryIterable;-><init>(Ljava/util/List;Z)V

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 278
    iget-object v0, p0, Lflow/History$Builder;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public peek()Ljava/lang/Object;
    .locals 2

    .line 274
    iget-object v0, p0, Lflow/History$Builder;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lflow/History$Builder;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/History$Entry;

    iget-object v0, v0, Lflow/History$Entry;->state:Ljava/lang/Object;

    :goto_0
    return-object v0
.end method

.method public pop(I)Lflow/History$Builder;
    .locals 6

    .line 312
    iget-object v0, p0, Lflow/History$Builder;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-gt p1, v0, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    .line 314
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    const-string v0, "Cannot pop %d elements, history only has %d"

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 313
    invoke-static {v3, v0}, Lflow/Preconditions;->checkArgument(ZLjava/lang/String;)V

    :goto_1
    add-int/lit8 v0, p1, -0x1

    if-lez p1, :cond_1

    .line 316
    invoke-virtual {p0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    move p1, v0

    goto :goto_1

    :cond_1
    return-object p0
.end method

.method public pop()Ljava/lang/Object;
    .locals 3

    .line 290
    invoke-virtual {p0}, Lflow/History$Builder;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    iget-object v0, p0, Lflow/History$Builder;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/History$Entry;

    .line 294
    iget-object v1, p0, Lflow/History$Builder;->entryMemory:Ljava/util/Map;

    iget-object v2, v0, Lflow/History$Entry;->state:Ljava/lang/Object;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    iget-object v0, v0, Lflow/History$Entry;->state:Ljava/lang/Object;

    return-object v0

    .line 291
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot pop from an empty builder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public popTo(Ljava/lang/Object;)Lflow/History$Builder;
    .locals 3

    .line 304
    :goto_0
    invoke-virtual {p0}, Lflow/History$Builder;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 305
    invoke-virtual {p0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    goto :goto_0

    .line 307
    :cond_0
    invoke-virtual {p0}, Lflow/History$Builder;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "%s not found in history"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lflow/Preconditions;->checkArgument(ZLjava/lang/String;)V

    return-object p0
.end method

.method public push(Ljava/lang/Object;)Lflow/History$Builder;
    .locals 2

    .line 254
    iget-object v0, p0, Lflow/History$Builder;->entryMemory:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/History$Entry;

    if-nez v0, :cond_0

    .line 256
    new-instance v0, Lflow/History$Entry;

    invoke-direct {v0, p1}, Lflow/History$Entry;-><init>(Ljava/lang/Object;)V

    .line 258
    :cond_0
    iget-object v1, p0, Lflow/History$Builder;->history:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    iget-object v0, p0, Lflow/History$Builder;->entryMemory:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 326
    iget-object v0, p0, Lflow/History$Builder;->history:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->deepToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
