.class final Lhu/akarnokd/rxjava/interop/RxJavaInterop$8;
.super Ljava/lang/Object;
.source "RxJavaInterop.java"

# interfaces
.implements Lrx/Single$Transformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Transformer(Lio/reactivex/SingleTransformer;)Lrx/Single$Transformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Single$Transformer<",
        "TT;TR;>;"
    }
.end annotation


# instance fields
.field final synthetic val$transformer:Lio/reactivex/SingleTransformer;


# direct methods
.method constructor <init>(Lio/reactivex/SingleTransformer;)V
    .locals 0

    .line 570
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$8;->val$transformer:Lio/reactivex/SingleTransformer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 570
    check-cast p1, Lrx/Single;

    invoke-virtual {p0, p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$8;->call(Lrx/Single;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public call(Lrx/Single;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Single<",
            "TT;>;)",
            "Lrx/Single<",
            "TR;>;"
        }
    .end annotation

    .line 573
    iget-object v0, p0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$8;->val$transformer:Lio/reactivex/SingleTransformer;

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    invoke-interface {v0, p1}, Lio/reactivex/SingleTransformer;->apply(Lio/reactivex/Single;)Lio/reactivex/SingleSource;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method
