.class Lcom/starmicronics/stario/h;
.super Lcom/starmicronics/stario/TCPPort;


# instance fields
.field private l:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/starmicronics/stario/TCPPort;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method private declared-synchronized a([BII)I
    .locals 3

    monitor-enter p0

    :try_start_0
    array-length v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/2addr p2, p3

    const/4 v1, 0x0

    if-ge v0, p2, :cond_0

    monitor-exit p0

    return v1

    :cond_0
    :try_start_1
    iget-object p2, p0, Lcom/starmicronics/stario/h;->c:Ljava/net/Socket;

    invoke-virtual {p2}, Ljava/net/Socket;->isConnected()Z

    move-result p2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez p2, :cond_1

    :try_start_2
    invoke-virtual {p0}, Lcom/starmicronics/stario/h;->b()V
    :try_end_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    monitor-exit p0

    return v1

    :cond_1
    :goto_0
    const/4 p2, 0x0

    :goto_1
    sub-int v0, p3, p2

    if-lez v0, :cond_3

    :try_start_3
    iget-object v2, p0, Lcom/starmicronics/stario/h;->e:Ljava/io/DataInputStream;

    invoke-virtual {v2}, Ljava/io/DataInputStream;->available()I

    move-result v2
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v2, :cond_2

    monitor-exit p0

    return p2

    :cond_2
    :try_start_4
    iget-object v2, p0, Lcom/starmicronics/stario/h;->e:Ljava/io/DataInputStream;

    invoke-virtual {v2, p1, p2, v0}, Ljava/io/DataInputStream;->read([BII)I

    move-result v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    add-int/2addr p2, v0

    goto :goto_1

    :catch_1
    monitor-exit p0

    return v1

    :cond_3
    monitor-exit p0

    return p2

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method


# virtual methods
.method protected declared-synchronized b()V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v1, p0

    monitor-enter p0

    :try_start_0
    iget-object v0, v1, Lcom/starmicronics/stario/h;->f:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/starmicronics/stario/h;->l:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_b
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v3, v1, Lcom/starmicronics/stario/h;->l:Ljava/lang/String;

    const/16 v4, 0x238c

    invoke-direct {v0, v3, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    new-instance v3, Ljava/net/Socket;

    invoke-direct {v3}, Ljava/net/Socket;-><init>()V

    iput-object v3, v1, Lcom/starmicronics/stario/h;->c:Ljava/net/Socket;

    iget-object v3, v1, Lcom/starmicronics/stario/h;->c:Ljava/net/Socket;

    iget v5, v1, Lcom/starmicronics/stario/h;->h:I

    invoke-virtual {v3, v5}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v3, v1, Lcom/starmicronics/stario/h;->c:Ljava/net/Socket;

    iget-boolean v5, v1, Lcom/starmicronics/stario/h;->j:Z

    invoke-virtual {v3, v5}, Ljava/net/Socket;->setKeepAlive(Z)V

    iget-object v3, v1, Lcom/starmicronics/stario/h;->c:Ljava/net/Socket;

    iget-boolean v5, v1, Lcom/starmicronics/stario/h;->k:Z

    invoke-virtual {v3, v5}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    iget-object v3, v1, Lcom/starmicronics/stario/h;->c:Ljava/net/Socket;

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Ljava/net/Socket;->setReuseAddress(Z)V

    iget-object v3, v1, Lcom/starmicronics/stario/h;->c:Ljava/net/Socket;

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Ljava/net/Socket;->bind(Ljava/net/SocketAddress;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/16 v3, 0x200

    :try_start_2
    new-array v7, v3, [B

    const/16 v8, 0xd

    new-array v8, v8, [B

    const/4 v9, 0x0

    aput-byte v9, v8, v9

    aput-byte v9, v8, v5

    const/16 v10, 0x30

    const/4 v11, 0x2

    aput-byte v10, v8, v11

    const/4 v10, 0x3

    aput-byte v9, v8, v10

    const/16 v12, 0x23

    aput-byte v12, v8, v2

    const/16 v12, -0x74

    const/4 v13, 0x5

    aput-byte v12, v8, v13

    const/4 v12, 0x6

    aput-byte v9, v8, v12

    const/4 v12, 0x7

    aput-byte v13, v8, v12

    const/16 v12, 0x31

    const/16 v13, 0x8

    aput-byte v12, v8, v13

    iget-object v12, v1, Lcom/starmicronics/stario/h;->c:Ljava/net/Socket;

    invoke-virtual {v12}, Ljava/net/Socket;->getLocalPort()I

    move-result v12

    div-int/lit16 v12, v12, 0x100

    int-to-byte v12, v12

    const/16 v14, 0x9

    aput-byte v12, v8, v14

    iget-object v12, v1, Lcom/starmicronics/stario/h;->c:Ljava/net/Socket;

    invoke-virtual {v12}, Ljava/net/Socket;->getLocalPort()I

    move-result v12

    rem-int/lit16 v12, v12, 0x100

    int-to-byte v12, v12

    const/16 v15, 0xa

    aput-byte v12, v8, v15

    const/16 v12, 0xb

    aput-byte v9, v8, v12

    const/16 v16, 0xc

    aput-byte v15, v8, v16

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    iget v6, v1, Lcom/starmicronics/stario/h;->h:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    int-to-long v10, v6

    add-long v16, v16, v10

    :try_start_3
    iget-object v6, v1, Lcom/starmicronics/stario/h;->l:Ljava/lang/String;

    invoke-static {v6}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v6
    :try_end_3
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    new-instance v10, Ljava/net/DatagramPacket;

    array-length v11, v8

    invoke-direct {v10, v8, v11, v6, v4}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    const/4 v6, 0x0

    :goto_0
    :try_start_5
    new-instance v8, Ljava/net/DatagramSocket;

    invoke-direct {v8, v4}, Ljava/net/DatagramSocket;-><init>(I)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const/16 v11, 0x64

    :try_start_6
    invoke-virtual {v8, v11}, Ljava/net/DatagramSocket;->setSoTimeout(I)V

    invoke-virtual {v8, v10}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_7
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    new-instance v11, Ljava/net/DatagramPacket;

    invoke-direct {v11, v7, v3}, Ljava/net/DatagramPacket;-><init>([BI)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    invoke-virtual {v8, v11}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    invoke-virtual {v11}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v11

    array-length v3, v11

    const/16 v4, 0x17

    if-lt v3, v4, :cond_3

    new-array v3, v2, [B

    aget-byte v4, v11, v13

    aput-byte v4, v3, v9

    aget-byte v4, v11, v14

    aput-byte v4, v3, v5

    aget-byte v4, v11, v15
    :try_end_8
    .catch Ljava/net/SocketTimeoutException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    const/16 v19, 0x2

    :try_start_9
    aput-byte v4, v3, v19

    aget-byte v4, v11, v12
    :try_end_9
    .catch Ljava/net/SocketTimeoutException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    const/4 v11, 0x3

    :try_start_a
    aput-byte v4, v3, v11

    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v20

    if-eqz v20, :cond_4

    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/net/NetworkInterface;

    invoke-virtual/range {v20 .. v20}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v20

    :goto_2
    invoke-interface/range {v20 .. v20}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v21

    if-eqz v21, :cond_1

    invoke-interface/range {v20 .. v20}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/net/InetAddress;

    invoke-virtual/range {v21 .. v21}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v2

    invoke-static {v3, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2
    :try_end_a
    .catch Ljava/net/SocketTimeoutException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    if-ne v2, v5, :cond_0

    const/4 v6, 0x1

    goto :goto_3

    :cond_0
    const/4 v2, 0x4

    goto :goto_2

    :cond_1
    :goto_3
    if-ne v6, v5, :cond_2

    goto :goto_4

    :cond_2
    const/4 v2, 0x4

    goto :goto_1

    :catch_0
    const/4 v11, 0x3

    goto :goto_4

    :cond_3
    const/4 v11, 0x3

    const/16 v19, 0x2

    :catch_1
    :cond_4
    :goto_4
    :try_start_b
    invoke-virtual {v8}, Ljava/net/DatagramSocket;->close()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_6

    :catchall_0
    move-exception v0

    goto :goto_5

    :catch_2
    move-exception v0

    :try_start_c
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    :goto_5
    :try_start_d
    invoke-virtual {v8}, Ljava/net/DatagramSocket;->close()V

    throw v0

    :catch_3
    const/4 v11, 0x3

    const/16 v19, 0x2

    goto :goto_4

    :goto_6
    if-ne v6, v5, :cond_5

    goto :goto_7

    :cond_5
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    const/16 v3, 0x12c

    :try_start_e
    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/2addr v2, v3

    int-to-long v2, v2

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_e
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :catch_4
    :try_start_f
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    cmp-long v4, v16, v2

    if-gtz v4, :cond_7

    :goto_7
    if-eqz v6, :cond_6

    :try_start_10
    iget-object v2, v1, Lcom/starmicronics/stario/h;->c:Ljava/net/Socket;

    iget v3, v1, Lcom/starmicronics/stario/h;->h:I

    invoke-virtual {v2, v0, v3}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v2, v1, Lcom/starmicronics/stario/h;->c:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, v1, Lcom/starmicronics/stario/h;->d:Ljava/io/DataOutputStream;

    new-instance v0, Ljava/io/DataInputStream;

    iget-object v2, v1, Lcom/starmicronics/stario/h;->c:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, v1, Lcom/starmicronics/stario/h;->e:Ljava/io/DataInputStream;
    :try_end_10
    .catch Ljava/net/UnknownHostException; {:try_start_10 .. :try_end_10} :catch_6
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_5
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :try_start_11
    const-string v0, "DK-AirCash-W"

    iput-object v0, v1, Lcom/starmicronics/stario/h;->b:Ljava/lang/String;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    monitor-exit p0

    return-void

    :catch_5
    :try_start_12
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Printer might be power off."

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_6
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Cannot connect to printer."

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Device is busy."

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    const/4 v2, 0x4

    const/16 v3, 0x200

    const/16 v4, 0x238c

    goto/16 :goto_0

    :catch_7
    move-exception v0

    move-object/from16 v18, v8

    goto :goto_8

    :catch_8
    move-exception v0

    const/16 v18, 0x0

    :goto_8
    if-eqz v18, :cond_8

    invoke-virtual/range {v18 .. v18}, Ljava/net/DatagramSocket;->close()V

    :cond_8
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_9
    move-exception v0

    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_a
    move-exception v0

    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :catchall_1
    move-exception v0

    goto :goto_9

    :catch_b
    move-exception v0

    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    :goto_9
    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized beginCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 27
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v1, p0

    monitor-enter p0

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/stario/h;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    const/4 v3, 0x1

    if-eq v2, v3, :cond_b

    const/16 v2, 0x16

    new-array v4, v2, [B

    const/4 v5, 0x0

    aput-byte v5, v4, v5

    aput-byte v5, v4, v3

    const/4 v6, 0x2

    aput-byte v5, v4, v6

    const/4 v7, 0x3

    aput-byte v5, v4, v7

    const/4 v8, 0x4

    aput-byte v5, v4, v8

    const/4 v9, 0x5

    aput-byte v5, v4, v9

    const/4 v10, 0x6

    aput-byte v5, v4, v10

    const/4 v11, 0x7

    aput-byte v5, v4, v11

    const/16 v12, 0x8

    aput-byte v5, v4, v12

    const/16 v13, 0x9

    aput-byte v5, v4, v13

    const/16 v14, 0xa

    aput-byte v5, v4, v14

    const/16 v15, 0xb

    aput-byte v5, v4, v15

    const/16 v16, 0xc

    aput-byte v5, v4, v16

    const/16 v16, 0xd

    aput-byte v5, v4, v16

    const/16 v16, 0xe

    aput-byte v5, v4, v16

    const/16 v16, 0xf

    aput-byte v5, v4, v16

    const/16 v16, 0x10

    const/16 v15, 0x1b

    aput-byte v15, v4, v16

    const/16 v16, 0x11

    const/16 v18, 0x23

    aput-byte v18, v4, v16

    const/16 v16, 0x12

    const/16 v18, 0x2c

    aput-byte v18, v4, v16

    const/16 v16, 0x13

    const/16 v18, 0x31

    aput-byte v18, v4, v16

    const/16 v16, 0x14

    aput-byte v14, v4, v16

    const/16 v16, 0x15

    aput-byte v5, v4, v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    array-length v13, v4

    invoke-virtual {v1, v4, v5, v13}, Lcom/starmicronics/stario/h;->writePort([BII)V
    :try_end_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget v4, v1, Lcom/starmicronics/stario/h;->h:I

    const/16 v13, 0x2710

    if-le v4, v13, :cond_0

    iget v4, v1, Lcom/starmicronics/stario/h;->h:I

    goto :goto_0

    :cond_0
    const/16 v4, 0x2710

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    const/16 v13, 0x200

    new-array v13, v13, [B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v12, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    :goto_1
    :try_start_3
    iget-object v10, v1, Lcom/starmicronics/stario/h;->e:Ljava/io/DataInputStream;

    invoke-virtual {v10}, Ljava/io/DataInputStream;->available()I

    move-result v10

    if-lez v10, :cond_1

    iget-object v10, v1, Lcom/starmicronics/stario/h;->e:Ljava/io/DataInputStream;

    array-length v9, v13

    sub-int/2addr v9, v12

    invoke-virtual {v10, v13, v12, v9}, Ljava/io/DataInputStream;->read([BII)I

    move-result v9

    add-int/2addr v12, v9

    add-int v21, v21, v9

    :cond_1
    move/from16 v9, v21

    :goto_2
    if-lt v9, v11, :cond_3

    aget-byte v10, v13, v22

    if-ne v10, v15, :cond_2

    add-int/lit8 v10, v22, 0x1

    aget-byte v10, v13, v10

    const/16 v11, 0x23

    if-ne v10, v11, :cond_2

    add-int/lit8 v10, v22, 0x2

    aget-byte v10, v13, v10

    const/16 v11, 0x2c

    if-ne v10, v11, :cond_2

    add-int/lit8 v10, v22, 0x3

    aget-byte v10, v13, v10

    const/16 v11, 0x31

    if-ne v10, v11, :cond_2

    add-int/lit8 v10, v22, 0x5

    aget-byte v10, v13, v10

    if-ne v10, v14, :cond_2

    add-int/lit8 v10, v22, 0x6

    aget-byte v10, v13, v10
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez v10, :cond_2

    const/4 v10, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v22, v22, 0x1

    add-int/lit8 v9, v9, -0x1

    const/4 v11, 0x7

    goto :goto_2

    :cond_3
    move/from16 v10, v23

    :goto_3
    if-ne v10, v3, :cond_9

    :try_start_4
    new-array v9, v3, [B

    add-int/lit8 v22, v22, 0x4

    aget-byte v10, v13, v22

    aput-byte v10, v9, v5

    iget v10, v1, Lcom/starmicronics/stario/h;->h:I

    invoke-static {v9, v10}, Lcom/starmicronics/stario/f;->a([BI)I

    move-result v9

    if-eqz v9, :cond_4

    iput v9, v1, Lcom/starmicronics/stario/h;->a:I

    :cond_4
    new-array v2, v2, [B

    aput-byte v15, v2, v5

    const/16 v9, 0x2a

    aput-byte v9, v2, v3

    const/16 v9, 0x72

    aput-byte v9, v2, v6

    const/16 v9, 0x42

    aput-byte v9, v2, v7

    aput-byte v15, v2, v8

    const/16 v9, 0x1d

    const/4 v10, 0x5

    aput-byte v9, v2, v10

    const/4 v10, 0x6

    aput-byte v7, v2, v10

    const/4 v11, 0x7

    aput-byte v8, v2, v11

    const/16 v10, 0x8

    aput-byte v5, v2, v10

    const/16 v16, 0x9

    aput-byte v5, v2, v16

    aput-byte v15, v2, v14

    const/16 v17, 0xb

    aput-byte v9, v2, v17

    const/16 v10, 0xc

    aput-byte v7, v2, v10

    const/16 v10, 0xd

    aput-byte v6, v2, v10

    const/16 v10, 0xe

    aput-byte v5, v2, v10

    const/16 v10, 0xf

    aput-byte v5, v2, v10

    const/16 v10, 0x10

    aput-byte v15, v2, v10

    const/16 v10, 0x11

    aput-byte v9, v2, v10

    const/16 v10, 0x12

    aput-byte v7, v2, v10

    const/16 v10, 0x13

    aput-byte v5, v2, v10

    const/16 v10, 0x14

    aput-byte v5, v2, v10

    const/16 v10, 0x15

    aput-byte v5, v2, v10

    array-length v10, v2

    invoke-virtual {v1, v2, v5, v10}, Lcom/starmicronics/stario/h;->writePort([BII)V

    const/16 v2, 0x200

    new-array v2, v2, [B

    const/4 v10, 0x6

    new-array v11, v10, [B

    aput-byte v15, v11, v5

    aput-byte v9, v11, v3

    aput-byte v7, v11, v6

    aput-byte v5, v11, v7

    aput-byte v5, v11, v8

    const/4 v10, 0x5

    aput-byte v5, v11, v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    :goto_4
    array-length v10, v2

    invoke-direct {v1, v2, v5, v10}, Lcom/starmicronics/stario/h;->a([BII)I

    move-result v10

    const/16 v14, 0x8

    if-lt v10, v14, :cond_6

    invoke-static {v2}, Lcom/starmicronics/stario/f;->a([B)I

    move-result v10

    if-nez v10, :cond_5

    const/4 v10, 0x6

    new-array v2, v10, [B

    aput-byte v15, v2, v5

    aput-byte v9, v2, v3

    aput-byte v7, v2, v6

    aput-byte v7, v2, v7

    aput-byte v5, v2, v8

    const/16 v20, 0x5

    aput-byte v5, v2, v20

    array-length v3, v2

    invoke-virtual {v1, v2, v5, v3}, Lcom/starmicronics/stario/h;->writePort([BII)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_5
    const/4 v10, 0x6

    const/16 v20, 0x5

    :try_start_5
    array-length v0, v11

    invoke-virtual {v1, v11, v5, v0}, Lcom/starmicronics/stario/h;->writePort([BII)V

    goto :goto_5

    :cond_6
    const/4 v10, 0x6

    const/16 v20, 0x5

    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/stario/h;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v5

    iget-boolean v0, v5, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eq v0, v3, :cond_8

    const-wide/16 v16, 0x64

    :try_start_6
    invoke-static/range {v16 .. v17}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_6

    :catch_0
    move-exception v0

    move-object/from16 v16, v0

    :try_start_7
    invoke-virtual/range {v16 .. v16}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v16, v16, v12

    int-to-long v6, v4

    cmp-long v0, v16, v6

    if-gtz v0, :cond_7

    move-object v0, v5

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x3

    goto :goto_4

    :cond_7
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "There was no response of the printer within the timeout period."

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Failed because printer is offline before Print End counter is zero."

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_9
    const/16 v5, 0x8

    const/4 v6, 0x6

    const/4 v11, 0x7

    const/16 v16, 0x9

    const/16 v17, 0xb

    const/16 v20, 0x5

    :try_start_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    sub-long v24, v24, v18

    int-to-long v2, v4

    cmp-long v26, v24, v2

    if-gtz v26, :cond_a

    const-wide/16 v2, 0x64

    :try_start_9
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :catch_1
    move/from16 v21, v9

    move/from16 v23, v10

    const/16 v2, 0x16

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x2

    const/4 v7, 0x3

    const/4 v9, 0x5

    goto/16 :goto_1

    :cond_a
    :try_start_a
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v2, "There was no response of the device within the timeout period."

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :catch_2
    move-exception v0

    :try_start_b
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_3
    move-exception v0

    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_4
    move-exception v0

    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_b
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Failed because printer is offline."

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public endCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/16 v0, 0x12

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    array-length v1, v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v1}, Lcom/starmicronics/stario/h;->writePort([BII)V

    const/16 v0, 0x200

    new-array v0, v0, [B

    const/4 v1, 0x6

    new-array v1, v1, [B

    fill-array-data v1, :array_1

    iget v3, p0, Lcom/starmicronics/stario/h;->a:I

    const/16 v4, 0x2710

    if-le v3, v4, :cond_0

    iget v4, p0, Lcom/starmicronics/stario/h;->a:I

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    :goto_0
    invoke-virtual {p0}, Lcom/starmicronics/stario/h;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v3

    iget-boolean v7, v3, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    const/4 v8, 0x1

    if-eq v7, v8, :cond_6

    array-length v7, v0

    invoke-direct {p0, v0, v2, v7}, Lcom/starmicronics/stario/h;->a([BII)I

    move-result v7

    const/4 v9, 0x0

    :goto_1
    const/16 v10, 0x8

    if-lt v7, v10, :cond_4

    aget-byte v10, v0, v9

    and-int/lit16 v10, v10, 0x91

    if-ne v10, v8, :cond_1

    add-int/lit8 v10, v9, 0x1

    aget-byte v10, v0, v10

    and-int/lit16 v10, v10, 0x91

    const/16 v11, 0x80

    if-ne v10, v11, :cond_1

    add-int/lit8 v10, v9, 0x2

    aget-byte v10, v0, v10

    and-int/lit16 v10, v10, 0x91

    if-nez v10, :cond_1

    add-int/lit8 v10, v9, 0x3

    aget-byte v10, v0, v10

    and-int/lit16 v10, v10, 0x91

    if-nez v10, :cond_1

    add-int/lit8 v10, v9, 0x4

    aget-byte v10, v0, v10

    and-int/lit16 v10, v10, 0x91

    if-nez v10, :cond_1

    add-int/lit8 v10, v9, 0x5

    aget-byte v10, v0, v10

    and-int/lit16 v10, v10, 0x91

    if-nez v10, :cond_1

    add-int/lit8 v10, v9, 0x6

    aget-byte v10, v0, v10

    and-int/lit16 v10, v10, 0x91

    if-nez v10, :cond_1

    aget-byte v10, v0, v9

    and-int/lit8 v10, v10, 0x20

    shr-int/lit8 v10, v10, 0x2

    aget-byte v11, v0, v9

    and-int/lit8 v11, v11, 0xe

    shr-int/2addr v11, v8

    add-int/2addr v10, v11

    add-int/2addr v9, v10

    sub-int/2addr v7, v10

    goto :goto_1

    :cond_1
    aget-byte v10, v0, v9

    const/16 v11, 0x1b

    if-ne v10, v11, :cond_3

    add-int/lit8 v10, v9, 0x1

    aget-byte v10, v0, v10

    const/16 v11, 0x1d

    if-ne v10, v11, :cond_3

    add-int/lit8 v10, v9, 0x2

    aget-byte v10, v0, v10

    const/4 v11, 0x3

    if-ne v10, v11, :cond_3

    add-int/lit8 v10, v9, 0x3

    aget-byte v10, v0, v10

    if-nez v10, :cond_3

    add-int/lit8 v10, v9, 0x4

    aget-byte v10, v0, v10

    if-nez v10, :cond_3

    add-int/lit8 v10, v9, 0x5

    aget-byte v10, v0, v10

    if-nez v10, :cond_3

    add-int/lit8 v10, v9, 0x7

    aget-byte v10, v0, v10

    if-nez v10, :cond_3

    add-int/lit8 v9, v9, 0x6

    aget-byte v7, v0, v9

    if-ne v7, v8, :cond_2

    return-object v3

    :cond_2
    array-length v3, v1

    invoke-virtual {p0, v1, v2, v3}, Lcom/starmicronics/stario/h;->writePort([BII)V

    goto :goto_2

    :cond_3
    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v7, v7, -0x1

    goto/16 :goto_1

    :cond_4
    :goto_2
    const-wide/16 v7, 0xc8

    :try_start_0
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v5

    int-to-long v9, v4

    cmp-long v3, v7, v9

    if-gtz v3, :cond_5

    goto/16 :goto_0

    :cond_5
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "There was no response of the printer within the timeout period."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Failed because printer is offline before Print End counter is updated."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :array_0
    .array-data 1
        0x1bt
        0x1dt
        0x3t
        0x1t
        0x0t
        0x0t
        0x1bt
        0x1dt
        0x3t
        0x4t
        0x0t
        0x0t
        0x1bt
        0x1dt
        0x3t
        0x0t
        0x0t
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x1bt
        0x1dt
        0x3t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public declared-synchronized retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v1, p0

    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/starmicronics/stario/StarPrinterStatus;

    invoke-direct {v0}, Lcom/starmicronics/stario/StarPrinterStatus;-><init>()V

    iget-object v2, v1, Lcom/starmicronics/stario/h;->c:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/stario/h;->b()V

    :cond_0
    const/16 v2, 0x200

    new-array v3, v2, [B

    const/16 v4, 0xd

    new-array v4, v4, [B

    const/4 v5, 0x0

    aput-byte v5, v4, v5

    const/4 v6, 0x1

    aput-byte v5, v4, v6

    const/16 v7, 0x30

    const/4 v8, 0x2

    aput-byte v7, v4, v8

    const/4 v9, 0x3

    aput-byte v5, v4, v9

    const/16 v10, 0x23

    const/4 v11, 0x4

    aput-byte v10, v4, v11

    const/16 v12, -0x74

    const/4 v13, 0x5

    aput-byte v12, v4, v13

    const/4 v14, 0x6

    aput-byte v5, v4, v14

    const/4 v15, 0x7

    aput-byte v13, v4, v15

    const/16 v16, 0x8

    aput-byte v7, v4, v16

    const/16 v7, 0x9

    aput-byte v5, v4, v7

    const/16 v16, 0xa

    aput-byte v5, v4, v16

    const/16 v16, 0xb

    aput-byte v5, v4, v16

    const/16 v16, 0xc

    aput-byte v5, v4, v16

    new-array v7, v15, [B

    aput-byte v5, v7, v5

    aput-byte v5, v7, v6

    const/16 v17, 0x31

    aput-byte v17, v7, v8

    aput-byte v5, v7, v9

    aput-byte v10, v7, v11

    aput-byte v12, v7, v13

    aput-byte v5, v7, v14

    new-array v8, v15, [B

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    iget v11, v1, Lcom/starmicronics/stario/h;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    int-to-long v11, v11

    add-long/2addr v9, v11

    :try_start_1
    iget-object v11, v1, Lcom/starmicronics/stario/h;->l:Ljava/lang/String;

    invoke-static {v11}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v11
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    new-instance v12, Ljava/net/DatagramPacket;

    array-length v13, v4

    const/16 v14, 0x238c

    invoke-direct {v12, v4, v13, v11, v14}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :goto_0
    const/4 v4, 0x0

    :try_start_3
    new-instance v11, Ljava/net/DatagramSocket;

    invoke-direct {v11, v14}, Ljava/net/DatagramSocket;-><init>(I)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/16 v4, 0x64

    :try_start_4
    invoke-virtual {v11, v4}, Ljava/net/DatagramSocket;->setSoTimeout(I)V

    invoke-virtual {v11, v12}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    new-instance v4, Ljava/net/DatagramPacket;

    invoke-direct {v4, v3, v2}, Ljava/net/DatagramPacket;-><init>([BI)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    invoke-virtual {v11, v4}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    invoke-virtual {v4}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v4

    array-length v13, v4

    const/4 v2, 0x0

    :goto_1
    const/16 v14, 0x17

    if-lt v13, v14, :cond_2

    invoke-static {v4, v2, v8, v5, v15}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v8, v7}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v14

    if-ne v14, v6, :cond_1

    add-int/lit8 v2, v2, 0xe

    iget-object v13, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B
    :try_end_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    const/16 v14, 0x9

    :try_start_7
    invoke-static {v4, v2, v13, v5, v14}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v14, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    invoke-static {v0}, Lcom/starmicronics/stario/f;->c(Lcom/starmicronics/stario/StarPrinterStatus;)V

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarPrinterStatus;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/starmicronics/stario/StarPrinterStatus;

    iput-object v2, v1, Lcom/starmicronics/stario/h;->i:Lcom/starmicronics/stario/StarPrinterStatus;

    iget-object v0, v1, Lcom/starmicronics/stario/h;->i:Lcom/starmicronics/stario/StarPrinterStatus;
    :try_end_7
    .catch Ljava/net/SocketTimeoutException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    invoke-virtual {v11}, Ljava/net/DatagramSocket;->close()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    monitor-exit p0

    return-object v0

    :cond_1
    const/16 v14, 0x9

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v13, v13, -0x1

    goto :goto_1

    :cond_2
    const/16 v14, 0x9

    :try_start_9
    invoke-virtual {v11}, Ljava/net/DatagramSocket;->close()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_3

    :catchall_0
    move-exception v0

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_a
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :goto_2
    :try_start_b
    invoke-virtual {v11}, Ljava/net/DatagramSocket;->close()V

    throw v0

    :catch_1
    const/16 v14, 0x9

    :catch_2
    invoke-virtual {v11}, Ljava/net/DatagramSocket;->close()V

    :goto_3
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    const/16 v4, 0x12c

    :try_start_c
    invoke-virtual {v2, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/2addr v2, v4

    int-to-long v5, v2

    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_c
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :catch_3
    :try_start_d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v2, v9, v4

    if-lez v2, :cond_3

    const/16 v2, 0x200

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/16 v14, 0x238c

    goto/16 :goto_0

    :cond_3
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Couldn\'t get printer status because device is busy."

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_4
    move-exception v0

    move-object v4, v11

    goto :goto_4

    :catch_5
    move-exception v0

    :goto_4
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/net/DatagramSocket;->close()V

    :cond_4
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_6
    move-exception v0

    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
