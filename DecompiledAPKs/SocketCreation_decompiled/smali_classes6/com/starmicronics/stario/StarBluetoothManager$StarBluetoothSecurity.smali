.class public final enum Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/stario/StarBluetoothManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StarBluetoothSecurity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

.field public static final enum DISABLE:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

.field public static final enum PINCODE:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

.field public static final enum SSP:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    const/4 v1, 0x0

    const-string v2, "DISABLE"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->DISABLE:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    new-instance v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    const/4 v2, 0x1

    const-string v3, "SSP"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->SSP:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    new-instance v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    const/4 v3, 0x2

    const-string v4, "PINCODE"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->PINCODE:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    sget-object v4, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->DISABLE:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    aput-object v4, v0, v1

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->SSP:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->PINCODE:Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    aput-object v1, v0, v3

    sput-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->$VALUES:[Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;
    .locals 1

    const-class v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;
    .locals 1

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->$VALUES:[Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    invoke-virtual {v0}, [Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/stario/StarBluetoothManager$StarBluetoothSecurity;

    return-object v0
.end method
