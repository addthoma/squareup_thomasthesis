.class Lcom/starmicronics/starioextension/bd;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;",
            "I)V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/bd$1;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/bd$1;-><init>()V

    div-int/lit8 p2, p2, 0xa

    const/4 v1, 0x1

    if-ge p2, v1, :cond_0

    const/4 p2, 0x1

    :cond_0
    const/16 v2, 0x7f

    if-le p2, v2, :cond_1

    const/16 p2, 0x7f

    :cond_1
    const/4 v2, 0x5

    new-array v2, v2, [B

    const/4 v3, 0x0

    const/16 v4, 0x1b

    aput-byte v4, v2, v3

    const/4 v3, 0x7

    aput-byte v3, v2, v1

    const/4 v1, 0x2

    int-to-byte p2, p2

    aput-byte p2, v2, v1

    const/4 v1, 0x3

    aput-byte p2, v2, v1

    const/4 p2, 0x4

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    aput-byte p1, v2, p2

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;",
            "I)V"
        }
    .end annotation

    new-instance p2, Lcom/starmicronics/starioextension/bd$2;

    invoke-direct {p2}, Lcom/starmicronics/starioextension/bd$2;-><init>()V

    const/4 v0, 0x6

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x1b

    aput-byte v2, v0, v1

    const/4 v2, 0x1

    const/16 v3, 0x2a

    aput-byte v3, v0, v2

    const/4 v2, 0x2

    const/16 v3, 0x72

    aput-byte v3, v0, v2

    const/4 v2, 0x3

    const/16 v3, 0x44

    aput-byte v3, v0, v2

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    const/4 p2, 0x4

    aput-byte p1, v0, p2

    const/4 p1, 0x5

    aput-byte v1, v0, p1

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;I)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;",
            "I)V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/bd$3;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/bd$3;-><init>()V

    const/4 v1, 0x2

    div-int/2addr p2, v1

    const/4 v2, 0x1

    if-ge p2, v2, :cond_0

    const/4 p2, 0x1

    :cond_0
    const/16 v3, 0xff

    if-le p2, v3, :cond_1

    const/16 p2, 0xff

    :cond_1
    const/4 v3, 0x5

    new-array v3, v3, [B

    const/4 v4, 0x0

    const/16 v5, 0x1b

    aput-byte v5, v3, v4

    const/16 v4, 0x70

    aput-byte v4, v3, v2

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    aput-byte p1, v3, v1

    const/4 p1, 0x3

    int-to-byte p2, p2

    aput-byte p2, v3, p1

    const/4 p1, 0x4

    aput-byte p2, v3, p1

    invoke-interface {p0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static d(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$PeripheralChannel;",
            "I)V"
        }
    .end annotation

    return-void
.end method
