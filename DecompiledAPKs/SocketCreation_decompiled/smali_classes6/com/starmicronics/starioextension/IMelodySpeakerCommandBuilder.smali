.class public interface abstract Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starioextension/IMelodySpeakerCommandBuilder$SoundStorageArea;
    }
.end annotation


# virtual methods
.method public abstract appendSound(Lcom/starmicronics/starioextension/SoundSetting;)V
.end method

.method public abstract appendSoundData([BLcom/starmicronics/starioextension/SoundSetting;)V
.end method

.method public abstract getCommands()[B
.end method
