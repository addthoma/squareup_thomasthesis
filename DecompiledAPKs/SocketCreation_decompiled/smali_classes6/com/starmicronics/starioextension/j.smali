.class Lcom/starmicronics/starioextension/j;
.super Ljava/lang/Object;


# instance fields
.field private a:[[I

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;ZIZLcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 14

    move-object v0, p0

    move-object v1, p1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v2, Lcom/starmicronics/starioextension/j$1;->a:[I

    invoke-virtual/range {p5 .. p5}, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eq v2, v5, :cond_2

    if-eq v2, v3, :cond_2

    if-gtz p3, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    goto :goto_0

    :cond_0
    move/from16 v2, p3

    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    if-eqz p4, :cond_1

    mul-int v6, v6, v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    div-int/2addr v6, v7

    :cond_1
    invoke-static {p1, v2, v6, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_2

    :cond_2
    if-gtz p3, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    goto :goto_1

    :cond_3
    move/from16 v2, p3

    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    if-eqz p4, :cond_4

    mul-int v6, v6, v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    div-int/2addr v6, v7

    :cond_4
    invoke-static {p1, v6, v2, v4}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    :goto_2
    sget-object v2, Lcom/starmicronics/starioextension/j$1;->a:[I

    invoke-virtual/range {p5 .. p5}, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->ordinal()I

    move-result v6

    aget v2, v2, v6

    if-eq v2, v5, :cond_5

    if-eq v2, v3, :cond_5

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iput v2, v0, Lcom/starmicronics/starioextension/j;->b:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    :goto_3
    iput v2, v0, Lcom/starmicronics/starioextension/j;->c:I

    goto :goto_4

    :cond_5
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iput v2, v0, Lcom/starmicronics/starioextension/j;->b:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    goto :goto_3

    :goto_4
    iget v2, v0, Lcom/starmicronics/starioextension/j;->c:I

    add-int/lit8 v2, v2, 0x7

    iget v6, v0, Lcom/starmicronics/starioextension/j;->b:I

    add-int/lit8 v6, v6, 0x7

    filled-new-array {v2, v6}, [I

    move-result-object v2

    const-class v6, I

    invoke-static {v6, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[I

    iput-object v2, v0, Lcom/starmicronics/starioextension/j;->a:[[I

    iget v2, v0, Lcom/starmicronics/starioextension/j;->c:I

    iget v6, v0, Lcom/starmicronics/starioextension/j;->b:I

    mul-int v2, v2, v6

    new-array v2, v2, [I

    const/4 v8, 0x0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    move-object v6, v1

    move-object v7, v2

    invoke-virtual/range {v6 .. v13}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    sget-object v6, Lcom/starmicronics/starioextension/j$1;->a:[I

    invoke-virtual/range {p5 .. p5}, Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;->ordinal()I

    move-result v7

    aget v6, v6, v7

    if-eq v6, v5, :cond_b

    if-eq v6, v3, :cond_9

    const/4 v3, 0x3

    if-eq v6, v3, :cond_7

    const/4 v1, 0x0

    :goto_5
    iget v3, v0, Lcom/starmicronics/starioextension/j;->b:I

    if-ge v1, v3, :cond_d

    const/4 v3, 0x0

    :goto_6
    iget v5, v0, Lcom/starmicronics/starioextension/j;->c:I

    if-ge v3, v5, :cond_6

    iget-object v6, v0, Lcom/starmicronics/starioextension/j;->a:[[I

    aget-object v6, v6, v3

    mul-int v5, v5, v1

    add-int/2addr v5, v3

    aget v5, v2, v5

    aput v5, v6, v1

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_7
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    mul-int v3, v3, v1

    sub-int/2addr v3, v5

    const/4 v1, 0x0

    :goto_7
    iget v5, v0, Lcom/starmicronics/starioextension/j;->b:I

    if-ge v1, v5, :cond_d

    move v5, v3

    const/4 v3, 0x0

    :goto_8
    iget v6, v0, Lcom/starmicronics/starioextension/j;->c:I

    if-ge v3, v6, :cond_8

    iget-object v6, v0, Lcom/starmicronics/starioextension/j;->a:[[I

    aget-object v6, v6, v3

    add-int/lit8 v7, v5, -0x1

    aget v5, v2, v5

    aput v5, v6, v1

    add-int/lit8 v3, v3, 0x1

    move v5, v7

    goto :goto_8

    :cond_8
    add-int/lit8 v1, v1, 0x1

    move v3, v5

    goto :goto_7

    :cond_9
    const/4 v3, 0x0

    :goto_9
    iget v6, v0, Lcom/starmicronics/starioextension/j;->b:I

    if-ge v3, v6, :cond_d

    const/4 v6, 0x0

    :goto_a
    iget v7, v0, Lcom/starmicronics/starioextension/j;->c:I

    if-ge v6, v7, :cond_a

    iget-object v7, v0, Lcom/starmicronics/starioextension/j;->a:[[I

    aget-object v7, v7, v6

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    mul-int v8, v8, v6

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    sub-int/2addr v9, v5

    sub-int/2addr v9, v3

    add-int/2addr v8, v9

    aget v8, v2, v8

    aput v8, v7, v3

    add-int/lit8 v6, v6, 0x1

    goto :goto_a

    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_b
    const/4 v3, 0x0

    :goto_b
    iget v6, v0, Lcom/starmicronics/starioextension/j;->b:I

    if-ge v3, v6, :cond_d

    const/4 v6, 0x0

    :goto_c
    iget v7, v0, Lcom/starmicronics/starioextension/j;->c:I

    if-ge v6, v7, :cond_c

    iget-object v7, v0, Lcom/starmicronics/starioextension/j;->a:[[I

    aget-object v7, v7, v6

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    sub-int/2addr v8, v5

    sub-int/2addr v8, v6

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    mul-int v8, v8, v9

    add-int/2addr v8, v3

    aget v8, v2, v8

    aput v8, v7, v3

    add-int/lit8 v6, v6, 0x1

    goto :goto_c

    :cond_c
    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_d
    if-eqz p2, :cond_e

    invoke-direct {p0}, Lcom/starmicronics/starioextension/j;->c()V

    :cond_e
    return-void
.end method

.method private b(II)I
    .locals 3

    iget-object v0, p0, Lcom/starmicronics/starioextension/j;->a:[[I

    aget-object p1, v0, p1

    aget p1, p1, p2

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result p2

    const/16 v0, 0xff

    if-nez p2, :cond_0

    return v0

    :cond_0
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result p2

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    add-int/2addr p2, v1

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result p1

    add-int/2addr p2, p1

    int-to-double p1, p2

    const-wide/high16 v1, 0x4008000000000000L    # 3.0

    div-double/2addr p1, v1

    double-to-float p1, p1

    const/high16 p2, 0x3fc00000    # 1.5f

    mul-float p1, p1, p2

    float-to-int p1, p1

    if-le p1, v0, :cond_1

    const/16 p1, 0xff

    :cond_1
    return p1
.end method

.method private c()V
    .locals 12

    iget v0, p0, Lcom/starmicronics/starioextension/j;->c:I

    iget v1, p0, Lcom/starmicronics/starioextension/j;->b:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, I

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    iget v3, p0, Lcom/starmicronics/starioextension/j;->b:I

    if-ge v2, v3, :cond_a

    and-int/lit8 v3, v2, 0x1

    const/high16 v4, -0x1000000

    const/4 v5, -0x1

    const/16 v6, 0xff

    if-nez v3, :cond_4

    const/4 v3, 0x0

    :goto_1
    iget v7, p0, Lcom/starmicronics/starioextension/j;->c:I

    if-ge v3, v7, :cond_9

    aget-object v7, v0, v3

    aget v8, v7, v2

    invoke-direct {p0, v3, v2}, Lcom/starmicronics/starioextension/j;->b(II)I

    move-result v9

    rsub-int v9, v9, 0xff

    add-int/2addr v8, v9

    aput v8, v7, v2

    aget-object v7, v0, v3

    aget v7, v7, v2

    if-lt v7, v6, :cond_0

    iget-object v7, p0, Lcom/starmicronics/starioextension/j;->a:[[I

    aget-object v7, v7, v3

    aput v4, v7, v2

    aget-object v7, v0, v3

    aget v8, v7, v2

    sub-int/2addr v8, v6

    aput v8, v7, v2

    goto :goto_2

    :cond_0
    iget-object v7, p0, Lcom/starmicronics/starioextension/j;->a:[[I

    aget-object v7, v7, v3

    aput v5, v7, v2

    :goto_2
    aget-object v7, v0, v3

    aget v7, v7, v2

    div-int/lit8 v7, v7, 0x10

    iget v8, p0, Lcom/starmicronics/starioextension/j;->c:I

    add-int/lit8 v8, v8, -0x1

    if-ge v3, v8, :cond_1

    add-int/lit8 v8, v3, 0x1

    aget-object v8, v0, v8

    aget v9, v8, v2

    mul-int/lit8 v10, v7, 0x7

    add-int/2addr v9, v10

    aput v9, v8, v2

    :cond_1
    iget v8, p0, Lcom/starmicronics/starioextension/j;->b:I

    add-int/lit8 v8, v8, -0x1

    if-ge v2, v8, :cond_3

    aget-object v8, v0, v3

    add-int/lit8 v9, v2, 0x1

    aget v10, v8, v9

    mul-int/lit8 v11, v7, 0x5

    add-int/2addr v10, v11

    aput v10, v8, v9

    if-lez v3, :cond_2

    add-int/lit8 v8, v3, -0x1

    aget-object v8, v0, v8

    aget v10, v8, v9

    mul-int/lit8 v11, v7, 0x3

    add-int/2addr v10, v11

    aput v10, v8, v9

    :cond_2
    iget v8, p0, Lcom/starmicronics/starioextension/j;->c:I

    add-int/lit8 v8, v8, -0x1

    if-ge v3, v8, :cond_3

    add-int/lit8 v8, v3, 0x1

    aget-object v8, v0, v8

    aget v10, v8, v9

    mul-int/lit8 v7, v7, 0x1

    add-int/2addr v10, v7

    aput v10, v8, v9

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    iget v3, p0, Lcom/starmicronics/starioextension/j;->c:I

    add-int/lit8 v3, v3, -0x1

    :goto_3
    if-ltz v3, :cond_9

    aget-object v7, v0, v3

    aget v8, v7, v2

    invoke-direct {p0, v3, v2}, Lcom/starmicronics/starioextension/j;->b(II)I

    move-result v9

    rsub-int v9, v9, 0xff

    add-int/2addr v8, v9

    aput v8, v7, v2

    aget-object v7, v0, v3

    aget v7, v7, v2

    if-lt v7, v6, :cond_5

    iget-object v7, p0, Lcom/starmicronics/starioextension/j;->a:[[I

    aget-object v7, v7, v3

    aput v4, v7, v2

    aget-object v7, v0, v3

    aget v8, v7, v2

    sub-int/2addr v8, v6

    aput v8, v7, v2

    goto :goto_4

    :cond_5
    iget-object v7, p0, Lcom/starmicronics/starioextension/j;->a:[[I

    aget-object v7, v7, v3

    aput v5, v7, v2

    :goto_4
    aget-object v7, v0, v3

    aget v7, v7, v2

    div-int/lit8 v7, v7, 0x10

    if-lez v3, :cond_6

    add-int/lit8 v8, v3, -0x1

    aget-object v8, v0, v8

    aget v9, v8, v2

    mul-int/lit8 v10, v7, 0x7

    add-int/2addr v9, v10

    aput v9, v8, v2

    :cond_6
    iget v8, p0, Lcom/starmicronics/starioextension/j;->b:I

    add-int/lit8 v8, v8, -0x1

    if-ge v2, v8, :cond_8

    aget-object v8, v0, v3

    add-int/lit8 v9, v2, 0x1

    aget v10, v8, v9

    mul-int/lit8 v11, v7, 0x5

    add-int/2addr v10, v11

    aput v10, v8, v9

    iget v8, p0, Lcom/starmicronics/starioextension/j;->c:I

    add-int/lit8 v8, v8, -0x1

    if-ge v3, v8, :cond_7

    add-int/lit8 v8, v3, 0x1

    aget-object v8, v0, v8

    aget v10, v8, v9

    mul-int/lit8 v11, v7, 0x3

    add-int/2addr v10, v11

    aput v10, v8, v9

    :cond_7
    if-lez v3, :cond_8

    add-int/lit8 v8, v3, -0x1

    aget-object v8, v0, v8

    aget v10, v8, v9

    mul-int/lit8 v7, v7, 0x1

    add-int/2addr v10, v7

    aput v10, v8, v9

    :cond_8
    add-int/lit8 v3, v3, -0x1

    goto :goto_3

    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_a
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/starmicronics/starioextension/j;->b:I

    return v0
.end method

.method a(II)Z
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starioextension/j;->a:[[I

    aget-object p1, v0, p1

    aget p1, p1, p2

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result p2

    const/4 v0, 0x0

    if-nez p2, :cond_0

    return v0

    :cond_0
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result p2

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    add-int/2addr p2, v1

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result p1

    add-int/2addr p2, p1

    div-int/lit8 p2, p2, 0x3

    const/16 p1, 0x7f

    if-ge p2, p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/starmicronics/starioextension/j;->c:I

    return v0
.end method
