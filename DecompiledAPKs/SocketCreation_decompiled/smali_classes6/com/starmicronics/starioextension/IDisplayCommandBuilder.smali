.class public interface abstract Lcom/starmicronics/starioextension/IDisplayCommandBuilder;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;,
        Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CursorMode;,
        Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;,
        Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;
    }
.end annotation


# virtual methods
.method public abstract append(B)V
.end method

.method public abstract append([B)V
.end method

.method public abstract appendBackSpace()V
.end method

.method public abstract appendBitmap(Landroid/graphics/Bitmap;Z)V
.end method

.method public abstract appendCarriageReturn()V
.end method

.method public abstract appendClearScreen()V
.end method

.method public abstract appendCodePage(Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CodePageType;)V
.end method

.method public abstract appendContrastMode(Lcom/starmicronics/starioextension/IDisplayCommandBuilder$ContrastMode;)V
.end method

.method public abstract appendCursorMode(Lcom/starmicronics/starioextension/IDisplayCommandBuilder$CursorMode;)V
.end method

.method public abstract appendDeleteToEndOfLine()V
.end method

.method public abstract appendHomePosition()V
.end method

.method public abstract appendHorizontalTab()V
.end method

.method public abstract appendInternational(Lcom/starmicronics/starioextension/IDisplayCommandBuilder$InternationalType;)V
.end method

.method public abstract appendLineFeed()V
.end method

.method public abstract appendSpecifiedPosition(II)V
.end method

.method public abstract appendTurnOn(Z)V
.end method

.method public abstract appendUserDefinedCharacter(II[B)V
.end method

.method public abstract appendUserDefinedDbcsCharacter(II[B)V
.end method

.method public abstract getCommands()[B
.end method

.method public abstract getPassThroughCommands()[B
.end method
