.class Lcom/starmicronics/starioextension/ay;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;II)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;II)V"
        }
    .end annotation

    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    const/4 p1, 0x1

    :cond_0
    const/4 v1, 0x6

    if-le p1, v1, :cond_1

    const/4 p1, 0x6

    :cond_1
    if-ge p2, v0, :cond_2

    const/4 p2, 0x1

    :cond_2
    if-le p2, v1, :cond_3

    const/4 p2, 0x6

    :cond_3
    add-int/lit8 p1, p1, -0x1

    add-int/lit8 p2, p2, -0x1

    new-array v1, v1, [B

    const/4 v2, 0x0

    const/16 v3, 0x1b

    aput-byte v3, v1, v2

    const/16 v2, 0x57

    aput-byte v2, v1, v0

    const/4 v0, 0x2

    int-to-byte p1, p1

    aput-byte p1, v1, v0

    const/4 p1, 0x3

    aput-byte v3, v1, p1

    const/4 p1, 0x4

    const/16 v0, 0x68

    aput-byte v0, v1, p1

    const/4 p1, 0x5

    int-to-byte p2, p2

    aput-byte p2, v1, p1

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static b(Ljava/util/List;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;II)V"
        }
    .end annotation

    return-void
.end method

.method static c(Ljava/util/List;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;II)V"
        }
    .end annotation

    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    const/4 p1, 0x1

    :cond_0
    const/16 v1, 0x8

    if-le p1, v1, :cond_1

    const/16 p1, 0x8

    :cond_1
    if-ge p2, v0, :cond_2

    const/4 p2, 0x1

    :cond_2
    if-le p2, v1, :cond_3

    const/16 p2, 0x8

    :cond_3
    add-int/lit8 p1, p1, -0x1

    add-int/lit8 p2, p2, -0x1

    shl-int/lit8 p1, p1, 0x4

    or-int/2addr p1, p2

    int-to-byte p1, p1

    const/4 p2, 0x3

    new-array p2, p2, [B

    const/4 v1, 0x0

    const/16 v2, 0x1d

    aput-byte v2, p2, v1

    const/16 v1, 0x21

    aput-byte v1, p2, v0

    const/4 v0, 0x2

    aput-byte p1, p2, v0

    invoke-interface {p0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static d(Ljava/util/List;II)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;II)V"
        }
    .end annotation

    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    const/4 p1, 0x1

    :cond_0
    const/4 v1, 0x2

    if-le p1, v1, :cond_1

    const/4 p1, 0x2

    :cond_1
    if-ge p2, v0, :cond_2

    const/4 p2, 0x1

    :cond_2
    if-le p2, v1, :cond_3

    const/4 p2, 0x2

    :cond_3
    add-int/lit8 p1, p1, -0x1

    add-int/lit8 p2, p2, -0x1

    const/4 v2, 0x6

    new-array v2, v2, [B

    const/16 v3, 0x1b

    const/4 v4, 0x0

    aput-byte v3, v2, v4

    const/16 v5, 0x57

    aput-byte v5, v2, v0

    int-to-byte p1, p1

    aput-byte p1, v2, v1

    const/4 p1, 0x3

    aput-byte v3, v2, p1

    const/4 v5, 0x4

    const/16 v6, 0x68

    aput-byte v6, v2, v5

    const/4 v5, 0x5

    int-to-byte v6, p2

    aput-byte v6, v2, v5

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-nez p2, :cond_4

    const/4 p2, 0x1

    goto :goto_0

    :cond_4
    const/4 p2, 0x0

    :goto_0
    new-array p1, p1, [B

    aput-byte v3, p1, v4

    const/16 v2, 0x78

    aput-byte v2, p1, v0

    int-to-byte p2, p2

    aput-byte p2, p1, v1

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
