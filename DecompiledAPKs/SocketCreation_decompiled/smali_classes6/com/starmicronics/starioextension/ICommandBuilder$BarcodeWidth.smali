.class public final enum Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/ICommandBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BarcodeWidth"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

.field public static final enum Mode1:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

.field public static final enum Mode2:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

.field public static final enum Mode3:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

.field public static final enum Mode4:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

.field public static final enum Mode5:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

.field public static final enum Mode6:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

.field public static final enum Mode7:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

.field public static final enum Mode8:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

.field public static final enum Mode9:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v1, 0x0

    const-string v2, "Mode1"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode1:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v2, 0x1

    const-string v3, "Mode2"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode2:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v3, 0x2

    const-string v4, "Mode3"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode3:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v4, 0x3

    const-string v5, "Mode4"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode4:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v5, 0x4

    const-string v6, "Mode5"

    invoke-direct {v0, v6, v5}, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode5:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v6, 0x5

    const-string v7, "Mode6"

    invoke-direct {v0, v7, v6}, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode6:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v7, 0x6

    const-string v8, "Mode7"

    invoke-direct {v0, v8, v7}, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode7:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/4 v8, 0x7

    const-string v9, "Mode8"

    invoke-direct {v0, v9, v8}, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode8:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/16 v9, 0x8

    const-string v10, "Mode9"

    invoke-direct {v0, v10, v9}, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode9:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    sget-object v10, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode1:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    aput-object v10, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode2:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode3:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode4:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    aput-object v1, v0, v4

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode5:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    aput-object v1, v0, v5

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode6:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    aput-object v1, v0, v6

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode7:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    aput-object v1, v0, v7

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode8:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    aput-object v1, v0, v8

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->Mode9:Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    aput-object v1, v0, v9

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->$VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->$VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;

    return-object v0
.end method
