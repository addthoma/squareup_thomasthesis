.class Lcom/starmicronics/starioextension/m;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;Ljava/lang/Integer;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    if-gez v0, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x7

    if-le v0, v2, :cond_1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v3, 0x2

    mul-int/lit8 p1, p1, 0x2

    const/16 v4, 0xb

    new-array v4, v4, [B

    const/16 v5, 0x1b

    aput-byte v5, v4, v1

    const/4 v6, 0x1

    const/16 v7, 0x20

    aput-byte v7, v4, v6

    int-to-byte v0, v0

    aput-byte v0, v4, v3

    const/4 v3, 0x3

    aput-byte v5, v4, v3

    const/4 v3, 0x4

    const/16 v6, 0x73

    aput-byte v6, v4, v3

    const/4 v3, 0x5

    aput-byte v1, v4, v3

    const/4 v3, 0x6

    int-to-byte p1, p1

    aput-byte p1, v4, v3

    aput-byte v5, v4, v2

    const/16 p1, 0x8

    const/16 v2, 0x74

    aput-byte v2, v4, p1

    const/16 p1, 0x9

    aput-byte v1, v4, p1

    const/16 p1, 0xa

    aput-byte v0, v4, p1

    invoke-interface {p0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static b(Ljava/util/List;Ljava/lang/Integer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method static c(Ljava/util/List;Ljava/lang/Integer;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    if-gez v0, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x7

    if-le v0, v2, :cond_1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v3, 0x2

    mul-int/lit8 p1, p1, 0x2

    new-array v2, v2, [B

    const/16 v4, 0x1b

    aput-byte v4, v2, v1

    const/4 v4, 0x1

    const/16 v5, 0x20

    aput-byte v5, v2, v4

    int-to-byte v0, v0

    aput-byte v0, v2, v3

    const/4 v0, 0x3

    const/16 v3, 0x1c

    aput-byte v3, v2, v0

    const/4 v0, 0x4

    const/16 v3, 0x53

    aput-byte v3, v2, v0

    const/4 v0, 0x5

    aput-byte v1, v2, v0

    const/4 v0, 0x6

    int-to-byte p1, p1

    aput-byte p1, v2, v0

    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static d(Ljava/util/List;Ljava/lang/Integer;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    if-gez v0, :cond_0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x7

    if-le v0, v2, :cond_1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v3, 0x2

    mul-int/lit8 p1, p1, 0x2

    const/4 v4, 0x4

    add-int/2addr p1, v4

    const/16 v5, 0xb

    new-array v5, v5, [B

    const/16 v6, 0x1b

    aput-byte v6, v5, v1

    const/4 v7, 0x1

    const/16 v8, 0x20

    aput-byte v8, v5, v7

    int-to-byte v0, v0

    aput-byte v0, v5, v3

    const/4 v0, 0x3

    aput-byte v6, v5, v0

    const/16 v0, 0x73

    aput-byte v0, v5, v4

    const/4 v0, 0x5

    aput-byte v1, v5, v0

    const/4 v0, 0x6

    int-to-byte v4, p1

    aput-byte v4, v5, v0

    aput-byte v6, v5, v2

    const/16 v0, 0x8

    const/16 v2, 0x74

    aput-byte v2, v5, v0

    const/16 v0, 0x9

    aput-byte v1, v5, v0

    const/16 v0, 0xa

    div-int/2addr p1, v3

    int-to-byte p1, p1

    aput-byte p1, v5, v0

    invoke-interface {p0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
