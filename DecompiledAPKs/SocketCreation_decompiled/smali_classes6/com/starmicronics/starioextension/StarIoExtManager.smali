.class public Lcom/starmicronics/starioextension/StarIoExtManager;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starioextension/StarIoExtManager$e;,
        Lcom/starmicronics/starioextension/StarIoExtManager$f;,
        Lcom/starmicronics/starioextension/StarIoExtManager$d;,
        Lcom/starmicronics/starioextension/StarIoExtManager$c;,
        Lcom/starmicronics/starioextension/StarIoExtManager$b;,
        Lcom/starmicronics/starioextension/StarIoExtManager$a;,
        Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;,
        Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;,
        Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;,
        Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;,
        Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;,
        Lcom/starmicronics/starioextension/StarIoExtManager$Type;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String; = "tcp:"

.field private static final b:Ljava/lang/String; = "bt:"

.field private static final c:Ljava/lang/String; = "usb:"

.field private static final d:Ljava/lang/String; = "usb:sn:"

.field private static final e:Ljava/lang/String; = "^usb:.+\\-.+"

.field private static final f:I = 0x519


# instance fields
.field private A:Landroid/content/BroadcastReceiver;

.field private B:Lcom/starmicronics/starioextension/u;

.field private g:Lcom/starmicronics/starioextension/ai;

.field private h:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

.field private i:Lcom/starmicronics/stario/StarIOPort;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Lcom/starmicronics/starioextension/bs;

.field private n:Landroid/content/Context;

.field private o:Z

.field private p:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

.field private q:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

.field private r:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

.field private s:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

.field private t:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

.field private u:Landroid/os/Handler;

.field private v:Z

.field private w:Lcom/starmicronics/starioextension/StarIoExtManager$d;

.field private x:Lcom/starmicronics/starioextension/StarIoExtManager$f;

.field private y:Lcom/starmicronics/starioextension/StarIoExtManager$e;

.field private z:Lcom/starmicronics/starioextension/StarIoExtManager$b;


# direct methods
.method public constructor <init>(Lcom/starmicronics/starioextension/StarIoExtManager$Type;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->g:Lcom/starmicronics/starioextension/ai;

    iput-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->i:Lcom/starmicronics/stario/StarIOPort;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->o:Z

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;->Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    iput-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->p:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    iput-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->q:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;->Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    iput-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->r:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    iput-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->s:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;->Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    iput-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->t:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->u:Landroid/os/Handler;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->v:Z

    iput-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->w:Lcom/starmicronics/starioextension/StarIoExtManager$d;

    iput-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->x:Lcom/starmicronics/starioextension/StarIoExtManager$f;

    iput-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->y:Lcom/starmicronics/starioextension/StarIoExtManager$e;

    iput-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->z:Lcom/starmicronics/starioextension/StarIoExtManager$b;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$3;

    invoke-direct {v0, p0}, Lcom/starmicronics/starioextension/StarIoExtManager$3;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;)V

    iput-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->A:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/starmicronics/starioextension/u;

    new-instance v1, Lcom/starmicronics/starioextension/StarIoExtManager$4;

    invoke-direct {v1, p0}, Lcom/starmicronics/starioextension/StarIoExtManager$4;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;)V

    invoke-direct {v0, v1}, Lcom/starmicronics/starioextension/u;-><init>(Lcom/starmicronics/starioextension/ConnectionCallback;)V

    iput-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->B:Lcom/starmicronics/starioextension/u;

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->h:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    iput-object p2, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->j:Ljava/lang/String;

    iput-object p3, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->k:Ljava/lang/String;

    iput p4, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->l:I

    iput-object p5, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->n:Landroid/content/Context;

    return-void
.end method

.method static synthetic a(Lcom/starmicronics/starioextension/StarIoExtManager;Z)I
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Z)I

    move-result p0

    return p0
.end method

.method private a(Z)I
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->i:Lcom/starmicronics/stario/StarIOPort;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v1

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iget-object v2, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->j:Ljava/lang/String;

    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->k:Ljava/lang/String;

    iget v4, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->l:I

    iget-object v5, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->n:Landroid/content/Context;

    invoke-static {v2, v3, v4, v5}, Lcom/starmicronics/stario/StarIOPort;->getPort(Ljava/lang/String;Ljava/lang/String;ILandroid/content/Context;)Lcom/starmicronics/stario/StarIOPort;

    move-result-object v2

    iput-object v2, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->i:Lcom/starmicronics/stario/StarIOPort;

    iget-object v2, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->i:Lcom/starmicronics/stario/StarIOPort;

    invoke-virtual {v2}, Lcom/starmicronics/stario/StarIOPort;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v2

    iget v2, v2, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->h:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    sget-object v3, Lcom/starmicronics/starioextension/StarIoExtManager$Type;->WithBarcodeReader:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->h:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    sget-object v3, Lcom/starmicronics/starioextension/StarIoExtManager$Type;->OnlyBarcodeReader:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    if-ne v2, v3, :cond_2

    :cond_1
    const/4 v2, 0x4

    new-array v2, v2, [B

    const/16 v3, 0x1b

    aput-byte v3, v2, v1

    const/4 v3, 0x1

    const/16 v4, 0x1d

    aput-byte v4, v2, v3

    const/4 v3, 0x2

    const/16 v4, 0x42

    aput-byte v4, v2, v3

    const/4 v3, 0x3

    const/16 v4, 0x33

    aput-byte v4, v2, v3

    iget-object v3, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->i:Lcom/starmicronics/stario/StarIOPort;

    array-length v4, v2

    invoke-virtual {v3, v2, v1, v4}, Lcom/starmicronics/stario/StarIOPort;->writePort([BII)V
    :try_end_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->j:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "bt:"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->j:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "usb:"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_4

    :cond_3
    new-instance p1, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    sget-object v2, Lcom/starmicronics/starioextension/StarIoExtManager$a;->b:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {p1, p0, v2}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v2, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->u:Landroid/os/Handler;

    invoke-virtual {v2, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_4
    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->h:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    sget-object v2, Lcom/starmicronics/starioextension/StarIoExtManager$Type;->Standard:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    if-eq p1, v2, :cond_5

    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->h:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    sget-object v2, Lcom/starmicronics/starioextension/StarIoExtManager$Type;->WithBarcodeReader:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    if-ne p1, v2, :cond_7

    :cond_5
    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->x:Lcom/starmicronics/starioextension/StarIoExtManager$f;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/StarIoExtManager$f;->a()Z

    move-result p1

    if-nez p1, :cond_7

    :cond_6
    new-instance p1, Lcom/starmicronics/starioextension/StarIoExtManager$f;

    invoke-direct {p1, p0, v0}, Lcom/starmicronics/starioextension/StarIoExtManager$f;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$1;)V

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->x:Lcom/starmicronics/starioextension/StarIoExtManager$f;

    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->x:Lcom/starmicronics/starioextension/StarIoExtManager$f;

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/StarIoExtManager$f;->start()V

    :cond_7
    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->h:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    sget-object v2, Lcom/starmicronics/starioextension/StarIoExtManager$Type;->WithBarcodeReader:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    if-eq p1, v2, :cond_8

    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->h:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    sget-object v2, Lcom/starmicronics/starioextension/StarIoExtManager$Type;->OnlyBarcodeReader:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    if-ne p1, v2, :cond_a

    :cond_8
    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->y:Lcom/starmicronics/starioextension/StarIoExtManager$e;

    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a()Z

    move-result p1

    if-nez p1, :cond_a

    :cond_9
    new-instance p1, Lcom/starmicronics/starioextension/StarIoExtManager$e;

    invoke-direct {p1, p0, v0}, Lcom/starmicronics/starioextension/StarIoExtManager$e;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$1;)V

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->y:Lcom/starmicronics/starioextension/StarIoExtManager$e;

    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->y:Lcom/starmicronics/starioextension/StarIoExtManager$e;

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/StarIoExtManager$e;->start()V

    :cond_a
    new-instance p1, Lcom/starmicronics/starioextension/bs;

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->j:Ljava/lang/String;

    invoke-direct {p1, v0}, Lcom/starmicronics/starioextension/bs;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->m:Lcom/starmicronics/starioextension/bs;

    return v1

    :cond_b
    :try_start_3
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Status Length is 0."

    invoke-direct {v1, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catch_0
    move-exception v1

    :try_start_4
    iget-object v2, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->i:Lcom/starmicronics/stario/StarIOPort;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v2, :cond_c

    :try_start_5
    iget-object v2, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->i:Lcom/starmicronics/stario/StarIOPort;

    invoke-static {v2}, Lcom/starmicronics/stario/StarIOPort;->releasePort(Lcom/starmicronics/stario/StarIOPort;)V
    :try_end_5
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catch_1
    :try_start_6
    iput-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->i:Lcom/starmicronics/stario/StarIOPort;

    :cond_c
    if-eqz p1, :cond_e

    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->j:Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "bt:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_d

    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->j:Ljava/lang/String;

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "usb:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_e

    :cond_d
    new-instance p1, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->c:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {p1, p0, v0}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->u:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_e
    instance-of p1, v1, Lcom/starmicronics/stario/StarConnectionRejectedException;

    if-eqz p1, :cond_f

    const/16 p1, -0x64

    monitor-exit p0

    return p1

    :cond_f
    const/4 p1, -0x1

    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw p1
.end method

.method static synthetic a(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/os/Handler;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->u:Landroid/os/Handler;

    return-object p0
.end method

.method static synthetic a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/stario/StarIOPort;)Lcom/starmicronics/stario/StarIOPort;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->i:Lcom/starmicronics/stario/StarIOPort;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->t:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->s:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->r:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->q:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->p:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$b;)Lcom/starmicronics/starioextension/StarIoExtManager$b;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->z:Lcom/starmicronics/starioextension/StarIoExtManager$b;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$d;)Lcom/starmicronics/starioextension/StarIoExtManager$d;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->w:Lcom/starmicronics/starioextension/StarIoExtManager$d;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$e;)Lcom/starmicronics/starioextension/StarIoExtManager$e;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->y:Lcom/starmicronics/starioextension/StarIoExtManager$e;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$f;)Lcom/starmicronics/starioextension/StarIoExtManager$f;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->x:Lcom/starmicronics/starioextension/StarIoExtManager$f;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/bs;)Lcom/starmicronics/starioextension/bs;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->m:Lcom/starmicronics/starioextension/bs;

    return-object p1
.end method

.method private a()V
    .locals 4

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->p:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    const/high16 v2, 0x8000000

    const/4 v3, 0x0

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->p:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;->Offline:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->q:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->Empty:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    if-ne v0, v1, :cond_1

    or-int/lit8 v2, v2, 0xc

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->q:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;->NearEmpty:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    if-ne v0, v1, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->r:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;->Open:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    if-ne v0, v1, :cond_3

    const/high16 v0, 0x20000000

    or-int/2addr v0, v2

    move v2, v0

    :cond_3
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->s:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Open:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    if-ne v0, v1, :cond_4

    const/high16 v0, 0x4000000

    or-int/2addr v2, v0

    :cond_4
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "%08x"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    sget-object v2, Lcom/starmicronics/starioextension/StarIoExtManager$a;->r:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {v1, p0, v2}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    invoke-virtual {v1, v0}, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->u:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private a(Lcom/starmicronics/starioextension/StarIoExtManager$e;)V
    .locals 1

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/StarIoExtManager$e;->a()Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->t:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    if-eq p1, v0, :cond_3

    sget-object p1, Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->t:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    new-instance p1, Lcom/starmicronics/starioextension/StarIoExtManager$c;

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$a;->n:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-direct {p1, p0, v0}, Lcom/starmicronics/starioextension/StarIoExtManager$c;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->u:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->h:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$Type;->OnlyBarcodeReader:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    if-eq p1, v0, :cond_0

    return-void

    :cond_0
    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->i:Lcom/starmicronics/stario/StarIOPort;

    invoke-virtual {p1}, Lcom/starmicronics/stario/StarIOPort;->getPortName()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "tcp:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->i:Lcom/starmicronics/stario/StarIOPort;

    invoke-virtual {p1}, Lcom/starmicronics/stario/StarIOPort;->getPortName()Ljava/lang/String;

    move-result-object p1

    sget-object v0, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "bt:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_1
    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->z:Lcom/starmicronics/starioextension/StarIoExtManager$b;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/StarIoExtManager$b;->isAlive()Z

    move-result p1

    if-nez p1, :cond_3

    :cond_2
    new-instance p1, Lcom/starmicronics/starioextension/StarIoExtManager$b;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lcom/starmicronics/starioextension/StarIoExtManager$b;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$1;)V

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->z:Lcom/starmicronics/starioextension/StarIoExtManager$b;

    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->z:Lcom/starmicronics/starioextension/StarIoExtManager$b;

    invoke-virtual {p1}, Lcom/starmicronics/starioextension/StarIoExtManager$b;->start()V

    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/starmicronics/starioextension/StarIoExtManager;ZLcom/starmicronics/starioextension/u;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(ZLcom/starmicronics/starioextension/u;)V

    return-void
.end method

.method private a(Lcom/starmicronics/starioextension/u;)V
    .locals 3

    iget-boolean v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->o:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/starmicronics/starioextension/t;

    sget-object v1, Lcom/starmicronics/starioextension/t$a;->c:Lcom/starmicronics/starioextension/t$a;

    invoke-direct {v0, v1, p1}, Lcom/starmicronics/starioextension/t;-><init>(Lcom/starmicronics/starioextension/t$a;Lcom/starmicronics/starioextension/u;)V

    iget-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->u:Landroid/os/Handler;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.hardware.usb.action.USB_DEVICE_ATTACHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.hardware.usb.action.USB_DEVICE_DETACHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->n:Landroid/content/Context;

    iget-object v2, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->A:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->o:Z

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$1;

    invoke-direct {v0, p0, p1}, Lcom/starmicronics/starioextension/StarIoExtManager$1;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/u;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private a(ZLcom/starmicronics/starioextension/u;)V
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->o:Z

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->n:Landroid/content/Context;

    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->A:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/starmicronics/starioextension/StarIoExtManager$2;-><init>(Lcom/starmicronics/starioextension/StarIoExtManager;ZLcom/starmicronics/starioextension/u;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method static synthetic b(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/content/BroadcastReceiver;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->A:Landroid/content/BroadcastReceiver;

    return-object p0
.end method

.method static synthetic b(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$e;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/StarIoExtManager$e;)V

    return-void
.end method

.method static synthetic b(Lcom/starmicronics/starioextension/StarIoExtManager;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->o:Z

    return p1
.end method

.method static synthetic c(Lcom/starmicronics/starioextension/StarIoExtManager;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->n:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic d(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$d;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->w:Lcom/starmicronics/starioextension/StarIoExtManager$d;

    return-object p0
.end method

.method static synthetic e(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$e;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->y:Lcom/starmicronics/starioextension/StarIoExtManager$e;

    return-object p0
.end method

.method static synthetic f(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$f;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->x:Lcom/starmicronics/starioextension/StarIoExtManager$f;

    return-object p0
.end method

.method static synthetic g(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/stario/StarIOPort;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->i:Lcom/starmicronics/stario/StarIOPort;

    return-object p0
.end method

.method static synthetic h(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$Type;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->h:Lcom/starmicronics/starioextension/StarIoExtManager$Type;

    return-object p0
.end method

.method static synthetic i(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->p:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    return-object p0
.end method

.method static synthetic j(Lcom/starmicronics/starioextension/StarIoExtManager;)V
    .locals 0

    invoke-direct {p0}, Lcom/starmicronics/starioextension/StarIoExtManager;->a()V

    return-void
.end method

.method static synthetic k(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->t:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    return-object p0
.end method

.method static synthetic l(Lcom/starmicronics/starioextension/StarIoExtManager;)Ljava/lang/String;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->j:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic m(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/bs;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->m:Lcom/starmicronics/starioextension/bs;

    return-object p0
.end method

.method static synthetic n(Lcom/starmicronics/starioextension/StarIoExtManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->o:Z

    return p0
.end method

.method static synthetic o(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/u;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->B:Lcom/starmicronics/starioextension/u;

    return-object p0
.end method

.method static synthetic p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->g:Lcom/starmicronics/starioextension/ai;

    return-object p0
.end method

.method static synthetic q(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->q:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    return-object p0
.end method

.method static synthetic r(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->r:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    return-object p0
.end method

.method static synthetic s(Lcom/starmicronics/starioextension/StarIoExtManager;)Z
    .locals 0

    iget-boolean p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->v:Z

    return p0
.end method

.method static synthetic t(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->s:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    return-object p0
.end method

.method static synthetic u(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/StarIoExtManager$b;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->z:Lcom/starmicronics/starioextension/StarIoExtManager$b;

    return-object p0
.end method


# virtual methods
.method public connect(Lcom/starmicronics/starioextension/ConnectionCallback;)V
    .locals 1

    new-instance v0, Lcom/starmicronics/starioextension/u;

    invoke-direct {v0, p1}, Lcom/starmicronics/starioextension/u;-><init>(Lcom/starmicronics/starioextension/ConnectionCallback;)V

    invoke-direct {p0, v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/u;)V

    return-void
.end method

.method public connect(Lcom/starmicronics/starioextension/IConnectionCallback;)V
    .locals 1

    new-instance v0, Lcom/starmicronics/starioextension/u;

    invoke-direct {v0, p1}, Lcom/starmicronics/starioextension/u;-><init>(Lcom/starmicronics/starioextension/IConnectionCallback;)V

    invoke-direct {p0, v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(Lcom/starmicronics/starioextension/u;)V

    return-void
.end method

.method public disconnect(Lcom/starmicronics/starioextension/ConnectionCallback;)V
    .locals 1

    new-instance v0, Lcom/starmicronics/starioextension/u;

    invoke-direct {v0, p1}, Lcom/starmicronics/starioextension/u;-><init>(Lcom/starmicronics/starioextension/ConnectionCallback;)V

    const/4 p1, 0x1

    invoke-direct {p0, p1, v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(ZLcom/starmicronics/starioextension/u;)V

    return-void
.end method

.method public disconnect(Lcom/starmicronics/starioextension/IConnectionCallback;)V
    .locals 1

    new-instance v0, Lcom/starmicronics/starioextension/u;

    invoke-direct {v0, p1}, Lcom/starmicronics/starioextension/u;-><init>(Lcom/starmicronics/starioextension/IConnectionCallback;)V

    const/4 p1, 0x1

    invoke-direct {p0, p1, v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->a(ZLcom/starmicronics/starioextension/u;)V

    return-void
.end method

.method public getBarcodeReaderConnectStatus()Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->t:Lcom/starmicronics/starioextension/StarIoExtManager$BarcodeReaderStatus;

    return-object v0
.end method

.method public getCashDrawerOpenActiveHigh()Z
    .locals 1

    iget-boolean v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->v:Z

    return v0
.end method

.method public getCashDrawerOpenStatus()Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->s:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    return-object v0
.end method

.method public getPort()Lcom/starmicronics/stario/StarIOPort;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->i:Lcom/starmicronics/stario/StarIOPort;

    return-object v0
.end method

.method public getPrinterCoverOpenStatus()Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->r:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterCoverStatus;

    return-object v0
.end method

.method public getPrinterOnlineStatus()Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->p:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterStatus;

    return-object v0
.end method

.method public getPrinterPaperReadyStatus()Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->q:Lcom/starmicronics/starioextension/StarIoExtManager$PrinterPaperStatus;

    return-object v0
.end method

.method public setCashDrawerOpenActiveHigh(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->v:Z

    return-void
.end method

.method public declared-synchronized setListener(Lcom/starmicronics/starioextension/StarIoExtManagerListener;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager;->g:Lcom/starmicronics/starioextension/ai;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method
