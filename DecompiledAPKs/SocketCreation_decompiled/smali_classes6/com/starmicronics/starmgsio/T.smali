.class Lcom/starmicronics/starmgsio/T;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/starmicronics/starmgsio/p;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starmgsio/T$a;,
        Lcom/starmicronics/starmgsio/T$b;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/starmicronics/starmgsio/ConnectionInfo;

.field private c:Landroid/hardware/usb/UsbDevice;

.field private d:Landroid/hardware/usb/UsbInterface;

.field private e:Landroid/hardware/usb/UsbDeviceConnection;

.field private f:Landroid/hardware/usb/UsbEndpoint;

.field private g:Landroid/hardware/usb/UsbEndpoint;

.field private h:Lcom/starmicronics/starmgsio/S;

.field private i:Ljava/lang/Thread;

.field private j:Ljava/lang/Thread;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/starmicronics/starmgsio/ConnectionInfo;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/starmgsio/T;->h:Lcom/starmicronics/starmgsio/S;

    iput-object p1, p0, Lcom/starmicronics/starmgsio/T;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/starmicronics/starmgsio/T;->b:Lcom/starmicronics/starmgsio/ConnectionInfo;

    return-void
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbDevice;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/T;->c:Landroid/hardware/usb/UsbDevice;

    return-object p0
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbDevice;)Landroid/hardware/usb/UsbDevice;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/T;->c:Landroid/hardware/usb/UsbDevice;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/T;Ljava/lang/String;)Landroid/hardware/usb/UsbDevice;
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starmgsio/T;->b(Ljava/lang/String;)Landroid/hardware/usb/UsbDevice;

    move-result-object p0

    return-object p0
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbDeviceConnection;)Landroid/hardware/usb/UsbDeviceConnection;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/T;->e:Landroid/hardware/usb/UsbDeviceConnection;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbEndpoint;)Landroid/hardware/usb/UsbEndpoint;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/T;->f:Landroid/hardware/usb/UsbEndpoint;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbInterface;)Landroid/hardware/usb/UsbInterface;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/T;->d:Landroid/hardware/usb/UsbInterface;

    return-object p1
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/T;Lcom/starmicronics/starmgsio/S;)Lcom/starmicronics/starmgsio/S;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/T;->h:Lcom/starmicronics/starmgsio/S;

    return-object p1
.end method

.method static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string v0, "/dev/bus/usb/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x0

    aget-object v0, p0, v0

    const/4 v1, 0x1

    aget-object p0, p0, v1

    const-string v1, ""

    const-string v2, "^0+"

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "-"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private b(Ljava/lang/String;)Landroid/hardware/usb/UsbDevice;
    .locals 9

    iget-object v0, p0, Lcom/starmicronics/starmgsio/T;->a:Landroid/content/Context;

    const-string v1, "usb"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbManager;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v3

    sget v4, Lcom/starmicronics/starmgsio/Q;->a:I

    if-eq v3, v4, :cond_2

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/starmicronics/starmgsio/Q;->b:[I

    array-length v4, v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v4, :cond_4

    aget v7, v3, v6

    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v8

    if-ne v7, v8, :cond_3

    const/4 v5, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_4
    :goto_2
    if-nez v5, :cond_5

    goto :goto_0

    :cond_5
    const-string v3, ""

    invoke-virtual {p1, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_6

    return-object v2

    :cond_6
    invoke-virtual {v2}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/starmicronics/starmgsio/T;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_7

    goto :goto_0

    :cond_7
    invoke-virtual {p1, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    return-object v2

    :cond_8
    return-object v1
.end method

.method static synthetic b(Lcom/starmicronics/starmgsio/T;Landroid/hardware/usb/UsbEndpoint;)Landroid/hardware/usb/UsbEndpoint;
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/T;->g:Landroid/hardware/usb/UsbEndpoint;

    return-object p1
.end method

.method static synthetic b(Lcom/starmicronics/starmgsio/T;)Lcom/starmicronics/starmgsio/ConnectionInfo;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/T;->b:Lcom/starmicronics/starmgsio/ConnectionInfo;

    return-object p0
.end method

.method static synthetic c(Lcom/starmicronics/starmgsio/T;)Landroid/content/Context;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/T;->a:Landroid/content/Context;

    return-object p0
.end method

.method static synthetic d(Lcom/starmicronics/starmgsio/T;)Lcom/starmicronics/starmgsio/S;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/T;->h:Lcom/starmicronics/starmgsio/S;

    return-object p0
.end method

.method static synthetic e(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbInterface;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/T;->d:Landroid/hardware/usb/UsbInterface;

    return-object p0
.end method

.method static synthetic f(Lcom/starmicronics/starmgsio/T;)Landroid/hardware/usb/UsbDeviceConnection;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/T;->e:Landroid/hardware/usb/UsbDeviceConnection;

    return-object p0
.end method


# virtual methods
.method public a([BIII)I
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object p3, p0, Lcom/starmicronics/starmgsio/T;->e:Landroid/hardware/usb/UsbDeviceConnection;

    if-nez p3, :cond_0

    const/4 p1, -0x1

    monitor-exit p0

    return p1

    :cond_0
    const/4 p3, 0x0

    const/16 p4, 0x32

    if-nez p2, :cond_1

    iget-object p2, p0, Lcom/starmicronics/starmgsio/T;->e:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v0, p0, Lcom/starmicronics/starmgsio/T;->f:Landroid/hardware/usb/UsbEndpoint;

    array-length v1, p1

    invoke-virtual {p2, v0, p1, v1, p4}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result p1

    if-gez p1, :cond_3

    const/4 p1, 0x0

    goto :goto_0

    :cond_1
    array-length v0, p1

    sub-int/2addr v0, p2

    new-array v0, v0, [B

    iget-object v1, p0, Lcom/starmicronics/starmgsio/T;->e:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/starmicronics/starmgsio/T;->f:Landroid/hardware/usb/UsbEndpoint;

    array-length v3, v0

    invoke-virtual {v1, v2, v0, v3, p4}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result p4

    if-lez p4, :cond_2

    invoke-static {v0, p3, p1, p2, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    move p1, p4

    :cond_3
    :goto_0
    monitor-exit p0

    return p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/T;->h:Lcom/starmicronics/starmgsio/S;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/starmicronics/starmgsio/S;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/starmgsio/T;->h:Lcom/starmicronics/starmgsio/S;

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/T;->j:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    new-instance v0, Lcom/starmicronics/starmgsio/T$a;

    invoke-direct {v0, p0}, Lcom/starmicronics/starmgsio/T$a;-><init>(Lcom/starmicronics/starmgsio/T;)V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/T;->j:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/starmicronics/starmgsio/T;->j:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_2
    return-void
.end method

.method public a(Lcom/starmicronics/starmgsio/p$a;)V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/T;->i:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/starmicronics/starmgsio/T$b;

    invoke-direct {v0, p0, p1}, Lcom/starmicronics/starmgsio/T$b;-><init>(Lcom/starmicronics/starmgsio/T;Lcom/starmicronics/starmgsio/p$a;)V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/T;->i:Ljava/lang/Thread;

    iget-object p1, p0, Lcom/starmicronics/starmgsio/T;->i:Ljava/lang/Thread;

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    :cond_1
    return-void
.end method

.method public b([BIII)I
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object p4, p0, Lcom/starmicronics/starmgsio/T;->e:Landroid/hardware/usb/UsbDeviceConnection;

    if-nez p4, :cond_0

    const/4 p1, -0x1

    monitor-exit p0

    return p1

    :cond_0
    array-length p4, p1

    new-array p4, p4, [B

    const/4 v0, 0x0

    invoke-static {p1, p2, p4, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, p4

    move v2, v1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_4

    array-length v3, p4

    array-length v4, p4

    const v5, 0x8000

    if-ge v5, v4, :cond_1

    const v3, 0x8000

    :cond_1
    iget-object v4, p0, Lcom/starmicronics/starmgsio/T;->e:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v5, p0, Lcom/starmicronics/starmgsio/T;->g:Landroid/hardware/usb/UsbEndpoint;

    const/16 v6, 0x2710

    invoke-virtual {v4, v5, p4, v3, v6}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result v3

    if-nez v3, :cond_2

    goto :goto_1

    :cond_2
    if-gez v3, :cond_3

    goto :goto_1

    :cond_3
    add-int/2addr v1, v3

    sub-int/2addr v2, v3

    add-int v3, p2, v1

    invoke-static {p1, v3, p4, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    :cond_4
    :goto_1
    monitor-exit p0

    return p3

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public b()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/T;->e:Landroid/hardware/usb/UsbDeviceConnection;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
