.class public Lcom/starmicronics/starmgsio/ConnectionInfo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;,
        Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;
    }
.end annotation


# static fields
.field public static final ANY_DEVICE_IDENTIFIER:Ljava/lang/String; = ""


# instance fields
.field protected a:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

.field protected b:Ljava/lang/String;

.field protected c:Ljava/lang/String;

.field protected d:Ljava/lang/String;

.field protected e:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;->INVALID:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    iput-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo;->a:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    const-string v0, ""

    iput-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo;->b:Ljava/lang/String;

    const/16 v0, 0x4b0

    iput v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo;->e:I

    return-void
.end method


# virtual methods
.method public getBaudRate()I
    .locals 1

    iget v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo;->e:I

    return v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo;->d:Ljava/lang/String;

    return-object v0
.end method

.method public getIdentifier()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getInterfaceType()Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo;->a:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    return-object v0
.end method

.method public getMacAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo;->c:Ljava/lang/String;

    return-object v0
.end method
