.class public final enum Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/ConnectionInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InterfaceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BLE:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

.field public static final enum INVALID:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

.field public static final enum USB:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

.field private static final synthetic a:[Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    const/4 v1, 0x0

    const-string v2, "BLE"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;->BLE:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    new-instance v0, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    const/4 v2, 0x1

    const-string v3, "USB"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;->USB:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    new-instance v0, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    const/4 v3, 0x2

    const-string v4, "INVALID"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;->INVALID:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    sget-object v4, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;->BLE:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    aput-object v4, v0, v1

    sget-object v1, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;->USB:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;->INVALID:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;->a:[Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;
    .locals 1

    const-class v0, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;
    .locals 1

    sget-object v0, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;->a:[Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    invoke-virtual {v0}, [Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    return-object v0
.end method
