.class public Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/ConnectionInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field protected a:Lcom/starmicronics/starmgsio/z;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/starmicronics/starmgsio/z;

    invoke-direct {v0}, Lcom/starmicronics/starmgsio/z;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->a:Lcom/starmicronics/starmgsio/z;

    return-void
.end method


# virtual methods
.method public build()Lcom/starmicronics/starmgsio/ConnectionInfo;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->a:Lcom/starmicronics/starmgsio/z;

    return-object v0
.end method

.method public setBaudRate(I)Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->a:Lcom/starmicronics/starmgsio/z;

    invoke-virtual {v0, p1}, Lcom/starmicronics/starmgsio/z;->a(I)V

    return-object p0
.end method

.method public setBleInfo(Ljava/lang/String;)Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->a:Lcom/starmicronics/starmgsio/z;

    sget-object v1, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;->BLE:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    invoke-virtual {v0, v1}, Lcom/starmicronics/starmgsio/z;->a(Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;)V

    iget-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->a:Lcom/starmicronics/starmgsio/z;

    invoke-virtual {v0, p1}, Lcom/starmicronics/starmgsio/z;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->a:Lcom/starmicronics/starmgsio/z;

    invoke-virtual {v0, p1}, Lcom/starmicronics/starmgsio/z;->b(Ljava/lang/String;)V

    return-object p0
.end method

.method public setUsbInfo(Ljava/lang/String;)Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->a:Lcom/starmicronics/starmgsio/z;

    sget-object v1, Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;->USB:Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;

    invoke-virtual {v0, v1}, Lcom/starmicronics/starmgsio/z;->a(Lcom/starmicronics/starmgsio/ConnectionInfo$InterfaceType;)V

    iget-object v0, p0, Lcom/starmicronics/starmgsio/ConnectionInfo$Builder;->a:Lcom/starmicronics/starmgsio/z;

    invoke-virtual {v0, p1}, Lcom/starmicronics/starmgsio/z;->b(Ljava/lang/String;)V

    return-object p0
.end method
