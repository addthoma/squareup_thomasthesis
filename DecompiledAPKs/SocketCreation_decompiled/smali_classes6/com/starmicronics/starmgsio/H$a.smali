.class Lcom/starmicronics/starmgsio/H$a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/H;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/starmicronics/starmgsio/H;",
            ">;"
        }
    .end annotation
.end field

.field b:[B


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/H$a;->a:Ljava/util/List;

    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/starmicronics/starmgsio/H$a;->b:[B

    return-void
.end method


# virtual methods
.method a()Lcom/starmicronics/starmgsio/H;
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starmgsio/H$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/H$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/starmicronics/starmgsio/H;

    return-object v0
.end method

.method a([BI)Lcom/starmicronics/starmgsio/H;
    .locals 7

    const/4 v0, 0x0

    if-ltz p2, :cond_5

    array-length v1, p1

    if-ge v1, p2, :cond_0

    goto/16 :goto_2

    :cond_0
    array-length v1, p1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    return-object v0

    :cond_1
    new-instance v1, Lcom/starmicronics/starmgsio/H;

    invoke-direct {v1}, Lcom/starmicronics/starmgsio/H;-><init>()V

    const/4 v3, 0x5

    new-array v4, v3, [B

    fill-array-data v4, :array_0

    invoke-static {p1, p2, v4}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v4

    const/4 v5, 0x0

    if-eqz v4, :cond_2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/starmicronics/starmgsio/H;->a(Lcom/starmicronics/starmgsio/H;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    add-int/lit8 v2, p2, 0x3

    invoke-virtual {v0, p2, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/starmicronics/starmgsio/H;->a(Lcom/starmicronics/starmgsio/H;Ljava/lang/String;)Ljava/lang/String;

    :goto_0
    const/4 v2, 0x5

    goto :goto_1

    :cond_2
    new-array v4, v2, [B

    const/4 v6, 0x6

    aput-byte v6, v4, v5

    invoke-static {p1, p2, v4}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/starmicronics/starmgsio/H;->a(Lcom/starmicronics/starmgsio/H;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>([B)V

    add-int/lit8 v3, p2, 0x3

    invoke-virtual {v0, p2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/starmicronics/starmgsio/H;->a(Lcom/starmicronics/starmgsio/H;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    :cond_3
    new-array v4, v3, [B

    fill-array-data v4, :array_1

    invoke-static {p1, v5, v4}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/starmicronics/starmgsio/H;->a(Lcom/starmicronics/starmgsio/H;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    goto :goto_0

    :cond_4
    new-array v3, v2, [B

    const/16 v4, 0x15

    aput-byte v4, v3, v5

    invoke-static {p1, v5, v3}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/starmicronics/starmgsio/H;->a(Lcom/starmicronics/starmgsio/H;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    :goto_1
    array-length v0, p1

    add-int/2addr p2, v2

    sub-int/2addr v0, p2

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/starmicronics/starmgsio/H$a;->b:[B

    iget-object v0, p0, Lcom/starmicronics/starmgsio/H$a;->b:[B

    array-length v2, v0

    invoke-static {p1, p2, v0, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1

    :cond_5
    :goto_2
    return-object v0

    :array_0
    .array-data 1
        0x41t
        0x30t
        0x30t
        0xdt
        0xat
    .end array-data

    nop

    :array_1
    .array-data 1
        0x45t
        0x30t
        0x31t
        0xdt
        0xat
    .end array-data
.end method

.method a([B)V
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starmgsio/H$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    new-array v1, v0, [B

    iput-object v1, p0, Lcom/starmicronics/starmgsio/H$a;->b:[B

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    invoke-virtual {p0, p1, v0}, Lcom/starmicronics/starmgsio/H$a;->a([BI)Lcom/starmicronics/starmgsio/H;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/starmicronics/starmgsio/H$a;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    array-length v0, p1

    iget-object v1, p0, Lcom/starmicronics/starmgsio/H$a;->b:[B

    array-length v1, v1

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
