.class public final Lcom/squareup/user/NotificationPresenter;
.super Lmortar/Presenter;
.source "NotificationPresenter.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/user/NotificationPresenter$View;,
        Lcom/squareup/user/NotificationPresenter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/user/NotificationPresenter$View;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nNotificationPresenter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 NotificationPresenter.kt\ncom/squareup/user/NotificationPresenter\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,156:1\n250#2,2:157\n250#2,2:159\n*E\n*S KotlinDebug\n*F\n+ 1 NotificationPresenter.kt\ncom/squareup/user/NotificationPresenter\n*L\n130#1,2:157\n134#1,2:159\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u0000 *2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0002*+B+\u0008\u0007\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0006\u0010\u0016\u001a\u00020\u0017J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\rH\u0002J\u0008\u0010\u001b\u001a\u00020\u0008H\u0002J\u0010\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\rH\u0002J\u0010\u0010\u001d\u001a\u00020\u00172\u0006\u0010\u001e\u001a\u00020\u0002H\u0016J\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010\u001e\u001a\u00020\u0002H\u0014J\u0018\u0010!\u001a\u0004\u0018\u00010\r2\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\r0#H\u0002J\u0018\u0010$\u001a\u0004\u0018\u00010\r2\u000c\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\r0#H\u0007J\u0012\u0010%\u001a\u00020\u00172\u0008\u0010&\u001a\u0004\u0018\u00010\'H\u0014J\u0010\u0010(\u001a\u00020\u00172\u0006\u0010)\u001a\u00020\'H\u0014R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u000e\u001a\u0004\u0018\u00010\r2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\r8G@BX\u0086\u000e\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001f\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00130\u00128G\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/user/NotificationPresenter;",
        "Lmortar/Presenter;",
        "Lcom/squareup/user/NotificationPresenter$View;",
        "settings",
        "Ljavax/inject/Provider;",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "dismissedNotificationsSetting",
        "Lcom/squareup/settings/LocalSetting;",
        "Lcom/squareup/user/DismissedNotifications;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "(Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/BrowserLauncher;)V",
        "<set-?>",
        "Lcom/squareup/server/account/protos/Notification;",
        "current",
        "getCurrent",
        "()Lcom/squareup/server/account/protos/Notification;",
        "showsNotification",
        "Lcom/squareup/mortar/PopupPresenter;",
        "Lcom/squareup/server/account/protos/Notification$Button;",
        "getShowsNotification",
        "()Lcom/squareup/mortar/PopupPresenter;",
        "check",
        "",
        "checkDismissLockOut",
        "",
        "notification",
        "dismissedNotifications",
        "doNotShowAgain",
        "dropView",
        "view",
        "extractBundleService",
        "Lmortar/bundler/BundleService;",
        "getFirstNewNonLockout",
        "source",
        "",
        "getLockout",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onSave",
        "outState",
        "Companion",
        "View",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CURRENT_NOTIFICATION_KEY:Ljava/lang/String; = "Current Notification Key"

.field public static final Companion:Lcom/squareup/user/NotificationPresenter$Companion;


# instance fields
.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private current:Lcom/squareup/server/account/protos/Notification;

.field private final dismissedNotificationsSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/user/DismissedNotifications;",
            ">;"
        }
    .end annotation
.end field

.field private final settings:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final showsNotification:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/server/account/protos/Notification;",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/user/NotificationPresenter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/user/NotificationPresenter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/user/NotificationPresenter;->Companion:Lcom/squareup/user/NotificationPresenter$Companion;

    return-void
.end method

.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/BrowserLauncher;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/user/DismissedNotifications;",
            ">;",
            "Lcom/squareup/util/BrowserLauncher;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dismissedNotificationsSetting"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "browserLauncher"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    iput-object p1, p0, Lcom/squareup/user/NotificationPresenter;->settings:Ljavax/inject/Provider;

    iput-object p2, p0, Lcom/squareup/user/NotificationPresenter;->dismissedNotificationsSetting:Lcom/squareup/settings/LocalSetting;

    iput-object p3, p0, Lcom/squareup/user/NotificationPresenter;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    .line 38
    new-instance p1, Lcom/squareup/user/NotificationPresenter$showsNotification$1;

    invoke-direct {p1, p0}, Lcom/squareup/user/NotificationPresenter$showsNotification$1;-><init>(Lcom/squareup/user/NotificationPresenter;)V

    check-cast p1, Lcom/squareup/mortar/PopupPresenter;

    iput-object p1, p0, Lcom/squareup/user/NotificationPresenter;->showsNotification:Lcom/squareup/mortar/PopupPresenter;

    return-void
.end method

.method public static final synthetic access$doNotShowAgain(Lcom/squareup/user/NotificationPresenter;Lcom/squareup/server/account/protos/Notification;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1}, Lcom/squareup/user/NotificationPresenter;->doNotShowAgain(Lcom/squareup/server/account/protos/Notification;)V

    return-void
.end method

.method public static final synthetic access$getBrowserLauncher$p(Lcom/squareup/user/NotificationPresenter;)Lcom/squareup/util/BrowserLauncher;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/user/NotificationPresenter;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-object p0
.end method

.method public static final synthetic access$getCurrent$p(Lcom/squareup/user/NotificationPresenter;)Lcom/squareup/server/account/protos/Notification;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/user/NotificationPresenter;->current:Lcom/squareup/server/account/protos/Notification;

    return-object p0
.end method

.method public static final synthetic access$setCurrent$p(Lcom/squareup/user/NotificationPresenter;Lcom/squareup/server/account/protos/Notification;)V
    .locals 0

    .line 27
    iput-object p1, p0, Lcom/squareup/user/NotificationPresenter;->current:Lcom/squareup/server/account/protos/Notification;

    return-void
.end method

.method private final checkDismissLockOut(Lcom/squareup/server/account/protos/Notification;)Z
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter;->settings:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "settings.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getNotifications()Ljava/util/List;

    move-result-object v0

    .line 118
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 119
    iget-object p1, p0, Lcom/squareup/user/NotificationPresenter;->showsNotification:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {p1}, Lcom/squareup/mortar/PopupPresenter;->dismiss()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private final dismissedNotifications()Lcom/squareup/user/DismissedNotifications;
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter;->dismissedNotificationsSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/user/DismissedNotifications;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/user/DismissedNotifications;

    invoke-direct {v0}, Lcom/squareup/user/DismissedNotifications;-><init>()V

    .line 148
    iget-object v1, p0, Lcom/squareup/user/NotificationPresenter;->dismissedNotificationsSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    :goto_0
    return-object v0
.end method

.method private final doNotShowAgain(Lcom/squareup/server/account/protos/Notification;)V
    .locals 1

    .line 141
    invoke-direct {p0}, Lcom/squareup/user/NotificationPresenter;->dismissedNotifications()Lcom/squareup/user/DismissedNotifications;

    move-result-object v0

    .line 142
    invoke-virtual {v0, p1}, Lcom/squareup/user/DismissedNotifications;->dismiss(Lcom/squareup/server/account/protos/Notification;)V

    .line 143
    iget-object p1, p0, Lcom/squareup/user/NotificationPresenter;->dismissedNotificationsSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final getFirstNewNonLockout(Ljava/util/List;)Lcom/squareup/server/account/protos/Notification;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Notification;",
            ">;)",
            "Lcom/squareup/server/account/protos/Notification;"
        }
    .end annotation

    .line 133
    invoke-direct {p0}, Lcom/squareup/user/NotificationPresenter;->dismissedNotifications()Lcom/squareup/user/DismissedNotifications;

    move-result-object v0

    .line 134
    check-cast p1, Ljava/lang/Iterable;

    .line 159
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/server/account/protos/Notification;

    .line 134
    iget-object v3, v2, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v3, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    xor-int/2addr v3, v4

    if-eqz v3, :cond_1

    invoke-virtual {v0, v2}, Lcom/squareup/user/DismissedNotifications;->isDismissed(Lcom/squareup/server/account/protos/Notification;)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    :goto_0
    if-eqz v4, :cond_0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    .line 160
    :goto_1
    check-cast v1, Lcom/squareup/server/account/protos/Notification;

    return-object v1
.end method


# virtual methods
.method public final check()V
    .locals 4

    .line 94
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter;->showsNotification:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/mortar/PopupPresenter;->showing()Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/Notification;

    iput-object v0, p0, Lcom/squareup/user/NotificationPresenter;->current:Lcom/squareup/server/account/protos/Notification;

    .line 97
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter;->current:Lcom/squareup/server/account/protos/Notification;

    if-eqz v0, :cond_1

    .line 98
    iget-object v1, v0, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v2

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/squareup/user/NotificationPresenter;->checkDismissLockOut(Lcom/squareup/server/account/protos/Notification;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter;->settings:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "settings.get()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getNotifications()Ljava/util/List;

    move-result-object v0

    const-string v1, "remote"

    .line 105
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/squareup/user/NotificationPresenter;->getLockout(Ljava/util/List;)Lcom/squareup/server/account/protos/Notification;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/user/NotificationPresenter;->current:Lcom/squareup/server/account/protos/Notification;

    .line 106
    iget-object v1, p0, Lcom/squareup/user/NotificationPresenter;->current:Lcom/squareup/server/account/protos/Notification;

    if-eqz v1, :cond_2

    .line 107
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter;->showsNotification:Lcom/squareup/mortar/PopupPresenter;

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    goto :goto_1

    .line 109
    :cond_2
    invoke-direct {p0, v0}, Lcom/squareup/user/NotificationPresenter;->getFirstNewNonLockout(Ljava/util/List;)Lcom/squareup/server/account/protos/Notification;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 110
    iget-object v1, p0, Lcom/squareup/user/NotificationPresenter;->showsNotification:Lcom/squareup/mortar/PopupPresenter;

    move-object v2, v0

    check-cast v2, Landroid/os/Parcelable;

    invoke-virtual {v1, v2}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    .line 109
    :goto_0
    iput-object v0, p0, Lcom/squareup/user/NotificationPresenter;->current:Lcom/squareup/server/account/protos/Notification;

    :goto_1
    return-void
.end method

.method public dropView(Lcom/squareup/user/NotificationPresenter$View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter;->showsNotification:Lcom/squareup/mortar/PopupPresenter;

    invoke-interface {p1}, Lcom/squareup/user/NotificationPresenter$View;->getNotificationPopup()Lcom/squareup/mortar/Popup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 90
    invoke-super {p0, p1}, Lmortar/Presenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/user/NotificationPresenter$View;

    invoke-virtual {p0, p1}, Lcom/squareup/user/NotificationPresenter;->dropView(Lcom/squareup/user/NotificationPresenter$View;)V

    return-void
.end method

.method protected extractBundleService(Lcom/squareup/user/NotificationPresenter$View;)Lmortar/bundler/BundleService;
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-interface {p1}, Lcom/squareup/user/NotificationPresenter$View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    const-string v0, "BundleService.getBundleService(view.context)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 27
    check-cast p1, Lcom/squareup/user/NotificationPresenter$View;

    invoke-virtual {p0, p1}, Lcom/squareup/user/NotificationPresenter;->extractBundleService(Lcom/squareup/user/NotificationPresenter$View;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public final getCurrent()Lcom/squareup/server/account/protos/Notification;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter;->current:Lcom/squareup/server/account/protos/Notification;

    return-object v0
.end method

.method public final getLockout(Ljava/util/List;)Lcom/squareup/server/account/protos/Notification;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Notification;",
            ">;)",
            "Lcom/squareup/server/account/protos/Notification;"
        }
    .end annotation

    const-string v0, "source"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    check-cast p1, Ljava/lang/Iterable;

    .line 157
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/server/account/protos/Notification;

    .line 130
    iget-object v1, v1, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 158
    :goto_0
    check-cast v0, Lcom/squareup/server/account/protos/Notification;

    return-object v0
.end method

.method public final getShowsNotification()Lcom/squareup/mortar/PopupPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/server/account/protos/Notification;",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter;->showsNotification:Lcom/squareup/mortar/PopupPresenter;

    return-object v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 69
    invoke-super {p0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter;->showsNotification:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {p0}, Lcom/squareup/user/NotificationPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/user/NotificationPresenter$View;

    invoke-interface {v1}, Lcom/squareup/user/NotificationPresenter$View;->getNotificationPopup()Lcom/squareup/mortar/Popup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter;->current:Lcom/squareup/server/account/protos/Notification;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "Current Notification Key"

    .line 77
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/account/protos/Notification;

    iput-object p1, p0, Lcom/squareup/user/NotificationPresenter;->current:Lcom/squareup/server/account/protos/Notification;

    goto :goto_0

    .line 79
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/user/NotificationPresenter;->check()V

    :goto_0
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "outState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-super {p0, p1}, Lmortar/Presenter;->onSave(Landroid/os/Bundle;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter;->current:Lcom/squareup/server/account/protos/Notification;

    check-cast v0, Landroid/os/Parcelable;

    const-string v1, "Current Notification Key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
