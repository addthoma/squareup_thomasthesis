.class public interface abstract Lcom/squareup/user/NotificationPresenter$View;
.super Ljava/lang/Object;
.source "NotificationPresenter.kt"

# interfaces
.implements Lcom/squareup/mortar/HasContext;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/user/NotificationPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "View"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001R\u001e\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/user/NotificationPresenter$View;",
        "Lcom/squareup/mortar/HasContext;",
        "notificationPopup",
        "Lcom/squareup/mortar/Popup;",
        "Lcom/squareup/server/account/protos/Notification;",
        "Lcom/squareup/server/account/protos/Notification$Button;",
        "getNotificationPopup",
        "()Lcom/squareup/mortar/Popup;",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getNotificationPopup()Lcom/squareup/mortar/Popup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/mortar/Popup<",
            "Lcom/squareup/server/account/protos/Notification;",
            "Lcom/squareup/server/account/protos/Notification$Button;",
            ">;"
        }
    .end annotation
.end method
