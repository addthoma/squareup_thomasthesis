.class public final Lcom/squareup/user/NotificationPresenter_Factory;
.super Ljava/lang/Object;
.source "NotificationPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/user/NotificationPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final dismissedNotificationsSettingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/user/DismissedNotifications;",
            ">;>;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/user/DismissedNotifications;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/user/NotificationPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/user/NotificationPresenter_Factory;->dismissedNotificationsSettingProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/user/NotificationPresenter_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/user/NotificationPresenter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/user/DismissedNotifications;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;)",
            "Lcom/squareup/user/NotificationPresenter_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/user/NotificationPresenter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/user/NotificationPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/BrowserLauncher;)Lcom/squareup/user/NotificationPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/user/DismissedNotifications;",
            ">;",
            "Lcom/squareup/util/BrowserLauncher;",
            ")",
            "Lcom/squareup/user/NotificationPresenter;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/user/NotificationPresenter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/user/NotificationPresenter;-><init>(Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/BrowserLauncher;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/user/NotificationPresenter;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/user/NotificationPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    iget-object v1, p0, Lcom/squareup/user/NotificationPresenter_Factory;->dismissedNotificationsSettingProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/LocalSetting;

    iget-object v2, p0, Lcom/squareup/user/NotificationPresenter_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/BrowserLauncher;

    invoke-static {v0, v1, v2}, Lcom/squareup/user/NotificationPresenter_Factory;->newInstance(Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/util/BrowserLauncher;)Lcom/squareup/user/NotificationPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/user/NotificationPresenter_Factory;->get()Lcom/squareup/user/NotificationPresenter;

    move-result-object v0

    return-object v0
.end method
