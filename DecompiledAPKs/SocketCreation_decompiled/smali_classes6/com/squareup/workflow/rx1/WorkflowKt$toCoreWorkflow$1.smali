.class final Lcom/squareup/workflow/rx1/WorkflowKt$toCoreWorkflow$1;
.super Lkotlin/jvm/internal/Lambda;
.source "Workflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/rx1/WorkflowKt;->toCoreWorkflow(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Throwable;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u00032\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "S",
        "",
        "E",
        "O",
        "cause",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_toCoreWorkflow:Lcom/squareup/workflow/rx1/Workflow;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/rx1/Workflow;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/rx1/WorkflowKt$toCoreWorkflow$1;->$this_toCoreWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/rx1/WorkflowKt$toCoreWorkflow$1;->invoke(Ljava/lang/Throwable;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Throwable;)V
    .locals 0

    .line 140
    instance-of p1, p1, Ljava/util/concurrent/CancellationException;

    if-eqz p1, :cond_0

    .line 141
    iget-object p1, p0, Lcom/squareup/workflow/rx1/WorkflowKt$toCoreWorkflow$1;->$this_toCoreWorkflow:Lcom/squareup/workflow/rx1/Workflow;

    invoke-interface {p1}, Lcom/squareup/workflow/rx1/Workflow;->abandon()V

    :cond_0
    return-void
.end method
