.class public final Lcom/squareup/workflow/rx1/ScreensKt;
.super Ljava/lang/Object;
.source "Screens.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000P\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u001a\u00a2\u0001\u0010\u0000\u001a0\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0001j\u0014\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005`\u0006\"\u0008\u0008\u0000\u0010\u0007*\u00020\u0008\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0008\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0008\"\u0008\u0008\u0003\u0010\u0005*\u00020\u0008*0\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00070\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0001j\u0014\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005`\u00062\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u0002H\u0007\u0012\u0004\u0012\u0002H\u00030\n*X\u0008\u0007\u0010\u000b\u001a\u0004\u0008\u0000\u0010\u0004\u001a\u0004\u0008\u0001\u0010\u0005\"\u0014\u0012\u0004\u0012\u0002`\u000c\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005`\u00062\"\u0012\u0012\u0012\u0010\u0012\u000c\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\r0\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0001B\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0010*n\u0008\u0007\u0010\u0011\u001a\u0004\u0008\u0000\u0010\u0004\u001a\u0004\u0008\u0001\u0010\u0005\"\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u0013`\u0012\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005`\u000622\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0013\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\rj\u0002`\u000c0\u00140\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0001B\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0015*\u008c\u0001\u0008\u0007\u0010\u0016\u001a\u0004\u0008\u0000\u0010\u0004\u001a\u0004\u0008\u0001\u0010\u0005\"\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u0017`\u0012\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0005`\u000622\u0012\"\u0012 \u0012\u001c\u0012\u001a\u0012\u0004\u0012\u00020\u0017\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\rj\u0002`\u000c0\u00140\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0001B*\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0018\u0012\u001c\u0008\u0019\u0012\u0018\u0008\u000bB\u0014\u0008\u001a\u0012\u0006\u0008\u001b\u0012\u0002\u0008\u000c\u0012\u0008\u0008\u001c\u0012\u0004\u0008\u0008(\u001d*\\\u0008\u0007\u0010\u001e\u001a\u0004\u0008\u0000\u0010\u001f\u001a\u0004\u0008\u0001\u0010\u0004\u001a\u0004\u0008\u0002\u0010\u0005\"\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u001f0\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00012\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u001f0\u0002\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0001B\u000c\u0008\u000e\u0012\u0008\u0008\u000f\u0012\u0004\u0008\u0008(\u0015\u00a8\u0006 "
    }
    d2 = {
        "mapScreen",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "R2",
        "E",
        "O",
        "Lcom/squareup/workflow/rx1/ScreenWorkflow;",
        "R1",
        "",
        "mapper",
        "Lkotlin/Function1;",
        "FlatScreenWorkflow",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lkotlin/Deprecated;",
        "message",
        "Use Renderer instead.",
        "ModalScreenWorkflow",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/workflow/MainAndModal;",
        "",
        "Use com.squareup.workflow.Workflow",
        "PosScreenWorkflow",
        "Lcom/squareup/container/PosLayering;",
        "Use PosWorkflow to integrate with PosWorkflowRunner, ideally via a Renderer.",
        "replaceWith",
        "Lkotlin/ReplaceWith;",
        "imports",
        "expression",
        "PosWorkflow",
        "ScreenWorkflow",
        "R",
        "pure-rx1"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static synthetic FlatScreenWorkflow$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use Renderer instead."
    .end annotation

    return-void
.end method

.method public static synthetic ModalScreenWorkflow$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    return-void
.end method

.method public static synthetic PosScreenWorkflow$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use PosWorkflow to integrate with PosWorkflowRunner, ideally via a Renderer."
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "PosWorkflow"
            imports = {}
        .end subannotation
    .end annotation

    return-void
.end method

.method public static synthetic ScreenWorkflow$annotations()V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    return-void
.end method

.method public static final mapScreen(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R1:",
            "Ljava/lang/Object;",
            "R2:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+",
            "Lcom/squareup/workflow/ScreenState<",
            "+TR1;>;-TE;+TO;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TR1;+TR2;>;)",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "TR2;>;TE;TO;>;"
        }
    .end annotation

    const-string v0, "$this$mapScreen"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/rx1/ScreensKt$mapScreen$1;-><init>(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/rx1/Workflow;

    return-object v0
.end method
