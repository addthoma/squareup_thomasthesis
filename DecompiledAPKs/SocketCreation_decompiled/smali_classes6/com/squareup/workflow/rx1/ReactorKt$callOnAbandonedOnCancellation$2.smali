.class final Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$2;
.super Lkotlin/jvm/internal/Lambda;
.source "Reactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/rx1/ReactorKt;->callOnAbandonedOnCancellation(Lcom/squareup/workflow/rx1/Reactor;Lcom/squareup/workflow/legacy/Workflow;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Ljava/lang/Throwable;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReactor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Reactor.kt\ncom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$2\n*L\n1#1,78:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u00032\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "S",
        "",
        "E",
        "O",
        "it",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $lastSeenState:Lkotlin/jvm/internal/Ref$ObjectRef;

.field final synthetic $reactor:Lcom/squareup/workflow/rx1/Reactor;


# direct methods
.method constructor <init>(Lkotlin/jvm/internal/Ref$ObjectRef;Lcom/squareup/workflow/rx1/Reactor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$2;->$lastSeenState:Lkotlin/jvm/internal/Ref$ObjectRef;

    iput-object p2, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$2;->$reactor:Lcom/squareup/workflow/rx1/Reactor;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Throwable;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$2;->invoke(Ljava/lang/Throwable;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Ljava/lang/Throwable;)V
    .locals 1

    .line 73
    instance-of p1, p1, Ljava/util/concurrent/CancellationException;

    if-eqz p1, :cond_0

    .line 74
    iget-object p1, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$2;->$lastSeenState:Lkotlin/jvm/internal/Ref$ObjectRef;

    iget-object p1, p1, Lkotlin/jvm/internal/Ref$ObjectRef;->element:Ljava/lang/Object;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/rx1/ReactorKt$callOnAbandonedOnCancellation$2;->$reactor:Lcom/squareup/workflow/rx1/Reactor;

    invoke-interface {v0, p1}, Lcom/squareup/workflow/rx1/Reactor;->onAbandoned(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
