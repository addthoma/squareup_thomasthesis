.class public final Lcom/squareup/workflow/ViewPersistenceStack;
.super Ljava/lang/Object;
.source "ViewPersistenceStack.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/ViewPersistenceStack$ReplaceResult;,
        Lcom/squareup/workflow/ViewPersistenceStack$CREATOR;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViewPersistenceStack.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ViewPersistenceStack.kt\ncom/squareup/workflow/ViewPersistenceStack\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,83:1\n310#2,7:84\n*E\n*S KotlinDebug\n*F\n+ 1 ViewPersistenceStack.kt\ncom/squareup/workflow/ViewPersistenceStack\n*L\n39#1,7:84\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u00132\u00020\u0001:\u0002\u0013\u0014B\u0007\u0008\u0016\u00a2\u0006\u0002\u0010\u0002B\u0015\u0008\u0002\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0016\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000cJ\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0008H\u0016R\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/workflow/ViewPersistenceStack;",
        "Landroid/os/Parcelable;",
        "()V",
        "viewStates",
        "",
        "Lcom/squareup/workflow/ViewStateFrame;",
        "(Ljava/util/List;)V",
        "describeContents",
        "",
        "replace",
        "Lcom/squareup/workflow/ViewPersistenceStack$ReplaceResult;",
        "currentView",
        "Landroid/view/View;",
        "newView",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "CREATOR",
        "ReplaceResult",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/workflow/ViewPersistenceStack$CREATOR;


# instance fields
.field private viewStates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/workflow/ViewStateFrame;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/workflow/ViewPersistenceStack$CREATOR;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/workflow/ViewPersistenceStack$CREATOR;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/workflow/ViewPersistenceStack;->CREATOR:Lcom/squareup/workflow/ViewPersistenceStack$CREATOR;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 20
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/workflow/ViewPersistenceStack;-><init>(Ljava/util/List;)V

    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/workflow/ViewStateFrame;",
            ">;)V"
        }
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/ViewPersistenceStack;->viewStates:Ljava/util/List;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/workflow/ViewPersistenceStack;-><init>(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final replace(Landroid/view/View;Landroid/view/View;)Lcom/squareup/workflow/ViewPersistenceStack$ReplaceResult;
    .locals 5

    const-string v0, "currentView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newView"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/workflow/ViewPersistenceStack;->viewStates:Ljava/util/List;

    .line 85
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 86
    check-cast v3, Lcom/squareup/workflow/ViewStateFrame;

    .line 39
    invoke-virtual {v3}, Lcom/squareup/workflow/ViewStateFrame;->getKey()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    invoke-static {p2}, Lcom/squareup/workflow/WorkflowViewFactoryKt;->getKey(Landroid/view/View;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    :goto_1
    if-gez v2, :cond_2

    .line 41
    new-instance p2, Landroid/util/SparseArray;

    invoke-direct {p2}, Landroid/util/SparseArray;-><init>()V

    invoke-virtual {p1, p2}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/workflow/ViewPersistenceStack;->viewStates:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    new-instance v1, Lcom/squareup/workflow/ViewStateFrame;

    invoke-static {p1}, Lcom/squareup/workflow/WorkflowViewFactoryKt;->getKey(Landroid/view/View;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object p1

    invoke-direct {v1, p1, p2}, Lcom/squareup/workflow/ViewStateFrame;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Landroid/util/SparseArray;)V

    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/ViewPersistenceStack;->viewStates:Ljava/util/List;

    .line 43
    sget-object p1, Lcom/squareup/workflow/ViewPersistenceStack$ReplaceResult;->PUSHED:Lcom/squareup/workflow/ViewPersistenceStack$ReplaceResult;

    return-object p1

    .line 46
    :cond_2
    iget-object p1, p0, Lcom/squareup/workflow/ViewPersistenceStack;->viewStates:Ljava/util/List;

    add-int/lit8 v2, v2, 0x1

    invoke-interface {p1, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/ViewPersistenceStack;->viewStates:Ljava/util/List;

    .line 47
    iget-object p1, p0, Lcom/squareup/workflow/ViewPersistenceStack;->viewStates:Ljava/util/List;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->lastOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/ViewStateFrame;

    if-eqz p1, :cond_3

    .line 48
    iget-object p1, p0, Lcom/squareup/workflow/ViewPersistenceStack;->viewStates:Ljava/util/List;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/ViewStateFrame;

    invoke-virtual {p1}, Lcom/squareup/workflow/ViewStateFrame;->getViewState()Landroid/util/SparseArray;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 50
    :cond_3
    sget-object p1, Lcom/squareup/workflow/ViewPersistenceStack$ReplaceResult;->POPPED:Lcom/squareup/workflow/ViewPersistenceStack$ReplaceResult;

    return-object p1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    iget-object p2, p0, Lcom/squareup/workflow/ViewPersistenceStack;->viewStates:Ljava/util/List;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    return-void
.end method
