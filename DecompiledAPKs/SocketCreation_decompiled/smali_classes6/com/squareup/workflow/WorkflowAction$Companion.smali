.class public final Lcom/squareup/workflow/WorkflowAction$Companion;
.super Ljava/lang/Object;
.source "WorkflowAction.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowAction.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowAction.kt\ncom/squareup/workflow/WorkflowAction$Companion\n+ 2 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt\n*L\n1#1,211:1\n199#2,4:212\n199#2,4:216\n199#2,4:220\n199#2,4:224\n199#2,4:228\n199#2,4:232\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowAction.kt\ncom/squareup/workflow/WorkflowAction$Companion\n*L\n86#1,4:212\n107#1,4:216\n127#1,4:220\n143#1,4:224\n159#1,4:228\n161#1,4:232\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J1\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u0004\"\u0004\u0008\u0002\u0010\u0006\"\u0008\u0008\u0003\u0010\u0007*\u00020\u00012\u0006\u0010\u0008\u001a\u0002H\u0007H\u0007\u00a2\u0006\u0002\u0010\tJ9\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u0004\"\u0004\u0008\u0002\u0010\u0006\"\u0008\u0008\u0003\u0010\u0007*\u00020\u00012\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0008\u001a\u0002H\u0007H\u0007\u00a2\u0006\u0002\u0010\u000cJ=\u0010\r\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u0004\"\u0004\u0008\u0002\u0010\u0006\"\u0008\u0008\u0003\u0010\u0007*\u00020\u00012\u0006\u0010\u000e\u001a\u0002H\u00062\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u0001H\u0007H\u0007\u00a2\u0006\u0002\u0010\u0010JE\u0010\r\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u0004\"\u0004\u0008\u0002\u0010\u0006\"\u0008\u0008\u0003\u0010\u0007*\u00020\u00012\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u0002H\u00062\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u0001H\u0007H\u0007\u00a2\u0006\u0002\u0010\u0011JW\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u0004\"\u0004\u0008\u0002\u0010\u0006\"\u0008\u0008\u0003\u0010\u0007*\u00020\u00012\u000c\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\u00132\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u0001H\u00072\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00060\u0015H\u0007\u00a2\u0006\u0002\u0010\u0016J\"\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u0002H\u0006\u0012\u0004\u0012\u0002H\u00070\u0004\"\u0004\u0008\u0002\u0010\u0006\"\u0008\u0008\u0003\u0010\u0007*\u00020\u0001R\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00010\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/workflow/WorkflowAction$Companion;",
        "",
        "()V",
        "NO_ACTION",
        "Lcom/squareup/workflow/WorkflowAction;",
        "emitOutput",
        "StateT",
        "OutputT",
        "output",
        "(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;",
        "name",
        "",
        "(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;",
        "enterState",
        "newState",
        "emittingOutput",
        "(Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;",
        "(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;",
        "modifyState",
        "Lkotlin/Function0;",
        "modify",
        "Lkotlin/Function1;",
        "(Lkotlin/jvm/functions/Function0;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;",
        "noAction",
        "workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/workflow/WorkflowAction$Companion;

.field private static final NO_ACTION:Lcom/squareup/workflow/WorkflowAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/WorkflowAction<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 60
    new-instance v0, Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-direct {v0}, Lcom/squareup/workflow/WorkflowAction$Companion;-><init>()V

    sput-object v0, Lcom/squareup/workflow/WorkflowAction$Companion;->$$INSTANCE:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 232
    new-instance v0, Lcom/squareup/workflow/WorkflowAction$Companion$$special$$inlined$action$1;

    invoke-direct {v0}, Lcom/squareup/workflow/WorkflowAction$Companion$$special$$inlined$action$1;-><init>()V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    .line 235
    sput-object v0, Lcom/squareup/workflow/WorkflowAction$Companion;->NO_ACTION:Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 84
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState(Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    const/4 p3, 0x0

    .line 105
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic modifyState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Lkotlin/jvm/functions/Function0;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p4, p4, 0x2

    if-eqz p4, :cond_0

    const/4 p2, 0x0

    .line 124
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/workflow/WorkflowAction$Companion;->modifyState(Lkotlin/jvm/functions/Function0;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(TOutputT;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use action"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "action { setOutput(output) }"
            imports = {
                "com.squareup.workflow.action"
            }
        .end subannotation
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 224
    new-instance v0, Lcom/squareup/workflow/WorkflowAction$Companion$emitOutput$$inlined$action$1;

    invoke-direct {v0, p1, p1}, Lcom/squareup/workflow/WorkflowAction$Companion$emitOutput$$inlined$action$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method public final emitOutput(Ljava/lang/String;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TOutputT;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use action"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "action { setOutput(output) }"
            imports = {
                "com.squareup.workflow.action"
            }
        .end subannotation
    .end annotation

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "output"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    new-instance v0, Lcom/squareup/workflow/WorkflowAction$Companion$emitOutput$$inlined$action$2;

    invoke-direct {v0, p2, p1, p2}, Lcom/squareup/workflow/WorkflowAction$Companion$emitOutput$$inlined$action$2;-><init>(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method public final enterState(Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(TStateT;TOutputT;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use action"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "action { nextState = newState }"
            imports = {
                "com.squareup.workflow.action"
            }
        .end subannotation
    .end annotation

    .line 212
    new-instance v0, Lcom/squareup/workflow/WorkflowAction$Companion$enterState$$inlined$action$1;

    invoke-direct {v0, p1, p2, p1, p2}, Lcom/squareup/workflow/WorkflowAction$Companion$enterState$$inlined$action$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method public final enterState(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "TStateT;TOutputT;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use action"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "action { nextState = newState }"
            imports = {
                "com.squareup.workflow.action"
            }
        .end subannotation
    .end annotation

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    new-instance v0, Lcom/squareup/workflow/WorkflowAction$Companion$enterState$$inlined$action$2;

    move-object v1, v0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/squareup/workflow/WorkflowAction$Companion$enterState$$inlined$action$2;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method public final modifyState(Lkotlin/jvm/functions/Function0;Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/String;",
            ">;TOutputT;",
            "Lkotlin/jvm/functions/Function1<",
            "-TStateT;+TStateT;>;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use action"
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "action(name) { nextState = state }"
            imports = {
                "com.squareup.workflow.action"
            }
        .end subannotation
    .end annotation

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "modify"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    new-instance v0, Lcom/squareup/workflow/WorkflowAction$Companion$modifyState$$inlined$action$1;

    invoke-direct {v0, p3, p2, p1, p2}, Lcom/squareup/workflow/WorkflowAction$Companion$modifyState$$inlined$action$1;-><init>(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;Lkotlin/jvm/functions/Function0;Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method public final noAction()Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;TOutputT;>;"
        }
    .end annotation

    .line 69
    sget-object v0, Lcom/squareup/workflow/WorkflowAction$Companion;->NO_ACTION:Lcom/squareup/workflow/WorkflowAction;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type com.squareup.workflow.WorkflowAction<StateT, OutputT>"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
