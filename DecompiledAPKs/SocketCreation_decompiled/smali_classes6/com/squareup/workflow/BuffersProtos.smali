.class public final Lcom/squareup/workflow/BuffersProtos;
.super Ljava/lang/Object;
.source "BuffersProtos.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBuffersProtos.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,101:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u001a,\u0010\u0000\u001a\u0004\u0018\u0001H\u0001\"\u0016\u0008\u0000\u0010\u0001\u0018\u0001*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0002\u0008\u0003\u0018\u00010\u0002*\u00020\u0003H\u0086\u0008\u00a2\u0006\u0002\u0010\u0004\u001a*\u0010\u0005\u001a\u0002H\u0001\"\u0016\u0008\u0000\u0010\u0001\u0018\u0001*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0002\u0008\u0003\u0018\u00010\u0002*\u00020\u0003H\u0086\u0008\u00a2\u0006\u0002\u0010\u0004\u001a3\u0010\u0005\u001a\u0002H\u0001\"\u0014\u0008\u0000\u0010\u0001*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0002\u0008\u0003\u0018\u00010\u0002*\u00020\u00032\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00010\u0007\u00a2\u0006\u0002\u0010\u0008\u001aA\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00010\n\"\u0016\u0008\u0000\u0010\u0001\u0018\u0001*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u000b0\u0002\"\u0014\u0008\u0001\u0010\u000b*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u000b0\u000c*\u00020\u0003H\u0086\u0008\u001aJ\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\u00010\n\"\u0014\u0008\u0000\u0010\u0001*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u000b0\u0002\"\u0014\u0008\u0001\u0010\u000b*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u000b0\u000c*\u00020\u00032\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u0002H\u00010\u0007\u001aE\u0010\r\u001a\u00020\u000e\"\u0014\u0008\u0000\u0010\u0001*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u000b0\u0002\"\u0014\u0008\u0001\u0010\u000b*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u000b0\u000c*\u00020\u000e2\u0008\u0010\u000f\u001a\u0004\u0018\u0001H\u0001\u00a2\u0006\u0002\u0010\u0010\u001aC\u0010\u0011\u001a\u00020\u000e\"\u0014\u0008\u0000\u0010\u0001*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u000b0\u0002\"\u0014\u0008\u0001\u0010\u000b*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u000b0\u000c*\u00020\u000e2\u0006\u0010\u000f\u001a\u0002H\u0001\u00a2\u0006\u0002\u0010\u0010\u001aD\u0010\u0012\u001a\u00020\u000e\"\u0014\u0008\u0000\u0010\u0001*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u000b0\u0002\"\u0014\u0008\u0001\u0010\u000b*\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u000b0\u000c*\u00020\u000e2\u000c\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u0002H\u00010\n\u00a8\u0006\u0014"
    }
    d2 = {
        "readOptionalProtoWithLength",
        "M",
        "Lcom/squareup/wire/Message;",
        "Lokio/BufferedSource;",
        "(Lokio/BufferedSource;)Lcom/squareup/wire/Message;",
        "readProtoWithLength",
        "adapter",
        "Lcom/squareup/wire/ProtoAdapter;",
        "(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;",
        "readProtosWithLength",
        "",
        "B",
        "Lcom/squareup/wire/Message$Builder;",
        "writeOptionalProtoWithLength",
        "Lokio/BufferedSink;",
        "msg",
        "(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;",
        "writeProtoWithLength",
        "writeProtosWithLength",
        "msgs",
        "pure"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic readOptionalProtoWithLength(Lokio/BufferedSource;)Lcom/squareup/wire/Message;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/squareup/wire/Message<",
            "TM;*>;>(",
            "Lokio/BufferedSource;",
            ")TM;"
        }
    .end annotation

    const-string v0, "$this$readOptionalProtoWithLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const/4 v1, 0x4

    const-string v2, "M"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Lcom/squareup/wire/Message;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static final synthetic readProtoWithLength(Lokio/BufferedSource;)Lcom/squareup/wire/Message;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/squareup/wire/Message<",
            "TM;*>;>(",
            "Lokio/BufferedSource;",
            ")TM;"
        }
    .end annotation

    const-string v0, "$this$readProtoWithLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const/4 v1, 0x4

    const-string v2, "M"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Lcom/squareup/wire/Message;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p0

    return-object p0
.end method

.method public static final readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/squareup/wire/Message<",
            "TM;*>;>(",
            "Lokio/BufferedSource;",
            "Lcom/squareup/wire/ProtoAdapter<",
            "TM;>;)TM;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "$this$readProtoWithLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    int-to-long v0, v0

    .line 51
    invoke-interface {p0, v0, v1}, Lokio/BufferedSource;->readByteArray(J)[B

    move-result-object p0

    .line 52
    invoke-virtual {p1, p0}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wire/Message;

    return-object p0
.end method

.method public static final synthetic readProtosWithLength(Lokio/BufferedSource;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/squareup/wire/Message<",
            "TM;TB;>;B:",
            "Lcom/squareup/wire/Message$Builder<",
            "TM;TB;>;>(",
            "Lokio/BufferedSource;",
            ")",
            "Ljava/util/List<",
            "TM;>;"
        }
    .end annotation

    const-string v0, "$this$readProtosWithLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const/4 v1, 0x4

    const-string v2, "M"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Lcom/squareup/wire/Message;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/squareup/workflow/BuffersProtos;->readProtosWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final readProtosWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/squareup/wire/Message<",
            "TM;TB;>;B:",
            "Lcom/squareup/wire/Message$Builder<",
            "TM;TB;>;>(",
            "Lokio/BufferedSource;",
            "Lcom/squareup/wire/ProtoAdapter<",
            "TM;>;)",
            "Ljava/util/List<",
            "TM;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "$this$readProtosWithLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "adapter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 92
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    int-to-long v0, v0

    .line 93
    invoke-interface {p0, v0, v1}, Lokio/BufferedSource;->readByteArray(J)[B

    move-result-object p0

    .line 94
    invoke-static {p1, p0}, Lcom/squareup/util/ProtosPure;->decodeAsList(Lcom/squareup/wire/ProtoAdapter;[B)Ljava/util/List;

    move-result-object p0

    if-nez p0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object p0
.end method

.method public static final writeOptionalProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/squareup/wire/Message<",
            "TM;TB;>;B:",
            "Lcom/squareup/wire/Message$Builder<",
            "TM;TB;>;>(",
            "Lokio/BufferedSink;",
            "TM;)",
            "Lokio/BufferedSink;"
        }
    .end annotation

    const-string v0, "$this$writeOptionalProtoWithLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    .line 35
    invoke-static {p0, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 36
    invoke-static {p0, p1}, Lcom/squareup/workflow/BuffersProtos;->writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 37
    invoke-static {p0, p1}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    :goto_0
    return-object p0
.end method

.method public static final writeProtoWithLength(Lokio/BufferedSink;Lcom/squareup/wire/Message;)Lokio/BufferedSink;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/squareup/wire/Message<",
            "TM;TB;>;B:",
            "Lcom/squareup/wire/Message$Builder<",
            "TM;TB;>;>(",
            "Lokio/BufferedSink;",
            "TM;)",
            "Lokio/BufferedSink;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "$this$writeProtoWithLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "msg"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-virtual {p1}, Lcom/squareup/wire/Message;->encode()[B

    move-result-object p1

    .line 25
    array-length v0, p1

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 26
    invoke-interface {p0, p1}, Lokio/BufferedSink;->write([B)Lokio/BufferedSink;

    return-object p0
.end method

.method public static final writeProtosWithLength(Lokio/BufferedSink;Ljava/util/List;)Lokio/BufferedSink;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<M:",
            "Lcom/squareup/wire/Message<",
            "TM;TB;>;B:",
            "Lcom/squareup/wire/Message$Builder<",
            "TM;TB;>;>(",
            "Lokio/BufferedSink;",
            "Ljava/util/List<",
            "+TM;>;)",
            "Lokio/BufferedSink;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "$this$writeProtosWithLength"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "msgs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-static {p1}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Ljava/util/List;)[B

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 76
    :cond_0
    array-length v0, p1

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 77
    invoke-interface {p0, p1}, Lokio/BufferedSink;->write([B)Lokio/BufferedSink;

    return-object p0
.end method
