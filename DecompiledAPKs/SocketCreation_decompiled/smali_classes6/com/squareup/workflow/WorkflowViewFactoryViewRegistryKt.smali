.class public final Lcom/squareup/workflow/WorkflowViewFactoryViewRegistryKt;
.super Ljava/lang/Object;
.source "WorkflowViewFactoryViewRegistry.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "asViewRegistry",
        "Lcom/squareup/workflow/ui/ViewRegistry;",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "android_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final asViewRegistry(Lcom/squareup/workflow/WorkflowViewFactory;)Lcom/squareup/workflow/ui/ViewRegistry;
    .locals 1

    const-string v0, "$this$asViewRegistry"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v0, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/WorkflowViewFactoryViewRegistry;-><init>(Lcom/squareup/workflow/WorkflowViewFactory;)V

    check-cast v0, Lcom/squareup/workflow/ui/ViewRegistry;

    return-object v0
.end method
