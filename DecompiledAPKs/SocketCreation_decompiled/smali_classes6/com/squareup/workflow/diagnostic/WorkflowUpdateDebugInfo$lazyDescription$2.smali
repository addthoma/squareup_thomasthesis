.class final Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$lazyDescription$2;
.super Lkotlin/jvm/internal/Lambda;
.source "WorkflowUpdateDebugInfo.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;-><init>(Ljava/lang/String;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$Kind;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowUpdateDebugInfo.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowUpdateDebugInfo.kt\ncom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$lazyDescription$2\n*L\n1#1,168:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$lazyDescription$2;->this$0:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$lazyDescription$2;->invoke()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Ljava/lang/String;
    .locals 2

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    iget-object v1, p0, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo$lazyDescription$2;->this$0:Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;

    invoke-static {v0, v1}, Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfoKt;->access$writeUpdate(Ljava/lang/StringBuilder;Lcom/squareup/workflow/diagnostic/WorkflowUpdateDebugInfo;)V

    .line 117
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "StringBuilder().apply(builderAction).toString()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
