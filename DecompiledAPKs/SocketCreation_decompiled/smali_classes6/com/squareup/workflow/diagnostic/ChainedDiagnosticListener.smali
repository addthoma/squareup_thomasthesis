.class public final Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;
.super Ljava/lang/Object;
.source "ChainedDiagnosticListener.kt"

# interfaces
.implements Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChainedDiagnosticListener.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChainedDiagnosticListener.kt\ncom/squareup/workflow/diagnostic/ChainedDiagnosticListener\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,163:1\n1591#2,2:164\n1591#2,2:166\n1591#2,2:168\n1591#2,2:170\n1591#2,2:172\n1591#2,2:174\n1591#2,2:176\n1591#2,2:178\n1591#2,2:180\n1591#2,2:182\n1591#2,2:184\n1591#2,2:186\n1591#2,2:188\n1591#2,2:190\n1591#2,2:192\n1591#2,2:194\n*E\n*S KotlinDebug\n*F\n+ 1 ChainedDiagnosticListener.kt\ncom/squareup/workflow/diagnostic/ChainedDiagnosticListener\n*L\n51#1,2:164\n61#1,2:166\n69#1,2:168\n76#1,2:170\n80#1,2:172\n84#1,2:174\n88#1,2:176\n95#1,2:178\n99#1,2:180\n111#1,2:182\n119#1,2:184\n128#1,2:186\n135#1,2:188\n143#1,2:190\n150#1,2:192\n160#1,2:194\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010!\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0008\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0002\u001a\u00020\u0001J\u0012\u0010\u0008\u001a\u00020\u00072\u0008\u0010\t\u001a\u0004\u0018\u00010\nH\u0016J\u0008\u0010\u000b\u001a\u00020\u0007H\u0016J\u001a\u0010\u000c\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010\t\u001a\u0004\u0018\u00010\nH\u0016J\u0012\u0010\u000f\u001a\u00020\u00072\u0008\u0010\u0010\u001a\u0004\u0018\u00010\nH\u0016J\u0008\u0010\u0011\u001a\u00020\u0007H\u0016J$\u0010\u0012\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\n2\u0008\u0010\u0013\u001a\u0004\u0018\u00010\nH\u0016J?\u0010\u0014\u001a\u00020\u00072\u0008\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0008\u0010\u0015\u001a\u0004\u0018\u00010\n2\u0008\u0010\u0016\u001a\u0004\u0018\u00010\n2\u0008\u0010\u0017\u001a\u0004\u0018\u00010\n2\u0008\u0010\u0018\u001a\u0004\u0018\u00010\nH\u0016\u00a2\u0006\u0002\u0010\u0019J\u0018\u0010\u001a\u001a\u00020\u00072\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0008\u0010\u001f\u001a\u00020\u0007H\u0016J \u0010 \u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u000e2\u000e\u0010!\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"H\u0016J \u0010#\u001a\u00020\u00072\u0006\u0010$\u001a\u00020\u000e2\u0006\u0010%\u001a\u00020\u000e2\u0006\u0010&\u001a\u00020\nH\u0016J(\u0010\'\u001a\u00020\u00072\u0006\u0010$\u001a\u00020\u000e2\u0006\u0010%\u001a\u00020\u000e2\u0006\u0010(\u001a\u00020\u001e2\u0006\u0010)\u001a\u00020\u001eH\u0016J\u0018\u0010*\u001a\u00020\u00072\u0006\u0010$\u001a\u00020\u000e2\u0006\u0010%\u001a\u00020\u000eH\u0016J>\u0010+\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u000e2\u000e\u0010!\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\"2\u0008\u0010\u0017\u001a\u0004\u0018\u00010\n2\u0008\u0010\u0018\u001a\u0004\u0018\u00010\n2\u0008\u0010&\u001a\u0004\u0018\u00010\nH\u0016JK\u0010,\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u000e2\u0008\u0010-\u001a\u0004\u0018\u00010\u000e2\u0006\u0010.\u001a\u00020\u001e2\u0006\u0010(\u001a\u00020\u001e2\u0008\u0010/\u001a\u0004\u0018\u00010\n2\u0008\u00100\u001a\u0004\u0018\u00010\n2\u0006\u00101\u001a\u000202H\u0016\u00a2\u0006\u0002\u00103J\u0010\u00104\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;",
        "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
        "listener",
        "(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V",
        "visitors",
        "",
        "addVisitor",
        "",
        "onAfterRenderPass",
        "rendering",
        "",
        "onAfterSnapshotPass",
        "onAfterWorkflowRendered",
        "workflowId",
        "",
        "onBeforeRenderPass",
        "props",
        "onBeforeSnapshotPass",
        "onBeforeWorkflowRendered",
        "state",
        "onPropsChanged",
        "oldProps",
        "newProps",
        "oldState",
        "newState",
        "(Ljava/lang/Long;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V",
        "onRuntimeStarted",
        "workflowScope",
        "Lkotlinx/coroutines/CoroutineScope;",
        "rootWorkflowType",
        "",
        "onRuntimeStopped",
        "onSinkReceived",
        "action",
        "Lcom/squareup/workflow/WorkflowAction;",
        "onWorkerOutput",
        "workerId",
        "parentWorkflowId",
        "output",
        "onWorkerStarted",
        "key",
        "description",
        "onWorkerStopped",
        "onWorkflowAction",
        "onWorkflowStarted",
        "parentId",
        "workflowType",
        "initialProps",
        "initialState",
        "restoredFromSnapshot",
        "",
        "(JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Z)V",
        "onWorkflowStopped",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final visitors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V
    .locals 2

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 40
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->mutableListOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final addVisitor(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V
    .locals 1

    const-string v0, "listener"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    instance-of v0, p1, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast p1, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;

    iget-object p1, p1, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast p1, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method

.method public onAfterRenderPass(Ljava/lang/Object;)V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 172
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    .line 80
    invoke-interface {v1, p1}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onAfterRenderPass(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onAfterSnapshotPass()V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 176
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    .line 88
    invoke-interface {v1}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onAfterSnapshotPass()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onAfterWorkflowRendered(JLjava/lang/Object;)V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 170
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    .line 76
    invoke-interface {v1, p1, p2, p3}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onAfterWorkflowRendered(JLjava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onBeforeRenderPass(Ljava/lang/Object;)V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 164
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    .line 51
    invoke-interface {v1, p1}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onBeforeRenderPass(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onBeforeSnapshotPass()V
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 174
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    .line 84
    invoke-interface {v1}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onBeforeSnapshotPass()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onBeforeWorkflowRendered(JLjava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 168
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    .line 69
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onBeforeWorkflowRendered(JLjava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onPropsChanged(Ljava/lang/Long;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 8

    .line 61
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 166
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    .line 61
    invoke-interface/range {v2 .. v7}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onPropsChanged(Ljava/lang/Long;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onRuntimeStarted(Lkotlinx/coroutines/CoroutineScope;Ljava/lang/String;)V
    .locals 2

    const-string v0, "workflowScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rootWorkflowType"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 178
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    .line 95
    invoke-interface {v1, p1, p2}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onRuntimeStarted(Lkotlinx/coroutines/CoroutineScope;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onRuntimeStopped()V
    .locals 2

    .line 99
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 180
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    .line 99
    invoke-interface {v1}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onRuntimeStopped()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onSinkReceived(JLcom/squareup/workflow/WorkflowAction;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/workflow/WorkflowAction<",
            "**>;)V"
        }
    .end annotation

    const-string v0, "action"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 192
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    .line 150
    invoke-interface {v1, p1, p2, p3}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onSinkReceived(JLcom/squareup/workflow/WorkflowAction;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onWorkerOutput(JJLjava/lang/Object;)V
    .locals 8

    const-string v0, "output"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 190
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    move-wide v3, p1

    move-wide v5, p3

    move-object v7, p5

    .line 143
    invoke-interface/range {v2 .. v7}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onWorkerOutput(JJLjava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onWorkerStarted(JJLjava/lang/String;Ljava/lang/String;)V
    .locals 9

    const-string v0, "key"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "description"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 186
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    move-wide v3, p1

    move-wide v5, p3

    move-object v7, p5

    move-object v8, p6

    .line 128
    invoke-interface/range {v2 .. v8}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onWorkerStarted(JJLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onWorkerStopped(JJ)V
    .locals 2

    .line 135
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 188
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    .line 135
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onWorkerStopped(JJ)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onWorkflowAction(JLcom/squareup/workflow/WorkflowAction;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/squareup/workflow/WorkflowAction<",
            "**>;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    const-string v0, "action"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 194
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    move-wide v3, p1

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move-object v8, p6

    .line 160
    invoke-interface/range {v2 .. v8}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onWorkflowAction(JLcom/squareup/workflow/WorkflowAction;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onWorkflowStarted(JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Z)V
    .locals 13

    const-string v0, "workflowType"

    move-object/from16 v10, p4

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "key"

    move-object/from16 v11, p5

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    .line 111
    iget-object v1, v0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 182
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    move-wide v2, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    .line 112
    invoke-interface/range {v1 .. v9}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onWorkflowStarted(JLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onWorkflowStopped(J)V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/workflow/diagnostic/ChainedDiagnosticListener;->visitors:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 184
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    .line 119
    invoke-interface {v1, p1, p2}, Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;->onWorkflowStopped(J)V

    goto :goto_0

    :cond_0
    return-void
.end method
