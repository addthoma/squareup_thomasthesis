.class public final Lcom/squareup/workflow/Snapshot;
.super Ljava/lang/Object;
.source "Snapshot.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/Snapshot$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSnapshot.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Snapshot.kt\ncom/squareup/workflow/Snapshot\n*L\n1#1,181:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u0015\u0008\u0002\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u0013\u0010\n\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u0001H\u0096\u0002J\u0008\u0010\r\u001a\u00020\u000eH\u0016J\u0008\u0010\u000f\u001a\u00020\u0010H\u0016R\u001b\u0010\u0006\u001a\u00020\u00048GX\u0086\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0008\u0010\t\u001a\u0004\u0008\u0006\u0010\u0007R\u0014\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/workflow/Snapshot;",
        "",
        "toByteString",
        "Lkotlin/Function0;",
        "Lokio/ByteString;",
        "(Lkotlin/jvm/functions/Function0;)V",
        "bytes",
        "()Lokio/ByteString;",
        "bytes$delegate",
        "Lkotlin/Lazy;",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "Companion",
        "workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field public static final Companion:Lcom/squareup/workflow/Snapshot$Companion;

.field public static final EMPTY:Lcom/squareup/workflow/Snapshot;


# instance fields
.field private final bytes$delegate:Lkotlin/Lazy;

.field private final toByteString:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lokio/ByteString;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/workflow/Snapshot;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "bytes"

    const-string v4, "bytes()Lokio/ByteString;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/workflow/Snapshot;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    new-instance v0, Lcom/squareup/workflow/Snapshot$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    .line 37
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    sget-object v1, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object v0

    sput-object v0, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-void
.end method

.method private constructor <init>(Lkotlin/jvm/functions/Function0;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Lokio/ByteString;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/Snapshot;->toByteString:Lkotlin/jvm/functions/Function0;

    .line 71
    new-instance p1, Lcom/squareup/workflow/Snapshot$bytes$2;

    invoke-direct {p1, p0}, Lcom/squareup/workflow/Snapshot$bytes$2;-><init>(Lcom/squareup/workflow/Snapshot;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/Snapshot;->bytes$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/workflow/Snapshot;-><init>(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public static final synthetic access$getToByteString$p(Lcom/squareup/workflow/Snapshot;)Lkotlin/jvm/functions/Function0;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/workflow/Snapshot;->toByteString:Lkotlin/jvm/functions/Function0;

    return-object p0
.end method

.method public static final of(I)Lcom/squareup/workflow/Snapshot;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/Snapshot$Companion;->of(I)Lcom/squareup/workflow/Snapshot;

    move-result-object p0

    return-object p0
.end method

.method public static final of(Ljava/lang/String;)Lcom/squareup/workflow/Snapshot;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/Snapshot$Companion;->of(Ljava/lang/String;)Lcom/squareup/workflow/Snapshot;

    move-result-object p0

    return-object p0
.end method

.method public static final of(Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/Snapshot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Lokio/ByteString;",
            ">;)",
            "Lcom/squareup/workflow/Snapshot;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/Snapshot;

    move-result-object p0

    return-object p0
.end method

.method public static final of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object p0

    return-object p0
.end method

.method public static final write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lokio/BufferedSink;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/Snapshot;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final bytes()Lokio/ByteString;
    .locals 3

    iget-object v0, p0, Lcom/squareup/workflow/Snapshot;->bytes$delegate:Lkotlin/Lazy;

    sget-object v1, Lcom/squareup/workflow/Snapshot;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lokio/ByteString;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 85
    instance-of v0, p1, Lcom/squareup/workflow/Snapshot;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    check-cast p1, Lcom/squareup/workflow/Snapshot;

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Snapshot("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
