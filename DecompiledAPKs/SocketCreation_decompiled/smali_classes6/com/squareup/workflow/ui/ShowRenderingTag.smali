.class public final Lcom/squareup/workflow/ui/ShowRenderingTag;
.super Ljava/lang/Object;
.source "ViewShowRendering.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RenderingT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\n\u0008\u0000\u0010\u0001 \u0001*\u00020\u00022\u00020\u0002B9\u0012\u0006\u0010\u0003\u001a\u00028\u0000\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\"\u0010\u0006\u001a\u001e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00080\u0007j\u0008\u0012\u0004\u0012\u00028\u0000`\t\u00a2\u0006\u0002\u0010\nJ\u000e\u0010\u0012\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0010J\t\u0010\u0013\u001a\u00020\u0005H\u00c6\u0003J%\u0010\u0014\u001a\u001e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00080\u0007j\u0008\u0012\u0004\u0012\u00028\u0000`\tH\u00c6\u0003JN\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00028\u00002\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052$\u0008\u0002\u0010\u0006\u001a\u001e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00080\u0007j\u0008\u0012\u0004\u0012\u00028\u0000`\tH\u00c6\u0001\u00a2\u0006\u0002\u0010\u0016J\u0013\u0010\u0017\u001a\u00020\u00182\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR-\u0010\u0006\u001a\u001e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00080\u0007j\u0008\u0012\u0004\u0012\u00028\u0000`\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0013\u0010\u0003\u001a\u00028\u0000\u00a2\u0006\n\n\u0002\u0010\u0011\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/ShowRenderingTag;",
        "RenderingT",
        "",
        "showing",
        "hints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "showRendering",
        "Lkotlin/Function2;",
        "",
        "Lcom/squareup/workflow/ui/ViewShowRendering;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;)V",
        "getHints",
        "()Lcom/squareup/workflow/ui/ContainerHints;",
        "getShowRendering",
        "()Lkotlin/jvm/functions/Function2;",
        "getShowing",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "component1",
        "component2",
        "component3",
        "copy",
        "(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/ui/ShowRenderingTag;",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final hints:Lcom/squareup/workflow/ui/ContainerHints;

.field private final showRendering:Lkotlin/jvm/functions/Function2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function2<",
            "TRenderingT;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final showing:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TRenderingT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRenderingT;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Lkotlin/jvm/functions/Function2<",
            "-TRenderingT;-",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "showing"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showRendering"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->showing:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->hints:Lcom/squareup/workflow/ui/ContainerHints;

    iput-object p3, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->showRendering:Lkotlin/jvm/functions/Function2;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/workflow/ui/ShowRenderingTag;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lcom/squareup/workflow/ui/ShowRenderingTag;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->showing:Ljava/lang/Object;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->hints:Lcom/squareup/workflow/ui/ContainerHints;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->showRendering:Lkotlin/jvm/functions/Function2;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/workflow/ui/ShowRenderingTag;->copy(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/ui/ShowRenderingTag;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRenderingT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->showing:Ljava/lang/Object;

    return-object v0
.end method

.method public final component2()Lcom/squareup/workflow/ui/ContainerHints;
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->hints:Lcom/squareup/workflow/ui/ContainerHints;

    return-object v0
.end method

.method public final component3()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "TRenderingT;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->showRendering:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/ui/ShowRenderingTag;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRenderingT;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Lkotlin/jvm/functions/Function2<",
            "-TRenderingT;-",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/ui/ShowRenderingTag<",
            "TRenderingT;>;"
        }
    .end annotation

    const-string v0, "showing"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "showRendering"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/ui/ShowRenderingTag;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/workflow/ui/ShowRenderingTag;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function2;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/workflow/ui/ShowRenderingTag;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/workflow/ui/ShowRenderingTag;

    iget-object v0, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->showing:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/workflow/ui/ShowRenderingTag;->showing:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->hints:Lcom/squareup/workflow/ui/ContainerHints;

    iget-object v1, p1, Lcom/squareup/workflow/ui/ShowRenderingTag;->hints:Lcom/squareup/workflow/ui/ContainerHints;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->showRendering:Lkotlin/jvm/functions/Function2;

    iget-object p1, p1, Lcom/squareup/workflow/ui/ShowRenderingTag;->showRendering:Lkotlin/jvm/functions/Function2;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getHints()Lcom/squareup/workflow/ui/ContainerHints;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->hints:Lcom/squareup/workflow/ui/ContainerHints;

    return-object v0
.end method

.method public final getShowRendering()Lkotlin/jvm/functions/Function2;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function2<",
            "TRenderingT;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->showRendering:Lkotlin/jvm/functions/Function2;

    return-object v0
.end method

.method public final getShowing()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRenderingT;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->showing:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->showing:Ljava/lang/Object;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->hints:Lcom/squareup/workflow/ui/ContainerHints;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->showRendering:Lkotlin/jvm/functions/Function2;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ShowRenderingTag(showing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->showing:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", hints="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->hints:Lcom/squareup/workflow/ui/ContainerHints;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showRendering="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/ui/ShowRenderingTag;->showRendering:Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
