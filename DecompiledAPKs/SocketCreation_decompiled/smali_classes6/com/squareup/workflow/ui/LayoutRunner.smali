.class public interface abstract Lcom/squareup/workflow/ui/LayoutRunner;
.super Ljava/lang/Object;
.source "LayoutRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/ui/LayoutRunner$Binding;,
        Lcom/squareup/workflow/ui/LayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RenderingT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u0000 \n*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0002:\u0002\t\nJ\u001d\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00028\u00002\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a2\u0006\u0002\u0010\u0008\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "RenderingT",
        "",
        "showRendering",
        "",
        "rendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V",
        "Binding",
        "Companion",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/workflow/ui/LayoutRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/workflow/ui/LayoutRunner$Companion;->$$INSTANCE:Lcom/squareup/workflow/ui/LayoutRunner$Companion;

    sput-object v0, Lcom/squareup/workflow/ui/LayoutRunner;->Companion:Lcom/squareup/workflow/ui/LayoutRunner$Companion;

    return-void
.end method


# virtual methods
.method public abstract showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRenderingT;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")V"
        }
    .end annotation
.end method
