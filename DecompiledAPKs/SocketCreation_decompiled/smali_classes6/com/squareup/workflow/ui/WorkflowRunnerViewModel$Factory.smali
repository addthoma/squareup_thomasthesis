.class public final Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;
.super Ljava/lang/Object;
.source "WorkflowRunnerViewModel.kt"

# interfaces
.implements Landroidx/lifecycle/ViewModelProvider$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PropsT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroidx/lifecycle/ViewModelProvider$Factory;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0000\u0018\u0000*\u0004\u0008\u0001\u0010\u0001*\u0008\u0008\u0002\u0010\u0002*\u00020\u00032\u00020\u0004B\'\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0018\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\t0\u0008\u00a2\u0006\u0002\u0010\nJ%\u0010\r\u001a\u0002H\u000e\"\u0008\u0008\u0003\u0010\u000e*\u00020\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u0002H\u000e0\u0011H\u0016\u00a2\u0006\u0002\u0010\u0012R \u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\t0\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;",
        "PropsT",
        "OutputT",
        "",
        "Landroidx/lifecycle/ViewModelProvider$Factory;",
        "savedStateRegistry",
        "Landroidx/savedstate/SavedStateRegistry;",
        "configure",
        "Lkotlin/Function0;",
        "Lcom/squareup/workflow/ui/WorkflowRunner$Config;",
        "(Landroidx/savedstate/SavedStateRegistry;Lkotlin/jvm/functions/Function0;)V",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "create",
        "T",
        "Landroidx/lifecycle/ViewModel;",
        "modelClass",
        "Ljava/lang/Class;",
        "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final configure:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lcom/squareup/workflow/ui/WorkflowRunner$Config<",
            "TPropsT;TOutputT;>;>;"
        }
    .end annotation
.end field

.field private final savedStateRegistry:Landroidx/savedstate/SavedStateRegistry;

.field private final snapshot:Lcom/squareup/workflow/Snapshot;


# direct methods
.method public constructor <init>(Landroidx/savedstate/SavedStateRegistry;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/savedstate/SavedStateRegistry;",
            "Lkotlin/jvm/functions/Function0<",
            "Lcom/squareup/workflow/ui/WorkflowRunner$Config<",
            "TPropsT;TOutputT;>;>;)V"
        }
    .end annotation

    const-string v0, "savedStateRegistry"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;->savedStateRegistry:Landroidx/savedstate/SavedStateRegistry;

    iput-object p2, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;->configure:Lkotlin/jvm/functions/Function0;

    .line 48
    iget-object p1, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;->savedStateRegistry:Landroidx/savedstate/SavedStateRegistry;

    .line 49
    invoke-static {}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->access$Companion()Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Companion;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Companion;->getBUNDLE_KEY()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroidx/savedstate/SavedStateRegistry;->consumeRestoredStateForKey(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 50
    invoke-static {}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->access$Companion()Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Companion;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Companion;->getBUNDLE_KEY()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/ui/PickledWorkflow;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/workflow/ui/PickledWorkflow;->getSnapshot$core_android_release()Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;->snapshot:Lcom/squareup/workflow/Snapshot;

    return-void
.end method

.method public static final synthetic access$getSavedStateRegistry$p(Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;)Landroidx/savedstate/SavedStateRegistry;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;->savedStateRegistry:Landroidx/savedstate/SavedStateRegistry;

    return-object p0
.end method


# virtual methods
.method public create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroidx/lifecycle/ViewModel;",
            ">(",
            "Ljava/lang/Class<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    iget-object p1, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;->configure:Lkotlin/jvm/functions/Function0;

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/ui/WorkflowRunner$Config;

    .line 56
    invoke-virtual {p1}, Lcom/squareup/workflow/ui/WorkflowRunner$Config;->getDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    check-cast v0, Lkotlin/coroutines/CoroutineContext;

    invoke-static {v0}, Lkotlinx/coroutines/CoroutineScopeKt;->CoroutineScope(Lkotlin/coroutines/CoroutineContext;)Lkotlinx/coroutines/CoroutineScope;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/workflow/ui/WorkflowRunner$Config;->getWorkflow()Lcom/squareup/workflow/Workflow;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/workflow/ui/WorkflowRunner$Config;->getProps()Lkotlinx/coroutines/flow/Flow;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;->snapshot:Lcom/squareup/workflow/Snapshot;

    .line 57
    new-instance v4, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory$create$1;

    invoke-direct {v4, p0, p1}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory$create$1;-><init>(Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;Lcom/squareup/workflow/ui/WorkflowRunner$Config;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    .line 55
    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/workflow/LaunchWorkflowKt;->launchWorkflowIn(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Snapshot;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/lifecycle/ViewModel;

    return-object p1
.end method
