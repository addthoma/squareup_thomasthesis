.class public final Lcom/squareup/workflow/ui/backstack/BackStackScreen;
.super Ljava/lang/Object;
.source "BackStackScreen.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<StackedT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBackStackScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BackStackScreen.kt\ncom/squareup/workflow/ui/backstack/BackStackScreen\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,92:1\n1313#2:93\n1382#2,3:94\n1323#2:97\n1354#2,4:98\n*E\n*S KotlinDebug\n*F\n+ 1 BackStackScreen.kt\ncom/squareup/workflow/ui/backstack/BackStackScreen\n*L\n61#1:93\n61#1,3:94\n66#1:97\n66#1,4:98\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010 \n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0002B#\u0008\u0016\u0012\u0006\u0010\u0003\u001a\u00028\u0000\u0012\u0012\u0010\u0004\u001a\n\u0012\u0006\u0008\u0001\u0012\u00028\u00000\u0005\"\u00028\u0000\u00a2\u0006\u0002\u0010\u0006B\u001b\u0012\u0006\u0010\u0003\u001a\u00028\u0000\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0002\u0010\u0008J\u0013\u0010\u0012\u001a\u00020\u00132\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0002H\u0096\u0002J\u0016\u0010\u0015\u001a\u00028\u00002\u0006\u0010\u0016\u001a\u00020\u0017H\u0086\u0002\u00a2\u0006\u0002\u0010\u0018J\u0008\u0010\u0019\u001a\u00020\u0017H\u0016J*\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0\u0000\"\u0008\u0008\u0001\u0010\u001b*\u00020\u00022\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u0002H\u001b0\u001dJ?\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u0002H\u001b0\u0000\"\u0008\u0008\u0001\u0010\u001b*\u00020\u00022\'\u0010\u001c\u001a#\u0012\u0013\u0012\u00110\u0017\u00a2\u0006\u000c\u0008 \u0012\u0008\u0008!\u0012\u0004\u0008\u0008(\u0016\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u0002H\u001b0\u001fJ\u001f\u0010\"\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u000e\u0010\u0014\u001a\n\u0012\u0004\u0012\u00028\u0000\u0018\u00010\u0000H\u0086\u0002J\u0008\u0010#\u001a\u00020$H\u0016R\u0017\u0010\t\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0017\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000bR\u0013\u0010\u000e\u001a\u00028\u0000\u00a2\u0006\n\n\u0002\u0010\u0011\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/backstack/BackStackScreen;",
        "StackedT",
        "",
        "bottom",
        "rest",
        "",
        "(Ljava/lang/Object;[Ljava/lang/Object;)V",
        "",
        "(Ljava/lang/Object;Ljava/util/List;)V",
        "backStack",
        "getBackStack",
        "()Ljava/util/List;",
        "frames",
        "getFrames",
        "top",
        "getTop",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "equals",
        "",
        "other",
        "get",
        "index",
        "",
        "(I)Ljava/lang/Object;",
        "hashCode",
        "map",
        "R",
        "transform",
        "Lkotlin/Function1;",
        "mapIndexed",
        "Lkotlin/Function2;",
        "Lkotlin/ParameterName;",
        "name",
        "plus",
        "toString",
        "",
        "backstack-common"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final backStack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TStackedT;>;"
        }
    .end annotation
.end field

.field private final frames:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TStackedT;>;"
        }
    .end annotation
.end field

.field private final top:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TStackedT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Object;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TStackedT;",
            "Ljava/util/List<",
            "+TStackedT;>;)V"
        }
    .end annotation

    const-string v0, "bottom"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rest"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/util/Collection;

    check-cast p2, Ljava/lang/Iterable;

    invoke-static {p1, p2}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->frames:Ljava/util/List;

    .line 46
    iget-object p1, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->frames:Ljava/util/List;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->last(Ljava/util/List;)Ljava/lang/Object;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->top:Ljava/lang/Object;

    .line 51
    iget-object p1, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->frames:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    const/4 v0, 0x0

    invoke-interface {p1, v0, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->backStack:Ljava/util/List;

    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/Object;[Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TStackedT;[TStackedT;)V"
        }
    .end annotation

    const-string v0, "bottom"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rest"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-static {p2}, Lkotlin/collections/ArraysKt;->toList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-direct {p0, p1, p2}, Lcom/squareup/workflow/ui/backstack/BackStackScreen;-><init>(Ljava/lang/Object;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 71
    instance-of v0, p1, Lcom/squareup/workflow/ui/backstack/BackStackScreen;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object p1, v1

    :cond_0
    check-cast p1, Lcom/squareup/workflow/ui/backstack/BackStackScreen;

    if-eqz p1, :cond_1

    iget-object v1, p1, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->frames:Ljava/util/List;

    :cond_1
    iget-object p1, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->frames:Ljava/util/List;

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TStackedT;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->frames:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final getBackStack()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TStackedT;>;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->backStack:Ljava/util/List;

    return-object v0
.end method

.method public final getFrames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TStackedT;>;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->frames:Ljava/util/List;

    return-object v0
.end method

.method public final getTop()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TStackedT;"
        }
    .end annotation

    .line 46
    iget-object v0, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->top:Ljava/lang/Object;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->frames:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public final map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/ui/backstack/BackStackScreen;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TStackedT;+TR;>;)",
            "Lcom/squareup/workflow/ui/backstack/BackStackScreen<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->frames:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 93
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 94
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 95
    invoke-interface {p1, v2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 96
    :cond_0
    check-cast v1, Ljava/util/List;

    .line 62
    invoke-static {v1}, Lcom/squareup/workflow/ui/backstack/BackStackScreenKt;->toBackStackScreen(Ljava/util/List;)Lcom/squareup/workflow/ui/backstack/BackStackScreen;

    move-result-object p1

    return-object p1
.end method

.method public final mapIndexed(Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/ui/backstack/BackStackScreen;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Ljava/lang/Integer;",
            "-TStackedT;+TR;>;)",
            "Lcom/squareup/workflow/ui/backstack/BackStackScreen<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->frames:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 97
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 99
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    add-int/lit8 v4, v2, 0x1

    if-gez v2, :cond_0

    .line 100
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p1, v2, v3}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v2, v4

    goto :goto_0

    .line 101
    :cond_1
    check-cast v1, Ljava/util/List;

    .line 67
    invoke-static {v1}, Lcom/squareup/workflow/ui/backstack/BackStackScreenKt;->toBackStackScreen(Ljava/util/List;)Lcom/squareup/workflow/ui/backstack/BackStackScreen;

    move-result-object p1

    return-object p1
.end method

.method public final plus(Lcom/squareup/workflow/ui/backstack/BackStackScreen;)Lcom/squareup/workflow/ui/backstack/BackStackScreen;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/ui/backstack/BackStackScreen<",
            "TStackedT;>;)",
            "Lcom/squareup/workflow/ui/backstack/BackStackScreen<",
            "TStackedT;>;"
        }
    .end annotation

    if-nez p1, :cond_0

    move-object v0, p0

    goto :goto_0

    .line 57
    :cond_0
    new-instance v0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;

    iget-object v1, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->frames:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->frames:Ljava/util/List;

    const/4 v3, 0x1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v2, v3, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    iget-object p1, p1, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->frames:Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {v2, p1}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/workflow/ui/backstack/BackStackScreen;-><init>(Ljava/lang/Object;Ljava/util/List;)V

    :goto_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/ui/backstack/BackStackScreen;->frames:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
