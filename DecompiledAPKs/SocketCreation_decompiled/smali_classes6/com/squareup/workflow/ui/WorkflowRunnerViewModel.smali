.class public final Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "WorkflowRunnerViewModel.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/WorkflowRunner;
.implements Landroidx/savedstate/SavedStateRegistry$SavedStateProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;,
        Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<OutputT:",
        "Ljava/lang/Object;",
        ">",
        "Landroidx/lifecycle/ViewModel;",
        "Lcom/squareup/workflow/ui/WorkflowRunner<",
        "TOutputT;>;",
        "Landroidx/savedstate/SavedStateRegistry$SavedStateProvider;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowRunnerViewModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowRunnerViewModel.kt\ncom/squareup/workflow/ui/WorkflowRunnerViewModel\n+ 2 Transform.kt\nkotlinx/coroutines/flow/FlowKt__TransformKt\n+ 3 Emitters.kt\nkotlinx/coroutines/flow/FlowKt__EmittersKt\n+ 4 SafeCollector.kt\nkotlinx/coroutines/flow/internal/SafeCollectorKt\n*L\n1#1,113:1\n47#2:114\n49#2:118\n47#2:119\n49#2:123\n51#3:115\n56#3:117\n51#3:120\n56#3:122\n119#4:116\n119#4:121\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowRunnerViewModel.kt\ncom/squareup/workflow/ui/WorkflowRunnerViewModel\n*L\n78#1:114\n78#1:118\n87#1:119\n87#1:123\n78#1:115\n78#1:117\n87#1:120\n87#1:122\n78#1:116\n87#1:121\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u0000  *\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u00032\u0008\u0012\u0004\u0012\u0002H\u00010\u00042\u00020\u0005:\u0002 !B!\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00020\t\u00a2\u0006\u0002\u0010\nJ\r\u0010\u0018\u001a\u00020\u0019H\u0000\u00a2\u0006\u0002\u0008\u001aJ\r\u0010\u001b\u001a\u00020\u000cH\u0000\u00a2\u0006\u0002\u0008\u001cJ\u0008\u0010\u001d\u001a\u00020\u0019H\u0014J\u0008\u0010\u001e\u001a\u00020\u001fH\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\r\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00020\u000eX\u0096\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u000f\u0010\u0010\u001a\u0004\u0008\u0011\u0010\u0012R\"\u0010\u0013\u001a\n\u0012\u0006\u0008\u0001\u0012\u00028\u00000\u0014X\u0096\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0015\u0010\u0010\u001a\u0004\u0008\u0016\u0010\u0017R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;",
        "OutputT",
        "",
        "Landroidx/lifecycle/ViewModel;",
        "Lcom/squareup/workflow/ui/WorkflowRunner;",
        "Landroidx/savedstate/SavedStateRegistry$SavedStateProvider;",
        "scope",
        "Lkotlinx/coroutines/CoroutineScope;",
        "session",
        "Lcom/squareup/workflow/WorkflowSession;",
        "(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)V",
        "lastSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "renderings",
        "Lio/reactivex/Observable;",
        "renderings$annotations",
        "()V",
        "getRenderings",
        "()Lio/reactivex/Observable;",
        "result",
        "Lio/reactivex/Maybe;",
        "result$annotations",
        "getResult",
        "()Lio/reactivex/Maybe;",
        "clearForTest",
        "",
        "clearForTest$core_android_release",
        "getLastSnapshotForTest",
        "getLastSnapshotForTest$core_android_release",
        "onCleared",
        "saveState",
        "Landroid/os/Bundle;",
        "Companion",
        "Factory",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final BUNDLE_KEY:Ljava/lang/String;

.field public static final Companion:Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private lastSnapshot:Lcom/squareup/workflow/Snapshot;

.field private final renderings:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final result:Lio/reactivex/Maybe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Maybe<",
            "+TOutputT;>;"
        }
    .end annotation
.end field

.field private final scope:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->Companion:Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Companion;

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/squareup/workflow/ui/WorkflowRunner;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-workflow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->BUNDLE_KEY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/CoroutineScope;",
            "Lcom/squareup/workflow/WorkflowSession<",
            "+TOutputT;+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "session"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->scope:Lkotlinx/coroutines/CoroutineScope;

    .line 68
    invoke-virtual {p2}, Lcom/squareup/workflow/WorkflowSession;->getOutputs()Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    invoke-static {p1}, Lkotlinx/coroutines/rx2/RxConvertKt;->from(Lkotlinx/coroutines/flow/Flow;)Lio/reactivex/Observable;

    move-result-object p1

    .line 69
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstElement()Lio/reactivex/Maybe;

    move-result-object p1

    .line 70
    new-instance v0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$result$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$result$1;-><init>(Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;)V

    check-cast v0, Lio/reactivex/functions/Action;

    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->doAfterTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Maybe;

    move-result-object p1

    .line 73
    invoke-virtual {p1}, Lio/reactivex/Maybe;->cache()Lio/reactivex/Maybe;

    move-result-object p1

    const-string v0, "session.outputs.asObserv\u2026))\n      }\n      .cache()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->result:Lio/reactivex/Maybe;

    .line 77
    invoke-virtual {p2}, Lcom/squareup/workflow/WorkflowSession;->getRenderingsAndSnapshots()Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 116
    new-instance v0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$$special$$inlined$map$1;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$$special$$inlined$map$1;-><init>(Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lkotlinx/coroutines/flow/Flow;

    .line 79
    new-instance p1, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$2;

    const/4 v1, 0x0

    invoke-direct {p1, p0, v1}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$2;-><init>(Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlin/jvm/functions/Function2;

    invoke-static {v0, p1}, Lkotlinx/coroutines/flow/FlowKt;->onEach(Lkotlinx/coroutines/flow/Flow;Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 80
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->scope:Lkotlinx/coroutines/CoroutineScope;

    invoke-static {p1, v0}, Lkotlinx/coroutines/flow/FlowKt;->launchIn(Lkotlinx/coroutines/flow/Flow;Lkotlinx/coroutines/CoroutineScope;)Lkotlinx/coroutines/Job;

    .line 83
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->lastSnapshot:Lcom/squareup/workflow/Snapshot;

    .line 86
    invoke-virtual {p2}, Lcom/squareup/workflow/WorkflowSession;->getRenderingsAndSnapshots()Lkotlinx/coroutines/flow/Flow;

    move-result-object p1

    .line 121
    new-instance p2, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$$special$$inlined$map$2;

    invoke-direct {p2, p1}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$$special$$inlined$map$2;-><init>(Lkotlinx/coroutines/flow/Flow;)V

    check-cast p2, Lkotlinx/coroutines/flow/Flow;

    .line 88
    invoke-static {p2}, Lkotlinx/coroutines/rx2/RxConvertKt;->from(Lkotlinx/coroutines/flow/Flow;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->renderings:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$Companion()Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Companion;
    .locals 1

    sget-object v0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->Companion:Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Companion;

    return-object v0
.end method

.method public static final synthetic access$getBUNDLE_KEY$cp()Ljava/lang/String;
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->BUNDLE_KEY:Ljava/lang/String;

    return-object v0
.end method

.method public static final synthetic access$getLastSnapshot$p(Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->lastSnapshot:Lcom/squareup/workflow/Snapshot;

    return-object p0
.end method

.method public static final synthetic access$getScope$p(Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;)Lkotlinx/coroutines/CoroutineScope;
    .locals 0

    .line 39
    iget-object p0, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->scope:Lkotlinx/coroutines/CoroutineScope;

    return-object p0
.end method

.method public static final synthetic access$setLastSnapshot$p(Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;Lcom/squareup/workflow/Snapshot;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->lastSnapshot:Lcom/squareup/workflow/Snapshot;

    return-void
.end method

.method public static synthetic renderings$annotations()V
    .locals 0

    return-void
.end method

.method public static synthetic result$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final clearForTest$core_android_release()V
    .locals 0

    .line 99
    invoke-virtual {p0}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->onCleared()V

    return-void
.end method

.method public final getLastSnapshotForTest$core_android_release()Lcom/squareup/workflow/Snapshot;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->lastSnapshot:Lcom/squareup/workflow/Snapshot;

    return-object v0
.end method

.method public getRenderings()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->renderings:Lio/reactivex/Observable;

    return-object v0
.end method

.method public getResult()Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Maybe<",
            "+TOutputT;>;"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->result:Lio/reactivex/Maybe;

    return-object v0
.end method

.method protected onCleared()V
    .locals 3

    .line 91
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->scope:Lkotlinx/coroutines/CoroutineScope;

    new-instance v1, Ljava/util/concurrent/CancellationException;

    const-string v2, "WorkflowRunnerViewModel cleared."

    invoke-direct {v1, v2}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lkotlinx/coroutines/CoroutineScopeKt;->cancel(Lkotlinx/coroutines/CoroutineScope;Ljava/util/concurrent/CancellationException;)V

    return-void
.end method

.method public saveState()Landroid/os/Bundle;
    .locals 4

    .line 94
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 95
    sget-object v1, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->BUNDLE_KEY:Ljava/lang/String;

    new-instance v2, Lcom/squareup/workflow/ui/PickledWorkflow;

    iget-object v3, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->lastSnapshot:Lcom/squareup/workflow/Snapshot;

    invoke-direct {v2, v3}, Lcom/squareup/workflow/ui/PickledWorkflow;-><init>(Lcom/squareup/workflow/Snapshot;)V

    check-cast v2, Landroid/os/Parcelable;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v0
.end method
