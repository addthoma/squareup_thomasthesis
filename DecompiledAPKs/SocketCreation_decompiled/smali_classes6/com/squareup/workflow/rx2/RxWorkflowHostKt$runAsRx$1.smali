.class final Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RxWorkflowHost.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/rx2/RxWorkflowHostKt;->runAsRx(Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/Workflow;Lcom/squareup/workflow/Snapshot;Lkotlin/coroutines/CoroutineContext;)Lcom/squareup/workflow/rx2/RxWorkflowHost;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lcom/squareup/workflow/WorkflowSession<",
        "+TO;+TR;>;",
        "Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001b\n\u0000\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0001\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0002\u0010\u0004*\u00020\u00062\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0008H\n\u00a2\u0006\u0004\u0008\t\u0010\n"
    }
    d2 = {
        "<anonymous>",
        "com/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1",
        "I",
        "O",
        "R",
        "",
        "Lkotlinx/coroutines/CoroutineScope;",
        "session",
        "Lcom/squareup/workflow/WorkflowSession;",
        "invoke",
        "(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1;

    invoke-direct {v0}, Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1;-><init>()V

    sput-object v0, Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1;->INSTANCE:Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/CoroutineScope;",
            "Lcom/squareup/workflow/WorkflowSession<",
            "+TO;+TR;>;)",
            "Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "session"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v0, Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1;

    invoke-direct {v0, p1, p2}, Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1;-><init>(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    check-cast p2, Lcom/squareup/workflow/WorkflowSession;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1;->invoke(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)Lcom/squareup/workflow/rx2/RxWorkflowHostKt$runAsRx$1$1;

    move-result-object p1

    return-object p1
.end method
