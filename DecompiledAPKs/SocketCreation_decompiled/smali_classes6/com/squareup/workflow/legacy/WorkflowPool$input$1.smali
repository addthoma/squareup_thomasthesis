.class public final Lcom/squareup/workflow/legacy/WorkflowPool$input$1;
.super Ljava/lang/Object;
.source "WorkflowPool.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/WorkflowInput;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/WorkflowPool;->input(Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lcom/squareup/workflow/legacy/WorkflowInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/WorkflowInput<",
        "TE;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowPool.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPool$input$1\n*L\n1#1,422:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00028\u00000\u0001J\u0015\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/workflow/legacy/WorkflowPool$input$1",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "sendEvent",
        "",
        "event",
        "(Ljava/lang/Object;)V",
        "legacy-workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $handle:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

.field final synthetic this$0:Lcom/squareup/workflow/legacy/WorkflowPool;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
            ")V"
        }
    .end annotation

    .line 231
    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowPool$input$1;->this$0:Lcom/squareup/workflow/legacy/WorkflowPool;

    iput-object p2, p0, Lcom/squareup/workflow/legacy/WorkflowPool$input$1;->$handle:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public sendEvent(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 233
    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool$input$1;->this$0:Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-static {v0}, Lcom/squareup/workflow/legacy/WorkflowPool;->access$getWorkflows$p(Lcom/squareup/workflow/legacy/WorkflowPool;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/workflow/legacy/WorkflowPool$input$1;->$handle:Lcom/squareup/workflow/legacy/WorkflowPool$Handle;

    invoke-virtual {v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Handle;->getId$legacy_workflow_core()Lcom/squareup/workflow/legacy/WorkflowPool$Id;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;

    if-eqz v0, :cond_1

    .line 235
    invoke-virtual {v0}, Lcom/squareup/workflow/legacy/WorkflowPool$WorkflowEntry;->getWorkflow()Lcom/squareup/workflow/legacy/Workflow;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-interface {v0, p1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.workflow.legacy.WorkflowInput<E>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    return-void
.end method
