.class public final Lcom/squareup/workflow/legacy/WorkflowInputKt$WorkflowInput$1;
.super Ljava/lang/Object;
.source "WorkflowInput.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/WorkflowInput;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/WorkflowInputKt;->WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/WorkflowInput<",
        "TE;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00028\u00000\u0001J\u0015\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00028\u0000H\u0016\u00a2\u0006\u0002\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/workflow/legacy/WorkflowInputKt$WorkflowInput$1",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "sendEvent",
        "",
        "event",
        "(Ljava/lang/Object;)V",
        "legacy-workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $handler:Lkotlin/jvm/functions/Function1;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowInputKt$WorkflowInput$1;->$handler:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public sendEvent(Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .line 49
    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowInputKt$WorkflowInput$1;->$handler:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
