.class final Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;
.super Lkotlin/coroutines/jvm/internal/ContinuationImpl;
.source "WorkflowPool.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/WorkflowPool;->awaitWorkerResult(Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\u0010\u0000\u001a\u0004\u0018\u00010\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0001\"\u0008\u0008\u0001\u0010\u0003*\u00020\u00012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u00052\u0006\u0010\u0006\u001a\u0002H\u00022\u0006\u0010\u0007\u001a\u00020\u00082\u0018\u0010\t\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u0002H\u00030\n2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\rH\u0081@"
    }
    d2 = {
        "awaitWorkerResult",
        "",
        "I",
        "O",
        "worker",
        "Lcom/squareup/workflow/legacy/Worker;",
        "input",
        "name",
        "",
        "type",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Type;",
        "",
        "continuation",
        "Lkotlin/coroutines/Continuation;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.workflow.legacy.WorkflowPool"
    f = "WorkflowPool.kt"
    i = {
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x0
    }
    l = {
        0xda
    }
    m = "awaitWorkerResult"
    n = {
        "this",
        "worker",
        "input",
        "name",
        "type",
        "handle",
        "workflow",
        "this_$iv",
        "id$iv"
    }
    s = {
        "L$0",
        "L$1",
        "L$2",
        "L$3",
        "L$4",
        "L$5",
        "L$6",
        "L$7",
        "L$8"
    }
.end annotation


# instance fields
.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field L$2:Ljava/lang/Object;

.field L$3:Ljava/lang/Object;

.field L$4:Ljava/lang/Object;

.field L$5:Ljava/lang/Object;

.field L$6:Ljava/lang/Object;

.field L$7:Ljava/lang/Object;

.field L$8:Ljava/lang/Object;

.field label:I

.field synthetic result:Ljava/lang/Object;

.field final synthetic this$0:Lcom/squareup/workflow/legacy/WorkflowPool;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->this$0:Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-direct {p0, p2}, Lkotlin/coroutines/jvm/internal/ContinuationImpl;-><init>(Lkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    iput-object p1, p0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->result:Ljava/lang/Object;

    iget p1, p0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->label:I

    const/high16 v0, -0x80000000

    or-int/2addr p1, v0

    iput p1, p0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->label:I

    iget-object v0, p0, Lcom/squareup/workflow/legacy/WorkflowPool$awaitWorkerResult$2;->this$0:Lcom/squareup/workflow/legacy/WorkflowPool;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/workflow/legacy/WorkflowPool;->awaitWorkerResult(Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
