.class public final Lcom/squareup/workflow/legacy/WorkflowPoolKt;
.super Ljava/lang/Object;
.source "WorkflowPool.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowPool.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPoolKt\n+ 2 Worker.kt\ncom/squareup/workflow/legacy/WorkerKt\n*L\n1#1,422:1\n402#1:423\n382#1:425\n114#2:424\n114#2:426\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowPool.kt\ncom/squareup/workflow/legacy/WorkflowPoolKt\n*L\n357#1:423\n382#1:424\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000]\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u000f\u001a=\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00040\u000f\"\u0008\u0008\u0000\u0010\u0010*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0005*\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00040\u0011H\u0002\u00a2\u0006\u0002\u0010\u0012\u001aK\u0010\u0013\u001a\u00020\u0014\"\n\u0008\u0000\u0010\u0002\u0018\u0001*\u00020\u0005\"\n\u0008\u0001\u0010\u0003\u0018\u0001*\u00020\u0005\"\n\u0008\u0002\u0010\u0004\u0018\u0001*\u00020\u0005*\u00020\u00152\u0018\u0010\u0016\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0006H\u0086\u0008\u001aV\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u0018\"\n\u0008\u0000\u0010\u0010\u0018\u0001*\u00020\u0005\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u00020\u0005*\u00020\u00152\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00040\u00112\u0006\u0010\u001a\u001a\u0002H\u00102\u0008\u0008\u0002\u0010\u001b\u001a\u00020\u001cH\u0086\u0008\u00a2\u0006\u0002\u0010\u001d\u001ak\u0010\u0017\u001a\u0008\u0012\u0004\u0012\u0002H\u00040\u0018\"\u0008\u0008\u0000\u0010\u0010*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0005*\u00020\u00152\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u0002H\u00040\u00112\u0006\u0010\u001a\u001a\u0002H\u00102\u0008\u0008\u0002\u0010\u001b\u001a\u00020\u001c2\u0018\u0010\u001e\u001a\u0014\u0012\u0004\u0012\u0002H\u0010\u0012\u0004\u0012\u00020\u001f\u0012\u0004\u0012\u0002H\u00040\u0001H\u0001\u00a2\u0006\u0002\u0010 \u001aZ\u0010!\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\"0\u0018\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005*\u00020\u00152\u0018\u0010#\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040$\"g\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0001\"\n\u0008\u0000\u0010\u0002\u0018\u0001*\u00020\u0005\"\n\u0008\u0001\u0010\u0003\u0018\u0001*\u00020\u0005\"\n\u0008\u0002\u0010\u0004\u0018\u0001*\u00020\u0005*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00068\u00c6\u0002X\u0087\u0004\u00a2\u0006\u000c\u0012\u0004\u0008\u0007\u0010\u0008\u001a\u0004\u0008\t\u0010\n\"l\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0001\"\n\u0008\u0000\u0010\u0002\u0018\u0001*\u00020\u0005\"\n\u0008\u0001\u0010\u0003\u0018\u0001*\u00020\u0005\"\n\u0008\u0002\u0010\u0004\u0018\u0001*\u00020\u0005*\u001c\u0012\u0018\u0008\u0001\u0012\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00060\u000b8\u00c6\u0002\u00a2\u0006\u000c\u0012\u0004\u0008\u0007\u0010\u000c\u001a\u0004\u0008\t\u0010\r\u00a8\u0006%"
    }
    d2 = {
        "workflowType",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Type;",
        "S",
        "E",
        "O",
        "",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "workflowType$annotations",
        "(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;)V",
        "getWorkflowType",
        "(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;)Lcom/squareup/workflow/legacy/WorkflowPool$Type;",
        "Lkotlin/reflect/KClass;",
        "(Lkotlin/reflect/KClass;)V",
        "(Lkotlin/reflect/KClass;)Lcom/squareup/workflow/legacy/WorkflowPool$Type;",
        "asLauncher",
        "com/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1",
        "I",
        "Lcom/squareup/workflow/legacy/Worker;",
        "(Lcom/squareup/workflow/legacy/Worker;)Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;",
        "register",
        "",
        "Lcom/squareup/workflow/legacy/WorkflowPool;",
        "launcher",
        "workerResult",
        "Lkotlinx/coroutines/Deferred;",
        "worker",
        "input",
        "name",
        "",
        "(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;)Lkotlinx/coroutines/Deferred;",
        "type",
        "",
        "(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)Lkotlinx/coroutines/Deferred;",
        "workflowUpdate",
        "Lcom/squareup/workflow/legacy/WorkflowUpdate;",
        "handle",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Handle;",
        "legacy-workflow-core"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$asLauncher(Lcom/squareup/workflow/legacy/Worker;)Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/workflow/legacy/WorkflowPoolKt;->asLauncher(Lcom/squareup/workflow/legacy/Worker;)Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;

    move-result-object p0

    return-object p0
.end method

.method private static final asLauncher(Lcom/squareup/workflow/legacy/Worker;)Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/Worker<",
            "-TI;+TO;>;)",
            "Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;"
        }
    .end annotation

    .line 414
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/legacy/WorkflowPoolKt$asLauncher$1;-><init>(Lcom/squareup/workflow/legacy/Worker;)V

    return-object v0
.end method

.method public static final synthetic getWorkflowType(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;)Lcom/squareup/workflow/legacy/WorkflowPool$Type;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "TS;-TE;+TO;>;)",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Type<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "$this$workflowType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 402
    new-instance p0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const/4 v0, 0x4

    const-string v1, "S"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, "E"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v2, Ljava/lang/Object;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "O"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    return-object p0
.end method

.method public static final synthetic getWorkflowType(Lkotlin/reflect/KClass;)Lcom/squareup/workflow/legacy/WorkflowPool$Type;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/reflect/KClass<",
            "+",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "TS;-TE;+TO;>;>;)",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Type<",
            "TS;TE;TO;>;"
        }
    .end annotation

    const-string v0, "$this$workflowType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 412
    new-instance p0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const/4 v0, 0x4

    const-string v1, "S"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, "E"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v2, Ljava/lang/Object;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "O"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    return-object p0
.end method

.method public static final synthetic register(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "TS;-TE;+TO;>;)V"
        }
    .end annotation

    const-string v0, "$this$register"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "launcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 423
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const/4 v1, 0x4

    const-string v2, "S"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v2, Ljava/lang/Object;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "E"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v3, Ljava/lang/Object;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "O"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 357
    invoke-virtual {p0, p1, v0}, Lcom/squareup/workflow/legacy/WorkflowPool;->register(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)V

    return-void
.end method

.method public static final synthetic workerResult(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;)Lkotlinx/coroutines/Deferred;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            "Lcom/squareup/workflow/legacy/Worker<",
            "-TI;+TO;>;TI;",
            "Ljava/lang/String;",
            ")",
            "Lkotlinx/coroutines/Deferred<",
            "TO;>;"
        }
    .end annotation

    const-string v0, "$this$workerResult"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "worker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 424
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const/4 v1, 0x4

    const-string v2, "I"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v2, Ljava/lang/Object;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-class v3, Ljava/lang/Void;

    invoke-static {v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v3

    const-string v4, "O"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v1, Ljava/lang/Object;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 382
    invoke-static {p0, p1, p2, p3, v0}, Lcom/squareup/workflow/legacy/WorkflowPoolKt;->workerResult(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)Lkotlinx/coroutines/Deferred;

    move-result-object p0

    return-object p0
.end method

.method public static final workerResult(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)Lkotlinx/coroutines/Deferred;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            "Lcom/squareup/workflow/legacy/Worker<",
            "-TI;+TO;>;TI;",
            "Ljava/lang/String;",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Type<",
            "TI;*+TO;>;)",
            "Lkotlinx/coroutines/Deferred<",
            "TO;>;"
        }
    .end annotation

    const-string v0, "$this$workerResult"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "worker"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 393
    sget-object v0, Lkotlinx/coroutines/GlobalScope;->INSTANCE:Lkotlinx/coroutines/GlobalScope;

    move-object v1, v0

    check-cast v1, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getUnconfined()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lkotlin/coroutines/CoroutineContext;

    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;

    const/4 v9, 0x0

    move-object v3, v0

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v3 .. v9}, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workerResult$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;Lkotlin/coroutines/Continuation;)V

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function2;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->async$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Deferred;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic workerResult$default(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;ILjava/lang/Object;)Lkotlinx/coroutines/Deferred;
    .locals 3

    const/4 p5, 0x4

    and-int/2addr p4, p5

    if-eqz p4, :cond_0

    const-string p3, ""

    :cond_0
    const-string p4, "$this$workerResult"

    .line 381
    invoke-static {p0, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "worker"

    invoke-static {p1, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "input"

    invoke-static {p2, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p4, "name"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 426
    new-instance p4, Lcom/squareup/workflow/legacy/WorkflowPool$Type;

    const-string v0, "I"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class v0, Ljava/lang/Object;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const-class v1, Ljava/lang/Void;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    const-string v2, "O"

    invoke-static {p5, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    const-class p5, Ljava/lang/Object;

    invoke-static {p5}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p5

    invoke-direct {p4, v0, v1, p5}, Lcom/squareup/workflow/legacy/WorkflowPool$Type;-><init>(Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;Lkotlin/reflect/KClass;)V

    .line 425
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/workflow/legacy/WorkflowPoolKt;->workerResult(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)Lkotlinx/coroutines/Deferred;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic workerResult$default(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;ILjava/lang/Object;)Lkotlinx/coroutines/Deferred;
    .locals 0

    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    const-string p3, ""

    .line 391
    :cond_0
    invoke-static {p0, p1, p2, p3, p4}, Lcom/squareup/workflow/legacy/WorkflowPoolKt;->workerResult(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/Worker;Ljava/lang/Object;Ljava/lang/String;Lcom/squareup/workflow/legacy/WorkflowPool$Type;)Lkotlinx/coroutines/Deferred;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic workflowType$annotations(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;)V
    .locals 0
    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    return-void
.end method

.method public static synthetic workflowType$annotations(Lkotlin/reflect/KClass;)V
    .locals 0

    return-void
.end method

.method public static final workflowUpdate(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;)Lkotlinx/coroutines/Deferred;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool;",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Handle<",
            "TS;-TE;+TO;>;)",
            "Lkotlinx/coroutines/Deferred<",
            "Lcom/squareup/workflow/legacy/WorkflowUpdate<",
            "TS;TE;TO;>;>;"
        }
    .end annotation

    const-string v0, "$this$workflowUpdate"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handle"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 368
    sget-object v0, Lkotlinx/coroutines/GlobalScope;->INSTANCE:Lkotlinx/coroutines/GlobalScope;

    move-object v1, v0

    check-cast v1, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getUnconfined()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lkotlin/coroutines/CoroutineContext;

    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workflowUpdate$1;

    const/4 v3, 0x0

    invoke-direct {v0, p0, p1, v3}, Lcom/squareup/workflow/legacy/WorkflowPoolKt$workflowUpdate$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool;Lcom/squareup/workflow/legacy/WorkflowPool$Handle;Lkotlin/coroutines/Continuation;)V

    move-object v4, v0

    check-cast v4, Lkotlin/jvm/functions/Function2;

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->async$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Deferred;

    move-result-object p0

    return-object p0
.end method
