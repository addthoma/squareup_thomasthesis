.class public final Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;
.super Ljava/lang/Object;
.source "EventChannel.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/rx2/EventChannel;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/rx2/EventChannelKt;->asEventChannel(Lkotlinx/coroutines/channels/ReceiveChannel;)Lcom/squareup/workflow/legacy/rx2/EventChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
        "TE;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEventChannel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EventChannel.kt\ncom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1\n+ 2 _Sequences.kt\nkotlin/sequences/SequencesKt___SequencesKt\n*L\n1#1,147:1\n140#2,2:148\n*E\n*S KotlinDebug\n*F\n+ 1 EventChannel.kt\ncom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1\n*L\n145#1,2:148\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00007\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00028\u00000\u0001J?\u0010\u0002\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00040\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u00052#\u0010\u0006\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u0002H\u00040\u0008\u0012\u0004\u0012\u00020\t0\u0007\u00a2\u0006\u0002\u0008\nH\u0016J\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u000c*\u00060\rj\u0002`\u000eH\u0002\u00a8\u0006\u000f"
    }
    d2 = {
        "com/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1",
        "Lcom/squareup/workflow/legacy/rx2/EventChannel;",
        "select",
        "Lio/reactivex/Single;",
        "R",
        "",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "unwrapRealCause",
        "",
        "Ljava/util/concurrent/CancellationException;",
        "Lkotlinx/coroutines/CancellationException;",
        "legacy-workflow-rx2"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_asEventChannel:Lkotlinx/coroutines/channels/ReceiveChannel;


# direct methods
.method constructor <init>(Lkotlinx/coroutines/channels/ReceiveChannel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "+TE;>;)V"
        }
    .end annotation

    .line 66
    iput-object p1, p0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;->$this_asEventChannel:Lkotlinx/coroutines/channels/ReceiveChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$unwrapRealCause(Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;Ljava/util/concurrent/CancellationException;)Ljava/lang/Throwable;
    .locals 0

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;->unwrapRealCause(Ljava/util/concurrent/CancellationException;)Ljava/lang/Throwable;

    move-result-object p0

    return-object p0
.end method

.method private final unwrapRealCause(Ljava/util/concurrent/CancellationException;)Ljava/lang/Throwable;
    .locals 2

    .line 144
    sget-object v0, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$unwrapRealCause$1;->INSTANCE:Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$unwrapRealCause$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lkotlin/sequences/SequencesKt;->generateSequence(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)Lkotlin/sequences/Sequence;

    move-result-object p1

    .line 148
    invoke-interface {p1}, Lkotlin/sequences/Sequence;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/Throwable;

    .line 145
    instance-of v1, v1, Ljava/util/concurrent/CancellationException;

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 149
    :goto_0
    check-cast v0, Ljava/lang/Throwable;

    return-object v0
.end method


# virtual methods
.method public select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
            "TE;TR;>;",
            "Lkotlin/Unit;",
            ">;)",
            "Lio/reactivex/Single<",
            "+TR;>;"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->getUnconfined()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    check-cast v0, Lkotlin/coroutines/CoroutineContext;

    new-instance v1, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1$select$1;-><init>(Lcom/squareup/workflow/legacy/rx2/EventChannelKt$asEventChannel$1;Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {v0, v1}, Lkotlinx/coroutines/rx2/RxSingleKt;->rxSingle(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
