.class public final Lcom/squareup/workflow/legacy/rx2/WorkersKt;
.super Ljava/lang/Object;
.source "Workers.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u001aB\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0003*\u00020\u00042\u0018\u0010\u0005\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00070\u0006H\u0007\u001a(\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u0002H\n0\u0001\"\u0008\u0008\u0000\u0010\n*\u00020\u0004*\u0008\u0012\u0004\u0012\u0002H\n0\u0007H\u0007\u00a8\u0006\u000b"
    }
    d2 = {
        "singleWorker",
        "Lcom/squareup/workflow/legacy/Worker;",
        "I",
        "O",
        "",
        "block",
        "Lkotlin/Function1;",
        "Lio/reactivex/Single;",
        "asWorker",
        "",
        "T",
        "legacy-workflow-rx2"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final asWorker(Lio/reactivex/Single;)Lcom/squareup/workflow/legacy/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Single<",
            "TT;>;)",
            "Lcom/squareup/workflow/legacy/Worker<",
            "Lkotlin/Unit;",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "$this$asWorker"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    new-instance v0, Lcom/squareup/workflow/legacy/rx2/WorkersKt$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/workflow/legacy/rx2/WorkersKt$asWorker$1;-><init>(Lio/reactivex/Single;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {v0}, Lcom/squareup/workflow/legacy/WorkerKt;->worker(Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p0

    return-object p0
.end method

.method public static final singleWorker(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-TI;+",
            "Lio/reactivex/Single<",
            "TO;>;>;)",
            "Lcom/squareup/workflow/legacy/Worker<",
            "TI;TO;>;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use com.squareup.workflow.Workflow"
    .end annotation

    const-string v0, "block"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    new-instance v0, Lcom/squareup/workflow/legacy/rx2/WorkersKt$singleWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/workflow/legacy/rx2/WorkersKt$singleWorker$1;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-static {v0}, Lcom/squareup/workflow/legacy/WorkerKt;->worker(Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Worker;

    move-result-object p0

    return-object p0
.end method
