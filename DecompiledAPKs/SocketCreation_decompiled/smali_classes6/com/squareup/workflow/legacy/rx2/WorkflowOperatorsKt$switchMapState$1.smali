.class final Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "WorkflowOperators.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->switchMapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "TS1;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlinx/coroutines/channels/ReceiveChannel<",
        "+TS2;>;>;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u0001H\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0004*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0006*\u00020\u0005\"\u0008\u0008\u0003\u0010\u0007*\u00020\u0005*\u00020\u00082\u0006\u0010\t\u001a\u0002H\u0004H\u008a@\u00a2\u0006\u0004\u0008\n\u0010\u000b"
    }
    d2 = {
        "<anonymous>",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "S2",
        "kotlin.jvm.PlatformType",
        "S1",
        "",
        "E",
        "O",
        "Lkotlinx/coroutines/CoroutineScope;",
        "it",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.workflow.legacy.rx2.WorkflowOperatorsKt$switchMapState$1"
    f = "WorkflowOperators.kt"
    i = {}
    l = {}
    m = "invokeSuspend"
    n = {}
    s = {}
.end annotation


# instance fields
.field final synthetic $transform:Lkotlin/jvm/functions/Function1;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field private p$0:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;->$transform:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x3

    invoke-direct {p0, p1, p2}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Lkotlinx/coroutines/CoroutineScope;Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/CoroutineScope;",
            "TS1;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "+TS2;>;>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$create"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "it"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "continuation"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;

    iget-object v1, p0, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;->$transform:Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1, p3}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)V

    iput-object p1, v0, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    iput-object p2, v0, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;->p$0:Ljava/lang/Object;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    check-cast p3, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;->create(Lkotlinx/coroutines/CoroutineScope;Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    .line 43
    iget v0, p0, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;->label:I

    if-nez v0, :cond_0

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;->p$:Lkotlinx/coroutines/CoroutineScope;

    iget-object p1, p0, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;->p$0:Ljava/lang/Object;

    .line 44
    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt$switchMapState$1;->$transform:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lio/reactivex/ObservableSource;

    invoke-static {p1}, Lkotlinx/coroutines/rx2/RxChannelKt;->openSubscription(Lio/reactivex/ObservableSource;)Lkotlinx/coroutines/channels/ReceiveChannel;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
