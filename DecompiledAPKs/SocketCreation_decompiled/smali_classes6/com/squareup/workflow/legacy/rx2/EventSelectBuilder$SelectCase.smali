.class public final Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;
.super Ljava/lang/Object;
.source "EventSelectBuilder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SelectCase"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        "T::TE;R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEventSelectBuilder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EventSelectBuilder.kt\ncom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase\n*L\n1#1,170:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0000\u0018\u0000*\u0008\u0008\u0002\u0010\u0001*\u00020\u0002*\u0008\u0008\u0003\u0010\u0003*\u0002H\u0001*\u0004\u0008\u0004\u0010\u00042\u00020\u0002B/\u0012\u0014\u0010\u0005\u001a\u0010\u0012\u0004\u0012\u00028\u0002\u0012\u0006\u0012\u0004\u0018\u00018\u00030\u0006\u0012\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u00040\u0006\u00a2\u0006\u0002\u0010\u0008J\u001b\u0010\u000c\u001a\n\u0012\u0004\u0012\u00028\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00028\u0002\u00a2\u0006\u0002\u0010\u000fR\u001d\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00028\u0003\u0012\u0004\u0012\u00028\u00040\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u001f\u0010\u0005\u001a\u0010\u0012\u0004\u0012\u00028\u0002\u0012\u0006\u0012\u0004\u0018\u00018\u00030\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\n\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;",
        "E",
        "",
        "T",
        "R",
        "predicateMapper",
        "Lkotlin/Function1;",
        "handler",
        "(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V",
        "getHandler",
        "()Lkotlin/jvm/functions/Function1;",
        "getPredicateMapper",
        "tryHandle",
        "Lkotlin/Function0;",
        "event",
        "(Ljava/lang/Object;)Lkotlin/jvm/functions/Function0;",
        "legacy-workflow-rx2"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final handler:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "TT;TR;>;"
        }
    .end annotation
.end field

.field private final predicateMapper:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "TE;TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TE;+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+TR;>;)V"
        }
    .end annotation

    const-string v0, "predicateMapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;->predicateMapper:Lkotlin/jvm/functions/Function1;

    iput-object p2, p0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;->handler:Lkotlin/jvm/functions/Function1;

    return-void
.end method


# virtual methods
.method public final getHandler()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "TT;TR;>;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;->handler:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getPredicateMapper()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "TE;TT;>;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;->predicateMapper:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final tryHandle(Ljava/lang/Object;)Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "Lkotlin/jvm/functions/Function0<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;->predicateMapper:Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-eqz p1, :cond_0

    new-instance v0, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase$tryHandle$$inlined$let$lambda$1;

    invoke-direct {v0, p1, p0}, Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase$tryHandle$$inlined$let$lambda$1;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder$SelectCase;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method
