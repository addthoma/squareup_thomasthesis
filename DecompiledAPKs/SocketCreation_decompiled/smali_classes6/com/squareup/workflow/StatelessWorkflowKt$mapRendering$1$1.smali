.class final Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "StatelessWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/StatelessWorkflowKt;->mapRendering(Lcom/squareup/workflow/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "TOutputT;",
        "Lcom/squareup/workflow/WorkflowAction;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStatelessWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StatelessWorkflow.kt\ncom/squareup/workflow/StatelessWorkflowKt$mapRendering$1$1\n+ 2 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt\n*L\n1#1,147:1\n199#2,4:148\n*E\n*S KotlinDebug\n*F\n+ 1 StatelessWorkflow.kt\ncom/squareup/workflow/StatelessWorkflowKt$mapRendering$1$1\n*L\n112#1,4:148\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0005\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\u0008\u0000\u0010\u0004\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0002\u0010\u0006\"\u0004\u0008\u0003\u0010\u00072\u0006\u0010\u0008\u001a\u0002H\u0003H\n\u00a2\u0006\u0004\u0008\t\u0010\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "OutputT",
        "PropsT",
        "",
        "FromRenderingT",
        "ToRenderingT",
        "output",
        "invoke",
        "(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$1$1;

    invoke-direct {v0}, Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$1$1;-><init>()V

    sput-object v0, Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$1$1;->INSTANCE:Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TOutputT;)",
            "Lcom/squareup/workflow/WorkflowAction;"
        }
    .end annotation

    const-string v0, "output"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    new-instance v0, Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$1$1$$special$$inlined$action$1;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$1$1$$special$$inlined$action$1;-><init>(Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$1$1;->invoke(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
