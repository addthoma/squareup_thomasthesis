.class public abstract Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.super Ljava/lang/Object;
.source "AbstractWorkflowViewFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/WorkflowViewFactory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;,
        Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;,
        Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;,
        Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;,
        Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;,
        Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealDialogBinding;,
        Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAbstractWorkflowViewFactory.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AbstractWorkflowViewFactory.kt\ncom/squareup/workflow/AbstractWorkflowViewFactory\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,291:1\n250#2,2:292\n250#2,2:294\n250#2,2:296\n250#2,2:298\n1360#2:300\n1429#2,3:301\n*E\n*S KotlinDebug\n*F\n+ 1 AbstractWorkflowViewFactory.kt\ncom/squareup/workflow/AbstractWorkflowViewFactory\n*L\n84#1,2:292\n203#1,2:294\n212#1,2:296\n221#1,2:298\n72#1:300\n72#1,3:301\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0008\u0008&\u0018\u0000 -2\u00020\u0001:\u0007,-./012B+\u0008\u0004\u0012\"\u0010\u0002\u001a\u0012\u0012\u000e\u0008\u0001\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00040\u0003\"\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0004\u00a2\u0006\u0002\u0010\u0005J,\u0010\u000e\u001a\u00020\u000f2\u000e\u0010\u0010\u001a\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00042\u0012\u0010\u0011\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000bH\u0002JH\u0010\u0012\u001a\u000c\u0012\u0006\u0008\u0001\u0012\u00020\u0014\u0018\u00010\u00132\u0012\u0010\u0011\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b2\u0018\u0010\u0015\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0017j\u0002`\u00180\u00162\u0006\u0010\u0019\u001a\u00020\u001aH\u0016JR\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0012\u0010\u0011\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b2\u0018\u0010\u0015\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0017j\u0002`\u00180\u00162\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001e2\u0006\u0010\u001f\u001a\u00020\u001a2\u0006\u0010 \u001a\u00020!H\u0016J&\u0010\"\u001a\u000c\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010#2\u0012\u0010\u0011\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000bH\u0002J&\u0010$\u001a\u000c\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010%2\u0012\u0010\u0011\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000bH\u0002J\u001e\u0010&\u001a\u0004\u0018\u00010\'2\u0012\u0010\u0011\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000bH\u0016J&\u0010(\u001a\u000c\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010)2\u0012\u0010\u0011\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000bH\u0002J\u0008\u0010*\u001a\u00020+H\u0016R\u001c\u0010\u0006\u001a\u0010\u0012\u000c\u0012\n\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u00040\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R#\u0010\u0008\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\nj\u0002`\u000b0\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\r\u00a8\u00063"
    }
    d2 = {
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "Lcom/squareup/workflow/WorkflowViewFactory;",
        "_bindings",
        "",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;",
        "([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V",
        "bindings",
        "",
        "keys",
        "",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "getKeys",
        "()Ljava/util/Set;",
        "assertNotDialogBinding",
        "",
        "binding",
        "screenKey",
        "buildDialog",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "contextForNewDialog",
        "Landroid/content/Context;",
        "buildView",
        "Landroid/view/View;",
        "container",
        "Landroid/view/ViewGroup;",
        "contextForNewView",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "getBuilderBinding",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;",
        "getDialogBinding",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;",
        "getHintForKey",
        "Lcom/squareup/workflow/ScreenHint;",
        "getLayoutBinding",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;",
        "toString",
        "",
        "Binding",
        "Companion",
        "DialogBinding",
        "LayoutBinding",
        "RealDialogBinding",
        "RealLayoutBinding",
        "ViewBuilderBinding",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;


# instance fields
.field private final bindings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding<",
            "**>;>;"
        }
    .end annotation
.end field

.field private final keys:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    return-void
.end method

.method protected varargs constructor <init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding<",
            "**>;)V"
        }
    .end annotation

    const-string v0, "_bindings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {p1}, Lkotlin/collections/ArraysKt;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->bindings:Ljava/util/List;

    .line 70
    new-instance p1, Ljava/util/LinkedHashSet;

    invoke-direct {p1}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast p1, Ljava/util/Set;

    .line 71
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v0, Ljava/util/Set;

    .line 72
    iget-object v1, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->bindings:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 300
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 301
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 302
    check-cast v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 72
    invoke-interface {v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;->getKey()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 303
    :cond_0
    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/workflow/legacy/Screen$Key;

    .line 73
    invoke-interface {p1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 74
    move-object v3, v0

    check-cast v3, Ljava/util/Collection;

    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 77
    :cond_2
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 80
    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->toSet(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->keys:Ljava/util/Set;

    return-void

    .line 78
    :cond_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected all binding keys to be unique, but found duplicates: "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v2, v0

    check-cast v2, Ljava/lang/Iterable;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x3f

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 77
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final assertNotDialogBinding(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;Lcom/squareup/workflow/legacy/Screen$Key;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding<",
            "**>;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)V"
        }
    .end annotation

    .line 235
    instance-of v0, p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    return-void

    .line 236
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, " to have a "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class p2, Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p2, 0x20

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string p2, "or "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    const-class p2, Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;

    invoke-virtual {p2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ", found "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/squareup/util/Objects;->getHumanClassName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 235
    new-instance p2, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p2, Ljava/lang/Throwable;

    throw p2
.end method

.method private final getBuilderBinding(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding<",
            "**>;"
        }
    .end annotation

    .line 202
    iget-object v0, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->bindings:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 294
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 203
    invoke-interface {v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;->getKey()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/squareup/workflow/legacy/Screen$Key;->castOrNull(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_0

    goto :goto_1

    :cond_2
    move-object v1, v2

    .line 295
    :goto_1
    move-object v0, v1

    check-cast v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    if-eqz v0, :cond_4

    .line 205
    invoke-direct {p0, v0, p1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->assertNotDialogBinding(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;Lcom/squareup/workflow/legacy/Screen$Key;)V

    .line 206
    instance-of p1, v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;

    if-nez p1, :cond_3

    move-object v0, v2

    :cond_3
    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;

    :cond_4
    return-object v2
.end method

.method private final getDialogBinding(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding<",
            "**>;"
        }
    .end annotation

    .line 220
    iget-object v0, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->bindings:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 298
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 221
    invoke-interface {v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;->getKey()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/squareup/workflow/legacy/Screen$Key;->castOrNull(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_0

    goto :goto_1

    :cond_2
    move-object v1, v2

    .line 299
    :goto_1
    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    if-eqz v1, :cond_4

    .line 223
    instance-of v0, v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    if-eqz v0, :cond_3

    .line 227
    move-object v2, v1

    check-cast v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    goto :goto_2

    .line 224
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " to have a "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class p1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "found "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    invoke-static {v1}, Lcom/squareup/util/Objects;->getHumanClassName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 223
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_4
    :goto_2
    return-object v2
.end method

.method private final getLayoutBinding(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)",
            "Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding<",
            "**>;"
        }
    .end annotation

    .line 211
    iget-object v0, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->bindings:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 296
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 212
    invoke-interface {v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;->getKey()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/squareup/workflow/legacy/Screen$Key;->castOrNull(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_0

    goto :goto_1

    :cond_2
    move-object v1, v2

    .line 297
    :goto_1
    move-object v0, v1

    check-cast v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    if-eqz v0, :cond_4

    .line 214
    invoke-direct {p0, v0, p1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->assertNotDialogBinding(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;Lcom/squareup/workflow/legacy/Screen$Key;)V

    .line 215
    instance-of p1, v0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    if-nez p1, :cond_3

    move-object v0, v2

    :cond_3
    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    :cond_4
    return-object v2
.end method


# virtual methods
.method public buildDialog(Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Landroid/content/Context;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "screenKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contextForNewDialog"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-direct {p0, p1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->getDialogBinding(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p3, p1, p2}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$DialogBinding;->promiseDialog(Landroid/content/Context;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;)Lio/reactivex/Single;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public buildView(Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Landroid/view/ViewGroup;Landroid/content/Context;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Landroid/view/ViewGroup;",
            "Landroid/content/Context;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    const-string v0, "screenKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contextForNewView"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-direct {p0, p1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->getLayoutBinding(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v2, p4

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    move-object v6, p5

    .line 96
    invoke-interface/range {v1 .. v6}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;->inflate(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 97
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->getBuilderBinding(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object v2, p4

    move-object v3, p3

    move-object v4, p5

    move-object v5, p1

    move-object v6, p2

    .line 98
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$ViewBuilderBinding;->build$android_release(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/workflow/ui/ContainerHints;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getHintForKey(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/ScreenHint;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;)",
            "Lcom/squareup/workflow/ScreenHint;"
        }
    .end annotation

    const-string v0, "screenKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->bindings:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 292
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 84
    invoke-interface {v3}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;->getKey()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/squareup/workflow/legacy/Screen$Key;->castOrNull(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_0

    goto :goto_1

    :cond_2
    move-object v1, v2

    .line 293
    :goto_1
    check-cast v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;->getHint()Lcom/squareup/workflow/ScreenHint;

    move-result-object v2

    :cond_3
    return-object v2
.end method

.method public final getKeys()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;>;"
        }
    .end annotation

    .line 67
    iget-object v0, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->keys:Ljava/util/Set;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->bindings:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
