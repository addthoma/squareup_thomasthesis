.class public final Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$$inlined$stateless$1;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "StatelessWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/StatelessWorkflowKt;->mapRendering(Lcom/squareup/workflow/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "TPropsT;TOutputT;TToRenderingT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStatelessWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StatelessWorkflow.kt\ncom/squareup/workflow/StatelessWorkflowKt$stateless$1\n+ 2 StatelessWorkflow.kt\ncom/squareup/workflow/StatelessWorkflowKt\n*L\n1#1,147:1\n111#2,3:148\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000%\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0001J)\u0010\u0002\u001a\u00028\u00022\u0006\u0010\u0003\u001a\u00028\u00002\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00028\u00010\u0005H\u0016\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/workflow/StatelessWorkflowKt$stateless$1",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;",
        "workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_mapRendering$inlined:Lcom/squareup/workflow/Workflow;

.field final synthetic $transform$inlined:Lkotlin/jvm/functions/Function1;


# direct methods
.method public constructor <init>(Lcom/squareup/workflow/Workflow;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$$inlined$stateless$1;->$this_mapRendering$inlined:Lcom/squareup/workflow/Workflow;

    iput-object p2, p0, Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$$inlined$stateless$1;->$transform$inlined:Lkotlin/jvm/functions/Function1;

    .line 86
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    return-void
.end method


# virtual methods
.method public render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPropsT;",
            "Lcom/squareup/workflow/RenderContext;",
            ")TToRenderingT;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    iget-object v2, p0, Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$$inlined$stateless$1;->$this_mapRendering$inlined:Lcom/squareup/workflow/Workflow;

    sget-object v0, Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$1$1;->INSTANCE:Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$1$1;

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p2

    move-object v3, p1

    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .line 150
    iget-object p2, p0, Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$$inlined$stateless$1;->$transform$inlined:Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
