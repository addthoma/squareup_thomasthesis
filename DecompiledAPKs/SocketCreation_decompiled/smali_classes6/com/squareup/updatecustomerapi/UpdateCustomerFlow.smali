.class public interface abstract Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;
.super Ljava/lang/Object;
.source "UpdateCustomerFlow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;,
        Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;,
        Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;,
        Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008f\u0018\u00002\u00020\u0001:\u0004\u0016\u0017\u0018\u0019J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J:\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u0013H&J\u000e\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0015H&\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
        "",
        "emitResult",
        "",
        "result",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;",
        "getFirstScreen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "parentKey",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "updateCustomerResultKey",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;",
        "type",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;",
        "contactValidationType",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;",
        "crmScopeType",
        "Lcom/squareup/ui/crm/flow/CrmScopeType;",
        "baseContact",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "results",
        "Lio/reactivex/Observable;",
        "ContactValidationType",
        "Result",
        "Type",
        "UpdateCustomerResultKey",
        "update-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract emitResult(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)V
.end method

.method public abstract getFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/container/ContainerTreeKey;
.end method

.method public abstract results()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;",
            ">;"
        }
    .end annotation
.end method
