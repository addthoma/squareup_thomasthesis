.class public final Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule_ProvideX2MonitorWorkflowRunnerFactory;
.super Ljava/lang/Object;
.source "NoX2MonitorWorkflowRunnerModule_ProvideX2MonitorWorkflowRunnerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule_ProvideX2MonitorWorkflowRunnerFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/x2/X2MonitorWorkflowRunner;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule_ProvideX2MonitorWorkflowRunnerFactory;
    .locals 1

    .line 22
    invoke-static {}, Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule_ProvideX2MonitorWorkflowRunnerFactory$InstanceHolder;->access$000()Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule_ProvideX2MonitorWorkflowRunnerFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideX2MonitorWorkflowRunner()Lcom/squareup/x2/X2MonitorWorkflowRunner;
    .locals 2

    .line 26
    invoke-static {}, Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule;->provideX2MonitorWorkflowRunner()Lcom/squareup/x2/X2MonitorWorkflowRunner;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/x2/X2MonitorWorkflowRunner;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/x2/X2MonitorWorkflowRunner;
    .locals 1

    .line 18
    invoke-static {}, Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule_ProvideX2MonitorWorkflowRunnerFactory;->provideX2MonitorWorkflowRunner()Lcom/squareup/x2/X2MonitorWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule_ProvideX2MonitorWorkflowRunnerFactory;->get()Lcom/squareup/x2/X2MonitorWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
