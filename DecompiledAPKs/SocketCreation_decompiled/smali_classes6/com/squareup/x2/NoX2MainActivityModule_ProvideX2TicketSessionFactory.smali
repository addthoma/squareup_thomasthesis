.class public final Lcom/squareup/x2/NoX2MainActivityModule_ProvideX2TicketSessionFactory;
.super Ljava/lang/Object;
.source "NoX2MainActivityModule_ProvideX2TicketSessionFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/x2/NoX2MainActivityModule_ProvideX2TicketSessionFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/X2TicketRunner;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/x2/NoX2MainActivityModule_ProvideX2TicketSessionFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/x2/NoX2MainActivityModule_ProvideX2TicketSessionFactory$InstanceHolder;->access$000()Lcom/squareup/x2/NoX2MainActivityModule_ProvideX2TicketSessionFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideX2TicketSession()Lcom/squareup/ui/ticket/X2TicketRunner;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/x2/NoX2MainActivityModule;->provideX2TicketSession()Lcom/squareup/ui/ticket/X2TicketRunner;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/X2TicketRunner;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/X2TicketRunner;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/x2/NoX2MainActivityModule_ProvideX2TicketSessionFactory;->provideX2TicketSession()Lcom/squareup/ui/ticket/X2TicketRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/x2/NoX2MainActivityModule_ProvideX2TicketSessionFactory;->get()Lcom/squareup/ui/ticket/X2TicketRunner;

    move-result-object v0

    return-object v0
.end method
