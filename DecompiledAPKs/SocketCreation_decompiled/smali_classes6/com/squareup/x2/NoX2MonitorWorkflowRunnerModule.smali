.class public final Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule;
.super Ljava/lang/Object;
.source "X2MonitorWorkflowRunner.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule;",
        "",
        "()V",
        "provideX2MonitorWorkflowRunner",
        "Lcom/squareup/x2/X2MonitorWorkflowRunner;",
        "x2-pos_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule;

    invoke-direct {v0}, Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule;-><init>()V

    sput-object v0, Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule;->INSTANCE:Lcom/squareup/x2/NoX2MonitorWorkflowRunnerModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideX2MonitorWorkflowRunner()Lcom/squareup/x2/X2MonitorWorkflowRunner;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 26
    sget-object v0, Lcom/squareup/x2/NoX2MonitorWorkflowRunner;->INSTANCE:Lcom/squareup/x2/NoX2MonitorWorkflowRunner;

    check-cast v0, Lcom/squareup/x2/X2MonitorWorkflowRunner;

    return-object v0
.end method
