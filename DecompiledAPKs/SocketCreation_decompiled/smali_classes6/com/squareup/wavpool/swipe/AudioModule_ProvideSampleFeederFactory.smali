.class public final Lcom/squareup/wavpool/swipe/AudioModule_ProvideSampleFeederFactory;
.super Ljava/lang/Object;
.source "AudioModule_ProvideSampleFeederFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/squarewave/SampleFeeder;",
        ">;"
    }
.end annotation


# instance fields
.field private final sampleProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gum/SampleProcessor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gum/SampleProcessor;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSampleFeederFactory;->sampleProcessorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/AudioModule_ProvideSampleFeederFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/gum/SampleProcessor;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/AudioModule_ProvideSampleFeederFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSampleFeederFactory;

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSampleFeederFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideSampleFeeder(Lcom/squareup/squarewave/gum/SampleProcessor;)Lcom/squareup/squarewave/SampleFeeder;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/wavpool/swipe/AudioModule;->provideSampleFeeder(Lcom/squareup/squarewave/gum/SampleProcessor;)Lcom/squareup/squarewave/SampleFeeder;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/squarewave/SampleFeeder;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/squarewave/SampleFeeder;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSampleFeederFactory;->sampleProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/gum/SampleProcessor;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSampleFeederFactory;->provideSampleFeeder(Lcom/squareup/squarewave/gum/SampleProcessor;)Lcom/squareup/squarewave/SampleFeeder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AudioModule_ProvideSampleFeederFactory;->get()Lcom/squareup/squarewave/SampleFeeder;

    move-result-object v0

    return-object v0
.end method
