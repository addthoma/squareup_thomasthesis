.class public abstract Lcom/squareup/wavpool/swipe/DecoderModule;
.super Ljava/lang/Object;
.source "DecoderModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/DecoderModule$Prod;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideClassifyingDecoder(Lcom/squareup/squarewave/o1/O1SignalDecoder;Lcom/squareup/squarewave/SignalDecoder;Lcom/squareup/squarewave/SignalDecoder;Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;)Lcom/squareup/squarewave/ClassifyingDecoder;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 65
    new-instance v0, Lcom/squareup/squarewave/ClassifyingDecoder;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/squarewave/ClassifyingDecoder;-><init>(Lcom/squareup/squarewave/o1/O1SignalDecoder;Lcom/squareup/squarewave/SignalDecoder;Lcom/squareup/squarewave/SignalDecoder;Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;)V

    return-object v0
.end method

.method public static provideFastSqLinkSignalDecoder(Lcom/squareup/squarewave/m1/R4Decoder;)Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-direct {v0, p0, v1}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;-><init>(Lcom/squareup/squarewave/m1/R4Decoder;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)V

    return-object v0
.end method

.method public static provideGen2SignalDecoder()Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;

    invoke-direct {v0}, Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;-><init>()V

    return-object v0
.end method

.method public static provideO1SignalDecoder()Lcom/squareup/squarewave/o1/O1SignalDecoder;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/squarewave/o1/O1SignalDecoder;

    invoke-direct {v0}, Lcom/squareup/squarewave/o1/O1SignalDecoder;-><init>()V

    return-object v0
.end method

.method public static provideR4FastSignalDecoder(Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;)Lcom/squareup/squarewave/SignalDecoder;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    return-object p0
.end method

.method public static provideR4SlowSignalDecoder(Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;)Lcom/squareup/squarewave/SignalDecoder;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    return-object p0
.end method

.method public static provideSlowSqLinkSignalDecoder(Lcom/squareup/squarewave/m1/R4Decoder;)Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-direct {v0, p0, v1}, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;-><init>(Lcom/squareup/squarewave/m1/R4Decoder;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)V

    return-object v0
.end method
