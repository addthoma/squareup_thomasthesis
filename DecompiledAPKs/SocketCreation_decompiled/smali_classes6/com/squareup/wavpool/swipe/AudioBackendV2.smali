.class public final Lcom/squareup/wavpool/swipe/AudioBackendV2;
.super Ljava/lang/Object;
.source "AudioBackendV2.kt"

# interfaces
.implements Lcom/squareup/cardreader/BackendPointerProvider;
.implements Lcom/squareup/cardreader/CanReset;
.implements Lcom/squareup/wavpool/swipe/AudioBackend;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0086\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0010\u000b\n\u0002\u0010\u0017\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u001b\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ\u0008\u0010\u000e\u001a\u00020\rH\u0016J\u0016\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014J\u0008\u0010\u0015\u001a\u00020\u0010H\u0002J\u0018\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0016\u0010\u001b\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0014J \u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020\u001a2\u0006\u0010 \u001a\u00020\u001a2\u0006\u0010!\u001a\u00020\"H\u0002J\u0008\u0010#\u001a\u00020\u0010H\u0002J\u0010\u0010$\u001a\u00020\u00102\u0006\u0010%\u001a\u00020&H\u0016J(\u0010\'\u001a\u00020\u00102\u0006\u0010(\u001a\u00020\u001a2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020\u001a2\u0006\u0010,\u001a\u00020*H\u0016J\u0008\u0010-\u001a\u00020\u0010H\u0016J\u0008\u0010.\u001a\u00020\u0010H\u0002J\u0008\u0010/\u001a\u00020\u0010H\u0002J\u0008\u00100\u001a\u00020\u0010H\u0016J\u0018\u00101\u001a\u00020\u00102\u0006\u00102\u001a\u0002032\u0006\u0010\u0017\u001a\u000204H\u0016J\u000e\u00105\u001a\u00020\u00102\u0006\u00106\u001a\u000207J\u0008\u00108\u001a\u00020\u0010H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00069"
    }
    d2 = {
        "Lcom/squareup/wavpool/swipe/AudioBackendV2;",
        "Lcom/squareup/cardreader/BackendPointerProvider;",
        "Lcom/squareup/cardreader/CanReset;",
        "Lcom/squareup/wavpool/swipe/AudioBackend;",
        "posSender",
        "Lcom/squareup/cardreader/SendsToPos;",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput;",
        "timerProvider",
        "Lcom/squareup/cardreader/TimerPointerProvider;",
        "(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/TimerPointerProvider;)V",
        "audioBackend",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;",
        "backend",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;",
        "backendPointer",
        "decodeR4Packet",
        "",
        "message",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;",
        "cardreaderPointerProvider",
        "Lcom/squareup/cardreader/CardreaderPointerProvider;",
        "enableTransmission",
        "feedSamples",
        "samples",
        "Ljava/nio/ByteBuffer;",
        "bytesRead",
        "",
        "handleMessage",
        "Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage;",
        "cardreaderProvider",
        "initialize",
        "inputSampleRate",
        "outputSampleRate",
        "timer",
        "Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;",
        "notifyTransmissionComplete",
        "onCarrierDetectEvent",
        "eventData",
        "Lcom/squareup/squarewave/gum/EventData;",
        "onCommsRateUpdated",
        "inCommsRate",
        "inCommsRateValue",
        "",
        "outCommsRate",
        "outCommsRateValue",
        "onConnectionTimeout",
        "onResume",
        "powerOnReader",
        "resetIfInitilized",
        "sendToReader",
        "loop",
        "",
        "",
        "setLegacyReaderType",
        "readerType",
        "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;",
        "stopSendingToReader",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private audioBackend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

.field private backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

.field private final posSender:Lcom/squareup/cardreader/SendsToPos;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput;",
            ">;"
        }
    .end annotation
.end field

.field private final timerProvider:Lcom/squareup/cardreader/TimerPointerProvider;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/SendsToPos;Lcom/squareup/cardreader/TimerPointerProvider;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cardreader/SendsToPos<",
            "Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput;",
            ">;",
            "Lcom/squareup/cardreader/TimerPointerProvider;",
            ")V"
        }
    .end annotation

    const-string v0, "posSender"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timerProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->timerProvider:Lcom/squareup/cardreader/TimerPointerProvider;

    return-void
.end method

.method public static final synthetic access$getAudioBackend$p(Lcom/squareup/wavpool/swipe/AudioBackendV2;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;
    .locals 1

    .line 36
    iget-object p0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->audioBackend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    if-nez p0, :cond_0

    const-string v0, "audioBackend"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setAudioBackend$p(Lcom/squareup/wavpool/swipe/AudioBackendV2;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->audioBackend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    return-void
.end method

.method private final enableTransmission()V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    sget-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$EnableTransmission;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$EnableTransmission;

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method private final feedSamples(Ljava/nio/ByteBuffer;I)V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->audioBackend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    if-nez v0, :cond_0

    const-string v1, "audioBackend"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, p1, v1, p2}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->feed_audio_samples(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Ljava/lang/Object;II)V

    return-void
.end method

.method private final initialize(IILcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;)V
    .locals 2

    .line 71
    invoke-static {}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->cr_comms_backend_audio_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    move-result-object v0

    const-string v1, "AudioBackendNative.cr_comms_backend_audio_alloc()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->audioBackend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    .line 75
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->audioBackend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    if-nez v0, :cond_0

    const-string v1, "audioBackend"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 72
    :cond_0
    invoke-static {p1, p2, v0, p3, p0}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->initialize_backend_audio(IILcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    move-result-object p1

    const-string p2, "AudioBackendNative.initi\u2026timer,\n        this\n    )"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    .line 79
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    sget-object p2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$OnInitialized;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$OnInitialized;

    check-cast p2, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {p1, p2}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method private final notifyTransmissionComplete()V
    .locals 2

    .line 134
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    sget-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$NotifyTransmissionComplete;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$NotifyTransmissionComplete;

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method private final onResume()V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AudioBackendV2;->notifyTransmissionComplete()V

    return-void
.end method

.method private final powerOnReader()V
    .locals 0

    .line 90
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AudioBackendV2;->enableTransmission()V

    .line 91
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AudioBackendV2;->notifyTransmissionComplete()V

    return-void
.end method


# virtual methods
.method public backendPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    if-nez v0, :cond_0

    const-string v1, "backend"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public final decodeR4Packet(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;Lcom/squareup/cardreader/CardreaderPointerProvider;)V
    .locals 4

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardreaderPointerProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    :try_start_0
    invoke-interface {p2}, Lcom/squareup/cardreader/CardreaderPointerProvider;->cardreaderPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object p2

    .line 119
    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->getLinkType()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/squarewave/gum/Mapping;->linkTypeToLcrLinkType(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)I

    move-result v0

    .line 120
    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->getSignal()[S

    move-result-object v1

    .line 118
    invoke-static {p2, v0, v1}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->decode_r4_packet(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;I[S)Ljava/lang/Object;

    move-result-object p2

    .line 122
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$DecodeR4PacketResponse$DecodedR4Packet;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->getPacketSequence()I

    move-result v2

    const-string v3, "packet"

    invoke-static {p2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$DecodeR4PacketResponse$DecodedR4Packet;-><init>(ILjava/lang/Object;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V
    :try_end_0
    .catch Lkotlin/UninitializedPropertyAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 125
    :catch_0
    iget-object p2, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$DecodeR4PacketResponse$NotReady;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;->getPacketSequence()I

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$DecodeR4PacketResponse$NotReady;-><init>(I)V

    check-cast v0, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {p2, v0}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    :goto_0
    return-void
.end method

.method public final handleMessage(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage;Lcom/squareup/cardreader/CardreaderPointerProvider;)V
    .locals 1

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardreaderProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$Initialize;

    if-eqz v0, :cond_0

    .line 56
    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$Initialize;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$Initialize;->getInputSampleRate()I

    move-result p2

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$Initialize;->getOutputSampleRate()I

    move-result p1

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->timerProvider:Lcom/squareup/cardreader/TimerPointerProvider;

    invoke-interface {v0}, Lcom/squareup/cardreader/TimerPointerProvider;->timerPointer()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;

    move-result-object v0

    .line 55
    invoke-direct {p0, p2, p1, v0}, Lcom/squareup/wavpool/swipe/AudioBackendV2;->initialize(IILcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;)V

    goto :goto_0

    .line 58
    :cond_0
    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$PowerOnReader;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$PowerOnReader;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AudioBackendV2;->powerOnReader()V

    goto :goto_0

    .line 59
    :cond_1
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$SetLegacyReaderType;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$SetLegacyReaderType;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$SetLegacyReaderType;->getReaderType()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/wavpool/swipe/AudioBackendV2;->setLegacyReaderType(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)V

    goto :goto_0

    .line 60
    :cond_2
    sget-object v0, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$OnResume;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$OnResume;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AudioBackendV2;->onResume()V

    goto :goto_0

    .line 61
    :cond_3
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$FeedSamples;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$FeedSamples;

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$FeedSamples;->getSamples()Ljava/nio/ByteBuffer;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$FeedSamples;->getBytesRead()I

    move-result p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/wavpool/swipe/AudioBackendV2;->feedSamples(Ljava/nio/ByteBuffer;I)V

    goto :goto_0

    .line 62
    :cond_4
    instance-of v0, p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;

    if-eqz v0, :cond_5

    check-cast p1, Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/wavpool/swipe/AudioBackendV2;->decodeR4Packet(Lcom/squareup/cardreader/ReaderMessage$ReaderInput$AudioBackendMessage$DecodeR4Packet;Lcom/squareup/cardreader/CardreaderPointerProvider;)V

    :cond_5
    :goto_0
    return-void
.end method

.method public onCarrierDetectEvent(Lcom/squareup/squarewave/gum/EventData;)V
    .locals 2

    const-string v0, "eventData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$OnCarrierDetectEvent;

    invoke-direct {v1, p1}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$OnCarrierDetectEvent;-><init>(Lcom/squareup/squarewave/gum/EventData;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onCommsRateUpdated(ILjava/lang/String;ILjava/lang/String;)V
    .locals 2

    const-string v0, "inCommsRateValue"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "outCommsRateValue"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    .line 171
    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$OnCommsRateUpdated;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$OnCommsRateUpdated;-><init>(ILjava/lang/String;ILjava/lang/String;)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    .line 170
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public onConnectionTimeout()V
    .locals 2

    .line 160
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    sget-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$OnConnectionTimeout;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$OnConnectionTimeout;

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public resetIfInitilized()V
    .locals 2

    .line 83
    move-object v0, p0

    check-cast v0, Lcom/squareup/wavpool/swipe/AudioBackendV2;

    iget-object v0, v0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->audioBackend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    if-eqz v0, :cond_2

    .line 84
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->audioBackend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    const-string v1, "audioBackend"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->cr_comms_backend_audio_shutdown(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->audioBackend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v0}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->cr_comms_backend_audio_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    :cond_2
    return-void
.end method

.method public sendToReader(Z[S)V
    .locals 2

    const-string v0, "samples"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    new-instance v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;

    invoke-direct {v1, p1, p2}, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$SendToReader;-><init>(Z[S)V

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method

.method public final setLegacyReaderType(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)V
    .locals 2

    const-string v0, "readerType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->audioBackend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    if-nez v0, :cond_0

    const-string v1, "audioBackend"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 108
    :cond_0
    invoke-static {p1}, Lcom/squareup/squarewave/gum/Mapping;->readerTypeToLcrReaderType(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)I

    move-result p1

    .line 106
    invoke-static {v0, p1}, Lcom/squareup/cardreader/lcr/AudioBackendNative;->set_legacy_reader_type(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;I)V

    return-void
.end method

.method public stopSendingToReader()V
    .locals 2

    .line 142
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendV2;->posSender:Lcom/squareup/cardreader/SendsToPos;

    sget-object v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$StopSendingToReader;->INSTANCE:Lcom/squareup/cardreader/ReaderMessage$ReaderOutput$AudioBackendOutput$StopSendingToReader;

    check-cast v1, Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;

    invoke-interface {v0, v1}, Lcom/squareup/cardreader/SendsToPos;->sendResponseToPos(Lcom/squareup/cardreader/ReaderMessage$ReaderOutput;)V

    return-void
.end method
