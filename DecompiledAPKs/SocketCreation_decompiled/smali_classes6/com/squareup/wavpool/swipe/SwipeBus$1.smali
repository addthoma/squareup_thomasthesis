.class Lcom/squareup/wavpool/swipe/SwipeBus$1;
.super Ljava/lang/Object;
.source "SwipeBus.java"

# interfaces
.implements Lcom/squareup/wavpool/swipe/SwipeBus$Danger;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/wavpool/swipe/SwipeBus;->DANGERdoNotUseDirectly()Lcom/squareup/wavpool/swipe/SwipeBus$Danger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/wavpool/swipe/SwipeBus;


# direct methods
.method constructor <init>(Lcom/squareup/wavpool/swipe/SwipeBus;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/SwipeBus$1;->this$0:Lcom/squareup/wavpool/swipe/SwipeBus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public failedSwipes()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;",
            ">;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SwipeBus$1;->this$0:Lcom/squareup/wavpool/swipe/SwipeBus;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/SwipeBus;->access$100(Lcom/squareup/wavpool/swipe/SwipeBus;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    return-object v0
.end method

.method public successfulSwipes()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;",
            ">;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/SwipeBus$1;->this$0:Lcom/squareup/wavpool/swipe/SwipeBus;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/SwipeBus;->access$000(Lcom/squareup/wavpool/swipe/SwipeBus;)Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    return-object v0
.end method
