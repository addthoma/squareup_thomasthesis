.class public final Lcom/squareup/wavpool/swipe/DecoderModule_ProvideSlowSqLinkSignalDecoderFactory;
.super Ljava/lang/Object;
.source "DecoderModule_ProvideSlowSqLinkSignalDecoderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;",
        ">;"
    }
.end annotation


# instance fields
.field private final r4DecoderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/R4Decoder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/R4Decoder;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideSlowSqLinkSignalDecoderFactory;->r4DecoderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/DecoderModule_ProvideSlowSqLinkSignalDecoderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/m1/R4Decoder;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/DecoderModule_ProvideSlowSqLinkSignalDecoderFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideSlowSqLinkSignalDecoderFactory;

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideSlowSqLinkSignalDecoderFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideSlowSqLinkSignalDecoder(Lcom/squareup/squarewave/m1/R4Decoder;)Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/wavpool/swipe/DecoderModule;->provideSlowSqLinkSignalDecoder(Lcom/squareup/squarewave/m1/R4Decoder;)Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideSlowSqLinkSignalDecoderFactory;->r4DecoderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/m1/R4Decoder;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideSlowSqLinkSignalDecoderFactory;->provideSlowSqLinkSignalDecoder(Lcom/squareup/squarewave/m1/R4Decoder;)Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/DecoderModule_ProvideSlowSqLinkSignalDecoderFactory;->get()Lcom/squareup/squarewave/m1/SqLinkSignalDecoder;

    move-result-object v0

    return-object v0
.end method
