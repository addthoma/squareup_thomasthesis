.class public Lcom/squareup/wavpool/swipe/AudioTrackFinisher;
.super Ljava/lang/Object;
.source "AudioTrackFinisher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;,
        Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackListener;
    }
.end annotation


# static fields
.field private static final EXTRA_DELAY_MS:J = 0x96L

.field private static final PLAYBACK_UPDATE_STATUS_UNKNOWN:I = 0x0

.field private static final PLAYBACK_UPDATE_STATUS_USE_TIMER:I = 0x2

.field private static final PLAYBACK_UPDATE_STATUS_WORKS:I = 0x1


# instance fields
.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field private playbackStatus:I

.field private final sampleRate:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/thread/executor/MainThread;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 27
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;->sampleRate:Ljavax/inject/Provider;

    const/4 p1, 0x0

    .line 28
    iput p1, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;->playbackStatus:I

    return-void
.end method

.method static synthetic access$002(Lcom/squareup/wavpool/swipe/AudioTrackFinisher;I)I
    .locals 0

    .line 15
    iput p1, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;->playbackStatus:I

    return p1
.end method

.method static synthetic access$100(Lcom/squareup/wavpool/swipe/AudioTrackFinisher;)Lcom/squareup/thread/executor/MainThread;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;->mainThread:Lcom/squareup/thread/executor/MainThread;

    return-object p0
.end method


# virtual methods
.method public notifyWhenComplete(ILandroid/media/AudioTrack;Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;)V
    .locals 4

    .line 35
    iget v0, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;->playbackStatus:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    int-to-float v0, p1

    .line 36
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;->sampleRate:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float v0, v0, v1

    float-to-long v0, v0

    const-wide/16 v2, 0x96

    add-long/2addr v0, v2

    .line 40
    new-instance v2, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;

    invoke-direct {v2, p0, p3, p2}, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;-><init>(Lcom/squareup/wavpool/swipe/AudioTrackFinisher;Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;Landroid/media/AudioTrack;)V

    .line 41
    iget-object v3, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;->mainThread:Lcom/squareup/thread/executor/MainThread;

    invoke-interface {v3, v2, v0, v1}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 44
    :goto_0
    iget v0, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;->playbackStatus:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 45
    invoke-virtual {p2, p1}, Landroid/media/AudioTrack;->setNotificationMarkerPosition(I)I

    .line 46
    new-instance p1, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackListener;

    invoke-direct {p1, p0, p3, v2}, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackListener;-><init>(Lcom/squareup/wavpool/swipe/AudioTrackFinisher;Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;)V

    invoke-virtual {p2, p1}, Landroid/media/AudioTrack;->setPlaybackPositionUpdateListener(Landroid/media/AudioTrack$OnPlaybackPositionUpdateListener;)V

    :cond_1
    return-void
.end method
