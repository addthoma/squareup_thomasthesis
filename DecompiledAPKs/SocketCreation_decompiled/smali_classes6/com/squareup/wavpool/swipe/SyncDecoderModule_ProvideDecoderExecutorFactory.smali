.class public final Lcom/squareup/wavpool/swipe/SyncDecoderModule_ProvideDecoderExecutorFactory;
.super Ljava/lang/Object;
.source "SyncDecoderModule_ProvideDecoderExecutorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/SyncDecoderModule_ProvideDecoderExecutorFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/concurrent/ExecutorService;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/wavpool/swipe/SyncDecoderModule_ProvideDecoderExecutorFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/wavpool/swipe/SyncDecoderModule_ProvideDecoderExecutorFactory$InstanceHolder;->access$000()Lcom/squareup/wavpool/swipe/SyncDecoderModule_ProvideDecoderExecutorFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideDecoderExecutor()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/wavpool/swipe/SyncDecoderModule;->provideDecoderExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/SyncDecoderModule_ProvideDecoderExecutorFactory;->get()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/wavpool/swipe/SyncDecoderModule_ProvideDecoderExecutorFactory;->provideDecoderExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    return-object v0
.end method
