.class public Lcom/squareup/wavpool/swipe/AudioGraphInitializer;
.super Ljava/lang/Object;
.source "AudioGraphInitializer.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderFactory$CardReaderGraphInitializer;


# instance fields
.field private final audioStartAndStopper:Lcom/squareup/wavpool/swipe/AudioStartAndStopper;

.field private final cardReaderPauseAndResumer:Lcom/squareup/cardreader/CardReaderPauseAndResumer;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderPauseAndResumer;Lcom/squareup/wavpool/swipe/AudioStartAndStopper;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioGraphInitializer;->cardReaderPauseAndResumer:Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    .line 13
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AudioGraphInitializer;->audioStartAndStopper:Lcom/squareup/wavpool/swipe/AudioStartAndStopper;

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .line 21
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioGraphInitializer;->cardReaderPauseAndResumer:Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioGraphInitializer;->audioStartAndStopper:Lcom/squareup/wavpool/swipe/AudioStartAndStopper;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->removeStartsAndStops(Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;)V

    .line 22
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioGraphInitializer;->audioStartAndStopper:Lcom/squareup/wavpool/swipe/AudioStartAndStopper;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/AudioStartAndStopper;->stop()V

    return-void
.end method

.method public initialize()V
    .locals 2

    .line 17
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioGraphInitializer;->cardReaderPauseAndResumer:Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioGraphInitializer;->audioStartAndStopper:Lcom/squareup/wavpool/swipe/AudioStartAndStopper;

    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/CardReaderPauseAndResumer;->addStartsAndStops(Lcom/squareup/cardreader/CardReaderPauseAndResumer$StartsAndStops;)V

    return-void
.end method
