.class public Lcom/squareup/ui/photo/ItemPhoto$Factory;
.super Ljava/lang/Object;
.source "ItemPhoto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/photo/ItemPhoto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/photo/ItemPhoto$Factory$ImageUriUpdated;
    }
.end annotation


# instance fields
.field private final loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

.field private final overrides:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/picasso/Picasso;Lcom/squareup/pollexor/Thumbor;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Lcom/squareup/ui/photo/ItemPhoto$Loader;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/ui/photo/ItemPhoto$Loader;-><init>(Lcom/squareup/picasso/Picasso;Lcom/squareup/pollexor/Thumbor;Lcom/squareup/ui/photo/ItemPhoto$1;)V

    iput-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$Factory;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    .line 73
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/photo/ItemPhoto$Factory;->overrides:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public clearOverride(Ljava/lang/String;)V
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$Factory;->overrides:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public get(Lcom/squareup/checkout/CartItem;)Lcom/squareup/ui/photo/ItemPhoto;
    .locals 7

    .line 77
    iget-object v1, p1, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    .line 78
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->photoUrl:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p1, Lcom/squareup/checkout/CartItem;->photoUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    move-object v2, v0

    .line 79
    iget-object p1, p1, Lcom/squareup/checkout/CartItem;->photoToken:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    .line 80
    new-instance p1, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;

    iget-object v4, p0, Lcom/squareup/ui/photo/ItemPhoto$Factory;->overrides:Ljava/util/Map;

    iget-object v5, p0, Lcom/squareup/ui/photo/ItemPhoto$Factory;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    const/4 v6, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;-><init>(Ljava/lang/String;Landroid/net/Uri;ZLjava/util/Map;Lcom/squareup/ui/photo/ItemPhoto$Loader;Lcom/squareup/ui/photo/ItemPhoto$1;)V

    return-object p1
.end method

.method public get(Lcom/squareup/protos/client/bills/Itemization;)Lcom/squareup/ui/photo/ItemPhoto;
    .locals 1

    .line 93
    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization;->write_only_backing_details:Lcom/squareup/protos/client/bills/Itemization$BackingDetails;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Itemization$BackingDetails;->item_image:Lcom/squareup/api/items/ItemImage;

    if-nez p1, :cond_0

    const-string p1, ""

    goto :goto_0

    .line 94
    :cond_0
    iget-object p1, p1, Lcom/squareup/api/items/ItemImage;->url:Ljava/lang/String;

    :goto_0
    const/4 v0, 0x1

    .line 95
    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->get(Ljava/lang/String;Z)Lcom/squareup/ui/photo/ItemPhoto;

    move-result-object p1

    return-object p1
.end method

.method public get(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Lcom/squareup/ui/photo/ItemPhoto;
    .locals 1

    .line 84
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getImageUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/photo/ItemPhoto;

    move-result-object p1

    return-object p1
.end method

.method public get(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/photo/ItemPhoto;
    .locals 7

    .line 88
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p2, 0x0

    goto :goto_0

    :cond_0
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    :goto_0
    move-object v2, p2

    .line 89
    new-instance p2, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/squareup/ui/photo/ItemPhoto$Factory;->overrides:Ljava/util/Map;

    iget-object v5, p0, Lcom/squareup/ui/photo/ItemPhoto$Factory;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    const/4 v6, 0x0

    move-object v0, p2

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;-><init>(Ljava/lang/String;Landroid/net/Uri;ZLjava/util/Map;Lcom/squareup/ui/photo/ItemPhoto$Loader;Lcom/squareup/ui/photo/ItemPhoto$1;)V

    return-object p2
.end method

.method public get(Ljava/lang/String;Z)Lcom/squareup/ui/photo/ItemPhoto;
    .locals 3

    .line 99
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object p1, v1

    goto :goto_0

    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 100
    :goto_0
    new-instance v0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;

    iget-object v2, p0, Lcom/squareup/ui/photo/ItemPhoto$Factory;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    invoke-direct {v0, p1, p2, v2, v1}, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;-><init>(Landroid/net/Uri;ZLcom/squareup/ui/photo/ItemPhoto$Loader;Lcom/squareup/ui/photo/ItemPhoto$1;)V

    return-object v0
.end method

.method public setOverride(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$Factory;->overrides:Ljava/util/Map;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
