.class final Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$onEnterScope$1;
.super Ljava/lang/Object;
.source "LoggedOutScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/account/AccountEvents$SessionExpired;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/account/AccountEvents$SessionExpired;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/account/AccountEvents$SessionExpired;)V
    .locals 1

    .line 140
    iget-object p1, p0, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$onEnterScope$1;->this$0:Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    invoke-static {p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;->access$getLegacyAuthenticator$p(Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;)Lcom/squareup/account/LegacyAuthenticator;

    move-result-object p1

    sget-object v0, Lcom/squareup/account/LogOutReason$UserPasswordIncorrect;->INSTANCE:Lcom/squareup/account/LogOutReason$UserPasswordIncorrect;

    check-cast v0, Lcom/squareup/account/LogOutReason;

    invoke-interface {p1, v0}, Lcom/squareup/account/LegacyAuthenticator;->logOut(Lcom/squareup/account/LogOutReason;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/account/AccountEvents$SessionExpired;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner$onEnterScope$1;->accept(Lcom/squareup/account/AccountEvents$SessionExpired;)V

    return-void
.end method
