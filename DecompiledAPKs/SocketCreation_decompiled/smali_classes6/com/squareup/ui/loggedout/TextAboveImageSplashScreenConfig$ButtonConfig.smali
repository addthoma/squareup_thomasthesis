.class public final Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;
.super Ljava/lang/Object;
.source "TextAboveImageSplashScreenConfig.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ButtonConfig"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B#\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\'\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0003\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0003\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;",
        "",
        "text",
        "",
        "background",
        "textColor",
        "(III)V",
        "getBackground",
        "()I",
        "getText",
        "getTextColor",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "toString",
        "",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final background:I

.field private final text:I

.field private final textColor:I


# direct methods
.method public constructor <init>(III)V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->text:I

    iput p2, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->background:I

    iput p3, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->textColor:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;IIIILjava/lang/Object;)Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget p1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->text:I

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget p2, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->background:I

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget p3, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->textColor:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->copy(III)Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->text:I

    return v0
.end method

.method public final component2()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->background:I

    return v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->textColor:I

    return v0
.end method

.method public final copy(III)Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;
    .locals 1

    new-instance v0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;-><init>(III)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;

    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->text:I

    iget v1, p1, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->text:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->background:I

    iget v1, p1, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->background:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->textColor:I

    iget p1, p1, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->textColor:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getBackground()I
    .locals 1

    .line 57
    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->background:I

    return v0
.end method

.method public final getText()I
    .locals 1

    .line 56
    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->text:I

    return v0
.end method

.method public final getTextColor()I
    .locals 1

    .line 58
    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->textColor:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->text:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->background:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->textColor:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ButtonConfig(text="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->text:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", background="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->background:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", textColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/ui/loggedout/TextAboveImageSplashScreenConfig$ButtonConfig;->textColor:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
