.class public Lcom/squareup/ui/loggedout/LandingPortraitView;
.super Lcom/squareup/widgets/HalfAndHalfLayout;
.source "LandingPortraitView.java"

# interfaces
.implements Lcom/squareup/ui/loggedout/LandingScreen$PaymentLandingView;


# instance fields
.field presenter:Lcom/squareup/ui/loggedout/LandingScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/HalfAndHalfLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const-class p2, Lcom/squareup/ui/loggedout/AbstractLandingScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/loggedout/AbstractLandingScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/loggedout/AbstractLandingScreen$Component;->inject(Lcom/squareup/ui/loggedout/LandingPortraitView;)V

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LandingPortraitView;->presenter:Lcom/squareup/ui/loggedout/LandingScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/loggedout/LandingScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 34
    invoke-super {p0}, Lcom/squareup/widgets/HalfAndHalfLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 24
    invoke-super {p0}, Lcom/squareup/widgets/HalfAndHalfLayout;->onFinishInflate()V

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LandingPortraitView;->presenter:Lcom/squareup/ui/loggedout/LandingScreen$Presenter;

    invoke-static {p0, v0}, Lcom/squareup/ui/loggedout/LandingViews;->viewFinishedInflate(Landroid/view/View;Lcom/squareup/ui/loggedout/LandingScreen$Presenter;)V

    .line 27
    invoke-static {p0}, Lcom/squareup/ui/loggedout/LandingViews;->setupReader(Landroid/view/View;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/loggedout/LandingPortraitView;->presenter:Lcom/squareup/ui/loggedout/LandingScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/loggedout/LandingScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public showLearnMore()V
    .locals 2

    .line 42
    sget v0, Lcom/squareup/loggedout/R$string;->learn_about_square:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/squareup/ui/loggedout/LandingViews;->setCallToAction(Landroid/view/View;IZ)V

    return-void
.end method

.method public startReaderAnimation()V
    .locals 0

    .line 38
    invoke-static {p0}, Lcom/squareup/ui/loggedout/LandingViews;->startReaderAnimation(Landroid/view/View;)V

    return-void
.end method
