.class public final Lcom/squareup/ui/timecards/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final employee_management_timecards_back_button_width:I = 0x7f07014c

.field public static final employee_management_timecards_button_height:I = 0x7f07014d

.field public static final employee_management_timecards_button_text_size:I = 0x7f07014e

.field public static final employee_management_timecards_clocked_in_green_indicator_margin:I = 0x7f07014f

.field public static final employee_management_timecards_clocked_in_green_indicator_size:I = 0x7f070150

.field public static final employee_management_timecards_current_time_text_size:I = 0x7f070151

.field public static final employee_management_timecards_employee_name_margin:I = 0x7f070152

.field public static final employee_management_timecards_glyph_margin:I = 0x7f070153

.field public static final employee_management_timecards_glyph_margin_bottom_card:I = 0x7f070154

.field public static final employee_management_timecards_glyph_margin_top_card:I = 0x7f070155

.field public static final employee_management_timecards_gutter:I = 0x7f070156

.field public static final employee_management_timecards_gutter_card:I = 0x7f070157

.field public static final employee_management_timecards_margin:I = 0x7f070158

.field public static final employee_management_timecards_sub_header_text_size:I = 0x7f070159

.field public static final employee_management_timecards_summary_margin:I = 0x7f07015a

.field public static final employee_management_timecards_summary_text_size:I = 0x7f07015b


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
