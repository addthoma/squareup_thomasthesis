.class final Lcom/squareup/ui/timecards/TimecardsRunnerLauncher$showTimecardsLoadingScreenFromHome$1;
.super Ljava/lang/Object;
.source "TimecardsRunnerLauncher.kt"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;->showTimecardsLoadingScreenFromHome()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/Command;",
        "kotlin.jvm.PlatformType",
        "history",
        "Lflow/History;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;


# direct methods
.method constructor <init>(Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher$showTimecardsLoadingScreenFromHome$1;->this$0:Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 1

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher$showTimecardsLoadingScreenFromHome$1;->this$0:Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;

    invoke-static {v0}, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;->access$getHome$p(Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;)Lcom/squareup/ui/main/Home;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/squareup/ui/main/HomeKt;->buildUpon(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History$Builder;

    move-result-object p1

    .line 46
    sget-object v0, Lcom/squareup/ui/timecards/TimecardsLoadingScreen;->INSTANCE:Lcom/squareup/ui/timecards/TimecardsLoadingScreen;

    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    .line 47
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    .line 48
    sget-object v0, Lflow/Direction;->FORWARD:Lflow/Direction;

    .line 44
    invoke-static {p1, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method
