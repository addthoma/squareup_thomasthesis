.class public Lcom/squareup/ui/timecards/ClockInModalCoordinator;
.super Lcom/squareup/ui/timecards/TimecardsCoordinator;
.source "ClockInModalCoordinator.java"


# instance fields
.field private appNameFormatter:Lcom/squareup/util/AppNameFormatter;

.field private successContinueButton:Lcom/squareup/marketfont/MarketButton;

.field private successTitle:Lcom/squareup/marketfont/MarketTextView;

.field private successView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/AppNameFormatter;)V
    .locals 0
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/timecards/TimecardsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V

    .line 32
    iput-object p6, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 36
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->attach(Landroid/view/View;)V

    .line 38
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInModalCoordinator$MnYBM-dxGg61ISj6pSDg6guvAPg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInModalCoordinator$MnYBM-dxGg61ISj6pSDg6guvAPg;-><init>(Lcom/squareup/ui/timecards/ClockInModalCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->successContinueButton:Lcom/squareup/marketfont/MarketButton;

    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_success_continue_button_text:I

    invoke-interface {v0, v1}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->successContinueButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInModalCoordinator$ersVVM9H4zRaipEtlM8xMSmKVC4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInModalCoordinator$ersVVM9H4zRaipEtlM8xMSmKVC4;-><init>(Lcom/squareup/ui/timecards/ClockInModalCoordinator;)V

    .line 47
    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 46
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected bindViews(Landroid/view/View;)V
    .locals 2

    .line 82
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 83
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_modal:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->successView:Landroid/view/View;

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->successView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 85
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_title_modal:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->successTitle:Lcom/squareup/marketfont/MarketTextView;

    .line 86
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_continue_button_modal:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->successContinueButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method protected getErrorMessage()Ljava/lang/String;
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorTitle()Ljava/lang/String;
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getProgressBarTitle()Ljava/lang/String;
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_clock_in_progress_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSuccessViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 59
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method protected isCardScreen()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public synthetic lambda$attach$0$ClockInModalCoordinator()Lrx/Subscription;
    .locals 3

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->clockInScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 39
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$_ZFCKyBIE5HYT9k5foP3CCeNx4M;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$_ZFCKyBIE5HYT9k5foP3CCeNx4M;-><init>(Lcom/squareup/ui/timecards/ClockInModalCoordinator;)V

    .line 40
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$z5sAoJbuAQkbQY8L48T7tc6wC50;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$z5sAoJbuAQkbQY8L48T7tc6wC50;-><init>(Lcom/squareup/ui/timecards/ClockInModalCoordinator;)V

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$CZ2u_anCls9gdVyma4A_5tgkSDk;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$CZ2u_anCls9gdVyma4A_5tgkSDk;-><init>(Lcom/squareup/ui/timecards/ClockInModalCoordinator;)V

    .line 41
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$1$ClockInModalCoordinator(Landroid/view/View;)V
    .locals 0

    .line 47
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->navigateToHomeScreen()V

    return-void
.end method

.method protected showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 5

    .line 71
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    .line 72
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/timecards/ClockInScreen$Data;

    .line 73
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->currentTimeView:Lcom/squareup/marketfont/MarketTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 74
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    iget-object v3, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v4, Lcom/squareup/ui/timecards/-$$Lambda$cePlQqc8brG5yNhhmgl4PpRKLXE;

    invoke-direct {v4, v3}, Lcom/squareup/ui/timecards/-$$Lambda$cePlQqc8brG5yNhhmgl4PpRKLXE;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    invoke-virtual {v1, v4}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->updateConfigWithUpButtonCommand(Ljava/lang/Runnable;)V

    .line 75
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->successTitle:Lcom/squareup/marketfont/MarketTextView;

    iget-object v3, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v0, v0, Lcom/squareup/ui/timecards/ClockInScreen$Data;->jobTitle:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/squareup/ui/timecards/ClockInCoordinator;->getClockedInSummary(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object p1, p1, Lcom/squareup/ui/timecards/TimecardsScreenData;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInModalCoordinator;->successContinueButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p1, v2}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    :cond_0
    return-void
.end method
