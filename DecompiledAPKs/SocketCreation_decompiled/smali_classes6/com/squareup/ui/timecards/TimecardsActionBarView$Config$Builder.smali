.class public Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
.super Ljava/lang/Object;
.source "TimecardsActionBarView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private actionBarVisible:Z

.field private primaryButtonCommand:Ljava/lang/Runnable;

.field private primaryButtonEnabled:Z

.field private primaryButtonText:Ljava/lang/CharSequence;

.field private primaryButtonVisible:Z

.field public titleTextPrimary:Ljava/lang/CharSequence;

.field public titleTextSecondary:Ljava/lang/CharSequence;

.field public titleTextVisible:Z

.field public upButtonCommand:Ljava/lang/Runnable;

.field private upButtonContentDescription:Ljava/lang/CharSequence;

.field private upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

.field private upButtonText:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 217
    iput-boolean v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->actionBarVisible:Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Z
    .locals 0

    .line 216
    iget-boolean p0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->actionBarVisible:Z

    return p0
.end method

.method static synthetic access$002(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Z)Z
    .locals 0

    .line 216
    iput-boolean p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->actionBarVisible:Z

    return p1
.end method

.method static synthetic access$100(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    .line 216
    iget-object p0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method static synthetic access$102(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Ljava/lang/CharSequence;
    .locals 0

    .line 216
    iget-object p0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->upButtonContentDescription:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$202(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->upButtonContentDescription:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$300(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Ljava/lang/CharSequence;
    .locals 0

    .line 216
    iget-object p0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->upButtonText:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$302(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->upButtonText:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$400(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Z
    .locals 0

    .line 216
    iget-boolean p0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->primaryButtonVisible:Z

    return p0
.end method

.method static synthetic access$402(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Z)Z
    .locals 0

    .line 216
    iput-boolean p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->primaryButtonVisible:Z

    return p1
.end method

.method static synthetic access$500(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Z
    .locals 0

    .line 216
    iget-boolean p0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->primaryButtonEnabled:Z

    return p0
.end method

.method static synthetic access$502(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Z)Z
    .locals 0

    .line 216
    iput-boolean p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->primaryButtonEnabled:Z

    return p1
.end method

.method static synthetic access$600(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Ljava/lang/CharSequence;
    .locals 0

    .line 216
    iget-object p0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->primaryButtonText:Ljava/lang/CharSequence;

    return-object p0
.end method

.method static synthetic access$602(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->primaryButtonText:Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$700(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;)Ljava/lang/Runnable;
    .locals 0

    .line 216
    iget-object p0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->primaryButtonCommand:Ljava/lang/Runnable;

    return-object p0
.end method

.method static synthetic access$702(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .line 216
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->primaryButtonCommand:Ljava/lang/Runnable;

    return-object p1
.end method


# virtual methods
.method public build()Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;
    .locals 2

    .line 291
    new-instance v0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config;-><init>(Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;Lcom/squareup/ui/timecards/TimecardsActionBarView$1;)V

    return-object v0
.end method

.method public setActionBarVisible(Z)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
    .locals 0

    .line 231
    iput-boolean p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->actionBarVisible:Z

    return-object p0
.end method

.method public setPrimaryButtonCommand(Ljava/lang/Runnable;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
    .locals 0

    .line 286
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->primaryButtonCommand:Ljava/lang/Runnable;

    return-object p0
.end method

.method public setPrimaryButtonEnabled(Z)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
    .locals 0

    .line 276
    iput-boolean p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->primaryButtonEnabled:Z

    return-object p0
.end method

.method public setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
    .locals 0

    .line 281
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->primaryButtonText:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setPrimaryButtonVisible(Z)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
    .locals 0

    .line 271
    iput-boolean p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->primaryButtonVisible:Z

    return-object p0
.end method

.method public setTitleTextPrimary(Ljava/lang/CharSequence;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
    .locals 0

    .line 261
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->titleTextPrimary:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setTitleTextSecondary(Ljava/lang/CharSequence;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
    .locals 0

    .line 266
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->titleTextSecondary:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setTitleTextVisible(Z)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
    .locals 0

    .line 256
    iput-boolean p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->titleTextVisible:Z

    return-object p0
.end method

.method public setUpButtonCommand(Ljava/lang/Runnable;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
    .locals 0

    .line 246
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->upButtonCommand:Ljava/lang/Runnable;

    return-object p0
.end method

.method public setUpButtonContentDescription(Ljava/lang/CharSequence;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
    .locals 0

    .line 241
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->upButtonContentDescription:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public setUpButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
    .locals 0

    .line 236
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->upButtonGlyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object p0
.end method

.method public setUpButtonText(Ljava/lang/CharSequence;)Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;
    .locals 0

    .line 251
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView$Config$Builder;->upButtonText:Ljava/lang/CharSequence;

    return-object p0
.end method
