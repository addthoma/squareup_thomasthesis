.class public interface abstract Lcom/squareup/ui/timecards/api/TimecardsLauncher;
.super Ljava/lang/Object;
.source "TimecardsLauncher.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u000e\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H&J\u0008\u0010\u0007\u001a\u00020\u0008H&J\u0008\u0010\t\u001a\u00020\u0008H&J\u0008\u0010\n\u001a\u00020\u0008H&J\u0008\u0010\u000b\u001a\u00020\u0008H&\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
        "",
        "clockInOutHistoryFactory",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "isClockInOutScreenVisible",
        "Lio/reactivex/Observable;",
        "",
        "showClockInOrContinueScreenFromHome",
        "",
        "showClockInOut",
        "showClockInOutFromPasscode",
        "showTimecardsLoadingScreenFromHome",
        "timecards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract clockInOutHistoryFactory()Lcom/squareup/ui/main/HistoryFactory;
.end method

.method public abstract isClockInOutScreenVisible()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showClockInOrContinueScreenFromHome()V
.end method

.method public abstract showClockInOut()V
.end method

.method public abstract showClockInOutFromPasscode()V
.end method

.method public abstract showTimecardsLoadingScreenFromHome()V
.end method
