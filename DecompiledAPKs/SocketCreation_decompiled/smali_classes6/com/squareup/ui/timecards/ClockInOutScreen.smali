.class public final Lcom/squareup/ui/timecards/ClockInOutScreen;
.super Lcom/squareup/ui/timecards/InTimecardsScope;
.source "ClockInOutScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/timecards/ClockInOutScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/timecards/ClockInOutScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/timecards/ClockInOutScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final FROM_PASSCODE:Lcom/squareup/ui/timecards/ClockInOutScreen;

.field public static final NORMAL:Lcom/squareup/ui/timecards/ClockInOutScreen;


# instance fields
.field final firstScreenInLayer:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 19
    new-instance v0, Lcom/squareup/ui/timecards/ClockInOutScreen;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/ClockInOutScreen;-><init>(Z)V

    sput-object v0, Lcom/squareup/ui/timecards/ClockInOutScreen;->NORMAL:Lcom/squareup/ui/timecards/ClockInOutScreen;

    .line 20
    new-instance v0, Lcom/squareup/ui/timecards/ClockInOutScreen;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/timecards/ClockInOutScreen;-><init>(Z)V

    sput-object v0, Lcom/squareup/ui/timecards/ClockInOutScreen;->FROM_PASSCODE:Lcom/squareup/ui/timecards/ClockInOutScreen;

    .line 50
    sget-object v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutScreen$NcM1wFxV0JuEWx7HGpO4kwDYYPQ;->INSTANCE:Lcom/squareup/ui/timecards/-$$Lambda$ClockInOutScreen$NcM1wFxV0JuEWx7HGpO4kwDYYPQ;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/timecards/ClockInOutScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/timecards/InTimecardsScope;-><init>()V

    .line 37
    iput-boolean p1, p0, Lcom/squareup/ui/timecards/ClockInOutScreen;->firstScreenInLayer:Z

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/timecards/ClockInOutScreen;
    .locals 1

    .line 51
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 52
    sget-object p0, Lcom/squareup/ui/timecards/ClockInOutScreen;->NORMAL:Lcom/squareup/ui/timecards/ClockInOutScreen;

    goto :goto_1

    :cond_1
    sget-object p0, Lcom/squareup/ui/timecards/ClockInOutScreen;->FROM_PASSCODE:Lcom/squareup/ui/timecards/ClockInOutScreen;

    :goto_1
    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 47
    iget-boolean p2, p0, Lcom/squareup/ui/timecards/ClockInOutScreen;->firstScreenInLayer:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 25
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_PASSCODE:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 33
    iget-boolean p1, p0, Lcom/squareup/ui/timecards/ClockInOutScreen;->firstScreenInLayer:Z

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    :goto_0
    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 29
    sget v0, Lcom/squareup/ui/timecards/R$layout;->clock_in_out_view:I

    return v0
.end method
