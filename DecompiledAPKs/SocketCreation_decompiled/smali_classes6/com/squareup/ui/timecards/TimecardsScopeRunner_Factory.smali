.class public final Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;
.super Ljava/lang/Object;
.source "TimecardsScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/timecards/TimecardsScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final currentTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featureReleaseHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/FeatureReleaseHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final posContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final timecardsPrintingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/TimecardsPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final timecardsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/Timecards;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/Timecards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/FeatureReleaseHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/TimecardsPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)V"
        }
    .end annotation

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p2, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p3, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p4, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->timecardsProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p5, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p6, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p7, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p8, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p9, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p10, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->posContainerProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p11, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p12, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->featureReleaseHelperProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p13, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->timecardsPrintingDispatcherProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p14, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 84
    iput-object p15, p0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->localeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/Timecards;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/FeatureReleaseHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/TimecardsPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;"
        }
    .end annotation

    .line 103
    new-instance v16, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v16
.end method

.method public static newInstance(Landroid/app/Application;Lcom/squareup/time/CurrentTime;Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/Timecards;Lflow/Flow;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Device;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/timecards/FeatureReleaseHelper;Lcom/squareup/print/TimecardsPrintingDispatcher;Lcom/squareup/permissions/PermissionGatekeeper;Ljavax/inject/Provider;)Lcom/squareup/ui/timecards/TimecardsScopeRunner;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/timecards/Timecards;",
            "Lflow/Flow;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/ui/timecards/FeatureReleaseHelper;",
            "Lcom/squareup/print/TimecardsPrintingDispatcher;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;)",
            "Lcom/squareup/ui/timecards/TimecardsScopeRunner;"
        }
    .end annotation

    .line 113
    new-instance v16, Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;-><init>(Landroid/app/Application;Lcom/squareup/time/CurrentTime;Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/Timecards;Lflow/Flow;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Device;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/timecards/FeatureReleaseHelper;Lcom/squareup/print/TimecardsPrintingDispatcher;Lcom/squareup/permissions/PermissionGatekeeper;Ljavax/inject/Provider;)V

    return-object v16
.end method


# virtual methods
.method public get()Lcom/squareup/ui/timecards/TimecardsScopeRunner;
    .locals 17

    move-object/from16 v0, p0

    .line 89
    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/app/Application;

    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/time/CurrentTime;

    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->timecardsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/timecards/Timecards;

    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->posContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->featureReleaseHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/ui/timecards/FeatureReleaseHelper;

    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->timecardsPrintingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/print/TimecardsPrintingDispatcher;

    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->localeProvider:Ljavax/inject/Provider;

    move-object/from16 v16, v1

    invoke-static/range {v2 .. v16}, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->newInstance(Landroid/app/Application;Lcom/squareup/time/CurrentTime;Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/Timecards;Lflow/Flow;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/util/Device;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/timecards/FeatureReleaseHelper;Lcom/squareup/print/TimecardsPrintingDispatcher;Lcom/squareup/permissions/PermissionGatekeeper;Ljavax/inject/Provider;)Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner_Factory;->get()Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    move-result-object v0

    return-object v0
.end method
