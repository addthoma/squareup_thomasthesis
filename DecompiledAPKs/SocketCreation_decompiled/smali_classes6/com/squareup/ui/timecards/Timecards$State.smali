.class public Lcom/squareup/ui/timecards/Timecards$State;
.super Ljava/lang/Object;
.source "Timecards.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/Timecards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "State"
.end annotation


# instance fields
.field public final breakStartTime:Ljava/util/Date;

.field public final clockedInCurrentWorkday:Ljava/lang/Boolean;

.field public final currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

.field public final employee:Lcom/squareup/permissions/Employee;

.field public final expectedBreakDurationSeconds:Ljava/lang/Integer;

.field public final jobs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
            ">;"
        }
    .end annotation
.end field

.field public final minBreakDurationSeconds:Ljava/lang/Integer;

.field public final previousTimecard:Lcom/squareup/protos/client/timecards/Timecard;

.field public final timecardBreakDefinitions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final timecardBreakId:Ljava/lang/String;

.field public final timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

.field public final totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;

.field public final workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/Employee;)V
    .locals 0

    .line 428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 429
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 430
    sget-object p1, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->LOADING:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    const/4 p1, 0x0

    .line 431
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    .line 432
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    .line 433
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 434
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$State;->clockedInCurrentWorkday:Ljava/lang/Boolean;

    .line 435
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$State;->previousTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 436
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$State;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    .line 437
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakId:Ljava/lang/String;

    .line 438
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    .line 439
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$State;->expectedBreakDurationSeconds:Ljava/lang/Integer;

    .line 440
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$State;->minBreakDurationSeconds:Ljava/lang/Integer;

    .line 441
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$State;->totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/permissions/Employee;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/timecards/Timecard;Ljava/lang/Boolean;Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/permissions/Employee;",
            "Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
            ">;",
            "Lcom/squareup/protos/client/timecards/Timecard;",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/protos/client/timecards/Timecard;",
            "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .line 449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 450
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    .line 451
    iput-object p2, p0, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    .line 452
    iput-object p3, p0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    .line 453
    iput-object p4, p0, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    .line 454
    iput-object p6, p0, Lcom/squareup/ui/timecards/Timecards$State;->clockedInCurrentWorkday:Ljava/lang/Boolean;

    .line 455
    iput-object p9, p0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakId:Ljava/lang/String;

    .line 456
    iput-object p10, p0, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    .line 457
    iput-object p11, p0, Lcom/squareup/ui/timecards/Timecards$State;->expectedBreakDurationSeconds:Ljava/lang/Integer;

    .line 458
    iput-object p12, p0, Lcom/squareup/ui/timecards/Timecards$State;->minBreakDurationSeconds:Ljava/lang/Integer;

    .line 459
    iput-object p5, p0, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 460
    iput-object p7, p0, Lcom/squareup/ui/timecards/Timecards$State;->previousTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 461
    iput-object p8, p0, Lcom/squareup/ui/timecards/Timecards$State;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    .line 462
    iput-object p13, p0, Lcom/squareup/ui/timecards/Timecards$State;->totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public clearTimecard(JLcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;)Lcom/squareup/ui/timecards/Timecards$State;
    .locals 17

    move-object/from16 v0, p0

    .line 514
    new-instance v1, Lcom/squareup/protos/client/timecards/Timecard$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/timecards/Timecard$Builder;-><init>()V

    .line 515
    iget-object v2, v0, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, v2, Lcom/squareup/protos/client/timecards/Timecard;->token:Ljava/lang/String;

    iput-object v2, v1, Lcom/squareup/protos/client/timecards/Timecard$Builder;->token:Ljava/lang/String;

    .line 516
    iget-object v2, v0, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v2, v2, Lcom/squareup/protos/client/timecards/Timecard;->clockin_timestamp_ms:Ljava/lang/Long;

    iput-object v2, v1, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockin_timestamp_ms:Ljava/lang/Long;

    .line 517
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockout_timestamp_ms:Ljava/lang/Long;

    .line 518
    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/Timecard$Builder;->build()Lcom/squareup/protos/client/timecards/Timecard;

    move-result-object v10

    .line 519
    new-instance v1, Lcom/squareup/ui/timecards/Timecards$State;

    iget-object v4, v0, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    sget-object v5, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v9, v0, Lcom/squareup/ui/timecards/Timecards$State;->clockedInCurrentWorkday:Ljava/lang/Boolean;

    iget-object v2, v0, Lcom/squareup/ui/timecards/Timecards$State;->totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v3, v1

    move-object/from16 v11, p3

    move-object/from16 v16, v2

    invoke-direct/range {v3 .. v16}, Lcom/squareup/ui/timecards/Timecards$State;-><init>(Lcom/squareup/permissions/Employee;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/timecards/Timecard;Ljava/lang/Boolean;Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    return-object v1
.end method

.method public clearTimecardBreak()Lcom/squareup/ui/timecards/Timecards$State;
    .locals 1

    const/4 v0, 0x0

    .line 533
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/squareup/ui/timecards/Timecards$State;->withTimecardBreak(Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/ui/timecards/Timecards$State;

    move-result-object v0

    return-object v0
.end method

.method public withNotes(Ljava/lang/String;)Lcom/squareup/ui/timecards/Timecards$State;
    .locals 14

    .line 537
    iget-object v0, p0, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    invoke-virtual {v0}, Lcom/squareup/protos/client/timecards/Timecard;->newBuilder()Lcom/squareup/protos/client/timecards/Timecard$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/timecards/Timecard$Builder;->timecard_notes(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/Timecard$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/Timecard$Builder;->build()Lcom/squareup/protos/client/timecards/Timecard;

    move-result-object v5

    .line 538
    new-instance p1, Lcom/squareup/ui/timecards/Timecards$State;

    iget-object v1, p0, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    iget-object v2, p0, Lcom/squareup/ui/timecards/Timecards$State;->timecardRequestState:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v3, p0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/ui/timecards/Timecards$State;->clockedInCurrentWorkday:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/ui/timecards/Timecards$State;->previousTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v8, p0, Lcom/squareup/ui/timecards/Timecards$State;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iget-object v9, p0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakId:Ljava/lang/String;

    iget-object v10, p0, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    iget-object v11, p0, Lcom/squareup/ui/timecards/Timecards$State;->expectedBreakDurationSeconds:Ljava/lang/Integer;

    iget-object v12, p0, Lcom/squareup/ui/timecards/Timecards$State;->minBreakDurationSeconds:Ljava/lang/Integer;

    iget-object v13, p0, Lcom/squareup/ui/timecards/Timecards$State;->totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;

    move-object v0, p1

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/timecards/Timecards$State;-><init>(Lcom/squareup/permissions/Employee;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/timecards/Timecard;Ljava/lang/Boolean;Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    return-object p1
.end method

.method public withTimecard(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Boolean;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/Long;Ljava/lang/String;)Lcom/squareup/ui/timecards/Timecards$State;
    .locals 17

    move-object/from16 v0, p0

    .line 501
    new-instance v1, Lcom/squareup/protos/client/timecards/Timecard$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/timecards/Timecard$Builder;-><init>()V

    move-object/from16 v2, p1

    .line 502
    iput-object v2, v1, Lcom/squareup/protos/client/timecards/Timecard$Builder;->token:Ljava/lang/String;

    move-object/from16 v2, p2

    .line 503
    iput-object v2, v1, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockin_unit_token:Ljava/lang/String;

    .line 504
    invoke-virtual/range {p3 .. p3}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockin_timestamp_ms:Ljava/lang/Long;

    move-object/from16 v2, p5

    .line 505
    iput-object v2, v1, Lcom/squareup/protos/client/timecards/Timecard$Builder;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    move-object/from16 v2, p7

    .line 506
    iput-object v2, v1, Lcom/squareup/protos/client/timecards/Timecard$Builder;->timecard_notes:Ljava/lang/String;

    .line 507
    new-instance v16, Lcom/squareup/ui/timecards/Timecards$State;

    iget-object v3, v0, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    sget-object v4, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v5, v0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    iget-object v6, v0, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    .line 508
    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/Timecard$Builder;->build()Lcom/squareup/protos/client/timecards/Timecard;

    move-result-object v7

    iget-object v9, v0, Lcom/squareup/ui/timecards/Timecards$State;->previousTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v10, v0, Lcom/squareup/ui/timecards/Timecards$State;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v2, v16

    move-object/from16 v8, p4

    move-object/from16 v15, p6

    invoke-direct/range {v2 .. v15}, Lcom/squareup/ui/timecards/Timecards$State;-><init>(Lcom/squareup/permissions/Employee;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/timecards/Timecard;Ljava/lang/Boolean;Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    return-object v16
.end method

.method public withTimecardBreak(Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;)Lcom/squareup/ui/timecards/Timecards$State;
    .locals 16

    move-object/from16 v0, p0

    .line 526
    new-instance v15, Lcom/squareup/ui/timecards/Timecards$State;

    iget-object v2, v0, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    sget-object v3, Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;->SUCCESS:Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;

    iget-object v4, v0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    iget-object v5, v0, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    iget-object v6, v0, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v7, v0, Lcom/squareup/ui/timecards/Timecards$State;->clockedInCurrentWorkday:Ljava/lang/Boolean;

    iget-object v8, v0, Lcom/squareup/ui/timecards/Timecards$State;->previousTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v9, v0, Lcom/squareup/ui/timecards/Timecards$State;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iget-object v14, v0, Lcom/squareup/ui/timecards/Timecards$State;->totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;

    move-object v1, v15

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    move-object/from16 v13, p4

    invoke-direct/range {v1 .. v14}, Lcom/squareup/ui/timecards/Timecards$State;-><init>(Lcom/squareup/permissions/Employee;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/timecards/Timecard;Ljava/lang/Boolean;Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    return-object v15
.end method

.method public withTimecardRequestState(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;)Lcom/squareup/ui/timecards/Timecards$State;
    .locals 16

    move-object/from16 v0, p0

    .line 466
    new-instance v15, Lcom/squareup/ui/timecards/Timecards$State;

    iget-object v2, v0, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    iget-object v4, v0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    iget-object v5, v0, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    iget-object v6, v0, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v7, v0, Lcom/squareup/ui/timecards/Timecards$State;->clockedInCurrentWorkday:Ljava/lang/Boolean;

    iget-object v8, v0, Lcom/squareup/ui/timecards/Timecards$State;->previousTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v9, v0, Lcom/squareup/ui/timecards/Timecards$State;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iget-object v10, v0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakId:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    iget-object v12, v0, Lcom/squareup/ui/timecards/Timecards$State;->expectedBreakDurationSeconds:Ljava/lang/Integer;

    iget-object v13, v0, Lcom/squareup/ui/timecards/Timecards$State;->minBreakDurationSeconds:Ljava/lang/Integer;

    iget-object v14, v0, Lcom/squareup/ui/timecards/Timecards$State;->totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;

    move-object v1, v15

    move-object/from16 v3, p1

    invoke-direct/range {v1 .. v14}, Lcom/squareup/ui/timecards/Timecards$State;-><init>(Lcom/squareup/permissions/Employee;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/timecards/Timecard;Ljava/lang/Boolean;Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    return-object v15
.end method

.method public withTimecardRequestStateAndBreakDefinitions(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/util/List;)Lcom/squareup/ui/timecards/Timecards$State;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
            ">;)",
            "Lcom/squareup/ui/timecards/Timecards$State;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 483
    new-instance v15, Lcom/squareup/ui/timecards/Timecards$State;

    iget-object v2, v0, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    iget-object v5, v0, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    iget-object v6, v0, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v7, v0, Lcom/squareup/ui/timecards/Timecards$State;->clockedInCurrentWorkday:Ljava/lang/Boolean;

    iget-object v8, v0, Lcom/squareup/ui/timecards/Timecards$State;->previousTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v9, v0, Lcom/squareup/ui/timecards/Timecards$State;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iget-object v10, v0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakId:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    iget-object v12, v0, Lcom/squareup/ui/timecards/Timecards$State;->expectedBreakDurationSeconds:Ljava/lang/Integer;

    iget-object v13, v0, Lcom/squareup/ui/timecards/Timecards$State;->minBreakDurationSeconds:Ljava/lang/Integer;

    iget-object v14, v0, Lcom/squareup/ui/timecards/Timecards$State;->totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;

    move-object v1, v15

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    invoke-direct/range {v1 .. v14}, Lcom/squareup/ui/timecards/Timecards$State;-><init>(Lcom/squareup/permissions/Employee;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/timecards/Timecard;Ljava/lang/Boolean;Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    return-object v15
.end method

.method public withTimecardRequestStateAndJobs(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/util/List;)Lcom/squareup/ui/timecards/Timecards$State;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;",
            ">;)",
            "Lcom/squareup/ui/timecards/Timecards$State;"
        }
    .end annotation

    move-object/from16 v0, p0

    .line 491
    new-instance v15, Lcom/squareup/ui/timecards/Timecards$State;

    iget-object v2, v0, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    iget-object v4, v0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    iget-object v6, v0, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v7, v0, Lcom/squareup/ui/timecards/Timecards$State;->clockedInCurrentWorkday:Ljava/lang/Boolean;

    iget-object v8, v0, Lcom/squareup/ui/timecards/Timecards$State;->previousTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v9, v0, Lcom/squareup/ui/timecards/Timecards$State;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iget-object v10, v0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakId:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    iget-object v12, v0, Lcom/squareup/ui/timecards/Timecards$State;->expectedBreakDurationSeconds:Ljava/lang/Integer;

    iget-object v13, v0, Lcom/squareup/ui/timecards/Timecards$State;->minBreakDurationSeconds:Ljava/lang/Integer;

    iget-object v14, v0, Lcom/squareup/ui/timecards/Timecards$State;->totalSecondsClockedInForCurrentWorkday:Ljava/lang/Long;

    move-object v1, v15

    move-object/from16 v3, p1

    move-object/from16 v5, p2

    invoke-direct/range {v1 .. v14}, Lcom/squareup/ui/timecards/Timecards$State;-><init>(Lcom/squareup/permissions/Employee;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/timecards/Timecard;Ljava/lang/Boolean;Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    return-object v15
.end method

.method public withTimecardRequestStateAndTotalSecondsWorked(Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/lang/Long;)Lcom/squareup/ui/timecards/Timecards$State;
    .locals 16

    move-object/from16 v0, p0

    .line 474
    new-instance v15, Lcom/squareup/ui/timecards/Timecards$State;

    iget-object v2, v0, Lcom/squareup/ui/timecards/Timecards$State;->employee:Lcom/squareup/permissions/Employee;

    iget-object v4, v0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakDefinitions:Ljava/util/List;

    iget-object v5, v0, Lcom/squareup/ui/timecards/Timecards$State;->jobs:Ljava/util/List;

    iget-object v6, v0, Lcom/squareup/ui/timecards/Timecards$State;->currentTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v7, v0, Lcom/squareup/ui/timecards/Timecards$State;->clockedInCurrentWorkday:Ljava/lang/Boolean;

    iget-object v8, v0, Lcom/squareup/ui/timecards/Timecards$State;->previousTimecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v9, v0, Lcom/squareup/ui/timecards/Timecards$State;->workdayShiftSummary:Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    iget-object v10, v0, Lcom/squareup/ui/timecards/Timecards$State;->timecardBreakId:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/ui/timecards/Timecards$State;->breakStartTime:Ljava/util/Date;

    iget-object v12, v0, Lcom/squareup/ui/timecards/Timecards$State;->expectedBreakDurationSeconds:Ljava/lang/Integer;

    iget-object v13, v0, Lcom/squareup/ui/timecards/Timecards$State;->minBreakDurationSeconds:Ljava/lang/Integer;

    move-object v1, v15

    move-object/from16 v3, p1

    move-object/from16 v14, p2

    invoke-direct/range {v1 .. v14}, Lcom/squareup/ui/timecards/Timecards$State;-><init>(Lcom/squareup/permissions/Employee;Lcom/squareup/ui/timecards/Timecards$TimecardRequestState;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/timecards/Timecard;Ljava/lang/Boolean;Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;Ljava/lang/String;Ljava/util/Date;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;)V

    return-object v15
.end method
