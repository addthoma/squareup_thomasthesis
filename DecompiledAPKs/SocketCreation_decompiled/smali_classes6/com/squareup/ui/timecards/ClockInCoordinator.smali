.class public Lcom/squareup/ui/timecards/ClockInCoordinator;
.super Lcom/squareup/ui/timecards/TimecardsCoordinator;
.source "ClockInCoordinator.java"


# instance fields
.field private successContainer:Landroid/view/View;

.field private successTitle:Lcom/squareup/marketfont/MarketTextView;

.field private successView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 24
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/timecards/TimecardsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method

.method public static getClockedInSummary(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_1

    .line 66
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 69
    :cond_0
    sget v0, Lcom/squareup/ui/timecards/R$string;->timecard_clocked_in_summary_job:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    const-string v0, "job_title"

    .line 70
    invoke-virtual {p0, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 71
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 67
    :cond_1
    :goto_0
    sget p1, Lcom/squareup/ui/timecards/R$string;->timecard_clocked_in_summary:I

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 28
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->attach(Landroid/view/View;)V

    .line 30
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$ClockInCoordinator$IML1Et1gAozwGpsjwLds9ncsg_k;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$ClockInCoordinator$IML1Et1gAozwGpsjwLds9ncsg_k;-><init>(Lcom/squareup/ui/timecards/ClockInCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected bindViews(Landroid/view/View;)V
    .locals 2

    .line 75
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 76
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInCoordinator;->successView:Landroid/view/View;

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInCoordinator;->successView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 78
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInCoordinator;->successContainer:Landroid/view/View;

    .line 79
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_success_title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockInCoordinator;->successTitle:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method protected getErrorMessage()Ljava/lang/String;
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorTitle()Ljava/lang/String;
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getProgressBarTitle()Ljava/lang/String;
    .locals 2

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_clock_in_progress_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSuccessViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SUCCESS_CLOCK_IN:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public synthetic lambda$attach$0$ClockInCoordinator()Lrx/Subscription;
    .locals 3

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->clockInScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 31
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$APhJSWWJ8MlFx8ESxY9T1sSdjyE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$APhJSWWJ8MlFx8ESxY9T1sSdjyE;-><init>(Lcom/squareup/ui/timecards/ClockInCoordinator;)V

    .line 32
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$1o0jp4f3nZaz7TJxrJrIc4doksk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$1o0jp4f3nZaz7TJxrJrIc4doksk;-><init>(Lcom/squareup/ui/timecards/ClockInCoordinator;)V

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$doRKuiq7Zc5FwgiCuQOZsyeHmhY;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$doRKuiq7Zc5FwgiCuQOZsyeHmhY;-><init>(Lcom/squareup/ui/timecards/ClockInCoordinator;)V

    .line 33
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method protected showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 4

    .line 53
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    .line 54
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/timecards/ClockInScreen$Data;

    .line 55
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInCoordinator;->timecardsActionBar:Lcom/squareup/ui/timecards/TimecardsActionBarView;

    iget-object v2, p0, Lcom/squareup/ui/timecards/ClockInCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/timecards/-$$Lambda$7XwxV_VzV14tlOleK0NWbjzouFM;

    invoke-direct {v3, v2}, Lcom/squareup/ui/timecards/-$$Lambda$7XwxV_VzV14tlOleK0NWbjzouFM;-><init>(Lcom/squareup/ui/timecards/TimecardsScopeRunner;)V

    invoke-virtual {v1, v3}, Lcom/squareup/ui/timecards/TimecardsActionBarView;->updateConfigForSuccessScreen(Ljava/lang/Runnable;)V

    .line 56
    iget-object v1, p0, Lcom/squareup/ui/timecards/ClockInCoordinator;->successTitle:Lcom/squareup/marketfont/MarketTextView;

    iget-object v2, p0, Lcom/squareup/ui/timecards/ClockInCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v3, v0, Lcom/squareup/ui/timecards/ClockInScreen$Data;->jobTitle:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/squareup/ui/timecards/ClockInCoordinator;->getClockedInSummary(Lcom/squareup/util/Res;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    sget v1, Lcom/squareup/ui/timecards/R$id;->success_notes_button:I

    invoke-virtual {p0, p1, v1}, Lcom/squareup/ui/timecards/ClockInCoordinator;->showOrHideNotesButton(Lcom/squareup/ui/timecards/TimecardsScreenData;I)V

    .line 60
    iget-object p1, v0, Lcom/squareup/ui/timecards/ClockInScreen$Data;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 61
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInCoordinator;->successContainer:Landroid/view/View;

    sget-object v0, Lcom/squareup/ui/timecards/ClockInCoordinator;->MOBILE_LAYOUT_PARAMS:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method
