.class final Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$2;
.super Ljava/lang/Object;
.source "CreateAccountHelper.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/CreateAccountHelper;->postCreateLogin(Ljava/lang/String;Ljava/lang/String;Z)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;",
        "kotlin.jvm.PlatformType",
        "receivedResponse",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "Lcom/squareup/protos/register/api/LoginResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $existingMerchant:Z

.field final synthetic this$0:Lcom/squareup/ui/login/CreateAccountHelper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/CreateAccountHelper;Z)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$2;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    iput-boolean p2, p0, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$2;->$existingMerchant:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/ReceivedResponse;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "Lcom/squareup/protos/register/api/LoginResponse;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "receivedResponse"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 161
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    if-nez v0, :cond_0

    .line 163
    new-instance p1, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureMessage;

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$2;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-static {v0}, Lcom/squareup/ui/login/CreateAccountHelper;->access$getRes$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/loggedout/R$string;->duplicate_account_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 165
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$2;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-static {v1}, Lcom/squareup/ui/login/CreateAccountHelper;->access$getRes$p(Lcom/squareup/ui/login/CreateAccountHelper;)Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/loggedout/R$string;->duplicate_account_message:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 163
    invoke-direct {p1, v0, v1}, Lcom/squareup/ui/login/CreateAccountHelper$NextStepResponse$FailureMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 170
    :cond_0
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;

    invoke-virtual {p1}, Lcom/squareup/receiving/ReceivedResponse$Okay$Accepted;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/register/api/LoginResponse;

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$2;->this$0:Lcom/squareup/ui/login/CreateAccountHelper;

    iget-object p1, p1, Lcom/squareup/protos/register/api/LoginResponse;->session_token:Ljava/lang/String;

    const-string v1, "loginResponse.session_token"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$2;->$existingMerchant:Z

    invoke-static {v0, p1, v1}, Lcom/squareup/ui/login/CreateAccountHelper;->access$finalizeLogIn(Lcom/squareup/ui/login/CreateAccountHelper;Ljava/lang/String;Z)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/receiving/ReceivedResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/CreateAccountHelper$postCreateLogin$2;->apply(Lcom/squareup/receiving/ReceivedResponse;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
