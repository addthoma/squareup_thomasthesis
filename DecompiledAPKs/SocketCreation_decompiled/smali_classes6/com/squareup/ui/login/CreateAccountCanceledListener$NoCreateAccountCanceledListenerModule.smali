.class public abstract Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule;
.super Ljava/lang/Object;
.source "CreateAccountCanceledListener.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/CreateAccountCanceledListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "NoCreateAccountCanceledListenerModule"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0008\'\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"
    }
    d2 = {
        "Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule;",
        "",
        "()V",
        "Companion",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule;->Companion:Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final provideCreateAccountCanceledListener()Lcom/squareup/ui/login/CreateAccountCanceledListener;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule;->Companion:Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountCanceledListener$NoCreateAccountCanceledListenerModule$Companion;->provideCreateAccountCanceledListener()Lcom/squareup/ui/login/CreateAccountCanceledListener;

    move-result-object v0

    return-object v0
.end method
