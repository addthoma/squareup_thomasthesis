.class public final Lcom/squareup/ui/login/EnrollSmsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EnrollSmsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEnrollSmsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EnrollSmsCoordinator.kt\ncom/squareup/ui/login/EnrollSmsCoordinator\n+ 2 Strings.kt\nkotlin/text/StringsKt__StringsKt\n*L\n1#1,142:1\n17#2,22:143\n*E\n*S KotlinDebug\n*F\n+ 1 EnrollSmsCoordinator.kt\ncom/squareup/ui/login/EnrollSmsCoordinator\n*L\n129#1,22:143\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0001$B/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0018\u0010\u0006\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u00080\u0007\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0010\u0010!\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0008\u0010\"\u001a\u00020\u001eH\u0002J\u0006\u0010#\u001a\u00020\u001eR\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\u00020\u00118BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u00148BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0016R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0006\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006%"
    }
    d2 = {
        "Lcom/squareup/ui/login/EnrollSmsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "scrubber",
        "Lcom/squareup/text/InsertingScrubber;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lcom/squareup/util/Res;Lcom/squareup/text/InsertingScrubber;Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "hint",
        "Lcom/squareup/widgets/MessageView;",
        "isPhoneNumberValid",
        "",
        "()Z",
        "phoneNumber",
        "",
        "getPhoneNumber",
        "()Ljava/lang/CharSequence;",
        "phoneNumberField",
        "Lcom/squareup/ui/XableEditText;",
        "skipEnrollmentLink",
        "Landroid/widget/TextView;",
        "subtitle",
        "title",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "indicatePhoneNumberError",
        "updateActionState",
        "Factory",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private hint:Lcom/squareup/widgets/MessageView;

.field private phoneNumberField:Lcom/squareup/ui/XableEditText;

.field private final res:Lcom/squareup/util/Res;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private scrubber:Lcom/squareup/text/InsertingScrubber;

.field private skipEnrollmentLink:Landroid/widget/TextView;

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/InsertingScrubber;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/InsertingScrubber;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollSms;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scrubber"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenData"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->scrubber:Lcom/squareup/text/InsertingScrubber;

    iput-object p3, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->screenData:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 35
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getPhoneNumber$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;)Ljava/lang/CharSequence;
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->getPhoneNumber()Ljava/lang/CharSequence;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getPhoneNumberField$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;)Lcom/squareup/ui/XableEditText;
    .locals 1

    .line 35
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->phoneNumberField:Lcom/squareup/ui/XableEditText;

    if-nez p0, :cond_0

    const-string v0, "phoneNumberField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 35
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getSkipEnrollmentLink$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;)Landroid/widget/TextView;
    .locals 1

    .line 35
    iget-object p0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->skipEnrollmentLink:Landroid/widget/TextView;

    if-nez p0, :cond_0

    const-string v0, "skipEnrollmentLink"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$indicatePhoneNumberError(Lcom/squareup/ui/login/EnrollSmsCoordinator;)V
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->indicatePhoneNumberError()V

    return-void
.end method

.method public static final synthetic access$isPhoneNumberValid$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;)Z
    .locals 0

    .line 35
    invoke-direct {p0}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->isPhoneNumberValid()Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-void
.end method

.method public static final synthetic access$setPhoneNumberField$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;Lcom/squareup/ui/XableEditText;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->phoneNumberField:Lcom/squareup/ui/XableEditText;

    return-void
.end method

.method public static final synthetic access$setSkipEnrollmentLink$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;Landroid/widget/TextView;)V
    .locals 0

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->skipEnrollmentLink:Landroid/widget/TextView;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 122
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 123
    sget v0, Lcom/squareup/marin/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 124
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->mobile_phone_number_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->phoneNumberField:Lcom/squareup/ui/XableEditText;

    .line 125
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->hint:Lcom/squareup/widgets/MessageView;

    .line 126
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->skip_enrollment_link:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->skipEnrollmentLink:Landroid/widget/TextView;

    return-void
.end method

.method private final getPhoneNumber()Ljava/lang/CharSequence;
    .locals 8

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->phoneNumberField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "phoneNumberField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_7

    check-cast v0, Ljava/lang/CharSequence;

    .line 144
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    const/4 v3, 0x0

    move v4, v1

    const/4 v1, 0x0

    const/4 v5, 0x0

    :goto_0
    if-gt v1, v4, :cond_6

    if-nez v5, :cond_1

    move v6, v1

    goto :goto_1

    :cond_1
    move v6, v4

    .line 149
    :goto_1
    invoke-interface {v0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    const/16 v7, 0x20

    if-gt v6, v7, :cond_2

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    if-nez v5, :cond_4

    if-nez v6, :cond_3

    const/4 v5, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    if-nez v6, :cond_5

    goto :goto_3

    :cond_5
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    :cond_6
    :goto_3
    add-int/2addr v4, v2

    .line 164
    invoke-interface {v0, v1, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_7

    goto :goto_4

    :cond_7
    const-string v0, ""

    .line 129
    check-cast v0, Ljava/lang/CharSequence;

    :goto_4
    return-object v0
.end method

.method private final indicatePhoneNumberError()V
    .locals 4

    .line 137
    new-instance v0, Lcom/squareup/register/widgets/validation/ShakeAnimation;

    invoke-direct {v0}, Lcom/squareup/register/widgets/validation/ShakeAnimation;-><init>()V

    .line 138
    new-instance v1, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;

    iget-object v2, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->phoneNumberField:Lcom/squareup/ui/XableEditText;

    const-string v3, "phoneNumberField"

    if-nez v2, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v2}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;-><init>(Landroid/widget/TextView;)V

    check-cast v1, Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/validation/ShakeAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 139
    iget-object v1, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->phoneNumberField:Lcom/squareup/ui/XableEditText;

    if-nez v1, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/animation/Animation;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/XableEditText;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private final isPhoneNumberValid()Z
    .locals 1

    .line 130
    invoke-direct {p0}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->getPhoneNumber()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->bindViews(Landroid/view/View;)V

    .line 59
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->screenData:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;-><init>(Lcom/squareup/ui/login/EnrollSmsCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 100
    invoke-virtual {p0}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->updateActionState()V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "title"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_enroll_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1

    const-string v1, "subtitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_enroll_subtitle:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->hint:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_2

    const-string v1, "hint"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 104
    :cond_2
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 105
    sget p1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_enroll_sms_hint:I

    const-string v2, "learn_more"

    invoke-virtual {v1, p1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 106
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_auth_url:I

    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 107
    sget v1, Lcom/squareup/common/strings/R$string;->learn_more:I

    invoke-virtual {p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 108
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object p1, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->phoneNumberField:Lcom/squareup/ui/XableEditText;

    const-string v0, "phoneNumberField"

    if-nez p1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v1, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$2;

    .line 111
    iget-object v2, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->scrubber:Lcom/squareup/text/InsertingScrubber;

    iget-object v3, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->phoneNumberField:Lcom/squareup/ui/XableEditText;

    if-nez v3, :cond_4

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v3, Lcom/squareup/text/HasSelectableText;

    invoke-direct {v1, p0, v2, v3}, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$2;-><init>(Lcom/squareup/ui/login/EnrollSmsCoordinator;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    check-cast v1, Landroid/text/TextWatcher;

    .line 110
    invoke-virtual {p1, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->phoneNumberField:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_5

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    return-void
.end method

.method public final updateActionState()V
    .locals 2

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->isPhoneNumberValid()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
