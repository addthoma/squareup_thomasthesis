.class final Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;
.super Lkotlin/jvm/internal/Lambda;
.source "TwoFactorDetailsPickerCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTwoFactorDetailsPickerCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TwoFactorDetailsPickerCoordinator.kt\ncom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,94:1\n1103#2,7:95\n1103#2,7:102\n*E\n*S KotlinDebug\n*F\n+ 1 TwoFactorDetailsPickerCoordinator.kt\ncom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1\n*L\n51#1,7:95\n56#1,7:102\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 24
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component2()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 42
    iget-object v1, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;->$view:Landroid/view/View;

    new-instance v2, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {v1, v2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 44
    iget-object v1, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;

    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;->getMethods()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->access$displayTwoFactorDetailsMethods(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;Ljava/util/List;)V

    .line 51
    iget-object v1, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->access$getGoogleAuthButton$p(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;)Lcom/squareup/marketfont/MarketButton;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 95
    new-instance v2, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$$special$$inlined$onClickDebounced$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$$special$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iget-object v1, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->access$getSmsButton$p(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;)Lcom/squareup/marketfont/MarketButton;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 102
    new-instance v2, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$$special$$inlined$onClickDebounced$2;

    invoke-direct {v2, p1}, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$$special$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iget-object v1, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;->access$getActionBar$p(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    const-string v2, "actionBar.presenter"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 60
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v4, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/squareup/common/authenticatorviews/R$string;->back:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 61
    new-instance v3, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$$special$$inlined$apply$lambda$1;

    invoke-direct {v3, p0, p1, v0}, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$$special$$inlined$apply$lambda$1;-><init>(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 63
    invoke-virtual {v0}, Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;->getCanSkipEnroll()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 64
    iget-object v3, p0, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_enroll_skip:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 65
    new-instance v3, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$$special$$inlined$apply$lambda$2;

    invoke-direct {v3, p0, p1, v0}, Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1$$special$$inlined$apply$lambda$2;-><init>(Lcom/squareup/ui/login/TwoFactorDetailsPickerCoordinator$attach$1;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/ui/login/AuthenticatorScreen$PickTwoFactorMethod;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 69
    :cond_0
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
