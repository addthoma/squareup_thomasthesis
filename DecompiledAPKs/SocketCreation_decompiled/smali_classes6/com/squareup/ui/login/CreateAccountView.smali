.class public Lcom/squareup/ui/login/CreateAccountView;
.super Landroid/widget/LinearLayout;
.source "CreateAccountView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private acceptLegal:Lcom/squareup/noho/NohoCheckableRow;

.field private acceptLegalSection:Lcom/squareup/noho/NohoCheckableGroup;

.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private confirmEmail:Lcom/squareup/ui/XableEditText;

.field private final confirmationPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

.field private countryPicker:Landroid/widget/TextView;

.field private final countryPickerPopup:Lcom/squareup/ui/login/CountryPickerPopup;

.field private email:Lcom/squareup/ui/XableEditText;

.field private emailSuggestionHandler:Lcom/squareup/widgets/EmailSuggestionHandler;

.field private legal:Lcom/squareup/widgets/MessageView;

.field private password:Lcom/squareup/ui/XableEditText;

.field presenter:Lcom/squareup/ui/login/CreateAccountPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final serverCallView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;

.field supportedCountriesProvider:Lcom/squareup/loggedout/SupportedCountriesProvider;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    const-class p2, Lcom/squareup/ui/login/CreateAccountScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/login/CreateAccountScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/login/CreateAccountScreen$Component;->inject(Lcom/squareup/ui/login/CreateAccountView;)V

    .line 64
    new-instance p2, Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/ConfirmationPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/login/CreateAccountView;->confirmationPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    .line 65
    new-instance p2, Lcom/squareup/ui/login/CountryPickerPopup;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->supportedCountriesProvider:Lcom/squareup/loggedout/SupportedCountriesProvider;

    .line 66
    invoke-interface {v0}, Lcom/squareup/loggedout/SupportedCountriesProvider;->getSupportedCountries()[Lcom/squareup/CountryCode;

    move-result-object v0

    invoke-direct {p2, p1, v0}, Lcom/squareup/ui/login/CountryPickerPopup;-><init>(Landroid/content/Context;[Lcom/squareup/CountryCode;)V

    iput-object p2, p0, Lcom/squareup/ui/login/CreateAccountView;->countryPickerPopup:Lcom/squareup/ui/login/CountryPickerPopup;

    .line 67
    new-instance p2, Lcom/squareup/caller/ProgressAndFailureView;

    invoke-direct {p2, p1}, Lcom/squareup/caller/ProgressAndFailureView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/login/CreateAccountView;->serverCallView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    return-void
.end method

.method static synthetic lambda$onFinishInflate$0(Landroid/view/View;Z)V
    .locals 0

    .line 113
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method static synthetic lambda$onFinishInflate$1(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .line 115
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->hasFocus()Z

    move-result p1

    if-nez p1, :cond_0

    .line 116
    invoke-virtual {p0}, Landroid/view/View;->requestFocus()Z

    .line 117
    invoke-virtual {p0}, Landroid/view/View;->performClick()Z

    return v0

    :cond_0
    const/4 p0, 0x0

    return p0
.end method


# virtual methods
.method enableWorld(Z)V
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->emailSuggestionHandler:Lcom/squareup/widgets/EmailSuggestionHandler;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/EmailSuggestionHandler;->enableWorld(Z)V

    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-object v0
.end method

.method getConfirmEmail()Ljava/lang/String;
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->confirmEmail:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getEmail()Ljava/lang/String;
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->email:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getPassword()Ljava/lang/String;
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->password:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method isConfirmEmailGone()Z
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->confirmEmail:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method isTermsOfServiceAccepted()Z
    .locals 1

    .line 196
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->acceptLegalSection:Lcom/squareup/noho/NohoCheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoCheckableGroup;->isChecked()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onFinishInflate$2$CreateAccountView(Lcom/squareup/noho/NohoCheckableGroup;II)V
    .locals 0

    .line 132
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountView;->presenter:Lcom/squareup/ui/login/CreateAccountPresenter;

    iget-object p2, p0, Lcom/squareup/ui/login/CreateAccountView;->acceptLegal:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p2}, Lcom/squareup/noho/NohoCheckableRow;->isChecked()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/login/CreateAccountPresenter;->onTermsOfServiceAcceptedChanged(Z)V

    return-void
.end method

.method public synthetic lambda$requestInitialFocus$3$CreateAccountView()V
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->email:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;Z)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 157
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    const/4 v0, 0x0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->presenter:Lcom/squareup/ui/login/CreateAccountPresenter;

    iget-object v0, v0, Lcom/squareup/ui/login/CreateAccountPresenter;->helper:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountHelper;->getProgressPresenter()Lcom/squareup/caller/ProgressAndFailurePresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountView;->serverCallView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    invoke-virtual {v0, v1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->dropView(Lcom/squareup/caller/ProgressAndFailurePresenter$View;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->presenter:Lcom/squareup/ui/login/CreateAccountPresenter;

    iget-object v0, v0, Lcom/squareup/ui/login/CreateAccountPresenter;->countryPickerPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountView;->countryPickerPopup:Lcom/squareup/ui/login/CountryPickerPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->presenter:Lcom/squareup/ui/login/CreateAccountPresenter;

    iget-object v0, v0, Lcom/squareup/ui/login/CreateAccountPresenter;->confirmationPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountView;->confirmationPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->presenter:Lcom/squareup/ui/login/CreateAccountPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->dropView(Ljava/lang/Object;)V

    .line 153
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 71
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 73
    sget v0, Lcom/squareup/loggedout/R$id;->email:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->email:Lcom/squareup/ui/XableEditText;

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->email:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->disableActionsExceptPaste()V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->email:Lcom/squareup/ui/XableEditText;

    invoke-static {v0}, Lcom/squareup/text/EmailScrubber;->watcher(Lcom/squareup/text/HasSelectableText;)Landroid/text/TextWatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->email:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/login/CreateAccountView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/CreateAccountView$1;-><init>(Lcom/squareup/ui/login/CreateAccountView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->email:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/login/CreateAccountView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/CreateAccountView$2;-><init>(Lcom/squareup/ui/login/CreateAccountView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 87
    sget v0, Lcom/squareup/loggedout/R$id;->email_suggestion_box:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 88
    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountView;->email:Lcom/squareup/ui/XableEditText;

    invoke-static {v1, v0, p0}, Lcom/squareup/widgets/EmailSuggestionHandler;->wireEmailSuggestions(Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;Landroid/widget/TextView;Landroid/view/View;)Lcom/squareup/widgets/EmailSuggestionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->emailSuggestionHandler:Lcom/squareup/widgets/EmailSuggestionHandler;

    .line 89
    sget v0, Lcom/squareup/loggedout/R$id;->confirm_email:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->confirmEmail:Lcom/squareup/ui/XableEditText;

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->confirmEmail:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->disableActionsExceptPaste()V

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->confirmEmail:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/login/CreateAccountView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/CreateAccountView$3;-><init>(Lcom/squareup/ui/login/CreateAccountView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 96
    sget v0, Lcom/squareup/loggedout/R$id;->password:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->password:Lcom/squareup/ui/XableEditText;

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->password:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/login/CreateAccountView$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/CreateAccountView$4;-><init>(Lcom/squareup/ui/login/CreateAccountView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 103
    sget v0, Lcom/squareup/loggedout/R$id;->create_account_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 104
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 106
    sget v0, Lcom/squareup/loggedout/R$id;->country_picker:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->countryPicker:Landroid/widget/TextView;

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->countryPicker:Landroid/widget/TextView;

    sget v1, Lcom/squareup/loggedout/R$string;->postal_country_prompt:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(I)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->countryPicker:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/ui/login/CreateAccountView$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/CreateAccountView$5;-><init>(Lcom/squareup/ui/login/CreateAccountView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->countryPicker:Landroid/widget/TextView;

    sget-object v1, Lcom/squareup/ui/login/-$$Lambda$CreateAccountView$Jc44PPAe21JywVqPaRVN4ztKlpY;->INSTANCE:Lcom/squareup/ui/login/-$$Lambda$CreateAccountView$Jc44PPAe21JywVqPaRVN4ztKlpY;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->countryPicker:Landroid/widget/TextView;

    sget-object v1, Lcom/squareup/ui/login/-$$Lambda$CreateAccountView$4qMrmeyK6CWDXygTKAdmbulBAXM;->INSTANCE:Lcom/squareup/ui/login/-$$Lambda$CreateAccountView$4qMrmeyK6CWDXygTKAdmbulBAXM;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->email:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountView;->confirmEmail:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setNextFocusView(Landroid/view/View;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->confirmEmail:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountView;->password:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setNextFocusView(Landroid/view/View;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->password:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountView;->countryPicker:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setNextFocusView(Landroid/view/View;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->email:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    .line 129
    sget v0, Lcom/squareup/loggedout/R$id;->accept_legal:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->acceptLegal:Lcom/squareup/noho/NohoCheckableRow;

    .line 130
    sget v0, Lcom/squareup/loggedout/R$id;->accept_legal_parent:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableGroup;

    iput-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->acceptLegalSection:Lcom/squareup/noho/NohoCheckableGroup;

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->acceptLegalSection:Lcom/squareup/noho/NohoCheckableGroup;

    new-instance v1, Lcom/squareup/ui/login/-$$Lambda$CreateAccountView$6JWdQIK7KW-pZmsCE0eK9y-yfzg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/-$$Lambda$CreateAccountView$6JWdQIK7KW-pZmsCE0eK9y-yfzg;-><init>(Lcom/squareup/ui/login/CreateAccountView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;)V

    .line 134
    sget v0, Lcom/squareup/loggedout/R$id;->legal:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->legal:Lcom/squareup/widgets/MessageView;

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->legal:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/login/CreateAccountView$6;

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/CreateAccountView$6;-><init>(Lcom/squareup/ui/login/CreateAccountView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->presenter:Lcom/squareup/ui/login/CreateAccountPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/login/CreateAccountPresenter;->takeView(Ljava/lang/Object;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->presenter:Lcom/squareup/ui/login/CreateAccountPresenter;

    iget-object v0, v0, Lcom/squareup/ui/login/CreateAccountPresenter;->confirmationPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountView;->confirmationPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->presenter:Lcom/squareup/ui/login/CreateAccountPresenter;

    iget-object v0, v0, Lcom/squareup/ui/login/CreateAccountPresenter;->countryPickerPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountView;->countryPickerPopup:Lcom/squareup/ui/login/CountryPickerPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->presenter:Lcom/squareup/ui/login/CreateAccountPresenter;

    iget-object v0, v0, Lcom/squareup/ui/login/CreateAccountPresenter;->helper:Lcom/squareup/ui/login/CreateAccountHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/login/CreateAccountHelper;->getProgressPresenter()Lcom/squareup/caller/ProgressAndFailurePresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountView;->serverCallView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    invoke-virtual {v0, v1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method requestConfirmEmailFocus()V
    .locals 1

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->confirmEmail:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    return-void
.end method

.method requestEmailFocus()V
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->email:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    return-void
.end method

.method requestInitialFocus()V
    .locals 2

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->email:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->email:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/login/-$$Lambda$CreateAccountView$XnIrxh-92xVZFNuBXMZ9RAyDQvg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/-$$Lambda$CreateAccountView$XnIrxh-92xVZFNuBXMZ9RAyDQvg;-><init>(Lcom/squareup/ui/login/CreateAccountView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method requestPasswordFocus()V
    .locals 1

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->password:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    return-void
.end method

.method setSelectedCountry(Lcom/squareup/CountryCode;)V
    .locals 3

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->countryPicker:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/ui/login/CreateAccountView;->res:Lcom/squareup/util/Res;

    invoke-static {p1}, Lcom/squareup/address/CountryResources;->countryName(Lcom/squareup/CountryCode;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->countryPicker:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/address/CountryResources;->flagId(Lcom/squareup/CountryCode;)I

    move-result p1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    return-void
.end method

.method setTermsOfServiceAcceptText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->acceptLegal:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTermsOfServiceAccepted(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 189
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountView;->acceptLegalSection:Lcom/squareup/noho/NohoCheckableGroup;

    sget v0, Lcom/squareup/loggedout/R$id;->accept_legal:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableGroup;->check(I)V

    goto :goto_0

    .line 191
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountView;->acceptLegalSection:Lcom/squareup/noho/NohoCheckableGroup;

    sget v0, Lcom/squareup/loggedout/R$id;->accept_legal:I

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableGroup;->uncheck(I)V

    :goto_0
    return-void
.end method

.method setTermsOfServiceHelperText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->legal:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showConfirmEmail()V
    .locals 2

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->confirmEmail:Lcom/squareup/ui/XableEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setVisibility(I)V

    return-void
.end method

.method useLocalEmailAddress(Ljava/lang/CharSequence;)V
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountView;->email:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 226
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountView;->confirmEmail:Lcom/squareup/ui/XableEditText;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/ui/XableEditText;->setVisibility(I)V

    .line 228
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountView;->password:Lcom/squareup/ui/XableEditText;

    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->getVisibility()I

    move-result p1

    if-nez p1, :cond_0

    .line 229
    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountView;->password:Lcom/squareup/ui/XableEditText;

    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    :cond_0
    return-void
.end method
