.class final Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$6$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAuthenticator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$6;->apply(Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;",
        "Lcom/squareup/ui/login/RealAuthenticator$TokenAndDeviceDetails;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/login/RealAuthenticator$TokenAndDeviceDetails;",
        "it",
        "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$6$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$6$1;

    invoke-direct {v0}, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$6$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$6$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$6$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;)Lcom/squareup/ui/login/RealAuthenticator$TokenAndDeviceDetails;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 424
    new-instance v0, Lcom/squareup/ui/login/RealAuthenticator$TokenAndDeviceDetails;

    iget-object v1, p1, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;->session_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    invoke-direct {v0, v1, p1}, Lcom/squareup/ui/login/RealAuthenticator$TokenAndDeviceDetails;-><init>(Ljava/lang/String;Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$6$1;->invoke(Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;)Lcom/squareup/ui/login/RealAuthenticator$TokenAndDeviceDetails;

    move-result-object p1

    return-object p1
.end method
