.class final synthetic Lcom/squareup/ui/login/RealAuthenticator$render$1;
.super Lkotlin/jvm/internal/FunctionReference;
.source "RealAuthenticator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticator;->render(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/ui/login/AuthenticatorRendering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/account/AccountEvents$SessionExpired;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "+",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u00042\u0015\u0010\u0005\u001a\u00110\u0006\u00a2\u0006\u000c\u0008\u0007\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\t\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        "Lcom/squareup/ui/login/AuthUpdate;",
        "p1",
        "Lcom/squareup/account/AccountEvents$SessionExpired;",
        "Lkotlin/ParameterName;",
        "name",
        "event",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/login/RealAuthenticator$render$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/login/RealAuthenticator$render$1;

    invoke-direct {v0}, Lcom/squareup/ui/login/RealAuthenticator$render$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/login/RealAuthenticator$render$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticator$render$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/FunctionReference;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "handleSessionExpired"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 2

    const-class v0, Lcom/squareup/ui/login/RealAuthenticatorKt;

    const-string v1, "impl_release"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinPackage(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/reflect/KDeclarationContainer;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "handleSessionExpired(Lcom/squareup/account/AccountEvents$SessionExpired;)Lcom/squareup/workflow/WorkflowAction;"

    return-object v0
.end method

.method public final invoke(Lcom/squareup/account/AccountEvents$SessionExpired;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/account/AccountEvents$SessionExpired;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    invoke-static {p1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->access$handleSessionExpired(Lcom/squareup/account/AccountEvents$SessionExpired;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/account/AccountEvents$SessionExpired;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator$render$1;->invoke(Lcom/squareup/account/AccountEvents$SessionExpired;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
