.class final Lcom/squareup/ui/login/RealAuthenticator$deviceCodeLoginCallback$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAuthenticator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticator;->deviceCodeLoginCallback$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/register/api/LoginRequest;Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "it",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $response:Lcom/squareup/ui/login/AuthenticationCallResult;

.field final synthetic $this_deviceCodeLoginCallback:Lcom/squareup/ui/login/AuthenticatorState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticationCallResult;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator$deviceCodeLoginCallback$2;->$this_deviceCodeLoginCallback:Lcom/squareup/ui/login/AuthenticatorState;

    iput-object p2, p0, Lcom/squareup/ui/login/RealAuthenticator$deviceCodeLoginCallback$2;->$response:Lcom/squareup/ui/login/AuthenticationCallResult;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;
    .locals 7

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 943
    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator$deviceCodeLoginCallback$2;->$this_deviceCodeLoginCallback:Lcom/squareup/ui/login/AuthenticatorState;

    iget-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator$deviceCodeLoginCallback$2;->$response:Lcom/squareup/ui/login/AuthenticationCallResult;

    check-cast p1, Lcom/squareup/ui/login/AuthenticationCallResult$Success;

    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticationCallResult$Success;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/register/api/LoginResponse;

    iget-object v2, p1, Lcom/squareup/protos/register/api/LoginResponse;->session_token:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/login/RealAuthenticatorKt;->withTemporarySessionData$default(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Lcom/squareup/ui/login/UnitsAndMerchants;Ljava/lang/Boolean;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/ui/login/AuthenticatorState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator$deviceCodeLoginCallback$2;->invoke(Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    return-object p1
.end method
