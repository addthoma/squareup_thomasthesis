.class public final Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "DeviceCodeLoginCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDeviceCodeLoginCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DeviceCodeLoginCoordinator.kt\ncom/squareup/ui/login/DeviceCodeLoginCoordinator\n*L\n1#1,122:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000U\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004*\u0001\u000f\u0018\u00002\u00020\u0001B\u001f\u0012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0010\u0010!\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0008\u0010\"\u001a\u00020\u001eH\u0002J\u0006\u0010#\u001a\u00020\u001eR\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u00020\u000b8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\rR\u0010\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0010R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R \u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0015\u001a\u00020\u00168BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0019\u0010\u001a\u001a\u0004\u0008\u0017\u0010\u0018R\u000e\u0010\u001b\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "deviceCode",
        "",
        "getDeviceCode",
        "()Ljava/lang/String;",
        "deviceCodeChangeListener",
        "com/squareup/ui/login/DeviceCodeLoginCoordinator$deviceCodeChangeListener$1",
        "Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$deviceCodeChangeListener$1;",
        "deviceCodeField",
        "Lcom/squareup/ui/XableEditText;",
        "hint",
        "Lcom/squareup/widgets/MessageView;",
        "shakeAnimation",
        "Lcom/squareup/register/widgets/validation/ShakeAnimation;",
        "getShakeAnimation",
        "()Lcom/squareup/register/widgets/validation/ShakeAnimation;",
        "shakeAnimation$delegate",
        "Lkotlin/Lazy;",
        "subtitle",
        "title",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "indicateError",
        "updateEnabledState",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final deviceCodeChangeListener:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$deviceCodeChangeListener$1;

.field private deviceCodeField:Lcom/squareup/ui/XableEditText;

.field private hint:Lcom/squareup/widgets/MessageView;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private final shakeAnimation$delegate:Lkotlin/Lazy;

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$DeviceCode;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->screenData:Lio/reactivex/Observable;

    .line 40
    sget-object p1, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$shakeAnimation$2;->INSTANCE:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$shakeAnimation$2;

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->shakeAnimation$delegate:Lkotlin/Lazy;

    .line 44
    new-instance p1, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$deviceCodeChangeListener$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$deviceCodeChangeListener$1;-><init>(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;)V

    iput-object p1, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->deviceCodeChangeListener:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$deviceCodeChangeListener$1;

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getDeviceCode$p(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;)Ljava/lang/String;
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->getDeviceCode()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getDeviceCodeField$p(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;)Lcom/squareup/ui/XableEditText;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->deviceCodeField:Lcom/squareup/ui/XableEditText;

    if-nez p0, :cond_0

    const-string v0, "deviceCodeField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$indicateError(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->indicateError()V

    return-void
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-void
.end method

.method public static final synthetic access$setDeviceCodeField$p(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;Lcom/squareup/ui/XableEditText;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->deviceCodeField:Lcom/squareup/ui/XableEditText;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 114
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "view.findById<ActionBarV\u2026n_bar)\n        .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 116
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 117
    sget v0, Lcom/squareup/marin/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 118
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->device_code_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->deviceCodeField:Lcom/squareup/ui/XableEditText;

    .line 119
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->hint:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final getDeviceCode()Ljava/lang/String;
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->deviceCodeField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_0

    const-string v1, "deviceCodeField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private final getShakeAnimation()Lcom/squareup/register/widgets/validation/ShakeAnimation;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->shakeAnimation$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/validation/ShakeAnimation;

    return-object v0
.end method

.method private final indicateError()V
    .locals 4

    .line 108
    invoke-direct {p0}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->getShakeAnimation()Lcom/squareup/register/widgets/validation/ShakeAnimation;

    move-result-object v0

    new-instance v1, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;

    iget-object v2, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->deviceCodeField:Lcom/squareup/ui/XableEditText;

    const-string v3, "deviceCodeField"

    if-nez v2, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v2}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/validation/ShakeAnimationListener;-><init>(Landroid/widget/TextView;)V

    check-cast v1, Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/validation/ShakeAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->deviceCodeField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->getShakeAnimation()Lcom/squareup/register/widgets/validation/ShakeAnimation;

    move-result-object v1

    check-cast v1, Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->startAnimation(Landroid/view/animation/Animation;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->deviceCodeField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 6

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 53
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->bindViews(Landroid/view/View;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "title"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->device_code_sign_in_to_square:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1

    const-string v1, "subtitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->deviceCodeField:Lcom/squareup/ui/XableEditText;

    const-string v1, "deviceCodeField"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    const-string v2, "deviceCodeField.editText"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/text/InputFilter;

    const/4 v3, 0x0

    new-instance v4, Landroid/text/InputFilter$AllCaps;

    invoke-direct {v4}, Landroid/text/InputFilter$AllCaps;-><init>()V

    check-cast v4, Landroid/text/InputFilter;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Landroid/text/InputFilter$LengthFilter;

    invoke-static {}, Lcom/squareup/ui/login/AuthenticatorKt;->getDEVICE_CODE_MAX_LENGTH()I

    move-result v5

    invoke-direct {v4, v5}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    check-cast v4, Landroid/text/InputFilter;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/SelectableEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->deviceCodeField:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->deviceCodeChangeListener:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$deviceCodeChangeListener$1;

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->hint:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_4

    const-string v1, "hint"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 63
    :cond_4
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 64
    sget v2, Lcom/squareup/common/authenticatorviews/R$string;->device_code_hint:I

    const-string v3, "learn_more"

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 65
    sget v2, Lcom/squareup/common/authenticatorviews/R$string;->device_code_url:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 66
    sget v2, Lcom/squareup/common/authenticatorviews/R$string;->device_code_hint_link_text:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 67
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "view.context"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->screenData:Lio/reactivex/Observable;

    new-instance v2, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;-><init>(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;Landroid/content/res/Resources;Landroid/view/View;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, p1, v2}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->screenData:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$2;->INSTANCE:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$2;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 99
    sget-object v1, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$3;->INSTANCE:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$3;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screenData.map { (screen\u2026      .filter { it != 0 }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    new-instance v1, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$4;-><init>(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->updateEnabledState()V

    return-void
.end method

.method public final updateEnabledState()V
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->getDeviceCode()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
