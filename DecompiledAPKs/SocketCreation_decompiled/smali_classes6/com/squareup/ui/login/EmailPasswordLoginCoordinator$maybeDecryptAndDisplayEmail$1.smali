.class final Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$maybeDecryptAndDisplayEmail$1;
.super Ljava/lang/Object;
.source "EmailPasswordLoginCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->maybeDecryptAndDisplayEmail(Lcom/squareup/ui/login/PostInstallEncryptedEmail;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/protos/client/multipass/DecryptEmailResponse;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "decryptEmailResponse",
        "Lcom/squareup/protos/client/multipass/DecryptEmailResponse;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$maybeDecryptAndDisplayEmail$1;->this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/protos/client/multipass/DecryptEmailResponse;)V
    .locals 2

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$maybeDecryptAndDisplayEmail$1;->this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    iget-object p1, p1, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->decrypted_email:Ljava/lang/String;

    const-string v1, "decryptEmailResponse.decrypted_email"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->access$setEmail$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;Ljava/lang/String;)V

    .line 236
    iget-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$maybeDecryptAndDisplayEmail$1;->this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->access$requestFocusOnPassword(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)Z

    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$maybeDecryptAndDisplayEmail$1;->call(Lcom/squareup/protos/client/multipass/DecryptEmailResponse;)V

    return-void
.end method
