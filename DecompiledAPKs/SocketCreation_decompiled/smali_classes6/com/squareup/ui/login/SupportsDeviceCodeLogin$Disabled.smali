.class public final Lcom/squareup/ui/login/SupportsDeviceCodeLogin$Disabled;
.super Ljava/lang/Object;
.source "SupportsDeviceCodeLogin.kt"

# interfaces
.implements Lcom/squareup/ui/login/SupportsDeviceCodeLogin;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/SupportsDeviceCodeLogin;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Disabled"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u00048VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/login/SupportsDeviceCodeLogin$Disabled;",
        "Lcom/squareup/ui/login/SupportsDeviceCodeLogin;",
        "()V",
        "enabled",
        "",
        "getEnabled",
        "()Z",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/login/SupportsDeviceCodeLogin$Disabled;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/squareup/ui/login/SupportsDeviceCodeLogin$Disabled;

    invoke-direct {v0}, Lcom/squareup/ui/login/SupportsDeviceCodeLogin$Disabled;-><init>()V

    sput-object v0, Lcom/squareup/ui/login/SupportsDeviceCodeLogin$Disabled;->INSTANCE:Lcom/squareup/ui/login/SupportsDeviceCodeLogin$Disabled;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
