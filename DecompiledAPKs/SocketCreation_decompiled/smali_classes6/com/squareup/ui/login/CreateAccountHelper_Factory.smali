.class public final Lcom/squareup/ui/login/CreateAccountHelper_Factory;
.super Ljava/lang/Object;
.source "CreateAccountHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/login/CreateAccountHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PersistentAccountService;",
            ">;"
        }
    .end annotation
.end field

.field private final accountStatusServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/accountstatus/AccountStatusService;",
            ">;"
        }
    .end annotation
.end field

.field private final adAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final authenticationServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/AuthenticationService;",
            ">;"
        }
    .end annotation
.end field

.field private final authenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final finalCreateAccountLogInCheckProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PersistentAccountService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/accountstatus/AccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/AuthenticationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;",
            ">;)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->accountServiceProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p2, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->accountStatusServiceProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p3, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->authenticationServiceProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p4, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p5, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p6, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->resProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p7, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p8, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->adAnalyticsProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p9, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->finalCreateAccountLogInCheckProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/login/CreateAccountHelper_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PersistentAccountService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/accountstatus/AccountStatusService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/AuthenticationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;",
            ">;)",
            "Lcom/squareup/ui/login/CreateAccountHelper_Factory;"
        }
    .end annotation

    .line 74
    new-instance v10, Lcom/squareup/ui/login/CreateAccountHelper_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/login/CreateAccountHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/account/PersistentAccountService;Lcom/squareup/server/accountstatus/AccountStatusService;Lcom/squareup/server/account/AuthenticationService;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;)Lcom/squareup/ui/login/CreateAccountHelper;
    .locals 11

    .line 81
    new-instance v10, Lcom/squareup/ui/login/CreateAccountHelper;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/login/CreateAccountHelper;-><init>(Lcom/squareup/account/PersistentAccountService;Lcom/squareup/server/accountstatus/AccountStatusService;Lcom/squareup/server/account/AuthenticationService;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/ui/login/CreateAccountHelper;
    .locals 10

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->accountServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/account/PersistentAccountService;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->accountStatusServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/server/accountstatus/AccountStatusService;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->authenticationServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/server/account/AuthenticationService;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->authenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/account/LegacyAuthenticator;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->adAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/adanalytics/AdAnalytics;

    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->finalCreateAccountLogInCheckProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->newInstance(Lcom/squareup/account/PersistentAccountService;Lcom/squareup/server/accountstatus/AccountStatusService;Lcom/squareup/server/account/AuthenticationService;Lcom/squareup/account/LegacyAuthenticator;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/ui/loggedout/FinalCreateAccountLogInCheck;)Lcom/squareup/ui/login/CreateAccountHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/login/CreateAccountHelper_Factory;->get()Lcom/squareup/ui/login/CreateAccountHelper;

    move-result-object v0

    return-object v0
.end method
