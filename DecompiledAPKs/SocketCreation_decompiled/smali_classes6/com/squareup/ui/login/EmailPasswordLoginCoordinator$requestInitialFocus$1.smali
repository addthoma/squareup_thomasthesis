.class final Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$requestInitialFocus$1;
.super Ljava/lang/Object;
.source "EmailPasswordLoginCoordinator.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->requestInitialFocus(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$requestInitialFocus$1;->this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$requestInitialFocus$1;->this$0:Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;->access$getEmailField$p(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;)Lcom/squareup/ui/XableEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "emailField.editText"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;Z)V

    return-void
.end method
