.class final Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ForgotPasswordCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/ForgotPasswordCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/login/ForgotPasswordCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/ForgotPasswordCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/ForgotPasswordCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1;->$view:Landroid/view/View;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/legacy/Screen;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPassword;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/Screen;->component2()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 65
    new-instance v0, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1$1;-><init>(Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 69
    iget-object v1, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1;->$view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 70
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 71
    sget v3, Lcom/squareup/utilities/R$string;->send:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 72
    new-instance v3, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1$config$1;

    invoke-direct {v3, v0}, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1$config$1;-><init>(Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1$1;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    new-instance v4, Lcom/squareup/ui/login/ForgotPasswordCoordinator$sam$java_lang_Runnable$0;

    invoke-direct {v4, v3}, Lcom/squareup/ui/login/ForgotPasswordCoordinator$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v4, Ljava/lang/Runnable;

    invoke-virtual {v2, v4}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 73
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v4, Lcom/squareup/common/authenticatorviews/R$string;->back:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 74
    new-instance v2, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1$config$2;

    invoke-direct {v2, p1}, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1$config$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 75
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 76
    iget-object v2, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/ForgotPasswordCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->access$getActionBar$p(Lcom/squareup/ui/login/ForgotPasswordCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v2

    const-string v3, "actionBar.presenter"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 78
    iget-object v1, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/ForgotPasswordCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/login/ForgotPasswordCoordinator;->access$getEmailField$p(Lcom/squareup/ui/login/ForgotPasswordCoordinator;)Lcom/squareup/ui/XableEditText;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1$2;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1$2;-><init>(Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1;Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1$1;)V

    check-cast v2, Lcom/squareup/debounce/DebouncedOnEditorActionListener;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1;->$view:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1$3;

    invoke-direct {v1, p1}, Lcom/squareup/ui/login/ForgotPasswordCoordinator$attach$1$3;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
