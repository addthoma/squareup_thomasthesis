.class public final Lcom/squareup/ui/login/CreateAccountPresenter$countryPickerPopupPresenter$1;
.super Lcom/squareup/mortar/PopupPresenter;
.source "CreateAccountPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/CreateAccountPresenter;-><init>(Lcom/squareup/location/CountryCodeGuesser;Lcom/squareup/ui/login/CreateAccountHelper;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Landroid/accounts/AccountManager;Lflow/Flow;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/ui/login/CreateAccountScreen$Runner;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/ui/login/CountryPickerPopup$Params;",
        "Lcom/squareup/CountryCode;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0003H\u0014\u00a8\u0006\u0007"
    }
    d2 = {
        "com/squareup/ui/login/CreateAccountPresenter$countryPickerPopupPresenter$1",
        "Lcom/squareup/mortar/PopupPresenter;",
        "Lcom/squareup/ui/login/CountryPickerPopup$Params;",
        "Lcom/squareup/CountryCode;",
        "onPopupResult",
        "",
        "country",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/CreateAccountPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/CreateAccountPresenter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 81
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter$countryPickerPopupPresenter$1;->this$0:Lcom/squareup/ui/login/CreateAccountPresenter;

    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPopupResult(Lcom/squareup/CountryCode;)V
    .locals 1

    const-string v0, "country"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/login/CreateAccountPresenter$countryPickerPopupPresenter$1;->this$0:Lcom/squareup/ui/login/CreateAccountPresenter;

    invoke-static {v0, p1}, Lcom/squareup/ui/login/CreateAccountPresenter;->access$setSelectedCountry(Lcom/squareup/ui/login/CreateAccountPresenter;Lcom/squareup/CountryCode;)V

    return-void
.end method

.method public bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 81
    check-cast p1, Lcom/squareup/CountryCode;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/CreateAccountPresenter$countryPickerPopupPresenter$1;->onPopupResult(Lcom/squareup/CountryCode;)V

    return-void
.end method
