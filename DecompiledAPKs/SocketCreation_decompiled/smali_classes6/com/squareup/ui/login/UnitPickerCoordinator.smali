.class public final Lcom/squareup/ui/login/UnitPickerCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "UnitPickerCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001\u0013B\u001f\u0012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u0014\u0010\u0008\u001a\u00020\tX\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u000e\u0010\u000c\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R \u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/ui/login/UnitPickerCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lio/reactivex/Observable;)V",
        "adapter",
        "Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;",
        "getAdapter$authenticator_views_release",
        "()Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;",
        "container",
        "Landroid/widget/ListView;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "detach",
        "UnitAdapter",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final adapter:Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;

.field private container:Landroid/widget/ListView;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$PickUnit;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/UnitPickerCoordinator;->screenData:Lio/reactivex/Observable;

    .line 27
    new-instance p1, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;

    invoke-direct {p1}, Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/UnitPickerCoordinator;->adapter:Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/ui/login/UnitPickerCoordinator;)Landroid/widget/ListView;
    .locals 1

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/login/UnitPickerCoordinator;->container:Landroid/widget/ListView;

    if-nez p0, :cond_0

    const-string v0, "container"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setContainer$p(Lcom/squareup/ui/login/UnitPickerCoordinator;Landroid/widget/ListView;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/login/UnitPickerCoordinator;->container:Landroid/widget/ListView;

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 34
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 35
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->select_a_location:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 36
    sget v0, Lcom/squareup/marin/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 37
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->select_a_location_subtext:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/login/UnitPickerCoordinator;->screenData:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/UnitPickerCoordinator$attach$1;-><init>(Lcom/squareup/ui/login/UnitPickerCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/login/UnitPickerCoordinator;->container:Landroid/widget/ListView;

    if-nez v0, :cond_0

    const-string v1, "container"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x0

    check-cast v1, Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 65
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->detach(Landroid/view/View;)V

    return-void
.end method

.method public final getAdapter$authenticator_views_release()Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/login/UnitPickerCoordinator;->adapter:Lcom/squareup/ui/login/UnitPickerCoordinator$UnitAdapter;

    return-object v0
.end method
