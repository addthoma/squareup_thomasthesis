.class public abstract Lcom/squareup/ui/login/AuthenticatorEvent;
.super Ljava/lang/Object;
.source "AuthenticatorEvent.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/AuthenticatorEvent$PromptToResetPassword;,
        Lcom/squareup/ui/login/AuthenticatorEvent$ResetPassword;,
        Lcom/squareup/ui/login/AuthenticatorEvent$RetryResetPassword;,
        Lcom/squareup/ui/login/AuthenticatorEvent$SwitchLoginPromptToDeviceCode;,
        Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithEmail;,
        Lcom/squareup/ui/login/AuthenticatorEvent$PasswordTextChanged;,
        Lcom/squareup/ui/login/AuthenticatorEvent$CreateAccount;,
        Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithDeviceCode;,
        Lcom/squareup/ui/login/AuthenticatorEvent$SkipTwoFactorEnrollment;,
        Lcom/squareup/ui/login/AuthenticatorEvent$EnrollInGoogleAuth;,
        Lcom/squareup/ui/login/AuthenticatorEvent$EnrollInSms;,
        Lcom/squareup/ui/login/AuthenticatorEvent$ShowGoogleAuthCodeInsteadOfQr;,
        Lcom/squareup/ui/login/AuthenticatorEvent$PromptToEnrollSms;,
        Lcom/squareup/ui/login/AuthenticatorEvent$PromptToVerifyGoogleAuthCode;,
        Lcom/squareup/ui/login/AuthenticatorEvent$PromptToVerifySmsCode;,
        Lcom/squareup/ui/login/AuthenticatorEvent$ResendSmsCode;,
        Lcom/squareup/ui/login/AuthenticatorEvent$SwitchVerificationPromptToSms;,
        Lcom/squareup/ui/login/AuthenticatorEvent$VerifyGoogleAuthCode;,
        Lcom/squareup/ui/login/AuthenticatorEvent$VerifySmsCode;,
        Lcom/squareup/ui/login/AuthenticatorEvent$SelectMerchant;,
        Lcom/squareup/ui/login/AuthenticatorEvent$SelectUnit;,
        Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;,
        Lcom/squareup/ui/login/AuthenticatorEvent$WarningDialogDismissed;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0017\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000b\u000c\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0017\u001a\u001b\u001c\u001d\u001e\u001f !\"#$%&\'()*+,-./0\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "",
        "()V",
        "CreateAccount",
        "EnrollInGoogleAuth",
        "EnrollInSms",
        "GoBackFrom",
        "LoginWithDeviceCode",
        "LoginWithEmail",
        "PasswordTextChanged",
        "PromptToEnrollSms",
        "PromptToResetPassword",
        "PromptToVerifyGoogleAuthCode",
        "PromptToVerifySmsCode",
        "ResendSmsCode",
        "ResetPassword",
        "RetryResetPassword",
        "SelectMerchant",
        "SelectUnit",
        "ShowGoogleAuthCodeInsteadOfQr",
        "SkipTwoFactorEnrollment",
        "SwitchLoginPromptToDeviceCode",
        "SwitchVerificationPromptToSms",
        "VerifyGoogleAuthCode",
        "VerifySmsCode",
        "WarningDialogDismissed",
        "Lcom/squareup/ui/login/AuthenticatorEvent$PromptToResetPassword;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$ResetPassword;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$RetryResetPassword;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$SwitchLoginPromptToDeviceCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithEmail;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$PasswordTextChanged;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$CreateAccount;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithDeviceCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$SkipTwoFactorEnrollment;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$EnrollInGoogleAuth;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$EnrollInSms;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$ShowGoogleAuthCodeInsteadOfQr;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$PromptToEnrollSms;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$PromptToVerifyGoogleAuthCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$PromptToVerifySmsCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$ResendSmsCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$SwitchVerificationPromptToSms;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$VerifyGoogleAuthCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$VerifySmsCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$SelectMerchant;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$SelectUnit;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$GoBackFrom;",
        "Lcom/squareup/ui/login/AuthenticatorEvent$WarningDialogDismissed;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 10
    invoke-direct {p0}, Lcom/squareup/ui/login/AuthenticatorEvent;-><init>()V

    return-void
.end method
