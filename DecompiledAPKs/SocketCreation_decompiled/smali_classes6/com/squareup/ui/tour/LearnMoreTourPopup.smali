.class public Lcom/squareup/ui/tour/LearnMoreTourPopup;
.super Lcom/squareup/flowlegacy/DialogPopup;
.source "LearnMoreTourPopup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/flowlegacy/DialogPopup<",
        "Lcom/squareup/util/VoidParcelable;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/flowlegacy/DialogPopup;-><init>(Landroid/content/Context;)V

    .line 19
    iput-object p2, p0, Lcom/squareup/ui/tour/LearnMoreTourPopup;->presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    return-void
.end method


# virtual methods
.method protected bridge synthetic createDialog(Landroid/os/Parcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 0

    .line 14
    check-cast p1, Lcom/squareup/util/VoidParcelable;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/tour/LearnMoreTourPopup;->createDialog(Lcom/squareup/util/VoidParcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;

    move-result-object p1

    return-object p1
.end method

.method protected createDialog(Lcom/squareup/util/VoidParcelable;ZLcom/squareup/mortar/PopupPresenter;)Landroid/app/Dialog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/VoidParcelable;",
            "Z",
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/util/VoidParcelable;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Landroid/app/Dialog;"
        }
    .end annotation

    .line 24
    iget-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopup;->presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->createPopupViewScopedContext()Landroid/content/Context;

    move-result-object p1

    .line 25
    sget p2, Lcom/squareup/loggedout/R$layout;->tour_popup_view:I

    const/4 p3, 0x0

    invoke-static {p1, p2, p3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 26
    new-instance p2, Landroid/app/Dialog;

    .line 27
    invoke-virtual {p0}, Lcom/squareup/ui/tour/LearnMoreTourPopup;->getContext()Landroid/content/Context;

    move-result-object p3

    sget v0, Lcom/squareup/noho/R$style;->Theme_Noho_Dialog_NoBackground_NoDim:I

    invoke-direct {p2, p3, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 28
    invoke-static {}, Lcom/squareup/util/Dialogs;->ignoresSearchKeyListener()Landroid/content/DialogInterface$OnKeyListener;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 29
    invoke-virtual {p2, p1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    const/4 p1, 0x1

    .line 30
    invoke-virtual {p2, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 31
    new-instance p1, Lcom/squareup/ui/tour/-$$Lambda$LearnMoreTourPopup$6onnC-8ELIzjVUvXUoYIFFd5aMo;

    invoke-direct {p1, p0}, Lcom/squareup/ui/tour/-$$Lambda$LearnMoreTourPopup$6onnC-8ELIzjVUvXUoYIFFd5aMo;-><init>(Lcom/squareup/ui/tour/LearnMoreTourPopup;)V

    invoke-virtual {p2, p1}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 34
    invoke-virtual {p2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/4 p3, -0x1

    invoke-virtual {p1, p3, p3}, Landroid/view/Window;->setLayout(II)V

    return-object p2
.end method

.method public synthetic lambda$createDialog$0$LearnMoreTourPopup(Landroid/content/DialogInterface;)V
    .locals 1

    .line 31
    iget-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopup;->presenter:Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->onDismissed(Ljava/lang/Object;)V

    return-void
.end method
