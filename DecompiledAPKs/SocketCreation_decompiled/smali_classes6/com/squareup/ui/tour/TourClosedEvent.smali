.class public Lcom/squareup/ui/tour/TourClosedEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "TourClosedEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/tour/TourClosedEvent$Origin;
    }
.end annotation


# instance fields
.field public final call_to_action_clicked:Z

.field public final complete:Z

.field public final origin:Lcom/squareup/ui/tour/TourClosedEvent$Origin;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/tour/TourClosedEvent$Origin;ZZ)V
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->TOUR_CLOSED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/tour/TourClosedEvent;->origin:Lcom/squareup/ui/tour/TourClosedEvent$Origin;

    .line 31
    iput-boolean p2, p0, Lcom/squareup/ui/tour/TourClosedEvent;->complete:Z

    .line 32
    iput-boolean p3, p0, Lcom/squareup/ui/tour/TourClosedEvent;->call_to_action_clicked:Z

    return-void
.end method
