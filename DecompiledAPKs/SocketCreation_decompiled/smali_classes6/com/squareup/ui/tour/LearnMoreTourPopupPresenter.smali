.class public Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;
.super Lcom/squareup/mortar/PopupPresenter;
.source "LearnMoreTourPopupPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/util/VoidParcelable;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private callToAction:Ljava/lang/Runnable;

.field private callToActionText:Ljava/lang/String;

.field private complete:Z

.field private currentPage:I

.field private itemsToShow:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/tour/Tour$HasTourPages;",
            ">;"
        }
    .end annotation
.end field

.field private final learnMoreTourPager:Lcom/squareup/ui/tour/LearnMoreTourPager;

.field private origin:Lcom/squareup/ui/tour/TourClosedEvent$Origin;

.field private popupViewScope:Lmortar/MortarScope;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/tour/LearnMoreTourPager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 40
    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 42
    iput-object p2, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->learnMoreTourPager:Lcom/squareup/ui/tour/LearnMoreTourPager;

    return-void
.end method

.method private destroyChildScope()V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->popupViewScope:Lmortar/MortarScope;

    invoke-virtual {v0}, Lmortar/MortarScope;->destroy()V

    const/4 v0, 0x0

    .line 66
    iput-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->popupViewScope:Lmortar/MortarScope;

    return-void
.end method


# virtual methods
.method public callToActionClicked()V
    .locals 1

    const/4 v0, 0x1

    .line 51
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->onDismissed(Ljava/lang/Object;)V

    .line 52
    invoke-virtual {p0}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mortar/Popup;

    invoke-interface {v0}, Lcom/squareup/mortar/Popup;->dismiss()V

    return-void
.end method

.method close()V
    .locals 1

    const/4 v0, 0x0

    .line 46
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->onDismissed(Ljava/lang/Object;)V

    .line 47
    invoke-virtual {p0}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mortar/Popup;

    invoke-interface {v0}, Lcom/squareup/mortar/Popup;->dismiss()V

    return-void
.end method

.method createPopupViewScopedContext()Landroid/content/Context;
    .locals 4

    .line 120
    invoke-virtual {p0}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/mortar/Popup;

    invoke-interface {v0}, Lcom/squareup/mortar/Popup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 122
    invoke-static {v0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object v1

    .line 124
    const-class v2, Lcom/squareup/ui/tour/LearnMoreTourPopup;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    .line 131
    invoke-virtual {v1, v2}, Lmortar/MortarScope;->findChild(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object v3

    iput-object v3, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->popupViewScope:Lmortar/MortarScope;

    .line 132
    iget-object v3, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->popupViewScope:Lmortar/MortarScope;

    if-nez v3, :cond_0

    .line 133
    const-class v3, Lcom/squareup/ui/tour/LearnMoreTourPopupView$Component;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/dagger/Components;->createChildComponent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/tour/LearnMoreTourPopupView$Component;

    .line 135
    invoke-virtual {v1}, Lmortar/MortarScope;->buildChild()Lmortar/MortarScope$Builder;

    move-result-object v1

    .line 136
    invoke-static {v1, v3}, Lcom/squareup/dagger/Components;->addAsScopeService(Lmortar/MortarScope$Builder;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    .line 138
    invoke-virtual {v1, v2}, Lmortar/MortarScope$Builder;->build(Ljava/lang/String;)Lmortar/MortarScope;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->popupViewScope:Lmortar/MortarScope;

    .line 139
    iget-object v1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->popupViewScope:Lmortar/MortarScope;

    invoke-static {v1, v3}, Lcom/squareup/leakcanary/ScopedObjectWatcher;->watchForLeaks(Lmortar/MortarScope;Ljava/lang/Object;)V

    .line 141
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->popupViewScope:Lmortar/MortarScope;

    invoke-virtual {v1, v0}, Lmortar/MortarScope;->createContext(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final dismiss()V
    .locals 1

    .line 85
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method getCallToActionText()Ljava/lang/String;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->callToActionText:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentPage()I
    .locals 1

    .line 113
    iget v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->currentPage:I

    return v0
.end method

.method getItemsToShow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/tour/Tour$HasTourPages;",
            ">;"
        }
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->itemsToShow:Ljava/util/List;

    return-object v0
.end method

.method hasCallToAction()Z
    .locals 1

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->callToAction:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 0

    const/4 p1, 0x0

    .line 90
    invoke-super {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->onLoad(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onPopupResult(Ljava/lang/Boolean;)V
    .locals 5

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/tour/TourClosedEvent;

    iget-object v2, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->origin:Lcom/squareup/ui/tour/TourClosedEvent$Origin;

    iget-boolean v3, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->complete:Z

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/tour/TourClosedEvent;-><init>(Lcom/squareup/ui/tour/TourClosedEvent$Origin;ZZ)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 57
    invoke-direct {p0}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->destroyChildScope()V

    .line 58
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->callToAction:Ljava/lang/Runnable;

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_0
    const/4 p1, 0x0

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->callToAction:Ljava/lang/Runnable;

    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->onPopupResult(Ljava/lang/Boolean;)V

    return-void
.end method

.method public setCurrentPage(I)V
    .locals 2

    .line 106
    iput p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->currentPage:I

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->itemsToShow:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_0

    .line 108
    iput-boolean v1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->complete:Z

    :cond_0
    return-void
.end method

.method public bridge synthetic show(Landroid/os/Parcelable;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/util/VoidParcelable;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->show(Lcom/squareup/util/VoidParcelable;)V

    return-void
.end method

.method public final show(Lcom/squareup/util/VoidParcelable;)V
    .locals 0

    .line 81
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public showLearnMore(ZLjava/lang/String;Ljava/lang/Runnable;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 71
    sget-object p1, Lcom/squareup/ui/tour/TourClosedEvent$Origin;->LEARN_MORE_PAYMENT:Lcom/squareup/ui/tour/TourClosedEvent$Origin;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/ui/tour/TourClosedEvent$Origin;->LEARN_MORE_WORLD:Lcom/squareup/ui/tour/TourClosedEvent$Origin;

    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->origin:Lcom/squareup/ui/tour/TourClosedEvent$Origin;

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->learnMoreTourPager:Lcom/squareup/ui/tour/LearnMoreTourPager;

    invoke-interface {p1}, Lcom/squareup/ui/tour/LearnMoreTourPager;->getLearnMoreItems()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->itemsToShow:Ljava/util/List;

    .line 73
    iput-object p2, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->callToActionText:Ljava/lang/String;

    .line 74
    iput-object p3, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->callToAction:Ljava/lang/Runnable;

    const/4 p1, 0x0

    .line 75
    iput p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->currentPage:I

    .line 76
    iget-object p2, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->itemsToShow:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    const/4 p3, 0x1

    if-ne p2, p3, :cond_1

    const/4 p1, 0x1

    :cond_1
    iput-boolean p1, p0, Lcom/squareup/ui/tour/LearnMoreTourPopupPresenter;->complete:Z

    .line 77
    new-instance p1, Lcom/squareup/util/VoidParcelable;

    invoke-direct {p1}, Lcom/squareup/util/VoidParcelable;-><init>()V

    invoke-super {p0, p1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method
