.class public final Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule;
.super Ljava/lang/Object;
.source "OrderHubAppletWorkflowModule.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007J\u0015\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0001\u00a2\u0006\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule;",
        "",
        "()V",
        "providesDebounceRateForOrdersSearch",
        "",
        "providesEmailAppAvailable",
        "",
        "context",
        "Landroid/app/Application;",
        "providesEmailAppAvailable$orderhub_applet_release",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubAppletWorkflowModule;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final providesDebounceRateForOrdersSearch()J
    .locals 2
    .annotation runtime Lcom/squareup/ui/orderhub/SearchDebounceRate;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-wide/16 v0, 0x2bc

    return-wide v0
.end method

.method public final providesEmailAppAvailable$orderhub_applet_release(Landroid/app/Application;)Z
    .locals 1
    .annotation runtime Lcom/squareup/ui/orderhub/EmailAppAvailable;
    .end annotation

    .annotation runtime Ldagger/Provides;
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mailto:"

    .line 27
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/Intents;->hasLinkableAppToUri(Landroid/app/Application;Landroid/net/Uri;)Z

    move-result p1

    return p1
.end method
