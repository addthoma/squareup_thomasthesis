.class public final Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderHubDetailCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;,
        Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubDetailCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubDetailCoordinator.kt\ncom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 5 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 6 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n+ 7 Views.kt\ncom/squareup/util/Views\n*L\n1#1,869:1\n49#2:870\n50#2,3:876\n53#2:917\n599#3,4:871\n601#3:875\n310#4,6:879\n310#4,3:885\n313#4,3:894\n310#4,3:897\n313#4,3:906\n355#4,6:909\n35#5,6:888\n35#5,6:900\n43#6,2:915\n1103#7,7:918\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubDetailCoordinator.kt\ncom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator\n*L\n275#1:870\n275#1,3:876\n275#1:917\n275#1,4:871\n275#1:875\n275#1,6:879\n275#1,3:885\n275#1,3:894\n275#1,3:897\n275#1,3:906\n275#1,6:909\n275#1,6:888\n275#1,6:900\n275#1,2:915\n787#1,7:918\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009c\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010$\n\u0002\u0008\u0004\u0018\u0000 \u0084\u00012\u00020\u0001:\u0004\u0084\u0001\u0085\u0001By\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\"\u0010\u0016\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u0018j\u0008\u0012\u0004\u0012\u00020\u0019`\u001b0\u0017\u00a2\u0006\u0002\u0010\u001cJ\u0010\u0010E\u001a\u0002062\u0006\u0010F\u001a\u00020 H\u0016J\u0010\u0010G\u001a\u0002062\u0006\u0010F\u001a\u00020 H\u0002J\u0010\u0010H\u001a\u0002062\u0006\u0010I\u001a\u00020 H\u0002J\u0010\u0010J\u001a\u0002062\u0006\u0010F\u001a\u00020 H\u0016J\u0008\u0010K\u001a\u000206H\u0002J\u0008\u0010L\u001a\u000206H\u0002J\u0008\u0010M\u001a\u000206H\u0002J\u0008\u0010N\u001a\u000206H\u0002J\u0008\u0010O\u001a\u000206H\u0002J:\u0010P\u001a\u0002062\u0006\u0010Q\u001a\u00020$2\u0006\u0010R\u001a\u00020S2\u0006\u0010T\u001a\u00020D2\u0006\u0010U\u001a\u00020V2\u0006\u0010W\u001a\u00020X2\u0008\u0010Y\u001a\u0004\u0018\u00010ZH\u0002J\u0010\u0010[\u001a\u0002062\u0006\u0010\\\u001a\u00020+H\u0002J\u0008\u0010]\u001a\u000206H\u0002J\u001a\u0010^\u001a\u0002062\u0006\u0010_\u001a\u00020`2\u0008\u0010a\u001a\u0004\u0018\u00010`H\u0002J\u0018\u0010b\u001a\u0002062\u0006\u0010c\u001a\u00020+2\u0006\u0010a\u001a\u00020dH\u0002J\u0010\u0010e\u001a\u0002062\u0006\u0010<\u001a\u00020\u0019H\u0002J\u0010\u0010f\u001a\u0002062\u0006\u0010g\u001a\u00020hH\u0002J\u0010\u0010i\u001a\u0002062\u0006\u0010<\u001a\u00020\u0019H\u0002J\u0018\u0010j\u001a\u0002062\u0006\u0010k\u001a\u00020l2\u0006\u0010m\u001a\u00020nH\u0002J6\u0010o\u001a\u0002062\u000c\u0010p\u001a\u0008\u0012\u0004\u0012\u00020X0q2\u0006\u0010r\u001a\u00020+2\u0006\u0010g\u001a\u00020h2\u0006\u0010s\u001a\u00020+2\u0006\u0010t\u001a\u00020+H\u0002J\u0010\u0010u\u001a\u0002062\u0006\u0010g\u001a\u00020hH\u0002J*\u0010v\u001a\u0002062\u0006\u0010w\u001a\u00020+2\u0006\u0010x\u001a\u00020+2\u0008\u0010y\u001a\u0004\u0018\u00010z2\u0006\u0010{\u001a\u00020lH\u0002J\u0008\u0010|\u001a\u000206H\u0002J\u0010\u0010}\u001a\u0002062\u0006\u0010m\u001a\u00020nH\u0002J2\u0010~\u001a\u0002062\u0006\u0010W\u001a\u00020X2\u0006\u0010R\u001a\u00020S2\u0006\u0010T\u001a\u00020D2\u0006\u0010U\u001a\u00020V2\u0008\u0010Y\u001a\u0004\u0018\u00010ZH\u0002JE\u0010\u007f\u001a\u0002062\r\u0010\u0080\u0001\u001a\u0008\u0012\u0004\u0012\u00020X0q2\u0006\u0010r\u001a\u00020+2\u0014\u0010\u0081\u0001\u001a\u000f\u0012\u0004\u0012\u00020d\u0012\u0004\u0012\u00020Z0\u0082\u00012\u0006\u0010t\u001a\u00020+2\u0006\u0010k\u001a\u00020lH\u0002J\u0011\u0010\u0083\u0001\u001a\u0002062\u0006\u0010<\u001a\u00020\u0019H\u0002R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020&X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020 X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020)X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020+X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020+X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010-\u001a\u00020+8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008-\u0010.R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u00020 X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u000201X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00102\u001a\u00020$X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u00103\u001a\u00020$X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u00104\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002060\u001705X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00107\u001a\u0008\u0012\u0004\u0012\u00020+05X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00108\u001a\u0008\u0012\u0004\u0012\u0002060\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u00109\u001a\u0008\u0012\u0004\u0012\u00020;0:X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010<\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0016\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0019\u0012\u0004\u0012\u00020\u001a0\u0018j\u0008\u0012\u0004\u0012\u00020\u0019`\u001b0\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010=\u001a\u00020>X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010?\u001a\u00020$X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010@\u001a\u00020$X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010A\u001a\u0004\u0018\u00010BX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010C\u001a\u00020DX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0086\u0001"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "relativeDateAndTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "locale",
        "Ljava/util/Locale;",
        "dateAndTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
        "badgePresenter",
        "Lcom/squareup/applet/BadgePresenter;",
        "actionBarNavigationHelper",
        "Lcom/squareup/applet/ActionBarNavigationHelper;",
        "appletSelection",
        "Lcom/squareup/applet/AppletSelection;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "(Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lcom/squareup/time/CurrentTime;Ljava/util/Locale;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;Lcom/squareup/recycler/RecyclerFactory;Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "detailContainer",
        "Landroid/view/View;",
        "dropDownContainer",
        "Lcom/squareup/ui/DropDownContainer;",
        "dropDownTitle",
        "Landroid/widget/TextView;",
        "filtersAdapter",
        "Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;",
        "filtersContainer",
        "filtersRecyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "isMasterDetail",
        "",
        "isQuickActionsEnabled",
        "isSearchBarHideable",
        "()Z",
        "mainView",
        "messageView",
        "Lcom/squareup/noho/NohoMessageView;",
        "notSyncingMessage",
        "notSyncingTitle",
        "onReadyToLoadMoreOrders",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "onScrollPositionIsAtEndOfList",
        "onScrollToEndOfList",
        "ordersRecycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/ui/orderhub/detail/OrderDetailRow;",
        "screen",
        "searchBar",
        "Lcom/squareup/noho/NohoEditRow;",
        "searchErrorMessage",
        "searchInstructionsMessage",
        "searchRevealButton",
        "Landroid/widget/ImageView;",
        "searchSpinner",
        "Landroid/widget/ProgressBar;",
        "attach",
        "view",
        "bindViews",
        "createOrdersRecycler",
        "coordinatorView",
        "detach",
        "displayLoadingView",
        "displayMessageView",
        "displayRecyclerView",
        "displaySearchErrorMessageView",
        "displaySearchInstructionsView",
        "setDisplayStatesOrQuickActions",
        "displayStateTextView",
        "quickActionButton",
        "Lcom/squareup/noho/NohoButton;",
        "quickActionSpinner",
        "quickActionFailureLabel",
        "Lcom/squareup/noho/NohoLabel;",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "quickActionStatus",
        "Lcom/squareup/ui/orderhub/OrderQuickActionStatus;",
        "setNotSyncingViewsVisibility",
        "visible",
        "setSyncErrorTextInMessageView",
        "setTextInMessageView",
        "message",
        "",
        "title",
        "setUpActionBar",
        "showBadge",
        "",
        "setupActionBarAndFiltersContainer",
        "setupEmptyOrdersView",
        "detailState",
        "Lcom/squareup/ui/orderhub/DetailState;",
        "setupFiltersContainer",
        "setupNoOrdersForSearchMessage",
        "filter",
        "Lcom/squareup/ui/orderhub/master/Filter;",
        "numberOfDaysSearched",
        "",
        "setupOrderListDisplay",
        "ordersToDisplay",
        "",
        "isLoadingMoreOrders",
        "canLoadMoreOrders",
        "isSearching",
        "setupOrdersNotSyncingBanner",
        "setupSearchBar",
        "enabled",
        "isInSearchContext",
        "searchText",
        "Lcom/squareup/workflow/text/WorkflowEditableText;",
        "selectedFilter",
        "setupSearchErrorMessage",
        "setupSearchInfoMessage",
        "updateQuickActionButton",
        "updateRecycler",
        "orders",
        "orderQuickActionStatusMap",
        "",
        "updateViews",
        "Companion",
        "Factory",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NEAR_END_OF_LIST_THRESHOLD:I = 0xa


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private final actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

.field private final appletSelection:Lcom/squareup/applet/AppletSelection;

.field private final badgePresenter:Lcom/squareup/applet/BadgePresenter;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

.field private detailContainer:Landroid/view/View;

.field private dropDownContainer:Lcom/squareup/ui/DropDownContainer;

.field private dropDownTitle:Landroid/widget/TextView;

.field private filtersAdapter:Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;

.field private filtersContainer:Landroid/view/View;

.field private filtersRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private isMasterDetail:Z

.field private isQuickActionsEnabled:Z

.field private final locale:Ljava/util/Locale;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private mainView:Landroid/view/View;

.field private messageView:Lcom/squareup/noho/NohoMessageView;

.field private notSyncingMessage:Landroid/widget/TextView;

.field private notSyncingTitle:Landroid/widget/TextView;

.field private final onReadyToLoadMoreOrders:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end field

.field private final onScrollPositionIsAtEndOfList:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final onScrollToEndOfList:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private ordersRecycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/orderhub/detail/OrderDetailRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final relativeDateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private searchBar:Lcom/squareup/noho/NohoEditRow;

.field private searchErrorMessage:Landroid/widget/TextView;

.field private searchInstructionsMessage:Landroid/widget/TextView;

.field private searchRevealButton:Landroid/widget/ImageView;

.field private searchSpinner:Landroid/widget/ProgressBar;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->Companion:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lcom/squareup/time/CurrentTime;Ljava/util/Locale;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;Lcom/squareup/recycler/RecyclerFactory;Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;",
            "Lcom/squareup/time/CurrentTime;",
            "Ljava/util/Locale;",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            "Lcom/squareup/applet/BadgePresenter;",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            "Lcom/squareup/applet/AppletSelection;",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)V"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "relativeDateAndTimeFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateAndTimeFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badgePresenter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionBarNavigationHelper"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appletSelection"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->relativeDateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->locale:Ljava/util/Locale;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    iput-object p8, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->appletSelection:Lcom/squareup/applet/AppletSelection;

    iput-object p9, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p10, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p11, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->screens:Lio/reactivex/Observable;

    const/4 p1, 0x1

    .line 135
    iput-boolean p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->isMasterDetail:Z

    .line 140
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->onScrollPositionIsAtEndOfList:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 143
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->onScrollPositionIsAtEndOfList:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 144
    invoke-virtual {p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    .line 145
    sget-object p3, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$onScrollToEndOfList$1;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$onScrollToEndOfList$1;

    check-cast p3, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    .line 146
    sget-object p3, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$onScrollToEndOfList$2;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$onScrollToEndOfList$2;

    check-cast p3, Lio/reactivex/functions/Function;

    invoke-virtual {p1, p3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p3, "onScrollPositionIsAtEndO\u2026ist }\n      .map { Unit }"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->onScrollToEndOfList:Lio/reactivex/Observable;

    .line 150
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->onReadyToLoadMoreOrders:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$Companion()Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Companion;
    .locals 1

    sget-object v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->Companion:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Companion;

    return-object v0
.end method

.method public static final synthetic access$displayRecyclerView(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)V
    .locals 0

    .line 100
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->displayRecyclerView()V

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;
    .locals 1

    .line 100
    iget-object p0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getActionBarNavigationHelper$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/applet/ActionBarNavigationHelper;
    .locals 0

    .line 100
    iget-object p0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    return-object p0
.end method

.method public static final synthetic access$getCurrentTime$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/time/CurrentTime;
    .locals 0

    .line 100
    iget-object p0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    return-object p0
.end method

.method public static final synthetic access$getDropDownContainer$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/ui/DropDownContainer;
    .locals 1

    .line 100
    iget-object p0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    if-nez p0, :cond_0

    const-string v0, "dropDownContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getOnScrollPositionIsAtEndOfList$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 100
    iget-object p0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->onScrollPositionIsAtEndOfList:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getOrdersRecycler$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/cycler/Recycler;
    .locals 1

    .line 100
    iget-object p0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->ordersRecycler:Lcom/squareup/cycler/Recycler;

    if-nez p0, :cond_0

    const-string v0, "ordersRecycler"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRelativeDateAndTimeFormatter$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;
    .locals 0

    .line 100
    iget-object p0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->relativeDateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 100
    iget-object p0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getScreen$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;
    .locals 1

    .line 100
    iget-object p0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->screen:Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    if-nez p0, :cond_0

    const-string v0, "screen"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getSearchBar$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/noho/NohoEditRow;
    .locals 1

    .line 100
    iget-object p0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchBar:Lcom/squareup/noho/NohoEditRow;

    if-nez p0, :cond_0

    const-string v0, "searchBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;Lcom/squareup/marin/widgets/ActionBarView;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method

.method public static final synthetic access$setDisplayStatesOrQuickActions(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;Landroid/widget/TextView;Lcom/squareup/noho/NohoButton;Landroid/widget/ProgressBar;Lcom/squareup/noho/NohoLabel;Lcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/OrderQuickActionStatus;)V
    .locals 0

    .line 100
    invoke-direct/range {p0 .. p6}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setDisplayStatesOrQuickActions(Landroid/widget/TextView;Lcom/squareup/noho/NohoButton;Landroid/widget/ProgressBar;Lcom/squareup/noho/NohoLabel;Lcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/OrderQuickActionStatus;)V

    return-void
.end method

.method public static final synthetic access$setDropDownContainer$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;Lcom/squareup/ui/DropDownContainer;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    return-void
.end method

.method public static final synthetic access$setOrdersRecycler$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;Lcom/squareup/cycler/Recycler;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->ordersRecycler:Lcom/squareup/cycler/Recycler;

    return-void
.end method

.method public static final synthetic access$setScreen$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->screen:Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    return-void
.end method

.method public static final synthetic access$setSearchBar$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;Lcom/squareup/noho/NohoEditRow;)V
    .locals 0

    .line 100
    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchBar:Lcom/squareup/noho/NohoEditRow;

    return-void
.end method

.method public static final synthetic access$updateViews(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;)V
    .locals 0

    .line 100
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->updateViews(Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 10

    .line 208
    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->mainView:Landroid/view/View;

    .line 210
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_detail_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.orderhub_detail_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->detailContainer:Landroid/view/View;

    .line 211
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_detail_orders_not_syncing_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026orders_not_syncing_title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->notSyncingTitle:Landroid/widget/TextView;

    .line 212
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_detail_orders_not_syncing_message:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026ders_not_syncing_message)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->notSyncingMessage:Landroid/widget/TextView;

    .line 213
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_detail_message_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026rhub_detail_message_view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    .line 214
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_detail_search:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.orderhub_detail_search)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoEditRow;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchBar:Lcom/squareup/noho/NohoEditRow;

    .line 215
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchBar:Lcom/squareup/noho/NohoEditRow;

    const-string v1, "searchBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v9, Lcom/squareup/noho/ClearPlugin;

    .line 216
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v2, "view.context"

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    .line 217
    sget-object v2, Lcom/squareup/noho/ClearPlugin$Visibility;->FOCUSED:Lcom/squareup/noho/ClearPlugin$Visibility;

    invoke-static {v2}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v5

    .line 218
    new-instance v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$1;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)V

    move-object v6, v2

    check-cast v6, Lkotlin/jvm/functions/Function0;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v2, v9

    .line 215
    invoke-direct/range {v2 .. v8}, Lcom/squareup/noho/ClearPlugin;-><init>(Landroid/content/Context;ILjava/util/Set;Lkotlin/jvm/functions/Function0;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast v9, Lcom/squareup/noho/NohoEditRow$Plugin;

    invoke-virtual {v0, v9}, Lcom/squareup/noho/NohoEditRow;->addPlugin(Lcom/squareup/noho/NohoEditRow$Plugin;)V

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchBar:Lcom/squareup/noho/NohoEditRow;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$2;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)V

    check-cast v2, Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditRow;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchBar:Lcom/squareup/noho/NohoEditRow;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$3;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)V

    check-cast v1, Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditRow;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 237
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_reveal_search_icon:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchRevealButton:Landroid/widget/ImageView;

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchRevealButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    new-instance v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$4;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    :cond_3
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_search_results_spinner:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026b_search_results_spinner)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchSpinner:Landroid/widget/ProgressBar;

    .line 246
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_detail_search_error_message:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026ail_search_error_message)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchErrorMessage:Landroid/widget/TextView;

    .line 247
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_detail_search_instructions:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026tail_search_instructions)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchInstructionsMessage:Landroid/widget/TextView;

    .line 249
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(com.sq\u2026s.R.id.stable_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 250
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v1, :cond_4

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0, v1}, Lcom/squareup/applet/BadgePresenter;->takeView(Ljava/lang/Object;)V

    .line 251
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_detail_filters_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026detail_filters_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->filtersContainer:Landroid/view/View;

    .line 252
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_detail_filters_dropdown_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026lters_dropdown_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/DropDownContainer;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    .line 253
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_detail_filters_title:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026hub_detail_filters_title)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->dropDownTitle:Landroid/widget/TextView;

    .line 255
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_detail_filters_dropdown_arrow:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 254
    check-cast v0, Lcom/squareup/marin/widgets/MarinVerticalCaretView;

    .line 256
    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    if-nez v1, :cond_5

    const-string v2, "dropDownContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$5;

    invoke-direct {v2, v0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$5;-><init>(Lcom/squareup/marin/widgets/MarinVerticalCaretView;)V

    check-cast v2, Lcom/squareup/ui/DropDownContainer$DropDownListener;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/DropDownContainer;->setDropDownListener(Lcom/squareup/ui/DropDownContainer$DropDownListener;)V

    .line 266
    new-instance v0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;-><init>(Lcom/squareup/util/Res;)V

    iput-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->filtersAdapter:Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;

    .line 267
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_detail_filters_recycler_view:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "view.findViewById(R.id.o\u2026il_filters_recycler_view)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->filtersRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 268
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->filtersRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v0, "filtersRecyclerView"

    if-nez p1, :cond_6

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    new-instance v1, Lcom/squareup/noho/NohoEdgeDecoration;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->mainView:Landroid/view/View;

    const-string v3, "mainView"

    if-nez v2, :cond_7

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v4, "mainView.resources"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(Landroid/content/res/Resources;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 269
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->filtersRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_8

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->filtersAdapter:Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;

    if-nez v0, :cond_9

    const-string v1, "filtersAdapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 271
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->mainView:Landroid/view/View;

    if-nez p1, :cond_a

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->createOrdersRecycler(Landroid/view/View;)V

    return-void
.end method

.method private final createOrdersRecycler(Landroid/view/View;)V
    .locals 4

    .line 275
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 276
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_detail_orders_recycler_view:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "coordinatorView.findView\u2026ail_orders_recycler_view)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 870
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 871
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 872
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 876
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 877
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 278
    sget-object v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$1$1;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 279
    sget-object v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$1$2;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$1$2;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->compareItemsContent(Lkotlin/jvm/functions/Function2;)V

    .line 880
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$$special$$inlined$row$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 283
    sget-object v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$1$3$1;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$1$3$1;

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 290
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$1$3$2;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$1$3$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v2}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 879
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 886
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$$special$$inlined$row$2;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$$special$$inlined$row$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 295
    sget v2, Lcom/squareup/orderhub/applet/R$layout;->orderhub_detail_order_row:I

    .line 888
    new-instance v3, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v3, v2, p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 344
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$1$4$2;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$1$4$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v2}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 885
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 898
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$$special$$inlined$row$3;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$$special$$inlined$row$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 349
    sget v2, Lcom/squareup/orderhub/applet/R$layout;->orderhub_detail_header_row:I

    .line 900
    new-instance v3, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$$inlined$adopt$lambda$2;

    invoke-direct {v3, v2, p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$$inlined$adopt$lambda$2;-><init>(ILcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 355
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    sget-object v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$1$5$2;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$1$5$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v2}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->edges(Lcom/squareup/cycler/Recycler$RowSpec;Lkotlin/jvm/functions/Function1;)V

    .line 897
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 910
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$$special$$inlined$extraItem$1;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$$special$$inlined$extraItem$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 361
    sget-object v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$1$6$1;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$1$6$1;

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 910
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 909
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extraItem(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 915
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/16 v2, 0xa

    .line 368
    invoke-virtual {v0, v2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 369
    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 915
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 874
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->ordersRecycler:Lcom/squareup/cycler/Recycler;

    .line 372
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->ordersRecycler:Lcom/squareup/cycler/Recycler;

    const-string v0, "ordersRecycler"

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    const/4 v1, 0x0

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    .line 374
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->ordersRecycler:Lcom/squareup/cycler/Recycler;

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$createOrdersRecycler$2;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;

    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    return-void

    .line 871
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final displayLoadingView()V
    .locals 2

    .line 768
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->ordersRecycler:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_0

    const-string v1, "ordersRecycler"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 769
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_1

    const-string v1, "messageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 770
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchInstructionsMessage:Landroid/widget/TextView;

    if-nez v0, :cond_2

    const-string v1, "searchInstructionsMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 771
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchErrorMessage:Landroid/widget/TextView;

    if-nez v0, :cond_3

    const-string v1, "searchErrorMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 772
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchSpinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_4

    const-string v1, "searchSpinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    return-void
.end method

.method private final displayMessageView()V
    .locals 2

    .line 736
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->ordersRecycler:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_0

    const-string v1, "ordersRecycler"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 737
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_1

    const-string v1, "messageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 738
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchInstructionsMessage:Landroid/widget/TextView;

    if-nez v0, :cond_2

    const-string v1, "searchInstructionsMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 739
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchErrorMessage:Landroid/widget/TextView;

    if-nez v0, :cond_3

    const-string v1, "searchErrorMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 740
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchSpinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_4

    const-string v1, "searchSpinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    return-void
.end method

.method private final displayRecyclerView()V
    .locals 2

    .line 744
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->ordersRecycler:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_0

    const-string v1, "ordersRecycler"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 745
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_1

    const-string v1, "messageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 746
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchInstructionsMessage:Landroid/widget/TextView;

    if-nez v0, :cond_2

    const-string v1, "searchInstructionsMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 747
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchErrorMessage:Landroid/widget/TextView;

    if-nez v0, :cond_3

    const-string v1, "searchErrorMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 748
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchSpinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_4

    const-string v1, "searchSpinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    return-void
.end method

.method private final displaySearchErrorMessageView()V
    .locals 2

    .line 760
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->ordersRecycler:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_0

    const-string v1, "ordersRecycler"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 761
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_1

    const-string v1, "messageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 762
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchInstructionsMessage:Landroid/widget/TextView;

    if-nez v0, :cond_2

    const-string v1, "searchInstructionsMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 763
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchErrorMessage:Landroid/widget/TextView;

    if-nez v0, :cond_3

    const-string v1, "searchErrorMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 764
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchSpinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_4

    const-string v1, "searchSpinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    return-void
.end method

.method private final displaySearchInstructionsView()V
    .locals 2

    .line 752
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->ordersRecycler:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_0

    const-string v1, "ordersRecycler"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 753
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_1

    const-string v1, "messageView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 754
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchInstructionsMessage:Landroid/widget/TextView;

    if-nez v0, :cond_2

    const-string v1, "searchInstructionsMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 755
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchErrorMessage:Landroid/widget/TextView;

    if-nez v0, :cond_3

    const-string v1, "searchErrorMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 756
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchSpinner:Landroid/widget/ProgressBar;

    if-nez v0, :cond_4

    const-string v1, "searchSpinner"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    return-void
.end method

.method private final isSearchBarHideable()Z
    .locals 1

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchRevealButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private final setDisplayStatesOrQuickActions(Landroid/widget/TextView;Lcom/squareup/noho/NohoButton;Landroid/widget/ProgressBar;Lcom/squareup/noho/NohoLabel;Lcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/OrderQuickActionStatus;)V
    .locals 6

    .line 401
    move-object v0, p1

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 402
    move-object v1, p2

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 403
    move-object v1, p3

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 404
    move-object v1, p4

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    const/4 v1, 0x0

    .line 405
    invoke-virtual {p2, v1}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    .line 407
    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->isMasterDetail:Z

    if-nez v2, :cond_0

    return-void

    .line 411
    :cond_0
    iget-boolean v2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->isQuickActionsEnabled:Z

    if-eqz v2, :cond_1

    invoke-static {p5}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->isFinished(Lcom/squareup/orders/model/Order;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    if-eqz v1, :cond_2

    .line 413
    invoke-static {p5}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getPrimaryAction(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Action;

    move-result-object v2

    if-eqz v2, :cond_2

    move-object v0, p0

    move-object v1, p5

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p6

    .line 414
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->updateQuickActionButton(Lcom/squareup/orders/model/Order;Lcom/squareup/noho/NohoButton;Landroid/widget/ProgressBar;Lcom/squareup/noho/NohoLabel;Lcom/squareup/ui/orderhub/OrderQuickActionStatus;)V

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_4

    .line 423
    invoke-static {p5}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    sget-object p3, Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;->READY:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    if-ne p2, p3, :cond_3

    invoke-static {p5}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p2

    iget-object p2, p2, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    sget-object p3, Lcom/squareup/orders/model/Order$FulfillmentType;->MANAGED_DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-ne p2, p3, :cond_3

    .line 424
    iget-object p2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/orderhub/applet/R$string;->orderhub_quick_actions_awaiting_courier:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 425
    iget-object p3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    sget p4, Lcom/squareup/orderhub/applet/R$color;->orderhub_text_color_completed:I

    invoke-interface {p3, p4}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p3

    goto :goto_0

    :cond_3
    return-void

    .line 431
    :cond_4
    invoke-static {p5}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {p2, p3}, Lcom/squareup/ui/orderhub/util/proto/OrderDisplayStateDatasKt;->getDisplayName(Lcom/squareup/protos/client/orders/OrderDisplayStateData;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p2

    .line 432
    iget-object p3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {p5}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object p4

    iget-object p5, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {p5}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object p5

    invoke-static {p4, p5}, Lcom/squareup/ui/orderhub/util/proto/OrderDisplayStateDatasKt;->getColor(Lcom/squareup/protos/client/orders/OrderDisplayStateData;Lorg/threeten/bp/ZonedDateTime;)I

    move-result p4

    invoke-interface {p3, p4}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p3

    .line 436
    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 437
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 438
    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_1
    return-void
.end method

.method private final setNotSyncingViewsVisibility(Z)V
    .locals 2

    .line 731
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->notSyncingMessage:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "notSyncingMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 732
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->notSyncingTitle:Landroid/widget/TextView;

    if-nez v0, :cond_1

    const-string v1, "notSyncingTitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final setSyncErrorTextInMessageView()V
    .locals 3

    .line 708
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "messageView"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_detail_orders_not_syncing_title:I

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoMessageView;->setTitle(I)V

    .line 709
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_detail_orders_not_syncing_message:I

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoMessageView;->setMessage(I)V

    return-void
.end method

.method private final setTextInMessageView(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    .line 703
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    const-string v1, "messageView"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p2}, Lcom/squareup/noho/NohoMessageView;->setTitle(Ljava/lang/CharSequence;)V

    .line 704
    iget-object p2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->messageView:Lcom/squareup/noho/NohoMessageView;

    if-nez p2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoMessageView;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final setUpActionBar(ZLjava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_1

    .line 662
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->appletSelection:Lcom/squareup/applet/AppletSelection;

    invoke-interface {p1}, Lcom/squareup/applet/AppletSelection;->selectedApplet()Lio/reactivex/Observable;

    move-result-object p1

    .line 663
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "appletSelection.selected\u2026.observeOn(mainScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 664
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->mainView:Landroid/view/View;

    if-nez v0, :cond_0

    const-string v1, "mainView"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setUpActionBar$1;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setUpActionBar$1;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;Ljava/lang/String;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    goto :goto_0

    .line 670
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p1, :cond_2

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setTitleNoUpButton(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private final setupActionBarAndFiltersContainer(Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;)V
    .locals 3

    .line 631
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->getData()Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 632
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->getData()Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/DetailState;->getSelectedFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/orderhub/master/Filter;->getFilterName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setUpActionBar(ZLjava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    .line 634
    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->mainView:Landroid/view/View;

    if-nez v1, :cond_1

    const-string v2, "mainView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_applet_name:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "mainView.resources.getSt\u2026ing.orderhub_applet_name)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setUpActionBar(ZLjava/lang/String;)V

    .line 638
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setupFiltersContainer(Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;)V

    :goto_0
    return-void
.end method

.method private final setupEmptyOrdersView(Lcom/squareup/ui/orderhub/DetailState;)V
    .locals 4

    .line 538
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->displayMessageView()V

    .line 539
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/DetailState;->getSyncingError()Lcom/squareup/ordermanagerdata/ResultState$Failure;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 540
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setSyncErrorTextInMessageView()V

    goto :goto_0

    .line 543
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_message_no_orders_subtitle:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 545
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/DetailState;->getSelectedFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/orderhub/master/Filter;->getFilterNameShortLowerCased(Lcom/squareup/util/Res;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "order_type"

    .line 544
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 547
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "res.phrase(R.string.orde\u2026)\n              .format()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 548
    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_message_no_orders_title:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 549
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/DetailState;->getSelectedFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object p1

    iget-object v3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1, v3}, Lcom/squareup/ui/orderhub/master/Filter;->getFilterNameShort(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 550
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 542
    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setTextInMessageView(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :goto_0
    const/4 p1, 0x0

    .line 554
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setNotSyncingViewsVisibility(Z)V

    return-void
.end method

.method private final setupFiltersContainer(Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;)V
    .locals 4

    .line 781
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->getSearchRendering()Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->isSearchEnabled()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->getSearchRendering()Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->getSearchDisplayState()Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    move-result-object v0

    sget-object v2, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->NOT_SEARCHING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    if-eq v0, v2, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->isSearchBarHideable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 782
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->filtersContainer:Landroid/view/View;

    const-string v3, "filtersContainer"

    if-nez v2, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    if-eqz v0, :cond_2

    const/16 v1, 0x8

    :cond_2
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 783
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->filtersAdapter:Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;

    if-nez v0, :cond_3

    const-string v1, "filtersAdapter"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0, p1}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->setData(Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;)V

    .line 784
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    const-string v1, "dropDownContainer"

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lcom/squareup/ui/DropDownContainer;->isDropDownVisible()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 785
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->dropDownContainer:Lcom/squareup/ui/DropDownContainer;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0}, Lcom/squareup/ui/DropDownContainer;->closeDropDown()V

    .line 787
    :cond_6
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->filtersContainer:Landroid/view/View;

    if-nez v0, :cond_7

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 918
    :cond_7
    new-instance v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setupFiltersContainer$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setupFiltersContainer$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 790
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->dropDownTitle:Landroid/widget/TextView;

    if-nez v0, :cond_8

    const-string v1, "dropDownTitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->getData()Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/OrderHubState;->getMasterState()Lcom/squareup/ui/orderhub/MasterState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/MasterState;->getSelectedFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/orderhub/master/Filter;->getFilterName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final setupNoOrdersForSearchMessage(Lcom/squareup/ui/orderhub/master/Filter;I)V
    .locals 6

    .line 572
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchErrorMessage:Landroid/widget/TextView;

    const-string v1, "searchErrorMessage"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 561
    :cond_0
    new-instance v2, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->mainView:Landroid/view/View;

    if-nez v3, :cond_1

    const-string v4, "mainView"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 562
    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_message_no_orders_found_for_search:I

    const-string v4, "transactions_link"

    invoke-virtual {v2, v3, v4}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v2

    .line 563
    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_message_transactions_link_text:I

    invoke-virtual {v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v2

    .line 564
    new-instance v3, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setupNoOrdersForSearchMessage$1;

    iget-object v4, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/orderhub/applet/R$color;->orderhub_link_color:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v4

    invoke-direct {v3, p0, v4}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setupNoOrdersForSearchMessage$1;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;I)V

    check-cast v3, Lcom/squareup/ui/LinkSpan;

    invoke-virtual {v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v2

    .line 569
    invoke-virtual {v2}, Lcom/squareup/ui/LinkSpan$Builder;->asPhrase()Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 570
    iget-object v3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->locale:Ljava/util/Locale;

    invoke-virtual {p1, v3, v4}, Lcom/squareup/ui/orderhub/master/Filter;->getFilterNameShortLowerCased(Lcom/squareup/util/Res;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v3, "order_type"

    invoke-virtual {v2, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v2, "number_of_days"

    .line 571
    invoke-virtual {p1, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 572
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 573
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchErrorMessage:Landroid/widget/TextView;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 574
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->displaySearchErrorMessageView()V

    return-void
.end method

.method private final setupOrderListDisplay(Ljava/util/List;ZLcom/squareup/ui/orderhub/DetailState;ZZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;Z",
            "Lcom/squareup/ui/orderhub/DetailState;",
            "ZZ)V"
        }
    .end annotation

    .line 617
    invoke-virtual {p3}, Lcom/squareup/ui/orderhub/DetailState;->getOrderQuickActionStatusMap()Ljava/util/Map;

    move-result-object v3

    .line 618
    invoke-virtual {p3}, Lcom/squareup/ui/orderhub/DetailState;->getSelectedFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v4, p5

    .line 616
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->updateRecycler(Ljava/util/List;ZLjava/util/Map;ZLcom/squareup/ui/orderhub/master/Filter;)V

    if-eqz p4, :cond_0

    if-nez p2, :cond_0

    .line 624
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->onReadyToLoadMoreOrders:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object p2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->onScrollToEndOfList:Lio/reactivex/Observable;

    const-wide/16 p4, 0x1

    invoke-virtual {p2, p4, p5}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 627
    :cond_0
    invoke-direct {p0, p3}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setupOrdersNotSyncingBanner(Lcom/squareup/ui/orderhub/DetailState;)V

    return-void
.end method

.method private final setupOrdersNotSyncingBanner(Lcom/squareup/ui/orderhub/DetailState;)V
    .locals 3

    .line 713
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/DetailState;->getSyncingError()Lcom/squareup/ordermanagerdata/ResultState$Failure;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 714
    invoke-direct {p0, v0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setNotSyncingViewsVisibility(Z)V

    .line 716
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->notSyncingMessage:Landroid/widget/TextView;

    if-nez v0, :cond_0

    const-string v1, "notSyncingMessage"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/DetailState;->getLastSyncDate()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 717
    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_detail_orders_not_syncing_message_last_sync_date:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 719
    iget-object v2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/DetailState;->getLastSyncDate()Lorg/threeten/bp/ZonedDateTime;

    move-result-object p1

    invoke-virtual {p1}, Lorg/threeten/bp/ZonedDateTime;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    const-string v2, "last_synced"

    .line 718
    invoke-virtual {v1, v2, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 721
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_0

    .line 723
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_detail_orders_not_syncing_message:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 716
    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    .line 726
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setNotSyncingViewsVisibility(Z)V

    :goto_1
    return-void
.end method

.method private final setupSearchBar(ZZLcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/ui/orderhub/master/Filter;)V
    .locals 4

    const/16 v0, 0x8

    const-string v1, "searchBar"

    if-nez p1, :cond_2

    .line 681
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchBar:Lcom/squareup/noho/NohoEditRow;

    if-nez p1, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setVisibility(I)V

    .line 682
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchRevealButton:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    return-void

    .line 686
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchBar:Lcom/squareup/noho/NohoEditRow;

    if-nez p1, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    const/4 v2, 0x0

    if-nez p2, :cond_5

    invoke-direct {p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->isSearchBarHideable()Z

    move-result v3

    if-nez v3, :cond_4

    goto :goto_0

    :cond_4
    const/16 v3, 0x8

    goto :goto_1

    :cond_5
    :goto_0
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {p1, v3}, Lcom/squareup/noho/NohoEditRow;->setVisibility(I)V

    .line 687
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchRevealButton:Landroid/widget/ImageView;

    if-eqz p1, :cond_7

    if-eqz p2, :cond_6

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 690
    :cond_7
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchBar:Lcom/squareup/noho/NohoEditRow;

    if-nez p1, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 688
    :cond_8
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_search_hint:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 689
    iget-object v2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->locale:Ljava/util/Locale;

    invoke-virtual {p4, v2, v3}, Lcom/squareup/ui/orderhub/master/Filter;->getFilterNameShortLowerCased(Lcom/squareup/util/Res;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p4

    check-cast p4, Ljava/lang/CharSequence;

    const-string v2, "filter"

    invoke-virtual {v0, v2, p4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p4

    .line 690
    invoke-virtual {p4}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p4

    invoke-virtual {p1, p4}, Lcom/squareup/noho/NohoEditRow;->setHint(Ljava/lang/CharSequence;)V

    if-nez p2, :cond_b

    .line 693
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchBar:Lcom/squareup/noho/NohoEditRow;

    if-nez p1, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 694
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchBar:Lcom/squareup/noho/NohoEditRow;

    if-nez p1, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditRow;->clearFocus()V

    .line 696
    :cond_b
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchBar:Lcom/squareup/noho/NohoEditRow;

    if-nez p1, :cond_c

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    check-cast p1, Landroid/widget/EditText;

    if-eqz p3, :cond_d

    invoke-static {p1, p3}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    return-void

    :cond_d
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Required value was null."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final setupSearchErrorMessage()V
    .locals 6

    .line 586
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchErrorMessage:Landroid/widget/TextView;

    const-string v1, "searchErrorMessage"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 578
    :cond_0
    new-instance v2, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->mainView:Landroid/view/View;

    if-nez v3, :cond_1

    const-string v4, "mainView"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 579
    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_message_search_error:I

    const-string v4, "retry_link"

    invoke-virtual {v2, v3, v4}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v2

    .line 580
    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_message_search_error_retry_cta:I

    invoke-virtual {v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v2

    .line 581
    new-instance v3, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setupSearchErrorMessage$1;

    iget-object v4, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/orderhub/applet/R$color;->orderhub_link_color:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v4

    invoke-direct {v3, p0, v4}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setupSearchErrorMessage$1;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;I)V

    check-cast v3, Lcom/squareup/ui/LinkSpan;

    invoke-virtual {v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v2

    .line 586
    invoke-virtual {v2}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 587
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchErrorMessage:Landroid/widget/TextView;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 589
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->displaySearchErrorMessageView()V

    return-void
.end method

.method private final setupSearchInfoMessage(I)V
    .locals 6

    .line 603
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchInstructionsMessage:Landroid/widget/TextView;

    const-string v1, "searchInstructionsMessage"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 593
    :cond_0
    new-instance v2, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->mainView:Landroid/view/View;

    if-nez v3, :cond_1

    const-string v4, "mainView"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 594
    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_message_search_instructions:I

    const-string v4, "transactions_link"

    invoke-virtual {v2, v3, v4}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v2

    .line 595
    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_message_transactions_link_text:I

    invoke-virtual {v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v2

    .line 596
    new-instance v3, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setupSearchInfoMessage$1;

    iget-object v4, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/orderhub/applet/R$color;->orderhub_link_color:I

    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v4

    invoke-direct {v3, p0, v4}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$setupSearchInfoMessage$1;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;I)V

    check-cast v3, Lcom/squareup/ui/LinkSpan;

    invoke-virtual {v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v2

    .line 601
    invoke-virtual {v2}, Lcom/squareup/ui/LinkSpan$Builder;->asPhrase()Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "number_of_days"

    .line 602
    invoke-virtual {v2, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 603
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 604
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->searchInstructionsMessage:Landroid/widget/TextView;

    if-nez p1, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 606
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->displaySearchInstructionsView()V

    return-void
.end method

.method private final updateQuickActionButton(Lcom/squareup/orders/model/Order;Lcom/squareup/noho/NohoButton;Landroid/widget/ProgressBar;Lcom/squareup/noho/NohoLabel;Lcom/squareup/ui/orderhub/OrderQuickActionStatus;)V
    .locals 9

    .line 450
    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getPrimaryAction(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Action;

    move-result-object v1

    const/4 v0, 0x1

    if-eqz p5, :cond_0

    .line 452
    invoke-virtual {p5}, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->isLoading()Z

    move-result v2

    if-ne v2, v0, :cond_0

    check-cast p3, Landroid/view/View;

    invoke-static {p3}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    goto/16 :goto_1

    :cond_0
    if-eqz p5, :cond_2

    .line 453
    invoke-virtual {p5}, Lcom/squareup/ui/orderhub/OrderQuickActionStatus;->getHasError()Z

    move-result v2

    if-ne v2, v0, :cond_2

    if-eqz v1, :cond_1

    .line 454
    iget-object v1, v1, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/squareup/ui/orderhub/util/proto/ActionsKt;->getNameRes(Lcom/squareup/protos/client/orders/Action$Type;)I

    move-result v1

    .line 455
    move-object v2, p4

    check-cast v2, Landroid/view/View;

    invoke-static {v2}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 456
    iget-object v2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_quick_actions_failed_action:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 457
    iget-object v3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    invoke-interface {v3, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v3, "action"

    invoke-virtual {v2, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 458
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 459
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/squareup/noho/NohoLabel;->setText(Ljava/lang/CharSequence;)V

    .line 462
    :cond_1
    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    .line 463
    move-object v0, p2

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 464
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_quick_actions_retry:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 465
    new-instance v0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$$inlined$let$lambda$1;

    move-object v1, v0

    move-object v2, p0

    move-object v3, p5

    move-object v4, p3

    move-object v5, p4

    move-object v6, p2

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$$inlined$let$lambda$1;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;Lcom/squareup/ui/orderhub/OrderQuickActionStatus;Landroid/widget/ProgressBar;Lcom/squareup/noho/NohoLabel;Lcom/squareup/noho/NohoButton;Lcom/squareup/orders/model/Order;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 469
    :cond_2
    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    .line 470
    move-object v0, p2

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    if-eqz v1, :cond_3

    .line 471
    iget-object v0, v1, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    if-eqz v0, :cond_3

    invoke-static {v0}, Lcom/squareup/ui/orderhub/util/proto/ActionsKt;->getNameRes(Lcom/squareup/protos/client/orders/Action$Type;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 472
    iget-object v2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->res:Lcom/squareup/util/Res;

    invoke-interface {v2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    .line 473
    new-instance v8, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$$inlined$let$lambda$2;

    move-object v0, v8

    move-object v2, p0

    move-object v3, p5

    move-object v4, p3

    move-object v5, p4

    move-object v6, p2

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateQuickActionButton$$inlined$let$lambda$2;-><init>(Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;Lcom/squareup/ui/orderhub/OrderQuickActionStatus;Landroid/widget/ProgressBar;Lcom/squareup/noho/NohoLabel;Lcom/squareup/noho/NohoButton;Lcom/squareup/orders/model/Order;)V

    check-cast v8, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v8}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    return-void
.end method

.method private final updateRecycler(Ljava/util/List;ZLjava/util/Map;ZLcom/squareup/ui/orderhub/master/Filter;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;Z",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/orderhub/OrderQuickActionStatus;",
            ">;Z",
            "Lcom/squareup/ui/orderhub/master/Filter;",
            ")V"
        }
    .end annotation

    .line 649
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->isSearchBarHideable()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p4, :cond_0

    goto :goto_0

    :cond_0
    const/4 p5, 0x0

    :goto_0
    move-object v4, p5

    .line 650
    iget-object p4, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->ordersRecycler:Lcom/squareup/cycler/Recycler;

    if-nez p4, :cond_1

    const-string p5, "ordersRecycler"

    invoke-static {p5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance p5, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1;

    move-object v0, p5

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateRecycler$1;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;Ljava/util/List;Ljava/util/Map;Lcom/squareup/ui/orderhub/master/Filter;Z)V

    check-cast p5, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p4, p5}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method private final updateViews(Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;)V
    .locals 7

    .line 482
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->getData()Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/OrderHubState;->isMasterDetail()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->isMasterDetail:Z

    .line 483
    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->screen:Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    .line 484
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->getData()Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/OrderHubState;->isQuickActionsEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->isQuickActionsEnabled:Z

    .line 486
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->getData()Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v4

    .line 487
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->getSearchRendering()Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    move-result-object v0

    .line 489
    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->mainView:Landroid/view/View;

    if-nez v1, :cond_0

    const-string v2, "mainView"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateViews$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$updateViews$1;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-static {v1, v2}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 490
    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->filtersContainer:Landroid/view/View;

    if-nez v1, :cond_1

    const-string v2, "filtersContainer"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 493
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->isSearchEnabled()Z

    move-result v1

    .line 494
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->getSearchDisplayState()Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->NOT_SEARCHING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    if-eq v2, v3, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    .line 495
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->getSearchEditText()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v3

    .line 496
    invoke-virtual {v4}, Lcom/squareup/ui/orderhub/DetailState;->getSelectedFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v5

    .line 492
    invoke-direct {p0, v1, v2, v3, v5}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setupSearchBar(ZZLcom/squareup/workflow/text/WorkflowEditableText;Lcom/squareup/ui/orderhub/master/Filter;)V

    .line 499
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setupActionBarAndFiltersContainer(Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;)V

    .line 501
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->getSearchDisplayState()Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    move-result-object p1

    sget-object v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->ordinal()I

    move-result p1

    aget p1, v1, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_1

    .line 532
    :pswitch_0
    invoke-virtual {v4}, Lcom/squareup/ui/orderhub/DetailState;->getSelectedFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object p1

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->getNumberOfDaysSearched()I

    move-result v0

    .line 531
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setupNoOrdersForSearchMessage(Lcom/squareup/ui/orderhub/master/Filter;I)V

    goto :goto_1

    .line 524
    :pswitch_1
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->getSearchResults()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    .line 523
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setupOrderListDisplay(Ljava/util/List;ZLcom/squareup/ui/orderhub/DetailState;ZZ)V

    goto :goto_1

    .line 520
    :pswitch_2
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setupSearchErrorMessage()V

    goto :goto_1

    .line 517
    :pswitch_3
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->displayLoadingView()V

    goto :goto_1

    .line 515
    :pswitch_4
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;->getNumberOfDaysSearched()I

    move-result p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setupSearchInfoMessage(I)V

    goto :goto_1

    .line 503
    :pswitch_5
    invoke-virtual {v4}, Lcom/squareup/ui/orderhub/DetailState;->getFilteredOrders()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 504
    invoke-direct {p0, v4}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setupEmptyOrdersView(Lcom/squareup/ui/orderhub/DetailState;)V

    goto :goto_1

    .line 507
    :cond_3
    invoke-virtual {v4}, Lcom/squareup/ui/orderhub/DetailState;->getFilteredOrders()Ljava/util/List;

    move-result-object v2

    .line 508
    invoke-virtual {v4}, Lcom/squareup/ui/orderhub/DetailState;->getLoadingMoreOrders()Z

    move-result v3

    .line 510
    invoke-virtual {v4}, Lcom/squareup/ui/orderhub/DetailState;->getCanLoadMoreOrders()Z

    move-result v5

    const/4 v6, 0x0

    move-object v1, p0

    .line 506
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->setupOrderListDisplay(Ljava/util/List;ZLcom/squareup/ui/orderhub/DetailState;ZZ)V

    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 185
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->bindViews(Landroid/view/View;)V

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->screens:Lio/reactivex/Observable;

    .line 187
    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 188
    sget-object v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$attach$1;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$attach$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens\n        .observe\u2026map { it.unwrapV2Screen }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    new-instance v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$attach$2;-><init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->onReadyToLoadMoreOrders:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v0, Lio/reactivex/ObservableSource;

    invoke-static {v0}, Lio/reactivex/Observable;->switchOnNext(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "switchOnNext(onReadyToLoadMoreOrders)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 196
    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->screens:Lio/reactivex/Observable;

    check-cast v1, Lio/reactivex/ObservableSource;

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 197
    sget-object v1, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$attach$3;->INSTANCE:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$attach$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public detach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Lcom/squareup/marin/widgets/Badgeable;

    invoke-virtual {p1, v0}, Lcom/squareup/applet/BadgePresenter;->dropView(Lcom/squareup/marin/widgets/Badgeable;)V

    return-void
.end method
