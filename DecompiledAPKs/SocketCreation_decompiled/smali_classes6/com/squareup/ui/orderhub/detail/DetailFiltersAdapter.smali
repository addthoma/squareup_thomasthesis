.class public final Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "DetailFiltersAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder;,
        Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$HeaderViewHolder;,
        Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;,
        Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;,
        Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailFiltersAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailFiltersAdapter.kt\ncom/squareup/ui/orderhub/detail/DetailFiltersAdapter\n+ 2 _Maps.kt\nkotlin/collections/MapsKt___MapsKt\n*L\n1#1,131:1\n151#2,2:132\n*E\n*S KotlinDebug\n*F\n+ 1 DetailFiltersAdapter.kt\ncom/squareup/ui/orderhub/detail/DetailFiltersAdapter\n*L\n28#1,2:132\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0008\u0000\u0018\u0000 \u00182\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0005\u0018\u0019\u001a\u001b\u001cB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u000c\u001a\u00020\rH\u0016J\u0010\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\rH\u0016J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\rH\u0016J\u0018\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\rH\u0016J\u000e\u0010\u0017\u001a\u00020\u00112\u0006\u0010\n\u001a\u00020\u000bR\u001e\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\u00080\u0007j\u0008\u0012\u0004\u0012\u00020\u0008`\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter;",
        "Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/util/Res;)V",
        "filterList",
        "Ljava/util/ArrayList;",
        "Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;",
        "Lkotlin/collections/ArrayList;",
        "screen",
        "Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;",
        "getItemCount",
        "",
        "getItemViewType",
        "position",
        "onBindViewHolder",
        "",
        "holder",
        "onCreateViewHolder",
        "parent",
        "Landroid/view/ViewGroup;",
        "viewType",
        "setData",
        "Companion",
        "FilterRow",
        "FilterViewHolder",
        "HeaderViewHolder",
        "ViewHolder",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$Companion;

.field public static final ORDERHUB_DETAIL_FILTER_HEADER_ROW:I = 0x0

.field public static final ORDERHUB_DETAIL_FILTER_ROW:I = 0x1


# instance fields
.field private final filterList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->Companion:Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;)V
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->res:Lcom/squareup/util/Res;

    .line 21
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->filterList:Ljava/util/ArrayList;

    return-void
.end method

.method public static final synthetic access$getFilterList$p(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;)Ljava/util/ArrayList;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->filterList:Ljava/util/ArrayList;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;)Lcom/squareup/util/Res;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getScreen$p(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;)Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;
    .locals 1

    .line 17
    iget-object p0, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->screen:Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    if-nez p0, :cond_0

    const-string v0, "screen"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setScreen$p(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;)V
    .locals 0

    .line 17
    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->screen:Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->filterList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->filterList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;->getHeader()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    :goto_0
    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->onBindViewHolder(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;I)V
    .locals 1

    const-string v0, "holder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p1, p2}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;->bind(I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;
    .locals 4

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 70
    sget v1, Lcom/squareup/orderhub/applet/R$layout;->orderhub_detail_filter_row:I

    goto :goto_0

    .line 72
    :cond_0
    sget v1, Lcom/squareup/orderhub/applet/R$layout;->orderhub_detail_filter_header_row:I

    .line 74
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const/4 v3, 0x0

    .line 75
    invoke-virtual {v2, v1, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    const-string v1, "inflate"

    if-ne p2, v0, :cond_1

    .line 77
    new-instance p2, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder;-><init>(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;Landroid/view/View;)V

    check-cast p2, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;

    goto :goto_1

    .line 79
    :cond_1
    new-instance p2, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$HeaderViewHolder;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$HeaderViewHolder;-><init>(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;Landroid/view/View;)V

    check-cast p2, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;

    :goto_1
    return-object p2
.end method

.method public final setData(Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;)V
    .locals 9

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->screen:Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->filterList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 27
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    check-cast v0, Ljava/util/Map;

    .line 28
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->getData()Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/OrderHubState;->getMasterState()Lcom/squareup/ui/orderhub/MasterState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/MasterState;->getFiltersAndOpenOrderCounts()Ljava/util/Map;

    move-result-object v1

    .line 132
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/orderhub/master/Filter;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->intValue()I

    move-result v2

    .line 29
    iget-object v4, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->res:Lcom/squareup/util/Res;

    invoke-virtual {v3, v4}, Lcom/squareup/ui/orderhub/master/Filter;->getGroupName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v4

    .line 30
    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 31
    iget-object v5, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->filterList:Ljava/util/ArrayList;

    .line 32
    new-instance v6, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;

    .line 33
    sget-object v7, Lcom/squareup/ui/orderhub/master/Filter;->Companion:Lcom/squareup/ui/orderhub/master/Filter$Companion;

    invoke-virtual {v7}, Lcom/squareup/ui/orderhub/master/Filter$Companion;->getDEFAULT()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v7

    const/4 v8, 0x0

    .line 32
    invoke-direct {v6, v7, v4, v8, v8}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;-><init>(Lcom/squareup/ui/orderhub/master/Filter;Ljava/lang/String;IZ)V

    .line 31
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x1

    .line 39
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    :cond_0
    iget-object v4, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->filterList:Ljava/util/ArrayList;

    .line 42
    new-instance v5, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;

    const/4 v6, 0x0

    .line 46
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailScreen;->getData()Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v7

    invoke-virtual {v7}, Lcom/squareup/ui/orderhub/OrderHubState;->getMasterState()Lcom/squareup/ui/orderhub/MasterState;

    move-result-object v7

    invoke-virtual {v7}, Lcom/squareup/ui/orderhub/MasterState;->getSelectedFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v7

    invoke-static {v7, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    .line 42
    invoke-direct {v5, v3, v6, v2, v7}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;-><init>(Lcom/squareup/ui/orderhub/master/Filter;Ljava/lang/String;IZ)V

    .line 41
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 50
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->notifyDataSetChanged()V

    return-void
.end method
