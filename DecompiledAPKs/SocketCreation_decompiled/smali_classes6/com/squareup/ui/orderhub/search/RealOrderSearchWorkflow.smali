.class public final Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;
.super Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;
.source "SearchWorkflow.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/OrderHubAppletScope;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Action;,
        Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSearchWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SearchWorkflow.kt\ncom/squareup/ui/orderhub/search/RealOrderSearchWorkflow\n+ 2 RxWorkers.kt\ncom/squareup/workflow/rx2/RxWorkersKt\n+ 3 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n1#1,272:1\n41#2:273\n56#2,2:274\n276#3:276\n*E\n*S KotlinDebug\n*F\n+ 1 SearchWorkflow.kt\ncom/squareup/ui/orderhub/search/RealOrderSearchWorkflow\n*L\n135#1:273\n135#1,2:274\n135#1:276\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 \u001f2\u00020\u0001:\u0002\u001e\u001fB)\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u001a\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J \u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0015\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u000fH\u0016J,\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u000f2\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u001c0\u001bH\u0016J\u0010\u0010\u001d\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u000fH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;",
        "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;",
        "orderRepository",
        "Lcom/squareup/ordermanagerdata/OrderRepository;",
        "activityAppletGateway",
        "Lcom/squareup/ui/activity/ActivityAppletGateway;",
        "debounceSearchMs",
        "",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/activity/ActivityAppletGateway;JLcom/squareup/settings/server/AccountStatusSettings;)V",
        "searchRangeDays",
        "",
        "searchRangeMillis",
        "initialState",
        "Lcom/squareup/ui/orderhub/search/SearchState;",
        "props",
        "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "onPropsChanged",
        "old",
        "new",
        "state",
        "render",
        "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "snapshotState",
        "Action",
        "Companion",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Companion;

.field public static final FALLBACK_SEARCH_MILLIS:J = 0x9a7ec800L

.field public static final MILLIS_PER_DAY:J = 0x5265c00L

.field private static final SEARCH_INITIAL_STATE:Lcom/squareup/ui/orderhub/search/SearchState;

.field public static final SEARCH_ORDERS_DEBOUNCE_KEY:Ljava/lang/String; = "search_orders-debounce"

.field public static final SEARCH_ORDERS_WORKER_KEY_A:Ljava/lang/String; = "a-search-orders"

.field public static final SEARCH_ORDERS_WORKER_KEY_B:Ljava/lang/String; = "b-search-orders"


# instance fields
.field private final activityAppletGateway:Lcom/squareup/ui/activity/ActivityAppletGateway;

.field private final debounceSearchMs:J

.field private final orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

.field private final searchRangeDays:I

.field private final searchRangeMillis:J


# direct methods
.method static constructor <clinit>()V
    .locals 10

    new-instance v0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->Companion:Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$Companion;

    .line 267
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v8

    .line 261
    new-instance v0, Lcom/squareup/ui/orderhub/search/SearchState;

    const/4 v3, 0x0

    const-string v4, ""

    const-string v5, ""

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v9}, Lcom/squareup/ui/orderhub/search/SearchState;-><init>(ZLjava/lang/String;Ljava/lang/String;ZZLjava/util/List;Z)V

    sput-object v0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->SEARCH_INITIAL_STATE:Lcom/squareup/ui/orderhub/search/SearchState;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/activity/ActivityAppletGateway;JLcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .param p3    # J
        .annotation runtime Lcom/squareup/ui/orderhub/SearchDebounceRate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderRepository"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityAppletGateway"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->activityAppletGateway:Lcom/squareup/ui/activity/ActivityAppletGateway;

    iput-wide p3, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->debounceSearchMs:J

    .line 87
    invoke-virtual {p5}, Lcom/squareup/settings/server/AccountStatusSettings;->getOrderHubSettings()Lcom/squareup/settings/server/OrderHubSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/OrderHubSettings;->getThrottleOrdersSearch()Z

    move-result p1

    const-wide p2, 0x9a7ec800L

    if-eqz p1, :cond_0

    .line 88
    invoke-virtual {p5}, Lcom/squareup/settings/server/AccountStatusSettings;->getOrderHubSettings()Lcom/squareup/settings/server/OrderHubSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/OrderHubSettings;->getOrdersSearchRangeThrottled()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    goto :goto_0

    .line 90
    :cond_0
    invoke-virtual {p5}, Lcom/squareup/settings/server/AccountStatusSettings;->getOrderHubSettings()Lcom/squareup/settings/server/OrderHubSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/OrderHubSettings;->getOrdersSearchRange()Ljava/lang/Long;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    .line 87
    :cond_1
    :goto_0
    iput-wide p2, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->searchRangeMillis:J

    .line 92
    iget-wide p1, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->searchRangeMillis:J

    const-wide/32 p3, 0x5265c00

    div-long/2addr p1, p3

    long-to-int p2, p1

    iput p2, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->searchRangeDays:I

    return-void
.end method

.method public static final synthetic access$getActivityAppletGateway$p(Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;)Lcom/squareup/ui/activity/ActivityAppletGateway;
    .locals 0

    .line 78
    iget-object p0, p0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->activityAppletGateway:Lcom/squareup/ui/activity/ActivityAppletGateway;

    return-object p0
.end method

.method public static final synthetic access$getSEARCH_INITIAL_STATE$cp()Lcom/squareup/ui/orderhub/search/SearchState;
    .locals 1

    .line 78
    sget-object v0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->SEARCH_INITIAL_STATE:Lcom/squareup/ui/orderhub/search/SearchState;

    return-object v0
.end method


# virtual methods
.method public initialState(Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/search/SearchState;
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    sget-object p1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->SEARCH_INITIAL_STATE:Lcom/squareup/ui/orderhub/search/SearchState;

    return-object p1
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->initialState(Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/orderhub/search/SearchState;

    move-result-object p1

    return-object p1
.end method

.method public onPropsChanged(Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;Lcom/squareup/ui/orderhub/search/SearchState;)Lcom/squareup/ui/orderhub/search/SearchState;
    .locals 1

    const-string v0, "old"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "new"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "state"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-object p3

    .line 105
    :cond_0
    sget-object p1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->SEARCH_INITIAL_STATE:Lcom/squareup/ui/orderhub/search/SearchState;

    return-object p1
.end method

.method public bridge synthetic onPropsChanged(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;

    check-cast p2, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;

    check-cast p3, Lcom/squareup/ui/orderhub/search/SearchState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->onPropsChanged(Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;Lcom/squareup/ui/orderhub/search/SearchState;)Lcom/squareup/ui/orderhub/search/SearchState;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;Lcom/squareup/ui/orderhub/search/SearchState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;",
            "Lcom/squareup/ui/orderhub/search/SearchState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/orderhub/search/SearchState;",
            "-",
            "Lcom/squareup/ui/orderhub/OrderHubResult;",
            ">;)",
            "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    const-string v4, "props"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "state"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "context"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->isInSearchContext()Z

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getSearchText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getDispatchedSearchText()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    xor-int/2addr v4, v5

    if-eqz v4, :cond_0

    .line 119
    sget-object v6, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    iget-wide v7, v0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->debounceSearchMs:J

    const/4 v9, 0x0

    const/4 v10, 0x2

    const/4 v11, 0x0

    invoke-static/range {v6 .. v11}, Lcom/squareup/workflow/Worker$Companion;->timer$default(Lcom/squareup/workflow/Worker$Companion;JLjava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/Worker;

    move-result-object v4

    .line 120
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "search_orders-debounce"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getSearchText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 121
    new-instance v7, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$1;

    invoke-direct {v7, v2}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$1;-><init>(Lcom/squareup/ui/orderhub/search/SearchState;)V

    check-cast v7, Lkotlin/jvm/functions/Function1;

    .line 118
    invoke-interface {v3, v4, v6, v7}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    .line 131
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->isInSearchContext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getDispatchedSearchText()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v4}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    xor-int/2addr v4, v5

    if-eqz v4, :cond_3

    .line 133
    iget-object v4, v0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    .line 134
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;->getFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getDispatchedSearchText()Ljava/lang/String;

    move-result-object v7

    iget-wide v8, v0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->searchRangeMillis:J

    invoke-static {v6, v7, v8, v9}, Lcom/squareup/ui/orderhub/master/FilterKt;->getSearchQuery(Lcom/squareup/ui/orderhub/master/Filter;Ljava/lang/String;J)Lcom/squareup/ordermanagerdata/SearchQuery;

    move-result-object v6

    .line 133
    invoke-interface {v4, v6}, Lcom/squareup/ordermanagerdata/OrderRepository;->searchOrders(Lcom/squareup/ordermanagerdata/SearchQuery;)Lio/reactivex/Observable;

    move-result-object v4

    .line 273
    sget-object v6, Lio/reactivex/BackpressureStrategy;->BUFFER:Lio/reactivex/BackpressureStrategy;

    invoke-virtual {v4, v6}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object v4

    const-string v6, "this.toFlowable(BUFFER)"

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lorg/reactivestreams/Publisher;

    if-eqz v4, :cond_2

    .line 275
    invoke-static {v4}, Lkotlinx/coroutines/reactive/ReactiveFlowKt;->asFlow(Lorg/reactivestreams/Publisher;)Lkotlinx/coroutines/flow/Flow;

    move-result-object v4

    .line 276
    const-class v6, Lcom/squareup/ordermanagerdata/ResultState;

    sget-object v7, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v8, Ljava/util/List;

    sget-object v9, Lkotlin/reflect/KTypeProjection;->Companion:Lkotlin/reflect/KTypeProjection$Companion;

    const-class v10, Lcom/squareup/orders/model/Order;

    invoke-static {v10}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;)Lkotlin/reflect/KType;

    move-result-object v10

    invoke-virtual {v9, v10}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v9

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v8

    invoke-virtual {v7, v8}, Lkotlin/reflect/KTypeProjection$Companion;->invariant(Lkotlin/reflect/KType;)Lkotlin/reflect/KTypeProjection;

    move-result-object v7

    invoke-static {v6, v7}, Lkotlin/jvm/internal/Reflection;->typeOf(Ljava/lang/Class;Lkotlin/reflect/KTypeProjection;)Lkotlin/reflect/KType;

    move-result-object v6

    new-instance v7, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v7, v6, v4}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v7, Lcom/squareup/workflow/Worker;

    .line 138
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getRedoSearchSwitch()Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "a-search-orders"

    goto :goto_0

    :cond_1
    const-string v6, "b-search-orders"

    :goto_0
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getDispatchedSearchText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 141
    new-instance v6, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$2;

    invoke-direct {v6, v1}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$2;-><init>(Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;)V

    check-cast v6, Lkotlin/jvm/functions/Function1;

    .line 132
    invoke-interface {v3, v7, v4, v6}, Lcom/squareup/workflow/RenderContext;->runningWorker(Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V

    goto :goto_1

    .line 275
    :cond_2
    new-instance v1, Lkotlin/TypeCastException;

    const-string v2, "null cannot be cast to non-null type org.reactivestreams.Publisher<T>"

    invoke-direct {v1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 151
    :cond_3
    :goto_1
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getSearchText()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$searchEditText$1;->INSTANCE:Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$searchEditText$1;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const-string v6, "search_text"

    invoke-static {v3, v1, v6, v4}, Lcom/squareup/workflow/text/RenderContextsKt;->renderEditText(Lcom/squareup/workflow/RenderContext;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v11

    .line 156
    new-instance v1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$onChangeSearchFocus$1;

    invoke-direct {v1, v3}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$onChangeSearchFocus$1;-><init>(Lcom/squareup/workflow/RenderContext;)V

    move-object v13, v1

    check-cast v13, Lkotlin/jvm/functions/Function1;

    .line 159
    new-instance v1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$onPressedEnter$1;

    invoke-direct {v1, v3}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$onPressedEnter$1;-><init>(Lcom/squareup/workflow/RenderContext;)V

    move-object v14, v1

    check-cast v14, Lkotlin/jvm/functions/Function0;

    .line 162
    new-instance v1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$onClickedTransactionsLink$1;

    invoke-direct {v1, v0}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$onClickedTransactionsLink$1;-><init>(Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;)V

    move-object v15, v1

    check-cast v15, Lkotlin/jvm/functions/Function0;

    .line 165
    new-instance v1, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$onClickedRetrySearch$1;

    invoke-direct {v1, v3}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow$render$onClickedRetrySearch$1;-><init>(Lcom/squareup/workflow/RenderContext;)V

    move-object/from16 v16, v1

    check-cast v16, Lkotlin/jvm/functions/Function0;

    .line 168
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->isInSearchContext()Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->NOT_SEARCHING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    :goto_2
    move-object v9, v1

    goto :goto_4

    .line 169
    :cond_4
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->isSearching()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->LOADING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    goto :goto_2

    .line 170
    :cond_5
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getDispatchedSearchText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getSearchText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v5

    if-eqz v1, :cond_6

    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->TYPING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    goto :goto_2

    .line 171
    :cond_6
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getDispatchedSearchText()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_7

    const/4 v1, 0x1

    goto :goto_3

    :cond_7
    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_8

    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->SHOW_INFO_TEXT:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    goto :goto_2

    .line 172
    :cond_8
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getSearchError()Z

    move-result v1

    if-eqz v1, :cond_9

    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->ERROR:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    goto :goto_2

    .line 173
    :cond_9
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getSearchResults()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_a

    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->NO_SEARCH_RESULTS:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    goto :goto_2

    .line 174
    :cond_a
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getSearchResults()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    xor-int/2addr v1, v5

    if-eqz v1, :cond_b

    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->SHOW_RESULTS:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    goto :goto_2

    .line 175
    :cond_b
    sget-object v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;->NOT_SEARCHING:Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;

    goto :goto_2

    .line 178
    :goto_4
    new-instance v1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    const/4 v8, 0x1

    .line 181
    iget v10, v0, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->searchRangeDays:I

    .line 183
    invoke-virtual/range {p2 .. p2}, Lcom/squareup/ui/orderhub/search/SearchState;->getSearchResults()Ljava/util/List;

    move-result-object v12

    move-object v7, v1

    .line 178
    invoke-direct/range {v7 .. v16}, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;-><init>(ZLcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering$SearchDisplayState;ILcom/squareup/workflow/text/WorkflowEditableText;Ljava/util/List;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    return-object v1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;

    check-cast p2, Lcom/squareup/ui/orderhub/search/SearchState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->render(Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$Props;Lcom/squareup/ui/orderhub/search/SearchState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow$SearchRendering;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lcom/squareup/ui/orderhub/search/SearchState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/ui/orderhub/search/SearchState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/search/RealOrderSearchWorkflow;->snapshotState(Lcom/squareup/ui/orderhub/search/SearchState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
