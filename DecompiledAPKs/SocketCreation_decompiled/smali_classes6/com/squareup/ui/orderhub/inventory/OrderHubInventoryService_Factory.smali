.class public final Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService_Factory;
.super Ljava/lang/Object;
.source "OrderHubInventoryService_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;",
        ">;"
    }
.end annotation


# instance fields
.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final inventoryServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/inventory/InventoryService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/inventory/InventoryService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService_Factory;->inventoryServiceProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService_Factory;->cogsProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/inventory/InventoryService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/inventory/InventoryService;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;-><init>(Lcom/squareup/server/inventory/InventoryService;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService_Factory;->inventoryServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/inventory/InventoryService;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cogs/Cogs;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService_Factory;->newInstance(Lcom/squareup/server/inventory/InventoryService;Lcom/squareup/cogs/Cogs;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService_Factory;->get()Lcom/squareup/ui/orderhub/inventory/OrderHubInventoryService;

    move-result-object v0

    return-object v0
.end method
