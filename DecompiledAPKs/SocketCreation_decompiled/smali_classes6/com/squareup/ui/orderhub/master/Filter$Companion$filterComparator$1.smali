.class final Lcom/squareup/ui/orderhub/master/Filter$Companion$filterComparator$1;
.super Ljava/lang/Object;
.source "Filter.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/master/Filter$Companion;->filterComparator(Lcom/squareup/util/Res;)Ljava/util/Comparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/ui/orderhub/master/Filter;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "first",
        "Lcom/squareup/ui/orderhub/master/Filter;",
        "kotlin.jvm.PlatformType",
        "second",
        "compare"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/master/Filter$Companion$filterComparator$1;->$res:Lcom/squareup/util/Res;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Lcom/squareup/ui/orderhub/master/Filter;Lcom/squareup/ui/orderhub/master/Filter;)I
    .locals 1

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/Filter$Companion$filterComparator$1;->$res:Lcom/squareup/util/Res;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/orderhub/master/Filter;->getFilterName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/Filter$Companion$filterComparator$1;->$res:Lcom/squareup/util/Res;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/orderhub/master/Filter;->getFilterName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 116
    check-cast p1, Lcom/squareup/ui/orderhub/master/Filter;

    check-cast p2, Lcom/squareup/ui/orderhub/master/Filter;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/master/Filter$Companion$filterComparator$1;->compare(Lcom/squareup/ui/orderhub/master/Filter;Lcom/squareup/ui/orderhub/master/Filter;)I

    move-result p1

    return p1
.end method
