.class public final Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealOrderHubWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final currentTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final orderDetailsWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final orderMarkShippedWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final orderQuickActionsEnabledPreferenceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final orderRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final searchWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final uuidGeneratorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/UUIDGenerator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/UUIDGenerator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;)V"
        }
    .end annotation

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->orderDetailsWorkflowProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p2, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->orderMarkShippedWorkflowProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p3, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->searchWorkflowProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p4, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->resProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p5, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p6, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p7, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p8, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->orderHubAnalyticsProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p9, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->uuidGeneratorProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p10, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p11, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p12, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->orderQuickActionsEnabledPreferenceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/UUIDGenerator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;)",
            "Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;"
        }
    .end annotation

    .line 90
    new-instance v13, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v13
.end method

.method public static newInstance(Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/log/UUIDGenerator;Lcom/squareup/time/CurrentTime;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/f2prateek/rx/preferences2/Preference;)Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;",
            "Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            "Lcom/squareup/log/UUIDGenerator;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;"
        }
    .end annotation

    .line 100
    new-instance v13, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    move-object v0, v13

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;-><init>(Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/log/UUIDGenerator;Lcom/squareup/time/CurrentTime;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/f2prateek/rx/preferences2/Preference;)V

    return-object v13
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;
    .locals 13

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->orderDetailsWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->orderMarkShippedWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->searchWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ordermanagerdata/OrderRepository;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->orderHubAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->uuidGeneratorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/log/UUIDGenerator;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/time/CurrentTime;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->orderQuickActionsEnabledPreferenceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static/range {v1 .. v12}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->newInstance(Lcom/squareup/ui/orderhub/order/OrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;Lcom/squareup/ui/orderhub/search/OrderSearchWorkflow;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/log/UUIDGenerator;Lcom/squareup/time/CurrentTime;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/f2prateek/rx/preferences2/Preference;)Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow_Factory;->get()Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    move-result-object v0

    return-object v0
.end method
