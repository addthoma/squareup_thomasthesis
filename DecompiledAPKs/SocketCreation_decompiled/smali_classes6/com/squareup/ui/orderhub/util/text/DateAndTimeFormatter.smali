.class public final Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;
.super Ljava/lang/Object;
.source "DateAndTimeFormatter.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/dagger/AppScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDateAndTimeFormatter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DateAndTimeFormatter.kt\ncom/squareup/ui/orderhub/util/text/DateAndTimeFormatter\n*L\n1#1,64:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\u0008J\u0016\u0010\n\u001a\u00020\u00082\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eJ\u000e\u0010\u000f\u001a\u00020\u000c2\u0006\u0010\t\u001a\u00020\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
        "",
        "currentTimeZone",
        "Lcom/squareup/time/CurrentTimeZone;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/util/Res;)V",
        "format",
        "",
        "dateTimeString",
        "formatDateAndTime",
        "dateTime",
        "Lorg/threeten/bp/ZonedDateTime;",
        "includeDate",
        "",
        "parse",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/time/CurrentTimeZone;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "currentTimeZone"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public final format(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 39
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    const-string p1, ""

    goto :goto_2

    .line 42
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_relative_date_time_format:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->parse(Ljava/lang/String;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object p1

    invoke-static {v0}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/threeten/bp/ZonedDateTime;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "parse(dateTimeString).fo\u2026atter.ofPattern(pattern))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_2
    return-object p1
.end method

.method public final formatDateAndTime(Lorg/threeten/bp/ZonedDateTime;Z)Ljava/lang/String;
    .locals 1

    const-string v0, "dateTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 56
    iget-object p2, p0, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_date_format_month_day_and_time:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 58
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_date_format_hour_minute_time:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 61
    :goto_0
    invoke-static {p2}, Lorg/threeten/bp/format/DateTimeFormatter;->ofPattern(Ljava/lang/String;)Lorg/threeten/bp/format/DateTimeFormatter;

    move-result-object p2

    invoke-virtual {p1, p2}, Lorg/threeten/bp/ZonedDateTime;->format(Lorg/threeten/bp/format/DateTimeFormatter;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "dateTime.format(DateTime\u2026atter.ofPattern(pattern))"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final parse(Ljava/lang/String;)Lorg/threeten/bp/ZonedDateTime;
    .locals 3

    const-string v0, "dateTimeString"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lorg/threeten/bp/ZonedDateTime;->parse(Ljava/lang/CharSequence;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    .line 23
    iget-object v1, p0, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->currentTimeZone:Lcom/squareup/time/CurrentTimeZone;

    invoke-interface {v1}, Lcom/squareup/time/CurrentTimeZone;->zoneId()Lorg/threeten/bp/ZoneId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/threeten/bp/ZonedDateTime;->withZoneSameInstant(Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    const-string v1, "ZonedDateTime.parse(date\u2026currentTimeZone.zoneId())"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/threeten/bp/format/DateTimeParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 25
    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 26
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attempting to format invalid zonedDateTime string on the "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "order: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 p1, 0x2e

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 25
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
