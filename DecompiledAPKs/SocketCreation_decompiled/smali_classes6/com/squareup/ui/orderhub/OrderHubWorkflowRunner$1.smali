.class final synthetic Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$1;
.super Lkotlin/jvm/internal/FunctionReference;
.source "OrderHubWorkflowRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner;-><init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;Lcom/squareup/ui/orderhub/OrderHubApplet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;",
        "Lcom/squareup/ui/orderhub/OrderHubViewFactory;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/orderhub/OrderHubViewFactory;",
        "p1",
        "Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$1;

    invoke-direct {v0}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$1;->INSTANCE:Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/FunctionReference;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "viewFactory"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "viewFactory()Lcom/squareup/ui/orderhub/OrderHubViewFactory;"

    return-object v0
.end method

.method public final invoke(Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;)Lcom/squareup/ui/orderhub/OrderHubViewFactory;
    .locals 1

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    invoke-interface {p1}, Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;->viewFactory()Lcom/squareup/ui/orderhub/OrderHubViewFactory;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/OrderHubWorkflowRunner$1;->invoke(Lcom/squareup/ui/orderhub/OrderHubAppletScope$Component;)Lcom/squareup/ui/orderhub/OrderHubViewFactory;

    move-result-object p1

    return-object p1
.end method
