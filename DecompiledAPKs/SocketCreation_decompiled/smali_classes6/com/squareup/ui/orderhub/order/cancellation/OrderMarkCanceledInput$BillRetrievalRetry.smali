.class public final Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;
.super Ljava/lang/Object;
.source "OrderMarkCanceledInput.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BillRetrievalRetry"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B9\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012*\u0010\u0004\u001a&\u0012\u0004\u0012\u00020\u0006\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00080\u0005j\u0002`\t0\u0005j\u0002`\n\u00a2\u0006\u0002\u0010\u000bJ\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J-\u0010\u0011\u001a&\u0012\u0004\u0012\u00020\u0006\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00080\u0005j\u0002`\t0\u0005j\u0002`\nH\u00c6\u0003JA\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032,\u0008\u0002\u0010\u0004\u001a&\u0012\u0004\u0012\u00020\u0006\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00080\u0005j\u0002`\t0\u0005j\u0002`\nH\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0006H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR5\u0010\u0004\u001a&\u0012\u0004\u0012\u00020\u0006\u0012\u0018\u0012\u0016\u0012\u0008\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00080\u0005j\u0002`\t0\u0005j\u0002`\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;",
        "",
        "cancellationReason",
        "Lcom/squareup/ordermanagerdata/CancellationReason;",
        "selectedLineItems",
        "",
        "",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemRowIdentifier;",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemWithQuantityByIdentifier;",
        "Lcom/squareup/ui/orderhub/order/itemselection/LineItemSelectionsByUid;",
        "(Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)V",
        "getCancellationReason",
        "()Lcom/squareup/ordermanagerdata/CancellationReason;",
        "getSelectedLineItems",
        "()Ljava/util/Map;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

.field private final selectedLineItems:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/CancellationReason;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "cancellationReason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedLineItems"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->selectedLineItems:Ljava/util/Map;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->selectedLineItems:Ljava/util/Map;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->copy(Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ordermanagerdata/CancellationReason;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    return-object v0
.end method

.method public final component2()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->selectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public final copy(Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/CancellationReason;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;)",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;"
        }
    .end annotation

    const-string v0, "cancellationReason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "selectedLineItems"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;-><init>(Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->selectedLineItems:Ljava/util/Map;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->selectedLineItems:Ljava/util/Map;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    return-object v0
.end method

.method public final getSelectedLineItems()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;>;"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->selectedLineItems:Ljava/util/Map;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->selectedLineItems:Ljava/util/Map;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BillRetrievalRetry(cancellationReason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->cancellationReason:Lcom/squareup/ordermanagerdata/CancellationReason;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", selectedLineItems="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->selectedLineItems:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
