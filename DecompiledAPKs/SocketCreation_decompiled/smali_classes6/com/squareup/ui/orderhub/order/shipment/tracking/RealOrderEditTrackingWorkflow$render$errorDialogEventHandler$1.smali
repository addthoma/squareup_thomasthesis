.class final Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$errorDialogEventHandler$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderEditTrackingWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow;->render(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingInput;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderEditTrackingWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderEditTrackingWorkflow.kt\ncom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$errorDialogEventHandler$1\n*L\n1#1,141:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
        "event",
        "Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$errorDialogEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    instance-of v0, p1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelSaveTracking;

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$errorDialogEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xef

    const/4 v10, 0x0

    invoke-static/range {v0 .. v10}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->copy$default(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;Lcom/squareup/orders/model/Order;ZZZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/ordermanagerdata/TrackingInfo;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 94
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetrySaveTracking;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult$EditTrackingComplete;

    check-cast p1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetrySaveTracking;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetrySaveTracking;->getTracking()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult$EditTrackingComplete;-><init>(Lcom/squareup/ordermanagerdata/TrackingInfo;)V

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 95
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryFulfillmentAction;

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 96
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult$EditTrackingComplete;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$errorDialogEventHandler$1;->$state:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->getDisplayTrackingInfo()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult$EditTrackingComplete;-><init>(Lcom/squareup/ordermanagerdata/TrackingInfo;)V

    .line 95
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    .line 98
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected error dialog event "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/RealOrderEditTrackingWorkflow$render$errorDialogEventHandler$1;->invoke(Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
