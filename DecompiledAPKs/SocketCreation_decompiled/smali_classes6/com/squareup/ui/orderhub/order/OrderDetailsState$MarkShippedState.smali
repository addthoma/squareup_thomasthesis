.class public final Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;
.super Lcom/squareup/ui/orderhub/order/OrderDetailsState;
.source "OrderDetailsState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/OrderDetailsState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MarkShippedState"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0013\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B?\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\u000cJ\t\u0010\u0016\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0018\u001a\u00020\u0006H\u00c6\u0003J\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003JI\u0010\u001c\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00032\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00c6\u0001J\u0013\u0010\u001d\u001a\u00020\u00032\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u00d6\u0003J\t\u0010 \u001a\u00020!H\u00d6\u0001J\t\u0010\"\u001a\u00020#H\u00d6\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0002\u0010\rR\u0014\u0010\u0005\u001a\u00020\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0014\u0010\u0004\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\rR\u0014\u0010\u0007\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\r\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "isReadOnly",
        "",
        "showOrderIdInActionBar",
        "order",
        "Lcom/squareup/orders/model/Order;",
        "showSpinner",
        "orderUpdateFailureState",
        "Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
        "shipAction",
        "Lcom/squareup/protos/client/orders/Action;",
        "(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;)V",
        "()Z",
        "getOrder",
        "()Lcom/squareup/orders/model/Order;",
        "getOrderUpdateFailureState",
        "()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;",
        "getShipAction",
        "()Lcom/squareup/protos/client/orders/Action;",
        "getShowOrderIdInActionBar",
        "getShowSpinner",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final isReadOnly:Z

.field private final order:Lcom/squareup/orders/model/Order;

.field private final orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

.field private final shipAction:Lcom/squareup/protos/client/orders/Action;

.field private final showOrderIdInActionBar:Z

.field private final showSpinner:Z


# direct methods
.method public constructor <init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;)V
    .locals 7

    const-string v0, "order"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v6, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    .line 38
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-boolean p1, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->isReadOnly:Z

    iput-boolean p2, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->showOrderIdInActionBar:Z

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->order:Lcom/squareup/orders/model/Order;

    iput-boolean p4, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->showSpinner:Z

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->shipAction:Lcom/squareup/protos/client/orders/Action;

    return-void
.end method

.method public synthetic constructor <init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x8

    if-eqz p8, :cond_0

    const/4 p4, 0x0

    const/4 v4, 0x0

    goto :goto_0

    :cond_0
    move v4, p4

    :goto_0
    and-int/lit8 p4, p7, 0x10

    const/4 p8, 0x0

    if-eqz p4, :cond_1

    .line 36
    move-object p5, p8

    check-cast p5, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    :cond_1
    move-object v5, p5

    and-int/lit8 p4, p7, 0x20

    if-eqz p4, :cond_2

    .line 37
    move-object p6, p8

    check-cast p6, Lcom/squareup/protos/client/orders/Action;

    :cond_2
    move-object v6, p6

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;
    .locals 4

    and-int/lit8 p8, p7, 0x1

    if-eqz p8, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->isReadOnly()Z

    move-result p1

    :cond_0
    and-int/lit8 p8, p7, 0x2

    if-eqz p8, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowOrderIdInActionBar()Z

    move-result p2

    :cond_1
    move p8, p2

    and-int/lit8 p2, p7, 0x4

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p3

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p7, 0x8

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowSpinner()Z

    move-result p4

    :cond_3
    move v1, p4

    and-int/lit8 p2, p7, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p7, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->shipAction:Lcom/squareup/protos/client/orders/Action;

    :cond_5
    move-object v3, p6

    move-object p2, p0

    move p3, p1

    move p4, p8

    move-object p5, v0

    move p6, v1

    move-object p7, v2

    move-object p8, v3

    invoke-virtual/range {p2 .. p8}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->copy(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->isReadOnly()Z

    move-result v0

    return v0
.end method

.method public final component2()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowOrderIdInActionBar()Z

    move-result v0

    return v0
.end method

.method public final component3()Lcom/squareup/orders/model/Order;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowSpinner()Z

    move-result v0

    return v0
.end method

.method public final component5()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    return-object v0
.end method

.method public final component6()Lcom/squareup/protos/client/orders/Action;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->shipAction:Lcom/squareup/protos/client/orders/Action;

    return-object v0
.end method

.method public final copy(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;
    .locals 8

    const-string v0, "order"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->isReadOnly()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->isReadOnly()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowOrderIdInActionBar()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowOrderIdInActionBar()Z

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowSpinner()Z

    move-result v0

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowSpinner()Z

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    iget-object v1, p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->shipAction:Lcom/squareup/protos/client/orders/Action;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->shipAction:Lcom/squareup/protos/client/orders/Action;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getOrder()Lcom/squareup/orders/model/Order;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->order:Lcom/squareup/orders/model/Order;

    return-object v0
.end method

.method public final getOrderUpdateFailureState()Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    return-object v0
.end method

.method public final getShipAction()Lcom/squareup/protos/client/orders/Action;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->shipAction:Lcom/squareup/protos/client/orders/Action;

    return-object v0
.end method

.method public getShowOrderIdInActionBar()Z
    .locals 1

    .line 33
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->showOrderIdInActionBar:Z

    return v0
.end method

.method public getShowSpinner()Z
    .locals 1

    .line 35
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->showSpinner:Z

    return v0
.end method

.method public hashCode()I
    .locals 4

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->isReadOnly()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :cond_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowOrderIdInActionBar()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowSpinner()Z

    move-result v2

    if-eqz v2, :cond_3

    goto :goto_1

    :cond_3
    move v1, v2

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->shipAction:Lcom/squareup/protos/client/orders/Action;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v3

    :cond_5
    add-int/2addr v0, v3

    return v0
.end method

.method public isReadOnly()Z
    .locals 1

    .line 32
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->isReadOnly:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MarkShippedState(isReadOnly="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->isReadOnly()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", showOrderIdInActionBar="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowOrderIdInActionBar()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", showSpinner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->getShowSpinner()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", orderUpdateFailureState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->orderUpdateFailureState:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", shipAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$MarkShippedState;->shipAction:Lcom/squareup/protos/client/orders/Action;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
