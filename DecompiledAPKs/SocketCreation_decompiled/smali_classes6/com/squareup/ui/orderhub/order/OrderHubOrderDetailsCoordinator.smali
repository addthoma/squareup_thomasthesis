.class public final Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderHubOrderDetailsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$Factory;,
        Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubOrderDetailsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubOrderDetailsCoordinator.kt\ncom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,1080:1\n1099#2,2:1081\n1127#2,4:1083\n1642#2,2:1087\n1360#2:1096\n1429#2,3:1097\n1360#2:1100\n1429#2,3:1101\n2073#2,5:1104\n1642#2,2:1109\n704#2:1111\n777#2,2:1112\n1642#2,2:1114\n704#2:1116\n777#2,2:1117\n1642#2,2:1119\n1642#2,2:1121\n1642#2,2:1123\n1103#3,7:1089\n1103#3,7:1125\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubOrderDetailsCoordinator.kt\ncom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator\n*L\n233#1,2:1081\n233#1,4:1083\n236#1,2:1087\n471#1:1096\n471#1,3:1097\n475#1:1100\n475#1,3:1101\n478#1,5:1104\n490#1,2:1109\n736#1:1111\n736#1,2:1112\n737#1,2:1114\n750#1:1116\n750#1,2:1117\n751#1,2:1119\n807#1,2:1121\n851#1,2:1123\n429#1,7:1089\n1009#1,7:1125\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00e8\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0010\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0011\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001:\u0004\u0080\u0001\u0081\u0001B\u008f\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\"\u0010\u000b\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rj\u0008\u0012\u0004\u0012\u00020\u000e`\u00100\u000c\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u0012\u0006\u0010\u0015\u001a\u00020\u0016\u0012\u0006\u0010\u0017\u001a\u00020\u0018\u0012\u0006\u0010\u0019\u001a\u00020\u001a\u0012\u0006\u0010\u001b\u001a\u00020\u001c\u0012\u0006\u0010\u001d\u001a\u00020\u001e\u0012\u0006\u0010\u001f\u001a\u00020 \u00a2\u0006\u0002\u0010!JL\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020\u001e2\u0006\u0010@\u001a\u00020\u001e2\u0006\u0010A\u001a\u00020\u000e2\u0006\u0010B\u001a\u00020\'2\u0006\u0010C\u001a\u0002032\u0006\u0010D\u001a\u00020E2\u0012\u0010F\u001a\u000e\u0012\u0004\u0012\u000202\u0012\u0004\u0012\u00020H0GH\u0002J\u0018\u0010I\u001a\u00020>2\u0006\u0010J\u001a\u00020\'2\u0006\u0010C\u001a\u000203H\u0002J(\u0010K\u001a\u00020>2\u0006\u0010J\u001a\u00020\'2\u0006\u0010C\u001a\u0002032\u0006\u0010D\u001a\u00020E2\u0006\u0010L\u001a\u00020EH\u0002J\u0018\u0010M\u001a\u00020>2\u0006\u0010N\u001a\u00020\'2\u0006\u0010O\u001a\u00020PH\u0002J,\u0010Q\u001a\u00020>2\u0006\u0010R\u001a\u00020\'2\u0006\u0010S\u001a\u0002022\u0008\u0010T\u001a\u0004\u0018\u00010\u00062\u0008\u0008\u0002\u0010U\u001a\u00020\u001eH\u0002J\u0010\u0010V\u001a\u00020>2\u0006\u0010W\u001a\u00020XH\u0002J\u0010\u0010Y\u001a\u00020>2\u0006\u0010Z\u001a\u00020[H\u0002J\u0010\u0010\\\u001a\u00020>2\u0006\u0010]\u001a\u00020*H\u0016J\u0010\u0010^\u001a\u00020>2\u0006\u0010]\u001a\u00020*H\u0002J(\u0010_\u001a\u00020>2\u0006\u0010@\u001a\u00020\u001e2\u0006\u0010`\u001a\u00020.2\u0006\u0010a\u001a\u00020\u001e2\u0006\u0010A\u001a\u00020\u000eH\u0002J\u0018\u0010b\u001a\u00020>2\u0006\u0010J\u001a\u00020\'2\u0006\u0010C\u001a\u000203H\u0002J \u0010c\u001a\u00020>2\u0006\u0010@\u001a\u00020\u001e2\u0006\u0010`\u001a\u00020.2\u0006\u0010A\u001a\u00020\u000eH\u0002J\u0010\u0010d\u001a\u00020>2\u0006\u0010C\u001a\u000203H\u0002J \u0010e\u001a\u00020>2\u0006\u0010`\u001a\u00020.2\u0006\u0010@\u001a\u00020\u001e2\u0006\u0010f\u001a\u00020\'H\u0002J*\u0010g\u001a\u00020>2\u0006\u0010`\u001a\u00020.2\u0006\u0010C\u001a\u0002032\u0008\u0010h\u001a\u0004\u0018\u0001022\u0006\u0010A\u001a\u00020\u000eH\u0002J\u0018\u0010i\u001a\u00020>2\u0006\u0010`\u001a\u00020.2\u0006\u0010a\u001a\u00020\u001eH\u0002J\u0010\u0010j\u001a\u00020>2\u0006\u0010`\u001a\u00020.H\u0002J*\u0010k\u001a\u00020>2\u0006\u0010@\u001a\u00020\u001e2\u0006\u0010f\u001a\u00020\'2\u0008\u0010l\u001a\u0004\u0018\u00010m2\u0006\u0010A\u001a\u00020\u000eH\u0002J\u0018\u0010n\u001a\u00020>2\u0006\u0010`\u001a\u00020.2\u0006\u0010f\u001a\u00020\'H\u0002J(\u0010o\u001a\u00020>2\u0006\u0010@\u001a\u00020\u001e2\u0006\u0010f\u001a\u00020\'2\u0006\u0010l\u001a\u00020m2\u0006\u0010A\u001a\u00020\u000eH\u0002J\u0010\u0010p\u001a\u00020>2\u0006\u0010`\u001a\u00020.H\u0002J\u0010\u0010q\u001a\u00020>2\u0006\u0010`\u001a\u00020.H\u0002J0\u0010r\u001a\u00020>2\u0006\u0010J\u001a\u00020\'2\u0006\u0010?\u001a\u00020\u001e2\u0006\u0010@\u001a\u00020\u001e2\u0006\u0010C\u001a\u0002032\u0006\u0010A\u001a\u00020\u000eH\u0002J(\u0010s\u001a\u00020>2\u0006\u0010t\u001a\u00020\u001e2\u0006\u0010@\u001a\u00020\u001e2\u0006\u0010f\u001a\u00020\'2\u0006\u0010A\u001a\u00020\u000eH\u0002J\u0010\u0010u\u001a\u00020>2\u0006\u0010A\u001a\u00020\u000eH\u0002J\u000c\u0010v\u001a\u00020w*\u00020\'H\u0002J\u0018\u0010x\u001a\u0004\u0018\u000102*\u0002032\u0008\u0010h\u001a\u0004\u0018\u000102H\u0002J\u000c\u0010y\u001a\u00020P*\u00020HH\u0002J\u0014\u0010y\u001a\u00020P*\u00020H2\u0006\u0010z\u001a\u000202H\u0002J,\u0010{\u001a\u00020>*\u00020w2\u0008\u0008\u0002\u0010@\u001a\u00020\u001e2\u0006\u0010|\u001a\u0002022\u000c\u0010}\u001a\u0008\u0012\u0004\u0012\u00020>0~H\u0002J&\u0010\u007f\u001a\u00020>*\u00020w2\u0006\u0010@\u001a\u00020\u001e2\u0008\u0010l\u001a\u0004\u0018\u00010m2\u0006\u0010A\u001a\u00020\u000eH\u0002R\u000e\u0010\"\u001a\u00020#X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\'X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020*X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\'X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u000b\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u000f0\rj\u0008\u0012\u0004\u0012\u00020\u000e`\u00100\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\'X\u0082.\u00a2\u0006\u0002\n\u0000R\u0018\u0010-\u001a\u00020\u001e*\u00020.8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008/\u00100R\u001a\u00101\u001a\u0004\u0018\u000102*\u0002038BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u00084\u00105R\u001a\u00106\u001a\u0004\u0018\u000102*\u0002038BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u00087\u00105R\u0018\u00108\u001a\u00020\u001e*\u0002038BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u00088\u00109R\u0018\u0010:\u001a\u00020\u001e*\u0002038BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008:\u00109R\u001a\u0010;\u001a\u0004\u0018\u000102*\u0002038BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008<\u00105\u00a8\u0006\u0082\u0001"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "res",
        "Lcom/squareup/util/Res;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "dateTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
        "lineItemSubtitleFormatter",
        "Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "orderPrintingDispatcher",
        "Lcom/squareup/print/OrderPrintingDispatcher;",
        "printerStations",
        "Lcom/squareup/print/PrinterStations;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "badMaybeSquareDeviceCheck",
        "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
        "orderHubAnalytics",
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
        "emailAppAvailable",
        "",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;Lio/reactivex/Observable;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/time/CurrentTime;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;ZLio/reactivex/Scheduler;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "animator",
        "Lcom/squareup/widgets/SquareViewAnimator;",
        "container",
        "Landroid/view/ViewGroup;",
        "customerContainer",
        "mainView",
        "Landroid/view/View;",
        "paymentDetailsContainer",
        "tenderContainer",
        "canShowExternalLink",
        "Lcom/squareup/orders/model/Order;",
        "getCanShowExternalLink",
        "(Lcom/squareup/orders/model/Order;)Z",
        "fulfillmentTime",
        "",
        "Lcom/squareup/orders/model/Order$Fulfillment;",
        "getFulfillmentTime",
        "(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;",
        "fulfillmentTimeHeaderText",
        "getFulfillmentTimeHeaderText",
        "isFulfillmentTimeVisible",
        "(Lcom/squareup/orders/model/Order$Fulfillment;)Z",
        "isRecipientAddressVisible",
        "recipientAddressHeaderText",
        "getRecipientAddressHeaderText",
        "addFulfillment",
        "",
        "orderIsFinished",
        "isReadOnly",
        "screen",
        "fulfillmentsContainer",
        "fulfillment",
        "fulfillmentCount",
        "",
        "lineItemsByUid",
        "",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "addItemsSectionDetailHeader",
        "fulfillmentContainer",
        "addItemsSectionHeader",
        "lineItemQuantity",
        "addLineItemRow",
        "itemsContainer",
        "lineItemRow",
        "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;",
        "addPrice",
        "priceContainer",
        "label",
        "money",
        "isTotal",
        "addServiceChargePrice",
        "serviceCharge",
        "Lcom/squareup/orders/model/Order$ServiceCharge;",
        "addTender",
        "tender",
        "Lcom/squareup/protos/connect/v2/resources/Tender;",
        "attach",
        "view",
        "bindViews",
        "setupActionBar",
        "order",
        "showOrderIdInActionBar",
        "showReadOnlyNoteView",
        "updateActionButtons",
        "updateCustomer",
        "updateExternalLinkButton",
        "buttonContainer",
        "updateFulfillmentDetails",
        "fulfillmentTimeOverride",
        "updateId",
        "updatePaymentDetails",
        "updatePrimaryActionButton",
        "action",
        "Lcom/squareup/protos/client/orders/Action;",
        "updatePrintButton",
        "updateSecondaryActionButton",
        "updateStatus",
        "updateTenders",
        "updateTracking",
        "updateViewTransactionDetail",
        "shouldShow",
        "updateViews",
        "addSecondaryButton",
        "Lcom/squareup/noho/NohoButton;",
        "fulfillmentTimeText",
        "toLineItemRow",
        "quantity",
        "update",
        "buttonText",
        "onClick",
        "Lkotlin/Function0;",
        "updateWithAction",
        "Factory",
        "LineItemRow",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private animator:Lcom/squareup/widgets/SquareViewAnimator;

.field private final badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private container:Landroid/view/ViewGroup;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private customerContainer:Landroid/view/ViewGroup;

.field private final dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

.field private final emailAppAvailable:Z

.field private final lineItemSubtitleFormatter:Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private mainView:Landroid/view/View;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private paymentDetailsContainer:Landroid/view/ViewGroup;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private tenderContainer:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;Lio/reactivex/Observable;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/time/CurrentTime;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;ZLio/reactivex/Scheduler;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            "Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/time/CurrentTime;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            "Z",
            "Lio/reactivex/Scheduler;",
            ")V"
        }
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateTimeFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lineItemSubtitleFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screens"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderPrintingDispatcher"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printerStations"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "browserLauncher"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badMaybeSquareDeviceCheck"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubAnalytics"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->lineItemSubtitleFormatter:Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->printerStations:Lcom/squareup/print/PrinterStations;

    iput-object p8, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p9, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    iput-object p10, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    iput-object p11, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    iput-boolean p12, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->emailAppAvailable:Z

    iput-object p13, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method public static final synthetic access$getBrowserLauncher$p(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;)Lcom/squareup/util/BrowserLauncher;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-object p0
.end method

.method public static final synthetic access$getCurrentTime$p(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;)Lcom/squareup/time/CurrentTime;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    return-object p0
.end method

.method public static final synthetic access$getDateTimeFormatter$p(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;)Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    return-object p0
.end method

.method public static final synthetic access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getOrderPrintingDispatcher$p(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;)Lcom/squareup/print/OrderPrintingDispatcher;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 108
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$updateViews(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V
    .locals 0

    .line 108
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateViews(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V

    return-void
.end method

.method private final addFulfillment(ZZLcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;Landroid/view/ViewGroup;Lcom/squareup/orders/model/Order$Fulfillment;ILjava/util/Map;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/orders/model/Order$Fulfillment;",
            "I",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;)V"
        }
    .end annotation

    move-object v6, p0

    move-object v0, p4

    move-object/from16 v4, p5

    .line 464
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 466
    sget v2, Lcom/squareup/orderhub/applet/R$layout;->orderhub_order_fulfillment_view:I

    const/4 v3, 0x0

    .line 465
    invoke-virtual {v1, v2, p4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_7

    check-cast v1, Landroid/view/ViewGroup;

    .line 468
    move-object v2, v1

    check-cast v2, Landroid/view/View;

    invoke-virtual {p4, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 470
    iget-object v0, v4, Lcom/squareup/orders/model/Order$Fulfillment;->line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    const/16 v2, 0xa

    const/4 v5, 0x1

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v7, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ordinal()I

    move-result v0

    aget v0, v7, v0

    if-eq v0, v5, :cond_2

    .line 475
    :goto_0
    invoke-interface/range {p7 .. p7}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 1100
    new-instance v7, Ljava/util/ArrayList;

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v7, Ljava/util/Collection;

    .line 1101
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1102
    check-cast v2, Lcom/squareup/orders/model/Order$LineItem;

    .line 475
    invoke-direct {p0, v2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->toLineItemRow(Lcom/squareup/orders/model/Order$LineItem;)Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1103
    :cond_1
    check-cast v7, Ljava/util/List;

    goto :goto_3

    .line 471
    :cond_2
    iget-object v0, v4, Lcom/squareup/orders/model/Order$Fulfillment;->entries:Ljava/util/List;

    const-string v7, "fulfillment.entries"

    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 1096
    new-instance v7, Ljava/util/ArrayList;

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v7, Ljava/util/Collection;

    .line 1097
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1098
    check-cast v2, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;

    .line 472
    iget-object v8, v2, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->line_item_uid:Ljava/lang/String;

    const-string v9, "it.line_item_uid"

    invoke-static {v8, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object/from16 v9, p7

    invoke-static {v9, v8}, Lkotlin/collections/MapsKt;->getValue(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/squareup/orders/model/Order$LineItem;

    .line 473
    iget-object v2, v2, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;->quantity:Ljava/lang/String;

    const-string v10, "it.quantity"

    invoke-static {v2, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v8, v2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->toLineItemRow(Lcom/squareup/orders/model/Order$LineItem;Ljava/lang/String;)Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;

    move-result-object v2

    invoke-interface {v7, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1099
    :cond_3
    check-cast v7, Ljava/util/List;

    .line 478
    :goto_3
    check-cast v7, Ljava/lang/Iterable;

    .line 1105
    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1106
    check-cast v2, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;

    .line 481
    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;->getLineItem()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object v8

    invoke-static {v8}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->isUnitPriced(Lcom/squareup/orders/model/Order$LineItem;)Z

    move-result v8

    if-eqz v8, :cond_4

    const/4 v2, 0x1

    goto :goto_5

    :cond_4
    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->intValue()I

    move-result v2

    :goto_5
    add-int/2addr v3, v2

    goto :goto_4

    :cond_5
    move/from16 v2, p6

    .line 484
    invoke-direct {p0, v1, v4, v2, v3}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addItemsSectionHeader(Landroid/view/ViewGroup;Lcom/squareup/orders/model/Order$Fulfillment;II)V

    .line 489
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_items_container:I

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "fulfillmentContainer.fin\u2026ub_order_items_container)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 488
    check-cast v0, Landroid/view/ViewGroup;

    .line 1109
    invoke-interface {v7}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;

    .line 490
    invoke-direct {p0, v0, v3}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addLineItemRow(Landroid/view/ViewGroup;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;)V

    goto :goto_6

    .line 492
    :cond_6
    invoke-direct {p0, v1, v4}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->showReadOnlyNoteView(Landroid/view/ViewGroup;Lcom/squareup/orders/model/Order$Fulfillment;)V

    move-object v0, p0

    move v2, p1

    move v3, p2

    move-object/from16 v4, p5

    move-object v5, p3

    .line 493
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateTracking(Landroid/view/ViewGroup;ZZLcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V

    return-void

    .line 465
    :cond_7
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final addItemsSectionDetailHeader(Landroid/view/ViewGroup;Lcom/squareup/orders/model/Order$Fulfillment;)V
    .locals 6

    .line 529
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_items_header_detail:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "fulfillmentContainer.fin\u2026rder_items_header_detail)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 528
    check-cast p1, Landroid/widget/TextView;

    .line 531
    iget-object v0, p2, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    const/16 v1, 0x8

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$Fulfillment$State;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x2

    const-string v3, "date_time"

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eq v0, v5, :cond_7

    if-eq v0, v2, :cond_1

    .line 584
    :goto_0
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 553
    :cond_1
    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getCanceledAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    .line 554
    :goto_1
    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getCancelReason(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    if-nez v0, :cond_4

    if-nez v5, :cond_4

    .line 558
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    .line 562
    :cond_4
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 563
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderhub/applet/R$color;->orderhub_text_color_canceled:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    if-eqz v0, :cond_5

    if-nez v5, :cond_5

    .line 567
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_canceled_at:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 568
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getCanceledAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 569
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_5
    const-string v1, "reason"

    if-nez v0, :cond_6

    if-eqz v5, :cond_6

    .line 572
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_canceled_with_reason:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 573
    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getCancelReason(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 574
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    .line 577
    :cond_6
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_canceled_at_with_reason:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 578
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getCanceledAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 579
    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getCancelReason(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 580
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    .line 534
    :cond_7
    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getCompletedAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    .line 535
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    .line 539
    :cond_8
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 541
    iget-object v0, p2, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-nez v0, :cond_9

    goto :goto_3

    :cond_9
    sget-object v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    if-eq v0, v5, :cond_b

    if-eq v0, v2, :cond_b

    const/4 v1, 0x3

    if-eq v0, v1, :cond_a

    .line 546
    :goto_3
    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_completed_at:I

    goto :goto_4

    .line 545
    :cond_a
    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_shipped_at:I

    goto :goto_4

    .line 543
    :cond_b
    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_status_picked_up_at:I

    .line 548
    :goto_4
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 549
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getCompletedAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 550
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    return-void
.end method

.method private final addItemsSectionHeader(Landroid/view/ViewGroup;Lcom/squareup/orders/model/Order$Fulfillment;II)V
    .locals 4

    .line 503
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_items_header:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "fulfillmentContainer.fin\u2026erhub_order_items_header)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 502
    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x1

    if-ne p3, v1, :cond_0

    .line 506
    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_items_header_uppercase:I

    goto :goto_1

    .line 507
    :cond_0
    iget-object v2, p2, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    if-nez v2, :cond_1

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v2}, Lcom/squareup/orders/model/Order$Fulfillment$State;->ordinal()I

    move-result v2

    aget v2, v3, v2

    if-eq v2, v1, :cond_3

    const/4 v3, 0x2

    if-eq v2, v3, :cond_2

    .line 510
    :goto_0
    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_items_header_uppercase:I

    goto :goto_1

    .line 509
    :cond_2
    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_items_header_canceled_uppercase:I

    goto :goto_1

    .line 508
    :cond_3
    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_items_header_completed_uppercase:I

    .line 513
    :goto_1
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    invoke-interface {v3, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    const-string v3, "num_items"

    .line 514
    invoke-virtual {v2, v3, p4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p4

    .line 515
    invoke-virtual {p4}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p4

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-le p3, v1, :cond_4

    .line 520
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addItemsSectionDetailHeader(Landroid/view/ViewGroup;Lcom/squareup/orders/model/Order$Fulfillment;)V

    :cond_4
    return-void
.end method

.method private final addLineItemRow(Landroid/view/ViewGroup;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;)V
    .locals 6

    .line 593
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 594
    sget v1, Lcom/squareup/orderhub/applet/R$layout;->orderhub_order_item_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    check-cast v0, Landroid/view/ViewGroup;

    .line 595
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 598
    sget p1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_item_name_description_section:I

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 597
    check-cast p1, Landroid/view/ViewGroup;

    .line 600
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_item_row_item_name:I

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 599
    check-cast v1, Landroid/widget/TextView;

    const-string v3, "itemName"

    .line 601
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;->getLineItem()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {v3, v4}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->displayName(Lcom/squareup/orders/model/Order$LineItem;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 605
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_item_row_item_subtitle:I

    .line 604
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    .line 603
    check-cast p1, Landroid/widget/TextView;

    .line 608
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->lineItemSubtitleFormatter:Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;->getLineItem()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object v3

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v1, v3, v2, v4, v5}, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;->createOrderLineItemSubtitle$default(Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;Lcom/squareup/orders/model/Order$LineItem;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 609
    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    const-string v4, "itemSubtitle"

    if-eqz v3, :cond_1

    .line 610
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 611
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 613
    :cond_1
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 616
    :goto_1
    sget p1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_item_row_item_quantity:I

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const-string v1, "itemQuantity"

    .line 625
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 617
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_item_quantity:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 620
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->lineItemSubtitleFormatter:Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;

    .line 621
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;->getQuantity()Ljava/math/BigDecimal;

    move-result-object v3

    .line 622
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;->getLineItem()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object v4

    iget-object v4, v4, Lcom/squareup/orders/model/Order$LineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    .line 620
    invoke-virtual {v2, v3, v4}, Lcom/squareup/ui/orderhub/util/text/LineItemSubtitleFormatter;->quantityWithPrecision(Ljava/math/BigDecimal;Lcom/squareup/orders/model/Order$QuantityUnit;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    const-string v3, "item_quantity"

    .line 618
    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 625
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 627
    sget p1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_item_row_item_price:I

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const-string v0, "itemPrice"

    .line 628
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 630
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;->getLineItem()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/orderhub/util/proto/LineItemsKt;->getBasePricePerQuantity(Lcom/squareup/orders/model/Order$LineItem;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 631
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;->getQuantity()Ljava/math/BigDecimal;

    move-result-object p2

    .line 629
    invoke-static {v1, p2}, Lcom/squareup/money/MoneyMath;->multiplyByItemQuantity(Lcom/squareup/protos/common/Money;Ljava/math/BigDecimal;)Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 628
    invoke-interface {v0, p2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 594
    :cond_2
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type android.view.ViewGroup"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final addPrice(Landroid/view/ViewGroup;Ljava/lang/String;Lcom/squareup/protos/common/Money;Z)V
    .locals 3

    if-nez p3, :cond_0

    return-void

    .line 786
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 787
    sget v1, Lcom/squareup/orderhub/applet/R$layout;->orderhub_order_price_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 788
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 789
    sget p1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_price_row_label:I

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "priceView.findViewById(R\u2026ub_order_price_row_label)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    .line 790
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_price_row_value:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "priceView.findViewById(R\u2026ub_order_price_row_value)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    .line 791
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 792
    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-interface {p2, p3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p4, :cond_1

    .line 794
    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 795
    sget-object p1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    goto :goto_0

    .line 797
    :cond_1
    sget-object p2, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 798
    sget-object p1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    :goto_0
    return-void
.end method

.method static synthetic addPrice$default(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;Landroid/view/ViewGroup;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_0

    const/4 p4, 0x0

    .line 781
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addPrice(Landroid/view/ViewGroup;Ljava/lang/String;Lcom/squareup/protos/common/Money;Z)V

    return-void
.end method

.method private final addSecondaryButton(Landroid/view/ViewGroup;)Lcom/squareup/noho/NohoButton;
    .locals 3

    .line 977
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 978
    sget v1, Lcom/squareup/orderhub/applet/R$layout;->orderhub_order_secondary_button:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    .line 979
    move-object v1, v0

    check-cast v1, Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v0

    .line 978
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.noho.NohoButton"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private final addServiceChargePrice(Lcom/squareup/orders/model/Order$ServiceCharge;)V
    .locals 7

    .line 771
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->paymentDetailsContainer:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    const-string v0, "paymentDetailsContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 772
    :cond_0
    iget-object v2, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->name:Ljava/lang/String;

    const-string v0, "serviceCharge.name"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 773
    iget-object p1, p1, Lcom/squareup/orders/model/Order$ServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    const-string v0, "serviceCharge.total_money"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    .line 770
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addPrice$default(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;Landroid/view/ViewGroup;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZILjava/lang/Object;)V

    return-void
.end method

.method private final addTender(Lcom/squareup/protos/connect/v2/resources/Tender;)V
    .locals 5

    .line 812
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->tenderContainer:Landroid/view/ViewGroup;

    const-string v1, "tenderContainer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 813
    sget v2, Lcom/squareup/orderhub/applet/R$layout;->orderhub_order_tender_row_view:I

    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->tenderContainer:Landroid/view/ViewGroup;

    if-nez v3, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 814
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->tenderContainer:Landroid/view/ViewGroup;

    if-nez v2, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 815
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_tender_amount:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "tenderView.findViewById(\u2026rhub_order_tender_amount)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/TextView;

    .line 816
    sget v2, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_tender_details:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "tenderView.findViewById(\u2026hub_order_tender_details)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    .line 817
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v3, p1, Lcom/squareup/protos/connect/v2/resources/Tender;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const-string v4, "tender.amount_money"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 818
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v1}, Lcom/squareup/ui/orderhub/util/proto/TendersKt;->getTenderDetails(Lcom/squareup/protos/connect/v2/resources/Tender;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 819
    sget-object p1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 177
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->mainView:Landroid/view/View;

    .line 178
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(com.sq\u2026s.R.id.stable_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 179
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_detail_animator:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026ub_order_detail_animator)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 180
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "view.findViewById(R.id.orderhub_order_container)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    .line 182
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    const-string v0, "container"

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_customer_container:I

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "container.findViewById(R\u2026order_customer_container)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->customerContainer:Landroid/view/ViewGroup;

    .line 183
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez p1, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_price_container:I

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "container.findViewById(R\u2026ub_order_price_container)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->paymentDetailsContainer:Landroid/view/ViewGroup;

    .line 184
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_tenders_container:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "container.findViewById(R\u2026_order_tenders_container)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->tenderContainer:Landroid/view/ViewGroup;

    return-void
.end method

.method private final fulfillmentTimeText(Lcom/squareup/orders/model/Order$Fulfillment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    if-eqz p2, :cond_0

    .line 1027
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 1029
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->getFulfillmentTime(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private final getCanShowExternalLink(Lcom/squareup/orders/model/Order;)Z
    .locals 3

    .line 972
    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getExternalDeepLink(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_5

    .line 973
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->badMaybeSquareDeviceCheck:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result v0

    if-nez v0, :cond_5

    .line 974
    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getChannel(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Channel;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p1, Lcom/squareup/protos/client/orders/Channel;->display_name:Ljava/lang/String;

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    :goto_2
    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_4

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_3

    :cond_3
    const/4 p1, 0x0

    goto :goto_4

    :cond_4
    :goto_3
    const/4 p1, 0x1

    :goto_4
    if-nez p1, :cond_5

    const/4 v1, 0x1

    :cond_5
    return v1
.end method

.method private final getFulfillmentTime(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;
    .locals 3

    .line 1019
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-eq v0, v2, :cond_1

    goto :goto_0

    .line 1021
    :cond_1
    iget-object p1, p1, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    if-eqz p1, :cond_3

    iget-object v1, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->deliver_at:Ljava/lang/String;

    goto :goto_0

    .line 1020
    :cond_2
    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getPickupAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object v1

    :cond_3
    :goto_0
    return-object v1
.end method

.method private final getFulfillmentTimeHeaderText(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;
    .locals 1

    .line 1034
    iget-object p1, p1, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$5:[I

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    :goto_0
    const/4 p1, 0x0

    goto :goto_1

    .line 1037
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_delivery_time_header:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 1036
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_pickup_time_header:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 1035
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_pickup_time_header:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method private final getRecipientAddressHeaderText(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;
    .locals 1

    .line 1043
    iget-object p1, p1, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$6:[I

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    :goto_0
    const/4 p1, 0x0

    goto :goto_1

    .line 1045
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_delivery_deliver_to_header:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 1044
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_shipment_to_header:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    return-object p1
.end method

.method private final isFulfillmentTimeVisible(Lcom/squareup/orders/model/Order$Fulfillment;)Z
    .locals 0

    .line 1015
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->getFulfillmentTime(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final isRecipientAddressVisible(Lcom/squareup/orders/model/Order$Fulfillment;)Z
    .locals 2

    .line 1050
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->SHIPMENT:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-eq v0, v1, :cond_1

    iget-object p1, p1, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private final setupActionBar(ZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V
    .locals 15

    move-object v8, p0

    .line 264
    iget-object v0, v8, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 300
    iget-object v9, v8, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v9, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 265
    :cond_1
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 266
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_action_bar_title:I

    invoke-direct {v1, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v1, Lcom/squareup/resources/TextModel;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 267
    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$1;

    move-object/from16 v6, p4

    invoke-direct {v3, v6}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$1;-><init>(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v10

    if-eqz p3, :cond_a

    .line 273
    invoke-static/range {p2 .. p2}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getDisplayId(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_a

    .line 274
    invoke-static/range {p2 .. p2}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getChannel(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Channel;

    move-result-object v0

    const/4 v3, 0x0

    if-eqz v0, :cond_4

    iget-object v0, v0, Lcom/squareup/protos/client/orders/Channel;->display_name:Ljava/lang/String;

    goto :goto_2

    :cond_4
    move-object v0, v3

    :goto_2
    check-cast v0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_5

    goto :goto_3

    :cond_5
    const/4 v1, 0x0

    :cond_6
    :goto_3
    if-eqz v1, :cond_7

    goto :goto_5

    .line 279
    :cond_7
    iget-object v0, v8, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_source_id:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 280
    invoke-static/range {p2 .. p2}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getDisplayId(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "short_ref_id"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 281
    invoke-static/range {p2 .. p2}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getChannel(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Channel;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v3, v1, Lcom/squareup/protos/client/orders/Channel;->display_name:Ljava/lang/String;

    :cond_8
    if-eqz v3, :cond_9

    goto :goto_4

    :cond_9
    const-string v3, ""

    :goto_4
    check-cast v3, Ljava/lang/CharSequence;

    const-string v1, "order_source"

    invoke-virtual {v0, v1, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 282
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "res.phrase(R.string.orde\u2026                .format()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 278
    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v1, v0}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v1, Lcom/squareup/util/ViewString;

    goto :goto_6

    .line 276
    :cond_a
    :goto_5
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_action_bar_title:I

    invoke-direct {v0, v1}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/ViewString;

    .line 272
    :goto_6
    check-cast v1, Lcom/squareup/resources/TextModel;

    .line 271
    invoke-virtual {v10, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 286
    sget-object v7, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v11, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$1;

    move-object v0, v11

    move-object v1, p0

    move/from16 v2, p3

    move-object/from16 v3, p2

    move-object/from16 v4, p4

    move/from16 v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;ZLcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;Z)V

    check-cast v11, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v10, v7, v11}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 290
    invoke-static/range {p2 .. p2}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getPrimaryAction(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Action;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 292
    sget-object v11, Lcom/squareup/noho/NohoActionButtonStyle;->SECONDARY:Lcom/squareup/noho/NohoActionButtonStyle;

    .line 293
    new-instance v0, Lcom/squareup/util/ViewString$ResourceString;

    iget-object v2, v1, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    const-string v3, "it.type"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v2}, Lcom/squareup/ui/orderhub/util/proto/ActionsKt;->getNameRes(Lcom/squareup/protos/client/orders/Action$Type;)I

    move-result v2

    invoke-direct {v0, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v12, v0

    check-cast v12, Lcom/squareup/resources/TextModel;

    xor-int/lit8 v13, p1, 0x1

    .line 295
    new-instance v14, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$2;

    move-object v0, v14

    move-object v2, v10

    move-object v3, p0

    move/from16 v4, p3

    move-object/from16 v5, p2

    move-object/from16 v6, p4

    move/from16 v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$setupActionBar$$inlined$apply$lambda$2;-><init>(Lcom/squareup/protos/client/orders/Action;Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;ZLcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;Z)V

    check-cast v14, Lkotlin/jvm/functions/Function0;

    .line 291
    invoke-virtual {v10, v11, v12, v13, v14}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 300
    :cond_b
    invoke-virtual {v10}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final showReadOnlyNoteView(Landroid/view/ViewGroup;Lcom/squareup/orders/model/Order$Fulfillment;)V
    .locals 1

    .line 641
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_note_container:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "fulfillmentContainer.fin\u2026hub_order_note_container)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 640
    check-cast p1, Landroid/view/ViewGroup;

    .line 643
    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getNote(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 644
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 645
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_note:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "noteContainer.findViewBy\u2026R.id.orderhub_order_note)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    .line 646
    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getNote(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const/16 p2, 0x8

    .line 648
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final toLineItemRow(Lcom/squareup/orders/model/Order$LineItem;)Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;
    .locals 2

    .line 1067
    iget-object v0, p1, Lcom/squareup/orders/model/Order$LineItem;->quantity:Ljava/lang/String;

    const-string v1, "quantity"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->toLineItemRow(Lcom/squareup/orders/model/Order$LineItem;Ljava/lang/String;)Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;

    move-result-object p1

    return-object p1
.end method

.method private final toLineItemRow(Lcom/squareup/orders/model/Order$LineItem;Ljava/lang/String;)Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;
    .locals 2

    .line 1062
    new-instance v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;

    .line 1064
    invoke-static {p2}, Lkotlin/text/StringsKt;->toBigDecimalOrNull(Ljava/lang/String;)Ljava/math/BigDecimal;

    move-result-object p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    sget-object p2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    const-string v1, "ZERO"

    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1062
    :goto_0
    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$LineItemRow;-><init>(Lcom/squareup/orders/model/Order$LineItem;Ljava/math/BigDecimal;)V

    return-object v0
.end method

.method private final update(Lcom/squareup/noho/NohoButton;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoButton;",
            "Z",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 1003
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    .line 1004
    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {p1, p3}, Lcom/squareup/noho/NohoButton;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    .line 1006
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    goto :goto_0

    :cond_0
    const/4 p2, 0x1

    .line 1008
    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    .line 1009
    check-cast p1, Landroid/view/View;

    .line 1125
    new-instance p2, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$update$$inlined$onClickDebounced$1;

    invoke-direct {p2, p4}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$update$$inlined$onClickDebounced$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method

.method static synthetic update$default(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;Lcom/squareup/noho/NohoButton;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V
    .locals 0

    and-int/lit8 p5, p5, 0x1

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    .line 999
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->update(Lcom/squareup/noho/NohoButton;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final updateActionButtons(ZLcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V
    .locals 3

    .line 827
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    const-string v1, "container"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_buttons_container:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "container.findViewById(R\u2026_order_buttons_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    .line 833
    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getPrimaryAction(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Action;

    move-result-object v1

    if-nez v1, :cond_1

    .line 830
    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getSecondaryActions(Lcom/squareup/orders/model/Order;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 831
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v1}, Lcom/squareup/print/PrinterStations;->hasEnabledKitchenPrintingStations()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->isFinished(Lcom/squareup/orders/model/Order;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0, p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->getCanShowExternalLink(Lcom/squareup/orders/model/Order;)Z

    move-result v1

    if-nez v1, :cond_1

    const/16 p1, 0x8

    .line 836
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 838
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 843
    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getPrimaryAction(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Action;

    move-result-object v1

    .line 840
    invoke-direct {p0, p1, v0, v1, p3}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updatePrimaryActionButton(ZLandroid/view/ViewGroup;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V

    .line 848
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_secondary_action_button_container:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "actionsContainer.findVie\u2026_action_button_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 847
    check-cast v0, Landroid/view/ViewGroup;

    .line 849
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 851
    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getSecondaryActions(Lcom/squareup/orders/model/Order;)Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 1123
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/orders/Action;

    .line 852
    invoke-direct {p0, p1, v0, v2, p3}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateSecondaryActionButton(ZLandroid/view/ViewGroup;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V

    goto :goto_0

    .line 860
    :cond_2
    invoke-direct {p0, p2, v0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updatePrintButton(Lcom/squareup/orders/model/Order;Landroid/view/ViewGroup;)V

    .line 865
    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->isFinished(Lcom/squareup/orders/model/Order;)Z

    move-result v1

    .line 864
    invoke-direct {p0, v1, p1, v0, p3}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateViewTransactionDetail(ZZLandroid/view/ViewGroup;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V

    .line 870
    invoke-direct {p0, p2, p1, v0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateExternalLinkButton(Lcom/squareup/orders/model/Order;ZLandroid/view/ViewGroup;)V

    :goto_1
    return-void
.end method

.method private final updateCustomer(Lcom/squareup/orders/model/Order$Fulfillment;)V
    .locals 13

    .line 349
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->customerContainer:Landroid/view/ViewGroup;

    const-string v1, "customerContainer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v2, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_customer_header:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 350
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->customerContainer:Landroid/view/ViewGroup;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v3, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_customer_name:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 352
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->customerContainer:Landroid/view/ViewGroup;

    if-nez v3, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v4, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_customer_email:I

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 351
    check-cast v3, Landroid/widget/TextView;

    .line 354
    iget-object v4, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->customerContainer:Landroid/view/ViewGroup;

    if-nez v4, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget v5, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_customer_phone:I

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 353
    check-cast v4, Landroid/widget/TextView;

    .line 356
    iget-object v5, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v5}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->recipientNameOrDefault(Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v5

    .line 358
    iget-object v6, p1, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    sget-object v7, Lcom/squareup/orders/model/Order$FulfillmentType;->MANAGED_DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    const-string v8, "nameTextView"

    const-string v9, "headerView"

    const-string v10, "phoneTextView"

    const-string v11, "emailTextView"

    const/16 v12, 0x8

    if-ne v6, v7, :cond_4

    .line 361
    invoke-static {v0, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_courier_header:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    invoke-static {v2, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 363
    invoke-static {v3, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 364
    invoke-static {v4, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v12}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    .line 368
    :cond_4
    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getRecipient(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    move-result-object p1

    if-nez p1, :cond_6

    .line 370
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->customerContainer:Landroid/view/ViewGroup;

    if-nez p1, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1, v12}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    .line 372
    :cond_6
    iget-object v6, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->customerContainer:Landroid/view/ViewGroup;

    if-nez v6, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 374
    invoke-static {v0, v9}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v7, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_customer_header:I

    invoke-interface {v6, v7}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    invoke-static {v2, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 377
    iget-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->email_address:Ljava/lang/String;

    if-nez v0, :cond_8

    .line 378
    invoke-static {v3, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 380
    :cond_8
    invoke-static {v3, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 381
    iget-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->email_address:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 382
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->emailAppAvailable:Z

    if-eqz v0, :cond_9

    .line 383
    iget-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->email_address:Ljava/lang/String;

    const/16 v2, 0x10

    invoke-static {v0, v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    const-string v2, "mailto:"

    .line 384
    invoke-static {v3, v0, v2}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    .line 385
    invoke-static {v3}, Lcom/squareup/text/NoUnderlineURLSpan;->removeLinkUnderlines(Landroid/widget/TextView;)V

    .line 389
    :cond_9
    :goto_0
    iget-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->phone_number:Ljava/lang/String;

    if-nez v0, :cond_a

    .line 390
    invoke-static {v4, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 392
    :cond_a
    invoke-static {v4, v10}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 393
    iget-object p1, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->phone_number:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method

.method private final updateExternalLinkButton(Lcom/squareup/orders/model/Order;ZLandroid/view/ViewGroup;)V
    .locals 3

    .line 948
    invoke-direct {p0, p3}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addSecondaryButton(Landroid/view/ViewGroup;)Lcom/squareup/noho/NohoButton;

    move-result-object p3

    .line 949
    move-object v0, p3

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->getCanShowExternalLink(Lcom/squareup/orders/model/Order;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 951
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->getCanShowExternalLink(Lcom/squareup/orders/model/Order;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 952
    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->isFinished(Lcom/squareup/orders/model/Order;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_external_source_view:I

    goto :goto_0

    .line 953
    :cond_0
    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_external_source_manage:I

    .line 955
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 956
    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getChannel(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Channel;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/squareup/protos/client/orders/Channel;->display_name:Ljava/lang/String;

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_2

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "order_source"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 957
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 958
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 963
    new-instance v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateExternalLinkButton$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateExternalLinkButton$1;-><init>(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;Lcom/squareup/orders/model/Order;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 960
    invoke-direct {p0, p3, p2, v0, v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->update(Lcom/squareup/noho/NohoButton;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;)V

    goto :goto_2

    .line 956
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Required value was null."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_3
    :goto_2
    return-void
.end method

.method private final updateFulfillmentDetails(Lcom/squareup/orders/model/Order;Lcom/squareup/orders/model/Order$Fulfillment;Ljava/lang/String;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V
    .locals 7

    .line 405
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    const-string v1, "container"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v2, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_fulfillment_time_container:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "container.findViewById(R\u2026lfillment_time_container)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 404
    check-cast v0, Landroid/view/ViewGroup;

    .line 407
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v3, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_recipient_address_container:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const-string v3, "container.findViewById(R\u2026ipient_address_container)"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 406
    check-cast v2, Landroid/view/ViewGroup;

    .line 409
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez v3, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget v4, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_shipment_method_container:I

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const-string v4, "container.findViewById(R\u2026hipment_method_container)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 408
    check-cast v3, Landroid/view/ViewGroup;

    .line 412
    invoke-direct {p0, p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->isFulfillmentTimeVisible(Lcom/squareup/orders/model/Order$Fulfillment;)Z

    move-result v4

    const/16 v5, 0x8

    const/4 v6, 0x0

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    goto :goto_0

    :cond_3
    const/16 v4, 0x8

    :goto_0
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 414
    invoke-direct {p0, p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->isRecipientAddressVisible(Lcom/squareup/orders/model/Order$Fulfillment;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 416
    iget-object v0, p2, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    sget-object v2, Lcom/squareup/orders/model/Order$FulfillmentType;->SHIPMENT:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-ne v0, v2, :cond_5

    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    const/16 v0, 0x8

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 419
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    sget v2, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_fulfillment_time_header:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "container.findViewById(R\u2026_fulfillment_time_header)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 418
    check-cast v0, Landroid/widget/TextView;

    .line 420
    invoke-direct {p0, p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->getFulfillmentTimeHeaderText(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 423
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez v0, :cond_7

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    sget v2, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_fulfillment_time:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "container.findViewById(R\u2026b_order_fulfillment_time)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 422
    check-cast v0, Landroid/widget/TextView;

    .line 424
    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->fulfillmentTimeText(Lcom/squareup/orders/model/Order$Fulfillment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    check-cast p3, Ljava/lang/CharSequence;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 427
    iget-object p3, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez p3, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_adjust_fulfillment_time_button:I

    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string v0, "container.findViewById(R\u2026_fulfillment_time_button)"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 426
    check-cast p3, Landroid/widget/TextView;

    .line 428
    check-cast p3, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getCanAdjustFulfillmentTime(Lcom/squareup/orders/model/Order;)Z

    move-result p1

    invoke-static {p3, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 1089
    new-instance p1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateFulfillmentDetails$$inlined$onClickDebounced$1;

    invoke-direct {p1, p4}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateFulfillmentDetails$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 431
    invoke-static {p2}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getRecipient(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    move-result-object p1

    .line 434
    iget-object p3, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez p3, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    sget p4, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_recipient_address_header:I

    invoke-virtual {p3, p4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p3

    const-string p4, "container.findViewById(R\u2026recipient_address_header)"

    invoke-static {p3, p4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 433
    check-cast p3, Landroid/widget/TextView;

    .line 436
    iget-object p4, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez p4, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_recipient_address_body:I

    invoke-virtual {p4, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p4

    const-string v0, "container.findViewById(R\u2026r_recipient_address_body)"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 435
    check-cast p4, Landroid/widget/TextView;

    .line 438
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez v0, :cond_b

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    sget v2, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_recipient_address_name:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "container.findViewById(R\u2026r_recipient_address_name)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 437
    check-cast v0, Landroid/widget/TextView;

    if-eqz p1, :cond_c

    .line 440
    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->display_name:Ljava/lang/String;

    if-eqz v2, :cond_c

    check-cast v2, Ljava/lang/CharSequence;

    goto :goto_3

    .line 441
    :cond_c
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/orderhub/applet/R$string;->orderhub_detail_order_missing_recipient:I

    invoke-interface {v2, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    :goto_3
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    if-eqz p1, :cond_d

    .line 442
    iget-object p1, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    goto :goto_4

    :cond_d
    move-object p1, v0

    :goto_4
    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinatorKt;->access$getAddressText$p(Lcom/squareup/protos/connect/v2/resources/Address;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p4, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 443
    invoke-direct {p0, p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->getRecipientAddressHeaderText(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 446
    iget-object p1, p2, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz p1, :cond_e

    iget-object p1, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_type:Ljava/lang/String;

    goto :goto_5

    :cond_e
    move-object p1, v0

    :goto_5
    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_10

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result p1

    if-nez p1, :cond_f

    goto :goto_6

    :cond_f
    const/4 p1, 0x0

    goto :goto_7

    :cond_10
    :goto_6
    const/4 p1, 0x1

    :goto_7
    if-eqz p1, :cond_11

    goto :goto_8

    :cond_11
    const/4 v5, 0x0

    :goto_8
    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 451
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez p1, :cond_12

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    sget p3, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_shipment_method:I

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p3, "container.findViewById(R\u2026ub_order_shipment_method)"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    .line 452
    iget-object p2, p2, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz p2, :cond_13

    iget-object v0, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_type:Ljava/lang/String;

    :cond_13
    if-eqz v0, :cond_14

    goto :goto_9

    :cond_14
    const-string v0, ""

    :goto_9
    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final updateId(Lcom/squareup/orders/model/Order;Z)V
    .locals 5

    .line 333
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    const-string v1, "container"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v2, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_id_container:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "container.findViewById(R\u2026erhub_order_id_container)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez p2, :cond_7

    .line 336
    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getDisplayId(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-nez p2, :cond_1

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p2, 0x1

    :goto_1
    if-nez p2, :cond_7

    .line 337
    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getChannel(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Channel;

    move-result-object p2

    if-eqz p2, :cond_3

    iget-object p2, p2, Lcom/squareup/protos/client/orders/Channel;->display_name:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move-object p2, v2

    :goto_2
    check-cast p2, Ljava/lang/CharSequence;

    if-eqz p2, :cond_5

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result p2

    if-nez p2, :cond_4

    goto :goto_3

    :cond_4
    const/4 p2, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    const/4 p2, 0x1

    :goto_4
    if-eqz p2, :cond_6

    goto :goto_5

    :cond_6
    const/4 v3, 0x0

    :cond_7
    :goto_5
    if-eqz v3, :cond_8

    const/16 v4, 0x8

    .line 338
    :cond_8
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    if-nez v3, :cond_c

    .line 340
    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez p2, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_id:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const-string v0, "container.findViewById<T\u2026>(R.id.orderhub_order_id)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p2, Landroid/widget/TextView;

    .line 341
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_source_id:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 342
    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getDisplayId(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v3, "short_ref_id"

    invoke-virtual {v0, v3, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 343
    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getChannel(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Channel;

    move-result-object p1

    if-eqz p1, :cond_a

    iget-object v2, p1, Lcom/squareup/protos/client/orders/Channel;->display_name:Ljava/lang/String;

    :cond_a
    if-eqz v2, :cond_b

    goto :goto_6

    :cond_b
    const-string v2, ""

    :goto_6
    check-cast v2, Ljava/lang/CharSequence;

    const-string p1, "order_source"

    invoke-virtual {v0, p1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 344
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_c
    return-void
.end method

.method private final updatePaymentDetails(Lcom/squareup/orders/model/Order;)V
    .locals 12

    .line 717
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->paymentDetailsContainer:Landroid/view/ViewGroup;

    const-string v7, "paymentDetailsContainer"

    if-nez v0, :cond_0

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 720
    iget-object v0, p1, Lcom/squareup/orders/model/Order;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_2

    invoke-static {v0}, Lcom/squareup/money/v2/MoneysKt;->isZero(Lcom/squareup/protos/connect/v2/common/Money;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 722
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->paymentDetailsContainer:Landroid/view/ViewGroup;

    if-nez v1, :cond_1

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 723
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_price_discount:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 724
    iget-object v0, p1, Lcom/squareup/orders/model/Order;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const-string v3, "order.total_discount_money"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/money/MoneyMath;->negate(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    .line 721
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addPrice$default(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;Landroid/view/ViewGroup;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZILjava/lang/Object;)V

    .line 729
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->paymentDetailsContainer:Landroid/view/ViewGroup;

    if-nez v1, :cond_3

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 730
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_price_subtotal:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 731
    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getSubtotal(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    .line 728
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addPrice$default(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;Landroid/view/ViewGroup;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZILjava/lang/Object;)V

    .line 735
    iget-object v0, p1, Lcom/squareup/orders/model/Order;->service_charges:Ljava/util/List;

    const-string v8, "order.service_charges"

    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 1111
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 1112
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v9, 0x0

    const/4 v10, 0x1

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/orders/model/Order$ServiceCharge;

    .line 736
    iget-object v4, v3, Lcom/squareup/orders/model/Order$ServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v4, :cond_5

    invoke-static {v4}, Lcom/squareup/money/v2/MoneysKt;->isZero(Lcom/squareup/protos/connect/v2/common/Money;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v3, v3, Lcom/squareup/orders/model/Order$ServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v3, :cond_5

    invoke-static {v3}, Lcom/squareup/money/v2/MoneysKt;->isZero(Lcom/squareup/protos/connect/v2/common/Money;)Z

    move-result v3

    if-nez v3, :cond_5

    const/4 v9, 0x1

    :cond_5
    if-eqz v9, :cond_4

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1113
    :cond_6
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 1114
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const-string v11, "serviceCharge"

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orders/model/Order$ServiceCharge;

    .line 737
    invoke-static {v1, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addServiceChargePrice(Lcom/squareup/orders/model/Order$ServiceCharge;)V

    goto :goto_1

    .line 740
    :cond_7
    iget-object v0, p1, Lcom/squareup/orders/model/Order;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_9

    invoke-static {v0}, Lcom/squareup/money/v2/MoneysKt;->isZero(Lcom/squareup/protos/connect/v2/common/Money;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 742
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->paymentDetailsContainer:Landroid/view/ViewGroup;

    if-nez v1, :cond_8

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 743
    :cond_8
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_price_tax:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 744
    iget-object v0, p1, Lcom/squareup/orders/model/Order;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    const-string v3, "order.total_tax_money"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    .line 741
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addPrice$default(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;Landroid/view/ViewGroup;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZILjava/lang/Object;)V

    .line 749
    :cond_9
    iget-object v0, p1, Lcom/squareup/orders/model/Order;->service_charges:Ljava/util/List;

    invoke-static {v0, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 1116
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 1117
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_a
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/squareup/orders/model/Order$ServiceCharge;

    .line 750
    iget-object v4, v3, Lcom/squareup/orders/model/Order$ServiceCharge;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v4, :cond_b

    invoke-static {v4}, Lcom/squareup/money/v2/MoneysKt;->isZero(Lcom/squareup/protos/connect/v2/common/Money;)Z

    move-result v4

    if-ne v4, v10, :cond_b

    iget-object v3, v3, Lcom/squareup/orders/model/Order$ServiceCharge;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v3, :cond_b

    invoke-static {v3}, Lcom/squareup/money/v2/MoneysKt;->isZero(Lcom/squareup/protos/connect/v2/common/Money;)Z

    move-result v3

    if-nez v3, :cond_b

    const/4 v3, 0x1

    goto :goto_3

    :cond_b
    const/4 v3, 0x0

    :goto_3
    if-eqz v3, :cond_a

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1118
    :cond_c
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 1119
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/orders/model/Order$ServiceCharge;

    .line 751
    invoke-static {v1, v11}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addServiceChargePrice(Lcom/squareup/orders/model/Order$ServiceCharge;)V

    goto :goto_4

    .line 753
    :cond_d
    iget-object v0, p1, Lcom/squareup/orders/model/Order;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_f

    invoke-static {v0}, Lcom/squareup/money/v2/MoneysKt;->isZero(Lcom/squareup/protos/connect/v2/common/Money;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 755
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->paymentDetailsContainer:Landroid/view/ViewGroup;

    if-nez v1, :cond_e

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 756
    :cond_e
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_price_tip:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 757
    iget-object v0, p1, Lcom/squareup/orders/model/Order;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    const-string v3, "order.total_tip_money"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v3

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    .line 754
    invoke-static/range {v0 .. v6}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addPrice$default(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;Landroid/view/ViewGroup;Ljava/lang/String;Lcom/squareup/protos/common/Money;ZILjava/lang/Object;)V

    .line 762
    :cond_f
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->paymentDetailsContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_10

    invoke-static {v7}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 763
    :cond_10
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_price_total:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 764
    iget-object v2, p1, Lcom/squareup/orders/model/Order;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v2, :cond_11

    invoke-static {v2}, Lcom/squareup/money/v2/MoneysKt;->toMoneyV1(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v2

    goto :goto_5

    :cond_11
    const/4 v2, 0x0

    .line 761
    :goto_5
    invoke-direct {p0, v0, v1, v2, v10}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addPrice(Landroid/view/ViewGroup;Ljava/lang/String;Lcom/squareup/protos/common/Money;Z)V

    return-void
.end method

.method private final updatePrimaryActionButton(ZLandroid/view/ViewGroup;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V
    .locals 1

    .line 884
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_primary_action:I

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoButton;

    const-string v0, "actionButton"

    .line 885
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p2, p1, p3, p4}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateWithAction(Lcom/squareup/noho/NohoButton;ZLcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V

    return-void
.end method

.method private final updatePrintButton(Lcom/squareup/orders/model/Order;Landroid/view/ViewGroup;)V
    .locals 8

    .line 910
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v0}, Lcom/squareup/print/PrinterStations;->hasEnabledKitchenPrintingStations()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->isFinished(Lcom/squareup/orders/model/Order;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 911
    :goto_0
    invoke-direct {p0, p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addSecondaryButton(Landroid/view/ViewGroup;)Lcom/squareup/noho/NohoButton;

    move-result-object v2

    .line 912
    move-object p2, v2

    check-cast p2, Landroid/view/View;

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz v0, :cond_1

    const/4 v3, 0x0

    .line 915
    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_print:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 916
    new-instance p2, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updatePrintButton$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updatePrintButton$1;-><init>(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;Lcom/squareup/orders/model/Order;)V

    move-object v5, p2

    check-cast v5, Lkotlin/jvm/functions/Function0;

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v1, p0

    .line 914
    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->update$default(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;Lcom/squareup/noho/NohoButton;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    :cond_1
    return-void
.end method

.method private final updateSecondaryActionButton(ZLandroid/view/ViewGroup;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V
    .locals 0

    .line 898
    invoke-direct {p0, p2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addSecondaryButton(Landroid/view/ViewGroup;)Lcom/squareup/noho/NohoButton;

    move-result-object p2

    .line 899
    invoke-direct {p0, p2, p1, p3, p4}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateWithAction(Lcom/squareup/noho/NohoButton;ZLcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V

    return-void
.end method

.method private final updateStatus(Lcom/squareup/orders/model/Order;)V
    .locals 5

    .line 304
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    const-string v1, "container"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v2, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_status:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 305
    check-cast v0, Landroid/widget/TextView;

    .line 306
    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {v2, v3}, Lcom/squareup/ui/orderhub/util/proto/OrderDisplayStateDatasKt;->getDisplayName(Lcom/squareup/protos/client/orders/OrderDisplayStateData;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 308
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {v4}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/squareup/ui/orderhub/util/proto/OrderDisplayStateDatasKt;->getColor(Lcom/squareup/protos/client/orders/OrderDisplayStateData;Lorg/threeten/bp/ZonedDateTime;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v2

    .line 307
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 312
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v2, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_created_date:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "container.findViewById(R\u2026erhub_order_created_date)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/widget/TextView;

    .line 313
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->dateTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getDisplayDateTime(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 314
    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v2, 0x0

    .line 315
    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 317
    :cond_2
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getDisplayState(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/ui/orderhub/util/proto/OrderDisplayStateDatasKt;->getUpdatedAtPatternRes(Lcom/squareup/protos/client/orders/OrderDisplayStateData;)I

    move-result v4

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    const-string v4, "date_time"

    .line 318
    invoke-virtual {v3, v4, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 319
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 322
    :goto_0
    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getChannel(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Channel;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-static {p1}, Lcom/squareup/ui/orderhub/util/proto/ChannelsKt;->getIconRes(Lcom/squareup/protos/client/orders/Channel;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    .line 323
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_source_icon:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 324
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    const-string v1, "iconView"

    .line 325
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    :cond_4
    return-void
.end method

.method private final updateTenders(Lcom/squareup/orders/model/Order;)V
    .locals 2

    .line 803
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->tenderContainer:Landroid/view/ViewGroup;

    const-string v1, "tenderContainer"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 804
    iget-object v0, p1, Lcom/squareup/orders/model/Order;->tenders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 805
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->tenderContainer:Landroid/view/ViewGroup;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1

    .line 807
    :cond_2
    iget-object p1, p1, Lcom/squareup/orders/model/Order;->tenders:Ljava/util/List;

    const-string v0, "order.tenders"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 1121
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/resources/Tender;

    const-string v1, "it"

    .line 807
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addTender(Lcom/squareup/protos/connect/v2/resources/Tender;)V

    goto :goto_0

    :cond_3
    :goto_1
    return-void
.end method

.method private final updateTracking(Landroid/view/ViewGroup;ZZLcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V
    .locals 6

    .line 660
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_tracking_container:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "fulfillmentContainer.fin\u2026order_tracking_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 659
    check-cast v0, Landroid/view/ViewGroup;

    .line 661
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_add_tracking:I

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "fulfillmentContainer.fin\u2026erhub_order_add_tracking)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Landroid/widget/TextView;

    .line 664
    iget-object v2, p4, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    sget-object v3, Lcom/squareup/orders/model/Order$FulfillmentType;->SHIPMENT:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/16 v4, 0x8

    if-ne v2, v3, :cond_6

    iget-object v2, p4, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    sget-object v3, Lcom/squareup/orders/model/Order$Fulfillment$State;->CANCELED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    if-ne v2, v3, :cond_0

    goto/16 :goto_2

    .line 670
    :cond_0
    invoke-static {p4}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getTrackingInfo(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v5, 0x0

    if-eqz v2, :cond_3

    .line 672
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 673
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 675
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_tracking_info:I

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "fulfillmentContainer.fin\u2026rhub_order_tracking_info)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 674
    check-cast v0, Landroid/widget/TextView;

    .line 677
    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_tracking_edit:I

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v1, "fulfillmentContainer.fin\u2026rhub_order_tracking_edit)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 676
    check-cast p1, Landroid/widget/TextView;

    if-eqz p2, :cond_1

    .line 681
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    if-eqz p3, :cond_2

    .line 682
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0

    .line 684
    :cond_2
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 685
    new-instance p2, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateTracking$1;

    invoke-direct {p2, p5, p4, v2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateTracking$1;-><init>(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/ordermanagerdata/TrackingInfo;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 692
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_tracking_info:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 693
    invoke-virtual {v2}, Lcom/squareup/ordermanagerdata/TrackingInfo;->getTrackingNumber()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string p3, "tracking_num"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 694
    invoke-virtual {v2}, Lcom/squareup/ordermanagerdata/TrackingInfo;->getCarrierName()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string p3, "tracking_carrier"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 695
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 696
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    if-eqz p2, :cond_4

    .line 700
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 701
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 703
    :cond_4
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 704
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    if-eqz p3, :cond_5

    .line 706
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_1

    .line 708
    :cond_5
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 709
    new-instance p1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateTracking$2;

    invoke-direct {p1, p5, p4}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateTracking$2;-><init>(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;Lcom/squareup/orders/model/Order$Fulfillment;)V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    return-void

    .line 665
    :cond_6
    :goto_2
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 666
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method private final updateViewTransactionDetail(ZZLandroid/view/ViewGroup;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V
    .locals 1

    .line 932
    invoke-direct {p0, p3}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addSecondaryButton(Landroid/view/ViewGroup;)Lcom/squareup/noho/NohoButton;

    move-result-object p3

    .line 933
    move-object v0, p3

    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p1, :cond_0

    .line 937
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_view_transaction_detail:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 938
    new-instance v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateViewTransactionDetail$1;

    invoke-direct {v0, p4}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateViewTransactionDetail$1;-><init>(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    .line 935
    invoke-direct {p0, p3, p2, p1, v0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->update(Lcom/squareup/noho/NohoButton;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;)V

    :cond_0
    return-void
.end method

.method private final updateViews(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V
    .locals 14

    .line 190
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;->getData()Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData;

    move-result-object v0

    .line 191
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsSpinner;

    const-string v2, "animator"

    if-eqz v1, :cond_2

    .line 192
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez p1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 193
    :cond_0
    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_detail_spinner:I

    .line 192
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 195
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez p1, :cond_1

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    goto/16 :goto_3

    .line 197
    :cond_2
    instance-of v1, v0, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;

    if-eqz v1, :cond_9

    .line 198
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    if-nez v1, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget v2, Lcom/squareup/orderhub/applet/R$id;->orderhub_order_detail_scrollview:I

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 199
    check-cast v0, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;->getState()Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v1

    .line 200
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;->getState()Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v10

    .line 202
    invoke-static {v10}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v2

    .line 205
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;->getState()Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v3

    invoke-direct {p0, v1, v10, v3, p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->setupActionBar(ZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V

    .line 207
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->mainView:Landroid/view/View;

    if-nez v3, :cond_4

    const-string v4, "mainView"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v4, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateViews$1;

    invoke-direct {v4, p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateViews$1;-><init>(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V

    check-cast v4, Lkotlin/jvm/functions/Function0;

    invoke-static {v3, v4}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 212
    invoke-direct {p0, v10}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateStatus(Lcom/squareup/orders/model/Order;)V

    .line 215
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;->getState()Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v3

    invoke-direct {p0, v10, v3}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateId(Lcom/squareup/orders/model/Order;Z)V

    .line 218
    invoke-direct {p0, v2}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateCustomer(Lcom/squareup/orders/model/Order$Fulfillment;)V

    .line 221
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;->getState()Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    move-result-object v3

    instance-of v3, v3, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    if-eqz v3, :cond_5

    .line 222
    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderDetailsScreenData$ShowingOrderDetailsScreen;->getState()Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;->getPickupTimeOverride()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    .line 227
    :goto_0
    invoke-direct {p0, v10, v2, v0, p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateFulfillmentDetails(Lcom/squareup/orders/model/Order;Lcom/squareup/orders/model/Order$Fulfillment;Ljava/lang/String;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->container:Landroid/view/ViewGroup;

    if-nez v0, :cond_6

    const-string v2, "container"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    sget v2, Lcom/squareup/orderhub/applet/R$id;->orderhub_fulfillments_section:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v2, "container.findViewById(R\u2026hub_fulfillments_section)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    check-cast v0, Landroid/view/ViewGroup;

    .line 231
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 233
    iget-object v2, v10, Lcom/squareup/orders/model/Order;->line_items:Ljava/util/List;

    const-string v3, "order.line_items"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Ljava/lang/Iterable;

    const/16 v3, 0xa

    .line 1081
    invoke-static {v2, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-static {v3}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v3

    const/16 v4, 0x10

    invoke-static {v3, v4}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v3

    .line 1082
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4, v3}, Ljava/util/LinkedHashMap;-><init>(I)V

    move-object v11, v4

    check-cast v11, Ljava/util/Map;

    .line 1083
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 1084
    move-object v4, v3

    check-cast v4, Lcom/squareup/orders/model/Order$LineItem;

    .line 233
    iget-object v4, v4, Lcom/squareup/orders/model/Order$LineItem;->uid:Ljava/lang/String;

    invoke-interface {v11, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 234
    :cond_7
    iget-object v2, v10, Lcom/squareup/orders/model/Order;->fulfillments:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v12

    .line 236
    invoke-static {v10}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getSortedFulfillments(Lcom/squareup/orders/model/Order;)Ljava/util/List;

    move-result-object v2

    check-cast v2, Ljava/lang/Iterable;

    .line 1087
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Lcom/squareup/orders/model/Order$Fulfillment;

    .line 238
    invoke-static {v10}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->isFinished(Lcom/squareup/orders/model/Order;)Z

    move-result v3

    move-object v2, p0

    move v4, v1

    move-object v5, p1

    move-object v6, v0

    move v8, v12

    move-object v9, v11

    .line 237
    invoke-direct/range {v2 .. v9}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->addFulfillment(ZZLcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;Landroid/view/ViewGroup;Lcom/squareup/orders/model/Order$Fulfillment;ILjava/util/Map;)V

    goto :goto_2

    .line 248
    :cond_8
    invoke-direct {p0, v10}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updatePaymentDetails(Lcom/squareup/orders/model/Order;)V

    .line 250
    invoke-direct {p0, v10}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateTenders(Lcom/squareup/orders/model/Order;)V

    .line 253
    invoke-direct {p0, v1, v10, p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->updateActionButtons(ZLcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V

    :cond_9
    :goto_3
    return-void
.end method

.method private final updateWithAction(Lcom/squareup/noho/NohoButton;ZLcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;)V
    .locals 3

    .line 988
    move-object v0, p1

    check-cast v0, Landroid/view/View;

    if-eqz p3, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eqz p3, :cond_1

    .line 992
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->res:Lcom/squareup/util/Res;

    iget-object v1, p3, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    const-string v2, "action.type"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/ui/orderhub/util/proto/ActionsKt;->getNameRes(Lcom/squareup/protos/client/orders/Action$Type;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 993
    new-instance v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateWithAction$1;

    invoke-direct {v1, p4, p3}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$updateWithAction$1;-><init>(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsScreen;Lcom/squareup/protos/client/orders/Action;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    .line 990
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->update(Lcom/squareup/noho/NohoButton;ZLjava/lang/String;Lkotlin/jvm/functions/Function0;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->bindViews(Landroid/view/View;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->screens:Lio/reactivex/Observable;

    .line 172
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "screens\n        .observeOn(mainScheduler)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    new-instance v1, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$attach$1;-><init>(Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
