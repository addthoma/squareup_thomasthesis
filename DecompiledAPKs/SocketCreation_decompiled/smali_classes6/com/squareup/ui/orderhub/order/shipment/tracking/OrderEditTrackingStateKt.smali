.class public final Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingStateKt;
.super Ljava/lang/Object;
.source "OrderEditTrackingState.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "trackingScreenData",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;",
        "getTrackingScreenData",
        "(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getTrackingScreenData(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;)Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;
    .locals 5

    const-string v0, "$this$trackingScreenData"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;

    .line 30
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->isReadOnly()Z

    move-result v1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->getShouldShowRemoveTracking()Z

    move-result v2

    .line 32
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->getShowCustomCarrierOnly()Z

    move-result v3

    .line 33
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getPrimaryAction(Lcom/squareup/orders/model/Order;)Lcom/squareup/protos/client/orders/Action;

    move-result-object p0

    if-eqz p0, :cond_0

    iget-object p0, p0, Lcom/squareup/protos/client/orders/Action;->can_apply_to_line_items:Ljava/lang/Boolean;

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {p0, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    .line 29
    invoke-direct {v0, v1, v2, v3, p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;-><init>(ZZZZ)V

    return-object v0
.end method
