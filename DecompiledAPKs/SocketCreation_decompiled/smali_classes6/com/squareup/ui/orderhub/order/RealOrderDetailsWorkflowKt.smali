.class public final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflowKt;
.super Ljava/lang/Object;
.source "RealOrderDetailsWorkflow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0000\u001a\u000c\u0010\u0003\u001a\u00020\u0004*\u00020\u0005H\u0002\u00a8\u0006\u0006"
    }
    d2 = {
        "asOrderUpdateFailure",
        "Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;",
        "Lcom/squareup/ordermanagerdata/ResultState$Failure;",
        "getVersionMismatchState",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getVersionMismatchState(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflowKt;->getVersionMismatchState(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    move-result-object p0

    return-object p0
.end method

.method public static final asOrderUpdateFailure(Lcom/squareup/ordermanagerdata/ResultState$Failure;)Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;
    .locals 1

    const-string v0, "$this$asOrderUpdateFailure"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 880
    sget-object v0, Lcom/squareup/ordermanagerdata/ResultState$Failure$GenericError;->INSTANCE:Lcom/squareup/ordermanagerdata/ResultState$Failure$GenericError;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p0, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$GenericError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$GenericError;

    check-cast p0, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    goto :goto_0

    .line 881
    :cond_0
    sget-object v0, Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;->INSTANCE:Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p0, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$VersionMismatchError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$VersionMismatchError;

    check-cast p0, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    goto :goto_0

    .line 882
    :cond_1
    sget-object v0, Lcom/squareup/ordermanagerdata/ResultState$Failure$ConnectionError;->INSTANCE:Lcom/squareup/ordermanagerdata/ResultState$Failure$ConnectionError;

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    sget-object p0, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$ConnectionError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$ConnectionError;

    check-cast p0, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    :goto_0
    return-object p0

    :cond_2
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method private static final getVersionMismatchState(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;
    .locals 11

    .line 867
    new-instance v10, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 868
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v1

    .line 869
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v2

    .line 870
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v3

    .line 871
    new-instance v5, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    .line 872
    sget-object p0, Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;->INSTANCE:Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;

    check-cast p0, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    invoke-static {p0}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflowKt;->asOrderUpdateFailure(Lcom/squareup/ordermanagerdata/ResultState$Failure;)Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    move-result-object p0

    .line 873
    sget-object v0, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CurrentOrderUpdated;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CurrentOrderUpdated;

    check-cast v0, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    const/4 v4, 0x0

    .line 871
    invoke-direct {v5, p0, v0, v4}, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)V

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x68

    const/4 v9, 0x0

    move-object v0, v10

    .line 867
    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v10
.end method
