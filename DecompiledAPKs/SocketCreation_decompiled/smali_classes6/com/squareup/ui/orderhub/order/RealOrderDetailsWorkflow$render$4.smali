.class final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderDetailsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->render(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Event;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "event",
        "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;->this$0:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Event;)Lcom/squareup/workflow/WorkflowAction;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Event;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Event$AdjustPickupTime;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;->this$0:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const-string v4, "state.order.id"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v4, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;->ORDER_HUB_ADJUST_PICKUP_TIME:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;

    invoke-virtual {v0, v3, v4}, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;->logEvent$orderhub_applet_release(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V

    .line 184
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 185
    new-instance v13, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 186
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v4

    .line 187
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v5

    .line 188
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 189
    check-cast p1, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Event$AdjustPickupTime;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Event$AdjustPickupTime;->getTime()Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x38

    const/4 v12, 0x0

    move-object v3, v13

    .line 185
    invoke-direct/range {v3 .. v12}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 184
    invoke-static {v0, v13, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 193
    :cond_0
    sget-object v0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Event$CancelAdjustPickupTime;->INSTANCE:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Event$CancelAdjustPickupTime;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 194
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 195
    new-instance v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 196
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v4

    .line 197
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v5

    .line 198
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 199
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    check-cast v3, Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$AdjustingPickupTimeState;->getSelectedPickupTime()Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x38

    const/4 v12, 0x0

    move-object v3, v0

    .line 195
    invoke-direct/range {v3 .. v12}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 194
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$4;->invoke(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeScreen$Event;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
