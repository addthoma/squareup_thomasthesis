.class public final Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInputKt;
.super Ljava/lang/Object;
.source "OrderMarkCanceledInput.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderMarkCanceledInput.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderMarkCanceledInput.kt\ncom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInputKt\n*L\n1#1,70:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\"\u0018\u0010\u0000\u001a\u00020\u0001*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "initialState",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;",
        "getInitialState",
        "(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getInitialState(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;
    .locals 23

    const-string v0, "$this$initialState"

    move-object/from16 v1, p0

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getBillRetrievalRetry()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    .line 38
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    const/4 v3, 0x0

    .line 40
    sget-object v4, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->RETRIEVE_BILL_STATE:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v7, 0x0

    .line 42
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getBillRetrievalRetry()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v5

    .line 43
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getBillRetrievalRetry()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;->getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xc0

    const/4 v11, 0x0

    move-object v1, v0

    .line 37
    invoke-direct/range {v1 .. v11}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto/16 :goto_0

    .line 45
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getInventoryAdjustmentRetry()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    .line 46
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    .line 47
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getCancelAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v3

    .line 48
    sget-object v4, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->ISSUE_INVENTORY_ADJUSTMENT:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    .line 49
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getInventoryAdjustmentRetry()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v5

    .line 50
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getInventoryAdjustmentRetry()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;->getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v6

    const/4 v7, 0x0

    .line 52
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getInventoryAdjustmentRetry()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;

    move-result-object v8

    invoke-virtual {v8}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;->getBillHistory()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v8

    .line 53
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getInventoryAdjustmentRetry()Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$InventoryAdjustmentRetry;->getIdempotencyKey()Ljava/lang/String;

    move-result-object v9

    move-object v1, v0

    .line 45
    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;)V

    goto :goto_0

    .line 55
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getCancelAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/squareup/protos/client/orders/Action;->can_apply_to_line_items:Ljava/lang/Boolean;

    const-string v2, "requireNotNull(cancelAct\u2026).can_apply_to_line_items"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    .line 56
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    .line 57
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getCancelAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v3

    .line 58
    sget-object v4, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->ITEM_SELECTION:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xd0

    const/4 v11, 0x0

    move-object v1, v0

    .line 55
    invoke-direct/range {v1 .. v11}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    .line 62
    :cond_2
    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    .line 63
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v13

    .line 64
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;->getCancelAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v14

    .line 65
    sget-object v15, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->CANCEL_REASON:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0xd0

    const/16 v22, 0x0

    move-object v12, v0

    .line 62
    invoke-direct/range {v12 .. v22}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    return-object v0

    .line 55
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
