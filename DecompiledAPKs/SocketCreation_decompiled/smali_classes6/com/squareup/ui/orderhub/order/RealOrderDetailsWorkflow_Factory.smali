.class public final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealOrderDetailsWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEditTrackingWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubBillLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final orderMarkCanceledWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final orderMarkShippedWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final orderRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->orderHubBillLoaderProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->orderHubAnalyticsProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p7, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->orderMarkShippedWorkflowProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p8, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->orderEditTrackingWorkflowProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p9, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->orderMarkCanceledWorkflowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;",
            ">;)",
            "Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;"
        }
    .end annotation

    .line 74
    new-instance v10, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/util/Device;Lcom/squareup/connectivity/ConnectivityMonitor;Lflow/Flow;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;)Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;
    .locals 11

    .line 82
    new-instance v10, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;-><init>(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/util/Device;Lcom/squareup/connectivity/ConnectivityMonitor;Lflow/Flow;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;
    .locals 10

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ordermanagerdata/OrderRepository;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->orderHubBillLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->orderHubAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->orderMarkShippedWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->orderEditTrackingWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->orderMarkCanceledWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->newInstance(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/util/Device;Lcom/squareup/connectivity/ConnectivityMonitor;Lflow/Flow;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ui/orderhub/order/shipment/OrderMarkShippedWorkflow;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledWorkflow;)Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow_Factory;->get()Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;

    move-result-object v0

    return-object v0
.end method
