.class public final Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "BillHistoryDetailScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Component;,
        Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;,
        Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Presenter;,
        Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0007\u0008\u0007\u0018\u0000 \u00112\u00020\u00012\u00020\u00022\u00020\u0003:\u0004\u0011\u0012\u0013\u0014B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0014J\u0008\u0010\r\u001a\u00020\u000eH\u0016J\u0008\u0010\u000f\u001a\u00020\u0001H\u0016J\u0008\u0010\u0010\u001a\u00020\u000cH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/ui/buyer/PaymentExempt;",
        "billToken",
        "",
        "(Ljava/lang/String;)V",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "getAnalyticsName",
        "Lcom/squareup/analytics/RegisterViewName;",
        "getParentKey",
        "screenLayout",
        "Companion",
        "Component",
        "Controller",
        "Presenter",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion;


# instance fields
.field private final billToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;->Companion:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion;

    .line 192
    sget-object v0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.fromParcel {\u2026reen(billToken)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const-string v0, "billToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;->billToken:Ljava/lang/String;

    return-void
.end method

.method public static final synthetic access$getBillToken$p(Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;)Ljava/lang/String;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;->billToken:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 184
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 185
    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;->billToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->ORDERS_BILL_HISTORY:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 49
    sget-object v0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 44
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 57
    sget v0, Lcom/squareup/orderhub/applet/R$layout;->orderhub_bill_history_view:I

    return v0
.end method
