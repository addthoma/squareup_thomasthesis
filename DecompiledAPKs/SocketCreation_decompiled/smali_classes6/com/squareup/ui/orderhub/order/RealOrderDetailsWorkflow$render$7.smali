.class final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$7;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderDetailsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->render(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ordermanagerdata/ResultState<",
        "+",
        "Lcom/squareup/orders/model/Order;",
        ">;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "result",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "Lcom/squareup/orders/model/Order;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$7;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ordermanagerdata/ResultState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lcom/squareup/orders/model/Order;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "result"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 261
    instance-of v2, v1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    .line 262
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 263
    new-instance v15, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 264
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$7;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v6

    .line 265
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$7;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v7

    .line 266
    check-cast v1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {v1}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/orders/model/Order;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x78

    const/4 v14, 0x0

    move-object v5, v15

    .line 263
    invoke-direct/range {v5 .. v14}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 262
    invoke-static {v2, v15, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_0

    .line 269
    :cond_0
    instance-of v2, v1, Lcom/squareup/ordermanagerdata/ResultState$Failure$VersionMismatchError;

    if-eqz v2, :cond_1

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v2, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$7;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-static {v2}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflowKt;->access$getVersionMismatchState(Lcom/squareup/ui/orderhub/order/OrderDetailsState;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    move-result-object v2

    invoke-static {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_0

    .line 270
    :cond_1
    instance-of v2, v1, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    if-eqz v2, :cond_2

    .line 271
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 272
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$7;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    move-object v6, v5

    check-cast v6, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 273
    new-instance v11, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    .line 274
    check-cast v1, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflowKt;->asOrderUpdateFailure(Lcom/squareup/ordermanagerdata/ResultState$Failure;)Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    move-result-object v1

    .line 275
    new-instance v5, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetrySaveTracking;

    iget-object v12, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$7;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    check-cast v12, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    invoke-virtual {v12}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getTracking()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object v12

    invoke-direct {v5, v12}, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetrySaveTracking;-><init>(Lcom/squareup/ordermanagerdata/TrackingInfo;)V

    check-cast v5, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    .line 276
    sget-object v12, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelSaveTracking;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelSaveTracking;

    check-cast v12, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    .line 273
    invoke-direct {v11, v1, v5, v12}, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)V

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v15, 0xef

    const/16 v16, 0x0

    .line 272
    invoke-static/range {v6 .. v16}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/orders/model/Order$Fulfillment;ZLcom/squareup/ordermanagerdata/TrackingInfo;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    move-result-object v1

    .line 271
    invoke-static {v2, v1, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_2
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$7;->invoke(Lcom/squareup/ordermanagerdata/ResultState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
