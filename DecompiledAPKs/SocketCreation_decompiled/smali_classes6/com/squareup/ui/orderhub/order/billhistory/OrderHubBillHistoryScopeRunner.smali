.class public final Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;
.super Ljava/lang/Object;
.source "OrderHubBillHistoryScopeRunner.kt"

# interfaces
.implements Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;
.implements Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubBillHistoryScopeRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubBillHistoryScopeRunner.kt\ncom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner\n*L\n1#1,178:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0010\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u0002B7\u0008\u0007\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000fJ\u0016\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u00132\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0008\u0010\u0017\u001a\u00020\u0018H\u0016J\u0008\u0010\u0019\u001a\u00020\u0018H\u0016J\u0008\u0010\u001a\u001a\u00020\u0018H\u0016J\u0010\u0010\u001a\u001a\u00020\u00182\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\n\u0010\u001d\u001a\u0004\u0018\u00010\u001cH\u0016J\u0008\u0010\u001e\u001a\u00020\u0016H\u0016J\n\u0010\u001f\u001a\u0004\u0018\u00010\u0011H\u0016J\u0008\u0010 \u001a\u00020\u0018H\u0016J\u0008\u0010!\u001a\u00020\u0018H\u0016J\u000e\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0013H\u0016J\u000e\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u0013H\u0016J\u0012\u0010$\u001a\u00020\u00182\u0008\u0010%\u001a\u0004\u0018\u00010\u0016H\u0016J\u0008\u0010&\u001a\u00020\u0018H\u0016J\u0008\u0010\'\u001a\u00020\u0018H\u0016J\u0012\u0010\'\u001a\u00020\u00182\u0008\u0010%\u001a\u0004\u0018\u00010\u0016H\u0016J\u0008\u0010(\u001a\u00020\u0018H\u0016J\u0008\u0010)\u001a\u00020\u0018H\u0016J\u0012\u0010*\u001a\u00020\u00182\u0008\u0010%\u001a\u0004\u0018\u00010\u0016H\u0016J\u0008\u0010+\u001a\u00020\u0018H\u0016R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;",
        "Lcom/squareup/activity/AbstractActivityCardPresenter$Runner;",
        "Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen$Controller;",
        "res",
        "Lcom/squareup/util/Res;",
        "flow",
        "Lflow/Flow;",
        "orderPrintingDispatcher",
        "Lcom/squareup/print/OrderPrintingDispatcher;",
        "hudToaster",
        "Lcom/squareup/hudtoaster/HudToaster;",
        "orderHubBillLoader",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;",
        "orderHubRefundFlowState",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;",
        "(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;)V",
        "currentTender",
        "Lcom/squareup/billhistory/model/TenderHistory;",
        "billFromBillToken",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
        "billToken",
        "",
        "closeBillHistory",
        "",
        "closeIssueReceiptScreen",
        "closeIssueRefundScreen",
        "refundedBill",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "getBill",
        "getPaymentIdForReceipt",
        "getTender",
        "goBack",
        "goBackBecauseBillIdChanged",
        "onBillIdChanged",
        "onTransactionsHistoryChanged",
        "printGiftReceiptForSelectedTender",
        "tenderId",
        "reprintTicket",
        "showIssueReceiptScreen",
        "showIssueRefundScreen",
        "showSelectBuyerLanguageScreen",
        "showStartRefundScreen",
        "startPrintGiftReceiptFlow",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private currentTender:Lcom/squareup/billhistory/model/TenderHistory;

.field private final flow:Lflow/Flow;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

.field private final orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderPrintingDispatcher"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hudToaster"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubBillLoader"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubRefundFlowState"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->flow:Lflow/Flow;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    return-void
.end method


# virtual methods
.method public billFromBillToken(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
            ">;"
        }
    .end annotation

    const-string v0, "billToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->loadedState$orderhub_applet_release(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public closeBillHistory()V
    .locals 2

    const/4 v0, 0x0

    .line 108
    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->currentTender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeIssueReceiptScreen()V
    .locals 2

    const/4 v0, 0x0

    .line 79
    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->currentTender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/activity/IssueReceiptScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeIssueRefundScreen()V
    .locals 2

    const/4 v0, 0x0

    .line 91
    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->currentTender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/activity/InIssueRefundScope;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeIssueRefundScreen(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    const-string v0, "refundedBill"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->loadFromBill$orderhub_applet_release(Lcom/squareup/billhistory/model/BillHistory;)V

    const/4 p1, 0x0

    .line 86
    check-cast p1, Lcom/squareup/billhistory/model/TenderHistory;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->currentTender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->closeIssueRefundScreen()V

    return-void
.end method

.method public getBill()Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->getBillHistory$orderhub_applet_release()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    return-object v0
.end method

.method public getPaymentIdForReceipt()Ljava/lang/String;
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->getBillHistory$orderhub_applet_release()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->getTender()Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/billhistory/model/BillHistoryId;->forTender(Lcom/squareup/billhistory/model/TenderHistory;)Lcom/squareup/billhistory/model/BillHistoryId;

    move-result-object v0

    const-string v1, "bill.id.forTender(tender)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistoryId;->getPaymentIdForReceipt()Ljava/lang/String;

    move-result-object v0

    const-string v1, "bill.id.forTender(tender\u2026     .paymentIdForReceipt"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    .line 49
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "bill"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public getTender()Lcom/squareup/billhistory/model/TenderHistory;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->currentTender:Lcom/squareup/billhistory/model/TenderHistory;

    return-object v0
.end method

.method public goBack()V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public goBackBecauseBillIdChanged()V
    .locals 1

    .line 170
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public onBillIdChanged()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 159
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.empty()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public onTransactionsHistoryChanged()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 165
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.empty()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public printGiftReceiptForSelectedTender(Ljava/lang/String;)V
    .locals 4

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 61
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_PRINTER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 62
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->printing_hud:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    .line 60
    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 65
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/BillHistory;->getTender(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    sget-object v2, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->GIFT_RECEIPT:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-virtual {v0, v1, p1, v2}, Lcom/squareup/print/OrderPrintingDispatcher;->printReceipt(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/billhistory/model/TenderHistory;Lcom/squareup/print/payload/ReceiptPayload$RenderType;)V

    return-void

    .line 65
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public reprintTicket()V
    .locals 4

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 150
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_PRINTER_CONNECTED:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 151
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/billhistoryui/R$string;->printing_hud:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    .line 149
    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/print/OrderPrintingDispatcher;->reprintTicket(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method

.method public showIssueReceiptScreen()V
    .locals 4

    .line 123
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 125
    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory;->isSplitTender()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/SelectReceiptTenderScreen;

    sget-object v2, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;

    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/SelectReceiptTenderScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 128
    :cond_0
    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->currentTender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/IssueReceiptScreen;

    sget-object v2, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;

    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/activity/IssueReceiptScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Z)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void

    .line 123
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "bill"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public showIssueReceiptScreen(Ljava/lang/String;)V
    .locals 3

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/squareup/billhistory/model/BillHistory;->getTender(Ljava/lang/String;)Lcom/squareup/billhistory/model/TenderHistory;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->currentTender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/activity/IssueReceiptScreen;

    sget-object v1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;

    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/activity/IssueReceiptScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Z)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 74
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public showIssueRefundScreen()V
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->orderHubBillLoader:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillLoader;->getBillHistory$orderhub_applet_release()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->orderHubRefundFlowState:Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;->initiateRefundFlowForBillHistory(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void

    .line 114
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "bill"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public showSelectBuyerLanguageScreen()V
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/buyer/language/BuyerLanguageSelectionBootstrapScreen;->INSTANCE:Lcom/squareup/buyer/language/BuyerLanguageSelectionBootstrapScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showStartRefundScreen(Ljava/lang/String;)V
    .locals 0

    .line 175
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public startPrintGiftReceiptFlow()V
    .locals 3

    .line 138
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 140
    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory;->isSplitTender()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen;

    sget-object v2, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScope;

    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 143
    :cond_0
    iget-object v0, v0, Lcom/squareup/billhistory/model/BillHistory;->tenders:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/billhistory/model/TenderHistory;

    iput-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->currentTender:Lcom/squareup/billhistory/model/TenderHistory;

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->currentTender:Lcom/squareup/billhistory/model/TenderHistory;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/squareup/billhistory/model/TenderHistory;->id:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryScopeRunner;->printGiftReceiptForSelectedTender(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Required value was null."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 138
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "bill"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
