.class final Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderMarkCanceledWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->render(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderMarkCanceledWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderMarkCanceledWorkflow.kt\ncom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$3\n*L\n1#1,390:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
        "result",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$3;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 201
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;

    if-eqz v0, :cond_2

    .line 202
    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory;->isFullyRefunded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 204
    new-instance v7, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledBillRetrievalFailed;

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$3;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 207
    new-instance v4, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    .line 208
    sget-object v0, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$OrderAlreadyRefundedError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$OrderAlreadyRefundedError;

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    .line 209
    sget-object v0, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$ResetOrderDetailsScreen;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$ResetOrderDetailsScreen;

    move-object v10, v0

    check-cast v10, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    move-object v8, v4

    .line 207
    invoke-direct/range {v8 .. v13}, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v0, v7

    .line 204
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledBillRetrievalFailed;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 203
    invoke-virtual {p1, v7}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto/16 :goto_2

    .line 214
    :cond_0
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 216
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$3;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v1

    .line 217
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;->getBill()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 218
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$3;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-static {v2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledStateKt;->getNonEmptySelectedLineItems(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Ljava/util/Map;

    move-result-object v2

    .line 219
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$3;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-static {v3}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledStateKt;->getNonEmptyCancellationReason(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v3

    .line 215
    new-instance v4, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledRefundComplete;

    invoke-direct {v4, v1, p1, v2, v3}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledRefundComplete;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/billhistory/model/BillHistory;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;)V

    .line 214
    invoke-virtual {v0, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_2

    .line 217
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 224
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Failed;

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$BillNotFound;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$BillNotFound;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_0
    if-eqz v0, :cond_4

    .line 226
    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Failed;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Failed;->getFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    sget-object v0, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 227
    sget-object p1, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$ConnectionError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$ConnectionError;

    check-cast p1, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    goto :goto_1

    .line 229
    :cond_4
    sget-object p1, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$BillRetrievalError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$BillRetrievalError;

    check-cast p1, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    .line 231
    :goto_1
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 232
    new-instance v1, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledBillRetrievalFailed;

    .line 233
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$3;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v2

    .line 234
    new-instance v3, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;

    .line 235
    iget-object v4, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$3;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-static {v4}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledStateKt;->getNonEmptyCancellationReason(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v4

    .line 236
    iget-object v5, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$3;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-static {v5}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledStateKt;->getNonEmptySelectedLineItems(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)Ljava/util/Map;

    move-result-object v5

    .line 234
    invoke-direct {v3, v4, v5}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;-><init>(Lcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;)V

    const/4 v4, 0x0

    .line 239
    new-instance v5, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    .line 241
    sget-object v6, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryBillRetrieval;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryBillRetrieval;

    check-cast v6, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    .line 242
    sget-object v7, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelBillRetrieval;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelBillRetrieval;

    check-cast v7, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    .line 239
    invoke-direct {v5, p1, v6, v7}, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)V

    .line 232
    invoke-direct {v1, v2, v3, v4, v5}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$MarkCanceledBillRetrievalFailed;-><init>(Lcom/squareup/orders/model/Order;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledInput$BillRetrievalRetry;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;)V

    .line 231
    invoke-virtual {v0, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_2
    return-object p1

    .line 247
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "bill loader must return success or failure"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$render$3;->invoke(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
