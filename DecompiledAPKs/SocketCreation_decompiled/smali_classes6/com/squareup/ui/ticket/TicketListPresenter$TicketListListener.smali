.class Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;
.super Ljava/lang/Object;
.source "TicketListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketListPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TicketListListener"
.end annotation


# instance fields
.field private final cursorList:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .line 856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 857
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;->cursorList:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;Lcom/squareup/tickets/TicketRowCursorList;)V
    .locals 0

    .line 852
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;->publishTicketCursor(Lcom/squareup/tickets/TicketRowCursorList;)V

    return-void
.end method

.method private publishTicketCursor(Lcom/squareup/tickets/TicketRowCursorList;)V
    .locals 1

    .line 865
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;->cursorList:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method readOnlyTicketCursor()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;"
        }
    .end annotation

    .line 861
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;->cursorList:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/PublishRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
