.class public Lcom/squareup/ui/ticket/MoveTicketPresenter;
.super Lmortar/ViewPresenter;
.source "MoveTicketPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/ticket/MoveTicketView;",
        ">;"
    }
.end annotation


# static fields
.field private static final DESTINATION_TICKET_INFO_KEY:Ljava/lang/String; = "move-ticket-presenter-destination-ticket-info-key"

.field private static final HUD_DELAY_LONG:J = 0x2bcL

.field private static final HUD_DELAY_MEDIUM:J = 0x190L

.field private static final HUD_DELAY_SHORT:J = 0x64L

.field private static final REQUEST_IN_PROGRESS_KEY:Ljava/lang/String; = "move-ticket-presenter-request-in-progress-key"

.field private static final TICKET_COUNT_KEY:Ljava/lang/String; = "move-ticket-presenter-ticket-count-key"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field final blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

.field private destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final flow:Lflow/Flow;

.field private forTransactionTicket:Z

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field final moveInFlightPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            ">;"
        }
    .end annotation
.end field

.field private final moveTicketListener:Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

.field private movingWasSlow:Z

.field private final openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private requestInProgress:Z

.field private final res:Lcom/squareup/util/Res;

.field private final ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

.field private ticketCount:I

.field private final ticketGroupsCache:Lcom/squareup/tickets/TicketGroupsCache;

.field private final ticketListListener:Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

.field private final ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

.field private final tickets:Lcom/squareup/tickets/Tickets;

.field private final ticketsToMove:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/TicketGroupsCache;Lcom/squareup/ui/ticket/TicketListPresenter;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Ljava/util/List;Lcom/squareup/tickets/Tickets;Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/payment/Transaction;Lflow/Flow;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketGroupsCache;",
            "Lcom/squareup/ui/ticket/TicketListPresenter;",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner;",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;",
            "Lcom/squareup/tickets/Tickets;",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
            "Lcom/squareup/payment/Transaction;",
            "Lflow/Flow;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            "Lcom/squareup/hudtoaster/HudToaster;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    move-object v1, p12

    .line 94
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    move-object v2, p1

    .line 95
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketGroupsCache:Lcom/squareup/tickets/TicketGroupsCache;

    move-object v2, p2

    .line 96
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    move-object v2, p3

    .line 97
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    move-object v2, p4

    .line 98
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketsToMove:Ljava/util/List;

    move-object v2, p5

    .line 99
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->tickets:Lcom/squareup/tickets/Tickets;

    move-object v2, p6

    .line 100
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketListListener:Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

    move-object v2, p7

    .line 101
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->moveTicketListener:Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

    move-object v2, p8

    .line 102
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    move-object v2, p9

    .line 103
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    move-object v2, p10

    .line 104
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    move-object v2, p11

    .line 105
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->flow:Lflow/Flow;

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    move-object/from16 v2, p13

    .line 107
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->res:Lcom/squareup/util/Res;

    move-object/from16 v2, p14

    .line 108
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    move-object/from16 v2, p15

    .line 109
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    move-object/from16 v2, p16

    .line 110
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    move-object/from16 v2, p17

    .line 111
    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 112
    new-instance v2, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {v2}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->moveInFlightPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 113
    new-instance v2, Lcom/squareup/caller/BlockingPopupPresenter;

    invoke-direct {v2, p12}, Lcom/squareup/caller/BlockingPopupPresenter;-><init>(Lcom/squareup/thread/executor/MainThread;)V

    iput-object v2, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    return-void
.end method

.method private buildActionBar(Lcom/squareup/ui/ticket/MoveTicketView;Z)V
    .locals 3

    .line 192
    invoke-virtual {p1}, Lcom/squareup/ui/ticket/MoveTicketView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    .line 193
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 194
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->getActionBarTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$C0yHsKvZptEl5YPi6KK4HyuLMKY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$C0yHsKvZptEl5YPi6KK4HyuLMKY;-><init>(Lcom/squareup/ui/ticket/MoveTicketPresenter;)V

    .line 195
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->open_tickets_move:I

    .line 196
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 197
    invoke-virtual {v0, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$uuHUETiRT2lGj374VL50vB_7CvI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$uuHUETiRT2lGj374VL50vB_7CvI;-><init>(Lcom/squareup/ui/ticket/MoveTicketPresenter;)V

    .line 198
    invoke-virtual {p2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 199
    invoke-virtual {p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private getActionBarTitle()Ljava/lang/String;
    .locals 3

    .line 203
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->requestInProgress:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketCount:I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketsToMove:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    .line 205
    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->open_tickets_move_tickets_many:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "number"

    .line 206
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 208
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 210
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_move_tickets_one:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getSelectedTemplate(Ljava/util/List;Lcom/squareup/ui/ticket/TicketInfo;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            ">;",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ")",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;"
        }
    .end annotation

    .line 360
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    .line 361
    iget-object v1, p2, Lcom/squareup/ui/ticket/TicketInfo;->id:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    .line 365
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Template not found for destination: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private getTicketGroupFor(Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;)Lcom/squareup/api/items/TicketGroup;
    .locals 4

    .line 369
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketGroupsCache:Lcom/squareup/tickets/TicketGroupsCache;

    invoke-virtual {v0}, Lcom/squareup/tickets/TicketGroupsCache;->getGroupEntries()Ljava/util/List;

    move-result-object v0

    .line 370
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 371
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getTicketGroupId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 372
    invoke-static {v1}, Lcom/squareup/util/PredefinedTicketsHelper;->buildTicketGroup(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)Lcom/squareup/api/items/TicketGroup;

    move-result-object p1

    return-object p1

    .line 375
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ticket group not found for template: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getToastText(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketsToMove:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object p2, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/orderentry/R$string;->open_tickets_move_tickets_success_one:I

    .line 215
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_move_tickets_success_many:I

    .line 216
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "number"

    .line 217
    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    :goto_0
    const-string v0, "ticket_name"

    .line 220
    invoke-virtual {p2, v0, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 221
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 222
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method static synthetic lambda$null$3(Lcom/squareup/ui/ticket/MoveTicketView;Ljava/util/List;)V
    .locals 0

    .line 250
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/MoveTicketView;->setMoveButtonEnabled(Z)V

    return-void
.end method

.method private listenForMoveUpdates(Lcom/squareup/ui/ticket/MoveTicketView;)V
    .locals 1

    .line 247
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$z4CaT8rlfb4feKzdIWkpYAtD_7s;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$z4CaT8rlfb4feKzdIWkpYAtD_7s;-><init>(Lcom/squareup/ui/ticket/MoveTicketPresenter;Lcom/squareup/ui/ticket/MoveTicketView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private listenForTicketListUpdates(Lcom/squareup/ui/ticket/MoveTicketView;)V
    .locals 1

    .line 239
    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$OYneHeFepwc5-FBSCa1ZbX67UBE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$OYneHeFepwc5-FBSCa1ZbX67UBE;-><init>(Lcom/squareup/ui/ticket/MoveTicketPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private listenForTrasactionTicketUpdates(Lmortar/MortarScope;)V
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->tickets:Lcom/squareup/tickets/Tickets;

    .line 227
    invoke-interface {v0}, Lcom/squareup/tickets/Tickets;->onLocalTicketsUpdated()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$ActjTUSmm2k5m4_untxIMBdZkkI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$ActjTUSmm2k5m4_untxIMBdZkkI;-><init>(Lcom/squareup/ui/ticket/MoveTicketPresenter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 226
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method private movingTransactionTicket()Z
    .locals 2

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketsToMove:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketInfo;->extractIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getTicketId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method doMove(Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 6

    .line 303
    new-instance v5, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$2_6c92ZhvGKT9wYkUKoDYqlRgtE;

    invoke-direct {v5, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$2_6c92ZhvGKT9wYkUKoDYqlRgtE;-><init>(Lcom/squareup/ui/ticket/MoveTicketPresenter;Lcom/squareup/ui/ticket/TicketInfo;)V

    .line 347
    iget-boolean v0, p1, Lcom/squareup/ui/ticket/TicketInfo;->isTemplate:Z

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getTicketTemplates()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->getSelectedTemplate(Ljava/util/List;Lcom/squareup/ui/ticket/TicketInfo;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    move-result-object v2

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    iget-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketsToMove:Ljava/util/List;

    invoke-static {p1}, Lcom/squareup/ui/ticket/TicketInfo;->extractIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 351
    invoke-direct {p0, v2}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->getTicketGroupFor(Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;)Lcom/squareup/api/items/TicketGroup;

    move-result-object v3

    iget-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {p1}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeInfo()Lcom/squareup/permissions/EmployeeInfo;

    move-result-object v4

    .line 350
    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->moveTicketsToTemplate(Ljava/util/List;Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/permissions/EmployeeInfo;Lcom/squareup/tickets/TicketsCallback;)V

    goto :goto_0

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketsToMove:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketInfo;->extractIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iget-object p1, p1, Lcom/squareup/ui/ticket/TicketInfo;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, v5}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->moveTicketsToTicket(Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketsCallback;)V

    :goto_0
    return-void
.end method

.method exit()V
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method getTicketsToMove()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;"
        }
    .end annotation

    .line 176
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketsToMove:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method isTicketSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketInfo;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public synthetic lambda$doMove$7$MoveTicketPresenter(Lcom/squareup/ui/ticket/TicketInfo;Lcom/squareup/tickets/TicketsResult;)V
    .locals 4

    .line 305
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/caller/BlockingPopupPresenter;->dismiss()V

    .line 306
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->moveInFlightPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dismiss()V

    .line 309
    invoke-interface {p2}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/tickets/Tickets$MergeResults;

    .line 310
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->moveTicketListener:Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

    iget-boolean v1, p2, Lcom/squareup/tickets/Tickets$MergeResults;->canceledDueToClosedTicket:Z

    invoke-static {v0, v1}, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;->access$000(Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Z)V

    .line 316
    iget-boolean v0, p2, Lcom/squareup/tickets/Tickets$MergeResults;->canceledDueToClosedTicket:Z

    if-eqz v0, :cond_0

    .line 317
    iget-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->flow:Lflow/Flow;

    iget p2, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketCount:I

    invoke-static {p2}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;->forMove(I)Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;

    move-result-object p2

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void

    .line 323
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->movingWasSlow:Z

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x2bc

    goto :goto_0

    .line 326
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x64

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x190

    .line 330
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v3, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$hQrI1ydMkio79zdxMLONEK45fRk;

    invoke-direct {v3, p0, p1, p2}, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$hQrI1ydMkio79zdxMLONEK45fRk;-><init>(Lcom/squareup/ui/ticket/MoveTicketPresenter;Lcom/squareup/ui/ticket/TicketInfo;Lcom/squareup/tickets/Tickets$MergeResults;)V

    invoke-interface {v2, v3, v0, v1}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    .line 337
    iget-object p2, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/log/tickets/MoveTicket;

    iget-boolean p1, p1, Lcom/squareup/ui/ticket/TicketInfo;->isTemplate:Z

    xor-int/lit8 p1, p1, 0x1

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketsToMove:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, p1, v1}, Lcom/squareup/log/tickets/MoveTicket;-><init>(ZI)V

    invoke-interface {p2, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 340
    iget-boolean p1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->forTransactionTicket:Z

    if-eqz p1, :cond_3

    .line 341
    iget-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    iget-object p2, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->finishMoveTicketsFromCartMenu(Lflow/Flow;)V

    return-void

    .line 344
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    iget-object p2, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->finishMoveTicketsFromEditBar(Lflow/Flow;)V

    return-void
.end method

.method public synthetic lambda$listenForMoveUpdates$4$MoveTicketPresenter(Lcom/squareup/ui/ticket/MoveTicketView;)Lrx/Subscription;
    .locals 2

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->onSelectedTicketsChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$JOezKMPeHYCW9BJuTL-Gd-9jVN4;

    invoke-direct {v1, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$JOezKMPeHYCW9BJuTL-Gd-9jVN4;-><init>(Lcom/squareup/ui/ticket/MoveTicketView;)V

    .line 249
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$listenForTicketListUpdates$2$MoveTicketPresenter()Lrx/Subscription;
    .locals 2

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketListListener:Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;->readOnlyTicketCursor()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$aWtzbxxDqBW4XNoC86svGjFcoLI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$aWtzbxxDqBW4XNoC86svGjFcoLI;-><init>(Lcom/squareup/ui/ticket/MoveTicketPresenter;)V

    .line 241
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$listenForTrasactionTicketUpdates$0$MoveTicketPresenter(Ljava/util/List;)V
    .locals 1

    .line 228
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/LocalTicketUpdateEvent;

    .line 229
    invoke-virtual {v0}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->isUnlockedByUpdate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    iget-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->getSelectedTicketsInfo()Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketInfo;

    .line 231
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->doMove(Lcom/squareup/ui/ticket/TicketInfo;)V

    :cond_1
    return-void
.end method

.method public synthetic lambda$moveTickets$5$MoveTicketPresenter()V
    .locals 4

    const/4 v0, 0x1

    .line 274
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->movingWasSlow:Z

    .line 275
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->moveInFlightPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v1, Lcom/squareup/caller/ProgressPopup$Progress;

    iget-object v2, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->open_tickets_move_in_progress:I

    .line 276
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/caller/ProgressPopup$Progress;-><init>(Ljava/lang/String;)V

    .line 275
    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method public synthetic lambda$null$1$MoveTicketPresenter(Lcom/squareup/tickets/TicketRowCursorList;)V
    .locals 0

    .line 242
    iget-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->pruneSelectedTickets()V

    return-void
.end method

.method public synthetic lambda$null$6$MoveTicketPresenter(Lcom/squareup/ui/ticket/TicketInfo;Lcom/squareup/tickets/Tickets$MergeResults;)V
    .locals 2

    .line 331
    iget-boolean p1, p1, Lcom/squareup/ui/ticket/TicketInfo;->isTemplate:Z

    if-eqz p1, :cond_0

    iget p1, p2, Lcom/squareup/tickets/Tickets$MergeResults;->mergedCount:I

    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    :cond_0
    iget p1, p2, Lcom/squareup/tickets/Tickets$MergeResults;->mergedCount:I

    .line 334
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object p2, p2, Lcom/squareup/tickets/Tickets$MergeResults;->ticketName:Ljava/lang/String;

    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->getToastText(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    invoke-interface {v0, v1, p1, p2}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    return-void
.end method

.method moveTickets()V
    .locals 3

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->getSelectedTicketsInfo()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketInfo;

    const/4 v2, 0x1

    .line 264
    iput-boolean v2, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->requestInProgress:Z

    .line 265
    iget-object v2, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketsToMove:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iput v2, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketCount:I

    .line 266
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/ticket/MoveTicketView;

    .line 267
    invoke-virtual {v2, v1}, Lcom/squareup/ui/ticket/MoveTicketView;->setMoveButtonEnabled(Z)V

    .line 273
    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    new-instance v2, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$DY4lzqVmHmArSLXR4u3CkpWhKn0;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MoveTicketPresenter$DY4lzqVmHmArSLXR4u3CkpWhKn0;-><init>(Lcom/squareup/ui/ticket/MoveTicketPresenter;)V

    invoke-virtual {v1, v2}, Lcom/squareup/caller/BlockingPopupPresenter;->show(Ljava/lang/Runnable;)V

    .line 279
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->movingTransactionTicket()Z

    move-result v1

    iput-boolean v1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->forTransactionTicket:Z

    .line 280
    iget-boolean v1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->forTransactionTicket:Z

    if-eqz v1, :cond_0

    .line 282
    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v2, v0, Lcom/squareup/ui/ticket/TicketInfo;->name:Ljava/lang/String;

    iget-object v0, v0, Lcom/squareup/ui/ticket/TicketInfo;->note:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/squareup/payment/Transaction;->setOpenTicketNameAndNote(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 284
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getDisplayNameOrDefault()Ljava/lang/String;

    move-result-object v2

    .line 283
    invoke-virtual {v0, v1, v2}, Lcom/squareup/print/OrderPrintingDispatcher;->printStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    .line 289
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->updateCurrentTicketBeforeReset()V

    .line 290
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->reset()V

    return-void

    .line 299
    :cond_0
    invoke-virtual {p0, v0}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->doMove(Lcom/squareup/ui/ticket/TicketInfo;)V

    return-void
.end method

.method onBackPressed()Z
    .locals 1

    .line 184
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->requestInProgress:Z

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 117
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketsToMove:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 119
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->listenForTrasactionTicketUpdates(Lmortar/MortarScope;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 123
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "move-ticket-presenter-destination-ticket-info-key"

    .line 126
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketInfo;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

    const-string v0, "move-ticket-presenter-request-in-progress-key"

    .line 127
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->requestInProgress:Z

    const-string v0, "move-ticket-presenter-ticket-count-key"

    .line 128
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketCount:I

    .line 131
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/MoveTicketView;

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->buildActionBar(Lcom/squareup/ui/ticket/MoveTicketView;Z)V

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->hasSelectedTickets()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/MoveTicketView;->setMoveButtonEnabled(Z)V

    .line 134
    invoke-virtual {p1, v1}, Lcom/squareup/ui/ticket/MoveTicketView;->setSearchBarVisible(Z)V

    .line 135
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->requestInProgress:Z

    if-eqz v0, :cond_2

    .line 136
    invoke-virtual {p1, v1}, Lcom/squareup/ui/ticket/MoveTicketView;->setMoveButtonEnabled(Z)V

    .line 139
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->listenForTicketListUpdates(Lcom/squareup/ui/ticket/MoveTicketView;)V

    .line 140
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->listenForMoveUpdates(Lcom/squareup/ui/ticket/MoveTicketView;)V

    .line 143
    iget-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketGroupsCache:Lcom/squareup/tickets/TicketGroupsCache;

    invoke-virtual {p1}, Lcom/squareup/tickets/TicketGroupsCache;->loadAndPost()V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 147
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

    const-string v1, "move-ticket-presenter-destination-ticket-info-key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 149
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->requestInProgress:Z

    const-string v1, "move-ticket-presenter-request-in-progress-key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 150
    iget v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketCount:I

    const-string v1, "move-ticket-presenter-ticket-count-key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method toggleUniqueSelected(Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 1

    .line 171
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->toggleUniqueSelected(Lcom/squareup/ui/ticket/TicketInfo;)V

    return-void
.end method

.method updateView()V
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->ticketPresenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketListPresenter;->updateView()V

    return-void
.end method
