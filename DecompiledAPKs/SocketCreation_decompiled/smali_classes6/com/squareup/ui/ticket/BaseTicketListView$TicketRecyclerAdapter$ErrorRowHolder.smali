.class Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;
.super Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
.source "BaseTicketListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ErrorRowHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
        ">.RangerHolder;"
    }
.end annotation


# instance fields
.field private message:Lcom/squareup/widgets/MessageView;

.field final synthetic this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

.field private title:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 456
    iput-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    .line 457
    sget v0, Lcom/squareup/orderentry/R$layout;->ticket_list_error_row:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;-><init>(Lcom/squareup/ui/RangerRecyclerAdapter;Landroid/view/ViewGroup;I)V

    .line 458
    invoke-direct {p0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;->bindViews()V

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 467
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$id;->ticket_error_title:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;->title:Landroid/widget/TextView;

    .line 468
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$id;->ticket_error_message:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;->message:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;II)V
    .locals 0

    .line 462
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;->title:Landroid/widget/TextView;

    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object p2, p2, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/TicketListPresenter;->getErrorTitle()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 463
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;->message:Lcom/squareup/widgets/MessageView;

    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object p2, p2, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/TicketListPresenter;->getErrorMessage()Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 451
    check-cast p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$ErrorRowHolder;->bindRow(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;II)V

    return-void
.end method
