.class public final Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;
.super Ljava/lang/Object;
.source "TicketDetailPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/TicketDetailPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final cardNameHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCardNameHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final editTicketControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final predefinedTicketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionCompsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionComps;",
            ">;"
        }
    .end annotation
.end field

.field private final voidCompSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCardNameHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionComps;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->editTicketControllerProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->predefinedTicketsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->openTicketsRunnerProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->cardNameHandlerProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p11, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->transactionCompsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/PredefinedTickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/OpenTicketsRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCardNameHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TransactionComps;",
            ">;)",
            "Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;"
        }
    .end annotation

    .line 85
    new-instance v12, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Ljava/lang/Object;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketCardNameHandler;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lflow/Flow;Lcom/squareup/payment/TransactionComps;)Lcom/squareup/ui/ticket/TicketDetailPresenter;
    .locals 13

    .line 93
    new-instance v12, Lcom/squareup/ui/ticket/TicketDetailPresenter;

    move-object v1, p0

    check-cast v1, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;

    move-object v0, v12

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/ticket/TicketDetailPresenter;-><init>(Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketCardNameHandler;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lflow/Flow;Lcom/squareup/payment/TransactionComps;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/TicketDetailPresenter;
    .locals 12

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->editTicketControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->predefinedTicketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/opentickets/PredefinedTickets;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->openTicketsRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/ticket/OpenTicketsRunner;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->cardNameHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/tickets/TicketCardNameHandler;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->voidCompSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->transactionCompsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/payment/TransactionComps;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->newInstance(Ljava/lang/Object;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketCardNameHandler;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lflow/Flow;Lcom/squareup/payment/TransactionComps;)Lcom/squareup/ui/ticket/TicketDetailPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter_Factory;->get()Lcom/squareup/ui/ticket/TicketDetailPresenter;

    move-result-object v0

    return-object v0
.end method
