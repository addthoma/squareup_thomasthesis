.class public Lcom/squareup/ui/ticket/TicketDetailView;
.super Lcom/squareup/ui/ticket/EditTicketView;
.source "TicketDetailView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/ticket/TicketDetailPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/EditTicketView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    const-class p2, Lcom/squareup/ui/ticket/TicketDetailScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketDetailScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/ticket/TicketDetailScreen$Component;->inject(Lcom/squareup/ui/ticket/TicketDetailView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 32
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 36
    invoke-super {p0}, Lcom/squareup/ui/ticket/EditTicketView;->onAttachedToWindow()V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailView;->presenter:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailView;->presenter:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->onCancelSelected()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onCompClicked()V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailView;->presenter:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->onCompClicked()V

    return-void
.end method

.method protected onConvertToCustomTicketClicked()V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailView;->presenter:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->onConvertToCustomTicketClicked()V

    return-void
.end method

.method protected onDeleteClicked()V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailView;->presenter:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->onDeleteClicked()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailView;->presenter:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->dropView(Ljava/lang/Object;)V

    .line 42
    invoke-super {p0}, Lcom/squareup/ui/ticket/EditTicketView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onTicketNameChanged()V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailView;->presenter:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->onTicketNameChanged()V

    return-void
.end method

.method protected onUncompClicked()V
    .locals 1

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailView;->presenter:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->onUncompClicked()V

    return-void
.end method

.method protected onVoidClicked()V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailView;->presenter:Lcom/squareup/ui/ticket/TicketDetailPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->onVoidClicked()V

    return-void
.end method
