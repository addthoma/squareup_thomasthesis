.class Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;
.super Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$BaseViewHolder;
.source "NewTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TicketTemplateViewHolder"
.end annotation


# instance fields
.field private final predefinedTicketRow:Lcom/squareup/noho/NohoRow;

.field final synthetic this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

.field private ticketTemplate:Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 206
    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;->this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    .line 207
    sget v0, Lcom/squareup/orderentry/R$layout;->predefined_tickets_group_or_template_row:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$BaseViewHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;I)V

    .line 208
    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->predefined_ticket_row:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/noho/NohoRow;

    iput-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;->predefinedTicketRow:Lcom/squareup/noho/NohoRow;

    .line 209
    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;->itemView:Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder$1;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/NewTicketView$1;)V
    .locals 0

    .line 200
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;-><init>(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;Landroid/view/ViewGroup;)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;
    .locals 0

    .line 200
    iget-object p0, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;->ticketTemplate:Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    return-object p0
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/NewTicketView$RowType;II)V
    .locals 1

    .line 218
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-nez p2, :cond_0

    .line 219
    iget-object p3, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;->itemView:Landroid/view/View;

    .line 220
    invoke-virtual {p3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v0, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    .line 221
    invoke-virtual {p3, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p3

    float-to-int p3, p3

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    iput p3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 223
    iget-object p3, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {p3, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 224
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;->this$1:Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;->access$300(Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    iput-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;->ticketTemplate:Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    .line 225
    iget-object p1, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;->predefinedTicketRow:Lcom/squareup/noho/NohoRow;

    iget-object p2, p0, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;->ticketTemplate:Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 200
    check-cast p1, Lcom/squareup/ui/ticket/NewTicketView$RowType;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/NewTicketView$NewTicketAdapter$TicketTemplateViewHolder;->bindRow(Lcom/squareup/ui/ticket/NewTicketView$RowType;II)V

    return-void
.end method
