.class public Lcom/squareup/ui/ticket/TicketDetailPresenter;
.super Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;
.source "TicketDetailPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter<",
        "Lcom/squareup/ui/ticket/TicketDetailView;",
        ">;"
    }
.end annotation


# static fields
.field private static final KEY_EDIT_TICKET_STATE:Ljava/lang/String; = "editTicketState"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final cardNameHandler:Lcom/squareup/tickets/TicketCardNameHandler;

.field private editState:Lcom/squareup/ui/ticket/EditTicketState;

.field private isEditingTransactionTicket:Z

.field private final openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

.field private final res:Lcom/squareup/util/Res;

.field private screenData:Lcom/squareup/ui/ticket/TicketDetailScreenData;

.field private ticketGroups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionComps:Lcom/squareup/payment/TransactionComps;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/opentickets/PredefinedTickets;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketCardNameHandler;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lflow/Flow;Lcom/squareup/payment/TransactionComps;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 71
    invoke-direct {p0, p1, p10}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;-><init>(Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketController;Lflow/Flow;)V

    .line 72
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 73
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 74
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

    .line 75
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 76
    iput-object p6, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    .line 77
    iput-object p7, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->res:Lcom/squareup/util/Res;

    .line 78
    iput-object p8, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->cardNameHandler:Lcom/squareup/tickets/TicketCardNameHandler;

    .line 79
    iput-object p9, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    .line 80
    iput-object p11, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->transactionComps:Lcom/squareup/payment/TransactionComps;

    .line 81
    new-instance p1, Lcom/squareup/ui/ticket/EditTicketState;

    invoke-interface {p5}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result p2

    invoke-direct {p1, p2}, Lcom/squareup/ui/ticket/EditTicketState;-><init>(Z)V

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->getNameHint()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/EditTicketState;->setNameHint(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/TicketDetailPresenter;)Lcom/squareup/ui/ticket/OpenTicketsRunner;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/ticket/TicketDetailPresenter;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/TicketDetailPresenter;)Lcom/squareup/ui/ticket/EditTicketState;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    return-object p0
.end method

.method private buildActionBar()V
    .locals 2

    .line 257
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketDetailView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketDetailView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->getActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private getActionBarConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 3

    .line 261
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 262
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->getActionBarTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    .line 263
    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketState;->canSave()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    .line 264
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$AuIC6wGc354ZRsz9IIU5F5Olby4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$AuIC6wGc354ZRsz9IIU5F5Olby4;-><init>(Lcom/squareup/ui/ticket/TicketDetailPresenter;)V

    .line 265
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 266
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 267
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$38YQx7CGjAOio8IKEFX5p6iGbqo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$38YQx7CGjAOio8IKEFX5p6iGbqo;-><init>(Lcom/squareup/ui/ticket/TicketDetailPresenter;)V

    .line 268
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 269
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    return-object v0
.end method

.method private getActionBarTitle()Ljava/lang/String;
    .locals 3

    .line 273
    sget-object v0, Lcom/squareup/ui/ticket/TicketDetailPresenter$4;->$SwitchMap$com$squareup$ui$ticket$EditTicketScreen$TicketAction:[I

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 288
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No glyph for ticket action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 281
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketState;->isConvertingToCustomTicket()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->predefined_tickets_convert_to_custom_ticket:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 284
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketState;->isCustomTicket()Z

    move-result v1

    if-eqz v1, :cond_3

    sget v1, Lcom/squareup/orderentry/R$string;->predefined_tickets_edit_custom_ticket:I

    goto :goto_1

    :cond_3
    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_edit_ticket:I

    :goto_1
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 276
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v1}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v1

    if-eqz v1, :cond_5

    sget v1, Lcom/squareup/orderentry/R$string;->predefined_tickets_custom_ticket:I

    goto :goto_2

    :cond_5
    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_ticket_new_ticket:I

    :goto_2
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private loadTicketGroups()V
    .locals 2

    .line 303
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketDetailPresenter$25mpnhN_aPR4Sf7bP3gd_slDjhs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketDetailPresenter$25mpnhN_aPR4Sf7bP3gd_slDjhs;-><init>(Lcom/squareup/ui/ticket/TicketDetailPresenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private updateButtons()V
    .locals 7

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v0}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isVoidEnabled()Z

    move-result v0

    .line 294
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v1}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasCompableItems()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 295
    :goto_0
    iget-object v4, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v4}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v4}, Lcom/squareup/payment/Transaction;->isComped()Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    .line 296
    :goto_1
    iget-object v5, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-boolean v6, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->isEditingTransactionTicket:Z

    if-eqz v6, :cond_2

    if-nez v0, :cond_2

    const/4 v6, 0x1

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    :goto_2
    invoke-virtual {v5, v6}, Lcom/squareup/ui/ticket/EditTicketState;->setDeleteButtonVisible(Z)V

    .line 297
    iget-object v5, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-boolean v6, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->isEditingTransactionTicket:Z

    if-eqz v6, :cond_3

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_3

    :cond_3
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {v5, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setVoidButtonVisible(Z)V

    .line 298
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-boolean v5, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->isEditingTransactionTicket:Z

    if-eqz v5, :cond_4

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketState;->setCompButtonVisible(Z)V

    .line 299
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-boolean v1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->isEditingTransactionTicket:Z

    if-eqz v1, :cond_5

    if-eqz v4, :cond_5

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    invoke-virtual {v0, v2}, Lcom/squareup/ui/ticket/EditTicketState;->setUncompButtonVisible(Z)V

    return-void
.end method


# virtual methods
.method getNameHint()Ljava/lang/String;
    .locals 2

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v1}, Lcom/squareup/tickets/OpenTicketsSettings;->isSwipeToCreateTicketAllowed()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_ticket_name_hint_swipe_allowed:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_ticket_name_hint:I

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$loadTicketGroups$2$TicketDetailPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 304
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->predefinedTickets:Lcom/squareup/opentickets/PredefinedTickets;

    invoke-interface {v0}, Lcom/squareup/opentickets/PredefinedTickets;->getAllTicketGroups()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketDetailPresenter$E8Wle33MFVV4qqemNNR5WTzYpZg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketDetailPresenter$E8Wle33MFVV4qqemNNR5WTzYpZg;-><init>(Lcom/squareup/ui/ticket/TicketDetailPresenter;)V

    .line 305
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$1$TicketDetailPresenter(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 306
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->ticketGroups:Ljava/util/List;

    .line 307
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketDetailView;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->ticketGroups:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/TicketDetailView;->setTicketGroups(Ljava/util/List;)V

    return-void
.end method

.method public synthetic lambda$onLoad$0$TicketDetailPresenter(Lcom/squareup/ui/ticket/TicketDetailView;Ljava/lang/String;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 1

    .line 156
    iget-object p3, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setCardNotStoredMessageVisible(Z)V

    .line 157
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p3

    if-nez p3, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->hasView()Z

    move-result p3

    if-eqz p3, :cond_0

    .line 158
    iget-object p3, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {p3, p2}, Lcom/squareup/ui/ticket/EditTicketState;->setName(Ljava/lang/String;)V

    .line 160
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketDetailView;->refresh()V

    .line 161
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method onCancelSelected()V
    .locals 2

    .line 183
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketDetailView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketDetailView;->hideKeyboard()V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->EDIT_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    if-ne v0, v1, :cond_0

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->ticketsLogger:Lcom/squareup/log/tickets/OpenTicketsLogger;

    sget-object v1, Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;->CANCEL:Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;

    invoke-virtual {v0, v1}, Lcom/squareup/log/tickets/OpenTicketsLogger;->logExitEditTicket(Lcom/squareup/log/tickets/OpenTicketsLogger$ExitEditTicketResult;)V

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method onCompClicked()V
    .locals 3

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->EDIT_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Can\'t comp a ticket that isn\'t in the transaction!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->USE_PASSCODE_RESTRICTED_DISCOUNT:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/ticket/TicketDetailPresenter$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter$2;-><init>(Lcom/squareup/ui/ticket/TicketDetailPresenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method onConvertToCustomTicketClicked()V
    .locals 3

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketState;->convertToCustomTicket()V

    .line 178
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketDetailView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketDetailView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->getActionBarTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 179
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->loadTicketGroups()V

    return-void
.end method

.method onDeleteClicked()V
    .locals 3

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->EDIT_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Can\'t delete a ticket that isn\'t in the transaction!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_TICKET_VOID:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/ticket/TicketDetailPresenter$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter$1;-><init>(Lcom/squareup/ui/ticket/TicketDetailPresenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 4

    .line 86
    invoke-super {p0, p1}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->cardNameHandler:Lcom/squareup/tickets/TicketCardNameHandler;

    invoke-interface {v0, p1}, Lcom/squareup/tickets/TicketCardNameHandler;->registerToScope(Lmortar/MortarScope;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->EDIT_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->isEditingTransactionTicket:Z

    .line 90
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketDetailScreen;

    .line 91
    iget-object v0, p1, Lcom/squareup/ui/ticket/TicketDetailScreen;->screenData:Lcom/squareup/ui/ticket/TicketDetailScreenData;

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->screenData:Lcom/squareup/ui/ticket/TicketDetailScreenData;

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object v1, p1, Lcom/squareup/ui/ticket/TicketDetailScreen;->preselectedGroup:Lcom/squareup/api/items/TicketGroup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketState;->setSelectedGroup(Lcom/squareup/api/items/TicketGroup;)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-boolean v1, p1, Lcom/squareup/ui/ticket/TicketDetailScreen;->startedFromSwipe:Z

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketState;->setCardNotStoredMessageVisible(Z)V

    .line 96
    iget-boolean v0, p1, Lcom/squareup/ui/ticket/TicketDetailScreen;->startedFromSwipe:Z

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object v1, p1, Lcom/squareup/ui/ticket/TicketDetailScreen;->cardOwnerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketState;->setName(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object p1, p1, Lcom/squareup/ui/ticket/TicketDetailScreen;->cardOwnerName:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/2addr p1, v3

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/EditTicketState;->setShouldSelectName(Z)V

    return-void

    .line 103
    :cond_1
    sget-object p1, Lcom/squareup/ui/ticket/TicketDetailPresenter$4;->$SwitchMap$com$squareup$ui$ticket$EditTicketScreen$TicketAction:[I

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->ordinal()I

    move-result v0

    aget p1, p1, v0

    if-eq p1, v3, :cond_7

    const/4 v0, 0x2

    if-eq p1, v0, :cond_4

    const/4 v0, 0x3

    if-eq p1, v0, :cond_3

    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 127
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->screenData:Lcom/squareup/ui/ticket/TicketDetailScreenData;

    iget-object v0, v0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setName(Ljava/lang/String;)V

    .line 128
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->screenData:Lcom/squareup/ui/ticket/TicketDetailScreenData;

    iget-object v0, v0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->note:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setNote(Ljava/lang/String;)V

    .line 129
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->screenData:Lcom/squareup/ui/ticket/TicketDetailScreenData;

    iget-object v0, v0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->group:Lcom/squareup/api/items/TicketGroup;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setSelectedGroup(Lcom/squareup/api/items/TicketGroup;)V

    .line 130
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->screenData:Lcom/squareup/ui/ticket/TicketDetailScreenData;

    iget-object v0, v0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->template:Lcom/squareup/api/items/TicketTemplate;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setTicketTemplate(Lcom/squareup/api/items/TicketTemplate;)V

    goto :goto_2

    .line 134
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot understand mode "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 117
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOpenTicketName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setName(Ljava/lang/String;)V

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getOpenTicketNote()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/EditTicketState;->setNote(Ljava/lang/String;)V

    .line 119
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getPredefinedTicket()Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;

    move-result-object p1

    if-eqz p1, :cond_7

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object v1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_group:Lcom/squareup/api/items/TicketGroup;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketState;->setSelectedGroup(Lcom/squareup/api/items/TicketGroup;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    iget-object p1, p1, Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;->ticket_template:Lcom/squareup/api/items/TicketTemplate;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/EditTicketState;->setTicketTemplate(Lcom/squareup/api/items/TicketTemplate;)V

    goto :goto_2

    .line 109
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasCustomer()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 110
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->getCustomerContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getFullNameOrNull(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    if-eqz p1, :cond_5

    move-object v1, p1

    goto :goto_1

    :cond_5
    const-string v1, ""

    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/EditTicketState;->setName(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    if-eqz p1, :cond_6

    const/4 v2, 0x1

    :cond_6
    invoke-virtual {v0, v2}, Lcom/squareup/ui/ticket/EditTicketState;->setShouldSelectName(Z)V

    :cond_7
    :goto_2
    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 139
    invoke-super {p0, p1}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 140
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketDetailView;

    if-nez p1, :cond_0

    .line 143
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->updateButtons()V

    goto :goto_0

    :cond_0
    const-string v1, "editTicketState"

    .line 145
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/EditTicketState;

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    .line 147
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->buildActionBar()V

    .line 148
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketDetailView;->setEditState(Lcom/squareup/ui/ticket/EditTicketState;)V

    .line 150
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/EditTicketState;->areTicketGroupsVisible()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 151
    invoke-direct {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->loadTicketGroups()V

    .line 154
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketDetailView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p1

    .line 155
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->cardNameHandler:Lcom/squareup/tickets/TicketCardNameHandler;

    new-instance v2, Lcom/squareup/ui/ticket/-$$Lambda$TicketDetailPresenter$gAG0eebUC3Hp7pn6eiZ3p0NgD5U;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketDetailPresenter$gAG0eebUC3Hp7pn6eiZ3p0NgD5U;-><init>(Lcom/squareup/ui/ticket/TicketDetailPresenter;Lcom/squareup/ui/ticket/TicketDetailView;)V

    invoke-interface {v1, p1, v2}, Lcom/squareup/tickets/TicketCardNameHandler;->setCallback(Lmortar/MortarScope;Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 166
    invoke-super {p0, p1}, Lcom/squareup/ui/ticket/EditTicketScreen$EditTicketPresenter;->onSave(Landroid/os/Bundle;)V

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    const-string v1, "editTicketState"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method onSaveClicked()V
    .locals 7

    .line 240
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketDetailView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketDetailView;->hideKeyboard()V

    .line 241
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->EDIT_EXISTING_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    if-ne v0, v1, :cond_0

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->screenData:Lcom/squareup/ui/ticket/TicketDetailScreenData;

    iget-object v2, v0, Lcom/squareup/ui/ticket/TicketDetailScreenData;->ticketId:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketState;->getName()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketState;->getNote()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    .line 243
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketState;->getSelectedGroup()Lcom/squareup/api/items/TicketGroup;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketState;->getTicketTemplate()Lcom/squareup/api/items/TicketTemplate;

    move-result-object v6

    move-object v1, p0

    .line 242
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->saveTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V

    goto :goto_0

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketState;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketState;->getNote()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v2}, Lcom/squareup/ui/ticket/EditTicketState;->getSelectedGroup()Lcom/squareup/api/items/TicketGroup;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    .line 246
    invoke-virtual {v3}, Lcom/squareup/ui/ticket/EditTicketState;->getTicketTemplate()Lcom/squareup/api/items/TicketTemplate;

    move-result-object v3

    .line 245
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->saveTicket(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;)V

    :goto_0
    return-void
.end method

.method onTicketNameChanged()V
    .locals 3

    .line 171
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketDetailView;

    .line 172
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketDetailView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v2}, Lcom/squareup/ui/ticket/EditTicketState;->canSave()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    .line 173
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->editState:Lcom/squareup/ui/ticket/EditTicketState;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketState;->canSave()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/TicketDetailView;->setButtonsEnabled(Z)V

    return-void
.end method

.method onUncompClicked()V
    .locals 4

    .line 214
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->EDIT_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Can\'t uncomp a ticket that isn\'t in the transaction!"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->transactionComps:Lcom/squareup/payment/TransactionComps;

    invoke-virtual {v0}, Lcom/squareup/payment/TransactionComps;->uncompAllCompedItems()V

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->COMP_CART_UNCOMPED:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->flow:Lflow/Flow;

    new-array v1, v2, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method onVoidClicked()V
    .locals 3

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->ticketAction:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->EDIT_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Can\'t void a ticket that isn\'t in the transaction"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->OPEN_TICKET_VOID:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/ticket/TicketDetailPresenter$3;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ticket/TicketDetailPresenter$3;-><init>(Lcom/squareup/ui/ticket/TicketDetailPresenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method
