.class synthetic Lcom/squareup/ui/ticket/BaseTicketListView$2;
.super Ljava/lang/Object;
.source "BaseTicketListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/BaseTicketListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$tickets$TicketSort:[I

.field static final synthetic $SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketRow:[I

.field static final synthetic $SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketViewHolder:[I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 491
    invoke-static {}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->values()[Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketViewHolder:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketViewHolder:[I

    sget-object v2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->TICKET_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    invoke-virtual {v2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketViewHolder:[I

    sget-object v3, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->TEMPLATE_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    invoke-virtual {v3}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    const/4 v2, 0x3

    :try_start_2
    sget-object v3, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketViewHolder:[I

    sget-object v4, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->TEXT_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    invoke-virtual {v4}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->ordinal()I

    move-result v4

    aput v2, v3, v4
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    const/4 v3, 0x4

    :try_start_3
    sget-object v4, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketViewHolder:[I

    sget-object v5, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->SORT_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    invoke-virtual {v5}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->ordinal()I

    move-result v5

    aput v3, v4, v5
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    const/4 v4, 0x5

    :try_start_4
    sget-object v5, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketViewHolder:[I

    sget-object v6, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->BUTTON_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    invoke-virtual {v6}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->ordinal()I

    move-result v6

    aput v4, v5, v6
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    const/4 v5, 0x6

    :try_start_5
    sget-object v6, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketViewHolder:[I

    sget-object v7, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->SECTION_HEADER_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    invoke-virtual {v7}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->ordinal()I

    move-result v7

    aput v5, v6, v7
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v6, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketViewHolder:[I

    sget-object v7, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->NO_TICKETS_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    invoke-virtual {v7}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->ordinal()I

    move-result v7

    const/4 v8, 0x7

    aput v8, v6, v7
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v6, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketViewHolder:[I

    sget-object v7, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->NO_RESULTS_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    invoke-virtual {v7}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->ordinal()I

    move-result v7

    const/16 v8, 0x8

    aput v8, v6, v7
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    :try_start_8
    sget-object v6, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketViewHolder:[I

    sget-object v7, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->ERROR_HOLDER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;

    invoke-virtual {v7}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;->ordinal()I

    move-result v7

    const/16 v8, 0x9

    aput v8, v6, v7
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_8

    .line 347
    :catch_8
    invoke-static {}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->values()[Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    move-result-object v6

    array-length v6, v6

    new-array v6, v6, [I

    sput-object v6, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketRow:[I

    :try_start_9
    sget-object v6, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketRow:[I

    sget-object v7, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->NEW_TICKET_BUTTON:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v7}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->ordinal()I

    move-result v7

    aput v0, v6, v7
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_9

    :catch_9
    :try_start_a
    sget-object v6, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketRow:[I

    sget-object v7, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->VIEW_ALL_TICKETS_BUTTON:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v7}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->ordinal()I

    move-result v7

    aput v1, v6, v7
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_a

    :catch_a
    :try_start_b
    sget-object v6, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketRow:[I

    sget-object v7, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->OPEN_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v7}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->ordinal()I

    move-result v7

    aput v2, v6, v7
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :catch_b
    :try_start_c
    sget-object v6, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketRow:[I

    sget-object v7, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->YOUR_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v7}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->ordinal()I

    move-result v7

    aput v3, v6, v7
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_c

    :catch_c
    :try_start_d
    sget-object v6, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketRow:[I

    sget-object v7, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->AVAILABLE_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v7}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->ordinal()I

    move-result v7

    aput v4, v6, v7
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_d

    :catch_d
    :try_start_e
    sget-object v4, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$ui$ticket$BaseTicketListView$TicketRow:[I

    sget-object v6, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->OTHER_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {v6}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->ordinal()I

    move-result v6

    aput v5, v4, v6
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_e

    .line 182
    :catch_e
    invoke-static {}, Lcom/squareup/tickets/TicketSort;->values()[Lcom/squareup/tickets/TicketSort;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [I

    sput-object v4, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    :try_start_f
    sget-object v4, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    sget-object v5, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    invoke-virtual {v5}, Lcom/squareup/tickets/TicketSort;->ordinal()I

    move-result v5

    aput v0, v4, v5
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_f

    :catch_f
    :try_start_10
    sget-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    sget-object v4, Lcom/squareup/tickets/TicketSort;->AMOUNT:Lcom/squareup/tickets/TicketSort;

    invoke-virtual {v4}, Lcom/squareup/tickets/TicketSort;->ordinal()I

    move-result v4

    aput v1, v0, v4
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_10

    :catch_10
    :try_start_11
    sget-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    sget-object v1, Lcom/squareup/tickets/TicketSort;->RECENT:Lcom/squareup/tickets/TicketSort;

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketSort;->ordinal()I

    move-result v1

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_11

    :catch_11
    :try_start_12
    sget-object v0, Lcom/squareup/ui/ticket/BaseTicketListView$2;->$SwitchMap$com$squareup$tickets$TicketSort:[I

    sget-object v1, Lcom/squareup/tickets/TicketSort;->EMPLOYEE_NAME:Lcom/squareup/tickets/TicketSort;

    invoke-virtual {v1}, Lcom/squareup/tickets/TicketSort;->ordinal()I

    move-result v1

    aput v3, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_12

    :catch_12
    return-void
.end method
