.class public Lcom/squareup/ui/ticket/SplitTicketCoordinator;
.super Ljava/lang/Object;
.source "SplitTicketCoordinator.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# instance fields
.field private final ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;


# direct methods
.method public constructor <init>(Lcom/squareup/splitticket/TicketSplitter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    return-void
.end method


# virtual methods
.method createNewSplitTicket()Lcom/squareup/splitticket/SplitState;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0}, Lcom/squareup/splitticket/TicketSplitter;->createNewSplitTicket()Lcom/squareup/splitticket/SplitState;

    move-result-object v0

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0}, Lcom/squareup/splitticket/TicketSplitter;->getMortarBundleKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getParentTicketBaseName()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0}, Lcom/squareup/splitticket/TicketSplitter;->getParentTicketBaseName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getSavedTicketsCount()I
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0}, Lcom/squareup/splitticket/TicketSplitter;->getSavedTicketsCount()I

    move-result v0

    return v0
.end method

.method getSelectedEntriesCountAcrossAllTickets()I
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0}, Lcom/squareup/splitticket/TicketSplitter;->getSelectedEntriesCountAcrossAllTickets()I

    move-result v0

    return v0
.end method

.method getSplitTicketsCount()I
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0}, Lcom/squareup/splitticket/TicketSplitter;->getSplitTicketsCount()I

    move-result v0

    return v0
.end method

.method getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0, p1}, Lcom/squareup/splitticket/TicketSplitter;->splitStateForTicket(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    return-object p1
.end method

.method moveSelectedItemsTo(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0, p1}, Lcom/squareup/splitticket/TicketSplitter;->moveSelectedItemsTo(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object p1

    return-object p1
.end method

.method onAsyncStateChange()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/splitticket/SplitState;",
            ">;"
        }
    .end annotation

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0}, Lcom/squareup/splitticket/TicketSplitter;->getOnAsyncStateChange()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0, p1}, Lcom/squareup/splitticket/TicketSplitter;->onEnterScope(Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0}, Lcom/squareup/splitticket/TicketSplitter;->onExitScope()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0, p1}, Lcom/squareup/splitticket/TicketSplitter;->onLoad(Landroid/os/Bundle;)V

    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0, p1}, Lcom/squareup/splitticket/TicketSplitter;->onSave(Landroid/os/Bundle;)V

    return-void
.end method

.method public printAllTickets()V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0}, Lcom/squareup/splitticket/TicketSplitter;->printAllTickets()V

    return-void
.end method

.method public printOneTickets(Ljava/lang/String;)V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0, p1}, Lcom/squareup/splitticket/TicketSplitter;->printOneTicket(Ljava/lang/String;)V

    return-void
.end method

.method removeOneTicket(Ljava/lang/String;)Z
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0, p1}, Lcom/squareup/splitticket/TicketSplitter;->removeOneTicket(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method saveAllTickets()I
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0}, Lcom/squareup/splitticket/TicketSplitter;->saveAllTickets()I

    move-result v0

    return v0
.end method

.method saveOneTicket(Ljava/lang/String;Z)Z
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0, p1, p2}, Lcom/squareup/splitticket/TicketSplitter;->saveOneTicket(Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method splitStates()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/splitticket/SplitState;",
            ">;"
        }
    .end annotation

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0}, Lcom/squareup/splitticket/TicketSplitter;->getSplitStates()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method splitStatesByViewIndex()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lcom/squareup/splitticket/SplitState;",
            ">;"
        }
    .end annotation

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0}, Lcom/squareup/splitticket/TicketSplitter;->getSplitStatesByViewIndex()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method updateTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)Z
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->ticketSplitter:Lcom/squareup/splitticket/TicketSplitter;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/splitticket/TicketSplitter;->updateTicket(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$OpenTicket$PredefinedTicket;)Z

    move-result p1

    return p1
.end method
