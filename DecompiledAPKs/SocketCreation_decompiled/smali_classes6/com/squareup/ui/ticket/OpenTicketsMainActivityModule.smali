.class public abstract Lcom/squareup/ui/ticket/OpenTicketsMainActivityModule;
.super Ljava/lang/Object;
.source "OpenTicketsMainActivityModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/compvoidcontroller/CompVoidControllerModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideOpenTicketsHomeScreenRedirector(Lcom/squareup/ui/ticket/RealOpenTicketsHomeScreenSelector;)Lcom/squareup/ui/ticket/api/OpenTicketsHomeScreenSelector;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTicketCardNameHandler(Lcom/squareup/tickets/RealTicketCardNameHandler;)Lcom/squareup/tickets/TicketCardNameHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
