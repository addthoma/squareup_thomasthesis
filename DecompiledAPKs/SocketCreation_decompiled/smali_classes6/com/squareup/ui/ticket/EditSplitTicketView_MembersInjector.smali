.class public final Lcom/squareup/ui/ticket/EditSplitTicketView_MembersInjector;
.super Ljava/lang/Object;
.source "EditSplitTicketView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/ticket/EditSplitTicketView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/ticket/EditSplitTicketView;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/ui/ticket/EditSplitTicketView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/EditSplitTicketView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/ticket/EditSplitTicketView;Ljava/lang/Object;)V
    .locals 0

    .line 35
    check-cast p1, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditSplitTicketView;->presenter:Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/ticket/EditSplitTicketView;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/ui/ticket/EditSplitTicketView_MembersInjector;->injectPresenter(Lcom/squareup/ui/ticket/EditSplitTicketView;Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/ui/ticket/EditSplitTicketView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/EditSplitTicketView_MembersInjector;->injectMembers(Lcom/squareup/ui/ticket/EditSplitTicketView;)V

    return-void
.end method
