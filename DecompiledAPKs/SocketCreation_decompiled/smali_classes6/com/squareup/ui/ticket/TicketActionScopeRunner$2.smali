.class Lcom/squareup/ui/ticket/TicketActionScopeRunner$2;
.super Ljava/lang/Object;
.source "TicketActionScopeRunner.java"

# interfaces
.implements Lcom/squareup/tickets/TicketsCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/TicketActionScopeRunner;->mergeTicketsTo(Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketsCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/tickets/TicketsCallback<",
        "Lcom/squareup/tickets/Tickets$MergeResults;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

.field final synthetic val$callback:Lcom/squareup/tickets/TicketsCallback;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 0

    .line 239
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner$2;->this$0:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner$2;->val$callback:Lcom/squareup/tickets/TicketsCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/tickets/TicketsResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsResult<",
            "Lcom/squareup/tickets/Tickets$MergeResults;",
            ">;)V"
        }
    .end annotation

    .line 241
    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/Tickets$MergeResults;

    iget-boolean v0, v0, Lcom/squareup/tickets/Tickets$MergeResults;->canceledDueToClosedTicket:Z

    if-nez v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner$2;->this$0:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->access$200(Lcom/squareup/ui/ticket/TicketActionScopeRunner;)Lcom/squareup/tickets/OpenTicketsSettings;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->access$102(Lcom/squareup/ui/ticket/TicketActionScopeRunner;Z)Z

    .line 243
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner$2;->this$0:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->access$300(Lcom/squareup/ui/ticket/TicketActionScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner$2;->this$0:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->access$400(Lcom/squareup/ui/ticket/TicketActionScopeRunner;)Lcom/squareup/ui/ticket/TicketSelection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketSelection;->clearSelectedTickets()V

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner$2;->val$callback:Lcom/squareup/tickets/TicketsCallback;

    invoke-interface {v0, p1}, Lcom/squareup/tickets/TicketsCallback;->call(Lcom/squareup/tickets/TicketsResult;)V

    return-void
.end method
