.class final enum Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;
.super Ljava/lang/Enum;
.source "EditTicketScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "TicketAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

.field public static final enum CREATE_NEW_EMPTY_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

.field public static final enum EDIT_EXISTING_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

.field public static final enum EDIT_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

.field public static final enum SAVE_TRANSACTION_TO_NEW_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;


# instance fields
.field final createsNewTicket:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 160
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "CREATE_NEW_EMPTY_TICKET"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->CREATE_NEW_EMPTY_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    .line 165
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    const-string v3, "EDIT_TRANSACTION_TICKET"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->EDIT_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    .line 170
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    const/4 v3, 0x2

    const-string v4, "EDIT_EXISTING_TICKET"

    invoke-direct {v0, v4, v3, v2}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->EDIT_EXISTING_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    .line 176
    new-instance v0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    const/4 v4, 0x3

    const-string v5, "SAVE_TRANSACTION_TO_NEW_TICKET"

    invoke-direct {v0, v5, v4, v1}, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->SAVE_TRANSACTION_TO_NEW_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    .line 155
    sget-object v5, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->CREATE_NEW_EMPTY_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    aput-object v5, v0, v2

    sget-object v2, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->EDIT_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->EDIT_EXISTING_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->SAVE_TRANSACTION_TO_NEW_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->$VALUES:[Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .line 180
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 181
    iput-boolean p3, p0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->createsNewTicket:Z

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;
    .locals 1

    .line 155
    const-class v0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;
    .locals 1

    .line 155
    sget-object v0, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->$VALUES:[Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    invoke-virtual {v0}, [Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    return-object v0
.end method
