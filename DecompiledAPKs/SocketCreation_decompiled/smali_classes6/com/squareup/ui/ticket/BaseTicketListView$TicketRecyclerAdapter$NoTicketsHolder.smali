.class Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$NoTicketsHolder;
.super Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
.source "BaseTicketListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NoTicketsHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
        ">.RangerHolder;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 430
    iput-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$NoTicketsHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    .line 431
    sget v0, Lcom/squareup/orderentry/R$layout;->ticket_list_simple_no_tickets:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;-><init>(Lcom/squareup/ui/RangerRecyclerAdapter;Landroid/view/ViewGroup;I)V

    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;II)V
    .locals 1

    .line 435
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$NoTicketsHolder;->itemView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 436
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$NoTicketsHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {p2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$1100(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Lcom/squareup/ui/Ranger;

    move-result-object p2

    sget-object p3, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;->OTHER_TICKETS_HEADER:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {p2, p3}, Lcom/squareup/ui/Ranger;->containsRowType(Ljava/lang/Enum;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    const/4 p3, 0x0

    if-eqz p2, :cond_0

    .line 437
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$NoTicketsHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-static {v0}, Lcom/squareup/ui/ticket/BaseTicketListView;->access$600(Lcom/squareup/ui/ticket/BaseTicketListView;)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eqz p2, :cond_1

    .line 438
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$NoTicketsHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    invoke-static {p2}, Lcom/squareup/ui/ticket/BaseTicketListView;->access$600(Lcom/squareup/ui/ticket/BaseTicketListView;)I

    move-result p3

    :cond_1
    iput p3, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 439
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$NoTicketsHolder;->itemView:Landroid/view/View;

    invoke-virtual {p2, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 428
    check-cast p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$NoTicketsHolder;->bindRow(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;II)V

    return-void
.end method
