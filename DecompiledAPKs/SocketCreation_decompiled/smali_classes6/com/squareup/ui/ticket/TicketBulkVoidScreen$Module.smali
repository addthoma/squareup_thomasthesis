.class public abstract Lcom/squareup/ui/ticket/TicketBulkVoidScreen$Module;
.super Ljava/lang/Object;
.source "TicketBulkVoidScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketBulkVoidScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideVoidCompPresenter(Lcom/squareup/ui/ticket/TicketBulkVoidScreen$TicketBulkVoidPresenter;)Lcom/squareup/configure/item/VoidCompPresenter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
