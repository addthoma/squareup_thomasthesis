.class final enum Lcom/squareup/ui/ticket/MergeTicketView$RowType;
.super Ljava/lang/Enum;
.source "MergeTicketView.java"

# interfaces
.implements Lcom/squareup/ui/Ranger$RowType;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MergeTicketView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "RowType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/ticket/MergeTicketView$RowType;",
        ">;",
        "Lcom/squareup/ui/Ranger$RowType<",
        "Lcom/squareup/ui/ticket/MergeTicketView$HolderType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/ticket/MergeTicketView$RowType;

.field public static final enum TICKET:Lcom/squareup/ui/ticket/MergeTicketView$RowType;


# instance fields
.field private final holderType:Lcom/squareup/ui/ticket/MergeTicketView$HolderType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 39
    new-instance v0, Lcom/squareup/ui/ticket/MergeTicketView$RowType;

    sget-object v1, Lcom/squareup/ui/ticket/MergeTicketView$HolderType;->TICKET_HOLDER:Lcom/squareup/ui/ticket/MergeTicketView$HolderType;

    const/4 v2, 0x0

    const-string v3, "TICKET"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/ui/ticket/MergeTicketView$RowType;-><init>(Ljava/lang/String;ILcom/squareup/ui/ticket/MergeTicketView$HolderType;)V

    sput-object v0, Lcom/squareup/ui/ticket/MergeTicketView$RowType;->TICKET:Lcom/squareup/ui/ticket/MergeTicketView$RowType;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/ui/ticket/MergeTicketView$RowType;

    .line 38
    sget-object v1, Lcom/squareup/ui/ticket/MergeTicketView$RowType;->TICKET:Lcom/squareup/ui/ticket/MergeTicketView$RowType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/ticket/MergeTicketView$RowType;->$VALUES:[Lcom/squareup/ui/ticket/MergeTicketView$RowType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/ui/ticket/MergeTicketView$HolderType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MergeTicketView$HolderType;",
            ")V"
        }
    .end annotation

    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    iput-object p3, p0, Lcom/squareup/ui/ticket/MergeTicketView$RowType;->holderType:Lcom/squareup/ui/ticket/MergeTicketView$HolderType;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/ticket/MergeTicketView$RowType;
    .locals 1

    .line 38
    const-class v0, Lcom/squareup/ui/ticket/MergeTicketView$RowType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/MergeTicketView$RowType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/ticket/MergeTicketView$RowType;
    .locals 1

    .line 38
    sget-object v0, Lcom/squareup/ui/ticket/MergeTicketView$RowType;->$VALUES:[Lcom/squareup/ui/ticket/MergeTicketView$RowType;

    invoke-virtual {v0}, [Lcom/squareup/ui/ticket/MergeTicketView$RowType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/ticket/MergeTicketView$RowType;

    return-object v0
.end method


# virtual methods
.method public getHolderType()Lcom/squareup/ui/ticket/MergeTicketView$HolderType;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketView$RowType;->holderType:Lcom/squareup/ui/ticket/MergeTicketView$HolderType;

    return-object v0
.end method

.method public bridge synthetic getHolderType()Ljava/lang/Enum;
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MergeTicketView$RowType;->getHolderType()Lcom/squareup/ui/ticket/MergeTicketView$HolderType;

    move-result-object v0

    return-object v0
.end method
