.class Lcom/squareup/ui/ticket/SplitTicketPresenter$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "SplitTicketPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/SplitTicketPresenter;->onViewCustomerClicked(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/SplitTicketPresenter;

.field final synthetic val$ticketId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/SplitTicketPresenter;Ljava/lang/String;)V
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter$1;->this$0:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    iput-object p2, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter$1;->val$ticketId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 4

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter$1;->this$0:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->access$100(Lcom/squareup/ui/ticket/SplitTicketPresenter;)Lflow/Flow;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/ticket/SplitTicketScreen;->INSTANCE:Lcom/squareup/ui/ticket/SplitTicketScreen;

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter$1;->this$0:Lcom/squareup/ui/ticket/SplitTicketPresenter;

    .line 153
    invoke-static {v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->access$000(Lcom/squareup/ui/ticket/SplitTicketPresenter;)Lcom/squareup/ui/ticket/SplitTicketCoordinator;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/ticket/SplitTicketPresenter$1;->val$ticketId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/squareup/ui/ticket/SplitTicketCoordinator;->getState(Ljava/lang/String;)Lcom/squareup/splitticket/SplitState;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/splitticket/SplitState;->getHoldsCustomer()Lcom/squareup/payment/crm/HoldsCustomer;

    move-result-object v2

    .line 152
    invoke-static {v1, v2}, Lcom/squareup/ui/crm/flow/CrmScope;->forViewingCustomerInSplitTicketScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/payment/crm/HoldsCustomer;)Lcom/squareup/ui/crm/flow/InCrmScope;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
