.class public final enum Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;
.super Ljava/lang/Enum;
.source "TicketBulkDeleteDialogScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Action"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

.field public static final enum DELETE:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

.field public static final enum VOID:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 35
    new-instance v0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    const/4 v1, 0x0

    const-string v2, "DELETE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->DELETE:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    .line 36
    new-instance v0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    const/4 v2, 0x1

    const-string v3, "VOID"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->VOID:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    .line 34
    sget-object v3, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->DELETE:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->VOID:Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->$VALUES:[Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;
    .locals 1

    .line 34
    const-class v0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->$VALUES:[Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    invoke-virtual {v0}, [Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen$Action;

    return-object v0
.end method
