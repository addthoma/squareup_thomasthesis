.class public Lcom/squareup/ui/ticket/MoveTicketView;
.super Lcom/squareup/ui/ticket/BaseTicketListView;
.source "MoveTicketView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private final blockingPopup:Lcom/squareup/caller/BlockingPopup;

.field private final moveInFlightPopup:Lcom/squareup/caller/ProgressPopup;

.field presenter:Lcom/squareup/ui/ticket/MoveTicketPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/BaseTicketListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    new-instance p2, Lcom/squareup/caller/ProgressPopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/ProgressPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/ticket/MoveTicketView;->moveInFlightPopup:Lcom/squareup/caller/ProgressPopup;

    .line 38
    new-instance p2, Lcom/squareup/caller/BlockingPopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/BlockingPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/ticket/MoveTicketView;->blockingPopup:Lcom/squareup/caller/BlockingPopup;

    .line 39
    const-class p2, Lcom/squareup/ui/ticket/MoveTicketScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/MoveTicketScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/ticket/MoveTicketScreen$Component;->inject(Lcom/squareup/ui/ticket/MoveTicketView;)V

    return-void
.end method

.method private selectTicketOrTemplate(Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView;->presenter:Lcom/squareup/ui/ticket/MoveTicketPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->toggleUniqueSelected(Lcom/squareup/ui/ticket/TicketInfo;)V

    .line 126
    iget-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketView;->presenter:Lcom/squareup/ui/ticket/MoveTicketPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->updateView()V

    return-void
.end method

.method private spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;
    .locals 2

    .line 130
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v0, v1}, Lcom/squareup/marketfont/MarketSpanKt;->marketSpanFor(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;)Lcom/squareup/fonts/FontSpan;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 113
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected getTicketRowText()Ljava/lang/CharSequence;
    .locals 4

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView;->presenter:Lcom/squareup/ui/ticket/MoveTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->getTicketsToMove()Ljava/util/List;

    move-result-object v0

    .line 81
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    .line 83
    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketInfo;->extractNames(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    add-int/lit8 v2, v1, -0x1

    .line 84
    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 85
    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    const-string v1, ", "

    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketView;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->open_tickets_move_ticket_list_many:I

    invoke-interface {v1, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 88
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MoveTicketView;->spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    const-string v3, "names"

    invoke-virtual {v1, v3, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 89
    invoke-direct {p0, v2}, Lcom/squareup/ui/ticket/MoveTicketView;->spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v1

    const-string v2, "last"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v3, 0x0

    if-ne v1, v2, :cond_1

    .line 94
    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketInfo;->extractNames(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->open_tickets_move_ticket_list_two:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 96
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-direct {p0, v2}, Lcom/squareup/ui/ticket/MoveTicketView;->spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v2

    const-string v3, "first"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const/4 v2, 0x1

    .line 97
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MoveTicketView;->spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    const-string v2, "second"

    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 98
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    .line 101
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketView;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->open_tickets_move_ticket_list_one:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 102
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketInfo;

    iget-object v0, v0, Lcom/squareup/ui/ticket/TicketInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MoveTicketView;->spanMedium(Ljava/lang/CharSequence;)Landroid/text/Spannable;

    move-result-object v0

    const-string v2, "name"

    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .line 53
    invoke-super {p0}, Lcom/squareup/ui/ticket/BaseTicketListView;->onAttachedToWindow()V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView;->presenter:Lcom/squareup/ui/ticket/MoveTicketPresenter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->moveInFlightPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketView;->moveInFlightPopup:Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView;->presenter:Lcom/squareup/ui/ticket/MoveTicketPresenter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketView;->blockingPopup:Lcom/squareup/caller/BlockingPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/caller/BlockingPopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView;->presenter:Lcom/squareup/ui/ticket/MoveTicketPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView;->presenter:Lcom/squareup/ui/ticket/MoveTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onBindTicketRowTablet(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;Landroid/widget/CompoundButton;)V
    .locals 1

    const/4 v0, 0x0

    .line 108
    invoke-virtual {p2, v0}, Landroid/widget/CompoundButton;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView;->presenter:Lcom/squareup/ui/ticket/MoveTicketPresenter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/TicketSelection;->ticketInfoFromRow(Lcom/squareup/tickets/TicketRowCursorList$TicketRow;)Lcom/squareup/ui/ticket/TicketInfo;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->isTicketSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z

    move-result p1

    invoke-virtual {p2, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView;->presenter:Lcom/squareup/ui/ticket/MoveTicketPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/MoveTicketPresenter;->dropView(Ljava/lang/Object;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView;->presenter:Lcom/squareup/ui/ticket/MoveTicketPresenter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->moveInFlightPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketView;->moveInFlightPopup:Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView;->presenter:Lcom/squareup/ui/ticket/MoveTicketPresenter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/MoveTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketView;->blockingPopup:Lcom/squareup/caller/BlockingPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/caller/BlockingPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 63
    invoke-super {p0}, Lcom/squareup/ui/ticket/BaseTicketListView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 43
    invoke-super {p0}, Lcom/squareup/ui/ticket/BaseTicketListView;->onFinishInflate()V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    sget v1, Lcom/squareup/orderentry/R$id;->move_ticket_view_recycler_view:I

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setId(I)V

    return-void
.end method

.method protected onHideProgress()V
    .locals 1

    const/4 v0, 0x0

    .line 75
    invoke-virtual {p0, v0}, Lcom/squareup/ui/ticket/MoveTicketView;->setSearchBarVisible(Z)V

    const/4 v0, 0x1

    .line 76
    invoke-virtual {p0, v0}, Lcom/squareup/ui/ticket/MoveTicketView;->setListVisible(Z)V

    return-void
.end method

.method protected onTemplateClicked(Landroid/widget/CompoundButton;Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 0

    .line 71
    invoke-direct {p0, p2}, Lcom/squareup/ui/ticket/MoveTicketView;->selectTicketOrTemplate(Lcom/squareup/ui/ticket/TicketInfo;)V

    return-void
.end method

.method protected onTicketClicked(Landroid/widget/CompoundButton;Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 0

    .line 67
    invoke-direct {p0, p2}, Lcom/squareup/ui/ticket/MoveTicketView;->selectTicketOrTemplate(Lcom/squareup/ui/ticket/TicketInfo;)V

    return-void
.end method

.method setMoveButtonEnabled(Z)V
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method
