.class public Lcom/squareup/ui/ticket/OpenTicketsRunner;
.super Ljava/lang/Object;
.source "OpenTicketsRunner.java"


# instance fields
.field private final eventSink:Lcom/squareup/badbus/BadEventSink;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/badbus/BadEventSink;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner;->eventSink:Lcom/squareup/badbus/BadEventSink;

    return-void
.end method

.method private calculateHomeScreen(Lflow/History;)Lcom/squareup/container/Command;
    .locals 2

    .line 144
    invoke-direct {p0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->getPreferredHomeScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lflow/History;->peek(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/container/ContainerTreeKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p1, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1

    .line 148
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->getPreferredHomeScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/container/Command;->set(Lflow/History;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method

.method private getPreferredHomeScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    invoke-static {}, Lcom/squareup/ui/ticket/MasterDetailTicketScreen;->forLoadTicket()Lcom/squareup/ui/ticket/MasterDetailTicketScreen;

    move-result-object v0

    return-object v0

    .line 230
    :cond_0
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreen;->LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryScreen;

    return-object v0
.end method

.method private varargs goBackPastAndEnsureDefaultHome(Lflow/Flow;[Ljava/lang/Class;)V
    .locals 2

    .line 189
    new-instance v0, Lcom/squareup/container/CalculatedKey;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$OpenTicketsRunner$zf_1zrocPS4FBpBWZrwLIAliUqU;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/ticket/-$$Lambda$OpenTicketsRunner$zf_1zrocPS4FBpBWZrwLIAliUqU;-><init>(Lcom/squareup/ui/ticket/OpenTicketsRunner;[Ljava/lang/Class;)V

    const-string p2, "finishCreateNewEmptyTicketAndExit"

    invoke-direct {v0, p2, v1}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private varargs goBackPastAndEnsureHome(Lflow/Flow;[Ljava/lang/Class;)V
    .locals 2

    .line 172
    new-instance v0, Lcom/squareup/container/CalculatedKey;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$OpenTicketsRunner$knu3QB1Mt7nVEZf03942iPoTxB4;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/ticket/-$$Lambda$OpenTicketsRunner$knu3QB1Mt7nVEZf03942iPoTxB4;-><init>(Lcom/squareup/ui/ticket/OpenTicketsRunner;[Ljava/lang/Class;)V

    const-string p2, "goBackPastAndEnsureHome"

    invoke-direct {v0, p2, v1}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private static varargs goBackPastTypesToBackstop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V
    .locals 2

    .line 206
    new-instance v0, Lcom/squareup/container/CalculatedKey;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$OpenTicketsRunner$vxLFhpQquRug6577bfXundQnzpI;

    invoke-direct {v1, p1, p2}, Lcom/squareup/ui/ticket/-$$Lambda$OpenTicketsRunner$vxLFhpQquRug6577bfXundQnzpI;-><init>(Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    const-string p1, "goBackPastTypesToBackstop"

    invoke-direct {v0, p1, v1}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {p0, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private goToHomeScreenAndCloseCart(Lflow/Flow;)V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    .line 157
    const-class v1, Lcom/squareup/ui/ticket/TicketScreen;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-class v1, Lcom/squareup/ui/cart/CartContainerScreen;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner;->eventSink:Lcom/squareup/badbus/BadEventSink;

    new-instance v0, Lcom/squareup/payment/OrderEntryEvents$TicketRemoved;

    invoke-direct {v0}, Lcom/squareup/payment/OrderEntryEvents$TicketRemoved;-><init>()V

    invoke-interface {p1, v0}, Lcom/squareup/badbus/BadEventSink;->post(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$goBackPastTypesToBackstop$2(Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;Lflow/History;)Lcom/squareup/container/Command;
    .locals 0

    .line 207
    invoke-virtual {p2}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p2

    .line 208
    invoke-static {p2, p0, p1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->popTypesToBackstop(Lflow/History$Builder;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)Z

    .line 209
    invoke-virtual {p2}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p0

    sget-object p1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-static {p0, p1}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p0

    return-object p0
.end method

.method private static varargs popTypesToBackstop(Lflow/History$Builder;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)Z
    .locals 1

    .line 220
    :goto_0
    invoke-virtual {p0}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/squareup/util/Objects;->isInstance(Ljava/lang/Object;[Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 221
    invoke-virtual {p0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    goto :goto_0

    .line 223
    :cond_0
    invoke-virtual {p0}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/squareup/container/ContainerTreeKey;->equals(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method finishBulkVoidTicketsFromEditBar(Lflow/Flow;)V
    .locals 3

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    new-array v0, v2, [Ljava/lang/Class;

    .line 97
    const-class v2, Lcom/squareup/ui/ticket/TicketBulkVoidScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    :cond_0
    new-array v0, v2, [Ljava/lang/Class;

    .line 99
    const-class v2, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method finishCreateNewEmptyTicketAndExit(Lflow/Flow;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 42
    const-class v1, Lcom/squareup/ui/ticket/TicketScreen;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->goBackPastAndEnsureDefaultHome(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method finishDeleteTicketsFromCartMenu(Lflow/Flow;)V
    .locals 3

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    .line 73
    const-class v2, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->goBackPastAndEnsureHome(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    .line 75
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->goToHomeScreenAndCloseCart(Lflow/Flow;)V

    :goto_0
    return-void
.end method

.method finishDeleteTicketsFromDialog(Lflow/Flow;)V
    .locals 3

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    new-array v0, v2, [Ljava/lang/Class;

    .line 81
    const-class v2, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    :cond_0
    new-array v0, v2, [Ljava/lang/Class;

    .line 83
    const-class v2, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method finishEditExistingTicketAndExit(Lflow/Flow;)V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    .line 50
    const-class v1, Lcom/squareup/ui/ticket/TicketScreen;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method finishEditTransactionTicketAndExit(Lflow/Flow;)V
    .locals 4

    .line 46
    invoke-direct {p0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->getPreferredHomeScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/ticket/TicketScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->goBackPastTypesToBackstop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method finishMergeTicketsFromCartMenu(Lflow/Flow;)V
    .locals 4

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    .line 105
    const-class v3, Lcom/squareup/ui/ticket/TicketListScreen;

    aput-object v3, v0, v1

    const-class v1, Lcom/squareup/ui/ticket/MergeTicketScreen;

    aput-object v1, v0, v2

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->goBackPastAndEnsureHome(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    :cond_0
    new-array v0, v2, [Ljava/lang/Class;

    .line 107
    const-class v2, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method finishMergeTicketsFromEditBar(Lflow/Flow;)V
    .locals 3

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    new-array v0, v2, [Ljava/lang/Class;

    .line 113
    const-class v2, Lcom/squareup/ui/ticket/MergeTicketScreen;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->goBackPastAndEnsureHome(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    :cond_0
    new-array v0, v2, [Ljava/lang/Class;

    .line 115
    const-class v2, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method finishMoveTicketsFromCartMenu(Lflow/Flow;)V
    .locals 4

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    .line 121
    const-class v3, Lcom/squareup/ui/ticket/TicketListScreen;

    aput-object v3, v0, v1

    const-class v1, Lcom/squareup/ui/ticket/MoveTicketScreen;

    aput-object v1, v0, v2

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->goBackPastAndEnsureHome(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    :cond_0
    new-array v0, v2, [Ljava/lang/Class;

    .line 123
    const-class v2, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method finishMoveTicketsFromEditBar(Lflow/Flow;)V
    .locals 3

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    new-array v0, v2, [Ljava/lang/Class;

    .line 129
    const-class v2, Lcom/squareup/ui/ticket/MoveTicketScreen;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->goBackPastAndEnsureHome(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    :cond_0
    new-array v0, v2, [Ljava/lang/Class;

    .line 131
    const-class v2, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method finishSaveForSplitTicket(Lflow/Flow;)V
    .locals 4

    .line 67
    sget-object v0, Lcom/squareup/ui/ticket/SplitTicketScreen;->INSTANCE:Lcom/squareup/ui/ticket/SplitTicketScreen;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/ticket/TicketScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p1, v0, v1}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method finishSaveTransactionToNewTicket(Lflow/Flow;ZZ)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-nez p3, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->getPreferredHomeScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object p2

    new-array p3, v1, [Ljava/lang/Class;

    const-class v1, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object v1, p3, v0

    invoke-static {p1, p2, p3}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->goBackPastTypesToBackstop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void

    :cond_0
    if-eqz p2, :cond_1

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Class;

    .line 60
    const-class p3, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object p3, p2, v0

    const-class p3, Lcom/squareup/ui/cart/CartContainerScreen;

    aput-object p3, p2, v1

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->goBackPastAndEnsureHome(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    :cond_1
    new-array p2, v1, [Ljava/lang/Class;

    .line 62
    const-class p3, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object p3, p2, v0

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->goBackPastAndEnsureHome(Lflow/Flow;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method finishTicketTransfer(Lflow/Flow;)V
    .locals 3

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    new-array v0, v2, [Ljava/lang/Class;

    .line 137
    const-class v2, Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->goBackPastAndEnsureHome(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    :cond_0
    new-array v0, v2, [Ljava/lang/Class;

    .line 139
    const-class v2, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :goto_0
    return-void
.end method

.method finishVoidTicketFromCartMenu(Lflow/Flow;)V
    .locals 3

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/ticket/OpenTicketsRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    .line 89
    const-class v2, Lcom/squareup/ui/ticket/TicketScreen;

    aput-object v2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->goBackPastAndEnsureHome(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    .line 91
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->goToHomeScreenAndCloseCart(Lflow/Flow;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$goBackPastAndEnsureDefaultHome$1$OpenTicketsRunner([Ljava/lang/Class;Lflow/History;)Lcom/squareup/container/Command;
    .locals 1

    .line 190
    invoke-virtual {p2}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p2

    .line 191
    sget-object v0, Lcom/squareup/orderentry/OrderEntryScreen;->LAST_SELECTED:Lcom/squareup/orderentry/OrderEntryScreen;

    invoke-static {p2, v0, p1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->popTypesToBackstop(Lflow/History$Builder;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 193
    invoke-virtual {p2}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object p2, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-static {p1, p2}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1

    .line 196
    :cond_0
    invoke-virtual {p2}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->calculateHomeScreen(Lflow/History;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$goBackPastAndEnsureHome$0$OpenTicketsRunner([Ljava/lang/Class;Lflow/History;)Lcom/squareup/container/Command;
    .locals 1

    .line 173
    invoke-virtual {p2}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p2

    .line 175
    invoke-direct {p0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->getPreferredHomeScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-static {p2, v0, p1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->popTypesToBackstop(Lflow/History$Builder;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 177
    invoke-virtual {p2}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object p2, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-static {p1, p2}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1

    .line 180
    :cond_0
    invoke-virtual {p2}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->calculateHomeScreen(Lflow/History;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method
