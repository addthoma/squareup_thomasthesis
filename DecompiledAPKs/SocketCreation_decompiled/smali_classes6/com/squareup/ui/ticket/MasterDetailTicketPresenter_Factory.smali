.class public final Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;
.super Ljava/lang/Object;
.source "MasterDetailTicketPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final busProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final cardNameHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCardNameHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final displayModeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final groupListListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;",
            ">;"
        }
    .end annotation
.end field

.field private final groupPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/GroupListPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final lockOrClockButtonHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/LockOrClockButtonHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final mergeTicketListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;",
            ">;"
        }
    .end annotation
.end field

.field private final moveTicketListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketActionScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketCountsCacheProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCountsCache;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketCreatedListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketListListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketModeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/GroupListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCardNameHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/LockOrClockButtonHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 116
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->groupPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 117
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->groupListListenerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 118
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 119
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketListListenerProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 120
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketCreatedListenerProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 121
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->mergeTicketListenerProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 122
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->moveTicketListenerProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 123
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 124
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketCountsCacheProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 125
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketActionScopeRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 126
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketScopeRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 127
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->cardNameHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 128
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 129
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 130
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketModeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 131
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->displayModeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 132
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 133
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 134
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 135
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 136
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 137
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 138
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 139
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 140
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->passcodeGatekeeperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 141
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->lockOrClockButtonHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 142
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 143
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 144
    iput-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;
    .locals 31
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/GroupListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCountsCache;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketActionScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/TicketScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/TicketCardNameHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/LockOrClockButtonHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)",
            "Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    .line 177
    new-instance v30, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;

    move-object/from16 v0, v30

    invoke-direct/range {v0 .. v29}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v30
.end method

.method public static newInstance(Lcom/squareup/ui/ticket/GroupListPresenter;Ljava/lang/Object;Lcom/squareup/ui/ticket/TicketListPresenter;Ljava/lang/Object;Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/Tickets;Lcom/squareup/tickets/TicketCountsCache;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/tickets/TicketCardNameHandler;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/log/tickets/OpenTicketsLogger;Ljava/lang/Object;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Lcom/squareup/ui/main/PosContainer;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/main/LockOrClockButtonHelper;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;
    .locals 31

    move-object/from16 v1, p0

    move-object/from16 v3, p2

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    .line 194
    new-instance v30, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    move-object/from16 v0, v30

    move-object/from16 v2, p1

    check-cast v2, Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;

    move-object/from16 v4, p3

    check-cast v4, Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;

    move-object/from16 v15, p14

    check-cast v15, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;

    invoke-direct/range {v0 .. v29}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;-><init>(Lcom/squareup/ui/ticket/GroupListPresenter;Lcom/squareup/ui/ticket/GroupListPresenter$GroupListListener;Lcom/squareup/ui/ticket/TicketListPresenter;Lcom/squareup/ui/ticket/TicketListPresenter$TicketListListener;Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/Tickets;Lcom/squareup/tickets/TicketCountsCache;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/tickets/TicketCardNameHandler;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$TicketMode;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Lcom/squareup/ui/main/PosContainer;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/main/LockOrClockButtonHelper;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-object v30
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;
    .locals 31

    move-object/from16 v0, p0

    .line 149
    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->groupPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/ticket/GroupListPresenter;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->groupListListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/ui/ticket/TicketListPresenter;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketListListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketCreatedListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->mergeTicketListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->moveTicketListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/tickets/Tickets;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketCountsCacheProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/tickets/TicketCountsCache;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketActionScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/ticket/TicketScopeRunner;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->cardNameHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/tickets/TicketCardNameHandler;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/log/tickets/OpenTicketsLogger;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->ticketModeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v16

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->displayModeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->busProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/badbus/BadEventSink;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/thread/executor/MainThread;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->passcodeGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->lockOrClockButtonHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/ui/main/LockOrClockButtonHelper;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/print/PrinterStations;

    iget-object v1, v0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static/range {v2 .. v30}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->newInstance(Lcom/squareup/ui/ticket/GroupListPresenter;Ljava/lang/Object;Lcom/squareup/ui/ticket/TicketListPresenter;Ljava/lang/Object;Lcom/squareup/ui/ticket/EditTicketScreen$TicketCreatedListener;Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/Tickets;Lcom/squareup/tickets/TicketCountsCache;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/ui/ticket/TicketScopeRunner;Lcom/squareup/tickets/TicketCardNameHandler;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/log/tickets/OpenTicketsLogger;Ljava/lang/Object;Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$DisplayMode;Lcom/squareup/ui/main/PosContainer;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/badbus/BadEventSink;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/util/Device;Lcom/squareup/util/Res;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/main/LockOrClockButtonHelper;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/print/PrinterStations;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter_Factory;->get()Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    move-result-object v0

    return-object v0
.end method
