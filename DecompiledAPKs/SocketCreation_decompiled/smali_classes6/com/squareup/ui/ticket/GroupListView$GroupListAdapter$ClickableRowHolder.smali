.class Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;
.super Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
.source "GroupListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClickableRowHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/GroupListView$GroupRow;",
        "Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;",
        ">.RangerHolder;"
    }
.end annotation


# instance fields
.field private final adapter:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

.field protected row:Lcom/squareup/ui/account/view/SmartLineRow;

.field final synthetic this$0:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;Landroid/view/ViewGroup;Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;)V
    .locals 1

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->this$0:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    .line 80
    sget v0, Lcom/squareup/orderentry/R$layout;->master_group_row:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;-><init>(Lcom/squareup/ui/RangerRecyclerAdapter;Landroid/view/ViewGroup;I)V

    .line 81
    iput-object p3, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->adapter:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    .line 82
    invoke-direct {p0}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->bindViews()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;)Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->adapter:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    return-object p0
.end method

.method private bindViews()V
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/orderentry/R$id;->master_group_row:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    iput-object v0, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/GroupListView$GroupRow;II)V
    .locals 0

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setPreserveValueText(Z)V

    .line 92
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    iget-object p2, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->adapter:Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;

    invoke-virtual {p2, p3}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter;->isSectionSelected(I)Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setActivated(Z)V

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->row:Lcom/squareup/ui/account/view/SmartLineRow;

    new-instance p2, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder$1;

    invoke-direct {p2, p0, p3}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder$1;-><init>(Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;I)V

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 73
    check-cast p1, Lcom/squareup/ui/ticket/GroupListView$GroupRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/GroupListView$GroupListAdapter$ClickableRowHolder;->bindRow(Lcom/squareup/ui/ticket/GroupListView$GroupRow;II)V

    return-void
.end method

.method protected onRowClicked(Landroid/view/View;)V
    .locals 0

    return-void
.end method
