.class Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TicketNoteViewHolder;
.super Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;
.source "SplitTicketRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TicketNoteViewHolder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

.field private final ticketNote:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;Landroid/view/View;)V
    .locals 0

    .line 289
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TicketNoteViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    .line 290
    invoke-direct {p0, p2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 291
    sget p1, Lcom/squareup/orderentry/R$id;->ticket_note:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TicketNoteViewHolder;->ticketNote:Landroid/widget/TextView;

    .line 292
    iget-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TicketNoteViewHolder;->ticketNote:Landroid/widget/TextView;

    invoke-static {}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$300()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    return-void
.end method


# virtual methods
.method public bind()V
    .locals 3

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TicketNoteViewHolder;->ticketNote:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TicketNoteViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TicketNoteViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getTicketNote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
