.class public Lcom/squareup/ui/ticket/TicketActionScopeRunner;
.super Ljava/lang/Object;
.source "TicketActionScopeRunner.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# instance fields
.field private final deletedTicketIds:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final inEditMode:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final inEditModeKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private isExitingTicketActionPath:Z

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private final moveTicketListener:Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final res:Lcom/squareup/util/Res;

.field private final ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

.field private final ticketStore:Lcom/squareup/tickets/TicketStore;

.field private final tickets:Lcom/squareup/tickets/Tickets;

.field private final ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final voidController:Lcom/squareup/compvoidcontroller/VoidController;

.field private final voidedTicketIds:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketSelection;Lcom/squareup/tickets/Tickets;Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/compvoidcontroller/VoidController;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/util/Res;Lcom/squareup/BundleKey;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/tickets/TicketStore;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/TicketsLoader;",
            "Lcom/squareup/ui/ticket/TicketSelection;",
            "Lcom/squareup/tickets/Tickets;",
            "Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/compvoidcontroller/VoidController;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            "Lcom/squareup/tickets/TicketStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->deletedTicketIds:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 67
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->voidedTicketIds:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 83
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    .line 84
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    .line 85
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->tickets:Lcom/squareup/tickets/Tickets;

    .line 86
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->moveTicketListener:Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

    .line 87
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 88
    iput-object p6, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->voidController:Lcom/squareup/compvoidcontroller/VoidController;

    .line 89
    iput-object p7, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    .line 90
    iput-object p8, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 91
    iput-object p9, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->res:Lcom/squareup/util/Res;

    .line 92
    iput-object p10, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->inEditModeKey:Lcom/squareup/BundleKey;

    .line 93
    iput-object p11, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    .line 94
    iput-object p12, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketStore:Lcom/squareup/tickets/TicketStore;

    const/4 p1, 0x0

    .line 96
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->inEditMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/ticket/TicketActionScopeRunner;)Lcom/squareup/compvoidcontroller/VoidController;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->voidController:Lcom/squareup/compvoidcontroller/VoidController;

    return-object p0
.end method

.method static synthetic access$102(Lcom/squareup/ui/ticket/TicketActionScopeRunner;Z)Z
    .locals 0

    .line 51
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isExitingTicketActionPath:Z

    return p1
.end method

.method static synthetic access$200(Lcom/squareup/ui/ticket/TicketActionScopeRunner;)Lcom/squareup/tickets/OpenTicketsSettings;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/ticket/TicketActionScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->inEditMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/ticket/TicketActionScopeRunner;)Lcom/squareup/ui/ticket/TicketSelection;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    return-object p0
.end method

.method static synthetic lambda$onEditModeChanged$2(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    .line 150
    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public addSelectedTicket(Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketSelection;->addSelectedTicket(Lcom/squareup/ui/ticket/TicketInfo;)V

    return-void
.end method

.method public deleteSelectedTickets()V
    .locals 3

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketSelection;->getSelectedTicketsInfo()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketInfo;->extractIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->tickets:Lcom/squareup/tickets/Tickets;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v1, v2}, Lcom/squareup/tickets/Tickets;->deleteTickets(Ljava/util/List;)V

    .line 194
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v1}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    iput-boolean v1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isExitingTicketActionPath:Z

    .line 195
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->inEditMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 196
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->deletedTicketIds:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketSelection;->clearSelectedTickets()V

    return-void
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 135
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedCount()I
    .locals 1

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketSelection;->getSelectedCount()I

    move-result v0

    return v0
.end method

.method public getSelectedTicketsInfo()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;"
        }
    .end annotation

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketSelection;->getSelectedTicketsInfo()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isExitingTicketActionPath()Z
    .locals 1

    .line 278
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isExitingTicketActionPath:Z

    return v0
.end method

.method public isInEditMode()Z
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->inEditMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isTicketSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z
    .locals 1

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketSelection;->isTicketSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$onEnterScope$0$TicketActionScopeRunner(Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 101
    const-class v0, Lcom/squareup/ui/ticket/TicketActionScope;

    invoke-virtual {p1, v0}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 106
    iput-boolean p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isExitingTicketActionPath:Z

    :cond_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$1$TicketActionScopeRunner(Ljava/lang/Boolean;)V
    .locals 2

    .line 118
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isExitingTicketActionPath:Z

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->inEditMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketSelection;->clearSelectedTickets()V

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->moveTicketListener:Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;->publishMoveTicketsCompleted(Z)V

    return-void
.end method

.method public mergeTicketsTo(Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$MergeResults;",
            ">;)V"
        }
    .end annotation

    .line 239
    new-instance v5, Lcom/squareup/ui/ticket/TicketActionScopeRunner$2;

    invoke-direct {v5, p0, p3}, Lcom/squareup/ui/ticket/TicketActionScopeRunner$2;-><init>(Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/tickets/TicketsCallback;)V

    .line 250
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object p3, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p3}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->res:Lcom/squareup/util/Res;

    move-object v1, p1

    move-object v2, p2

    invoke-interface/range {v0 .. v5}, Lcom/squareup/tickets/Tickets;->mergeTicketsToExisting(Ljava/util/List;Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public moveTicketsToTemplate(Ljava/util/List;Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/permissions/EmployeeInfo;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;",
            "Lcom/squareup/api/items/TicketGroup;",
            "Lcom/squareup/permissions/EmployeeInfo;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$MergeResults;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 264
    invoke-virtual {p2}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->getName()Ljava/lang/String;

    move-result-object v1

    .line 265
    invoke-static {p2}, Lcom/squareup/util/PredefinedTicketsHelper;->buildTicketTemplateOrNull(Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;)Lcom/squareup/api/items/TicketTemplate;

    move-result-object v4

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    const/4 v2, 0x0

    move-object/from16 v3, p3

    move-object/from16 v5, p4

    .line 264
    invoke-static/range {v1 .. v6}, Lcom/squareup/tickets/OpenTicket;->createTicket(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/api/items/TicketGroup;Lcom/squareup/api/items/TicketTemplate;Lcom/squareup/permissions/EmployeeInfo;Ljava/util/Date;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v9

    .line 266
    iget-object v7, v0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v1, v0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v10

    iget-object v11, v0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->res:Lcom/squareup/util/Res;

    move-object v8, p1

    move-object/from16 v12, p5

    invoke-interface/range {v7 .. v12}, Lcom/squareup/tickets/Tickets;->mergeTicketsToNew(Ljava/util/List;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public moveTicketsToTicket(Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$MergeResults;",
            ">;)V"
        }
    .end annotation

    .line 256
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->res:Lcom/squareup/util/Res;

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-interface/range {v0 .. v5}, Lcom/squareup/tickets/Tickets;->mergeTicketsToExisting(Ljava/util/List;Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method public onDeleteTicketsRequested()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->deletedTicketIds:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public onEditModeChanged()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->inEditMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketActionScopeRunner$Qp7XjVWUKhHQBWjrK_cn0pfAHLQ;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$TicketActionScopeRunner$Qp7XjVWUKhHQBWjrK_cn0pfAHLQ;

    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->topOfTraversalCompleting()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketActionScopeRunner$DgsEQPVKFWXRJJGXuChOFX2k09w;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketActionScopeRunner$DgsEQPVKFWXRJJGXuChOFX2k09w;-><init>(Lcom/squareup/ui/ticket/TicketActionScopeRunner;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->moveTicketListener:Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;

    .line 117
    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MoveTicketPresenter$MoveTicketListener;->onMoveTicketsAboutToComplete()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$TicketActionScopeRunner$0yRpJT37G6hg8vV-ZphacP_df-w;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$TicketActionScopeRunner$0yRpJT37G6hg8vV-ZphacP_df-w;-><init>(Lcom/squareup/ui/ticket/TicketActionScopeRunner;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 116
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 126
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->inEditModeKey:Lcom/squareup/BundleKey;

    invoke-virtual {v0, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->inEditMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->inEditModeKey:Lcom/squareup/BundleKey;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->inEditMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    return-void
.end method

.method public onVoidTicketsRequested()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->voidedTicketIds:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public reprintSelectedTickets()V
    .locals 6

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketSelection;->getSelectedTicketsInfo()Ljava/util/List;

    move-result-object v0

    .line 202
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/ticket/TicketInfo;

    .line 203
    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketStore:Lcom/squareup/tickets/TicketStore;

    iget-object v1, v1, Lcom/squareup/ui/ticket/TicketInfo;->id:Ljava/lang/String;

    invoke-interface {v2, v1}, Lcom/squareup/tickets/TicketStore;->retrieveTicket(Ljava/lang/String;)Lcom/squareup/tickets/OpenTicket;

    move-result-object v1

    .line 204
    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->res:Lcom/squareup/util/Res;

    const/4 v4, 0x0

    invoke-static {v2, v1, v3, v4}, Lcom/squareup/payment/OrderProtoConversions;->forTicketFromOrder(Lcom/squareup/payment/Order;Lcom/squareup/tickets/OpenTicket;Lcom/squareup/util/Res;Z)Lcom/squareup/payment/Order;

    move-result-object v1

    .line 205
    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->timestampAsDate()Ljava/util/Date;

    move-result-object v3

    .line 206
    invoke-virtual {v1}, Lcom/squareup/payment/Order;->getOpenTicketName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    .line 205
    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/squareup/print/OrderPrintingDispatcher;->reprintTicket(Lcom/squareup/payment/Order;Ljava/util/Date;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setInEditMode(Z)V
    .locals 1

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->inEditMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public toggleSelected(Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 1

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketSelection;->toggleSelected(Lcom/squareup/ui/ticket/TicketInfo;)V

    return-void
.end method

.method public transferSelectedTicketsTo(Lcom/squareup/permissions/EmployeeInfo;)V
    .locals 2

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->tickets:Lcom/squareup/tickets/Tickets;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/TicketSelection;->getSelectedTicketsInfo()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketInfo;->extractIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/squareup/tickets/Tickets;->transferOwnership(Ljava/util/List;Lcom/squareup/permissions/EmployeeInfo;)V

    .line 272
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {p1}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isExitingTicketActionPath:Z

    .line 273
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->inEditMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 274
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketSelection;->clearSelectedTickets()V

    return-void
.end method

.method public voidSelectedTickets(Ljava/lang/String;Lcom/squareup/protos/client/Employee;Lcom/squareup/tickets/TicketsCallback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/Employee;",
            "Lcom/squareup/tickets/TicketsCallback<",
            "Lcom/squareup/tickets/Tickets$VoidResults;",
            ">;)V"
        }
    .end annotation

    .line 222
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketSelection;->getSelectedTicketsInfo()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketInfo;->extractIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 223
    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Order;->snapshot()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v1

    .line 225
    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->tickets:Lcom/squareup/tickets/Tickets;

    new-instance v3, Lcom/squareup/ui/ticket/TicketActionScopeRunner$1;

    invoke-direct {v3, p0, p1, v1, p2}, Lcom/squareup/ui/ticket/TicketActionScopeRunner$1;-><init>(Lcom/squareup/ui/ticket/TicketActionScopeRunner;Ljava/lang/String;Lcom/squareup/payment/OrderSnapshot;Lcom/squareup/protos/client/Employee;)V

    invoke-interface {v2, v0, v3, p3}, Lcom/squareup/tickets/Tickets;->voidTickets(Ljava/util/List;Lcom/squareup/tickets/Tickets$VoidHandler;Lcom/squareup/tickets/TicketsCallback;)V

    .line 231
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {p1}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isExitingTicketActionPath:Z

    .line 232
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->inEditMode:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 p2, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 233
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->voidedTicketIds:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    .line 234
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->ticketSelection:Lcom/squareup/ui/ticket/TicketSelection;

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/TicketSelection;->clearSelectedTickets()V

    return-void
.end method
