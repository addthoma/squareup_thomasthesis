.class Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;
.super Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;
.source "SplitTicketRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PerItemDiscountViewHolder"
.end annotation


# instance fields
.field private final discountRow:Lcom/squareup/ui/cart/CartEntryView;

.field final synthetic this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;Landroid/view/View;)V
    .locals 0

    .line 266
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    .line 267
    invoke-direct {p0, p2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 268
    sget p1, Lcom/squareup/orderentry/R$id;->split_ticket_tax_total:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartEntryView;

    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;->discountRow:Lcom/squareup/ui/cart/CartEntryView;

    return-void
.end method


# virtual methods
.method public bind()V
    .locals 7

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->ticketMutable(Ljava/lang/String;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    .line 273
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;->discountRow:Lcom/squareup/ui/cart/CartEntryView;

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    .line 274
    invoke-static {v2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$500(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    move-result-object v2

    sget v3, Lcom/squareup/transaction/R$string;->cart_discounts:I

    iget-object v4, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    .line 275
    invoke-static {v4}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v5}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getNegativePerItemDiscountsAmount(Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    .line 276
    invoke-static {v5}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v6}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->hasUnappliedDiscountsThatCanBeAppliedToOnlyOneItem(Ljava/lang/String;)Z

    move-result v5

    .line 274
    invoke-interface {v2, v3, v4, v0, v5}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->discount(ILcom/squareup/protos/common/Money;ZZ)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v2

    .line 273
    invoke-virtual {v1, v2}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    if-nez v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;->discountRow:Lcom/squareup/ui/cart/CartEntryView;

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$PerItemDiscountViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$800(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$900(Landroid/view/View;I)V

    :cond_0
    return-void
.end method
