.class public abstract Lcom/squareup/ui/tender/InTenderScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InTenderScope.kt"

# interfaces
.implements Lcom/squareup/ui/seller/EnablesCardSwipes;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008&\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0006\u001a\u00020\u0001H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/tender/InTenderScope;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "Lcom/squareup/ui/seller/EnablesCardSwipes;",
        "tenderScopeTreeKey",
        "Lcom/squareup/ui/tender/TenderScopeTreeKey;",
        "(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V",
        "getParentKey",
        "pos-main-workflow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final tenderScopeTreeKey:Lcom/squareup/ui/tender/TenderScopeTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V
    .locals 1

    const-string v0, "tenderScopeTreeKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/tender/InTenderScope;->tenderScopeTreeKey:Lcom/squareup/ui/tender/TenderScopeTreeKey;

    return-void
.end method


# virtual methods
.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/tender/InTenderScope;->tenderScopeTreeKey:Lcom/squareup/ui/tender/TenderScopeTreeKey;

    invoke-interface {v0}, Lcom/squareup/ui/tender/TenderScopeTreeKey;->getRegisterTreeKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/tender/InTenderScope;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method
