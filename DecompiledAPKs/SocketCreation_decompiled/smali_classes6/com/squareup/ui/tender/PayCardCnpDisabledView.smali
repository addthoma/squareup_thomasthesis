.class public Lcom/squareup/ui/tender/PayCardCnpDisabledView;
.super Landroid/widget/LinearLayout;
.source "PayCardCnpDisabledView.java"


# instance fields
.field private actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

.field private glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field presenter:Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const-class p2, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Component;->inject(Lcom/squareup/ui/tender/PayCardCnpDisabledView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 28
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 29
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledView;->actionBarView:Lcom/squareup/marin/widgets/ActionBarView;

    .line 30
    sget v0, Lcom/squareup/ui/tender/legacy/R$id;->cnp_disabled_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledView;->glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledView;->presenter:Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 43
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledView;->presenter:Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen$Presenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public setMessageText(I)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCardCnpDisabledView;->glyphMessageView:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(I)V

    return-void
.end method
