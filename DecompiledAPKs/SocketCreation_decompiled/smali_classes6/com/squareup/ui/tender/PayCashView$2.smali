.class Lcom/squareup/ui/tender/PayCashView$2;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "PayCashView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/tender/PayCashView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/tender/PayCashView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/tender/PayCashView;)V
    .locals 0

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/tender/PayCashView$2;->this$0:Lcom/squareup/ui/tender/PayCashView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x6

    if-ne p2, p1, :cond_0

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/tender/PayCashView$2;->this$0:Lcom/squareup/ui/tender/PayCashView;

    iget-object p1, p1, Lcom/squareup/ui/tender/PayCashView;->presenter:Lcom/squareup/ui/tender/PayCashPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/tender/PayCashPresenter;->onTenderButtonClicked()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
