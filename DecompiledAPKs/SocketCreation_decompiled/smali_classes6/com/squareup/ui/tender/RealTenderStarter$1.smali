.class synthetic Lcom/squareup/ui/tender/RealTenderStarter$1;
.super Ljava/lang/Object;
.source "RealTenderStarter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/RealTenderStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$tenderpayment$TenderCompleter$CompleteTenderResult:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 219
    invoke-static {}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->values()[Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/tender/RealTenderStarter$1;->$SwitchMap$com$squareup$tenderpayment$TenderCompleter$CompleteTenderResult:[I

    :try_start_0
    sget-object v0, Lcom/squareup/ui/tender/RealTenderStarter$1;->$SwitchMap$com$squareup$tenderpayment$TenderCompleter$CompleteTenderResult:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->DO_NOTHING:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/ui/tender/RealTenderStarter$1;->$SwitchMap$com$squareup$tenderpayment$TenderCompleter$CompleteTenderResult:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/ui/tender/RealTenderStarter$1;->$SwitchMap$com$squareup$tenderpayment$TenderCompleter$CompleteTenderResult:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_BUYER_FLOW_AND_AUTHORIZE:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/ui/tender/RealTenderStarter$1;->$SwitchMap$com$squareup$tenderpayment$TenderCompleter$CompleteTenderResult:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_POS_APPLET:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lcom/squareup/ui/tender/RealTenderStarter$1;->$SwitchMap$com$squareup$tenderpayment$TenderCompleter$CompleteTenderResult:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->START_SPLIT_TENDER:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/squareup/ui/tender/RealTenderStarter$1;->$SwitchMap$com$squareup$tenderpayment$TenderCompleter$CompleteTenderResult:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_OFFLINE_MODE_TRANSACTION_LIMIT_REACHED_WARNING_DIALOG_SCREEN:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    :try_start_6
    sget-object v0, Lcom/squareup/ui/tender/RealTenderStarter$1;->$SwitchMap$com$squareup$tenderpayment$TenderCompleter$CompleteTenderResult:[I

    sget-object v1, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->SHOW_THIRD_PARTY:Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;

    invoke-virtual {v1}, Lcom/squareup/tenderpayment/TenderCompleter$CompleteTenderResult;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    return-void
.end method
