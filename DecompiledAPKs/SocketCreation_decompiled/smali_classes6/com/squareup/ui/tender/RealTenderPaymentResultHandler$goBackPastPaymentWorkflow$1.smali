.class final Lcom/squareup/ui/tender/RealTenderPaymentResultHandler$goBackPastPaymentWorkflow$1;
.super Ljava/lang/Object;
.source "RealTenderPaymentResultHandler.kt"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->goBackPastPaymentWorkflow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/Command;",
        "kotlin.jvm.PlatformType",
        "history",
        "Lflow/History;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler$goBackPastPaymentWorkflow$1;->this$0:Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 4

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    .line 207
    invoke-virtual {v0}, Lflow/History$Builder;->clear()Lflow/History$Builder;

    move-result-object v0

    .line 208
    invoke-virtual {p1}, Lflow/History;->framesFromBottom()Ljava/lang/Iterable;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/container/ContainerTreeKey;

    .line 209
    iget-object v2, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler$goBackPastPaymentWorkflow$1;->this$0:Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;

    invoke-static {v2}, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->access$getCheckoutWorkflowRunner$p(Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;)Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    move-result-object v2

    const-string v3, "screen"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v3, v1

    check-cast v3, Lflow/path/Path;

    invoke-interface {v2, v3}, Lcom/squareup/ui/main/CheckoutWorkflowRunner;->isPaymentWorkflowScreen(Lflow/path/Path;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 210
    invoke-virtual {v0, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_0

    .line 213
    :cond_1
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object v0, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-static {p1, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method
