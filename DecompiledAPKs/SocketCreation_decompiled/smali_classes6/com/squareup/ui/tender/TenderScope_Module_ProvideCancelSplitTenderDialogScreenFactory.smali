.class public final Lcom/squareup/ui/tender/TenderScope_Module_ProvideCancelSplitTenderDialogScreenFactory;
.super Ljava/lang/Object;
.source "TenderScope_Module_ProvideCancelSplitTenderDialogScreenFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;",
        "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/tender/TenderScope$Module;

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/tender/TenderScope$Module;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/tender/TenderScope$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/tender/TenderScope_Module_ProvideCancelSplitTenderDialogScreenFactory;->module:Lcom/squareup/ui/tender/TenderScope$Module;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/tender/TenderScope_Module_ProvideCancelSplitTenderDialogScreenFactory;->runnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/tender/TenderScope$Module;Ljavax/inject/Provider;)Lcom/squareup/ui/tender/TenderScope_Module_ProvideCancelSplitTenderDialogScreenFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/tender/TenderScope$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ">;)",
            "Lcom/squareup/ui/tender/TenderScope_Module_ProvideCancelSplitTenderDialogScreenFactory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/ui/tender/TenderScope_Module_ProvideCancelSplitTenderDialogScreenFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/tender/TenderScope_Module_ProvideCancelSplitTenderDialogScreenFactory;-><init>(Lcom/squareup/ui/tender/TenderScope$Module;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCancelSplitTenderDialogScreen(Lcom/squareup/ui/tender/TenderScope$Module;Lcom/squareup/tenderpayment/TenderScopeRunner;)Lcom/squareup/workflow/legacy/Screen;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/tender/TenderScope$Module;",
            "Lcom/squareup/tenderpayment/TenderScopeRunner;",
            ")",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;",
            ">;"
        }
    .end annotation

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/ui/tender/TenderScope$Module;->provideCancelSplitTenderDialogScreen(Lcom/squareup/tenderpayment/TenderScopeRunner;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/workflow/legacy/Screen;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/workflow/legacy/Screen;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$ScreenData;",
            "Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialog$Event;",
            ">;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderScope_Module_ProvideCancelSplitTenderDialogScreenFactory;->module:Lcom/squareup/ui/tender/TenderScope$Module;

    iget-object v1, p0, Lcom/squareup/ui/tender/TenderScope_Module_ProvideCancelSplitTenderDialogScreenFactory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tenderpayment/TenderScopeRunner;

    invoke-static {v0, v1}, Lcom/squareup/ui/tender/TenderScope_Module_ProvideCancelSplitTenderDialogScreenFactory;->provideCancelSplitTenderDialogScreen(Lcom/squareup/ui/tender/TenderScope$Module;Lcom/squareup/tenderpayment/TenderScopeRunner;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/tender/TenderScope_Module_ProvideCancelSplitTenderDialogScreenFactory;->get()Lcom/squareup/workflow/legacy/Screen;

    move-result-object v0

    return-object v0
.end method
