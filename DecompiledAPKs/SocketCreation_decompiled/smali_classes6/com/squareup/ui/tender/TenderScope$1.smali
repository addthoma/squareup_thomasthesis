.class Lcom/squareup/ui/tender/TenderScope$1;
.super Ljava/lang/Object;
.source "TenderScope.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/tender/TenderScope;->register(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/tender/TenderScope;

.field final synthetic val$component:Lcom/squareup/ui/tender/TenderScope$Component;


# direct methods
.method constructor <init>(Lcom/squareup/ui/tender/TenderScope;Lcom/squareup/ui/tender/TenderScope$Component;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/tender/TenderScope$1;->this$0:Lcom/squareup/ui/tender/TenderScope;

    iput-object p2, p0, Lcom/squareup/ui/tender/TenderScope$1;->val$component:Lcom/squareup/ui/tender/TenderScope$Component;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/tender/TenderScope$1;->val$component:Lcom/squareup/ui/tender/TenderScope$Component;

    invoke-interface {v0}, Lcom/squareup/ui/tender/TenderScope$Component;->changeHudToaster()Lcom/squareup/tenderpayment/ChangeHudToaster;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/tenderpayment/ChangeHudToaster;->toastIfAvailable()V

    return-void
.end method
