.class public final Lcom/squareup/ui/tender/RealTenderStarter_Factory;
.super Ljava/lang/Object;
.source "RealTenderStarter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/tender/RealTenderStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final buyerFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutflowConfigFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/AndroidNfcState;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentFlowHistoryFactoryStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentProcessingEventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final posAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardPaymentServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderCompleterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketAutoIdentifiersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/TicketAutoIdentifiers;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/AndroidNfcState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/TicketAutoIdentifiers;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 81
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->nfcStateProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 82
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 83
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 84
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->ticketAutoIdentifiersProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->posAppletProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->paymentFlowHistoryFactoryStarterProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->swipeHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 93
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->paymentProcessingEventSinkProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 95
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->storeAndForwardPaymentServiceProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->checkoutflowConfigFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/tender/RealTenderStarter_Factory;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/AndroidNfcState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/TicketAutoIdentifiers;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;",
            ">;)",
            "Lcom/squareup/ui/tender/RealTenderStarter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 119
    new-instance v17, Lcom/squareup/ui/tender/RealTenderStarter_Factory;

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/ui/tender/RealTenderStarter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v17
.end method

.method public static newInstance(Lcom/squareup/ui/AndroidNfcState;Ldagger/Lazy;Lcom/squareup/print/PrinterStations;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/print/TicketAutoIdentifiers;Ldagger/Lazy;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Ldagger/Lazy;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Ldagger/Lazy;Lcom/squareup/payment/offline/StoreAndForwardPaymentService;Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;)Lcom/squareup/ui/tender/RealTenderStarter;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/AndroidNfcState;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/print/TicketAutoIdentifiers;",
            "Ldagger/Lazy<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            "Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Ldagger/Lazy<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cardreader/dipper/ReaderIssueScreenRequestSink;",
            ">;",
            "Lcom/squareup/payment/offline/StoreAndForwardPaymentService;",
            "Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;",
            ")",
            "Lcom/squareup/ui/tender/RealTenderStarter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    .line 133
    new-instance v17, Lcom/squareup/ui/tender/RealTenderStarter;

    move-object/from16 v0, v17

    invoke-direct/range {v0 .. v16}, Lcom/squareup/ui/tender/RealTenderStarter;-><init>(Lcom/squareup/ui/AndroidNfcState;Ldagger/Lazy;Lcom/squareup/print/PrinterStations;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/print/TicketAutoIdentifiers;Ldagger/Lazy;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Ldagger/Lazy;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Ldagger/Lazy;Lcom/squareup/payment/offline/StoreAndForwardPaymentService;Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;)V

    return-object v17
.end method


# virtual methods
.method public get()Lcom/squareup/ui/tender/RealTenderStarter;
    .locals 18

    move-object/from16 v0, p0

    .line 101
    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->nfcStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/AndroidNfcState;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v3

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/print/PrinterStations;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->ticketAutoIdentifiersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/print/TicketAutoIdentifiers;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v8

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/buyer/BuyerFlowStarter;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->posAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->paymentFlowHistoryFactoryStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->maybeX2SellerScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->swipeHandlerProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v13

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->paymentProcessingEventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->readerIssueScreenRequestSinkProvider:Ljavax/inject/Provider;

    invoke-static {v1}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v15

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->storeAndForwardPaymentServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/payment/offline/StoreAndForwardPaymentService;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->checkoutflowConfigFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;

    invoke-static/range {v2 .. v17}, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->newInstance(Lcom/squareup/ui/AndroidNfcState;Ldagger/Lazy;Lcom/squareup/print/PrinterStations;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/print/TicketAutoIdentifiers;Ldagger/Lazy;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/main/PaymentFlowHistoryFactoryStarter;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Ldagger/Lazy;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Ldagger/Lazy;Lcom/squareup/payment/offline/StoreAndForwardPaymentService;Lcom/squareup/checkoutflow/CheckoutflowConfigFactory;)Lcom/squareup/ui/tender/RealTenderStarter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/ui/tender/RealTenderStarter_Factory;->get()Lcom/squareup/ui/tender/RealTenderStarter;

    move-result-object v0

    return-object v0
.end method
