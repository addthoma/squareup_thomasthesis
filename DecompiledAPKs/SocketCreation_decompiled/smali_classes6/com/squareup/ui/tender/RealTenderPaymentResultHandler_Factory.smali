.class public final Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;
.super Ljava/lang/Object;
.source "RealTenderPaymentResultHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final buyerFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final cardSellerWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/CardSellerWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final editInvoiceInTenderRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesAppletRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentInputHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentProcessingEventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final posContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final smartPaymentFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderCompleterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/CardSellerWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->orderEntryAppletProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->editInvoiceInTenderRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->cardSellerWorkflowProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 93
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->swipeHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 95
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->paymentInputHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->posContainerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->paymentProcessingEventSinkProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/CardSellerWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/payment/SwipeHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;)",
            "Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    .line 127
    new-instance v19, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;

    move-object/from16 v0, v19

    invoke-direct/range {v0 .. v18}, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v19
.end method

.method public static newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;Lflow/Flow;Lcom/squareup/tenderpayment/CardSellerWorkflow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/tender/TenderFactory;)Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;
    .locals 20

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    .line 139
    new-instance v19, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;

    move-object/from16 v0, v19

    invoke-direct/range {v0 .. v18}, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;-><init>(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;Lflow/Flow;Lcom/squareup/tenderpayment/CardSellerWorkflow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/tender/TenderFactory;)V

    return-object v19
.end method


# virtual methods
.method public get()Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;
    .locals 20

    move-object/from16 v0, p0

    .line 107
    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->orderEntryAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->editInvoiceInTenderRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->cardSellerWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/tenderpayment/CardSellerWorkflow;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/buyer/BuyerFlowStarter;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->swipeHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/ui/payment/SwipeHandler;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->smartPaymentFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->paymentInputHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/ui/main/errors/PaymentInputHandler;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->posContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->paymentProcessingEventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/payment/TenderInEdit;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/payment/tender/TenderFactory;

    invoke-static/range {v2 .. v19}, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->newInstance(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;Lflow/Flow;Lcom/squareup/tenderpayment/CardSellerWorkflow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/tender/TenderFactory;)Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler_Factory;->get()Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;

    move-result-object v0

    return-object v0
.end method
