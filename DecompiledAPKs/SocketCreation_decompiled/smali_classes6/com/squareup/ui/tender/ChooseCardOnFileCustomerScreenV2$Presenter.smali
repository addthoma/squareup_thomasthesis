.class Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;
.super Lmortar/ViewPresenter;
.source "ChooseCardOnFileCustomerScreenV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;",
        ">;"
    }
.end annotation


# static fields
.field private static final SEARCH_DELAY_MS:J = 0xc8L


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private cardTypeToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

.field private final contactLoader:Lcom/squareup/crm/RolodexContactLoader;

.field private final flow:Lflow/Flow;

.field private final formatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final rolodexGroupLoader:Lcom/squareup/crm/RolodexGroupLoader;

.field private final rolodexServiceHelper:Lcom/squareup/crm/RolodexServiceHelper;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method constructor <init>(Lcom/squareup/crm/RolodexContactLoader;Lflow/Flow;Lcom/squareup/payment/Transaction;Lcom/squareup/util/Res;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/crm/RolodexGroupLoader;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/RolodexContactLoader;",
            "Lflow/Flow;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            "Lcom/squareup/crm/RolodexGroupLoader;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 103
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    .line 105
    iput-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->flow:Lflow/Flow;

    .line 106
    iput-object p3, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 107
    iput-object p4, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    .line 108
    iput-object p5, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->rolodexServiceHelper:Lcom/squareup/crm/RolodexServiceHelper;

    .line 109
    iput-object p6, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->rolodexGroupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    .line 110
    iput-object p7, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    .line 111
    iput-object p8, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 112
    iput-object p9, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    .line 113
    iput-object p10, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->formatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method private getFullContact(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 203
    sget-object v0, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_PROGRESS_SPINNER:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->update(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V

    .line 205
    new-instance v0, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$gyWn4F3APah2UpDWSSDHY2L9OkY;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$gyWn4F3APah2UpDWSSDHY2L9OkY;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private update(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V
    .locals 4

    .line 227
    sget-object v0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$1;->$SwitchMap$com$squareup$crm$RolodexContactLoaderHelper$VisualState:[I

    invoke-virtual {p2}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    packed-switch v0, :pswitch_data_0

    .line 266
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected visual state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 260
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showProgress(Z)V

    .line 261
    invoke-virtual {p1, v3}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showContactList(Z)V

    .line 262
    iget-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_contact_search_empty:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v2, p2}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showMessage(ZLjava/lang/CharSequence;)V

    goto :goto_0

    .line 254
    :pswitch_1
    invoke-virtual {p1, v3}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showProgress(Z)V

    .line 255
    invoke-virtual {p1, v2}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showContactList(Z)V

    .line 256
    iget-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_contact_search_empty:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v2, p2}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showMessage(ZLjava/lang/CharSequence;)V

    goto :goto_0

    .line 248
    :pswitch_2
    invoke-virtual {p1, v3}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showProgress(Z)V

    .line 249
    invoke-virtual {p1, v2}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showContactList(Z)V

    .line 250
    invoke-virtual {p1, v3, v1}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showMessage(ZLjava/lang/CharSequence;)V

    goto :goto_0

    .line 242
    :pswitch_3
    invoke-virtual {p1, v3}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showProgress(Z)V

    .line 243
    invoke-virtual {p1, v2}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showContactList(Z)V

    .line 244
    invoke-virtual {p1, v3, v1}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showMessage(ZLjava/lang/CharSequence;)V

    goto :goto_0

    .line 235
    :pswitch_4
    invoke-virtual {p1, v3}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showProgress(Z)V

    .line 236
    invoke-virtual {p1, v3}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showContactList(Z)V

    .line 237
    iget-object p2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_failed_to_load_customers:I

    .line 238
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 237
    invoke-virtual {p1, v2, p2}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showMessage(ZLjava/lang/CharSequence;)V

    goto :goto_0

    .line 229
    :pswitch_5
    invoke-virtual {p1, v2}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showProgress(Z)V

    .line 230
    invoke-virtual {p1, v3}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showContactList(Z)V

    .line 231
    invoke-virtual {p1, v3, v1}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->showMessage(ZLjava/lang/CharSequence;)V

    :goto_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public synthetic lambda$getFullContact$14$ChooseCardOnFileCustomerScreenV2$Presenter(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->rolodexServiceHelper:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-interface {v0, p1}, Lcom/squareup/crm/RolodexServiceHelper;->getContact(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$kKCDh2OCQYQ2dDuWgOfHDfSJvw4;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$kKCDh2OCQYQ2dDuWgOfHDfSJvw4;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    .line 206
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$ChooseCardOnFileCustomerScreenV2$Presenter(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V
    .locals 0

    .line 173
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 174
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->update(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V

    return-void
.end method

.method public synthetic lambda$null$11$ChooseCardOnFileCustomerScreenV2$Presenter(Lcom/squareup/protos/client/rolodex/GetContactResponse;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/analytics/event/v1/DetailEvent;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->CRM_ADD_CUSTOMER_TO_SALE:Lcom/squareup/analytics/RegisterActionName;

    const-string v4, "SPLIT_TENDER"

    invoke-direct {v2, v3, v4}, Lcom/squareup/analytics/event/v1/DetailEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-interface {v0, v2}, Lcom/squareup/payment/TenderInEdit;->setContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, p1, v1, v1}, Lcom/squareup/payment/Transaction;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 213
    iget-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->flow:Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->cardTypeToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    invoke-static {v0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;->forSelectedCustomer(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)Lcom/squareup/ui/tender/ChooseCardOnFileScreen;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/analytics/event/v1/DetailEvent;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->CRM_ADD_CUSTOMER_TO_SALE:Lcom/squareup/analytics/RegisterActionName;

    const-string v4, "SINGLE_TENDER"

    invoke-direct {v2, v3, v4}, Lcom/squareup/analytics/event/v1/DetailEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {v0, p1, v1, v1}, Lcom/squareup/payment/Transaction;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 218
    iget-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->flow:Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->cardTypeToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    invoke-static {v0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;->forSelectedCustomer(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)Lcom/squareup/ui/tender/ChooseCardOnFileScreen;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$12$ChooseCardOnFileCustomerScreenV2$Presenter(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 222
    sget-object p2, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_FAILED_TO_LOAD:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->update(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V

    return-void
.end method

.method public synthetic lambda$null$13$ChooseCardOnFileCustomerScreenV2$Presenter(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 206
    new-instance v0, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$3BspJBGhmF6QIfE59u0N3CkOHGI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$3BspJBGhmF6QIfE59u0N3CkOHGI;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;)V

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$F7nRmBzuh7ljEReXa3AJNzQzhGc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$F7nRmBzuh7ljEReXa3AJNzQzhGc;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    invoke-virtual {p2, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$null$2$ChooseCardOnFileCustomerScreenV2$Presenter(Lcom/squareup/protos/client/rolodex/Group;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)Lrx/Subscription;
    .locals 1

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/squareup/crm/RolodexContactLoaderHelper;->visualStateOf(Lcom/squareup/crm/RolodexContactLoader;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$ta-fe7jcrkZirmKIVwcm4P__7kI;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$ta-fe7jcrkZirmKIVwcm4P__7kI;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    .line 172
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$3$ChooseCardOnFileCustomerScreenV2$Presenter(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 167
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/Group;->group_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/crm/RolodexContactLoader;->setGroupToken(Ljava/lang/String;)V

    .line 170
    new-instance v0, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$CbuMMY4kT5LG0AaH9FHZwyyH7YM;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$CbuMMY4kT5LG0AaH9FHZwyyH7YM;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;Lcom/squareup/protos/client/rolodex/Group;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$null$5$ChooseCardOnFileCustomerScreenV2$Presenter(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;Ljava/lang/Boolean;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 181
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 182
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 183
    sget-object p2, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_FAILED_TO_LOAD:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->update(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$7$ChooseCardOnFileCustomerScreenV2$Presenter(Ljava/lang/String;)V
    .locals 3

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    new-instance v1, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/crm/RolodexContactLoader;->setSearchTerm(Lcom/squareup/crm/RolodexContactLoader$SearchTerm;)V

    return-void
.end method

.method public synthetic lambda$null$9$ChooseCardOnFileCustomerScreenV2$Presenter(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 0

    .line 194
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 195
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->getFullContact(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public synthetic lambda$onLoad$0$ChooseCardOnFileCustomerScreenV2$Presenter(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V
    .locals 0

    .line 155
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 156
    iget-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->flow:Lflow/Flow;

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public synthetic lambda$onLoad$10$ChooseCardOnFileCustomerScreenV2$Presenter(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)Lrx/Subscription;
    .locals 1

    .line 192
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->onContactClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$NfeHl_Xj6xigCOBYJa1Cyt1K4FE;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$NfeHl_Xj6xigCOBYJa1Cyt1K4FE;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    .line 193
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$4$ChooseCardOnFileCustomerScreenV2$Presenter(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->rolodexGroupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    sget-object v1, Lcom/squareup/protos/client/rolodex/AudienceType;->CARDS_ON_FILE:Lcom/squareup/protos/client/rolodex/AudienceType;

    invoke-interface {v0, v1}, Lcom/squareup/crm/RolodexGroupLoader;->success(Lcom/squareup/protos/client/rolodex/AudienceType;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$rG6ecfN7Vmt1neNz9k1NsxYwKig;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$rG6ecfN7Vmt1neNz9k1NsxYwKig;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    .line 166
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$ChooseCardOnFileCustomerScreenV2$Presenter(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->rolodexGroupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->failure()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$5f9TbDnEDvKgwnNVCBeWhIxzhAw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$5f9TbDnEDvKgwnNVCBeWhIxzhAw;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    .line 180
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$8$ChooseCardOnFileCustomerScreenV2$Presenter(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)Lrx/Subscription;
    .locals 1

    .line 188
    invoke-virtual {p1}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->onSearchTermChanged()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$C4Z9emwzXoTQFMXRTMRnkA568Io;

    invoke-direct {v0, p0}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$C4Z9emwzXoTQFMXRTMRnkA568Io;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;)V

    .line 189
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 117
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->rolodexGroupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->maybeRegister(Lmortar/MortarScope;Lmortar/Scoped;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->rolodexGroupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/squareup/crm/RolodexGroupLoader;->setIncludeCounts(Z)V

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->rolodexGroupLoader:Lcom/squareup/crm/RolodexGroupLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexGroupLoader;->refresh()V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Lcom/squareup/crm/RolodexContactLoader;->setSearchDelayMs(J)V

    .line 127
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2;

    .line 128
    invoke-static {p1}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2;->access$000(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2;)Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->cardTypeToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 132
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 133
    invoke-virtual {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;

    .line 134
    invoke-virtual {p1}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->contactList()Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    move-result-object v0

    .line 136
    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->cardTypeToSelect:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    sget-object v2, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;->GIFT_CARD:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    if-ne v1, v2, :cond_0

    sget v1, Lcom/squareup/ui/tender/legacy/R$string;->gift_card_on_file_action_bar_title:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/ui/tender/legacy/R$string;->card_on_file_action_bar_title:I

    .line 141
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v2}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 143
    iget-object v2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v2}, Lcom/squareup/payment/TenderInEdit;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v2

    goto :goto_1

    .line 145
    :cond_1
    iget-object v2, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v2

    .line 148
    :goto_1
    invoke-static {p1, v1}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    iget-object v3, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->formatter:Lcom/squareup/text/Formatter;

    .line 149
    invoke-interface {v3, v2}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v2

    const-string v3, "amount"

    invoke-virtual {v1, v3, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 150
    invoke-virtual {v1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v1

    .line 151
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 153
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 154
    invoke-virtual {v2, v3, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$8woQCzhfLXKUwz9uYxgG-6vAmFo;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$8woQCzhfLXKUwz9uYxgG-6vAmFo;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 158
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 153
    invoke-virtual {p1, v1}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 160
    iget-object v1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->init(Lcom/squareup/crm/RolodexContactLoader;I)V

    .line 162
    sget-object v1, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_PROGRESS_SPINNER:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    invoke-direct {p0, p1, v1}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->update(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V

    .line 164
    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$CmUkRWSiAksBuysJBfvkzwq7jjk;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$CmUkRWSiAksBuysJBfvkzwq7jjk;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 178
    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$wgwvS9mPjdxeCjcki1Bdb0FDCv8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$wgwvS9mPjdxeCjcki1Bdb0FDCv8;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 187
    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$2lTkqa5HqmxT2WqTZKi-HeLsiHw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$2lTkqa5HqmxT2WqTZKi-HeLsiHw;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 191
    new-instance v1, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$WX469p4pp2dOQRUTpoznnPoko1c;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileCustomerScreenV2$Presenter$WX469p4pp2dOQRUTpoznnPoko1c;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;Lcom/squareup/ui/tender/ChooseCardOnFileCustomerViewV2;)V

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 198
    iget-object p1, p0, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2$Presenter;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    sget-object v0, Lcom/squareup/comms/protos/common/TenderType;->CARD_ON_FILE:Lcom/squareup/comms/protos/common/TenderType;

    invoke-interface {p1, v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->configuringTender(Lcom/squareup/comms/protos/common/TenderType;)Z

    return-void
.end method
