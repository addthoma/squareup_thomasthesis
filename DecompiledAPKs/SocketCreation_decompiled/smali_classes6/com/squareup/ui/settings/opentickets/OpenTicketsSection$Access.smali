.class public Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$Access;
.super Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;
.source "OpenTicketsSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Access"
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final ticketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/settings/server/Features;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/squareup/permissions/Permission;

    .line 71
    invoke-direct {p0, p2, v0}, Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;-><init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$Access;->ticketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 73
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$Access;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$Access;->ticketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEVICE_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    .line 78
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
