.class public final Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "OpenTicketsSettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;

    .line 37
    sget-object v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;

    .line 38
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_OPEN_TICKET:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 24
    const-class v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 41
    sget v0, Lcom/squareup/settingsapplet/R$layout;->open_tickets_settings_view:I

    return v0
.end method
