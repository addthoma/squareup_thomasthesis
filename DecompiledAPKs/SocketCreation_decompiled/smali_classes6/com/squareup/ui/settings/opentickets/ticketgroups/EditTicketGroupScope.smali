.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "EditTicketGroupScope.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope$Module;,
        Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope;

    invoke-direct {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope;

    .line 42
    sget-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method
