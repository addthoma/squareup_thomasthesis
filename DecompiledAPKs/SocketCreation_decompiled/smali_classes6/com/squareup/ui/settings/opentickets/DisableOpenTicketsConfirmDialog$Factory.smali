.class public Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog$Factory;
.super Ljava/lang/Object;
.source "DisableOpenTicketsConfirmDialog.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, 0x0

    .line 41
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->setTicketsEnabled(Z)V

    return-void
.end method

.method static synthetic lambda$create$1(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    const/4 p1, 0x1

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;->setTicketsEnabled(Z)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 30
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;

    .line 32
    invoke-static {v0}, Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;->access$000(Lcom/squareup/ui/settings/opentickets/DisableOpenTicketsConfirmDialog;)Lcom/squareup/register/widgets/Confirmation;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/register/widgets/Confirmation;->getStrings(Landroid/content/res/Resources;)Lcom/squareup/register/widgets/Confirmation$Strings;

    move-result-object v0

    .line 34
    const-class v1, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    .line 35
    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    invoke-interface {v1}, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;->openTicketsSettingsRunner()Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;

    move-result-object v1

    .line 37
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object p1, v0, Lcom/squareup/register/widgets/Confirmation$Strings;->title:Ljava/lang/String;

    .line 38
    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    iget-object v2, v0, Lcom/squareup/register/widgets/Confirmation$Strings;->body:Ljava/lang/String;

    .line 39
    invoke-virtual {p1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    iget-object v2, v0, Lcom/squareup/register/widgets/Confirmation$Strings;->confirm:Ljava/lang/String;

    new-instance v3, Lcom/squareup/ui/settings/opentickets/-$$Lambda$DisableOpenTicketsConfirmDialog$Factory$oP2qOjwP0jTwkwc98yDy6wOpy_Y;

    invoke-direct {v3, v1}, Lcom/squareup/ui/settings/opentickets/-$$Lambda$DisableOpenTicketsConfirmDialog$Factory$oP2qOjwP0jTwkwc98yDy6wOpy_Y;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;)V

    .line 40
    invoke-virtual {p1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    iget-object v0, v0, Lcom/squareup/register/widgets/Confirmation$Strings;->cancel:Ljava/lang/String;

    new-instance v2, Lcom/squareup/ui/settings/opentickets/-$$Lambda$DisableOpenTicketsConfirmDialog$Factory$aVE-gNUzabjgufAtPlD6pD7FEGA;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/opentickets/-$$Lambda$DisableOpenTicketsConfirmDialog$Factory$aVE-gNUzabjgufAtPlD6pD7FEGA;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;)V

    .line 42
    invoke-virtual {p1, v0, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 44
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 45
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 37
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
