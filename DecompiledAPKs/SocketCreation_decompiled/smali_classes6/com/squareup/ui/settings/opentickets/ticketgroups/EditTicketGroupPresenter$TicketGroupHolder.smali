.class Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$TicketGroupHolder;
.super Ljava/lang/Object;
.source "EditTicketGroupPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TicketGroupHolder"
.end annotation


# instance fields
.field final ticketGroup:Lcom/squareup/api/items/TicketGroup;

.field final ticketTempaltes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/TicketTemplate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/api/items/TicketGroup;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/TicketGroup;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/TicketTemplate;",
            ">;)V"
        }
    .end annotation

    .line 468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 469
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$TicketGroupHolder;->ticketGroup:Lcom/squareup/api/items/TicketGroup;

    .line 470
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter$TicketGroupHolder;->ticketTempaltes:Ljava/util/List;

    return-void
.end method
