.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;
.super Ljava/lang/Object;
.source "EditTicketGroupState.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;",
            ">;"
        }
    .end annotation
.end field

.field private static final TICKET_TEMPLATE_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "Lcom/squareup/api/items/TicketTemplate;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private automaticTemplateCount:I

.field private currentGroup:Lcom/squareup/api/items/TicketGroup;

.field private currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

.field private deletedTicketTemplateIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private initialAutomaticTemplateCount:I

.field private newTicketTemplateIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private ticketTemplateStates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/TicketTemplate$Builder;",
            ">;"
        }
    .end annotation
.end field

.field private ticketTemplates:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/TicketTemplate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupState$tQuCBniYXkVnLhtu9xKfdXl1ak4;->INSTANCE:Lcom/squareup/ui/settings/opentickets/ticketgroups/-$$Lambda$EditTicketGroupState$tQuCBniYXkVnLhtu9xKfdXl1ak4;

    sput-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->TICKET_TEMPLATE_COMPARATOR:Ljava/util/Comparator;

    .line 247
    new-instance v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState$1;

    invoke-direct {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    const-class v0, Lcom/squareup/api/items/TicketGroup;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketGroup;

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroup:Lcom/squareup/api/items/TicketGroup;

    .line 88
    const-class v0, Lcom/squareup/api/items/TicketGroup;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketGroup;

    invoke-virtual {v0}, Lcom/squareup/api/items/TicketGroup;->newBuilder()Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplates:Ljava/util/List;

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    .line 93
    iget-object v3, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplates:Ljava/util/List;

    const-class v4, Lcom/squareup/api/items/TicketTemplate;

    invoke-static {p1, v4}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 96
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    .line 99
    iget-object v3, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    const-class v4, Lcom/squareup/api/items/TicketTemplate;

    invoke-static {p1, v4}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/TicketTemplate;

    invoke-virtual {v4}, Lcom/squareup/api/items/TicketTemplate;->newBuilder()Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 102
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->deletedTicketTemplateIds:Ljava/util/Set;

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v0, :cond_2

    .line 105
    iget-object v3, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->deletedTicketTemplateIds:Ljava/util/Set;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 108
    :cond_2
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->newTicketTemplateIds:Ljava/util/Set;

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    :goto_3
    if-ge v1, v0, :cond_3

    .line 111
    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->newTicketTemplateIds:Ljava/util/Set;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 114
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->automaticTemplateCount:I

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/api/items/TicketGroup;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/items/TicketGroup;",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/TicketTemplate;",
            ">;)V"
        }
    .end annotation

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroup:Lcom/squareup/api/items/TicketGroup;

    .line 50
    sget-object v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->TICKET_TEMPLATE_COMPARATOR:Ljava/util/Comparator;

    invoke-static {p2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 51
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplates:Ljava/util/List;

    .line 53
    invoke-virtual {p1}, Lcom/squareup/api/items/TicketGroup;->newBuilder()Lcom/squareup/api/items/TicketGroup$Builder;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

    .line 54
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->deletedTicketTemplateIds:Ljava/util/Set;

    .line 55
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->newTicketTemplateIds:Ljava/util/Set;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    .line 59
    iget-object p1, p1, Lcom/squareup/api/items/TicketGroup;->naming_method:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    sget-object v0, Lcom/squareup/api/items/TicketGroup$NamingMethod;->MANUAL:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    if-ne p1, v0, :cond_0

    .line 60
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplates:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketTemplate;

    .line 61
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    invoke-virtual {v0}, Lcom/squareup/api/items/TicketTemplate;->newBuilder()Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 64
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->setInitialAutomaticTemplateCount(I)V

    .line 67
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->addEmptyTicketTemplateState()V

    return-void
.end method

.method private appendChangesForAutomaticTemplates(Ljava/util/List;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)V"
        }
    .end annotation

    .line 315
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->hasNamingMethodChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplates:Ljava/util/List;

    .line 322
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    .line 325
    :goto_1
    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->templatesRemain(II)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 326
    invoke-direct {p0, v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getAutomaticName(I)Ljava/lang/String;

    move-result-object v3

    .line 327
    invoke-direct {p0, v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->hasExcessTemplates(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 329
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/TicketTemplate;

    invoke-static {v3}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->create(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 330
    :cond_1
    invoke-direct {p0, v1, v2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->needsMoreTemplates(II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 332
    new-instance v4, Lcom/squareup/api/items/TicketTemplate$Builder;

    invoke-direct {v4}, Lcom/squareup/api/items/TicketTemplate$Builder;-><init>()V

    sget-object v5, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    iget-object v6, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroup:Lcom/squareup/api/items/TicketGroup;

    iget-object v6, v6, Lcom/squareup/api/items/TicketGroup;->id:Ljava/lang/String;

    .line 333
    invoke-virtual {v5, v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/api/items/TicketTemplate$Builder;->ticket_group(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v4

    .line 334
    invoke-static {}, Lcom/squareup/shared/catalog/utils/UUID;->generateId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/api/items/TicketTemplate$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v4

    .line 335
    invoke-virtual {v4, v3}, Lcom/squareup/api/items/TicketTemplate$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v3

    .line 336
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/api/items/TicketTemplate$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v3

    .line 337
    invoke-virtual {v3}, Lcom/squareup/api/items/TicketTemplate$Builder;->build()Lcom/squareup/api/items/TicketTemplate;

    move-result-object v3

    .line 332
    invoke-static {v3}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->create(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 338
    :cond_2
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/TicketTemplate;

    invoke-direct {p0, v3, v4}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->groupNameHasChanged(Ljava/lang/String;Lcom/squareup/api/items/TicketTemplate;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 340
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/TicketTemplate;

    .line 341
    invoke-virtual {v4}, Lcom/squareup/api/items/TicketTemplate;->newBuilder()Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v4

    sget-object v5, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    iget-object v6, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroup:Lcom/squareup/api/items/TicketGroup;

    iget-object v6, v6, Lcom/squareup/api/items/TicketGroup;->id:Ljava/lang/String;

    .line 342
    invoke-virtual {v5, v6}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/api/items/TicketTemplate$Builder;->ticket_group(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v4

    .line 343
    invoke-virtual {v4, v3}, Lcom/squareup/api/items/TicketTemplate$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v3

    .line 344
    invoke-virtual {v3}, Lcom/squareup/api/items/TicketTemplate$Builder;->build()Lcom/squareup/api/items/TicketTemplate;

    move-result-object v3

    .line 340
    invoke-static {v3}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->create(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_4
    return-void
.end method

.method private appendChangesForCustomTemplates(Ljava/util/List;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)V"
        }
    .end annotation

    .line 357
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplates:Ljava/util/List;

    .line 358
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->deletedTicketTemplateIds:Ljava/util/Set;

    .line 360
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->hasNamingMethodChanged()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 362
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 363
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 367
    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 368
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 369
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/TicketTemplate$Builder;

    .line 370
    iget-object v4, v3, Lcom/squareup/api/items/TicketTemplate$Builder;->name:Ljava/lang/String;

    invoke-static {v4}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 371
    iget-object v4, v3, Lcom/squareup/api/items/TicketTemplate$Builder;->id:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 372
    iget-object v3, v3, Lcom/squareup/api/items/TicketTemplate$Builder;->id:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 374
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 379
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/api/items/TicketTemplate;

    .line 380
    iget-object v4, v3, Lcom/squareup/api/items/TicketTemplate;->id:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 381
    invoke-static {v3}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->create(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    const/4 p2, 0x0

    .line 387
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    :goto_2
    if-ge p2, v1, :cond_8

    .line 388
    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/items/TicketTemplate$Builder;

    .line 389
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge p2, v3, :cond_6

    invoke-virtual {v2}, Lcom/squareup/api/items/TicketTemplate$Builder;->build()Lcom/squareup/api/items/TicketTemplate;

    move-result-object v3

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/api/items/TicketTemplate;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 391
    :cond_6
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/api/items/TicketTemplate$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v2

    .line 392
    invoke-virtual {v2}, Lcom/squareup/api/items/TicketTemplate$Builder;->build()Lcom/squareup/api/items/TicketTemplate;

    move-result-object v2

    .line 390
    invoke-static {v2}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->create(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    :cond_8
    return-void
.end method

.method private getAutomaticName(I)Ljava/lang/String;
    .locals 2

    .line 414
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

    iget-object v1, v1, Lcom/squareup/api/items/TicketGroup$Builder;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private groupNameHasChanged(Ljava/lang/String;Lcom/squareup/api/items/TicketTemplate;)Z
    .locals 0

    .line 410
    iget-object p2, p2, Lcom/squareup/api/items/TicketTemplate;->name:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method private hasExcessTemplates(I)Z
    .locals 1

    .line 402
    iget v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->automaticTemplateCount:I

    if-lt p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method static synthetic lambda$static$0(Lcom/squareup/api/items/TicketTemplate;Lcom/squareup/api/items/TicketTemplate;)I
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/api/items/TicketTemplate;->ordinal:Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    iget-object p1, p1, Lcom/squareup/api/items/TicketTemplate;->ordinal:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    sub-int/2addr p0, p1

    return p0
.end method

.method private needsMoreTemplates(II)Z
    .locals 0

    if-lt p2, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private templatesRemain(II)Z
    .locals 1

    .line 398
    iget v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->automaticTemplateCount:I

    if-lt p2, v0, :cond_1

    if-ge p2, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public addEmptyTicketTemplateState()V
    .locals 3

    .line 192
    new-instance v0, Lcom/squareup/api/items/TicketTemplate$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/TicketTemplate$Builder;-><init>()V

    sget-object v1, Lcom/squareup/shared/catalog/models/CatalogObjectType;->TICKET_GROUP:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroup:Lcom/squareup/api/items/TicketGroup;

    iget-object v2, v2, Lcom/squareup/api/items/TicketGroup;->id:Ljava/lang/String;

    .line 193
    invoke-virtual {v1, v2}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->wrapId(Ljava/lang/String;)Lcom/squareup/api/sync/ObjectId;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TicketTemplate$Builder;->ticket_group(Lcom/squareup/api/sync/ObjectId;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v0

    .line 194
    invoke-static {}, Lcom/squareup/shared/catalog/utils/UUID;->generateId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TicketTemplate$Builder;->id(Ljava/lang/String;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v0

    const-string v1, ""

    .line 195
    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TicketTemplate$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/TicketTemplate$Builder;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->newTicketTemplateIds:Ljava/util/Set;

    iget-object v0, v0, Lcom/squareup/api/items/TicketTemplate$Builder;->id:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public appendTemplateChanges(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)V"
        }
    .end annotation

    .line 291
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->hasNamingMethodChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/api/items/TicketTemplate;

    .line 293
    invoke-static {v1}, Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;->create(Lcom/squareup/api/items/TicketTemplate;)Lcom/squareup/shared/catalog/models/CatalogTicketTemplate;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 297
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->isAutomaticallyNumbered()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 298
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->appendChangesForAutomaticTemplates(Ljava/util/List;Ljava/util/List;)V

    goto :goto_1

    .line 300
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->appendChangesForCustomTemplates(Ljava/util/List;Ljava/util/List;)V

    :goto_1
    return-void
.end method

.method public buildTicketGroup()Lcom/squareup/api/items/TicketGroup;
    .locals 1

    .line 230
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

    invoke-virtual {v0}, Lcom/squareup/api/items/TicketGroup$Builder;->build()Lcom/squareup/api/items/TicketGroup;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAutomaticTemplateCount()I
    .locals 1

    .line 134
    iget v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->automaticTemplateCount:I

    return v0
.end method

.method public getCustomTemplateCount()I
    .locals 1

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/TicketGroup$Builder;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getNamingMethod()Lcom/squareup/api/items/TicketGroup$NamingMethod;
    .locals 1

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/TicketGroup$Builder;->naming_method:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    return-object v0
.end method

.method public getOrdinal()I
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/TicketGroup$Builder;->ordinal:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getTicketGroupId()Ljava/lang/String;
    .locals 1

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/TicketGroup$Builder;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getTicketTemplateCount()I
    .locals 1

    .line 151
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->isAutomaticallyNumbered()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getAutomaticTemplateCount()I

    move-result v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getCustomTemplateCount()I

    move-result v0

    :goto_0
    return v0
.end method

.method public getTicketTemplateStates()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/TicketTemplate$Builder;",
            ">;"
        }
    .end annotation

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    return-object v0
.end method

.method public getTicketTemplates()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/TicketTemplate;",
            ">;"
        }
    .end annotation

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplates:Ljava/util/List;

    return-object v0
.end method

.method public hasNamingMethodChanged()Z
    .locals 2

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/TicketGroup$Builder;->naming_method:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroup:Lcom/squareup/api/items/TicketGroup;

    iget-object v1, v1, Lcom/squareup/api/items/TicketGroup;->naming_method:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasTicketGroupChanged()Z
    .locals 2

    .line 206
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->buildTicketGroup()Lcom/squareup/api/items/TicketGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroup:Lcom/squareup/api/items/TicketGroup;

    invoke-virtual {v0, v1}, Lcom/squareup/api/items/TicketGroup;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasTicketTemplatesChanged()Z
    .locals 6

    .line 210
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->isAutomaticallyNumbered()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    .line 211
    iget v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->automaticTemplateCount:I

    iget v3, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->initialAutomaticTemplateCount:I

    if-eq v0, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v3, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    sub-int/2addr v3, v2

    if-eq v0, v3, :cond_2

    return v2

    .line 216
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v0, :cond_4

    .line 217
    iget-object v4, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplates:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/api/items/TicketTemplate;

    iget-object v5, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/api/items/TicketTemplate$Builder;

    invoke-virtual {v5}, Lcom/squareup/api/items/TicketTemplate$Builder;->build()Lcom/squareup/api/items/TicketTemplate;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/api/items/TicketTemplate;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    return v2

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    return v1
.end method

.method public isAutomaticallyNumbered()Z
    .locals 2

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

    iget-object v0, v0, Lcom/squareup/api/items/TicketGroup$Builder;->naming_method:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    sget-object v1, Lcom/squareup/api/items/TicketGroup$NamingMethod;->AUTOMATIC_NUMBERING:Lcom/squareup/api/items/TicketGroup$NamingMethod;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isConvertingFromCustomToAutomaticTemplates()Z
    .locals 1

    .line 242
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->hasNamingMethodChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->isAutomaticallyNumbered()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->getCustomTemplateCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isLastTicketTemplate(Lcom/squareup/api/items/TicketTemplate$Builder;)Z
    .locals 2

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isNewTicketTemplate(Lcom/squareup/api/items/TicketTemplate$Builder;)Z
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->newTicketTemplateIds:Ljava/util/Set;

    iget-object p1, p1, Lcom/squareup/api/items/TicketTemplate$Builder;->id:Ljava/lang/String;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public removeEmptyTicketTemplateState()V
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketTemplate$Builder;

    .line 202
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->newTicketTemplateIds:Ljava/util/Set;

    iget-object v0, v0, Lcom/squareup/api/items/TicketTemplate$Builder;->id:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public removeTicketTemplateState(Lcom/squareup/api/items/TicketTemplate$Builder;)Z
    .locals 2

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 169
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->isNewTicketTemplate(Lcom/squareup/api/items/TicketTemplate$Builder;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 170
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->newTicketTemplateIds:Ljava/util/Set;

    iget-object p1, p1, Lcom/squareup/api/items/TicketTemplate$Builder;->id:Ljava/lang/String;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 172
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->deletedTicketTemplateIds:Ljava/util/Set;

    iget-object p1, p1, Lcom/squareup/api/items/TicketTemplate$Builder;->id:Ljava/lang/String;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    return v0
.end method

.method public setAutomaticTemplateCount(I)V
    .locals 0

    .line 138
    iput p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->automaticTemplateCount:I

    return-void
.end method

.method public setInitialAutomaticTemplateCount(I)V
    .locals 0

    .line 142
    iput p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->automaticTemplateCount:I

    iput p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->initialAutomaticTemplateCount:I

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TicketGroup$Builder;->name(Ljava/lang/String;)Lcom/squareup/api/items/TicketGroup$Builder;

    return-void
.end method

.method public setNamingMethod(Lcom/squareup/api/items/TicketGroup$NamingMethod;)V
    .locals 1

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TicketGroup$Builder;->naming_method(Lcom/squareup/api/items/TicketGroup$NamingMethod;)Lcom/squareup/api/items/TicketGroup$Builder;

    return-void
.end method

.method public setOrdinal(I)V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/api/items/TicketGroup$Builder;->ordinal(Ljava/lang/Integer;)Lcom/squareup/api/items/TicketGroup$Builder;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 262
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroup:Lcom/squareup/api/items/TicketGroup;

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    .line 263
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->currentGroupState:Lcom/squareup/api/items/TicketGroup$Builder;

    invoke-virtual {p2}, Lcom/squareup/api/items/TicketGroup$Builder;->build()Lcom/squareup/api/items/TicketGroup;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    .line 264
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplates:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 265
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplates:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketTemplate;

    .line 266
    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    goto :goto_0

    .line 268
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 269
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->ticketTemplateStates:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketTemplate$Builder;

    .line 270
    invoke-virtual {v0}, Lcom/squareup/api/items/TicketTemplate$Builder;->build()Lcom/squareup/api/items/TicketTemplate;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    goto :goto_1

    .line 272
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->deletedTicketTemplateIds:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 273
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->deletedTicketTemplateIds:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 274
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 276
    :cond_2
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->newTicketTemplateIds:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 277
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->newTicketTemplateIds:Ljava/util/Set;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 278
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3

    .line 280
    :cond_3
    iget p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupState;->automaticTemplateCount:I

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
