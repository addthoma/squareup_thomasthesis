.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;
.super Lcom/squareup/ui/ReorderableRecyclerViewAdapter;
.source "EditTicketGroupRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;,
        Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/ReorderableRecyclerViewAdapter<",
        "Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;",
        "Lcom/squareup/api/items/TicketTemplate$Builder;",
        ">;"
    }
.end annotation


# static fields
.field static final AUTOMATIC_NUMBERING_COUNTER_ROW:I = 0x1

.field static final CUSTOM_TICKET_HEADER_ROW:I = 0x2

.field static final CUSTOM_TICKET_TEMPLATE_ROW:I = 0x3

.field static final EDIT_TICKET_GROUP_DELETE_BUTTON_ROW:I = 0x4

.field static final EDIT_TICKET_GROUP_HEADER_ROW:I


# instance fields
.field private editTicketGroupView:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

.field private presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

.field private showCustomTemplates:Z

.field private showDeleteButton:Z


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)V
    .locals 0

    .line 165
    invoke-direct {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;-><init>()V

    .line 166
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->editTicketGroupView:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    .line 167
    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    return-void
.end method


# virtual methods
.method public getDraggableItemCount()I
    .locals 1

    .line 213
    iget-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->showCustomTemplates:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/squareup/ui/ReorderableRecyclerViewAdapter;->getDraggableItemCount()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getDraggableItemId(I)J
    .locals 2

    .line 217
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->getDraggableItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/items/TicketTemplate$Builder;

    iget-object p1, p1, Lcom/squareup/api/items/TicketTemplate$Builder;->id:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 3

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 192
    iget-boolean p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->showCustomTemplates:Z

    if-eqz p1, :cond_1

    const/4 v0, 0x2

    :cond_1
    return v0

    .line 195
    :cond_2
    iget-boolean v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->showDeleteButton:Z

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->getItemCount()I

    move-result v1

    sub-int/2addr v1, v0

    if-ne p1, v1, :cond_3

    const/4 p1, 0x4

    return p1

    .line 197
    :cond_3
    iget-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->showCustomTemplates:Z

    if-eqz v0, :cond_4

    const/4 p1, 0x3

    return p1

    .line 200
    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not a valid position "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public getStaticBottomRowsCount()I
    .locals 1

    .line 209
    iget-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->showDeleteButton:Z

    return v0
.end method

.method public getStaticTopRowsCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->onBindViewHolder(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;I)V
    .locals 0

    .line 185
    invoke-virtual {p0, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->getDraggableItemAtPosition(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/api/items/TicketTemplate$Builder;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;->bindData(Lcom/squareup/api/items/TicketTemplate$Builder;)V

    return-void
.end method

.method public bridge synthetic onCreateReorderableViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/ReorderableRecyclerViewAdapter$ViewHolder;
    .locals 0

    .line 20
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->onCreateReorderableViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateReorderableViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;
    .locals 2

    if-eqz p2, :cond_2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_2

    const/4 v0, 0x2

    if-eq p2, v0, :cond_2

    const/4 v0, 0x3

    if-eq p2, v0, :cond_1

    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    goto :goto_0

    .line 180
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported view type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 178
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->presenter:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;

    invoke-static {p1, p2}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->inflate(Landroid/view/ViewGroup;Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupPresenter;)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

    move-result-object p1

    return-object p1

    .line 176
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->editTicketGroupView:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    invoke-static {p1, p2, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;->inflate(Landroid/view/ViewGroup;ILcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$BasicViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public requestFocusOnNewCustomTicketRow(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 2

    .line 236
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->getStaticTopRowsCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .line 238
    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->getDraggableItemId(I)J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForItemId(J)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;

    if-eqz p1, :cond_0

    .line 240
    iget-object v0, p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;->requestFocus()Z

    .line 241
    iget-object p1, p1, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter$TicketTemplateViewHolder;->ticketTemplateRow:Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketTemplateRow;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method public setDeleteButtonVisible(Z)V
    .locals 0

    .line 231
    iput-boolean p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->showDeleteButton:Z

    .line 232
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public showAutomaticNameTickets()V
    .locals 1

    const/4 v0, 0x0

    .line 226
    iput-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->showCustomTemplates:Z

    .line 227
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public showCustomNameTickets()V
    .locals 1

    const/4 v0, 0x1

    .line 221
    iput-boolean v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->showCustomTemplates:Z

    .line 222
    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method
