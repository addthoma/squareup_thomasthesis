.class final Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1;
.super Lkotlin/jvm/internal/Lambda;
.source "TipSettingsView.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/tipping/TipSettingsView;->update(Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/mosaic/core/UiModelContext<",
        "Lkotlin/Unit;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00010\u0002H\n\u00a2\u0006\u0002\u0008\u0003"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/mosaic/core/UiModelContext;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1;->$rendering:Lcom/squareup/ui/settings/tipping/TipSettingsView$Rendering;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Lcom/squareup/mosaic/core/UiModelContext;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1;->invoke(Lcom/squareup/mosaic/core/UiModelContext;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/mosaic/core/UiModelContext;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/mosaic/core/UiModelContext<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    new-instance v0, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1$1;-><init>(Lcom/squareup/ui/settings/tipping/TipSettingsView$update$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/mosaic/components/VerticalScrollUiModelKt;->verticalScroll(Lcom/squareup/mosaic/core/UiModelContext;Lkotlin/jvm/functions/Function1;)V

    return-void
.end method
