.class public final Lcom/squareup/ui/settings/tipping/TipSettingsScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "TipSettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Component;,
        Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/tipping/TipSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field private static final CUSTOM_TIPS_KEY:Ljava/lang/String; = "custom-tips"

.field public static final INSTANCE:Lcom/squareup/ui/settings/tipping/TipSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/tipping/TipSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/tipping/TipSettingsScreen;

    .line 213
    sget-object v0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/tipping/TipSettingsScreen;

    .line 214
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 57
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_TIPPING:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 53
    const-class v0, Lcom/squareup/ui/settings/tipping/TippingSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 217
    sget v0, Lcom/squareup/settingsapplet/R$layout;->tip_settings_view:I

    return v0
.end method
