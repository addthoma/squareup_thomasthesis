.class public Lcom/squareup/ui/settings/tipping/TippingSection;
.super Lcom/squareup/applet/AppletSection;
.source "TippingSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/tipping/TippingSection$Access;,
        Lcom/squareup/ui/settings/tipping/TippingSection$ListEntry;,
        Lcom/squareup/ui/settings/tipping/TippingSection$TippingCheckoutListEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    sget v0, Lcom/squareup/registerlib/R$string;->tips_title:I

    sput v0, Lcom/squareup/ui/settings/tipping/TippingSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 22
    new-instance v0, Lcom/squareup/ui/settings/tipping/TippingSection$Access;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/settings/tipping/TippingSection$Access;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/server/Features;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/settings/tipping/TippingSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 26
    sget-object v0, Lcom/squareup/ui/settings/tipping/TipSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/tipping/TipSettingsScreen;

    return-object v0
.end method
