.class public Lcom/squareup/ui/settings/RealSettingsAppletGateway;
.super Ljava/lang/Object;
.source "RealSettingsAppletGateway.java"

# interfaces
.implements Lcom/squareup/ui/settings/SettingsAppletGateway;


# instance fields
.field private final bankAccountSection:Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

.field private final depositsSection:Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

.field private final settingsApplet:Lcom/squareup/ui/settings/SettingsApplet;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/instantdeposits/DepositsSection;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/SettingsApplet;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/ui/settings/RealSettingsAppletGateway;->depositsSection:Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    .line 15
    iput-object p2, p0, Lcom/squareup/ui/settings/RealSettingsAppletGateway;->bankAccountSection:Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

    .line 16
    iput-object p3, p0, Lcom/squareup/ui/settings/RealSettingsAppletGateway;->settingsApplet:Lcom/squareup/ui/settings/SettingsApplet;

    return-void
.end method


# virtual methods
.method public activate()V
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/settings/RealSettingsAppletGateway;->settingsApplet:Lcom/squareup/ui/settings/SettingsApplet;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsApplet;->activate()V

    return-void
.end method

.method public activateBankAccountSettings()V
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/settings/RealSettingsAppletGateway;->settingsApplet:Lcom/squareup/ui/settings/SettingsApplet;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsApplet;->activateBankAccountSettings()V

    return-void
.end method

.method public activateDepositsSettings()V
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/settings/RealSettingsAppletGateway;->settingsApplet:Lcom/squareup/ui/settings/SettingsApplet;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsApplet;->activateDepositsSettings()V

    return-void
.end method

.method public isBankAccountsVisible()Z
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/RealSettingsAppletGateway;->bankAccountSection:Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result v0

    return v0
.end method

.method public isInstantDepositsVisible()Z
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/settings/RealSettingsAppletGateway;->depositsSection:Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/applet/SectionAccess;->determineVisibility()Z

    move-result v0

    return v0
.end method
