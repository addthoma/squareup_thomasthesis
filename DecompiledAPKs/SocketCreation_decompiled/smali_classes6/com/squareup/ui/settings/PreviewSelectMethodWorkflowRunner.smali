.class public final Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "PreviewSelectMethodWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$ParentComponent;,
        Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u0000 \u00152\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0002\u0015\u0016B\'\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0011H\u0014J\u000e\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u0014R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;",
        "",
        "workflow",
        "Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;",
        "viewFactory",
        "Lcom/squareup/tenderpayment/PaymentViewFactory;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "tenderOptionListsFactory",
        "Lcom/squareup/checkoutflow/TenderOptionListsFactory;",
        "(Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;Lcom/squareup/tenderpayment/PaymentViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/TenderOptionListsFactory;)V",
        "getWorkflow",
        "()Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;",
        "onEnterScope",
        "newScope",
        "Lmortar/MortarScope;",
        "start",
        "settings",
        "Lcom/squareup/protos/client/devicesettings/TenderSettings;",
        "Companion",
        "ParentComponent",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final tenderOptionListsFactory:Lcom/squareup/checkoutflow/TenderOptionListsFactory;

.field private final workflow:Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->Companion:Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$Companion;

    .line 57
    const-class v0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PreviewSelectMethodWorkflowRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;Lcom/squareup/tenderpayment/PaymentViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/TenderOptionListsFactory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "workflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderOptionListsFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    sget-object v2, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->NAME:Ljava/lang/String;

    .line 27
    invoke-interface {p3}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    .line 28
    move-object v4, p2

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 25
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->workflow:Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;

    iput-object p3, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p4, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->tenderOptionListsFactory:Lcom/squareup/checkoutflow/TenderOptionListsFactory;

    return-void
.end method

.method public static final synthetic access$getContainer$p(Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;
    .locals 0

    .line 19
    iget-object p0, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    return-object p0
.end method

.method public static final synthetic access$getNAME$cp()Ljava/lang/String;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->NAME:Ljava/lang/String;

    return-object v0
.end method

.method public static final get(Lmortar/MortarScope;)Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->Companion:Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->workflow:Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->getWorkflow()Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-super {p0, p1}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 47
    invoke-virtual {p0}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    .line 48
    new-instance v1, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onUpdateScreens()\n      \u2026ibe(container::pushStack)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 51
    invoke-virtual {p0}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 52
    new-instance v1, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onResult()\n        .subs\u2026cope<WorkflowTreeKey>() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public final start(Lcom/squareup/protos/client/devicesettings/TenderSettings;)V
    .locals 1

    const-string v0, "settings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    new-instance p1, Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;

    iget-object v0, p0, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->tenderOptionListsFactory:Lcom/squareup/checkoutflow/TenderOptionListsFactory;

    invoke-interface {v0}, Lcom/squareup/checkoutflow/TenderOptionListsFactory;->getTenderOptionLists()Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/squareup/tenderpayment/PreviewSelectMethodScreenWorkflow$InitEvent;-><init>(Lcom/squareup/checkoutflow/selecttender/tenderoption/TenderOptionLists;)V

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->setProps(Ljava/lang/Object;)V

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->startOrRestart()V

    return-void
.end method
