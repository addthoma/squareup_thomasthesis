.class public Lcom/squareup/ui/settings/cashmanagement/CloseDrawerConfirmationDialog;
.super Lcom/squareup/settings/cashmanagement/InCashManagementScope;
.source "CloseDrawerConfirmationDialog.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/cashmanagement/CloseDrawerConfirmationDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/cashmanagement/CloseDrawerConfirmationDialog$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/cashmanagement/CloseDrawerConfirmationDialog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/ui/settings/cashmanagement/-$$Lambda$CloseDrawerConfirmationDialog$OZfBLD4bVGHW4Z2qOwuwdefzsdY;->INSTANCE:Lcom/squareup/ui/settings/cashmanagement/-$$Lambda$CloseDrawerConfirmationDialog$OZfBLD4bVGHW4Z2qOwuwdefzsdY;

    .line 53
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/cashmanagement/CloseDrawerConfirmationDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/settings/cashmanagement/CashManagementScope;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1}, Lcom/squareup/settings/cashmanagement/InCashManagementScope;-><init>(Lcom/squareup/settings/cashmanagement/CashManagementScope;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/settings/cashmanagement/CashManagementScope;

    invoke-direct {v0, p1}, Lcom/squareup/settings/cashmanagement/CashManagementScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/cashmanagement/CloseDrawerConfirmationDialog;-><init>(Lcom/squareup/settings/cashmanagement/CashManagementScope;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/cashmanagement/CloseDrawerConfirmationDialog;
    .locals 1

    .line 54
    const-class v0, Lcom/squareup/settings/cashmanagement/CashManagementScope;

    .line 55
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/cashmanagement/CashManagementScope;

    .line 56
    new-instance v0, Lcom/squareup/ui/settings/cashmanagement/CloseDrawerConfirmationDialog;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/cashmanagement/CloseDrawerConfirmationDialog;-><init>(Lcom/squareup/settings/cashmanagement/CashManagementScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 48
    invoke-super {p0, p1, p2}, Lcom/squareup/settings/cashmanagement/InCashManagementScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/settings/cashmanagement/CloseDrawerConfirmationDialog;->getParentKey()Lcom/squareup/settings/cashmanagement/CashManagementScope;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
