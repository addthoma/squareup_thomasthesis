.class public final Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "HardwarePrinterSelectScreen.kt"

# interfaces
.implements Lcom/squareup/print/HardwarePrinterTracker$Listener;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;",
        ">;",
        "Lcom/squareup/print/HardwarePrinterTracker$Listener;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHardwarePrinterSelectScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HardwarePrinterSelectScreen.kt\ncom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,271:1\n1550#2,3:272\n*E\n*S KotlinDebug\n*F\n+ 1 HardwarePrinterSelectScreen.kt\ncom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter\n*L\n229#1,3:272\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0094\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0010!\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0001\u0018\u0000 R2\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0001RBW\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u00a2\u0006\u0002\u0010\u0018J\r\u0010,\u001a\u00020-H\u0000\u00a2\u0006\u0002\u0008.J\u0010\u0010/\u001a\u00020-2\u0006\u00100\u001a\u00020\u0002H\u0016J\u0016\u00101\u001a\u00020-2\u000c\u00102\u001a\u0008\u0012\u0004\u0012\u00020\u001b03H\u0002J\r\u00104\u001a\u00020-H\u0000\u00a2\u0006\u0002\u00085J\u0010\u00106\u001a\u00020\'2\u0006\u00107\u001a\u00020\'H\u0002J\u0012\u00108\u001a\u0004\u0018\u00010\'2\u0006\u00109\u001a\u00020:H\u0007J\u0010\u0010;\u001a\u00020\'2\u0006\u0010<\u001a\u00020\'H\u0002J\u0010\u0010=\u001a\u00020\u001b2\u0006\u00107\u001a\u00020\'H\u0002J\u0017\u0010>\u001a\u00020\u001f2\u0008\u00107\u001a\u0004\u0018\u00010\'H\u0000\u00a2\u0006\u0002\u0008?J\u0015\u0010@\u001a\u00020-2\u0006\u00107\u001a\u00020\'H\u0000\u00a2\u0006\u0002\u0008AJ\u0012\u0010B\u001a\u00020-2\u0008\u0010C\u001a\u0004\u0018\u00010DH\u0014J\r\u0010E\u001a\u00020-H\u0000\u00a2\u0006\u0002\u0008FJ\u0012\u0010G\u001a\u00020-2\u0008\u0010H\u001a\u0004\u0018\u00010DH\u0014J\r\u0010I\u001a\u00020-H\u0000\u00a2\u0006\u0002\u0008JJ\u0010\u0010K\u001a\u00020-2\u0006\u0010L\u001a\u00020MH\u0016J\u0010\u0010N\u001a\u00020-2\u0006\u0010L\u001a\u00020MH\u0016J\r\u0010O\u001a\u00020-H\u0000\u00a2\u0006\u0002\u0008PJ\u0008\u0010Q\u001a\u00020-H\u0002R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0019\u001a\n\u0012\u0004\u0012\u00020\u001b\u0018\u00010\u001a8\u0002@\u0002X\u0083\u000e\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u001c\u0010\u001dR\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001e\u001a\u00020\u001f8@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008 \u0010!R\u0014\u0010\"\u001a\u00020\u001f8@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008#\u0010!R\u0014\u0010$\u001a\u00020\u001f8@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008%\u0010!R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010&\u001a\u0004\u0018\u00010\'X\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008(\u0010)\"\u0004\u0008*\u0010+R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006S"
    }
    d2 = {
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;",
        "Lmortar/ViewPresenter;",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;",
        "Lcom/squareup/print/HardwarePrinterTracker$Listener;",
        "res",
        "Lcom/squareup/util/Res;",
        "scopeRunner",
        "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
        "hardwarePrinterTracker",
        "Lcom/squareup/print/HardwarePrinterTracker;",
        "printerStations",
        "Lcom/squareup/print/PrinterStations;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "printerStationState",
        "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;",
        "paperSignatureSettings",
        "Lcom/squareup/papersignature/PaperSignatureSettings;",
        "flow",
        "Lflow/Flow;",
        "testPrint",
        "Lcom/squareup/ui/settings/printerstations/station/TestPrint;",
        "appNameFormatter",
        "Lcom/squareup/util/AppNameFormatter;",
        "(Lcom/squareup/util/Res;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterStations;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;Lcom/squareup/papersignature/PaperSignatureSettings;Lflow/Flow;Lcom/squareup/ui/settings/printerstations/station/TestPrint;Lcom/squareup/util/AppNameFormatter;)V",
        "hardwarePrinterSelectList",
        "",
        "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;",
        "hardwarePrinterSelectList$annotations",
        "()V",
        "isPrintTestEnabled",
        "",
        "isPrintTestEnabled$settings_applet_release",
        "()Z",
        "isPrinterSelected",
        "isPrinterSelected$settings_applet_release",
        "isSelectedPrinterConnected",
        "isSelectedPrinterConnected$settings_applet_release",
        "selectedHardwarePrinterId",
        "",
        "getSelectedHardwarePrinterId$settings_applet_release",
        "()Ljava/lang/String;",
        "setSelectedHardwarePrinterId$settings_applet_release",
        "(Ljava/lang/String;)V",
        "commit",
        "",
        "commit$settings_applet_release",
        "dropView",
        "view",
        "ensureListContainsSelectedHardwarePrinterId",
        "hardwarePrinterList",
        "",
        "finish",
        "finish$settings_applet_release",
        "getFormattedStationsStringForPrinterId",
        "hardwarePrinterId",
        "getHardwarePrinterId",
        "position",
        "",
        "getHardwarePrinterName",
        "id",
        "getHardwarePrinterSelect",
        "isPrinterAtPositionSelected",
        "isPrinterAtPositionSelected$settings_applet_release",
        "onHardwarePrinterClicked",
        "onHardwarePrinterClicked$settings_applet_release",
        "onLoad",
        "savedInstanceState",
        "Landroid/os/Bundle;",
        "onNoPrinterSelectedClicked",
        "onNoPrinterSelectedClicked$settings_applet_release",
        "onSave",
        "outState",
        "onTestPrintClicked",
        "onTestPrintClicked$settings_applet_release",
        "printerConnected",
        "hardwarePrinter",
        "Lcom/squareup/print/HardwarePrinter;",
        "printerDisconnected",
        "refresh",
        "refresh$settings_applet_release",
        "warnIfCannotSignOnPrintedReceipt",
        "Companion",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter$Companion;

.field private static final SELECTED_HARDWARE_PRINTER_ID_KEY:Ljava/lang/String; = "selected_hardware_printer_id"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final appNameFormatter:Lcom/squareup/util/AppNameFormatter;

.field private final flow:Lflow/Flow;

.field private hardwarePrinterSelectList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;",
            ">;"
        }
    .end annotation
.end field

.field private final hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field private final printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

.field private selectedHardwarePrinterId:Ljava/lang/String;

.field private final testPrint:Lcom/squareup/ui/settings/printerstations/station/TestPrint;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->Companion:Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterStations;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;Lcom/squareup/papersignature/PaperSignatureSettings;Lflow/Flow;Lcom/squareup/ui/settings/printerstations/station/TestPrint;Lcom/squareup/util/AppNameFormatter;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scopeRunner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hardwarePrinterTracker"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printerStations"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printerStationState"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paperSignatureSettings"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "testPrint"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appNameFormatter"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->scopeRunner:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    iput-object p3, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    iput-object p4, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->printerStations:Lcom/squareup/print/PrinterStations;

    iput-object p5, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p6, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iput-object p7, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    iput-object p8, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->flow:Lflow/Flow;

    iput-object p9, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->testPrint:Lcom/squareup/ui/settings/printerstations/station/TestPrint;

    iput-object p10, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object p1, p1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    const-string p2, "printerStationState.builder"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    return-void
.end method

.method private final ensureListContainsSelectedHardwarePrinterId(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;",
            ">;)V"
        }
    .end annotation

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    const-string v1, "printerStationState.builder"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Ljava/lang/Iterable;

    .line 272
    instance-of v2, v0, Ljava/util/Collection;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    move-object v2, v0

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 273
    :cond_0
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;

    .line 230
    invoke-virtual {v2}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;->getPrinterId()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v4, v4, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v3, 0x1

    :cond_2
    :goto_0
    if-nez v3, :cond_3

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v0

    const-string v1, "printerStationState.buil\u2026selectedHardwarePrinterId"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->getHardwarePrinterSelect(Ljava/lang/String;)Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;

    move-result-object v0

    .line 233
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    return-void
.end method

.method private final getFormattedStationsStringForPrinterId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v0, p1}, Lcom/squareup/print/PrinterStations;->getDisplayableStationsStringForPrinter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final getHardwarePrinterName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v0, p1}, Lcom/squareup/print/HardwarePrinterTracker;->getCachedHardwareInfo(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getDisplayableModelName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object p1, v0

    :cond_0
    return-object p1
.end method

.method private final getHardwarePrinterSelect(Ljava/lang/String;)Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;
    .locals 3

    .line 192
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;

    .line 193
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->getHardwarePrinterName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 194
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->getFormattedStationsStringForPrinterId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 192
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static synthetic hardwarePrinterSelectList$annotations()V
    .locals 0

    return-void
.end method

.method private final warnIfCannotSignOnPrintedReceipt()V
    .locals 6

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v0}, Lcom/squareup/papersignature/PaperSignatureSettings;->isSignOnPrintedReceiptEnabled()Z

    move-result v0

    .line 205
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v1, v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    const-string v2, "printerStationState.builder"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/print/PrinterStationConfiguration;->NO_PRINTER_SELECTED_ID:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 206
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    sget-object v5, Lcom/squareup/print/PrinterStationConfiguration;->NO_PRINTER_SELECTED_ID:Ljava/lang/String;

    if-ne v2, v5, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    .line 207
    :goto_1
    iget-object v5, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v5}, Lcom/squareup/papersignature/PaperSignatureSettings;->getNumberOfReceiptPrinters()I

    move-result v5

    if-gt v5, v4, :cond_2

    const/4 v3, 0x1

    :cond_2
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    if-eqz v3, :cond_3

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->flow:Lflow/Flow;

    .line 213
    new-instance v1, Lcom/squareup/register/widgets/WarningDialogScreen;

    .line 214
    new-instance v2, Lcom/squareup/widgets/warning/WarningStrings;

    .line 215
    iget-object v3, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/settingsapplet/R$string;->printer_stations_cannot_sign_on_printed_receipt_title:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 216
    iget-object v4, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    .line 217
    sget v5, Lcom/squareup/settingsapplet/R$string;->printer_stations_cannot_sign_on_printed_receipt_content:I

    .line 216
    invoke-interface {v4, v5}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 218
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 214
    invoke-direct {v2, v3, v4}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/widgets/warning/Warning;

    .line 213
    invoke-direct {v1, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;)V

    .line 212
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_3
    return-void
.end method


# virtual methods
.method public final commit$settings_applet_release()V
    .locals 3

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    const-string v1, "printerStationState.builder"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    sget-object v2, Lcom/squareup/print/PrinterStationConfiguration;->NO_PRINTER_SELECTED_ID:Ljava/lang/String;

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->HARDWARE_PRINTER_NONE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    goto :goto_0

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->HARDWARE_PRINTER:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 170
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->printerStationState:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;->builder:Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->setSelectedHardwarePrinterId(Ljava/lang/String;)Lcom/squareup/print/PrinterStationConfiguration$Builder;

    .line 171
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->finish$settings_applet_release()V

    return-void
.end method

.method public dropView(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    move-object v1, p0

    check-cast v1, Lcom/squareup/print/HardwarePrinterTracker$Listener;

    invoke-virtual {v0, v1}, Lcom/squareup/print/HardwarePrinterTracker;->removeListener(Lcom/squareup/print/HardwarePrinterTracker$Listener;)V

    .line 111
    :cond_0
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic dropView(Ljava/lang/Object;)V
    .locals 0

    .line 52
    check-cast p1, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->dropView(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;)V

    return-void
.end method

.method public final finish$settings_applet_release()V
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->scopeRunner:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;->goBack()V

    return-void
.end method

.method public final getHardwarePrinterId(I)Ljava/lang/String;
    .locals 1

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->hardwarePrinterSelectList:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 179
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;->getPrinterId()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public final getSelectedHardwarePrinterId$settings_applet_release()Ljava/lang/String;
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    return-object v0
.end method

.method public final isPrintTestEnabled$settings_applet_release()Z
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    sget-object v1, Lcom/squareup/print/PrinterStationConfiguration;->NO_PRINTER_SELECTED_ID:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->isSelectedPrinterConnected$settings_applet_release()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->isSelectedPrinterConnected$settings_applet_release()Z

    move-result v0

    :goto_0
    return v0
.end method

.method public final isPrinterAtPositionSelected$settings_applet_release(Ljava/lang/String;)Z
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final isPrinterSelected$settings_applet_release()Z
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    sget-object v1, Lcom/squareup/print/PrinterStationConfiguration;->NO_PRINTER_SELECTED_ID:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final isSelectedPrinterConnected$settings_applet_release()Z
    .locals 2

    .line 84
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->isPrinterSelected$settings_applet_release()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/print/HardwarePrinterTracker;->getHardwarePrinter(Ljava/lang/String;)Lcom/squareup/print/HardwarePrinter;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final onHardwarePrinterClicked$settings_applet_release(Ljava/lang/String;)V
    .locals 1

    const-string v0, "hardwarePrinterId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    .line 128
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->updatePrinterList$settings_applet_release()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-eqz p1, :cond_0

    const-string v0, "selected_hardware_printer_id"

    .line 92
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    .line 95
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$string;->printer_stations_select_hardware_printer:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->getActionBar$settings_applet_release()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 96
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 97
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 98
    new-instance v1, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter$onLoad$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter$onLoad$1;-><init>(Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 99
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 102
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    move-object v0, p0

    check-cast v0, Lcom/squareup/print/HardwarePrinterTracker$Listener;

    invoke-virtual {p1, v0}, Lcom/squareup/print/HardwarePrinterTracker;->removeListener(Lcom/squareup/print/HardwarePrinterTracker$Listener;)V

    .line 103
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {p1, v0}, Lcom/squareup/print/HardwarePrinterTracker;->addListener(Lcom/squareup/print/HardwarePrinterTracker$Listener;)V

    .line 104
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->refresh$settings_applet_release()V

    return-void
.end method

.method public final onNoPrinterSelectedClicked$settings_applet_release()V
    .locals 1

    .line 132
    sget-object v0, Lcom/squareup/print/PrinterStationConfiguration;->NO_PRINTER_SELECTED_ID:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    .line 133
    invoke-direct {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->warnIfCannotSignOnPrintedReceipt()V

    .line 134
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->updatePrinterList$settings_applet_release()V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    .line 115
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    const-string v1, "selected_hardware_printer_id"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final onTestPrintClicked$settings_applet_release()V
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->testPrint:Lcom/squareup/ui/settings/printerstations/station/TestPrint;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/printerstations/station/TestPrint;->performTestPrint(Ljava/lang/String;)V

    return-void
.end method

.method public printerConnected(Lcom/squareup/print/HardwarePrinter;)V
    .locals 1

    const-string v0, "hardwarePrinter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->refresh$settings_applet_release()V

    return-void
.end method

.method public printerDisconnected(Lcom/squareup/print/HardwarePrinter;)V
    .locals 1

    const-string v0, "hardwarePrinter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->refresh$settings_applet_release()V

    return-void
.end method

.method public final refresh$settings_applet_release()V
    .locals 5

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 147
    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v1}, Lcom/squareup/print/HardwarePrinterTracker;->getAllAvailableHardwarePrinters()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/HardwarePrinter;

    const-string v3, "printer"

    .line 149
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    sget-object v4, Lcom/squareup/print/ConnectionType;->INTERNAL:Lcom/squareup/print/ConnectionType;

    if-eq v3, v4, :cond_0

    invoke-virtual {v2}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->connectionType:Lcom/squareup/print/ConnectionType;

    sget-object v4, Lcom/squareup/print/ConnectionType;->FAKE_INTERNAL:Lcom/squareup/print/ConnectionType;

    if-ne v3, v4, :cond_1

    goto :goto_0

    .line 153
    :cond_1
    invoke-virtual {v2}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v2

    const-string v3, "printer.id"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->getHardwarePrinterSelect(Ljava/lang/String;)Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$HardwarePrinterSelect;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 156
    :cond_2
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->ensureListContainsSelectedHardwarePrinterId(Ljava/util/List;)V

    .line 157
    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->hardwarePrinterSelectList:Ljava/util/List;

    .line 158
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectView;->refresh$settings_applet_release(Ljava/util/List;)V

    return-void
.end method

.method public final setSelectedHardwarePrinterId$settings_applet_release(Ljava/lang/String;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Presenter;->selectedHardwarePrinterId:Ljava/lang/String;

    return-void
.end method
