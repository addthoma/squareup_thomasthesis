.class public Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;
.super Ljava/lang/Object;
.source "PrinterStationScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)V
    .locals 0

    .line 94
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;->this$0:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideNewPrinterStation()Z
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;->this$0:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    invoke-static {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;->access$000(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;->access$100()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method providePrinterStation(Lcom/squareup/print/PrinterStations;Lcom/squareup/print/PrinterStationFactory;Z)Lcom/squareup/print/PrinterStation;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    if-eqz p3, :cond_0

    .line 100
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    .line 101
    invoke-interface {p2, p1}, Lcom/squareup/print/PrinterStationFactory;->generate(Ljava/lang/String;)Lcom/squareup/print/PrinterStation;

    move-result-object p1

    return-object p1

    .line 103
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;->this$0:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;

    invoke-static {p2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;->access$000(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;)Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/print/PrinterStations;->getPrinterStationById(Ljava/lang/String;)Lcom/squareup/print/PrinterStation;

    move-result-object p1

    return-object p1
.end method

.method provideState(Lcom/squareup/BundleKey;Lcom/squareup/print/PrinterStation;)Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/print/PrinterStationConfiguration$Builder;",
            ">;",
            "Lcom/squareup/print/PrinterStation;",
            ")",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;"
        }
    .end annotation

    .line 117
    invoke-virtual {p2}, Lcom/squareup/print/PrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/print/PrinterStationConfiguration;->buildUpon()Lcom/squareup/print/PrinterStationConfiguration$Builder;

    move-result-object p2

    .line 118
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;-><init>(Lcom/squareup/BundleKey;Lcom/squareup/print/PrinterStationConfiguration$Builder;)V

    return-object v0
.end method

.method provideStateKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/print/PrinterStationConfiguration$Builder;",
            ">;"
        }
    .end annotation

    .line 123
    const-class v0, Lcom/squareup/print/PrinterStationConfiguration$Builder;

    const-string v1, "PRINTER_STATION_BUNDLER_KEY"

    invoke-static {p1, v1, v0}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p1

    return-object p1
.end method
