.class public Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;
.super Landroid/widget/LinearLayout;
.source "PrinterStationsListView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;
    }
.end annotation


# instance fields
.field private adapter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private listView:Landroid/widget/ListView;

.field presenter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const-class p2, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Component;->inject(Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 68
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public initialize()V
    .locals 2

    .line 59
    new-instance v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;-><init>(Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$1;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->adapter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->adapter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$PrinterStationsListView(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->presenter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;

    invoke-virtual {p1, p3}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->onPrinterStationRowClicked(I)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->presenter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->dropView(Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;)V

    .line 55
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 46
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 47
    sget v0, Lcom/squareup/settingsapplet/R$id;->printer_stations_list:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->listView:Landroid/widget/ListView;

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->listView:Landroid/widget/ListView;

    new-instance v1, Lcom/squareup/ui/settings/printerstations/-$$Lambda$PrinterStationsListView$30xAy1rgaiKpB7r6_OAMZ3KKrRo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/printerstations/-$$Lambda$PrinterStationsListView$30xAy1rgaiKpB7r6_OAMZ3KKrRo;-><init>(Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->presenter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public refresh()V
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->adapter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->notifyDataSetChanged()V

    return-void
.end method
