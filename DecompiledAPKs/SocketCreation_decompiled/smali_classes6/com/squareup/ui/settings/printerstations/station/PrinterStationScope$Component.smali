.class public interface abstract Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Component;
.super Ljava/lang/Object;
.source "PrinterStationScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract hardwarePrinterSelect()Lcom/squareup/ui/settings/printerstations/station/HardwarePrinterSelectScreen$Component;
.end method

.method public abstract printerStationDetail()Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Component;
.end method

.method public abstract printerStationScopeRunner()Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;
.end method
