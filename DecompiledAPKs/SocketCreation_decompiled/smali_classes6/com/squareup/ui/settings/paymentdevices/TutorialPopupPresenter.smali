.class public abstract Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;
.super Lcom/squareup/mortar/PopupPresenter;
.source "TutorialPopupPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$Prompt;",
        "Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract onButtonTap(Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;)V
.end method

.method protected onPopupResult(Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;)V
    .locals 0

    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopupPresenter;->onPopupResult(Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;)V

    return-void
.end method
