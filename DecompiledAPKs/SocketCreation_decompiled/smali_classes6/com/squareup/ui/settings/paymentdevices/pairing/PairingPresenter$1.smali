.class Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$1;
.super Ljava/lang/Object;
.source "PairingPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->access$000(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/cardreader/WirelessSearcher;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/cardreader/WirelessSearcher;->stopSearch()V

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->access$200(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/util/Clock;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/util/Clock;->getCurrentTimeMillis()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->access$102(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;J)J

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->access$500(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-static {v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->access$300(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-static {v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->access$400(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/WirelessPairingEventListener;->startedPairing(Lcom/squareup/protos/client/bills/CardData$ReaderType;Lcom/squareup/cardreader/WirelessConnection;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->access$300(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_0

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->access$700(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/cardreader/CardReaderFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-static {v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->access$400(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/cardreader/WirelessConnection;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/cardreader/CardReaderFactory;->forBle(Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/cardreader/CardReaderContext;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->access$602(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;Lcom/squareup/cardreader/CardReaderContext;)Lcom/squareup/cardreader/CardReaderContext;

    return-void

    .line 122
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized ReaderTypee: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$1;->this$0:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;

    invoke-static {v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;->access$300(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;)Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
