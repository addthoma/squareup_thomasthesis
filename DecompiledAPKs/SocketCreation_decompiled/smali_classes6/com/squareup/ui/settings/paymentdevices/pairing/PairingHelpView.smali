.class public Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;
.super Landroid/widget/LinearLayout;
.source "PairingHelpView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView$Component;
    }
.end annotation


# instance fields
.field presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const-class p2, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView$Component;->inject(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;)V

    return-void
.end method


# virtual methods
.method getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 58
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 48
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 53
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;->presenter:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpPresenter;->dropView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 26
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 27
    sget v0, Lcom/squareup/cardreader/ui/R$id;->pairing_help_devices:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView$1;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;)V

    .line 28
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    sget v0, Lcom/squareup/cardreader/ui/R$id;->pairing_help_support:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView$2;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;)V

    .line 34
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    sget v0, Lcom/squareup/cardreader/ui/R$id;->pairing_help_video:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView$3;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingHelpView;)V

    .line 40
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
