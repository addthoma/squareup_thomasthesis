.class public Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "BleConnectionStateTransitionEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;
    }
.end annotation


# instance fields
.field public final additionalContext:Ljava/lang/String;

.field public final batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

.field public final bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

.field public final bleRate:Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;

.field public final bluetoothEnabled:Ljava/lang/Boolean;

.field public final bondedDevicesCountExcludingCurrent:Ljava/lang/Integer;

.field public final cardReaderId:Ljava/lang/Integer;

.field public final connectedDevicesCountExcludingCurrent:Ljava/lang/Integer;

.field public final connectionAttemptNumber:Ljava/lang/Integer;

.field public final connectionError:Ljava/lang/String;

.field public final currentBleState:Ljava/lang/String;

.field public final disconnectRequestedBeforeDisconnection:Ljava/lang/Boolean;

.field public final disconnectStatus:Ljava/lang/Integer;

.field public final errorBeforeConnection:Ljava/lang/Boolean;

.field public final firmwareVersion:Ljava/lang/String;

.field public final hardwareSerialNumber:Ljava/lang/String;

.field public final hoursMinutesAndSecondsSinceLastLCRCommunicationString:Ljava/lang/String;

.field public final internetConnection:Lcom/squareup/internet/InternetConnection;

.field public final macAddress:Ljava/lang/String;

.field public final noReconnectReason:Ljava/lang/String;

.field public final previousBleState:Ljava/lang/String;

.field public final readerEventName:Ljava/lang/String;

.field public final readerName:Ljava/lang/String;

.field public final readerState:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

.field public final receivedBleAction:Ljava/lang/String;

.field public final rssiMax:Ljava/lang/Integer;

.field public final rssiMean:Ljava/lang/Double;

.field public final rssiMin:Ljava/lang/Integer;

.field public final rssiSamples:Ljava/lang/String;

.field public final rssiStdDev:Ljava/lang/Double;

.field public final rssiVariance:Ljava/lang/Double;

.field public final secondsSinceLastConnectionAttempt:Ljava/lang/Long;

.field public final secondsSinceLastLCRCommunication:J

.field public final secondsSinceLastSuccessfulConnection:Ljava/lang/Long;

.field public final unlocalizedDescription:Ljava/lang/String;

.field public final willReconnect:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)V
    .locals 2

    .line 57
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->READER:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$000(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    .line 58
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$000(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->readerEventName:Ljava/lang/String;

    .line 59
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$100(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->readerName:Ljava/lang/String;

    .line 60
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$200(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->macAddress:Ljava/lang/String;

    .line 61
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$300(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$300(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    iget v0, v0, Lcom/squareup/cardreader/CardReaderId;->id:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->cardReaderId:Ljava/lang/Integer;

    .line 62
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$400(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->currentBleState:Ljava/lang/String;

    .line 63
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$500(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->previousBleState:Ljava/lang/String;

    .line 64
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$600(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->receivedBleAction:Ljava/lang/String;

    .line 65
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$700(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->unlocalizedDescription:Ljava/lang/String;

    .line 66
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$800(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->rssiMin:Ljava/lang/Integer;

    .line 67
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$900(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->rssiMax:Ljava/lang/Integer;

    .line 68
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$1000(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->rssiMean:Ljava/lang/Double;

    .line 69
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$1100(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->rssiVariance:Ljava/lang/Double;

    .line 70
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$1200(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->rssiStdDev:Ljava/lang/Double;

    .line 71
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$1300(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->rssiSamples:Ljava/lang/String;

    .line 73
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$1400(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->hoursMinutesAndSecondsSinceLastLCRCommunicationString:Ljava/lang/String;

    .line 74
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$1500(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->secondsSinceLastLCRCommunication:J

    .line 75
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$1600(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->secondsSinceLastSuccessfulConnection:Ljava/lang/Long;

    .line 76
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$1700(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->secondsSinceLastConnectionAttempt:Ljava/lang/Long;

    .line 77
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$1800(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->readerState:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    .line 78
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$1900(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Lcom/squareup/cardreader/CardReaderBatteryInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->batteryInfo:Lcom/squareup/cardreader/CardReaderBatteryInfo;

    .line 79
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$2000(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->bleRate:Lcom/squareup/cardreader/CardReaderInfo$BleConnectionRate;

    .line 80
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$2100(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->firmwareVersion:Ljava/lang/String;

    .line 81
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$2200(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->hardwareSerialNumber:Ljava/lang/String;

    .line 82
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$2300(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Lcom/squareup/cardreader/ble/BleConnectType;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->bleConnectType:Lcom/squareup/cardreader/ble/BleConnectType;

    .line 83
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$2400(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->bondedDevicesCountExcludingCurrent:Ljava/lang/Integer;

    .line 84
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$2500(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->connectedDevicesCountExcludingCurrent:Ljava/lang/Integer;

    .line 85
    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->internetConnection:Lcom/squareup/internet/InternetConnection;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->internetConnection:Lcom/squareup/internet/InternetConnection;

    .line 86
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$2600(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->additionalContext:Ljava/lang/String;

    .line 87
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$2700(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->connectionAttemptNumber:Ljava/lang/Integer;

    .line 88
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$2800(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->disconnectStatus:Ljava/lang/Integer;

    .line 89
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$2900(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->connectionError:Ljava/lang/String;

    .line 90
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$3000(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->disconnectRequestedBeforeDisconnection:Ljava/lang/Boolean;

    .line 91
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$3100(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->errorBeforeConnection:Ljava/lang/Boolean;

    .line 92
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$3200(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->bluetoothEnabled:Ljava/lang/Boolean;

    .line 93
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$3300(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->willReconnect:Ljava/lang/Boolean;

    .line 94
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;->access$3400(Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent$Builder;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/BleConnectionStateTransitionEvent;->noReconnectReason:Ljava/lang/String;

    return-void
.end method
