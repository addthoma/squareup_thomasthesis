.class Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;
.super Ljava/lang/Object;
.source "DetailDelegate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;
    }
.end annotation


# static fields
.field static final DEFAULT_VALUE:Ljava/lang/String; = "\u2014"


# instance fields
.field private final cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method private updateBatteryView(Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 1

    .line 226
    sget-object v0, Lcom/squareup/cardreader/BatteryLevel;->UNKNOWN:Lcom/squareup/cardreader/BatteryLevel;

    .line 227
    iget-object p2, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-eqz p2, :cond_0

    .line 229
    invoke-static {p2}, Lcom/squareup/cardreader/BatteryLevel;->forCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/cardreader/BatteryLevel;

    move-result-object v0

    .line 232
    :cond_0
    sget-object p2, Lcom/squareup/cardreader/BatteryLevel;->UNKNOWN:Lcom/squareup/cardreader/BatteryLevel;

    if-ne v0, p2, :cond_1

    const-string p2, "\u2014"

    .line 233
    invoke-interface {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->blankBatteryLevel(Ljava/lang/String;)V

    goto :goto_0

    .line 235
    :cond_1
    invoke-interface {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->setBatteryLevel(Lcom/squareup/cardreader/BatteryLevel;)V

    :goto_0
    return-void
.end method

.method private updateStatusSection(Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 2

    .line 42
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$ui$settings$paymentdevices$ReaderState$Type:[I

    iget-object v1, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 136
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Reader state not handled: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 132
    :pswitch_0
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_unavailable_headline:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_unavailable_advice:I

    .line 133
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 132
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    .line 128
    :pswitch_1
    invoke-interface {p1}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->hideStatusSection()V

    return-void

    .line 124
    :pswitch_2
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_battery_low_headline:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_battery_advice:I

    .line 125
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 124
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    .line 110
    :pswitch_3
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    iget-object p2, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    .line 120
    invoke-interface {p1}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->hideStatusSection()V

    return-void

    .line 116
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_offline_r6_headline:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_offline_r6_advice:I

    .line 117
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    .line 112
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_offline_r12_headline:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_offline_r12_advice:I

    .line 113
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 112
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    .line 105
    :pswitch_4
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_connecting_headline:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_connecting_advice:I

    .line 106
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 105
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    .line 101
    :pswitch_5
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_ss_connecting_headline:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_ss_connecting_advice:I

    .line 102
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 101
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    .line 96
    :pswitch_6
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_connecting_headline:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_connecting_advice:I

    .line 97
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 96
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    .line 91
    :pswitch_7
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_battery_dead_headline:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_battery_advice:I

    .line 92
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 91
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    .line 81
    :pswitch_8
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-eqz v0, :cond_3

    iget-object p2, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 82
    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result p2

    if-eqz p2, :cond_2

    goto :goto_0

    .line 86
    :cond_2
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_failed_to_connect:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_wired_connect_failed_advice:I

    .line 87
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 86
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 83
    :cond_3
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_failed_to_connect:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_wireless_connect_failed_advice:I

    .line 84
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 83
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    .line 77
    :pswitch_9
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_failed_to_connect:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->please_contact_support:I

    .line 78
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 77
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    .line 67
    :pswitch_a
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-eqz v0, :cond_5

    iget-object p2, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 68
    invoke-virtual {p2}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result p2

    if-eqz p2, :cond_4

    goto :goto_2

    .line 72
    :cond_4
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_fwup_failed_headline:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_wired_connect_failed_advice:I

    .line 73
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 69
    :cond_5
    :goto_2
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_fwup_failed_headline:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_wireless_connect_failed_advice:I

    .line 70
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 69
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :goto_3
    return-void

    .line 62
    :pswitch_b
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_tamper_headline:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_tamper_advice:I

    .line 63
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 62
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    .line 58
    :pswitch_c
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_fwup_inprogress_headline:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_fwup_advice:I

    .line 59
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 58
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    .line 53
    :pswitch_d
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_fwup_inprogress_headline:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_fwup_advice:I

    .line 54
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 53
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    .line 49
    :pswitch_e
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_fwup_rebooting_headline:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_fwup_advice:I

    .line 50
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 49
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    .line 44
    :pswitch_f
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_fwup_checking_for_updates_headline:I

    .line 45
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/cardreader/ui/R$string;->reader_status_fwup_checking_for_updates_advice:I

    .line 46
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 44
    invoke-interface {p1, p2, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSection(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method audioConnectionTakingLongTime(Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;Lcom/squareup/protos/client/bills/CardData$ReaderType;)V
    .locals 3

    .line 207
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R6:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v2

    const-string p2, "Can\'t say audio connection is taking a long time when you have an %s"

    invoke-static {v0, p2, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 210
    new-instance p2, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-interface {p1}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_status_connecting_advice_r6_taking_forever:I

    const-string v1, "support_center"

    .line 211
    invoke-virtual {p2, v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    sget v0, Lcom/squareup/cardreader/ui/R$string;->support_center_r6_help_url:I

    .line 212
    invoke-virtual {p2, v0}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    sget v0, Lcom/squareup/cardreader/ui/R$string;->support_center:I

    .line 213
    invoke-virtual {p2, v0}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 214
    invoke-virtual {p2}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p2

    .line 216
    invoke-interface {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showStatusSectionAdvice(Ljava/lang/CharSequence;)V

    return-void
.end method

.method canEditReaderNickname(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Z
    .locals 2

    .line 220
    invoke-virtual {p1}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v1, :cond_1

    iget-object v0, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_READY:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    if-eq v0, v1, :cond_0

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    iget-object p1, p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->level:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;->READER_UPDATING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Level;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method setHelpMessage(Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;Lcom/squareup/protos/client/bills/CardData$ReaderType;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    .line 183
    :cond_0
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$1;->$SwitchMap$com$squareup$protos$client$bills$CardData$ReaderType:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    goto :goto_0

    .line 194
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const-string p2, "There\'s no help text for reader %s"

    .line 195
    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 190
    :cond_2
    sget p2, Lcom/squareup/cardreader/ui/R$string;->card_reader_details_help_message_no_contactless:I

    .line 191
    sget v0, Lcom/squareup/cardreader/ui/R$string;->support_center_r6_help_url:I

    goto :goto_1

    .line 186
    :cond_3
    :goto_0
    sget p2, Lcom/squareup/cardreader/ui/R$string;->card_reader_details_help_message:I

    .line 187
    sget v0, Lcom/squareup/cardreader/ui/R$string;->support_center_r12_help_url:I

    .line 198
    :goto_1
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-interface {p1}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "support_center"

    .line 199
    invoke-virtual {v1, p2, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 200
    invoke-virtual {p2, v0}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    sget v0, Lcom/squareup/cardreader/ui/R$string;->support_center:I

    .line 201
    invoke-virtual {p2, v0}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p2

    .line 202
    invoke-virtual {p2}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p2

    .line 198
    invoke-interface {p1, p2}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->setHelpMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method updateView(Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 4

    .line 141
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->updateStatusSection(Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    .line 143
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderAddress:Ljava/lang/String;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 144
    invoke-interface {p1}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->showWirelessActions()V

    .line 145
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    .line 146
    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    sget-object v3, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 145
    :goto_0
    invoke-interface {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->setIdentifyButtonVisibility(Z)V

    .line 150
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v0

    sget-object v3, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    if-ne v0, v3, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    invoke-interface {p1, v1}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->setNickNameSectionVisible(Z)V

    .line 151
    invoke-virtual {p0, p2}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->canEditReaderNickname(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)Z

    move-result v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->setNickNameEnabled(Z)V

    .line 152
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->nickname:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->setName(Ljava/lang/String;)V

    .line 154
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->updateBatteryView(Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    .line 156
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    const-string v1, "\u2014"

    if-eqz v0, :cond_3

    .line 157
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getPtsFwVersion()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    move-object v0, v1

    .line 159
    :goto_2
    invoke-interface {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->setFirmwareVersion(Ljava/lang/String;)V

    .line 161
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    if-eqz v0, :cond_4

    .line 162
    iget-object v0, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderInfo;->getHardwareSerialNumber()Ljava/lang/String;

    move-result-object v1

    .line 164
    :cond_4
    invoke-interface {p1, v1}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->setSerialNumber(Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    iget-object v1, p2, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->type:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getCardReaderStatusDescription(Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->setStatusValue(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    .line 169
    invoke-virtual {p2}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getCardReaderConnectionType(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 168
    invoke-interface {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->setConnectionValue(Ljava/lang/CharSequence;)V

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->cardReaderMessages:Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;

    invoke-virtual {p2}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getAcceptedPaymentTypes(Lcom/squareup/protos/client/bills/CardData$ReaderType;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;->setAcceptsValue(Ljava/lang/CharSequence;)V

    .line 172
    invoke-virtual {p2}, Lcom/squareup/ui/settings/paymentdevices/ReaderState;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;->setHelpMessage(Lcom/squareup/ui/settings/paymentdevices/DetailDelegate$View;Lcom/squareup/protos/client/bills/CardData$ReaderType;)V

    return-void
.end method
