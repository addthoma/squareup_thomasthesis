.class public Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;
.source "CardReadersSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation


# instance fields
.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V
    .locals 8

    .line 46
    sget v2, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;->TITLE_ID:I

    sget v5, Lcom/squareup/settingsapplet/R$string;->peripheral_connected_one:I

    sget v6, Lcom/squareup/settingsapplet/R$string;->peripheral_connected_many:I

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;Lcom/squareup/util/Device;IILcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V

    .line 48
    iput-object p4, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$ListEntry;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    return-void
.end method


# virtual methods
.method protected connectedCount()I
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$ListEntry;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    invoke-virtual {v0}, Lcom/squareup/cardreader/CardReaderHub;->getCardReaders()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    return v0
.end method
