.class public Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog;
.super Ljava/lang/Object;
.source "EnableBluetoothDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog$EnableBluetoothDialogListener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$show$0(Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog$EnableBluetoothDialogListener;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 14
    invoke-interface {p0}, Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog$EnableBluetoothDialogListener;->cancel()V

    .line 15
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method static synthetic lambda$show$1(Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog$EnableBluetoothDialogListener;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 18
    invoke-interface {p0}, Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog$EnableBluetoothDialogListener;->enableBle()V

    .line 19
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method public static show(Landroid/content/Context;Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog$EnableBluetoothDialogListener;)V
    .locals 2

    .line 10
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p0, Lcom/squareup/cardreader/R$string;->pairing_screen_bluetooth_required_message:I

    .line 11
    invoke-virtual {v0, p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    sget v0, Lcom/squareup/cardreader/R$string;->pairing_screen_bluetooth_required_title:I

    .line 12
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$EnableBluetoothDialog$_DQakl3bTcWOEga17ieMCgwKmFg;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$EnableBluetoothDialog$_DQakl3bTcWOEga17ieMCgwKmFg;-><init>(Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog$EnableBluetoothDialogListener;)V

    .line 13
    invoke-virtual {p0, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    sget v0, Lcom/squareup/cardreader/R$string;->pairing_screen_bluetooth_enable_action:I

    new-instance v1, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$EnableBluetoothDialog$sB8ylXCmokpGk3P3Lxmv2_jNTv0;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$EnableBluetoothDialog$sB8ylXCmokpGk3P3Lxmv2_jNTv0;-><init>(Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog$EnableBluetoothDialogListener;)V

    .line 17
    invoke-virtual {p0, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p0

    .line 21
    invoke-virtual {p0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p0

    const/4 p1, 0x0

    .line 22
    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 23
    invoke-virtual {p0}, Landroid/app/Dialog;->show()V

    return-void
.end method
