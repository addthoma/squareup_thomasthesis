.class public final Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;
.super Lcom/squareup/ui/settings/paymentdevices/pairing/InR12PairingScope;
.source "PairingConfirmationScreen.kt"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$ParentComponent;,
        Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Factory;,
        Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPairingConfirmationScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PairingConfirmationScreen.kt\ncom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,80:1\n24#2,4:81\n*E\n*S KotlinDebug\n*F\n+ 1 PairingConfirmationScreen.kt\ncom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen\n*L\n71#1,4:81\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 \u000e2\u00020\u0001:\u0003\u000e\u000f\u0010B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0014R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;",
        "Lcom/squareup/ui/settings/paymentdevices/pairing/InR12PairingScope;",
        "title",
        "",
        "content",
        "parentKey",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "Companion",
        "Factory",
        "ParentComponent",
        "cardreader-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Companion;


# instance fields
.field private final content:Ljava/lang/String;

.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field private final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;->Companion:Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$Companion;

    .line 81
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 84
    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "title"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "content"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "parentKey"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0, p3}, Lcom/squareup/ui/settings/paymentdevices/pairing/InR12PairingScope;-><init>(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;->content:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method public static final synthetic access$getContent$p(Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;)Ljava/lang/String;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;->content:Ljava/lang/String;

    return-object p0
.end method

.method public static final synthetic access$getTitle$p(Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;)Ljava/lang/String;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;->title:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/settings/paymentdevices/pairing/InR12PairingScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;->content:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
