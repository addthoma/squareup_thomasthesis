.class public Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "ReaderSdkPairingHelpScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardOverSheetScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen$Component;,
        Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen;

    .line 36
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkPairingHelpScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 39
    sget v0, Lcom/squareup/cardreader/ui/R$layout;->pairing_help_view:I

    return v0
.end method
