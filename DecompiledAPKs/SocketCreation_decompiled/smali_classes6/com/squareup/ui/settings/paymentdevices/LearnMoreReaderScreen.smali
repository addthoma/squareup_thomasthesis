.class public final Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "LearnMoreReaderScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/LocksOrientation;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Component;,
        Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 231
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$LearnMoreReaderScreen$TFYPsptVAHXHsbKcxq4WG-n1zlA;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/-$$Lambda$LearnMoreReaderScreen$TFYPsptVAHXHsbKcxq4WG-n1zlA;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/bills/CardData$ReaderType;)V
    .locals 0

    .line 55
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;)Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;
    .locals 1

    .line 232
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    invoke-static {p0}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->fromValue(I)Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p0

    .line 233
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;-><init>(Lcom/squareup/protos/client/bills/CardData$ReaderType;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 228
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    invoke-virtual {p2}, Lcom/squareup/protos/client/bills/CardData$ReaderType;->getValue()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getOrientationForPhone()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 68
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->SENSOR_PORTRAIT:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public getOrientationForTablet()Lcom/squareup/workflow/WorkflowViewFactory$Orientation;
    .locals 1

    .line 72
    sget-object v0, Lcom/squareup/workflow/WorkflowViewFactory$Orientation;->UNLOCKED:Lcom/squareup/workflow/WorkflowViewFactory$Orientation;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 60
    const-class v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 76
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 64
    sget v0, Lcom/squareup/settingsapplet/R$layout;->learn_more_reader_view:I

    return v0
.end method
