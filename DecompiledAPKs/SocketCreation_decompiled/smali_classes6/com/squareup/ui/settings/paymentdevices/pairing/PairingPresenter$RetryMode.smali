.class final enum Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;
.super Ljava/lang/Enum;
.source "PairingPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "RetryMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

.field public static final enum DO_NOT_RETRY:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

.field public static final enum RETRY:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 473
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    const/4 v1, 0x0

    const-string v2, "RETRY"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;->RETRY:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    .line 474
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    const/4 v2, 0x1

    const-string v3, "DO_NOT_RETRY"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;->DO_NOT_RETRY:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    .line 472
    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;->RETRY:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;->DO_NOT_RETRY:Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;->$VALUES:[Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 472
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;
    .locals 1

    .line 472
    const-class v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;
    .locals 1

    .line 472
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;->$VALUES:[Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    invoke-virtual {v0}, [Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$RetryMode;

    return-object v0
.end method
