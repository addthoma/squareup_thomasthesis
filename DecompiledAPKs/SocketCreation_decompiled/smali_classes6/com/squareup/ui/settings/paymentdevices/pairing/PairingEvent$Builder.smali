.class public Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;
.super Ljava/lang/Object;
.source "PairingEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field public connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

.field public error:Ljava/lang/String;

.field public firmwareVersion:Ljava/lang/String;

.field public hardwareSerialNumberLast4:Ljava/lang/String;

.field public lookingToPair:Z

.field public macAddress:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 105
    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->readerEventName(Lcom/squareup/analytics/ReaderEventName;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;

    const/4 v0, 0x0

    .line 106
    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->lookingToPair(Z)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;

    return-void
.end method

.method private static errorToString(Lcom/squareup/dipper/events/BleErrorType;)Ljava/lang/String;
    .locals 3

    .line 166
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$1;->$SwitchMap$com$squareup$dipper$events$BleErrorType:[I

    invoke-virtual {p0}, Lcom/squareup/dipper/events/BleErrorType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 180
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown error type unexpected! "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :pswitch_0
    const-string p0, "Unknown error"

    return-object p0

    :pswitch_1
    const-string p0, "Failed to bond"

    return-object p0

    :pswitch_2
    const-string p0, "Too many reconnection attempts"

    return-object p0

    :pswitch_3
    const-string p0, "Device timeout while trying to connect to reader"

    return-object p0

    :pswitch_4
    const-string p0, "Service version incompatible; Application update required."

    return-object p0

    :pswitch_5
    const-string p0, "Old services cached"

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public build()Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;
    .locals 1

    .line 185
    new-instance v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent;-><init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;)V

    return-object v0
.end method

.method public errorType(Lcom/squareup/dipper/events/BleErrorType;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;
    .locals 0

    if-eqz p1, :cond_0

    .line 160
    invoke-static {p1}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->errorToString(Lcom/squareup/dipper/events/BleErrorType;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->error:Ljava/lang/String;

    :cond_0
    return-object p0
.end method

.method public forCardReaderInfo(Lcom/squareup/cardreader/CardReaderInfo;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;
    .locals 1

    if-eqz p1, :cond_0

    .line 120
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/LastFourDigits;->getLastDigitsAsSerialNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->hardwareSerialNumberLast4:Ljava/lang/String;

    .line 121
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getFirmwareVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->firmwareVersion:Ljava/lang/String;

    .line 122
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->macAddress:Ljava/lang/String;

    .line 123
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getConnectionType()Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    .line 124
    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderInfo;->getReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    :cond_0
    return-object p0
.end method

.method public forWirelessConnection(Lcom/squareup/cardreader/WirelessConnection;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;
    .locals 2

    if-eqz p1, :cond_1

    .line 132
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/LastFourDigits;->getLastDigitsAsSerialNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->hardwareSerialNumberLast4:Ljava/lang/String;

    .line 133
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->macAddress:Ljava/lang/String;

    .line 134
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getType()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 145
    sget-object v0, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->UNKNOWN:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    .line 146
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->UNKNOWN:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iput-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 147
    invoke-interface {p1}, Lcom/squareup/cardreader/WirelessConnection;->getType()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v1

    const-string p1, "Unknown BluetoothDevice type: %s"

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 137
    :cond_0
    sget-object p1, Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;->BLE:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->connectionType:Lcom/squareup/cardreader/CardReaderInfo$ConnectionType;

    .line 138
    sget-object p1, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->readerType:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    :cond_1
    :goto_0
    return-object p0
.end method

.method public lookingToPair(Z)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;
    .locals 0

    .line 154
    iput-boolean p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->lookingToPair:Z

    return-object p0
.end method

.method public readerEventName(Lcom/squareup/analytics/ReaderEventName;)Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;
    .locals 0

    if-eqz p1, :cond_0

    .line 111
    iget-object p1, p1, Lcom/squareup/analytics/ReaderEventName;->value:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingEvent$Builder;->name:Ljava/lang/String;

    :cond_0
    return-object p0
.end method
