.class public Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;
.super Lcom/squareup/applet/AppletSection;
.source "SwipeChipCardsSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$Access;,
        Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$ListEntry;,
        Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$SwipeChipCardsCheckoutEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    sget v0, Lcom/squareup/settingsapplet/R$string;->swipe_chip_cards_title:I

    sput v0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/server/SwipeChipCardsSettings;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/server/SwipeChipCardsSettings;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$Access;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$Access;-><init>(Lcom/squareup/settings/LocalSetting;Lcom/squareup/settings/server/SwipeChipCardsSettings;Lcom/squareup/settings/server/Features;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 29
    sget-object v0, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen;

    return-object v0
.end method
