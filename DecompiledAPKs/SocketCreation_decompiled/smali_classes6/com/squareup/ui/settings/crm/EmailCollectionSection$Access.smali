.class public final Lcom/squareup/ui/settings/crm/EmailCollectionSection$Access;
.super Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;
.source "EmailCollectionSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/crm/EmailCollectionSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Access"
.end annotation


# instance fields
.field private final emailCollectionSettings:Lcom/squareup/crm/EmailCollectionSettings;

.field private features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/crm/EmailCollectionSettings;Lcom/squareup/settings/server/Features;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/squareup/permissions/Permission;

    .line 54
    invoke-direct {p0, p2, v0}, Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;-><init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSection$Access;->emailCollectionSettings:Lcom/squareup/crm/EmailCollectionSettings;

    .line 56
    iput-object p2, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSection$Access;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSection$Access;->emailCollectionSettings:Lcom/squareup/crm/EmailCollectionSettings;

    invoke-interface {v0}, Lcom/squareup/crm/EmailCollectionSettings;->isEmailCollectionAllowed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEVICE_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    .line 61
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
