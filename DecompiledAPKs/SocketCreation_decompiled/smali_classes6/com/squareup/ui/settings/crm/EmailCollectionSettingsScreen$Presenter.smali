.class Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "EmailCollectionSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final controller:Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Controller;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Controller;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 55
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 56
    iput-object p2, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;->controller:Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Controller;

    .line 57
    iput-object p3, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/crm/EmailCollectionSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$0$EmailCollectionSettingsScreen$Presenter(Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;)Lrx/Subscription;
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;->controller:Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Controller;

    invoke-interface {v0}, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Controller;->emailCollectionEnabled()Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/settings/crm/-$$Lambda$g_f9RsVFlp4X6gGBakQhzZbL8-A;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/crm/-$$Lambda$g_f9RsVFlp4X6gGBakQhzZbL8-A;-><init>(Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;)V

    .line 66
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 61
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;

    .line 64
    new-instance v0, Lcom/squareup/ui/settings/crm/-$$Lambda$EmailCollectionSettingsScreen$Presenter$ZjEcBlMOyzSVkEp7bV7baOAt0bY;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/crm/-$$Lambda$EmailCollectionSettingsScreen$Presenter$ZjEcBlMOyzSVkEp7bV7baOAt0bY;-><init>(Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method onToggleClicked()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;->controller:Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Controller;

    invoke-interface {v0}, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Controller;->enableEmailCollectionToggleClicked()V

    return-void
.end method

.method protected saveSettings()V
    .locals 0

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 70
    const-class v0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen;

    return-object v0
.end method
