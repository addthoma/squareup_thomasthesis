.class Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "CustomerManagementSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/crm/CustomerManagementSettings;

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 57
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 58
    iput-object p3, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    .line 59
    iput-object p2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 60
    iput-object p4, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    .line 61
    iput-object p5, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/crm/CustomerManagementSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method onInCartToggled()V
    .locals 0

    .line 132
    invoke-virtual {p0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->saveSettings()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 65
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isBeforeCheckoutEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->setInCartEnabled(Z)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isAllowed()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->setPostTransactionVisibility(Z)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isAfterCheckoutEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->setPostTransactionEnabled(Z)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isCardOnFileFeatureEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->setSaveCardVisibility(Z)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isSaveCardEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->setSaveCardEnabled(Z)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isSaveCardEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->setSaveCardPostTransactionVisibility(Z)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v0}, Lcom/squareup/crm/CustomerManagementSettings;->isSaveCardPostTransactionEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->setSaveCardPostTransactionEnabled(Z)V

    return-void
.end method

.method onPostTransactionToggled()V
    .locals 0

    .line 136
    invoke-virtual {p0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->saveSettings()V

    return-void
.end method

.method onSaveCardPostTransactionToggled()V
    .locals 0

    .line 144
    invoke-virtual {p0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->saveSettings()V

    return-void
.end method

.method onSaveCardToggled()V
    .locals 0

    .line 140
    invoke-virtual {p0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->saveSettings()V

    return-void
.end method

.method protected saveSettings()V
    .locals 5

    .line 85
    invoke-virtual {p0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;

    .line 86
    invoke-virtual {v0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->isInCartEnabled()Z

    move-result v1

    .line 87
    iget-object v2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v2}, Lcom/squareup/crm/CustomerManagementSettings;->isBeforeCheckoutEnabled()Z

    move-result v2

    if-eq v1, v2, :cond_0

    .line 89
    iget-object v2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/analytics/event/v1/ToggleEvent;

    sget-object v4, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_BEFORE_CHECKOUT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v3, v4, v1}, Lcom/squareup/analytics/event/v1/ToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 91
    iget-object v2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/crm/events/CrmToggleEvent;

    sget-object v4, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_BEFORE_CHECKOUT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v3, v4, v1}, Lcom/squareup/crm/events/CrmToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 93
    iget-object v2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v2, v1}, Lcom/squareup/crm/CustomerManagementSettings;->setBeforeCheckoutEnabled(Z)V

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    .line 97
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->isPostTransactionEnabled()Z

    move-result v1

    .line 98
    iget-object v2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v2}, Lcom/squareup/crm/CustomerManagementSettings;->isAfterCheckoutEnabled()Z

    move-result v2

    if-eq v1, v2, :cond_1

    .line 100
    iget-object v2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/analytics/event/v1/ToggleEvent;

    sget-object v4, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_AFTER_CHECKOUT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v3, v4, v1}, Lcom/squareup/analytics/event/v1/ToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 101
    iget-object v2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/crm/events/CrmToggleEvent;

    sget-object v4, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_AFTER_CHECKOUT_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v3, v4, v1}, Lcom/squareup/crm/events/CrmToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 103
    iget-object v2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v2, v1}, Lcom/squareup/crm/CustomerManagementSettings;->setAfterCheckoutEnabled(Z)V

    .line 104
    iget-object v1, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    .line 107
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->isSaveCardEnabled()Z

    move-result v1

    .line 108
    iget-object v2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v2}, Lcom/squareup/crm/CustomerManagementSettings;->isSaveCardEnabled()Z

    move-result v2

    if-eq v1, v2, :cond_2

    .line 110
    iget-object v2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/analytics/event/v1/ToggleEvent;

    sget-object v4, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_CARD_ON_FILE_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v3, v4, v1}, Lcom/squareup/analytics/event/v1/ToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 111
    iget-object v2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v3, Lcom/squareup/crm/events/CrmToggleEvent;

    sget-object v4, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_CARD_ON_FILE_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v3, v4, v1}, Lcom/squareup/crm/events/CrmToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    invoke-interface {v2, v3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 113
    iget-object v2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v2, v1}, Lcom/squareup/crm/CustomerManagementSettings;->setSaveCardEnabled(Z)V

    .line 114
    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->setSaveCardPostTransactionVisibility(Z)V

    .line 115
    iget-object v2, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    :cond_2
    if-eqz v1, :cond_3

    .line 118
    invoke-virtual {v0}, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsView;->isSaveCardPostTransactionEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    .line 119
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v1}, Lcom/squareup/crm/CustomerManagementSettings;->isSaveCardPostTransactionEnabled()Z

    move-result v1

    if-eq v0, v1, :cond_4

    .line 121
    iget-object v1, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/analytics/event/v1/ToggleEvent;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_CARD_ON_FILE_POST_TRANSACTION_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v2, v3, v0}, Lcom/squareup/analytics/event/v1/ToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 123
    iget-object v1, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/crm/events/CrmToggleEvent;

    sget-object v3, Lcom/squareup/analytics/RegisterActionName;->CRM_CUSTOMER_MANAGEMENT_CARD_ON_FILE_POST_TRANSACTION_TOGGLE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v2, v3, v0}, Lcom/squareup/crm/events/CrmToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    invoke-interface {v1, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 126
    iget-object v1, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->settings:Lcom/squareup/crm/CustomerManagementSettings;

    invoke-interface {v1, v0}, Lcom/squareup/crm/CustomerManagementSettings;->setSaveCardPostTransactionEnabled(Z)V

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    :cond_4
    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 77
    const-class v0, Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen;

    return-object v0
.end method
