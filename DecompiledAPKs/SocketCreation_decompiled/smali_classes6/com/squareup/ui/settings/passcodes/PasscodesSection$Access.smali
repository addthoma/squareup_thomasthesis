.class public final Lcom/squareup/ui/settings/passcodes/PasscodesSection$Access;
.super Lcom/squareup/applet/SectionAccess;
.source "PasscodesSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/passcodes/PasscodesSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Access"
.end annotation


# instance fields
.field private final acceptablePermissions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation
.end field

.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/permissions/EmployeeManagementModeDecider;)V
    .locals 2

    .line 69
    invoke-direct {p0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    .line 70
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 71
    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SETTINGS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_DEVICE_SECURITY:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 74
    :cond_0
    sget-object v1, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 76
    :goto_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSection$Access;->acceptablePermissions:Ljava/util/Set;

    .line 77
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSection$Access;->features:Lcom/squareup/settings/server/Features;

    .line 78
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSection$Access;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 79
    iput-object p3, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSection$Access;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSection$Access;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/MerchantRegisterSettings;->useTeamPermissions()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSection$Access;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    .line 88
    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->EMPLOYEE_LOGIN:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->EMPLOYEE_MANAGEMENT:Lcom/squareup/settings/server/Features$Feature;

    .line 89
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEVICE_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    .line 90
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSection$Access;->acceptablePermissions:Ljava/util/Set;

    return-object v0
.end method
