.class public Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;
.super Ljava/lang/Object;
.source "PasscodesSettingsScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;


# static fields
.field private static final ACCOUNT_OWNER_PASSCODE_HASH_ITERATIONS:I = 0x1

.field private static final ACCOUNT_OWNER_PASSCODE_HASH_LENGTH:I = 0x20

.field static final MAX_RESULTS:I = 0x64

.field private static final NUMBER_OF_RETRIES:I = 0x3

.field private static final RANDOM_SALT_GEN:Ljava/security/SecureRandom;

.field private static final RETRY_DELAY:J = 0x2L

.field private static final SENTINEL_END:Ljava/lang/String; = "sentinel_end_value"

.field private static final SENTINEL_START:Ljava/lang/String; = "sentinel_start_value"


# instance fields
.field private final PERMISSION:Lcom/squareup/permissions/Permission;

.field private final accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final device:Lcom/squareup/util/Device;

.field private final employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

.field private final employees:Lcom/squareup/permissions/Employees;

.field private final employeesService:Lcom/squareup/server/employees/EmployeesService;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final settingsAppletEntryPoint:Lcom/squareup/ui/settings/SettingsAppletEntryPoint;

.field private final subs:Lio/reactivex/disposables/CompositeDisposable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 98
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->RANDOM_SALT_GEN:Ljava/security/SecureRandom;

    return-void
.end method

.method constructor <init>(Lcom/squareup/permissions/PasscodesSettings;Lflow/Flow;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/Employees;Lcom/squareup/server/employees/EmployeesService;Lio/reactivex/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/SettingsAppletEntryPoint;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .param p8    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    .line 111
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    .line 112
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    .line 113
    iput-object p3, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 114
    iput-object p4, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    .line 115
    iput-object p5, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 116
    iput-object p6, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->employees:Lcom/squareup/permissions/Employees;

    .line 117
    iput-object p7, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->employeesService:Lcom/squareup/server/employees/EmployeesService;

    .line 118
    iput-object p8, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 119
    iput-object p9, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 120
    iput-object p10, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    .line 121
    iput-object p11, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 122
    iput-object p12, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->device:Lcom/squareup/util/Device;

    .line 123
    iput-object p13, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->settingsAppletEntryPoint:Lcom/squareup/ui/settings/SettingsAppletEntryPoint;

    .line 124
    iput-object p14, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->features:Lcom/squareup/settings/server/Features;

    .line 126
    sget-object p1, Lcom/squareup/settings/server/Features$Feature;->USE_SETTINGS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p14, p1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 127
    sget-object p1, Lcom/squareup/permissions/Permission;->MANAGE_DEVICE_SECURITY:Lcom/squareup/permissions/Permission;

    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->PERMISSION:Lcom/squareup/permissions/Permission;

    goto :goto_0

    .line 129
    :cond_0
    sget-object p1, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->PERMISSION:Lcom/squareup/permissions/Permission;

    :goto_0
    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)Lcom/squareup/permissions/PasscodesSettings;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)Lflow/Flow;
    .locals 0

    .line 72
    iget-object p0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    return-object p0
.end method

.method private getAccountOwnerEmployee()Lio/reactivex/Maybe;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/permissions/Employee;",
            ">;"
        }
    .end annotation

    .line 396
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    const-string v1, "sentinel_start_value"

    .line 399
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$rhp10EBCdKSc30kIGIDZ-9TrnVc;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$rhp10EBCdKSc30kIGIDZ-9TrnVc;

    .line 400
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->takeWhile(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$cX7WvwkTKZq6-Yd0N78VjssO7Tk;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$cX7WvwkTKZq6-Yd0N78VjssO7Tk;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;Lcom/jakewharton/rxrelay2/PublishRelay;)V

    .line 401
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->concatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 420
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorResumeNext(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 421
    invoke-virtual {v0}, Lio/reactivex/Observable;->firstElement()Lio/reactivex/Maybe;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$createOrEditTeamPasscodeScreenData$2(Lcom/squareup/permissions/PasscodesSettings$State;)Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 167
    new-instance v0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscode:Ljava/lang/String;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedTeamPasscode:Ljava/lang/String;

    const/4 v3, 0x4

    iget-object p0, p0, Lcom/squareup/permissions/PasscodesSettings$State;->requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    invoke-direct {v0, v1, v2, v3, p0}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;-><init>(ZLjava/lang/String;ILcom/squareup/permissions/PasscodesSettings$RequestState;)V

    return-object v0
.end method

.method static synthetic lambda$createOrEditTeamPasscodeSuccessScreenData$4(Lcom/squareup/permissions/PasscodesSettings$State;)Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 187
    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings$State;->previousTeamPasscode:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings$State;->requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    sget-object v1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->LOADING:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscode:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 189
    :goto_1
    new-instance v1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;

    iget-object p0, p0, Lcom/squareup/permissions/PasscodesSettings$State;->requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    invoke-direct {v1, v0, p0}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;-><init>(ZLcom/squareup/permissions/PasscodesSettings$RequestState;)V

    return-object v1
.end method

.method static synthetic lambda$createOwnerPasscodeCoordinatorScreenData$3(Lcom/squareup/permissions/PasscodesSettings$State;)Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 173
    new-instance v0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;

    iget-object v1, p0, Lcom/squareup/permissions/PasscodesSettings$State;->requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    iget-object v2, p0, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedOwnerPasscode:Ljava/lang/String;

    iget-object p0, p0, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;-><init>(Lcom/squareup/permissions/PasscodesSettings$RequestState;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method static synthetic lambda$createOwnerPasscodeSuccessScreenData$5(Lcom/squareup/permissions/PasscodesSettings$State;)Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessScreen$Data;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 196
    new-instance v0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessScreen$Data;

    iget-object p0, p0, Lcom/squareup/permissions/PasscodesSettings$State;->requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessScreen$Data;-><init>(Lcom/squareup/permissions/PasscodesSettings$RequestState;)V

    return-object v0
.end method

.method static synthetic lambda$getAccountOwnerEmployee$6(Ljava/lang/String;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "sentinel_end_value"

    .line 400
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method static synthetic lambda$null$7(Ljava/lang/Throwable;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "Server request failed"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p0, v0, v1

    const-string p0, "getEmployees()"

    .line 407
    invoke-static {p0, v0}, Ltimber/log/Timber;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$8(Lcom/jakewharton/rxrelay2/PublishRelay;Lcom/squareup/server/employees/EmployeesResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 411
    invoke-virtual {p1}, Lcom/squareup/server/employees/EmployeesResponse;->getCursor()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/server/employees/EmployeesResponse;->getCursor()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const-string p1, "sentinel_end_value"

    .line 413
    :goto_0
    invoke-virtual {p0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic lambda$null$9(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/permissions/Employee;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 417
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/EmployeesEntity;->populateDefaults()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/permissions/Employee;->fromEmployeesEntity(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/permissions/Employee;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$passcodeSettingsScreenData$0(Lcom/squareup/permissions/PasscodesSettings$State;Lcom/squareup/util/DeviceScreenSizeInfo;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 156
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;

    .line 157
    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;-><init>(Lcom/squareup/permissions/PasscodesSettings$State;Z)V

    return-object v0
.end method

.method static synthetic lambda$timeoutScreenData$1(Lcom/squareup/permissions/PasscodesSettings$State;)Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutScreen$Data;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 162
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutScreen$Data;

    iget-object p0, p0, Lcom/squareup/permissions/PasscodesSettings$State;->passcodeTimeout:Lcom/squareup/permissions/PasscodesSettings$Timeout;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutScreen$Data;-><init>(Lcom/squareup/permissions/PasscodesSettings$Timeout;)V

    return-object v0
.end method

.method private setOwnerPasscode(Ljava/lang/String;)V
    .locals 3

    .line 425
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    .line 426
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->getAccountOwnerEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/permissions/Employee;->token:Ljava/lang/String;

    .line 425
    invoke-static {v0, p1}, Lcom/squareup/server/employees/SetPasscodeRequest;->createSetEmployeePasscodeRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/server/employees/SetPasscodeRequest;

    move-result-object v0

    .line 427
    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->employeesService:Lcom/squareup/server/employees/EmployeesService;

    invoke-interface {v2, v0}, Lcom/squareup/server/employees/EmployeesService;->setPasscode(Lcom/squareup/server/employees/SetPasscodeRequest;)Lrx/Observable;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 428
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$xwog9nzt7YnAhbavJ_x46hCLtpU;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$xwog9nzt7YnAhbavJ_x46hCLtpU;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V

    .line 429
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$6uvuGHPcf40dUcR4KM3gIyyYmWE;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$6uvuGHPcf40dUcR4KM3gIyyYmWE;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;Ljava/lang/String;)V

    new-instance p1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$JqofnVIS0ZTO1OWiTp39Qc3lwdQ;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$JqofnVIS0ZTO1OWiTp39Qc3lwdQ;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V

    .line 430
    invoke-virtual {v0, v2, p1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 427
    invoke-virtual {v1, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    return-void
.end method

.method private updateOwnerEmployeePasscode(Ljava/lang/String;)V
    .locals 4

    const/16 v0, 0x20

    new-array v1, v0, [B

    .line 470
    sget-object v2, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->RANDOM_SALT_GEN:Ljava/security/SecureRandom;

    invoke-virtual {v2, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    const/4 v2, 0x0

    .line 471
    invoke-static {v1, v2}, Lcom/squareup/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    .line 472
    invoke-static {p1, v1, v3, v0}, Lcom/squareup/encryption/Pbkdf2Generator;->derive(Ljava/lang/String;Ljava/lang/String;II)[B

    move-result-object p1

    .line 474
    invoke-static {p1, v2}, Lcom/squareup/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 476
    new-instance v0, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;-><init>()V

    .line 478
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->iterations(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v0

    .line 479
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->salt(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object v0

    .line 480
    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->hashed_passcode(Ljava/lang/String;)Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;

    move-result-object p1

    .line 482
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    .line 483
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->getAccountOwnerEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/permissions/Employee;->toEmployeesEntity()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/server/account/protos/EmployeesEntity;->newBuilder()Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object v0

    .line 484
    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->passcode_only_credential(Lcom/squareup/server/account/protos/EmployeesEntity$PasscodeOnlyCredential;)Lcom/squareup/server/account/protos/EmployeesEntity$Builder;

    move-result-object p1

    .line 485
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/EmployeesEntity$Builder;->build()Lcom/squareup/server/account/protos/EmployeesEntity;

    move-result-object p1

    .line 482
    invoke-static {p1}, Lcom/squareup/permissions/Employee;->fromEmployeesEntity(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/permissions/Employee;

    move-result-object p1

    .line 487
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodesSettings;->setAccountOwnerEmployee(Lcom/squareup/permissions/Employee;)V

    return-void
.end method


# virtual methods
.method public createOrEditTeamPasscodeScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;",
            ">;"
        }
    .end annotation

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$mSIEhopItY4GqVnt4qCwRD4hXaU;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$mSIEhopItY4GqVnt4qCwRD4hXaU;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public createOrEditTeamPasscodeSuccessScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen$Data;",
            ">;"
        }
    .end annotation

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$xyVNiJGsbv2z2qNArR4SOkTE588;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$xyVNiJGsbv2z2qNArR4SOkTE588;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public createOwnerPasscodeCoordinatorScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;",
            ">;"
        }
    .end annotation

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$XbwVlPsj1Dxmy_pihJhqH0gulIU;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$XbwVlPsj1Dxmy_pihJhqH0gulIU;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public createOwnerPasscodeSuccessScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessScreen$Data;",
            ">;"
        }
    .end annotation

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$nQCAyssJkXNqtOXojYGIDmPxVPU;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$nQCAyssJkXNqtOXojYGIDmPxVPU;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$getAccountOwnerEmployee$10$PasscodesSettingsScopeRunner(Lcom/jakewharton/rxrelay2/PublishRelay;Ljava/lang/String;)Lio/reactivex/ObservableSource;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const-string v0, "sentinel_start_value"

    .line 403
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    .line 406
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->employeesService:Lcom/squareup/server/employees/EmployeesService;

    const/16 v1, 0x64

    invoke-interface {v0, p2, v1}, Lcom/squareup/server/employees/EmployeesService;->getEmployees(Ljava/lang/String;I)Lrx/Observable;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p2

    sget-object v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$v4fomizRYNTRWBG3DxHPc4UPR2Y;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$v4fomizRYNTRWBG3DxHPc4UPR2Y;

    .line 407
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->doOnError(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p2

    const/4 v0, 0x3

    const-wide/16 v1, 0x2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v4, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 408
    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/util/rx2/Rx2RetryStrategies;->exponentialBackoffThenError(IJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/functions/Function;

    move-result-object v0

    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->retryWhen(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$_FhNsnDj6FwGEf-EqnBM0H4ZoSI;

    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$_FhNsnDj6FwGEf-EqnBM0H4ZoSI;-><init>(Lcom/jakewharton/rxrelay2/PublishRelay;)V

    .line 410
    invoke-virtual {p2, v0}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 415
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$uZrJV5N_PTS_3KI7iHFfcgwN4pk;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$uZrJV5N_PTS_3KI7iHFfcgwN4pk;

    .line 416
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->flatMapIterable(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$IE7ww1sYvPsUYrLllXe21-qw5os;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$IE7ww1sYvPsUYrLllXe21-qw5os;

    .line 417
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object p2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$j1cRbZ5pBBK6wU7Qqw6BhSmWO48;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$j1cRbZ5pBBK6wU7Qqw6BhSmWO48;

    .line 418
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$setOwnerPasscode$11$PasscodesSettingsScopeRunner(Lio/reactivex/disposables/Disposable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 429
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings;->setLoadingRequestState()V

    return-void
.end method

.method public synthetic lambda$setOwnerPasscode$12$PasscodesSettingsScopeRunner(Ljava/lang/String;Lcom/squareup/server/employees/SetPasscodeResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 431
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->updateOwnerEmployeePasscode(Ljava/lang/String;)V

    .line 433
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->forOwnerPasscodeCreate()Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 436
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    new-instance p2, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    invoke-direct {p2}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;-><init>()V

    const/4 v0, 0x1

    .line 437
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->employee_management_enabled_for_account(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->build()Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object p2

    .line 436
    invoke-interface {p1, p2}, Lcom/squareup/accountstatus/AccountStatusProvider;->maybeUpdatePreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)V

    .line 439
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object p2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    .line 440
    invoke-virtual {p2}, Lcom/squareup/permissions/PasscodesSettings;->getAccountOwnerEmployee()Lcom/squareup/permissions/Employee;

    move-result-object p2

    .line 439
    invoke-virtual {p1, p2}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->setCurrentEmployee(Lcom/squareup/permissions/Employee;)V

    .line 441
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings;->clearUnsavedOwnerPasscode()V

    .line 445
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {p1, v0}, Lcom/squareup/permissions/PasscodesSettings;->enableOrDisablePasscodes(Ljava/lang/Boolean;)V

    .line 447
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    sget-object p2, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessScreen;

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$setOwnerPasscode$13$PasscodesSettingsScopeRunner(Ljava/lang/Throwable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 450
    instance-of v0, p1, Lretrofit/RetrofitError;

    if-eqz v0, :cond_1

    .line 451
    check-cast p1, Lretrofit/RetrofitError;

    .line 452
    invoke-virtual {p1}, Lretrofit/RetrofitError;->getBody()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/employees/SetPasscodeResponse;

    .line 453
    invoke-virtual {p1}, Lcom/squareup/server/employees/SetPasscodeResponse;->duplicatePasscode()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->FAILED_NOT_UNIQUE:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->FAILED_ERROR:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    goto :goto_0

    .line 455
    :cond_1
    sget-object p1, Lcom/squareup/permissions/PasscodesSettings$RequestState;->FAILED_ERROR:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 457
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodesSettings;->setFailedRequest(Lcom/squareup/permissions/PasscodesSettings$RequestState;)V

    return-void
.end method

.method public maybeShowCreateTeamPasscodeDialogScreen()V
    .locals 3

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->getLatestState()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object v0

    .line 201
    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->isAccountOwnerLoggedIn()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscodeEnabled:Z

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscode:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen;

    invoke-direct {v1}, Lcom/squareup/ui/settings/passcodes/CreateTeamPasscodeDialogScreen;-><init>()V

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    :cond_0
    return-void
.end method

.method public onCreateOrEditTeamPasscodeError()V
    .locals 2

    .line 388
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onCreateOrEditTeamPasscodeExit()Lkotlin/Unit;
    .locals 2

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->clearUnsavedTeamPasscode()V

    .line 361
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->settingsAppletEntryPoint:Lcom/squareup/ui/settings/SettingsAppletEntryPoint;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/SettingsAppletEntryPoint;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 362
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public onCreateOrEditTeamPasscodeFinish(Z)Lkotlin/Unit;
    .locals 2

    .line 366
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object v0

    if-nez p1, :cond_0

    .line 367
    invoke-virtual {v0}, Lcom/squareup/settings/server/MerchantRegisterSettings;->showShareTeamPasscodeWarning()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 368
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/passcodes/ShareTeamPasscodeDialogScreen;-><init>()V

    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {p1, v0, v1}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    goto :goto_0

    .line 370
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->settingsAppletEntryPoint:Lcom/squareup/ui/settings/SettingsAppletEntryPoint;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsAppletEntryPoint;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 372
    :goto_0
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public onCreateOwnerPasscodeError()V
    .locals 2

    .line 392
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onCreateOwnerPasscodeExit()Lkotlin/Unit;
    .locals 2

    .line 376
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->clearUnsavedOwnerPasscode()V

    .line 377
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings;->enableOrDisablePasscodes(Ljava/lang/Boolean;)V

    .line 378
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->settingsAppletEntryPoint:Lcom/squareup/ui/settings/SettingsAppletEntryPoint;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/SettingsAppletEntryPoint;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 379
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public onCreateOwnerPasscodeFinish()Lkotlin/Unit;
    .locals 2

    .line 383
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->settingsAppletEntryPoint:Lcom/squareup/ui/settings/SettingsAppletEntryPoint;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/SettingsAppletEntryPoint;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 384
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 135
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/MerchantRegisterSettings;->useTeamPermissions()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 140
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {p1}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->modeSupportsEmployeeCache()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 141
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->employees:Lcom/squareup/permissions/Employees;

    invoke-virtual {p1}, Lcom/squareup/permissions/Employees;->getAccountOwnerEmployee()Lio/reactivex/Maybe;

    move-result-object p1

    goto :goto_0

    .line 143
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->getAccountOwnerEmployee()Lio/reactivex/Maybe;

    move-result-object p1

    .line 145
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$QpQCqWBcbL_-6SwNLf9xy8LqqPE;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$QpQCqWBcbL_-6SwNLf9xy8LqqPE;-><init>(Lcom/squareup/permissions/PasscodesSettings;)V

    .line 146
    invoke-virtual {p1, v2}, Lio/reactivex/Maybe;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    .line 145
    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    :cond_1
    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->subs:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    return-void
.end method

.method public onOwnerPasscodeBackspaceClicked()V
    .locals 1

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->removeOwnerPasscodeDigit()V

    return-void
.end method

.method public onOwnerPasscodeConfirmationBackspaceClicked()V
    .locals 1

    .line 344
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->removeOwnerPasscodeConfirmationDigit()V

    return-void
.end method

.method public onOwnerPasscodeConfirmationDigitEntered(C)V
    .locals 4

    .line 317
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->getLatestState()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object v0

    .line 318
    iget-object v1, v0, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const/4 v2, 0x4

    if-gt v1, v2, :cond_2

    if-ne v1, v2, :cond_1

    .line 323
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v0, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 324
    iget-object v0, v0, Lcom/squareup/permissions/PasscodesSettings$State;->unsavedOwnerPasscode:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->setOwnerPasscode(Ljava/lang/String;)V

    goto :goto_0

    .line 327
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    sget-object v0, Lcom/squareup/permissions/PasscodesSettings$RequestState;->FAILED_PASSCODES_DO_NOT_MATCH:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    invoke-virtual {p1, v0}, Lcom/squareup/permissions/PasscodesSettings;->setFailedRequest(Lcom/squareup/permissions/PasscodesSettings$RequestState;)V

    goto :goto_0

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodesSettings;->addOwnerPasscodeConfirmationDigit(C)V

    :goto_0
    return-void

    .line 320
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AddOwnerPasscodeConfirmationDigitEvent called with passcodeLength "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", maxExpectedLength="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public onOwnerPasscodeDigitEntered(C)V
    .locals 1

    .line 313
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodesSettings;->addOwnerPasscodeDigit(C)V

    return-void
.end method

.method public onPasscodeSettingsSwitchChanged(Ljava/lang/Boolean;)V
    .locals 2

    .line 212
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->forEnablePasscodesToggle(Z)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 213
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 214
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings;->getAccountOwnerEmployee()Lcom/squareup/permissions/Employee;

    move-result-object p1

    if-nez p1, :cond_0

    .line 215
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/PasscodesNotAvailableDialogScreen;

    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {p1, v0, v1}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void

    .line 218
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings;->getAccountOwnerEmployee()Lcom/squareup/permissions/Employee;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/permissions/Employee;->hashed_passcode()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_1

    .line 219
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 221
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$1;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V

    invoke-virtual {p1, v0}, Lcom/squareup/permissions/PermissionGatekeeper;->loginEmployee(Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 228
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$2;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V

    invoke-virtual {p1, v0}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGrantedByOwner(Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminPasscodeUnlockWhen;)V

    :goto_0
    return-void
.end method

.method public onPasscodesSettingsBackClicked()V
    .locals 2

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public onRequirePasscodeAfterEachSaleChanged(Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 2

    .line 286
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->forLockAfterEachSaleChange(Z)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodesSettings;->enableOrDisableRequirePasscodeAfterEachSale(Ljava/lang/Boolean;)V

    .line 288
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public onRequirePasscodeAfterLogoutChanged(Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 2

    .line 302
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->forPasscodeAfterLogoutChange(Z)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 303
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodesSettings;->enableOrDisableRequirePasscodeAfterLogout(Ljava/lang/Boolean;)V

    .line 304
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public onRequirePasscodeWhenBackingOutOfASaleChanged(Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 2

    .line 292
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->forBackingOutOfASaleChange(Z)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodesSettings;->enableOrDisableRequirePasscodeWhenBackingOutOfASale(Ljava/lang/Boolean;)V

    .line 294
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public onSaveTeamPasscode()Lkotlin/Unit;
    .locals 2

    .line 351
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->forTeamPasscodeCreate()Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 353
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->setLoadingRequestState()V

    .line 354
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 355
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->saveTeamPasscode()V

    .line 356
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public onTeamPasscodeBackspaceClicked()V
    .locals 1

    .line 335
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->setSuccessRequestState()V

    .line 336
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->removeTeamPasscodeDigit()V

    return-void
.end method

.method public onTeamPasscodeClicked()V
    .locals 2

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$7;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$7;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGrantedByAccountOwner(Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerPasscodeUnlockWhen;)V

    return-void
.end method

.method public onTeamPasscodeDigitEntered(C)V
    .locals 1

    .line 308
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->setSuccessRequestState()V

    .line 309
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodesSettings;->addTeamPasscodeDigit(C)V

    return-void
.end method

.method public onTeamPasscodeSwitchChanged(Ljava/lang/Boolean;)V
    .locals 2

    .line 237
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->forTeamPasscodeToggle(Z)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 238
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 239
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings;->getLatestState()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscode:Ljava/lang/String;

    if-nez p1, :cond_0

    .line 242
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$3;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$3;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V

    invoke-virtual {p1, v0}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGrantedByAccountOwner(Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerPasscodeUnlockWhen;)V

    goto :goto_0

    .line 249
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->PERMISSION:Lcom/squareup/permissions/Permission;

    new-instance v1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$4;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 255
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_2

    .line 258
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$5;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->PERMISSION:Lcom/squareup/permissions/Permission;

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$5;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;Lcom/squareup/permissions/Permission;)V

    invoke-virtual {p1, v0}, Lcom/squareup/permissions/PermissionGatekeeper;->loginEmployee(Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 264
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->PERMISSION:Lcom/squareup/permissions/Permission;

    new-instance v1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$6;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$6;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    :goto_0
    return-void
.end method

.method public onTimeoutChanged(Lcom/squareup/permissions/PasscodesSettings$Timeout;)V
    .locals 2

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->forTimeoutChange(Lcom/squareup/permissions/PasscodesSettings$Timeout;)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodesSettings;->changeTimeout(Lcom/squareup/permissions/PasscodesSettings$Timeout;)V

    return-void
.end method

.method public onTimeoutClicked()V
    .locals 2

    .line 298
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public passcodeSettingsScreenData()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;",
            ">;"
        }
    .end annotation

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->getScreenSize()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$JoyC4n2S_nnQAQgF09JU9q6yXS4;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$JoyC4n2S_nnQAQgF09JU9q6yXS4;

    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public timeoutScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutScreen$Data;",
            ">;"
        }
    .end annotation

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$WCm_KYm71aINBjOiZdrOmbw3sZg;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$PasscodesSettingsScopeRunner$WCm_KYm71aINBjOiZdrOmbw3sZg;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
