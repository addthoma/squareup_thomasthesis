.class public Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "CreateOwnerPasscodeSuccessCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private continueButton:Lcom/squareup/noho/NohoButton;

.field private final device:Lcom/squareup/util/Device;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private progressBar:Landroid/widget/LinearLayout;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

.field private successContainer:Lcom/squareup/noho/NohoLinearLayout;

.field private successDescription:Lcom/squareup/marketfont/MarketTextView;

.field private view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lio/reactivex/Scheduler;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V
    .locals 0
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 41
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->res:Lcom/squareup/util/Res;

    .line 43
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 44
    iput-object p3, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->device:Lcom/squareup/util/Device;

    .line 45
    iput-object p4, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->view:Landroid/view/View;

    sget v1, Lcom/squareup/settingsapplet/R$id;->create_passcode_success_action_bar:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->view:Landroid/view/View;

    sget v1, Lcom/squareup/settingsapplet/R$id;->create_passcode_success_progress_bar:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->progressBar:Landroid/widget/LinearLayout;

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->view:Landroid/view/View;

    sget v1, Lcom/squareup/settingsapplet/R$id;->create_passcode_success_container:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->successContainer:Lcom/squareup/noho/NohoLinearLayout;

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->view:Landroid/view/View;

    sget v1, Lcom/squareup/settingsapplet/R$id;->create_passcode_success_description:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->successDescription:Lcom/squareup/marketfont/MarketTextView;

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->view:Landroid/view/View;

    sget v1, Lcom/squareup/settingsapplet/R$id;->create_passcode_success_button:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoButton;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->continueButton:Lcom/squareup/noho/NohoButton;

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->continueButton:Lcom/squareup/noho/NohoButton;

    new-instance v1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOwnerPasscodeSuccessCoordinator$xO3iaIBrV472yOy61n2vfGwyNtg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOwnerPasscodeSuccessCoordinator$xO3iaIBrV472yOy61n2vfGwyNtg;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private getLoadingActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;
    .locals 3

    .line 104
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_owner_passcode_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    .line 105
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    return-object v0
.end method

.method private getSuccessActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;
    .locals 4

    .line 111
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/settings/passcodes/-$$Lambda$86A8kKtRUurwP2uvtrdASKsfn_4;

    invoke-direct {v3, v2}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$86A8kKtRUurwP2uvtrdASKsfn_4;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V

    .line 112
    invoke-virtual {v0, v1, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_owner_passcode_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    .line 113
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 115
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic lambda$QCAI4cC4vmLThYtSKKvpuomigMc(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessScreen$Data;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->showScreenData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessScreen$Data;)V

    return-void
.end method

.method static synthetic lambda$showScreenData$2()V
    .locals 0

    return-void
.end method

.method private showScreenData(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessScreen$Data;)V
    .locals 4

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 70
    sget-object v0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator$1;->$SwitchMap$com$squareup$permissions$PasscodesSettings$RequestState:[I

    iget-object p1, p1, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessScreen$Data;->status:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$RequestState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    const/16 v2, 0x8

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 99
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onCreateOwnerPasscodeError()V

    goto :goto_0

    .line 82
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->view:Landroid/view/View;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/settings/passcodes/-$$Lambda$9YMlUgJk82SwssovuA5czhZRyV8;

    invoke-direct {v3, v0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$9YMlUgJk82SwssovuA5czhZRyV8;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V

    invoke-static {p1, v3}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    invoke-direct {p0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->getSuccessActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->progressBar:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->successContainer:Lcom/squareup/noho/NohoLinearLayout;

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->successDescription:Lcom/squareup/marketfont/MarketTextView;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_owner_passcode_success_description:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhone()Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isTabletLessThan10Inches()Z

    move-result p1

    if-eqz p1, :cond_3

    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isLandscape()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->continueButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    goto :goto_0

    .line 93
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->continueButton:Lcom/squareup/noho/NohoButton;

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    goto :goto_0

    .line 74
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->view:Landroid/view/View;

    sget-object v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOwnerPasscodeSuccessCoordinator$784m5xAHZ-loQAriJ7D3ryoKs-s;->INSTANCE:Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOwnerPasscodeSuccessCoordinator$784m5xAHZ-loQAriJ7D3ryoKs-s;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    invoke-direct {p0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->getLoadingActionBarConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->progressBar:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 78
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->successContainer:Lcom/squareup/noho/NohoLinearLayout;

    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->view:Landroid/view/View;

    .line 50
    invoke-direct {p0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->bindViews()V

    .line 53
    new-instance v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOwnerPasscodeSuccessCoordinator$t1GN8yXs5VKQX6099ULC98NZY6s;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOwnerPasscodeSuccessCoordinator$t1GN8yXs5VKQX6099ULC98NZY6s;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$CreateOwnerPasscodeSuccessCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->createOwnerPasscodeSuccessScreenData()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 54
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOwnerPasscodeSuccessCoordinator$QCAI4cC4vmLThYtSKKvpuomigMc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOwnerPasscodeSuccessCoordinator$QCAI4cC4vmLThYtSKKvpuomigMc;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;)V

    .line 55
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$bindViews$1$CreateOwnerPasscodeSuccessCoordinator(Landroid/view/View;)V
    .locals 0

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onCreateOwnerPasscodeFinish()Lkotlin/Unit;

    return-void
.end method
