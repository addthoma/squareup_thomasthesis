.class public final Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;
.super Ljava/lang/Object;
.source "CreateOwnerPasscodeScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# instance fields
.field public final expectedPasscodeLength:I

.field public final requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

.field public final unsavedOwnerPasscode:Ljava/lang/String;

.field public final unsavedOwnerPasscodeConfirmation:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/PasscodesSettings$RequestState;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;->requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    .line 44
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;->unsavedOwnerPasscode:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;->unsavedOwnerPasscodeConfirmation:Ljava/lang/String;

    .line 46
    iput p4, p0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;->expectedPasscodeLength:I

    return-void
.end method
