.class public final Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator_Factory;
.super Ljava/lang/Object;
.source "CreateOrEditTeamPasscodeCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;",
            ">;)",
            "Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lio/reactivex/Scheduler;Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;-><init>(Lcom/squareup/util/Res;Lio/reactivex/Scheduler;Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/reactivex/Scheduler;

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator_Factory;->newInstance(Lcom/squareup/util/Res;Lio/reactivex/Scheduler;Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator_Factory;->get()Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;

    move-result-object v0

    return-object v0
.end method
