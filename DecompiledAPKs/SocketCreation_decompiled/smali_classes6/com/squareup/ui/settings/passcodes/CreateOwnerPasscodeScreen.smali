.class public Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "CreateOwnerPasscodeScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen$Data;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;

    .line 32
    sget-object v0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;->INSTANCE:Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;

    .line 33
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 27
    const-class v0, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    .line 28
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;

    .line 29
    invoke-interface {p1}, Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;->createOwnerPasscodeCoordinator()Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 23
    sget v0, Lcom/squareup/settingsapplet/R$layout;->passcodes_settings_create_or_edit_passcode:I

    return v0
.end method
