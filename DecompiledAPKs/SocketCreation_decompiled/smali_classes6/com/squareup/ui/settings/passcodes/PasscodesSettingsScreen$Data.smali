.class public final Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;
.super Ljava/lang/Object;
.source "PasscodesSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# instance fields
.field final passcodeSettingsState:Lcom/squareup/permissions/PasscodesSettings$State;

.field final showBackButton:Z


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/PasscodesSettings$State;Z)V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;->passcodeSettingsState:Lcom/squareup/permissions/PasscodesSettings$State;

    .line 45
    iput-boolean p2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScreen$Data;->showBackButton:Z

    return-void
.end method
