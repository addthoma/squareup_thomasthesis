.class public Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;
.super Lcom/squareup/eventstream/v1/EventStreamEvent;
.source "PasscodesSettingsEvent.java"


# static fields
.field private static final AFTER_LOGOUT_EVENT:Ljava/lang/String; = "Passcodes: Require Passcode After Log Out: Toggle:"

.field private static final BACK_OUT_OF_A_SALE_EVENT:Ljava/lang/String; = "Passcodes: Require Passcode To Back Out Of Sale: Toggle:"

.field private static final CREATE_OWNER_PASSCODE_EVENT:Ljava/lang/String; = "Passcodes: Permissions: Create Owner Passcode: Created"

.field private static final CREATE_TEAM_PASSCODE_EVENT:Ljava/lang/String; = "Passcodes: Permissions: Create a Team Passcode: Created"

.field private static final EDIT_PERMISSIONS_EVENT:Ljava/lang/String; = "Passcodes: Permissions: Edit Permissions: Sent to Dashboard"

.field private static final ENABLE_PASSCODES_EVENT:Ljava/lang/String; = "Passcodes: Toggle:"

.field private static final ENABLE_TEAM_PASSCODE_EVENT:Ljava/lang/String; = "Passcodes: Allow Team Passcode: Toggle:"

.field private static final EVENT_DETAIL_OFF:Ljava/lang/String; = "OFF"

.field private static final EVENT_DETAIL_ON:Ljava/lang/String; = "ON"

.field private static final EVENT_STREAM_NAME:Lcom/squareup/eventstream/v1/EventStream$Name;

.field private static final LOCK_AFTER_SALE_EVENT:Ljava/lang/String; = "Employee Management: Lock After Sale: Toggle:"

.field private static final TIMEOUT_EVENT:Ljava/lang/String; = "Employee Management: Timeout:"


# instance fields
.field public final detail:Ljava/lang/String;

.field public final version:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sput-object v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->EVENT_STREAM_NAME:Lcom/squareup/eventstream/v1/EventStream$Name;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->EVENT_STREAM_NAME:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    const-string p1, "v3"

    .line 33
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->version:Ljava/lang/String;

    const/4 p1, 0x0

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->detail:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->EVENT_STREAM_NAME:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-direct {p0, v0, p1}, Lcom/squareup/eventstream/v1/EventStreamEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Ljava/lang/String;)V

    const-string p1, "v3"

    .line 33
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->version:Ljava/lang/String;

    .line 42
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->detail:Ljava/lang/String;

    return-void
.end method

.method public static forBackingOutOfASaleChange(Z)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 2

    if-eqz p0, :cond_0

    const-string p0, "ON"

    goto :goto_0

    :cond_0
    const-string p0, "OFF"

    .line 74
    :goto_0
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;

    const-string v1, "Passcodes: Require Passcode To Back Out Of Sale: Toggle:"

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static forEditPermissionsLinkClicked()Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 2

    .line 83
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;

    const-string v1, "Passcodes: Permissions: Edit Permissions: Sent to Dashboard"

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static forEnablePasscodesToggle(Z)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 2

    if-eqz p0, :cond_0

    const-string p0, "ON"

    goto :goto_0

    :cond_0
    const-string p0, "OFF"

    .line 47
    :goto_0
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;

    const-string v1, "Passcodes: Toggle:"

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static forLockAfterEachSaleChange(Z)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 2

    if-eqz p0, :cond_0

    const-string p0, "ON"

    goto :goto_0

    :cond_0
    const-string p0, "OFF"

    .line 69
    :goto_0
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;

    const-string v1, "Employee Management: Lock After Sale: Toggle:"

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static forOwnerPasscodeCreate()Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 2

    .line 56
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;

    const-string v1, "Passcodes: Permissions: Create Owner Passcode: Created"

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static forPasscodeAfterLogoutChange(Z)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 2

    if-eqz p0, :cond_0

    const-string p0, "ON"

    goto :goto_0

    :cond_0
    const-string p0, "OFF"

    .line 79
    :goto_0
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;

    const-string v1, "Passcodes: Require Passcode After Log Out: Toggle:"

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static forTeamPasscodeCreate()Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 2

    .line 60
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;

    const-string v1, "Passcodes: Permissions: Create a Team Passcode: Created"

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static forTeamPasscodeToggle(Z)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 2

    if-eqz p0, :cond_0

    const-string p0, "ON"

    goto :goto_0

    :cond_0
    const-string p0, "OFF"

    .line 52
    :goto_0
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;

    const-string v1, "Passcodes: Allow Team Passcode: Toggle:"

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static forTimeoutChange(Lcom/squareup/permissions/PasscodesSettings$Timeout;)Lcom/squareup/eventstream/v1/EventStreamEvent;
    .locals 2

    .line 64
    new-instance v0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;

    iget-object p0, p0, Lcom/squareup/permissions/PasscodesSettings$Timeout;->eventStreamLoggingDetail:Ljava/lang/String;

    const-string v1, "Employee Management: Timeout:"

    invoke-direct {v0, v1, p0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 88
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 89
    :cond_1
    check-cast p1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;

    .line 90
    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->name:Lcom/squareup/eventstream/v1/EventStream$Name;

    iget-object v3, p1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->name:Lcom/squareup/eventstream/v1/EventStream$Name;

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->value:Ljava/lang/String;

    .line 91
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->detail:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->detail:Ljava/lang/String;

    .line 92
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string p1, "v3"

    .line 93
    invoke-static {p1, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 97
    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->name:Lcom/squareup/eventstream/v1/EventStream$Name;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->value:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsEvent;->detail:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    const/4 v1, 0x3

    const-string v2, "v3"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
