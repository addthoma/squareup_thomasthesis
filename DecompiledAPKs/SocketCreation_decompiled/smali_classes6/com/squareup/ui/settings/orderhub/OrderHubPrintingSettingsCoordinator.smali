.class public final Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderHubPrintingSettingsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubPrintingSettingsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubPrintingSettingsCoordinator.kt\ncom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator\n*L\n1#1,51:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u0010\u001a\u00020\u000c2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;Lcom/squareup/util/Device;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "printingEnabledRow",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "onScreenData",
        "data",
        "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final device:Lcom/squareup/util/Device;

.field private printingEnabledRow:Lcom/squareup/noho/NohoCheckableRow;

.field private final runner:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;Lcom/squareup/util/Device;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;->runner:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;)Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;->runner:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;

    return-object p0
.end method

.method public static final synthetic access$onScreenData(Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;)V
    .locals 0

    .line 16
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;->onScreenData(Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 46
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(com.sq\u2026s.R.id.stable_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 47
    sget v0, Lcom/squareup/settingsapplet/R$id;->orderhub_printing_settings_enabled:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "view.findViewById(R.id.o\u2026rinting_settings_enabled)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    iput-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;->printingEnabledRow:Lcom/squareup/noho/NohoCheckableRow;

    .line 48
    iget-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;->printingEnabledRow:Lcom/squareup/noho/NohoCheckableRow;

    if-nez p1, :cond_0

    const-string v0, "printingEnabledRow"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator$bindViews$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator$bindViews$1;-><init>(Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method private final onScreenData(Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;)V
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;->printingEnabledRow:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_0

    const-string v1, "printingEnabledRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->getPrintingEnabled$settings_applet_release()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;->bindViews(Landroid/view/View;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 26
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 28
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/settingsapplet/R$string;->orderhub_printing_settings_section_label:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 29
    iget-object v2, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v2}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 30
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator$attach$1$1;

    iget-object v4, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;->runner:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;

    invoke-direct {v3, v4}, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator$attach$1$1;-><init>(Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 32
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideUpButton()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 35
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;->runner:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->getScreenData$settings_applet_release()Lrx/Observable;

    move-result-object v0

    .line 37
    new-instance v1, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator$attach$2;-><init>(Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsCoordinator;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "runner.getScreenData()\n \u2026ribe { onScreenData(it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    return-void
.end method
