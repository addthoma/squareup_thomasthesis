.class public final Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "OrderHubSettingsScope.kt"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;,
        Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$Component;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubSettingsScope.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubSettingsScope.kt\ncom/squareup/ui/settings/orderhub/OrderHubSettingsScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,43:1\n35#2:44\n24#3,4:45\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubSettingsScope.kt\ncom/squareup/ui/settings/orderhub/OrderHubSettingsScope\n*L\n24#1:44\n41#1,4:45\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0002\u000b\u000cB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u001c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00058\u0006X\u0087\u0004\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0006\u0010\u0003\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope;",
        "Lcom/squareup/ui/settings/InSettingsAppletScope;",
        "Lcom/squareup/container/RegistersInScope;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "CREATOR$annotations",
        "register",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "Component",
        "ScreenData",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14
    new-instance v0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope;

    invoke-direct {v0}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope;->INSTANCE:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope;

    .line 45
    new-instance v0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 48
    sput-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method

.method public static synthetic CREATOR$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    const-class v0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 24
    check-cast v0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$Component;

    .line 25
    invoke-interface {v0}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$Component;->scopeRunner()Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;

    move-result-object v0

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
