.class public final Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;
.super Ljava/lang/Object;
.source "OrderHubSettingsScopeRunner.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u000c\u0008\u0007\u0018\u00002\u00020\u0001BW\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\u0008\u0001\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000e\u0008\u0001\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005\u0012\u000e\u0008\u0001\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000e\u0008\u0001\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\r\u0010\u0012\u001a\u00020\u0013H\u0000\u00a2\u0006\u0002\u0008\u0014J\u0008\u0010\u0015\u001a\u00020\u0010H\u0002J\u0013\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u0017H\u0000\u00a2\u0006\u0002\u0008\u0018J\u0010\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0008\u0010\u001c\u001a\u00020\u0013H\u0016J\u0015\u0010\u001d\u001a\u00020\u00132\u0006\u0010\u001e\u001a\u00020\u0006H\u0000\u00a2\u0006\u0002\u0008\u001fJ\u0015\u0010 \u001a\u00020\u00132\u0006\u0010\u001e\u001a\u00020\u0008H\u0000\u00a2\u0006\u0002\u0008!J\u0015\u0010\"\u001a\u00020\u00132\u0006\u0010\u001e\u001a\u00020\u0006H\u0000\u00a2\u0006\u0002\u0008#J\u0015\u0010$\u001a\u00020\u00132\u0006\u0010\u001e\u001a\u00020\u0006H\u0000\u00a2\u0006\u0002\u0008%J\u0008\u0010&\u001a\u00020\u0013H\u0002R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u000e\u001a&\u0012\u000c\u0012\n \u0011*\u0004\u0018\u00010\u00100\u0010 \u0011*\u0012\u0012\u000c\u0012\n \u0011*\u0004\u0018\u00010\u00100\u0010\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;",
        "Lmortar/Scoped;",
        "flow",
        "Lflow/Flow;",
        "notificationsEnabled",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "alertsFrequency",
        "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
        "printingEnabled",
        "quickActionsEnabled",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lflow/Flow;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/analytics/Analytics;)V",
        "screenData",
        "Lcom/jakewharton/rxrelay/BehaviorRelay;",
        "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;",
        "kotlin.jvm.PlatformType",
        "exitScreen",
        "",
        "exitScreen$settings_applet_release",
        "getLatestScreenData",
        "getScreenData",
        "Lrx/Observable;",
        "getScreenData$settings_applet_release",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "setNotificationsEnabled",
        "newValue",
        "setNotificationsEnabled$settings_applet_release",
        "setNotificationsFrequency",
        "setNotificationsFrequency$settings_applet_release",
        "setPrintingEnabled",
        "setPrintingEnabled$settings_applet_release",
        "setQuickActionsEnabled",
        "setQuickActionsEnabled$settings_applet_release",
        "updateScreenData",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final alertsFrequency:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
            ">;"
        }
    .end annotation
.end field

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final flow:Lflow/Flow;

.field private final notificationsEnabled:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final printingEnabled:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final quickActionsEnabled:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final screenData:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .param p2    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/orderhub/settings/OrderHubAlertsEnabledPreference;
        .end annotation
    .end param
    .param p3    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/orderhub/settings/OrderHubAlertsFrequencyPreference;
        .end annotation
    .end param
    .param p4    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/orderhub/settings/OrderHubPrintingEnabledPreference;
        .end annotation
    .end param
    .param p5    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/orderhub/settings/OrderHubQuickActionsEnabledPreference;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationsEnabled"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "alertsFrequency"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printingEnabled"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "quickActionsEnabled"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->notificationsEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p3, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->alertsFrequency:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p4, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->printingEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p5, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->quickActionsEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p6, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 30
    invoke-direct {p0}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->getLatestScreenData()Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->screenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method

.method private final getLatestScreenData()Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;
    .locals 6

    .line 38
    new-instance v0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;

    .line 39
    iget-object v1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->notificationsEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v1}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "notificationsEnabled.get()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 40
    iget-object v2, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->alertsFrequency:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v2}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v2

    const-string v3, "alertsFrequency.get()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    .line 41
    iget-object v3, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->printingEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v3}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v3

    const-string v4, "printingEnabled.get()"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 42
    iget-object v4, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->quickActionsEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v4}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v4

    const-string v5, "quickActionsEnabled.get()"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 38
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;-><init>(ZLcom/squareup/orderhub/settings/OrderHubAlertsFrequency;ZZ)V

    return-object v0
.end method

.method private final updateScreenData()V
    .locals 2

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->screenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-direct {p0}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->getLatestScreenData()Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final exitScreen$settings_applet_release()V
    .locals 4

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public final getScreenData$settings_applet_release()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;",
            ">;"
        }
    .end annotation

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->screenData:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, "screenData"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lrx/Observable;

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public final setNotificationsEnabled$settings_applet_release(Z)V
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->notificationsEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/settings/orderhub/OrderNotificationSettingChangedEnabledEvent;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/orderhub/OrderNotificationSettingChangedEnabledEvent;-><init>(Z)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 56
    invoke-direct {p0}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->updateScreenData()V

    return-void
.end method

.method public final setNotificationsFrequency$settings_applet_release(Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;)V
    .locals 4

    const-string v0, "newValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->alertsFrequency:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v0, p1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/settings/orderhub/OrderNotificationSettingChangedFrequencyEvent;

    invoke-virtual {p1}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->getInterval()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/settings/orderhub/OrderNotificationSettingChangedFrequencyEvent;-><init>(J)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 62
    invoke-direct {p0}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->updateScreenData()V

    return-void
.end method

.method public final setPrintingEnabled$settings_applet_release(Z)V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->printingEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/settings/orderhub/OrderPrintingSettingChangedEvent;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/orderhub/OrderPrintingSettingChangedEvent;-><init>(Z)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 68
    invoke-direct {p0}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->updateScreenData()V

    return-void
.end method

.method public final setQuickActionsEnabled$settings_applet_release(Z)V
    .locals 2

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->quickActionsEnabled:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/f2prateek/rx/preferences2/Preference;->set(Ljava/lang/Object;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/settings/orderhub/OrderQuickActionsSettingChangedEvent;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/orderhub/OrderQuickActionsSettingChangedEvent;-><init>(Z)V

    check-cast v1, Lcom/squareup/eventstream/v1/EventStreamEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 74
    invoke-direct {p0}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->updateScreenData()V

    return-void
.end method
