.class public Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "EmployeesUpsellScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Component;,
        Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;,
        Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final FROM_LEARN_MORE:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;

.field public static final FROM_TIME_TRACK_TOGGLE:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;


# instance fields
.field private final source:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 34
    new-instance v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;

    sget-object v1, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->LEARN_MORE_BUTTON:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;)V

    sput-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;->FROM_LEARN_MORE:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;

    .line 36
    new-instance v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;

    sget-object v1, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->TRACK_TIME_TOGGLE:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;)V

    sput-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;->FROM_TIME_TRACK_TOGGLE:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;

    .line 135
    sget-object v0, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeesUpsellScreen$Neg-zVTixPleMQErVjgN1auZkmc;->INSTANCE:Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeesUpsellScreen$Neg-zVTixPleMQErVjgN1auZkmc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;->source:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;)Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;->source:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;
    .locals 3

    .line 136
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->valueOf(Ljava/lang/String;)Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    move-result-object p0

    .line 137
    sget-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$1;->$SwitchMap$com$squareup$ui$settings$empmanagement$EmployeesUpsellScreen$EmployeesUpsellSource:[I

    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 139
    sget-object p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;->FROM_TIME_TRACK_TOGGLE:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;

    return-object p0

    .line 143
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown EmployeesUpsellSource "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->name()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_1
    sget-object p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;->FROM_LEARN_MORE:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;

    return-object p0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 132
    iget-object p2, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;->source:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    invoke-virtual {p2}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 51
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->PAYROLL_UPSELL:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 148
    sget v0, Lcom/squareup/settingsapplet/R$layout;->employees_upsell_screen:I

    return v0
.end method
