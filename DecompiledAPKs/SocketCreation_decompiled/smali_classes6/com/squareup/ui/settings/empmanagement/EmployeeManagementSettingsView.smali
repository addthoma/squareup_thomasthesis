.class public Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;
.super Landroid/widget/LinearLayout;
.source "EmployeeManagementSettingsView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private employeeManagementEnabledToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field features:Lcom/squareup/settings/server/Features;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private guestModeToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private learnMoreSection:Landroid/widget/LinearLayout;

.field private passcodeEmployeeManagementContainer:Landroid/widget/LinearLayout;

.field private passcodeEmployeeManagementOptions:Landroid/widget/LinearLayout;

.field private payrollLearnMore:Lcom/squareup/widgets/list/NameValueRow;

.field presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private timeTrackingToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private timeoutToggle1m:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private timeoutToggle30s:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private timeoutToggle5m:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private timeoutToggleNever:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private transactionLockToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private updatedEmployeeManagementContainer:Landroid/widget/LinearLayout;

.field private updatedPasscodeToggleAlways:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private updatedPasscodeToggleNever:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private updatedPasscodeToggleRestricted:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private updatedTimeoutContainer:Landroid/widget/LinearLayout;

.field private updatedTimeoutToggle1m:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private updatedTimeoutToggle30s:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private updatedTimeoutToggle5m:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private updatedTimeoutToggleNever:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private updatedTransactionLockToggle:Lcom/squareup/widgets/list/ToggleButtonRow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 64
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    const-class p2, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Component;->inject(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 296
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onAttachedToWindow$0$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onTimeTrackingChanged(Z)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$1$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onEmployeeManagementEnabledChanged(Z)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$10$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 160
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    sget-object p2, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->ALWAYS_REQUIRE_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onPasscodeAccessToggleChanged(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$11$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 165
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    sget-object p2, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->NEVER:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onTimeoutChanged(Lcom/squareup/settings/server/PasscodeTimeout;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$12$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 169
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    sget-object p2, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_30S:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onTimeoutChanged(Lcom/squareup/settings/server/PasscodeTimeout;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$13$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 173
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    sget-object p2, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_1M:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onTimeoutChanged(Lcom/squareup/settings/server/PasscodeTimeout;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$14$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 177
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    sget-object p2, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_5M:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onTimeoutChanged(Lcom/squareup/settings/server/PasscodeTimeout;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$15$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 182
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onTransactionLockModeChanged(Z)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$2$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 103
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onGuestModeChanged(Z)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$3$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 107
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    sget-object p2, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->NEVER:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onTimeoutChanged(Lcom/squareup/settings/server/PasscodeTimeout;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$4$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    sget-object p2, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_30S:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onTimeoutChanged(Lcom/squareup/settings/server/PasscodeTimeout;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$5$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    sget-object p2, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_1M:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onTimeoutChanged(Lcom/squareup/settings/server/PasscodeTimeout;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$6$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 119
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    sget-object p2, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_5M:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onTimeoutChanged(Lcom/squareup/settings/server/PasscodeTimeout;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$7$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onTransactionLockModeChanged(Z)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$8$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 150
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    sget-object p2, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->NO_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onPasscodeAccessToggleChanged(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$9$EmployeeManagementSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 155
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    sget-object p2, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->PASSCODE_FOR_RESTRICTED_ACTIONS:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->onPasscodeAccessToggleChanged(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .line 69
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 71
    sget v0, Lcom/squareup/settingsapplet/R$id;->employee_management_track_time_toggle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeTrackingToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeTrackingToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$mRuhkAnINVROT9zh47o14m16C9s;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$mRuhkAnINVROT9zh47o14m16C9s;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 75
    sget v0, Lcom/squareup/settingsapplet/R$id;->employee_management_track_time_toggle_description:I

    .line 76
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 77
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->TIME_TRACKING_DEVICE_LEVEL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 78
    sget v1, Lcom/squareup/settingsapplet/R$string;->employee_management_track_time_toggle_description:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    goto :goto_0

    .line 81
    :cond_0
    sget v1, Lcom/squareup/settingsapplet/R$string;->employee_management_track_time_toggle_description_old:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 85
    :goto_0
    sget v0, Lcom/squareup/settingsapplet/R$id;->employee_management_enabled_toggle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->employeeManagementEnabledToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->employeeManagementEnabledToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$9sCxRtByAngif5rykRhgxfH3JI8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$9sCxRtByAngif5rykRhgxfH3JI8;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 89
    sget v0, Lcom/squareup/settingsapplet/R$id;->employee_management_enabled_toggle_description:I

    .line 90
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 91
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/settingsapplet/R$string;->employee_management_enabled_toggle_description:I

    const-string v3, "dashboard"

    .line 92
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/registerlib/R$string;->employee_management_dashboard_url:I

    .line 93
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/settingsapplet/R$string;->employee_management_enabled_toggle_description_dashboard:I

    .line 94
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 95
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 91
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcode_employee_management_options:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->passcodeEmployeeManagementOptions:Landroid/widget/LinearLayout;

    .line 98
    sget v0, Lcom/squareup/settingsapplet/R$id;->passcode_employee_management_container:I

    .line 99
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->passcodeEmployeeManagementContainer:Landroid/widget/LinearLayout;

    .line 101
    sget v0, Lcom/squareup/settingsapplet/R$id;->employee_management_mode_toggle_guest:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->guestModeToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->guestModeToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$WurIVwq30ja4ssATsDGILwoMt8A;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$WurIVwq30ja4ssATsDGILwoMt8A;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 105
    sget v0, Lcom/squareup/settingsapplet/R$id;->employee_management_timeout_option_toggle_never:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggleNever:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggleNever:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$nnHrZhq8YGypiwQCS2mkwP6JY10;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$nnHrZhq8YGypiwQCS2mkwP6JY10;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 109
    sget v0, Lcom/squareup/settingsapplet/R$id;->employee_management_timeout_option_toggle_30s:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggle30s:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggle30s:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$eR8HzLqZ9mqgRFYBfAbT7wHI5Nw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$eR8HzLqZ9mqgRFYBfAbT7wHI5Nw;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 113
    sget v0, Lcom/squareup/settingsapplet/R$id;->employee_management_timeout_option_toggle_1m:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggle1m:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggle1m:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$85ggnB31iRtGLGSjk_Sjidr2N-k;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$85ggnB31iRtGLGSjk_Sjidr2N-k;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 117
    sget v0, Lcom/squareup/settingsapplet/R$id;->employee_management_timeout_option_toggle_5m:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggle5m:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggle5m:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$ZEt92PvEjEqKYMwO6xT07yySfao;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$ZEt92PvEjEqKYMwO6xT07yySfao;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 121
    sget v0, Lcom/squareup/settingsapplet/R$id;->employee_management_transaction_lock_mode_toggle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->transactionLockToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->transactionLockToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$xs1oEHrCTkujZ0GU9zj0kDZTX1g;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$xs1oEHrCTkujZ0GU9zj0kDZTX1g;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 125
    sget v0, Lcom/squareup/settingsapplet/R$id;->employee_management_learn_more:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->learnMoreSection:Landroid/widget/LinearLayout;

    .line 127
    sget v0, Lcom/squareup/settingsapplet/R$id;->payroll_learn_more:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->payrollLearnMore:Lcom/squareup/widgets/list/NameValueRow;

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->payrollLearnMore:Lcom/squareup/widgets/list/NameValueRow;

    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$91g_fDjArTgC59UXyXs9YXn3TAo;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$91g_fDjArTgC59UXyXs9YXn3TAo;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;)V

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/NameValueRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    sget v0, Lcom/squareup/settingsapplet/R$id;->updated_employee_management_passcode_description:I

    .line 134
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 135
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/settingsapplet/R$string;->employee_management_passcode_description:I

    .line 136
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/registerlib/R$string;->employee_management_dashboard_url:I

    .line 137
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/settingsapplet/R$string;->employee_management_passcode_description_link_text:I

    .line 138
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 139
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 135
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    sget v0, Lcom/squareup/settingsapplet/R$id;->updated_employee_management_container:I

    .line 142
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedEmployeeManagementContainer:Landroid/widget/LinearLayout;

    .line 144
    sget v0, Lcom/squareup/settingsapplet/R$id;->updated_employee_management_timeout_toggle_container:I

    .line 145
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutContainer:Landroid/widget/LinearLayout;

    .line 147
    sget v0, Lcom/squareup/settingsapplet/R$id;->updated_employee_management_passcode_toggle_never:I

    .line 148
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedPasscodeToggleNever:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedPasscodeToggleNever:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$MnJ25G8CxjYOoO4yzXI_PVeScJY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$MnJ25G8CxjYOoO4yzXI_PVeScJY;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 152
    sget v0, Lcom/squareup/settingsapplet/R$id;->updated_employee_management_passcode_toggle_restricted:I

    .line 153
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedPasscodeToggleRestricted:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedPasscodeToggleRestricted:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$Pv24Y9bsdw382atddUeyvWSJF2M;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$Pv24Y9bsdw382atddUeyvWSJF2M;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 157
    sget v0, Lcom/squareup/settingsapplet/R$id;->updated_employee_management_passcode_toggle_always:I

    .line 158
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedPasscodeToggleAlways:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedPasscodeToggleAlways:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$nzbWh3cmNQ3P2ENjH_k7tsvsvPU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$nzbWh3cmNQ3P2ENjH_k7tsvsvPU;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 162
    sget v0, Lcom/squareup/settingsapplet/R$id;->updated_employee_management_timeout_toggle_never:I

    .line 163
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggleNever:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggleNever:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$0kck8-sTNBr3XFvedGjUWd4Iog4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$0kck8-sTNBr3XFvedGjUWd4Iog4;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 167
    sget v0, Lcom/squareup/settingsapplet/R$id;->updated_employee_management_timeout_toggle_30s:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggle30s:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggle30s:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$luh_pdAKSa0NSa4peort0_udQtA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$luh_pdAKSa0NSa4peort0_udQtA;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 171
    sget v0, Lcom/squareup/settingsapplet/R$id;->updated_employee_management_timeout_toggle_1m:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggle1m:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggle1m:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$ocJaO_56s3pkT5YLoyc5P9yQxGA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$ocJaO_56s3pkT5YLoyc5P9yQxGA;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 175
    sget v0, Lcom/squareup/settingsapplet/R$id;->updated_employee_management_timeout_toggle_5m:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggle5m:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggle5m:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$COnF6nMi4ItPNRx18f31d2Xdl0Y;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$COnF6nMi4ItPNRx18f31d2Xdl0Y;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 179
    sget v0, Lcom/squareup/settingsapplet/R$id;->updated_employee_management_transaction_lock_mode_toggle:I

    .line 180
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTransactionLockToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTransactionLockToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$idKD6lFUZuYP2vgGcB6IXyG6FC0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$EmployeeManagementSettingsView$idKD6lFUZuYP2vgGcB6IXyG6FC0;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 291
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 292
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method setGuestModeEnabled(Z)V
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->guestModeToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setLearnMoreVisibility(Z)V
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->learnMoreSection:Landroid/widget/LinearLayout;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setPasscodeEmployeeManagementContainerVisibility(Z)V
    .locals 1

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->passcodeEmployeeManagementContainer:Landroid/widget/LinearLayout;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setPasscodeEmployeeManagementEnabledToggle(Z)V
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->employeeManagementEnabledToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setPasscodeEmployeeManagementSettingsVisibility(Z)V
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->passcodeEmployeeManagementOptions:Landroid/widget/LinearLayout;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setTimeTrackingToggle(Z)V
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeTrackingToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setTimeoutOption(Lcom/squareup/settings/server/PasscodeTimeout;)V
    .locals 5

    .line 201
    instance-of v0, p1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    .line 202
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "Unknown class value %s"

    .line 201
    invoke-static {v0, v3, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 203
    check-cast p1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggleNever:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, v4}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggle30s:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, v4}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggle1m:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, v4}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggle5m:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, v4}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 208
    sget-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$Timeout:[I

    invoke-virtual {p1}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->ordinal()I

    move-result p1

    aget p1, v0, p1

    if-eq p1, v1, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 219
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggle5m:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    goto :goto_0

    .line 216
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggle1m:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    goto :goto_0

    .line 213
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggle30s:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    goto :goto_0

    .line 210
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->timeoutToggleNever:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    :goto_0
    return-void
.end method

.method setTransactionLockToggle(Z)V
    .locals 1

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->transactionLockToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setUpdatedPasscodeEmployeeManagementContainerVisibility(Z)V
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedEmployeeManagementContainer:Landroid/widget/LinearLayout;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setUpdatedPasscodeOption(Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;)V
    .locals 4

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedPasscodeToggleNever:Lcom/squareup/widgets/list/ToggleButtonRow;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->NO_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedPasscodeToggleRestricted:Lcom/squareup/widgets/list/ToggleButtonRow;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->PASSCODE_FOR_RESTRICTED_ACTIONS:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    if-ne p1, v1, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedPasscodeToggleAlways:Lcom/squareup/widgets/list/ToggleButtonRow;

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;->ALWAYS_REQUIRE_PASSCODE:Lcom/squareup/settings/server/EmployeeManagementSettings$PasscodeAccess;

    if-ne p1, v1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v0, v2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setUpdatedTimeoutContainerVisibility(Z)V
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutContainer:Landroid/widget/LinearLayout;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method setUpdatedTimeoutOption(Lcom/squareup/settings/server/PasscodeTimeout;)V
    .locals 5

    .line 230
    instance-of v0, p1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Object;

    .line 231
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-string v3, "Unknown class value %s"

    .line 230
    invoke-static {v0, v3, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 232
    check-cast p1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggleNever:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, v4}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggle30s:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, v4}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggle1m:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, v4}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggle5m:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, v4}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 237
    sget-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$Timeout:[I

    invoke-virtual {p1}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->ordinal()I

    move-result p1

    aget p1, v0, p1

    if-eq p1, v1, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    goto :goto_0

    .line 248
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggle5m:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    goto :goto_0

    .line 245
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggle1m:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    goto :goto_0

    .line 242
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggle30s:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    goto :goto_0

    .line 239
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTimeoutToggleNever:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    :goto_0
    return-void
.end method

.method setUpdatedTransactionLockToggle(Z)V
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTransactionLockToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method

.method setUpdatedTransactionLockToggleVisibilty(Z)V
    .locals 1

    .line 286
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;->updatedTransactionLockToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method
