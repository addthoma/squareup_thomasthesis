.class public final Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;
.super Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;
.source "EmployeeManagementSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Access"
.end annotation


# instance fields
.field private accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/squareup/permissions/Permission;

    .line 60
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;-><init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V

    .line 62
    iput-object p2, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 63
    iput-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;->features:Lcom/squareup/settings/server/Features;

    .line 64
    iput-object p3, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    .line 65
    iput-object p4, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 3

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/MerchantRegisterSettings;->useTeamPermissions()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->canShowEmployeeManagementSettingsSection()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->CAN_SEE_PAYROLL_UPSELL:Lcom/squareup/settings/server/Features$Feature;

    .line 75
    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->DEVICE_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    .line 76
    invoke-interface {v0, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->TIME_TRACKING_DEVICE_LEVEL:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;->employeeManagementModeDecider:Lcom/squareup/permissions/EmployeeManagementModeDecider;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeManagementModeDecider;->getMode()Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    move-result-object v0

    sget-object v1, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->EMPLOYEE_LOGIN:Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;

    .line 83
    invoke-virtual {v0, v1}, Lcom/squareup/permissions/EmployeeManagementModeDecider$Mode;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;->getAcceptablePermissions()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
