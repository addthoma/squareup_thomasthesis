.class public Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection;
.super Lcom/squareup/applet/AppletSection;
.source "EmployeeManagementSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;,
        Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$ListEntry;
    }
.end annotation


# static fields
.field public static TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    sget v0, Lcom/squareup/settingsapplet/R$string;->employee_management_title:I

    sput v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$Access;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagementModeDecider;Lcom/squareup/settings/server/AccountStatusSettings;)V

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen;

    return-object v0
.end method
