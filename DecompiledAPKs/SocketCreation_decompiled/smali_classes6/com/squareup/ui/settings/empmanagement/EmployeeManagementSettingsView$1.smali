.class synthetic Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView$1;
.super Ljava/lang/Object;
.source "EmployeeManagementSettingsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$Timeout:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 208
    invoke-static {}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->values()[Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$Timeout:[I

    :try_start_0
    sget-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$Timeout:[I

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->NEVER:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {v1}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$Timeout:[I

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_30S:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {v1}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$Timeout:[I

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_1M:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {v1}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsView$1;->$SwitchMap$com$squareup$settings$server$EmployeeManagementSettings$Timeout:[I

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_5M:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {v1}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    return-void
.end method
