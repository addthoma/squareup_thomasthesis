.class public Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;
.super Lcom/squareup/marin/widgets/DetailConfirmationView;
.source "EmployeesUpsellView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field presenter:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 21
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/DetailConfirmationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const-class p2, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Component;->inject(Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 60
    invoke-super {p0}, Lcom/squareup/marin/widgets/DetailConfirmationView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 26
    invoke-super {p0}, Lcom/squareup/marin/widgets/DetailConfirmationView;->onAttachedToWindow()V

    .line 28
    new-instance v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView$1;-><init>(Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;->presenter:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 39
    invoke-super {p0}, Lcom/squareup/marin/widgets/DetailConfirmationView;->onDetachedFromWindow()V

    return-void
.end method

.method public setButtonText(Ljava/lang/CharSequence;)V
    .locals 0

    .line 56
    invoke-super {p0, p1}, Lcom/squareup/marin/widgets/DetailConfirmationView;->setButtonText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 0

    .line 43
    invoke-super {p0, p1}, Lcom/squareup/marin/widgets/DetailConfirmationView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    return-void
.end method

.method protected setMessage(I)V
    .locals 2

    .line 51
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/common/bootstrap/R$dimen;->message_new_line_spacing:I

    invoke-static {v0, p1, v1}, Lcom/squareup/text/Fonts;->addSectionBreaks(Landroid/content/res/Resources;II)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-super {p0, p1}, Lcom/squareup/marin/widgets/DetailConfirmationView;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 0

    .line 47
    invoke-super {p0, p1}, Lcom/squareup/marin/widgets/DetailConfirmationView;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method
