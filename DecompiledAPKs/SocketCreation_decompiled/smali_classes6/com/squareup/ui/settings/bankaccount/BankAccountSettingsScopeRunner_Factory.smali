.class public final Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;
.super Ljava/lang/Object;
.source "BankAccountSettingsScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final applicationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final bankAccountSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final bankLinkingStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final shortDateFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->applicationProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->bankLinkingStarterProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p11, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->shortDateFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankLinkingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;)",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;"
        }
    .end annotation

    .line 83
    new-instance v12, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/CountryCode;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/settings/server/Features;Landroid/app/Application;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/analytics/Analytics;Ljava/text/DateFormat;)Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;
    .locals 13

    .line 90
    new-instance v12, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;-><init>(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/CountryCode;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/settings/server/Features;Landroid/app/Application;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/analytics/Analytics;Ljava/text/DateFormat;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;
    .locals 12

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->countryCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/CountryCode;

    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/banklinking/BankAccountSettings;

    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->applicationProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->bankLinkingStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/banklinking/BankLinkingStarter;

    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->shortDateFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Ljava/text/DateFormat;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->newInstance(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/CountryCode;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/settings/server/Features;Landroid/app/Application;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/banklinking/BankLinkingStarter;Lcom/squareup/analytics/Analytics;Ljava/text/DateFormat;)Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner_Factory;->get()Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScopeRunner;

    move-result-object v0

    return-object v0
.end method
