.class public final Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;
.super Ljava/lang/Object;
.source "FormattedBankAccount.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFormattedBankAccount.kt\nKotlin\n*S Kotlin\n*F\n+ 1 FormattedBankAccount.kt\ncom/squareup/ui/settings/bankaccount/FormattedBankAccount\n*L\n1#1,184:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\r\n\u0002\u0008\u0015\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ0\u0010)\u001a\u00020\u00162\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010*\u001a\u00020\u00102\u0006\u0010+\u001a\u00020,2\u0006\u0010\n\u001a\u00020\u000bH\u0002J\u001a\u0010\u0019\u001a\u00020\u00162\u0006\u0010-\u001a\u00020.2\u0008\u0010/\u001a\u0004\u0018\u000100H\u0002J\u0018\u0010\'\u001a\u00020\u00162\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010-\u001a\u00020.H\u0002J\u000c\u0010\u001d\u001a\u00020\u0016*\u00020\u0007H\u0002J\u000c\u0010#\u001a\u00020\u0016*\u00020\u0007H\u0002R\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0013\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0012R\u0011\u0010\u0015\u001a\u00020\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0019\u001a\u00020\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0018R\u0011\u0010\u001b\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0012R\u0011\u0010\u001d\u001a\u00020\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u0018R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010 R\u0011\u0010!\u001a\u00020\u0010\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\"\u0010\u0012R\u0011\u0010#\u001a\u00020\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008$\u0010\u0018R\u0011\u0010%\u001a\u00020\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008&\u0010\u0018R\u0011\u0010\'\u001a\u00020\u0016\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008(\u0010\u0018\u00a8\u00061"
    }
    d2 = {
        "Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "dateFormatter",
        "Ljava/text/DateFormat;",
        "countryCode",
        "Lcom/squareup/CountryCode;",
        "bankAccount",
        "Lcom/squareup/protos/client/bankaccount/BankAccount;",
        "showAccountType",
        "",
        "titleId",
        "",
        "(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/CountryCode;Lcom/squareup/protos/client/bankaccount/BankAccount;ZI)V",
        "accountHolderName",
        "",
        "getAccountHolderName",
        "()Ljava/lang/String;",
        "accountNumber",
        "getAccountNumber",
        "accountType",
        "",
        "getAccountType",
        "()Ljava/lang/CharSequence;",
        "estimatedCompletionDate",
        "getEstimatedCompletionDate",
        "primaryInstitutionNumber",
        "getPrimaryInstitutionNumber",
        "primaryInstitutionNumberName",
        "getPrimaryInstitutionNumberName",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "secondaryInstitutionNumber",
        "getSecondaryInstitutionNumber",
        "secondaryInstitutionNumberName",
        "getSecondaryInstitutionNumberName",
        "title",
        "getTitle",
        "verificationStatus",
        "getVerificationStatus",
        "accountText",
        "bankName",
        "bankAccountType",
        "Lcom/squareup/protos/client/bankaccount/BankAccountType;",
        "bankAccountVerificationState",
        "Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;",
        "verifiedBy",
        "Lcom/squareup/protos/common/time/DateTime;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountHolderName:Ljava/lang/String;

.field private final accountNumber:Ljava/lang/String;

.field private final accountType:Ljava/lang/CharSequence;

.field private final dateFormatter:Ljava/text/DateFormat;

.field private final estimatedCompletionDate:Ljava/lang/CharSequence;

.field private final primaryInstitutionNumber:Ljava/lang/String;

.field private final primaryInstitutionNumberName:Ljava/lang/CharSequence;

.field private final res:Lcom/squareup/util/Res;

.field private final secondaryInstitutionNumber:Ljava/lang/String;

.field private final secondaryInstitutionNumberName:Ljava/lang/CharSequence;

.field private final title:Ljava/lang/CharSequence;

.field private final verificationStatus:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Ljava/text/DateFormat;Lcom/squareup/CountryCode;Lcom/squareup/protos/client/bankaccount/BankAccount;ZI)V
    .locals 6

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "countryCode"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bankAccount"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->dateFormatter:Ljava/text/DateFormat;

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->res:Lcom/squareup/util/Res;

    invoke-interface {p1, p6}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->title:Ljava/lang/CharSequence;

    .line 56
    iget-object v2, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->res:Lcom/squareup/util/Res;

    iget-object v3, p4, Lcom/squareup/protos/client/bankaccount/BankAccount;->bank_name:Ljava/lang/String;

    const-string p1, "bankAccount.bank_name"

    invoke-static {v3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, p4, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_type:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    const-string p1, "bankAccount.account_type"

    invoke-static {v4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p3

    move v5, p5

    .line 55
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->accountText(Lcom/squareup/CountryCode;Lcom/squareup/util/Res;Ljava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountType;Z)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->accountType:Ljava/lang/CharSequence;

    .line 58
    iget-object p1, p4, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_name:Ljava/lang/String;

    const-string p2, "bankAccount.account_name"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->accountHolderName:Ljava/lang/String;

    .line 59
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p2, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->res:Lcom/squareup/util/Res;

    sget p5, Lcom/squareup/registerlib/R$string;->masked_numbers:I

    invoke-interface {p2, p5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    iget-object p2, p4, Lcom/squareup/protos/client/bankaccount/BankAccount;->account_number_suffix:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->accountNumber:Ljava/lang/String;

    .line 61
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->res:Lcom/squareup/util/Res;

    iget-object p2, p4, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    const-string p5, "bankAccount.verification_state"

    invoke-static {p2, p5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->verificationStatus(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->verificationStatus:Ljava/lang/CharSequence;

    .line 62
    invoke-direct {p0, p3}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->primaryInstitutionNumberName(Lcom/squareup/CountryCode;)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->primaryInstitutionNumberName:Ljava/lang/CharSequence;

    .line 63
    iget-object p1, p4, Lcom/squareup/protos/client/bankaccount/BankAccount;->primary_institution_number:Ljava/lang/String;

    const-string p2, "bankAccount.primary_institution_number"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->primaryInstitutionNumber:Ljava/lang/String;

    .line 64
    invoke-direct {p0, p3}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->secondaryInstitutionNumberName(Lcom/squareup/CountryCode;)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->secondaryInstitutionNumberName:Ljava/lang/CharSequence;

    .line 65
    iget-object p1, p4, Lcom/squareup/protos/client/bankaccount/BankAccount;->secondary_institution_number:Ljava/lang/String;

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const-string p1, ""

    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->secondaryInstitutionNumber:Ljava/lang/String;

    .line 67
    iget-object p1, p4, Lcom/squareup/protos/client/bankaccount/BankAccount;->verification_state:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    invoke-static {p1, p5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p2, p4, Lcom/squareup/protos/client/bankaccount/BankAccount;->verified_by:Lcom/squareup/protos/common/time/DateTime;

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->estimatedCompletionDate(Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/CharSequence;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->estimatedCompletionDate:Ljava/lang/CharSequence;

    return-void
.end method

.method private final accountText(Lcom/squareup/CountryCode;Lcom/squareup/util/Res;Ljava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountType;Z)Ljava/lang/CharSequence;
    .locals 0

    if-eqz p5, :cond_5

    .line 83
    sget-object p5, Lcom/squareup/CountryCode;->AU:Lcom/squareup/CountryCode;

    if-eq p1, p5, :cond_5

    sget-object p5, Lcom/squareup/CountryCode;->GB:Lcom/squareup/CountryCode;

    if-ne p1, p5, :cond_0

    goto :goto_2

    .line 87
    :cond_0
    sget-object p1, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p4}, Lcom/squareup/protos/client/bankaccount/BankAccountType;->ordinal()I

    move-result p4

    aget p1, p1, p4

    const/4 p4, 0x1

    if-eq p1, p4, :cond_3

    const/4 p4, 0x2

    if-eq p1, p4, :cond_2

    const/4 p4, 0x3

    if-eq p1, p4, :cond_1

    const/4 p1, 0x0

    goto :goto_0

    .line 90
    :cond_1
    sget p1, Lcom/squareup/banklinking/R$string;->savings:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 89
    :cond_2
    sget p1, Lcom/squareup/banklinking/R$string;->business_checking:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 88
    :cond_3
    sget p1, Lcom/squareup/banklinking/R$string;->checking:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    if-nez p1, :cond_4

    .line 95
    check-cast p3, Ljava/lang/CharSequence;

    goto :goto_1

    .line 97
    :cond_4
    sget p4, Lcom/squareup/settingsapplet/R$string;->bank_name_and_account_type:I

    invoke-interface {p2, p4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 98
    check-cast p3, Ljava/lang/CharSequence;

    const-string p4, "bank_name"

    invoke-virtual {p2, p4, p3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 99
    check-cast p1, Ljava/lang/CharSequence;

    const-string p3, "account_type"

    invoke-virtual {p2, p3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 100
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p3

    const-string p1, "res.phrase(R.string.bank\u2026Type)\n          .format()"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_1
    return-object p3

    .line 84
    :cond_5
    :goto_2
    check-cast p3, Ljava/lang/CharSequence;

    return-object p3
.end method

.method private final estimatedCompletionDate(Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/CharSequence;
    .locals 2

    .line 154
    sget-object v0, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->VERIFICATION_IN_PROGRESS:Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;

    if-ne p1, v0, :cond_1

    if-nez p2, :cond_0

    goto :goto_0

    .line 158
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$string;->estimated_completion_date:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->dateFormatter:Ljava/text/DateFormat;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {p2, v1}, Lcom/squareup/util/ProtoTimes;->asDate(Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/util/Date;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string v0, "date"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 160
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    const-string p2, "res.phrase(R.string.esti\u2026, US)))\n        .format()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_1
    :goto_0
    const-string p1, ""

    .line 155
    check-cast p1, Ljava/lang/CharSequence;

    return-object p1
.end method

.method private final primaryInstitutionNumberName(Lcom/squareup/CountryCode;)Ljava/lang/CharSequence;
    .locals 1

    .line 164
    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    .line 170
    sget p1, Lcom/squareup/country/resources/R$string;->routing_number:I

    goto :goto_0

    .line 169
    :cond_0
    sget p1, Lcom/squareup/country/resources/R$string;->sort_code:I

    goto :goto_0

    .line 168
    :cond_1
    sget p1, Lcom/squareup/country/resources/R$string;->bank_code:I

    goto :goto_0

    .line 167
    :cond_2
    sget p1, Lcom/squareup/country/resources/R$string;->bic:I

    goto :goto_0

    .line 166
    :cond_3
    sget p1, Lcom/squareup/country/resources/R$string;->institution_number:I

    goto :goto_0

    .line 165
    :cond_4
    sget p1, Lcom/squareup/country/resources/R$string;->bsb_number:I

    .line 172
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private final secondaryInstitutionNumberName(Lcom/squareup/CountryCode;)Ljava/lang/CharSequence;
    .locals 1

    .line 176
    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {p1}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 179
    sget p1, Lcom/squareup/common/strings/R$string;->empty:I

    goto :goto_0

    .line 178
    :cond_0
    sget p1, Lcom/squareup/country/resources/R$string;->branch_code:I

    goto :goto_0

    .line 177
    :cond_1
    sget p1, Lcom/squareup/country/resources/R$string;->transit_number:I

    .line 181
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method private final verificationStatus(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;)Ljava/lang/CharSequence;
    .locals 2

    .line 111
    sget-object v0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p2}, Lcom/squareup/protos/client/bankaccount/BankAccountVerificationState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, -0x1

    packed-switch v0, :pswitch_data_0

    .line 135
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown verification status "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 135
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 132
    :pswitch_0
    sget p2, Lcom/squareup/common/strings/R$string;->failed:I

    .line 133
    sget v0, Lcom/squareup/marin/R$color;->marin_red:I

    goto :goto_0

    .line 128
    :pswitch_1
    sget p2, Lcom/squareup/settingsapplet/R$string;->verification_in_progress:I

    .line 129
    sget v0, Lcom/squareup/marin/R$color;->marin_orange:I

    goto :goto_0

    .line 124
    :pswitch_2
    sget p2, Lcom/squareup/settingsapplet/R$string;->verification_canceled:I

    .line 125
    sget v0, Lcom/squareup/marin/R$color;->marin_red:I

    goto :goto_0

    .line 121
    :pswitch_3
    sget p2, Lcom/squareup/registerlib/R$string;->verified:I

    const/4 v0, -0x1

    goto :goto_0

    .line 117
    :pswitch_4
    sget p2, Lcom/squareup/settingsapplet/R$string;->awaiting_email_confirmation:I

    .line 118
    sget v0, Lcom/squareup/marin/R$color;->marin_orange:I

    goto :goto_0

    .line 113
    :pswitch_5
    sget p2, Lcom/squareup/settingsapplet/R$string;->awaiting_debit_authorization:I

    .line 114
    sget v0, Lcom/squareup/marin/R$color;->marin_orange:I

    .line 140
    :goto_0
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    if-ne v0, v1, :cond_0

    .line 143
    check-cast p2, Ljava/lang/CharSequence;

    goto :goto_1

    .line 145
    :cond_0
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p1

    invoke-direct {v1, p1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 146
    check-cast p2, Ljava/lang/CharSequence;

    check-cast v1, Landroid/text/style/CharacterStyle;

    invoke-static {p2, v1}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object p1

    move-object p2, p1

    check-cast p2, Ljava/lang/CharSequence;

    :goto_1
    return-object p2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final getAccountHolderName()Ljava/lang/String;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->accountHolderName:Ljava/lang/String;

    return-object v0
.end method

.method public final getAccountNumber()Ljava/lang/String;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->accountNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final getAccountType()Ljava/lang/CharSequence;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->accountType:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getEstimatedCompletionDate()Ljava/lang/CharSequence;
    .locals 1

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->estimatedCompletionDate:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getPrimaryInstitutionNumber()Ljava/lang/String;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->primaryInstitutionNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final getPrimaryInstitutionNumberName()Ljava/lang/CharSequence;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->primaryInstitutionNumberName:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public final getSecondaryInstitutionNumber()Ljava/lang/String;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->secondaryInstitutionNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final getSecondaryInstitutionNumberName()Ljava/lang/CharSequence;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->secondaryInstitutionNumberName:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->title:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getVerificationStatus()Ljava/lang/CharSequence;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/FormattedBankAccount;->verificationStatus:Ljava/lang/CharSequence;

    return-object v0
.end method
