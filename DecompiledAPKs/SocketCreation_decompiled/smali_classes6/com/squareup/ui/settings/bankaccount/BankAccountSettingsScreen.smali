.class public final Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;
.super Lcom/squareup/ui/settings/bankaccount/InBankAccountSettingsScope;
.source "BankAccountSettingsScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$ScreenData;,
        Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBankAccountSettingsScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BankAccountSettingsScreen.kt\ncom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,59:1\n43#2:60\n*E\n*S KotlinDebug\n*F\n+ 1 BankAccountSettingsScreen.kt\ncom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen\n*L\n30#1:60\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 \u001c2\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0002\u001c\u001dB\u0011\u0012\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0014J\u0008\u0010\u0015\u001a\u00020\u0016H\u0016J\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0008\u0010\u001b\u001a\u00020\u0014H\u0016R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0008\u0010\t\"\u0004\u0008\n\u0010\u0007R\u0018\u0010\u000b\u001a\u0006\u0012\u0002\u0008\u00030\u000c8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;",
        "Lcom/squareup/ui/settings/bankaccount/InBankAccountSettingsScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/layer/InSection;",
        "confirmationToken",
        "",
        "(Ljava/lang/String;)V",
        "getConfirmationToken",
        "()Ljava/lang/String;",
        "setConfirmationToken",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "getAnalyticsName",
        "Lcom/squareup/analytics/RegisterViewName;",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "Companion",
        "ScreenData",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$Companion;


# instance fields
.field private confirmationToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;->Companion:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$Companion;

    .line 54
    sget-object v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "fromParcel { parcel ->\n \u2026arcel.readString())\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;->CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/ui/settings/bankaccount/InBankAccountSettingsScope;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;->confirmationToken:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    .line 17
    check-cast p1, Ljava/lang/String;

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    const-string p2, "parcel"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object p2, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;->confirmationToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 25
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_BANK_ACCOUNT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public final getConfirmationToken()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;->confirmationToken:Ljava/lang/String;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 23
    const-class v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    const-class v0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$Component;

    .line 31
    invoke-interface {p1}, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$Component;->bankAccountSettingsCoordinator()Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 27
    sget v0, Lcom/squareup/settingsapplet/R$layout;->bank_account_settings_view:I

    return v0
.end method

.method public final setConfirmationToken(Ljava/lang/String;)V
    .locals 0

    .line 17
    iput-object p1, p0, Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScreen;->confirmationToken:Ljava/lang/String;

    return-void
.end method
