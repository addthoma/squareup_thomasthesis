.class Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;
.super Ljava/lang/Object;
.source "PaymentTypesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EmptyPaymentType"
.end annotation


# instance fields
.field category:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;


# direct methods
.method private constructor <init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)V
    .locals 0

    .line 426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 427
    iput-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;->category:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$1;)V
    .locals 0

    .line 423
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;-><init>(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 431
    instance-of v0, p1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;->category:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    check-cast p1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;

    iget-object p1, p1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$EmptyPaymentType;->category:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
