.class Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "PaymentTypesSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;",
        ">;"
    }
.end annotation


# static fields
.field private static final TENDER_UPDATE_DEBOUNCE_MILLIS:I = 0x7d0


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final computationScheduler:Lrx/Scheduler;

.field private final customerCheckoutSettings:Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final mainScheduler:Lrx/Scheduler;

.field private final refresher:Lcom/squareup/ui/settings/SidebarRefresher;

.field private final res:Lcom/squareup/util/Res;

.field private final scopeRunner:Lcom/squareup/ui/settings/SettingsAppletScopeRunner;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tenderSettingsManager:Lcom/squareup/tenderpayment/TenderSettingsManager;

.field private tenderSettingsUpdate:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;

.field private final tenderUpdates:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/tenderpayment/TenderSettingsManager;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/tenderpayment/InvoiceTenderSetting;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SettingsAppletScopeRunner;Lcom/squareup/ui/settings/SidebarRefresher;Lrx/Scheduler;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;Lcom/squareup/buyercheckout/CustomerCheckoutSettings;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .param p9    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p10    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Computation;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 73
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->tenderUpdates:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 94
    iput-object p2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 95
    iput-object p3, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->tenderSettingsManager:Lcom/squareup/tenderpayment/TenderSettingsManager;

    .line 96
    iput-object p6, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 97
    iput-object p7, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->scopeRunner:Lcom/squareup/ui/settings/SettingsAppletScopeRunner;

    .line 98
    iput-object p8, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->refresher:Lcom/squareup/ui/settings/SidebarRefresher;

    .line 99
    iput-object p9, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->mainScheduler:Lrx/Scheduler;

    .line 100
    iput-object p10, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->computationScheduler:Lrx/Scheduler;

    .line 101
    iput-object p11, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 102
    iput-object p12, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->customerCheckoutSettings:Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

    .line 103
    iput-object p13, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    .line 105
    new-instance p1, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;

    .line 107
    invoke-interface {p4}, Lcom/squareup/crm/CustomerManagementSettings;->isCardOnFileEnabled()Z

    move-result p2

    .line 108
    invoke-interface {p5}, Lcom/squareup/tenderpayment/InvoiceTenderSetting;->canUseInvoiceAsTender()Z

    move-result p4

    .line 106
    invoke-virtual {p3, p2, p4}, Lcom/squareup/tenderpayment/TenderSettingsManager;->getTenderSettings(ZZ)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object p3

    const/4 p4, 0x0

    const/4 p5, 0x0

    const/4 p6, 0x0

    const/4 p7, 0x0

    move-object p2, p1

    invoke-direct/range {p2 .. p7}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Z)V

    .line 105
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->setTenderSettingsUpdate(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;)V

    return-void
.end method

.method static synthetic lambda$tenderUpdates$1(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;)Ljava/lang/Boolean;
    .locals 0

    .line 139
    iget-object p0, p0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->tender:Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private logTenderEnableDisable(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)V
    .locals 5

    .line 160
    iget-object v0, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    if-eqz v0, :cond_0

    .line 161
    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->tender_type:Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;

    invoke-virtual {p1}, Lcom/squareup/protos/client/devicesettings/TenderSettings$TenderType;->name()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 163
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Legacy Tender Type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;->legacy_other_tender_type_id:Ljava/lang/Integer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 167
    :goto_0
    sget-object v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    const-string v4, "SPOS Settings: Payment Types: %s %s"

    if-ne p2, v0, :cond_1

    .line 168
    iget-object p2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p3, Lcom/squareup/analytics/event/ClickEvent;

    new-array v0, v3, [Ljava/lang/Object;

    aput-object p1, v0, v2

    const-string p1, "Disabled"

    aput-object p1, v0, v1

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, p3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    goto :goto_1

    .line 169
    :cond_1
    sget-object p2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;->DISABLED:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    if-ne p3, p2, :cond_2

    .line 170
    iget-object p2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance p3, Lcom/squareup/analytics/event/ClickEvent;

    new-array v0, v3, [Ljava/lang/Object;

    aput-object p1, v0, v2

    const-string p1, "Enabled"

    aput-object p1, v0, v1

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {p2, p3}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    :cond_2
    :goto_1
    return-void
.end method

.method private setTenderSettingsUpdate(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;)V
    .locals 1

    .line 215
    iput-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->tenderSettingsUpdate:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;

    .line 216
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->tenderUpdates:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public atLeastOnePrimaryPaymentType()Z
    .locals 1

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->eligibleForSquareCardProcessing()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 117
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOtherTendersMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->getOtherTenderTypeNameMap()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getTenderSettings()Lcom/squareup/protos/client/devicesettings/TenderSettings;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->tenderSettingsUpdate:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;

    iget-object v0, v0, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;->settings:Lcom/squareup/protos/client/devicesettings/TenderSettings;

    return-object v0
.end method

.method public isCustomerCheckoutSettingsVisible()Z
    .locals 2

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->BUYER_CHECKOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method

.method public isDefaultCustomerCheckoutEnabled()Z
    .locals 1

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->customerCheckoutSettings:Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

    invoke-interface {v0}, Lcom/squareup/buyercheckout/CustomerCheckoutSettings;->isDefaultCustomerCheckoutEnabled()Z

    move-result v0

    return v0
.end method

.method public isSkipCartScreenEnabled()Z
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->customerCheckoutSettings:Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

    invoke-interface {v0}, Lcom/squareup/buyercheckout/CustomerCheckoutSettings;->isSkipCartScreenEnabled()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onEnterScope$0$PaymentTypesSettingsScreen$Presenter(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;)V
    .locals 0

    .line 131
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->saveSettings()V

    return-void
.end method

.method public moveTenderToCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)V
    .locals 7

    .line 145
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->logTenderEnableDisable(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)V

    .line 147
    :try_start_0
    new-instance v6, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;

    .line 148
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->getTenderSettings()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v0

    .line 149
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->atLeastOnePrimaryPaymentType()Z

    move-result v1

    .line 148
    invoke-static {p1, v0, p2, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->moveTenderToCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Z)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v1

    const/4 v5, 0x0

    move-object v0, v6

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Z)V

    .line 147
    invoke-direct {p0, v6}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->setTenderSettingsUpdate(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public moveTenderToPosition(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;)V
    .locals 7

    .line 177
    new-instance v6, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;

    .line 178
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->getTenderSettings()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v0

    invoke-static {p1, v0, p2}, Lcom/squareup/tenderpayment/TenderSettingsManager;->moveTenderToPosition(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;)Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v1

    iget-object v3, p2, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;->cat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    iget-object v4, p3, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;->cat:Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;

    const/4 v5, 0x1

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;-><init>(Lcom/squareup/protos/client/devicesettings/TenderSettings;Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Z)V

    .line 177
    invoke-direct {p0, v6}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->setTenderSettingsUpdate(Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 5

    .line 126
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 128
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->tenderUpdates()Lrx/Observable;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->computationScheduler:Lrx/Scheduler;

    const-wide/16 v3, 0x7d0

    .line 129
    invoke-virtual {v0, v3, v4, v1, v2}, Lrx/Observable;->debounce(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->mainScheduler:Lrx/Scheduler;

    .line 130
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesSettingsScreen$Presenter$ySTKdJUawZB91lY_d2akxWMqd8E;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesSettingsScreen$Presenter$ySTKdJUawZB91lY_d2akxWMqd8E;-><init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;)V

    .line 131
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 128
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method protected saveSettings()V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->tenderSettingsManager:Lcom/squareup/tenderpayment/TenderSettingsManager;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->getTenderSettings()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/tenderpayment/TenderSettingsManager;->setTenderSettings(Lcom/squareup/protos/client/devicesettings/TenderSettings;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->refresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 113
    const-class v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen;

    return-object v0
.end method

.method public setDefaultCustomerCheckoutEnabled(Z)V
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->customerCheckoutSettings:Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

    invoke-interface {v0, p1}, Lcom/squareup/buyercheckout/CustomerCheckoutSettings;->setDefaultCustomerCheckoutEnabled(Z)V

    return-void
.end method

.method public setSkipCartScreenEnabled(Z)V
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->customerCheckoutSettings:Lcom/squareup/buyercheckout/CustomerCheckoutSettings;

    invoke-interface {v0, p1}, Lcom/squareup/buyercheckout/CustomerCheckoutSettings;->setSkipCartScreenEnabled(Z)V

    return-void
.end method

.method public showPreview()V
    .locals 2

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->scopeRunner:Lcom/squareup/ui/settings/SettingsAppletScopeRunner;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->getTenderSettings()Lcom/squareup/protos/client/devicesettings/TenderSettings;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->showPreviewSelectMethodWorkflow(Lcom/squareup/protos/client/devicesettings/TenderSettings;)V

    return-void
.end method

.method public tenderUpdates()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsUpdate;",
            ">;"
        }
    .end annotation

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->tenderUpdates:Lcom/jakewharton/rxrelay/PublishRelay;

    sget-object v1, Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesSettingsScreen$Presenter$DBtaenndx8rcwIUeECvh3Y1a3yE;->INSTANCE:Lcom/squareup/ui/settings/paymenttypes/-$$Lambda$PaymentTypesSettingsScreen$Presenter$DBtaenndx8rcwIUeECvh3Y1a3yE;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/PublishRelay;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
