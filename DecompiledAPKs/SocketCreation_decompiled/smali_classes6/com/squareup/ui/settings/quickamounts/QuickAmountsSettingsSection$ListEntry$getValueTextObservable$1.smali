.class final Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry$getValueTextObservable$1;
.super Ljava/lang/Object;
.source "QuickAmountsSettingsSection.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;->getValueTextObservable()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "amounts",
        "Lcom/squareup/quickamounts/QuickAmounts;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry$getValueTextObservable$1;->this$0:Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 26
    check-cast p1, Lcom/squareup/quickamounts/QuickAmounts;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry$getValueTextObservable$1;->apply(Lcom/squareup/quickamounts/QuickAmounts;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lcom/squareup/quickamounts/QuickAmounts;)Ljava/lang/String;
    .locals 1

    const-string v0, "amounts"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmounts;->getStatus()Lcom/squareup/quickamounts/QuickAmountsStatus;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/quickamounts/QuickAmountsStatus;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_2

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 40
    iget-object p1, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry$getValueTextObservable$1;->this$0:Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;

    invoke-static {p1}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;->access$getRes$p(Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/settingsapplet/R$string;->quick_amounts_status_set:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 39
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry$getValueTextObservable$1;->this$0:Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;

    invoke-static {p1}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;->access$getRes$p(Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/settingsapplet/R$string;->quick_amounts_status_auto:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 38
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry$getValueTextObservable$1;->this$0:Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;

    invoke-static {p1}, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;->access$getRes$p(Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;)Lcom/squareup/util/Res;

    move-result-object p1

    sget v0, Lcom/squareup/settingsapplet/R$string;->quick_amounts_status_off:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method
