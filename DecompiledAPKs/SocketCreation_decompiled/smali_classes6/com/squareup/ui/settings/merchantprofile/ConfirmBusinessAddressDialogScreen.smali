.class public final Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "ConfirmBusinessAddressDialogScreen.kt"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Component;,
        Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;,
        Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Factory;,
        Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\u0008\u0007\u0018\u0000 \r2\u00020\u0001:\u0004\r\u000e\u000f\u0010B\u000f\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0014R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;",
        "Lcom/squareup/ui/settings/InSettingsAppletScope;",
        "data",
        "Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;",
        "(Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;)V",
        "getData",
        "()Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "Companion",
        "Component",
        "Factory",
        "ScreenData",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
            "Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Companion;


# instance fields
.field private final data:Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;->Companion:Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Companion;

    .line 136
    sget-object v0, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "fromParcel { parcel ->\n \u2026sDialogScreen(data)\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;->CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;)V
    .locals 1

    const-string v0, "data"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;->data:Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;->data:Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 132
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/settings/InSettingsAppletScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    return-void
.end method

.method public final getData()Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen;->data:Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$ScreenData;

    return-object v0
.end method
