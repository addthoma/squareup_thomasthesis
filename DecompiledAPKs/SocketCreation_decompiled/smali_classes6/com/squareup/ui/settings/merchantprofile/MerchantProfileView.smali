.class public Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;
.super Landroid/widget/LinearLayout;
.source "MerchantProfileView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/ui/HasActionBar;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$MerchantLogoTarget;,
        Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$FeaturedImageTarget;,
        Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$ContactInfoTextWatcher;,
        Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$DecodeDimensionCallback;
    }
.end annotation


# instance fields
.field private address:Landroid/widget/TextView;

.field private animator:Lcom/squareup/widgets/PersistentViewAnimator;

.field private businessAddressSection:Landroid/view/View;

.field private businessName:Lcom/squareup/ui/XableEditText;

.field private content:Landroid/view/ViewGroup;

.field private description:Landroid/widget/EditText;

.field private editAddressButton:Landroid/view/View;

.field private email:Lcom/squareup/ui/XableEditText;

.field private facebook:Lcom/squareup/ui/XableEditText;

.field private featured:Lcom/squareup/register/widgets/AspectRatioImageView;

.field private featuredImageTarget:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$FeaturedImageTarget;

.field fileThreadExecutor:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/squareup/thread/FileThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private logo:Landroid/widget/ImageView;

.field private logoText:Landroid/widget/TextView;

.field mainThread:Lcom/squareup/thread/executor/MainThread;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final merchantEditPhotoPopup:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;

.field private merchantLogoContainer:Landroid/view/View;

.field private merchantLogoHeight:I

.field private merchantLogoMessage:Landroid/view/View;

.field private merchantLogoTarget:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$MerchantLogoTarget;

.field private merchantLogoWidth:I

.field private minFeaturedImageHeight:I

.field private minFeaturedImageOffset:I

.field private mobileBusiness:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private mobileBusinessHelpText:Landroid/view/View;

.field private phone:Lcom/squareup/ui/XableEditText;

.field phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private photoOnReceipt:Lcom/squareup/widgets/list/ToggleButtonRow;

.field picasso:Lcom/squareup/picasso/Picasso;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private retry:Landroid/widget/Button;

.field thumbor:Lcom/squareup/pollexor/Thumbor;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private twitter:Lcom/squareup/ui/XableEditText;

.field private watcher:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$ContactInfoTextWatcher;

.field private website:Lcom/squareup/ui/XableEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 106
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 107
    const-class p2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileScreen$Component;->inject(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V

    .line 108
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 110
    sget v0, Lcom/squareup/registerlib/R$dimen;->merchant_logo_width:I

    .line 111
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoWidth:I

    .line 112
    sget v0, Lcom/squareup/registerlib/R$dimen;->merchant_logo_height:I

    .line 113
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoHeight:I

    .line 114
    sget v0, Lcom/squareup/registerlib/R$dimen;->merchant_featured_image_min_height:I

    .line 115
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->minFeaturedImageHeight:I

    .line 116
    sget v0, Lcom/squareup/marin/R$dimen;->marin_gap_small:I

    .line 117
    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->minFeaturedImageOffset:I

    .line 119
    new-instance p2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$MerchantLogoTarget;

    const/4 v0, 0x0

    invoke-direct {p2, p0, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$MerchantLogoTarget;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$1;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoTarget:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$MerchantLogoTarget;

    .line 120
    new-instance p2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$FeaturedImageTarget;

    invoke-direct {p2, p0, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$FeaturedImageTarget;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$1;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featuredImageTarget:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$FeaturedImageTarget;

    .line 121
    new-instance p2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$ContactInfoTextWatcher;

    invoke-direct {p2, p0, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$ContactInfoTextWatcher;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$1;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->watcher:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$ContactInfoTextWatcher;

    .line 122
    new-instance p2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;

    invoke-direct {p2, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantEditPhotoPopup:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)Lcom/squareup/widgets/PersistentViewAnimator;
    .locals 0

    .line 64
    iget-object p0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->animator:Lcom/squareup/widgets/PersistentViewAnimator;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V
    .locals 0

    .line 64
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->contactInfoChanged()V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setFeaturedBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$600(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setMerchantLogoBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private static adjustAspectRatio(Lcom/squareup/register/widgets/AspectRatioImageView;IIIF)[I
    .locals 2

    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x1

    if-le p2, p3, :cond_0

    int-to-float p1, p3

    mul-float p1, p1, p4

    float-to-int p1, p1

    .line 511
    invoke-virtual {p0}, Lcom/squareup/register/widgets/AspectRatioImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    check-cast p2, Landroid/widget/LinearLayout$LayoutParams;

    .line 512
    iput p1, p2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 513
    iput p3, p2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 515
    invoke-virtual {p0, p4}, Lcom/squareup/register/widgets/AspectRatioImageView;->setAspectRatio(F)V

    .line 516
    invoke-virtual {p0, v1}, Lcom/squareup/register/widgets/AspectRatioImageView;->setDominantMeasurement(I)V

    move p2, p3

    :cond_0
    const/4 p0, 0x0

    aput p1, v0, p0

    aput p2, v0, v1

    return-object v0
.end method

.method private contactInfoChanged()V
    .locals 8

    .line 436
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->phone:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 437
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->email:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 438
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->facebook:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 439
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->twitter:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 440
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->website:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 441
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    new-instance v7, Lcom/squareup/merchantprofile/ContactInfo;

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/squareup/merchantprofile/ContactInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->contactInfoChanged(Lcom/squareup/merchantprofile/ContactInfo;)V

    return-void
.end method

.method private fitFeaturedImage(IIII)[I
    .locals 5

    .line 526
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featured:Lcom/squareup/register/widgets/AspectRatioImageView;

    .line 528
    invoke-virtual {v0}, Lcom/squareup/register/widgets/AspectRatioImageView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr p1, v1

    invoke-virtual {v0}, Lcom/squareup/register/widgets/AspectRatioImageView;->getPaddingRight()I

    move-result v1

    sub-int/2addr p1, v1

    .line 529
    invoke-virtual {v0}, Lcom/squareup/register/widgets/AspectRatioImageView;->getPaddingTop()I

    move-result v1

    sub-int/2addr p2, v1

    invoke-virtual {v0}, Lcom/squareup/register/widgets/AspectRatioImageView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr p2, v1

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    :cond_0
    move p3, p1

    move p4, p2

    :cond_1
    const/4 v1, 0x2

    new-array v2, v1, [I

    .line 539
    invoke-static {p3, p4, p1, p2, v2}, Lcom/squareup/util/Images;->scaleImageToBounds(IIII[I)V

    const/4 p3, 0x0

    .line 541
    aget p4, v2, p3

    const/4 v3, 0x1

    if-ne p4, p1, :cond_2

    aget p4, v2, v3

    if-eq p4, p2, :cond_3

    .line 542
    :cond_2
    aget p1, v2, p3

    .line 543
    aget p2, v2, v3

    .line 545
    invoke-virtual {v0}, Lcom/squareup/register/widgets/AspectRatioImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p4

    check-cast p4, Landroid/widget/LinearLayout$LayoutParams;

    .line 546
    invoke-virtual {v0}, Lcom/squareup/register/widgets/AspectRatioImageView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v2, p1

    invoke-virtual {v0}, Lcom/squareup/register/widgets/AspectRatioImageView;->getPaddingRight()I

    move-result v4

    add-int/2addr v2, v4

    iput v2, p4, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 547
    invoke-virtual {v0}, Lcom/squareup/register/widgets/AspectRatioImageView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v2, p2

    invoke-virtual {v0}, Lcom/squareup/register/widgets/AspectRatioImageView;->getPaddingTop()I

    move-result v4

    add-int/2addr v2, v4

    iput v2, p4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 549
    invoke-virtual {v0, p3}, Lcom/squareup/register/widgets/AspectRatioImageView;->setAspectRatioEnabled(Z)V

    :cond_3
    new-array p4, v1, [I

    aput p1, p4, p3

    aput p2, p4, v3

    return-object p4
.end method

.method private getMaxHeight()I
    .locals 2

    .line 461
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->getHeight()I

    move-result v0

    .line 462
    iget v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->minFeaturedImageOffset:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->minFeaturedImageHeight:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method static synthetic lambda$null$8(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$DecodeDimensionCallback;[ILjava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    .line 431
    aget v0, p1, v0

    const/4 v1, 0x1

    aget p1, p1, v1

    invoke-interface {p0, v0, p1, p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$DecodeDimensionCallback;->onDimensionsDecoded(IILjava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$onFinishInflate$2(Landroid/content/Context;Landroid/view/View;Z)V
    .locals 0

    .line 182
    check-cast p1, Landroid/widget/TextView;

    if-eqz p2, :cond_0

    .line 184
    sget p2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_contact_phone_selected:I

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 186
    :cond_0
    sget p2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_contact_phone:I

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method static synthetic lambda$onFinishInflate$3(Landroid/content/Context;Landroid/view/View;Z)V
    .locals 0

    .line 193
    check-cast p1, Landroid/widget/TextView;

    if-eqz p2, :cond_0

    .line 195
    sget p2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_contact_email_selected:I

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 197
    :cond_0
    sget p2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_contact_email:I

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method static synthetic lambda$onFinishInflate$5(Landroid/content/Context;Landroid/view/View;Z)V
    .locals 0

    .line 224
    check-cast p1, Landroid/widget/TextView;

    if-eqz p2, :cond_0

    .line 226
    sget p2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_contact_twitter_selected:I

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 228
    :cond_0
    sget p2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_contact_twitter:I

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method static synthetic lambda$onFinishInflate$6(Landroid/content/Context;Landroid/view/View;Z)V
    .locals 0

    .line 236
    check-cast p1, Landroid/widget/TextView;

    if-eqz p2, :cond_0

    .line 238
    sget p2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_contact_website_selected:I

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 240
    :cond_0
    sget p2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_contact_website:I

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private loadFeaturedImage(Ljava/lang/String;IIZ)V
    .locals 8

    .line 467
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featured:Lcom/squareup/register/widgets/AspectRatioImageView;

    new-instance v7, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$PYpMNXaS_T2V_igbPFHEFCIm6sQ;

    move-object v1, v7

    move-object v2, p0

    move v3, p2

    move v4, p3

    move-object v5, p1

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$PYpMNXaS_T2V_igbPFHEFCIm6sQ;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;IILjava/lang/String;Z)V

    invoke-static {v0, v7}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    return-void
.end method

.method private setFeaturedBitmap(Landroid/graphics/Bitmap;)V
    .locals 3

    .line 446
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_image_border_light_gray:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 448
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    const/4 p1, 0x2

    new-array p1, p1, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    aput-object v1, p1, v2

    const/4 v1, 0x1

    aput-object v0, p1, v1

    .line 454
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featured:Lcom/squareup/register/widgets/AspectRatioImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/AspectRatioImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 455
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featured:Lcom/squareup/register/widgets/AspectRatioImageView;

    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v1, p1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/AspectRatioImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method private setMerchantLogoBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    .line 594
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->logo:Landroid/widget/ImageView;

    sget v1, Lcom/squareup/marin/R$drawable;->marin_image_border_light_gray_1px:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 595
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->logo:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private updateLayoutTransitions()V
    .locals 2

    .line 603
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    new-instance v1, Landroid/animation/LayoutTransition;

    invoke-direct {v1}, Landroid/animation/LayoutTransition;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    return-void
.end method


# virtual methods
.method clearFeaturedImage(Z)V
    .locals 2

    .line 402
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featured:Lcom/squareup/register/widgets/AspectRatioImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/AspectRatioImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 403
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featured:Lcom/squareup/register/widgets/AspectRatioImageView;

    sget v1, Lcom/squareup/marin/R$drawable;->marin_selector_image_border_light_gray:I

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/AspectRatioImageView;->setBackgroundResource(I)V

    if-eqz p1, :cond_0

    .line 407
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featured:Lcom/squareup/register/widgets/AspectRatioImageView;

    invoke-virtual {p1}, Lcom/squareup/register/widgets/AspectRatioImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    .line 408
    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    const/4 v0, -0x2

    .line 409
    iput v0, p1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 411
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featured:Lcom/squareup/register/widgets/AspectRatioImageView;

    const/high16 v0, 0x3f400000    # 0.75f

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/AspectRatioImageView;->setAspectRatio(F)V

    .line 412
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featured:Lcom/squareup/register/widgets/AspectRatioImageView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/AspectRatioImageView;->setAspectRatioEnabled(Z)V

    .line 413
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featured:Lcom/squareup/register/widgets/AspectRatioImageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/AspectRatioImageView;->setDominantMeasurement(I)V

    :cond_0
    return-void
.end method

.method clearMerchantLogo()V
    .locals 3

    .line 390
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$drawable;->marin_image_border_light_gray_1px:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 392
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 393
    iget v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoWidth:I

    iget v2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoHeight:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setSize(II)V

    .line 395
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->logoText:Landroid/widget/TextView;

    sget v2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_add_logo:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 396
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->logo:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 398
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->logo:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method decodeImageDimensions(Landroid/net/Uri;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$DecodeDimensionCallback;)V
    .locals 2

    .line 422
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->fileThreadExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$R92kIm7PIPzdgk8Y5e7zzKUzK3s;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$R92kIm7PIPzdgk8Y5e7zzKUzK3s;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Landroid/net/Uri;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$DecodeDimensionCallback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 607
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method getMerchantEditPhotoPopup()Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;
    .locals 1

    .line 376
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantEditPhotoPopup:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup;

    return-object v0
.end method

.method public hideChangeLogoContainer()V
    .locals 2

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 279
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoMessage:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method isFacebookFocused()Z
    .locals 1

    .line 418
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->facebook:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->isFocused()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$decodeImageDimensions$9$MerchantProfileView(Landroid/net/Uri;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$DecodeDimensionCallback;)V
    .locals 3

    .line 424
    new-instance v0, Lcom/squareup/server/ContentProviderImageResolver;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/server/ContentProviderImageResolver;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 425
    invoke-virtual {v0}, Lcom/squareup/server/ContentProviderImageResolver;->mimeType()Ljava/lang/String;

    move-result-object v0

    .line 428
    invoke-static {v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->isSupportedImageType(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 429
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/squareup/ui/Bitmaps;->decodeBitmapBounds(Landroid/content/Context;Landroid/net/Uri;)[I

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x2

    new-array p1, p1, [I

    .line 431
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$rK3P4qv7tNC2UXyE6aFc5AGdr4g;

    invoke-direct {v2, p2, p1, v0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$rK3P4qv7tNC2UXyE6aFc5AGdr4g;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$DecodeDimensionCallback;[ILjava/lang/String;)V

    invoke-interface {v1, v2}, Lcom/squareup/thread/executor/MainThread;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public synthetic lambda$loadFeaturedImage$10$MerchantProfileView(IILjava/lang/String;ZLandroid/view/View;II)V
    .locals 2

    .line 468
    iget-object p5, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featured:Lcom/squareup/register/widgets/AspectRatioImageView;

    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->getMaxHeight()I

    move-result v0

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-static {p5, p6, p7, v0, v1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->adjustAspectRatio(Lcom/squareup/register/widgets/AspectRatioImageView;IIIF)[I

    move-result-object p5

    const/4 p6, 0x0

    .line 469
    aget p7, p5, p6

    const/4 v0, 0x1

    aget p5, p5, v0

    invoke-direct {p0, p7, p5, p1, p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->fitFeaturedImage(IIII)[I

    move-result-object p5

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 475
    :cond_0
    aget p1, p5, p6

    .line 476
    aget p2, p5, v0

    :cond_1
    if-eqz p4, :cond_2

    .line 482
    aget p1, p5, p6

    .line 483
    aget p2, p5, v0

    .line 485
    iget-object p4, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->thumbor:Lcom/squareup/pollexor/Thumbor;

    invoke-virtual {p4, p3}, Lcom/squareup/pollexor/Thumbor;->buildImage(Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object p3

    invoke-virtual {p3, p1, p2}, Lcom/squareup/pollexor/ThumborUrlBuilder;->resize(II)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/pollexor/ThumborUrlBuilder;->fitIn()Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/pollexor/ThumborUrlBuilder;->toUrl()Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    .line 488
    :cond_2
    aget p4, p5, p6

    aget p5, p5, v0

    .line 489
    invoke-static {p1, p2, p4, p5}, Lcom/squareup/ui/Bitmaps;->calculateInSampleSize(IIII)I

    move-result p4

    .line 490
    invoke-static {p4}, Lcom/squareup/ui/Bitmaps;->previousPowerOfTwo(I)I

    move-result p4

    .line 492
    div-int/2addr p1, p4

    .line 493
    div-int/2addr p2, p4

    .line 496
    :goto_0
    iget-object p4, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {p4, p3}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p3

    sget-object p4, Lcom/squareup/picasso/MemoryPolicy;->NO_CACHE:Lcom/squareup/picasso/MemoryPolicy;

    new-array p5, v0, [Lcom/squareup/picasso/MemoryPolicy;

    sget-object p7, Lcom/squareup/picasso/MemoryPolicy;->NO_STORE:Lcom/squareup/picasso/MemoryPolicy;

    aput-object p7, p5, p6

    .line 497
    invoke-virtual {p3, p4, p5}, Lcom/squareup/picasso/RequestCreator;->memoryPolicy(Lcom/squareup/picasso/MemoryPolicy;[Lcom/squareup/picasso/MemoryPolicy;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p3

    .line 498
    invoke-virtual {p3, p1, p2}, Lcom/squareup/picasso/RequestCreator;->resize(II)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featuredImageTarget:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$FeaturedImageTarget;

    .line 499
    invoke-virtual {p1, p2}, Lcom/squareup/picasso/RequestCreator;->into(Lcom/squareup/picasso/Target;)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$MerchantProfileView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 151
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->mobileBusinessChanged(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$MerchantProfileView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 156
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->photoOnReceiptChanged(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$4$MerchantProfileView(Landroid/content/Context;Landroid/view/View;Z)V
    .locals 0

    .line 208
    check-cast p2, Landroid/widget/TextView;

    if-eqz p3, :cond_0

    .line 210
    sget p3, Lcom/squareup/settingsapplet/R$string;->merchant_profile_contact_facebook_selected:I

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 211
    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->facebook:Lcom/squareup/ui/XableEditText;

    invoke-virtual {p2}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 212
    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->facebook:Lcom/squareup/ui/XableEditText;

    sget p3, Lcom/squareup/registerlib/R$string;->facebook_url:I

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/ui/XableEditText;->append(Ljava/lang/String;)V

    goto :goto_0

    .line 215
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->resolveFacebookAutofill()V

    .line 216
    sget p3, Lcom/squareup/settingsapplet/R$string;->merchant_profile_contact_facebook:I

    invoke-virtual {p1, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public synthetic lambda$setFeaturedImage$7$MerchantProfileView(Landroid/net/Uri;IILjava/lang/String;)V
    .locals 0

    .line 345
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p4, 0x0

    .line 344
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->loadFeaturedImage(Ljava/lang/String;IIZ)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 290
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->dropView(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->picasso:Lcom/squareup/picasso/Picasso;

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoTarget:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$MerchantLogoTarget;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->cancelRequest(Lcom/squareup/picasso/Target;)V

    .line 285
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->picasso:Lcom/squareup/picasso/Picasso;

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featuredImageTarget:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$FeaturedImageTarget;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->cancelRequest(Lcom/squareup/picasso/Target;)V

    .line 286
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .line 127
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 128
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 130
    sget v1, Lcom/squareup/settingsapplet/R$id;->merchant_profile_animator:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/PersistentViewAnimator;

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->animator:Lcom/squareup/widgets/PersistentViewAnimator;

    .line 131
    sget v1, Lcom/squareup/settingsapplet/R$id;->merchant_profile_content:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    .line 133
    sget v1, Lcom/squareup/settingsapplet/R$id;->merchant_profile_retry:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->retry:Landroid/widget/Button;

    .line 134
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->retry:Landroid/widget/Button;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$1;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_logo_container:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoContainer:Landroid/view/View;

    .line 142
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoContainer:Landroid/view/View;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$2;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_logo_message:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoMessage:Landroid/view/View;

    .line 149
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_mobile_business:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->mobileBusiness:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 150
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->mobileBusiness:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$dxsP4pVs_MyYEJTyJAHl1XbXDaE;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$dxsP4pVs_MyYEJTyJAHl1XbXDaE;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 153
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_photo_on_receipt:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->photoOnReceipt:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 154
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->photoOnReceipt:Lcom/squareup/widgets/list/ToggleButtonRow;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setVisibility(I)V

    .line 155
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->photoOnReceipt:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$Math0R1c-PGifNaQmhTkmy-o5f4;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$Math0R1c-PGifNaQmhTkmy-o5f4;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 158
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->business_address_section:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->businessAddressSection:Landroid/view/View;

    .line 159
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_address:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->address:Landroid/widget/TextView;

    .line 160
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_address_edit_button:I

    .line 161
    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->editAddressButton:Landroid/view/View;

    .line 162
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->editAddressButton:Landroid/view/View;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$3;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$3;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_logo_image:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->logo:Landroid/widget/ImageView;

    .line 169
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_logo_text:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->logoText:Landroid/widget/TextView;

    .line 171
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_business_name:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/XableEditText;

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->businessName:Lcom/squareup/ui/XableEditText;

    .line 172
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->businessName:Lcom/squareup/ui/XableEditText;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$4;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$4;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 178
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_phone:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/XableEditText;

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->phone:Lcom/squareup/ui/XableEditText;

    .line 179
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->phone:Lcom/squareup/ui/XableEditText;

    new-instance v2, Lcom/squareup/text/ScrubbingTextWatcher;

    iget-object v3, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->phoneNumberScrubber:Lcom/squareup/text/InsertingScrubber;

    invoke-direct {v2, v3, v1}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 180
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->phone:Lcom/squareup/ui/XableEditText;

    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->watcher:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$ContactInfoTextWatcher;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 181
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->phone:Lcom/squareup/ui/XableEditText;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$cBsgTzJsIMD_T1E_xI5fmjIQrV4;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$cBsgTzJsIMD_T1E_xI5fmjIQrV4;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 190
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_email:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/XableEditText;

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->email:Lcom/squareup/ui/XableEditText;

    .line 191
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->email:Lcom/squareup/ui/XableEditText;

    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->watcher:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$ContactInfoTextWatcher;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 192
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->email:Lcom/squareup/ui/XableEditText;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$tyaGFGr3mYTXuCff743fZygH-e0;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$tyaGFGr3mYTXuCff743fZygH-e0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 201
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_email_suggestion_box:I

    .line 202
    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 203
    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->email:Lcom/squareup/ui/XableEditText;

    invoke-static {v2, v1, p0}, Lcom/squareup/widgets/EmailSuggestionHandler;->wireEmailSuggestions(Lcom/squareup/widgets/EmailSuggestionHandler$SuggestionListener;Landroid/widget/TextView;Landroid/view/View;)Lcom/squareup/widgets/EmailSuggestionHandler;

    .line 205
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_facebook:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/XableEditText;

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->facebook:Lcom/squareup/ui/XableEditText;

    .line 206
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->facebook:Lcom/squareup/ui/XableEditText;

    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->watcher:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$ContactInfoTextWatcher;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 207
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->facebook:Lcom/squareup/ui/XableEditText;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$9fO5vr24t4ZciqRVd7OO11aNGgY;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$9fO5vr24t4ZciqRVd7OO11aNGgY;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 219
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->facebook:Lcom/squareup/ui/XableEditText;

    new-instance v2, Lcom/squareup/text/ScrubbingTextWatcher;

    new-instance v3, Lcom/squareup/text/FacebookScrubber;

    invoke-direct {v3}, Lcom/squareup/text/FacebookScrubber;-><init>()V

    iget-object v4, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->facebook:Lcom/squareup/ui/XableEditText;

    invoke-direct {v2, v3, v4}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/SelectableTextScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 221
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_twitter:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/XableEditText;

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->twitter:Lcom/squareup/ui/XableEditText;

    .line 222
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->twitter:Lcom/squareup/ui/XableEditText;

    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->watcher:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$ContactInfoTextWatcher;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 223
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->twitter:Lcom/squareup/ui/XableEditText;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$JjBWt_Mbyr_7N4l5O4W99AcqoGw;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$JjBWt_Mbyr_7N4l5O4W99AcqoGw;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 231
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->twitter:Lcom/squareup/ui/XableEditText;

    new-instance v2, Lcom/squareup/text/ScrubbingTextWatcher;

    new-instance v3, Lcom/squareup/text/TwitterScrubber;

    invoke-direct {v3}, Lcom/squareup/text/TwitterScrubber;-><init>()V

    iget-object v4, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->twitter:Lcom/squareup/ui/XableEditText;

    invoke-direct {v2, v3, v4}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 233
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v2, Lcom/squareup/settingsapplet/R$id;->public_profile_website:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/XableEditText;

    iput-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->website:Lcom/squareup/ui/XableEditText;

    .line 234
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->website:Lcom/squareup/ui/XableEditText;

    iget-object v2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->watcher:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$ContactInfoTextWatcher;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 235
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->website:Lcom/squareup/ui/XableEditText;

    new-instance v2, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$mFJZ-CF5EB2zMYcwUTwDNU1FG7U;

    invoke-direct {v2, v0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$mFJZ-CF5EB2zMYcwUTwDNU1FG7U;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v1, Lcom/squareup/settingsapplet/R$id;->mobile_business_help_text:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->mobileBusinessHelpText:Landroid/view/View;

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->phone:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->email:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setNextFocusView(Landroid/view/View;)V

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->email:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->website:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setNextFocusView(Landroid/view/View;)V

    .line 248
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->website:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->facebook:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setNextFocusView(Landroid/view/View;)V

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->facebook:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->twitter:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setNextFocusView(Landroid/view/View;)V

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v1, Lcom/squareup/settingsapplet/R$id;->public_profile_description:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->description:Landroid/widget/EditText;

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->description:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$5;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->businessName:Lcom/squareup/ui/XableEditText;

    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->description:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setNextFocusView(Landroid/view/View;)V

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    sget v1, Lcom/squareup/settingsapplet/R$id;->public_profile_featured:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/register/widgets/AspectRatioImageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featured:Lcom/squareup/register/widgets/AspectRatioImageView;

    .line 261
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method resolveFacebookAutofill()V
    .locals 3

    .line 380
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 381
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->facebook:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v1}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 382
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/squareup/registerlib/R$string;->facebook_url:I

    .line 383
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->facebook:Lcom/squareup/ui/XableEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method restoreContent(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;IIZLcom/squareup/address/Address;)V
    .locals 8

    move-object v7, p0

    const/4 v0, 0x0

    move-object v1, p1

    move-object v2, p2

    .line 323
    invoke-virtual {p0, p1, v0, p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setMerchantLogoBitmap(Ljava/io/File;Landroid/graphics/Bitmap;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p3

    move v2, p4

    move v3, p5

    move-object v4, p6

    move v5, p7

    move/from16 v6, p8

    .line 324
    invoke-virtual/range {v0 .. v6}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setFeaturedImage(Ljava/lang/String;IILandroid/net/Uri;II)V

    move/from16 v0, p9

    .line 326
    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setMobileBusinessContent(Z)V

    .line 327
    iget-object v0, v7, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->address:Landroid/widget/TextView;

    invoke-static/range {p10 .. p10}, Lcom/squareup/address/AddressFormatter;->toTwoLineDisplayString(Lcom/squareup/address/Address;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 328
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->updateLayoutTransitions()V

    return-void
.end method

.method protected setBusinessAddress(Lcom/squareup/address/Address;)V
    .locals 1

    .line 599
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->address:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/address/AddressFormatter;->toTwoLineDisplayString(Lcom/squareup/address/Address;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setContent(ZZLcom/squareup/address/Address;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;IILandroid/net/Uri;IILjava/lang/String;Lcom/squareup/merchantprofile/ContactInfo;Ljava/lang/String;)V
    .locals 9

    move-object v7, p0

    move-object/from16 v0, p13

    .line 298
    iget-object v1, v7, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->mobileBusiness:Lcom/squareup/widgets/list/ToggleButtonRow;

    const/4 v2, 0x0

    move v8, p1

    invoke-virtual {v1, p1, v2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(ZZ)V

    .line 299
    iget-object v1, v7, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->photoOnReceipt:Lcom/squareup/widgets/list/ToggleButtonRow;

    move v3, p2

    invoke-virtual {v1, p2, v2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(ZZ)V

    .line 300
    iget-object v1, v7, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->address:Landroid/widget/TextView;

    invoke-static {p3}, Lcom/squareup/address/AddressFormatter;->toTwoLineDisplayString(Lcom/squareup/address/Address;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    iget-object v1, v7, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->businessName:Lcom/squareup/ui/XableEditText;

    move-object/from16 v2, p12

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 302
    iget-object v1, v7, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->phone:Lcom/squareup/ui/XableEditText;

    iget-object v2, v0, Lcom/squareup/merchantprofile/ContactInfo;->phone:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 303
    iget-object v1, v7, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->email:Lcom/squareup/ui/XableEditText;

    iget-object v2, v0, Lcom/squareup/merchantprofile/ContactInfo;->email:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 304
    iget-object v1, v7, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->facebook:Lcom/squareup/ui/XableEditText;

    iget-object v2, v0, Lcom/squareup/merchantprofile/ContactInfo;->facebook:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 305
    iget-object v1, v7, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->twitter:Lcom/squareup/ui/XableEditText;

    iget-object v2, v0, Lcom/squareup/merchantprofile/ContactInfo;->twitter:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 306
    iget-object v1, v7, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->website:Lcom/squareup/ui/XableEditText;

    iget-object v0, v0, Lcom/squareup/merchantprofile/ContactInfo;->website:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 307
    iget-object v0, v7, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->description:Landroid/widget/EditText;

    move-object/from16 v1, p14

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    move-object v1, p4

    move-object v2, p5

    .line 309
    invoke-virtual {p0, p4, v0, p5}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setMerchantLogoBitmap(Ljava/io/File;Landroid/graphics/Bitmap;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p6

    move/from16 v2, p7

    move/from16 v3, p8

    move-object/from16 v4, p9

    move/from16 v5, p10

    move/from16 v6, p11

    .line 310
    invoke-virtual/range {v0 .. v6}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setFeaturedImage(Ljava/lang/String;IILandroid/net/Uri;II)V

    .line 312
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setMobileBusinessContent(Z)V

    .line 314
    iget-object v0, v7, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->animator:Lcom/squareup/widgets/PersistentViewAnimator;

    iget-object v1, v7, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/PersistentViewAnimator;->setDisplayedChildById(I)V

    .line 316
    invoke-direct {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->updateLayoutTransitions()V

    return-void
.end method

.method setErrorContent()V
    .locals 2

    .line 332
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->animator:Lcom/squareup/widgets/PersistentViewAnimator;

    sget v1, Lcom/squareup/settingsapplet/R$id;->merchant_profile_error:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/PersistentViewAnimator;->setDisplayedChildById(I)V

    return-void
.end method

.method public setFeaturedClickListener()V
    .locals 2

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->featured:Lcom/squareup/register/widgets/AspectRatioImageView;

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$6;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$6;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/AspectRatioImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method setFeaturedImage(Ljava/lang/String;IILandroid/net/Uri;II)V
    .locals 1

    const/4 v0, 0x1

    if-eqz p4, :cond_2

    .line 339
    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->clearFeaturedImage(Z)V

    if-lez p5, :cond_1

    if-gtz p6, :cond_0

    goto :goto_0

    .line 347
    :cond_0
    invoke-virtual {p4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    invoke-direct {p0, p1, p5, p6, p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->loadFeaturedImage(Ljava/lang/String;IIZ)V

    goto :goto_1

    .line 343
    :cond_1
    :goto_0
    new-instance p1, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$SkH1YE3gTVWfsGWwVkUFo-ZP4-s;

    invoke-direct {p1, p0, p4}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileView$SkH1YE3gTVWfsGWwVkUFo-ZP4-s;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;Landroid/net/Uri;)V

    invoke-virtual {p0, p4, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->decodeImageDimensions(Landroid/net/Uri;Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$DecodeDimensionCallback;)V

    goto :goto_1

    .line 350
    :cond_2
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p4

    if-nez p4, :cond_3

    .line 351
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->loadFeaturedImage(Ljava/lang/String;IIZ)V

    :cond_3
    :goto_1
    return-void
.end method

.method setMerchantLogoBitmap(Ljava/io/File;Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 1

    .line 562
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->clearMerchantLogo()V

    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 566
    invoke-static {p3}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 568
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->logoText:Landroid/widget/TextView;

    sget p2, Lcom/squareup/settingsapplet/R$string;->merchant_profile_change_logo:I

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 569
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->thumbor:Lcom/squareup/pollexor/Thumbor;

    invoke-virtual {p1, p3}, Lcom/squareup/pollexor/Thumbor;->buildImage(Ljava/lang/String;)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object p1

    iget p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoWidth:I

    iget p3, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoHeight:I

    .line 570
    invoke-virtual {p1, p2, p3}, Lcom/squareup/pollexor/ThumborUrlBuilder;->resize(II)Lcom/squareup/pollexor/ThumborUrlBuilder;

    move-result-object p1

    .line 571
    invoke-virtual {p1}, Lcom/squareup/pollexor/ThumborUrlBuilder;->toUrl()Ljava/lang/String;

    move-result-object p1

    .line 572
    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->picasso:Lcom/squareup/picasso/Picasso;

    invoke-virtual {p2, p1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoTarget:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$MerchantLogoTarget;

    invoke-virtual {p1, p2}, Lcom/squareup/picasso/RequestCreator;->into(Lcom/squareup/picasso/Target;)V

    :cond_0
    return-void

    .line 578
    :cond_1
    iget-object p3, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->logoText:Landroid/widget/TextView;

    sget v0, Lcom/squareup/settingsapplet/R$string;->merchant_profile_change_logo:I

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(I)V

    if-eqz p2, :cond_2

    .line 582
    invoke-direct {p0, p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->setMerchantLogoBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 584
    :cond_2
    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->picasso:Lcom/squareup/picasso/Picasso;

    .line 585
    invoke-virtual {p2, p1}, Lcom/squareup/picasso/Picasso;->load(Ljava/io/File;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    iget p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoWidth:I

    iget p3, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoHeight:I

    .line 586
    invoke-virtual {p1, p2, p3}, Lcom/squareup/picasso/RequestCreator;->resize(II)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    .line 587
    invoke-virtual {p1}, Lcom/squareup/picasso/RequestCreator;->centerCrop()Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoTarget:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView$MerchantLogoTarget;

    .line 588
    invoke-virtual {p1, p2}, Lcom/squareup/picasso/RequestCreator;->into(Lcom/squareup/picasso/Target;)V

    :goto_0
    return-void
.end method

.method setMobileBusinessContent(Z)V
    .locals 4

    .line 365
    new-instance v0, Landroidx/transition/Fade;

    invoke-direct {v0}, Landroidx/transition/Fade;-><init>()V

    const-wide/16 v1, 0x12c

    .line 366
    invoke-virtual {v0, v1, v2}, Landroidx/transition/Transition;->setDuration(J)Landroidx/transition/Transition;

    .line 367
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->mobileBusinessHelpText:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroidx/transition/Transition;->addTarget(Landroid/view/View;)Landroidx/transition/Transition;

    .line 368
    iget-object v1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->content:Landroid/view/ViewGroup;

    invoke-static {v1, v0}, Landroidx/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;Landroidx/transition/Transition;)V

    .line 370
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->mobileBusiness:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 371
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->mobileBusinessHelpText:Landroid/view/View;

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eqz p1, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_0
    const/16 v3, 0x8

    :goto_0
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 372
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->businessAddressSection:Landroid/view/View;

    if-eqz p1, :cond_1

    const/16 v1, 0x8

    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public showChangeLogoContainer()V
    .locals 2

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoContainer:Landroid/view/View;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->merchantLogoMessage:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method showFeaturedImageTooSmallError()V
    .locals 1

    .line 356
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->showFeaturedImageTooSmallError()V

    return-void
.end method

.method showFeaturedImageWrongMimeTypeError()V
    .locals 1

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->showFeaturedImageWrongMimeTypeError()V

    return-void
.end method
