.class public final Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;
.super Ljava/lang/Object;
.source "InstantDepositsCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final starterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)V"
        }
    .end annotation

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p3, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;->starterProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p4, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;->clockProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)",
            "Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;"
        }
    .end annotation

    .line 44
    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/util/Device;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/util/Clock;)Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;
    .locals 1

    .line 49
    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;-><init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/util/Device;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/util/Clock;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;
    .locals 4

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Device;

    iget-object v2, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;->starterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/onboarding/OnboardingStarter;

    iget-object v3, p0, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/Clock;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;->newInstance(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/util/Device;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/util/Clock;)Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator_Factory;->get()Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;

    move-result-object v0

    return-object v0
.end method
