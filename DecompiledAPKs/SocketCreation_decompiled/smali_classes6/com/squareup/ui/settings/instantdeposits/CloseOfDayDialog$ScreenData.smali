.class public final Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;
.super Ljava/lang/Object;
.source "CloseOfDayDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;",
        "",
        "dayOfWeek",
        "",
        "cutoffAt",
        "Lcom/squareup/protos/common/time/DayTime;",
        "timeZone",
        "Ljava/util/TimeZone;",
        "(ILcom/squareup/protos/common/time/DayTime;Ljava/util/TimeZone;)V",
        "getCutoffAt",
        "()Lcom/squareup/protos/common/time/DayTime;",
        "getDayOfWeek",
        "()I",
        "getTimeZone",
        "()Ljava/util/TimeZone;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final cutoffAt:Lcom/squareup/protos/common/time/DayTime;

.field private final dayOfWeek:I

.field private final timeZone:Ljava/util/TimeZone;


# direct methods
.method public constructor <init>(ILcom/squareup/protos/common/time/DayTime;Ljava/util/TimeZone;)V
    .locals 1

    const-string v0, "cutoffAt"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "timeZone"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;->dayOfWeek:I

    iput-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;->cutoffAt:Lcom/squareup/protos/common/time/DayTime;

    iput-object p3, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;->timeZone:Ljava/util/TimeZone;

    return-void
.end method


# virtual methods
.method public final getCutoffAt()Lcom/squareup/protos/common/time/DayTime;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;->cutoffAt:Lcom/squareup/protos/common/time/DayTime;

    return-object v0
.end method

.method public final getDayOfWeek()I
    .locals 1

    .line 32
    iget v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;->dayOfWeek:I

    return v0
.end method

.method public final getTimeZone()Ljava/util/TimeZone;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;->timeZone:Ljava/util/TimeZone;

    return-object v0
.end method
