.class public interface abstract Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope$Component;
.super Ljava/lang/Object;
.source "LoyaltySettingsScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract loyaltySettingsCoordinator()Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;
.end method
