.class public abstract Lcom/squareup/ui/settings/taxes/tax/InTaxScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InTaxScope.java"


# instance fields
.field final cogsTaxId:Ljava/lang/String;

.field private final taxPath:Lcom/squareup/ui/settings/taxes/tax/TaxScope;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 10
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/InTaxScope;->cogsTaxId:Ljava/lang/String;

    .line 12
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxScope;

    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxScope;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/InTaxScope;->taxPath:Lcom/squareup/ui/settings/taxes/tax/TaxScope;

    return-void
.end method


# virtual methods
.method public final getParentKey()Ljava/lang/Object;
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/InTaxScope;->taxPath:Lcom/squareup/ui/settings/taxes/tax/TaxScope;

    return-object v0
.end method

.method isNewTax()Z
    .locals 1

    .line 16
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/InTaxScope;->taxPath:Lcom/squareup/ui/settings/taxes/tax/TaxScope;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxScope;->isNewTax()Z

    move-result v0

    return v0
.end method
