.class Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;
.super Ljava/lang/Object;
.source "TaxState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "TaxItemCounts"
.end annotation


# instance fields
.field private applicableItemCount:I

.field private applicableServiceCount:I

.field public final itemCount:I

.field public final serviceCount:I


# direct methods
.method constructor <init>(IIII)V
    .locals 0

    .line 441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442
    iput p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->itemCount:I

    .line 443
    iput p3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->serviceCount:I

    .line 444
    iput p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableItemCount:I

    .line 445
    iput p4, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableServiceCount:I

    return-void
.end method


# virtual methods
.method public allItems()V
    .locals 1

    .line 457
    iget v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->itemCount:I

    iput v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableItemCount:I

    return-void
.end method

.method public allServices()V
    .locals 1

    .line 477
    iget v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->itemCount:I

    iput v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableServiceCount:I

    return-void
.end method

.method decrementItem()V
    .locals 1

    .line 453
    iget v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableItemCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableItemCount:I

    return-void
.end method

.method decrementService()V
    .locals 1

    .line 473
    iget v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableServiceCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableServiceCount:I

    return-void
.end method

.method getApplicableItemCount()I
    .locals 1

    .line 465
    iget v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableItemCount:I

    return v0
.end method

.method getApplicableServiceCount()I
    .locals 1

    .line 485
    iget v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableServiceCount:I

    return v0
.end method

.method public incrementItem()V
    .locals 1

    .line 449
    iget v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableItemCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableItemCount:I

    return-void
.end method

.method public incrementService()V
    .locals 1

    .line 469
    iget v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableServiceCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableServiceCount:I

    return-void
.end method

.method public noItems()V
    .locals 1

    const/4 v0, 0x0

    .line 461
    iput v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableItemCount:I

    return-void
.end method

.method public noServices()V
    .locals 1

    const/4 v0, 0x0

    .line 481
    iput v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;->applicableServiceCount:I

    return-void
.end method
