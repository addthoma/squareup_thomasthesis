.class public Lcom/squareup/ui/settings/taxes/tax/ConfirmDiscardTaxChangesDialogScreen$Factory;
.super Ljava/lang/Object;
.source "ConfirmDiscardTaxChangesDialogScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/ConfirmDiscardTaxChangesDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 23
    const-class v0, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;

    .line 24
    invoke-interface {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;->taxScopeRunner()Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$-YTFClF2XMhGnmYkkwDj_BZr1hg;

    invoke-direct {v1, v0}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$-YTFClF2XMhGnmYkkwDj_BZr1hg;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;)V

    .line 26
    invoke-static {p1, v1}, Lcom/squareup/ui/ConfirmDiscardChangesAlertDialogFactory;->createConfirmDiscardChangesAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;)Landroid/app/AlertDialog;

    move-result-object p1

    .line 25
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
