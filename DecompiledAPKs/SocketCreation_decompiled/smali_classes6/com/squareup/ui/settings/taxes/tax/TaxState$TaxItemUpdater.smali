.class Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;
.super Ljava/lang/Object;
.source "TaxState.java"

# interfaces
.implements Lcom/squareup/settings/server/FeesEditor$TaxItemUpdater;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TaxItemUpdater"
.end annotation


# instance fields
.field private final applyItems:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private final exemptItems:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;"
        }
    .end annotation
.end field

.field private final feeId:Ljava/lang/String;

.field private final globalAction:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/taxes/tax/TaxState;)V
    .locals 2

    .line 329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330
    invoke-static {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->access$100(Lcom/squareup/ui/settings/taxes/tax/TaxState;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->feeId:Ljava/lang/String;

    .line 331
    invoke-static {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->access$200(Lcom/squareup/ui/settings/taxes/tax/TaxState;)Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->globalAction:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    .line 332
    iget-object v0, p1, Lcom/squareup/ui/settings/taxes/tax/TaxState;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 333
    invoke-static {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->access$300(Lcom/squareup/ui/settings/taxes/tax/TaxState;)Lcom/squareup/catalogapi/CatalogIntegrationController;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    .line 334
    new-instance v0, Ljava/util/LinkedHashSet;

    iget-object v1, p1, Lcom/squareup/ui/settings/taxes/tax/TaxState;->applyItems:Ljava/util/LinkedHashSet;

    invoke-direct {v0, v1}, Ljava/util/LinkedHashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->applyItems:Ljava/util/LinkedHashSet;

    .line 335
    new-instance v0, Ljava/util/LinkedHashMap;

    iget-object p1, p1, Lcom/squareup/ui/settings/taxes/tax/TaxState;->exemptItems:Ljava/util/Map;

    invoke-direct {v0, p1}, Ljava/util/LinkedHashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->exemptItems:Ljava/util/LinkedHashMap;

    return-void
.end method

.method private performGlobal(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/List;Ljava/util/List;)Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)",
            "Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;"
        }
    .end annotation

    .line 368
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->feeId:Ljava/lang/String;

    .line 369
    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->findTaxItemRelations(Ljava/lang/String;)Lcom/squareup/shared/catalog/TypedCursor;

    move-result-object p1

    .line 371
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/TypedCursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 372
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/TypedCursor;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/Related;

    .line 375
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v1}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowNewFeature()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    .line 376
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    new-array v2, v2, [Lcom/squareup/api/items/Item$Type;

    sget-object v4, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v1

    goto :goto_1

    .line 378
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/squareup/api/items/Item$Type;

    sget-object v5, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    aput-object v5, v4, v3

    sget-object v3, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    aput-object v3, v4, v2

    .line 379
    invoke-virtual {v1, v4}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v1

    .line 381
    :goto_1
    iget-object v2, v0, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 385
    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->applyItems:Ljava/util/LinkedHashSet;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->exemptItems:Ljava/util/LinkedHashMap;

    iget-object v3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->globalAction:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    iget-object v4, v0, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v4, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 386
    invoke-virtual {v4}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v4

    iget-boolean v5, v0, Lcom/squareup/shared/catalog/Related;->related:Z

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->access$400(Ljava/util/LinkedHashSet;Ljava/util/Map;Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;Ljava/lang/String;Z)Lcom/squareup/util/MaybeBoolean;

    move-result-object v1

    .line 389
    invoke-virtual {v1}, Lcom/squareup/util/MaybeBoolean;->isTrue()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, v0, Lcom/squareup/shared/catalog/Related;->related:Z

    if-nez v2, :cond_3

    .line 391
    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;

    invoke-direct {v1}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->feeId:Ljava/lang/String;

    .line 392
    invoke-virtual {v1, v2}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->setFeeId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;

    move-result-object v1

    iget-object v0, v0, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItem;

    .line 393
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->setItemId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;

    move-result-object v0

    .line 391
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 394
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/util/MaybeBoolean;->isFalse()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcom/squareup/shared/catalog/Related;->related:Z

    if-eqz v1, :cond_0

    .line 396
    iget-object v0, v0, Lcom/squareup/shared/catalog/Related;->relation:Lcom/squareup/shared/catalog/models/CatalogObject;

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 399
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/TypedCursor;->close()V

    .line 400
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->feeId:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;->globalAdd(Ljava/lang/String;)Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public findRelationsToUpdate(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/List;Ljava/util/List;)Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogObject<",
            "*>;>;)",
            "Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;"
        }
    .end annotation

    .line 342
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->globalAction:Lcom/squareup/ui/settings/taxes/tax/TaxState$GlobalAction;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->performGlobal(Lcom/squareup/shared/catalog/Catalog$Local;Ljava/util/List;Ljava/util/List;)Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;

    move-result-object p1

    return-object p1

    .line 345
    :cond_0
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 348
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->exemptItems:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;

    if-eqz v1, :cond_1

    .line 353
    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 354
    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;->getItemId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 357
    :cond_2
    iget-object p3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->applyItems:Ljava/util/LinkedHashSet;

    invoke-virtual {p3}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_1
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 358
    new-instance v1, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;

    invoke-direct {v1}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->feeId:Ljava/lang/String;

    .line 359
    invoke-virtual {v1, v2}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->setFeeId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;

    move-result-object v1

    .line 360
    invoke-virtual {v1, v0}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->setItemId(Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership$Builder;->build()Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;

    move-result-object v1

    .line 358
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    .line 361
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 363
    :cond_3
    iget-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemUpdater;->feeId:Ljava/lang/String;

    invoke-static {p2, p1}, Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;->edits(Ljava/lang/String;Ljava/util/Map;)Lcom/squareup/payment/OrderEntryEvents$TaxItemRelationshipsChanged;

    move-result-object p1

    return-object p1
.end method
