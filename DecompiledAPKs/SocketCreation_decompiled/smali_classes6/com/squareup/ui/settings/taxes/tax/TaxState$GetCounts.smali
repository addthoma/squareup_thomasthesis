.class Lcom/squareup/ui/settings/taxes/tax/TaxState$GetCounts;
.super Ljava/lang/Object;
.source "TaxState.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GetCounts"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;",
        ">;"
    }
.end annotation


# instance fields
.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final taxId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Ljava/lang/String;)V
    .locals 0

    .line 409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GetCounts;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 411
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GetCounts;->taxId:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;Ljava/lang/String;Lcom/squareup/ui/settings/taxes/tax/TaxState$1;)V
    .locals 0

    .line 404
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxState$GetCounts;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public perform(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;
    .locals 5

    .line 415
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GetCounts;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/squareup/api/items/Item$Type;

    sget-object v2, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 416
    invoke-virtual {v0, v1}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v0

    .line 417
    sget-object v1, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 419
    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->countItems(Ljava/util/List;)I

    move-result v2

    .line 420
    invoke-interface {p1, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->countItems(Ljava/util/List;)I

    move-result v3

    .line 422
    iget-object v4, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GetCounts;->taxId:Ljava/lang/String;

    if-nez v4, :cond_0

    move v0, v2

    goto :goto_0

    .line 424
    :cond_0
    invoke-interface {p1, v4, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->countTaxItems(Ljava/lang/String;Ljava/util/List;)I

    move-result v0

    .line 426
    :goto_0
    iget-object v4, p0, Lcom/squareup/ui/settings/taxes/tax/TaxState$GetCounts;->taxId:Ljava/lang/String;

    if-nez v4, :cond_1

    move p1, v3

    goto :goto_1

    .line 428
    :cond_1
    invoke-interface {p1, v4, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->countTaxItems(Ljava/lang/String;Ljava/util/List;)I

    move-result p1

    .line 430
    :goto_1
    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    invoke-direct {v1, v2, v0, v3, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;-><init>(IIII)V

    return-object v1
.end method

.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 404
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState$GetCounts;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/ui/settings/taxes/tax/TaxState$TaxItemCounts;

    move-result-object p1

    return-object p1
.end method
