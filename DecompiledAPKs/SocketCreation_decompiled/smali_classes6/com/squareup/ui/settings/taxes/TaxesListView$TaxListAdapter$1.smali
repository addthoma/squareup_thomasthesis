.class Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "TaxesListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->buildAndBindButtonRow(Landroid/view/ViewGroup;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;)V
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter$1;->this$1:Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 145
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter$1;->this$1:Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;

    iget-object p1, p1, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->this$0:Lcom/squareup/ui/settings/taxes/TaxesListView;

    iget-object p1, p1, Lcom/squareup/ui/settings/taxes/TaxesListView;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TAXES_CREATE:Lcom/squareup/analytics/RegisterViewName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter$1;->this$1:Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;

    iget-object p1, p1, Lcom/squareup/ui/settings/taxes/TaxesListView$TaxListAdapter;->this$0:Lcom/squareup/ui/settings/taxes/TaxesListView;

    iget-object p1, p1, Lcom/squareup/ui/settings/taxes/TaxesListView;->presenter:Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/TaxesListScreen$Presenter;->onNewTaxClicked()V

    return-void
.end method
