.class public Lcom/squareup/ui/settings/taxes/TaxesSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.source "TaxesSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/TaxesSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation


# instance fields
.field private enabledTaxCounter:Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V
    .locals 6

    .line 40
    new-instance v1, Lcom/squareup/ui/settings/taxes/TaxesSection;

    invoke-direct {v1, p3}, Lcom/squareup/ui/settings/taxes/TaxesSection;-><init>(Lcom/squareup/settings/server/Features;)V

    sget v3, Lcom/squareup/settingsapplet/R$string;->tax_taxes:I

    move-object v0, p0

    move-object v2, p5

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V

    .line 41
    iput-object p4, p0, Lcom/squareup/ui/settings/taxes/TaxesSection$ListEntry;->enabledTaxCounter:Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;

    return-void
.end method


# virtual methods
.method public getValueText()Ljava/lang/String;
    .locals 3

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesSection$ListEntry;->enabledTaxCounter:Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/EnabledTaxCounter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/TaxesSection$ListEntry;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->tax_count_zero:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 50
    sget v1, Lcom/squareup/settingsapplet/R$string;->tax_count_one:I

    goto :goto_0

    :cond_1
    sget v1, Lcom/squareup/settingsapplet/R$string;->tax_count_two_or_more:I

    .line 53
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/TaxesSection$ListEntry;->res:Lcom/squareup/util/Res;

    invoke-interface {v2, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "count"

    .line 54
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 56
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
