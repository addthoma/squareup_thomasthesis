.class Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;
.super Landroid/widget/BaseAdapter;
.source "TaxApplicableView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemSwitchAdapter"
.end annotation


# static fields
.field private static final CHANGE_ALL_ROW_INDEX:I = 0x0

.field private static final CHANGE_ALL_ROW_TYPE:I = 0x0

.field private static final DIVIDER_ROW_TYPE:I = 0x1

.field private static final HORIZONTAL_RULE_ITEMS_INDEX:I = 0x1

.field private static final ITEMS_ROW_INDEX:I = 0x2

.field private static final ITEM_ROW_TYPE:I = 0x2


# instance fields
.field private itemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;"
        }
    .end annotation
.end field

.field private final marinGutterHalf:I

.field final synthetic this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;)V
    .locals 1

    .line 107
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 109
    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->marinGutterHalf:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$1;)V
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;)Ljava/util/List;
    .locals 0

    .line 93
    iget-object p0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->itemList:Ljava/util/List;

    return-object p0
.end method

.method private buildAndBindButtonRow(Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;)Landroid/view/View;
    .locals 3

    if-nez p1, :cond_0

    .line 198
    new-instance p1, Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter$1;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;)V

    new-instance v2, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter$2;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;)V

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;-><init>(Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    :cond_0
    return-object p1
.end method

.method private buildAndBindToggleRow(Landroid/view/ViewGroup;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 2

    if-nez p1, :cond_0

    .line 182
    sget p1, Lcom/squareup/settingsapplet/R$layout;->check_row:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    .line 185
    :cond_0
    sget p2, Lcom/squareup/settingsapplet/R$id;->check_row_title:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 186
    sget v0, Lcom/squareup/settingsapplet/R$id;->check_row_check:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    add-int/lit8 p3, p3, -0x2

    .line 188
    invoke-direct {p0, p3}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->getRelated(I)Lcom/squareup/shared/catalog/Related;

    move-result-object p3

    .line 189
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    iget-object v1, v1, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    invoke-virtual {v1, p3}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->getTaxName(Lcom/squareup/shared/catalog/Related;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    iget-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    iget-object p2, p2, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->presenter:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;

    invoke-virtual {p2, p3}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->isTaxApplied(Lcom/squareup/shared/catalog/Related;)Z

    move-result p2

    invoke-virtual {v0, p2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 191
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    return-object p1
.end method

.method private buildDividerRow(Landroid/view/View;)Landroid/view/View;
    .locals 2

    if-nez p1, :cond_0

    .line 215
    new-instance p1, Lcom/squareup/marin/widgets/HorizontalRuleView;

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->this$0:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->marinGutterHalf:I

    invoke-direct {p1, v0, v1, v1}, Lcom/squareup/marin/widgets/HorizontalRuleView;-><init>(Landroid/content/Context;II)V

    :cond_0
    return-object p1
.end method

.method private getRelated(I)Lcom/squareup/shared/catalog/Related;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;"
        }
    .end annotation

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->itemList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/Related;

    return-object p1
.end method

.method private hasItemRows()Z
    .locals 1

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->itemList:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .line 128
    invoke-direct {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->hasItemRows()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->itemList:Ljava/util/List;

    .line 129
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .line 118
    invoke-direct {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->hasItemRows()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    goto :goto_0

    :cond_0
    sub-int/2addr p1, v0

    .line 120
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->getRelated(I)Lcom/squareup/shared/catalog/Related;

    move-result-object p1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 p1, 0x2

    return p1

    :cond_0
    return v0

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 164
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 172
    check-cast p2, Landroid/view/ViewGroup;

    invoke-direct {p0, p2, p3, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->buildAndBindToggleRow(Landroid/view/ViewGroup;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 176
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unrecognized row type."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 169
    :cond_1
    invoke-direct {p0, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->buildDividerRow(Landroid/view/View;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 166
    :cond_2
    check-cast p2, Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;

    invoke-direct {p0, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->buildAndBindButtonRow(Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .line 138
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->getItemViewType(I)I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public setItems(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;)V"
        }
    .end annotation

    .line 113
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->itemList:Ljava/util/List;

    .line 114
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView$ItemSwitchAdapter;->notifyDataSetChanged()V

    return-void
.end method
