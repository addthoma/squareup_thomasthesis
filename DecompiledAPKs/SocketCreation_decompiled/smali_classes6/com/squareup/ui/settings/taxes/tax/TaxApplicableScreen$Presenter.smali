.class Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "TaxApplicableScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;",
        ">;"
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;

.field private itemRelationshipCallback:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;

.field private final lazyCogs:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final taxScopeRunner:Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

.field private final taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/taxes/tax/TaxState;Ldagger/Lazy;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lflow/Flow;Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/taxes/tax/TaxState;",
            "Ldagger/Lazy<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lflow/Flow;",
            "Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 61
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    .line 63
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->lazyCogs:Ldagger/Lazy;

    .line 64
    iput-object p3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 65
    iput-object p4, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 66
    iput-object p5, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->flow:Lflow/Flow;

    .line 67
    iput-object p6, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->taxScopeRunner:Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    return-void
.end method

.method static synthetic access$102(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;)Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->itemRelationshipCallback:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;

    return-object p1
.end method

.method static synthetic access$200(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;)Ljava/lang/Object;
    .locals 0

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;Lcom/squareup/shared/catalog/TypedCursor;)Ljava/util/List;
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->filterUnsupportedItems(Lcom/squareup/shared/catalog/TypedCursor;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->filterScreenResult(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;)Ljava/lang/Object;
    .locals 0

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private applyTax(Lcom/squareup/shared/catalog/Related;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;)V"
        }
    .end annotation

    .line 130
    iget-object v0, p1, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v0

    .line 131
    iget-boolean v1, p1, Lcom/squareup/shared/catalog/Related;->related:Z

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->isNewCheckedState(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    iget-object p1, p1, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->isService(Lcom/squareup/shared/catalog/models/CatalogItem;)Z

    move-result p1

    invoke-virtual {v1, v0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->apply(Ljava/lang/String;Z)V

    .line 137
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->refreshView()V

    return-void
.end method

.method private exemptTax(Lcom/squareup/shared/catalog/Related;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;)V"
        }
    .end annotation

    .line 141
    iget-object v0, p1, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v0

    .line 142
    iget-boolean v1, p1, Lcom/squareup/shared/catalog/Related;->related:Z

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->isNewCheckedState(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 145
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    iget-boolean v2, p1, Lcom/squareup/shared/catalog/Related;->related:Z

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/squareup/shared/catalog/Related;->relation:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    iget-object p1, p1, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->isService(Lcom/squareup/shared/catalog/models/CatalogItem;)Z

    move-result p1

    invoke-virtual {v1, v0, v2, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->exempt(Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;Z)V

    .line 148
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->refreshView()V

    return-void
.end method

.method private filterScreenResult(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;"
        }
    .end annotation

    .line 190
    new-instance v0, Ljava/util/ArrayList;

    .line 191
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 193
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/Related;

    .line 194
    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->taxScopeRunner:Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->isApplicableDisplayForService()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 195
    iget-object v2, v1, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v2

    sget-object v3, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne v2, v3, :cond_0

    .line 196
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 199
    :cond_1
    iget-object v2, v1, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v2

    sget-object v3, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-eq v2, v3, :cond_0

    .line 200
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method private filterUnsupportedItems(Lcom/squareup/shared/catalog/TypedCursor;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/TypedCursor<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;)",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;"
        }
    .end annotation

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_2

    .line 173
    :try_start_0
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/squareup/api/items/Item$Type;

    const/4 v3, 0x0

    sget-object v4, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v1

    .line 174
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/TypedCursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 175
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/TypedCursor;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/shared/catalog/Related;

    .line 176
    iget-object v3, v2, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v3, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v3}, Lcom/squareup/shared/catalog/models/CatalogItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 177
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 181
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/TypedCursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/TypedCursor;->close()V

    .line 182
    throw v0

    :cond_2
    :goto_1
    return-object v0
.end method

.method private isNewCheckedState(Ljava/lang/String;Z)Z
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getAppliedState(Ljava/lang/String;Z)Lcom/squareup/util/MaybeBoolean;

    move-result-object p1

    .line 154
    invoke-virtual {p1}, Lcom/squareup/util/MaybeBoolean;->isKnown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    invoke-virtual {p1}, Lcom/squareup/util/MaybeBoolean;->booleanValue()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1

    :cond_0
    xor-int/lit8 p1, p2, 0x1

    return p1
.end method

.method private isService(Lcom/squareup/shared/catalog/models/CatalogItem;)Z
    .locals 1

    .line 233
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getItemType()Lcom/squareup/api/items/Item$Type;

    move-result-object p1

    sget-object v0, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method static synthetic lambda$onLoad$0(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/TypedCursor;
    .locals 0

    .line 84
    invoke-interface {p1, p0}, Lcom/squareup/shared/catalog/Catalog$Local;->findTaxItemRelations(Ljava/lang/String;)Lcom/squareup/shared/catalog/TypedCursor;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method applyTaxAll(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;)V"
        }
    .end annotation

    .line 118
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/Related;

    .line 119
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->applyTax(Lcom/squareup/shared/catalog/Related;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method exemptTaxAll(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;>;)V"
        }
    .end annotation

    .line 124
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/Related;

    .line 125
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->exemptTax(Lcom/squareup/shared/catalog/Related;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method getTaxName(Lcom/squareup/shared/catalog/Related;)Ljava/lang/String;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 92
    iget-object p1, p1, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method isTaxApplied(Lcom/squareup/shared/catalog/Related;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;)Z"
        }
    .end annotation

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    iget-object v1, p1, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p1, Lcom/squareup/shared/catalog/Related;->related:Z

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getAppliedState(Ljava/lang/String;Z)Lcom/squareup/util/MaybeBoolean;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/squareup/util/MaybeBoolean;->isKnown()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    invoke-virtual {v0}, Lcom/squareup/util/MaybeBoolean;->booleanValue()Z

    move-result p1

    return p1

    .line 100
    :cond_0
    iget-boolean p1, p1, Lcom/squareup/shared/catalog/Related;->related:Z

    return p1
.end method

.method onBackPressed()Z
    .locals 1

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->itemRelationshipCallback:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;

    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;->cancel()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 71
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->taxScopeRunner:Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;->isApplicableDisplayForService()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$string;->tax_applicable_services:I

    .line 73
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$string;->tax_applicable_items:I

    .line 74
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 76
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 77
    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    .line 78
    invoke-virtual {p1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 79
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 80
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 76
    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getId()Ljava/lang/String;

    move-result-object p1

    .line 83
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;-><init>(Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$1;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->itemRelationshipCallback:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->lazyCogs:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxApplicableScreen$Presenter$pxZXVKhnPxgj8an1Xzl038ftuFY;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxApplicableScreen$Presenter$pxZXVKhnPxgj8an1Xzl038ftuFY;-><init>(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->itemRelationshipCallback:Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter$ItemRelationshipCallback;

    invoke-interface {v0, v1, p1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    return-void
.end method

.method onProgressHidden()V
    .locals 1

    .line 88
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->showContent()V

    return-void
.end method

.method onRowClicked(Lcom/squareup/shared/catalog/Related;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Related<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            "Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;",
            ">;)V"
        }
    .end annotation

    .line 105
    iget-object v0, p1, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getId()Ljava/lang/String;

    move-result-object v0

    .line 106
    iget-boolean v1, p1, Lcom/squareup/shared/catalog/Related;->related:Z

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->isNewCheckedState(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    iget-object p1, p1, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->isService(Lcom/squareup/shared/catalog/models/CatalogItem;)Z

    move-result p1

    invoke-virtual {v1, v0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->apply(Ljava/lang/String;Z)V

    goto :goto_1

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    iget-boolean v2, p1, Lcom/squareup/shared/catalog/Related;->related:Z

    if-eqz v2, :cond_1

    iget-object v2, p1, Lcom/squareup/shared/catalog/Related;->relation:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast v2, Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    iget-object p1, p1, Lcom/squareup/shared/catalog/Related;->object:Lcom/squareup/shared/catalog/models/CatalogObject;

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->isService(Lcom/squareup/shared/catalog/models/CatalogItem;)Z

    move-result p1

    invoke-virtual {v1, v0, v2, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->exempt(Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogItemFeeMembership;Z)V

    .line 114
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxApplicableView;->refreshView()V

    return-void
.end method
