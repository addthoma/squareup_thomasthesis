.class public final Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;
.super Ljava/lang/Object;
.source "SettingsAppletDeepLinkHandler_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;",
        ">;"
    }
.end annotation


# instance fields
.field private final bankAccountSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection;",
            ">;"
        }
    .end annotation
.end field

.field private final depositsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final quickAmountsSectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;->depositsSectionProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;->bankAccountSectionProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;->quickAmountsSectionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;",
            ">;)",
            "Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/ui/settings/instantdeposits/DepositsSection;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;)Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;
    .locals 1

    .line 53
    new-instance v0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/ui/settings/instantdeposits/DepositsSection;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;->depositsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    iget-object v2, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;->bankAccountSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/settings/bankaccount/BankAccountSection;

    iget-object v3, p0, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;->quickAmountsSectionProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/ui/settings/instantdeposits/DepositsSection;Lcom/squareup/ui/settings/bankaccount/BankAccountSection;Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection;)Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler_Factory;->get()Lcom/squareup/ui/settings/SettingsAppletDeepLinkHandler;

    move-result-object v0

    return-object v0
.end method
