.class public final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;
.super Ljava/lang/Object;
.source "GiftCardsSettingsScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final eGiftCardImageUploadHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardServiceHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCardServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardsSettingsHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final maybeCameraHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final uploadSpinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCardServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)V"
        }
    .end annotation

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->giftCardServiceHelperProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->giftCardsSettingsHelperProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p4, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->eGiftCardImageUploadHelperProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p5, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p6, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p7, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->maybeCameraHelperProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p8, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->uploadSpinnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/giftcard/GiftCardServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;"
        }
    .end annotation

    .line 67
    new-instance v9, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Lio/reactivex/Scheduler;Lcom/squareup/util/Device;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;
    .locals 10

    .line 74
    new-instance v9, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;-><init>(Lflow/Flow;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Lio/reactivex/Scheduler;Lcom/squareup/util/Device;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;
    .locals 9

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->giftCardServiceHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/giftcard/GiftCardServiceHelper;

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->giftCardsSettingsHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->eGiftCardImageUploadHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->maybeCameraHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/camerahelper/CameraHelper;

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->uploadSpinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/register/widgets/GlassSpinner;

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Lio/reactivex/Scheduler;Lcom/squareup/util/Device;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner_Factory;->get()Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    move-result-object v0

    return-object v0
.end method
