.class public interface abstract Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;
.super Ljava/lang/Object;
.source "ViewDesignsSettingsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0008\u0010\u0008\u001a\u00020\u0003H&J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH&J\u000e\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\rH&J\u0008\u0010\u000f\u001a\u00020\u0003H&\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/ViewDesignsSettingsScreen$Runner;",
        "",
        "disableTheme",
        "",
        "oldState",
        "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
        "theme",
        "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
        "exitViewDesignsSettings",
        "gridColumnCount",
        "Lio/reactivex/Single;",
        "",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/settings/giftcards/ScreenData;",
        "showAddDesigns",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract disableTheme(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Lcom/squareup/protos/client/giftcards/EGiftTheme;)V
.end method

.method public abstract exitViewDesignsSettings()V
.end method

.method public abstract gridColumnCount()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end method

.method public abstract screenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/giftcards/ScreenData;",
            ">;"
        }
    .end annotation
.end method

.method public abstract showAddDesigns()V
.end method
