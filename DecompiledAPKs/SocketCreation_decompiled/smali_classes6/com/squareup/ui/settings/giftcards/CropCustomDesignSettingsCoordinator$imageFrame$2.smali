.class final Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$imageFrame$2;
.super Lkotlin/jvm/internal/Lambda;
.source "CropCustomDesignSettingsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;-><init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;Lcom/squareup/picasso/Picasso;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$imageFrame$2;->this$0:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$imageFrame$2;->this$0:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->access$getView$p(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/squareup/settingsapplet/R$id;->egiftcard_designs_image_frame:I

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$imageFrame$2;->invoke()Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;

    move-result-object v0

    return-object v0
.end method
