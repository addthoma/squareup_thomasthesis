.class public final Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory;
.super Ljava/lang/Object;
.source "UploadCustomDesignDialogScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUploadCustomDesignDialogScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UploadCustomDesignDialogScreen.kt\ncom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,66:1\n52#2:67\n*E\n*S KotlinDebug\n*F\n+ 1 UploadCustomDesignDialogScreen.kt\ncom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory\n*L\n24#1:67\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0018\u0010\u0008\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\nH\u0002\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "data",
        "Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory;Landroid/content/Context;Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;)Landroid/app/Dialog;
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory;->createDialog(Landroid/content/Context;Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;)Landroid/app/Dialog;

    move-result-object p0

    return-object p0
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;)Landroid/app/Dialog;
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 39
    invoke-virtual {p2}, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;->getSuccess()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 40
    sget p1, Lcom/squareup/settingsapplet/R$string;->giftcards_settings_egiftcard_designs_upload_custom_success:I

    goto :goto_0

    .line 42
    :cond_0
    sget p1, Lcom/squareup/settingsapplet/R$string;->giftcards_settings_egiftcard_designs_upload_custom_failed:I

    .line 38
    :goto_0
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const-string p2, ""

    .line 45
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 46
    sget p2, Lcom/squareup/common/strings/R$string;->ok:I

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 47
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026g.ok)\n          .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/app/Dialog;

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    const-class v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;

    .line 25
    invoke-interface {v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;->scopeRunner()Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->uploadCustomDesignDialogScreenData()Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v1, 0x1

    .line 28
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object v0

    .line 30
    new-instance v1, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory$create$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory$create$1;-><init>(Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$Factory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "runner.uploadCustomDesig\u2026eateDialog(context, it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
