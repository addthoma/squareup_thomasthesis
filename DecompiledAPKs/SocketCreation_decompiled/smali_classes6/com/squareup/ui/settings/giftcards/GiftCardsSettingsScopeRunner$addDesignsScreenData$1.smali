.class final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$addDesignsScreenData$1;
.super Ljava/lang/Object;
.source "GiftCardsSettingsScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->addDesignsScreenData()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGiftCardsSettingsScopeRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GiftCardsSettingsScopeRunner.kt\ncom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$addDesignsScreenData$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,285:1\n704#2:286\n777#2,2:287\n*E\n*S KotlinDebug\n*F\n+ 1 GiftCardsSettingsScopeRunner.kt\ncom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$addDesignsScreenData$1\n*L\n177#1:286\n177#1,2:287\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012.\u0010\u0002\u001a*\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0007 \u0005*\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00060\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
        "kotlin.jvm.PlatformType",
        "",
        "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$addDesignsScreenData$1;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;)Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;>;)",
            "Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 176
    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getAll_egift_themes()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/lang/Iterable;

    .line 286
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/Collection;

    .line 287
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/squareup/protos/client/giftcards/EGiftTheme;

    .line 177
    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->getOrder_configuration()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    move-result-object v5

    if-nez v5, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    iget-object v5, v5, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;->enabled_theme_tokens:Ljava/util/List;

    iget-object v4, v4, Lcom/squareup/protos/client/giftcards/EGiftTheme;->read_only_token:Ljava/lang/String;

    invoke-interface {v5, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_0

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 288
    :cond_2
    check-cast v2, Ljava/util/List;

    .line 178
    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$addDesignsScreenData$1;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-static {v1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->access$getMaybeCameraHelper$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/squareup/camerahelper/CameraHelper;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/camerahelper/CameraHelper;->isCameraOrGalleryAvailable()Z

    move-result v1

    const-string v3, "settings"

    .line 179
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "themesToBeAdded1"

    .line 180
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    new-instance v3, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;

    invoke-direct {v3, v2, v1, v0, p1}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;-><init>(Ljava/util/List;ZLcom/squareup/ui/settings/giftcards/ScreenData$Settings;Ljava/util/List;)V

    return-object v3
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$addDesignsScreenData$1;->apply(Lkotlin/Pair;)Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$AddDesignsScreenData;

    move-result-object p1

    return-object p1
.end method
