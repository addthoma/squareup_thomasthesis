.class Lcom/squareup/ui/settings/SettingsSectionsPresenter$2;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "SettingsSectionsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/SettingsSectionsPresenter;->onSectionClicked(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

.field final synthetic val$selectedEntry:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionsPresenter;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;)V
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter$2;->this$0:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    iput-object p2, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter$2;->val$selectedEntry:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 3

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter$2;->this$0:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    iget-object v1, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter$2;->val$selectedEntry:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/applet/AppletSection;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->access$100(Lcom/squareup/ui/settings/SettingsSectionsPresenter;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method
