.class public Lcom/squareup/ui/settings/SettingsAppletScopeRunner;
.super Ljava/lang/Object;
.source "SettingsAppletScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final analytics:Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;

.field private final applet:Lcom/squareup/ui/settings/SettingsApplet;

.field private final flow:Lflow/Flow;

.field private previewSelectMethodWorkflowRunner:Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;

.field private final settings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

.field private final settingsAppletServices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation
.end field

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/SettingsApplet;Lflow/Flow;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/SettingsApplet;",
            "Lflow/Flow;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->applet:Lcom/squareup/ui/settings/SettingsApplet;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->flow:Lflow/Flow;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->settings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    .line 38
    iput-object p5, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->analytics:Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;

    .line 39
    iput-object p6, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->settingsAppletServices:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public changeTileAppearanceToImage(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 60
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->settings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    sget-object v0, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;->IMAGE:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->set(Lcom/squareup/ui/settings/tiles/TileAppearanceSettings$TileType;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    .line 62
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->analytics:Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;->imageTileSelected()V

    goto :goto_0

    .line 64
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->analytics:Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/tiles/TileAppearanceAnalytics;->confirmationDialogCanceled()V

    :goto_0
    return-void
.end method

.method public confirmTileAppearanceChangedToImage()V
    .locals 3

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/settings/tiles/ConfirmTileAppearanceChangedToImageDialogScreen;

    invoke-direct {v1}, Lcom/squareup/ui/settings/tiles/ConfirmTileAppearanceChangedToImageDialogScreen;-><init>()V

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->settingsAppletServices:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmortar/Scoped;

    .line 44
    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    goto :goto_0

    .line 47
    :cond_0
    invoke-static {p1}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->get(Lmortar/MortarScope;)Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->previewSelectMethodWorkflowRunner:Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;

    .line 48
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->applet:Lcom/squareup/ui/settings/SettingsApplet;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsApplet;->select()V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public showPreviewSelectMethodWorkflow(Lcom/squareup/protos/client/devicesettings/TenderSettings;)V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsAppletScopeRunner;->previewSelectMethodWorkflowRunner:Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;->start(Lcom/squareup/protos/client/devicesettings/TenderSettings;)V

    return-void
.end method
