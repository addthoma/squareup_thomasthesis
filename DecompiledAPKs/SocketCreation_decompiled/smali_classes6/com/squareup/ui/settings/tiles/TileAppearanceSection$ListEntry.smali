.class public Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.source "TileAppearanceSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/tiles/TileAppearanceSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation


# instance fields
.field private final tileSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/tiles/TileAppearanceSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    sget-object v2, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->CHECKOUT_OPTIONS:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    sget v3, Lcom/squareup/ui/settings/tiles/TileAppearanceSection;->TITLE_ID:I

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V

    .line 32
    iput-object p4, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;->tileSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    return-void
.end method


# virtual methods
.method public getValueText()Ljava/lang/String;
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;->tileSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/settingsapplet/R$string;->item_appeareance_option_text:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/fileviewer/R$string;->item_appeareance_option_image:I

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
