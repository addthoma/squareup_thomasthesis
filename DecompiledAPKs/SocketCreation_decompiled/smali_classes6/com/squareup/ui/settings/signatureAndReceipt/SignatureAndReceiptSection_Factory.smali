.class public final Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection_Factory;
.super Ljava/lang/Object;
.source "SignatureAndReceiptSection_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;",
        ">;"
    }
.end annotation


# instance fields
.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection_Factory;->skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/settings/server/Features;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection_Factory;->skipReceiptScreenSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    iget-object v2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection_Factory;->get()Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;

    move-result-object v0

    return-object v0
.end method
