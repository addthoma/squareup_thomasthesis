.class public interface abstract Lcom/squareup/ui/settings/SettingsAppletScope$ParentComponent;
.super Ljava/lang/Object;
.source "SettingsAppletScope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/SettingsAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ParentComponent"
.end annotation


# virtual methods
.method public abstract previewSelectMethodWorkflowRunner()Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner;
.end method

.method public abstract settingsAppletScopePhone()Lcom/squareup/ui/settings/SettingsAppletScope$PhoneComponent;
.end method

.method public abstract settingsAppletScopeTablet()Lcom/squareup/ui/settings/SettingsAppletScope$TabletComponent;
.end method
