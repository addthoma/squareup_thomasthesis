.class public Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "BarcodeScannersSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

.field private connectionListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/barcodescanners/BarcodeScannerTracker;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 50
    iput-object p2, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 51
    iput-object p3, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    .line 52
    new-instance p1, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter$1;-><init>(Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->connectionListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->refreshView()V

    return-void
.end method

.method private refreshView()V
    .locals 2

    .line 89
    invoke-virtual {p0}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsView;

    iget-object v1, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    invoke-virtual {v1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->getAvailableBarcodeScanners()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsView;->updateView(Ljava/util/List;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public getActionbarText()Ljava/lang/String;
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection;->TITLE_ID:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v0, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->connectionListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;

    invoke-virtual {p1, v0}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->addConnectionListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 2

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->barcodeScannerTracker:Lcom/squareup/barcodescanners/BarcodeScannerTracker;

    iget-object v1, p0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->connectionListener:Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;

    invoke-virtual {v0, v1}, Lcom/squareup/barcodescanners/BarcodeScannerTracker;->removeConnectionListener(Lcom/squareup/barcodescanners/BarcodeScannerTracker$ConnectionListener;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 64
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 65
    invoke-direct {p0}, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Presenter;->refreshView()V

    return-void
.end method

.method protected saveSettings()V
    .locals 0

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 77
    const-class v0, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen;

    return-object v0
.end method
