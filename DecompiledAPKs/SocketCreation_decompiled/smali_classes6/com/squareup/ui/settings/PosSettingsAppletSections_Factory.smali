.class public final Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;
.super Ljava/lang/Object;
.source "PosSettingsAppletSections_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/PosSettingsAppletSections;",
        ">;"
    }
.end annotation


# instance fields
.field private final bankAccountEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final barcodeScannersEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReadersEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$CardReaderHardwareListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawerEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final cashManagementEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/crm/CustomerManagementSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final depositsEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final emailCollectionEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/crm/EmailCollectionSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final giftCardsEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/offline/OfflineSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final onlineCheckoutEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubNotificationsEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubPrintingEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubQuickActionsEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodesSettingsEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentTypesEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final publicProfileEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final quickAmountsEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final scalesEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final sharedSettingsEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$SharedSettingsEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final signatureAndReceiptEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final swipeChipCardsEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$SwipeChipCardsCheckoutEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final taxesEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/TaxesSection$TaxesCheckoutListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final tileAppearanceEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final timeTrackingSettingsEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$ListEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final tippingEntryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tipping/TippingSection$TippingCheckoutListEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$SharedSettingsEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/TaxesSection$TaxesCheckoutListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tipping/TippingSection$TippingCheckoutListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/offline/OfflineSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$SwipeChipCardsCheckoutEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/crm/CustomerManagementSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/crm/EmailCollectionSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$CardReaderHardwareListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection$ListEntry;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 141
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->sharedSettingsEntryProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 142
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->paymentTypesEntryProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 143
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->taxesEntryProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 144
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->signatureAndReceiptEntryProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 145
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->quickAmountsEntryProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 146
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->tippingEntryProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 147
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->cashManagementEntryProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 148
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->offlineEntryProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 149
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->employeeManagementEntryProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 150
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->openTicketsEntryProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 151
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->swipeChipCardsEntryProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 152
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->customerManagementEntryProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 153
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->emailCollectionEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 154
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->loyaltyEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 155
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->giftCardsEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 156
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->onlineCheckoutEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 157
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->tileAppearanceEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 158
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->passcodesSettingsEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 159
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->timeTrackingSettingsEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 160
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->cardReadersEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 161
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->printerStationsEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 162
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->cashDrawerEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 163
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->barcodeScannersEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 164
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->scalesEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 165
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->deviceEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 166
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->orderHubNotificationsEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 167
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->orderHubPrintingEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 168
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->orderHubQuickActionsEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 169
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->publicProfileEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 170
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->bankAccountEntryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p31

    .line 171
    iput-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->depositsEntryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;
    .locals 33
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$SharedSettingsEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/taxes/TaxesSection$TaxesCheckoutListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tipping/TippingSection$TippingCheckoutListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/offline/OfflineSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$SwipeChipCardsCheckoutEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/crm/CustomerManagementSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/crm/EmailCollectionSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/passcodes/PasscodesSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$CardReaderHardwareListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/bankaccount/BankAccountSection$ListEntry;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/instantdeposits/DepositsSection$ListEntry;",
            ">;)",
            "Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    .line 211
    new-instance v32, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;

    move-object/from16 v0, v32

    invoke-direct/range {v0 .. v31}, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v32
.end method

.method public static newInstance(Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$SharedSettingsEntry;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;Lcom/squareup/ui/settings/taxes/TaxesSection$TaxesCheckoutListEntry;Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;Lcom/squareup/ui/settings/tipping/TippingSection$TippingCheckoutListEntry;Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;Lcom/squareup/ui/settings/offline/OfflineSection$ListEntry;Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$ListEntry;Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$ListEntry;Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$SwipeChipCardsCheckoutEntry;Lcom/squareup/ui/settings/crm/CustomerManagementSection$ListEntry;Lcom/squareup/ui/settings/crm/EmailCollectionSection$ListEntry;Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$ListEntry;Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection$ListEntry;Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;Lcom/squareup/ui/settings/passcodes/PasscodesSection$ListEntry;Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$ListEntry;Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$CardReaderHardwareListEntry;Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection$ListEntry;Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection$ListEntry;Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$ListEntry;Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;Lcom/squareup/ui/settings/bankaccount/BankAccountSection$ListEntry;Lcom/squareup/ui/settings/instantdeposits/DepositsSection$ListEntry;)Lcom/squareup/ui/settings/PosSettingsAppletSections;
    .locals 33

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    .line 243
    new-instance v32, Lcom/squareup/ui/settings/PosSettingsAppletSections;

    move-object/from16 v0, v32

    invoke-direct/range {v0 .. v31}, Lcom/squareup/ui/settings/PosSettingsAppletSections;-><init>(Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$SharedSettingsEntry;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;Lcom/squareup/ui/settings/taxes/TaxesSection$TaxesCheckoutListEntry;Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;Lcom/squareup/ui/settings/tipping/TippingSection$TippingCheckoutListEntry;Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;Lcom/squareup/ui/settings/offline/OfflineSection$ListEntry;Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$ListEntry;Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$ListEntry;Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$SwipeChipCardsCheckoutEntry;Lcom/squareup/ui/settings/crm/CustomerManagementSection$ListEntry;Lcom/squareup/ui/settings/crm/EmailCollectionSection$ListEntry;Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$ListEntry;Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection$ListEntry;Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;Lcom/squareup/ui/settings/passcodes/PasscodesSection$ListEntry;Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$ListEntry;Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$CardReaderHardwareListEntry;Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection$ListEntry;Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection$ListEntry;Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$ListEntry;Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;Lcom/squareup/ui/settings/bankaccount/BankAccountSection$ListEntry;Lcom/squareup/ui/settings/instantdeposits/DepositsSection$ListEntry;)V

    return-object v32
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/PosSettingsAppletSections;
    .locals 33

    move-object/from16 v0, p0

    .line 176
    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->sharedSettingsEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$SharedSettingsEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->paymentTypesEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->taxesEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/ui/settings/taxes/TaxesSection$TaxesCheckoutListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->signatureAndReceiptEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->quickAmountsEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->tippingEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/settings/tipping/TippingSection$TippingCheckoutListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->cashManagementEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->offlineEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/settings/offline/OfflineSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->employeeManagementEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->openTicketsEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->swipeChipCardsEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$SwipeChipCardsCheckoutEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->customerManagementEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/ui/settings/crm/CustomerManagementSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->emailCollectionEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/ui/settings/crm/EmailCollectionSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->loyaltyEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->giftCardsEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->onlineCheckoutEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->tileAppearanceEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->passcodesSettingsEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/ui/settings/passcodes/PasscodesSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->timeTrackingSettingsEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->cardReadersEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$CardReaderHardwareListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->printerStationsEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->cashDrawerEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->barcodeScannersEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->scalesEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->deviceEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->orderHubNotificationsEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->orderHubPrintingEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->orderHubQuickActionsEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->publicProfileEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->bankAccountEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v31, v1

    check-cast v31, Lcom/squareup/ui/settings/bankaccount/BankAccountSection$ListEntry;

    iget-object v1, v0, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->depositsEntryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v32, v1

    check-cast v32, Lcom/squareup/ui/settings/instantdeposits/DepositsSection$ListEntry;

    invoke-static/range {v2 .. v32}, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->newInstance(Lcom/squareup/ui/settings/sharedsettings/SharedSettingsSection$SharedSettingsEntry;Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsSection$PaymentTypesSettingsCheckoutListEntry;Lcom/squareup/ui/settings/taxes/TaxesSection$TaxesCheckoutListEntry;Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection$ListEntry;Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsSection$ListEntry;Lcom/squareup/ui/settings/tipping/TippingSection$TippingCheckoutListEntry;Lcom/squareup/ui/settings/cashmanagement/CashManagementSection$ListEntry;Lcom/squareup/ui/settings/offline/OfflineSection$ListEntry;Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSection$ListEntry;Lcom/squareup/ui/settings/opentickets/OpenTicketsSection$ListEntry;Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSection$SwipeChipCardsCheckoutEntry;Lcom/squareup/ui/settings/crm/CustomerManagementSection$ListEntry;Lcom/squareup/ui/settings/crm/EmailCollectionSection$ListEntry;Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$ListEntry;Lcom/squareup/ui/settings/onlinecheckout/OnlineCheckoutSettingsSection$ListEntry;Lcom/squareup/ui/settings/tiles/TileAppearanceSection$ListEntry;Lcom/squareup/ui/settings/passcodes/PasscodesSection$ListEntry;Lcom/squareup/ui/settings/timetracking/TimeTrackingSection$ListEntry;Lcom/squareup/ui/settings/paymentdevices/CardReadersSection$CardReaderHardwareListEntry;Lcom/squareup/ui/settings/printerstations/PrinterStationsSection$ListEntry;Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSection$ListEntry;Lcom/squareup/ui/settings/scales/ScalesSettingsSection$ListEntry;Lcom/squareup/ui/settings/devicename/DeviceSection$ListEntry;Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsSection$ListEntry;Lcom/squareup/ui/settings/orderhub/OrderHubPrintingSettingsSection$ListEntry;Lcom/squareup/ui/settings/orderhub/OrderHubQuickActionsSettingsSection$ListEntry;Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;Lcom/squareup/ui/settings/bankaccount/BankAccountSection$ListEntry;Lcom/squareup/ui/settings/instantdeposits/DepositsSection$ListEntry;)Lcom/squareup/ui/settings/PosSettingsAppletSections;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/ui/settings/PosSettingsAppletSections_Factory;->get()Lcom/squareup/ui/settings/PosSettingsAppletSections;

    move-result-object v0

    return-object v0
.end method
