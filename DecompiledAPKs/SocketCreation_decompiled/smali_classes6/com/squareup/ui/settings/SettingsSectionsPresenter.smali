.class public Lcom/squareup/ui/settings/SettingsSectionsPresenter;
.super Lmortar/ViewPresenter;
.source "SettingsSectionsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/SettingsSectionsPresenter$SharedScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/settings/SettingsSectionsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

.field private final appletSelection:Lcom/squareup/applet/AppletSelection;

.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final badgePresenter:Lcom/squareup/applet/BadgePresenter;

.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

.field private final bannerButtonResolver:Lcom/squareup/onboarding/BannerButtonResolver;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final deepLinks:Lcom/squareup/ui/main/DeepLinks;

.field private final device:Lcom/squareup/util/Device;

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final listRefresher:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final settingsSections:Lcom/squareup/ui/settings/SettingsAppletSectionsList;

.field private final signOutRunner:Lcom/squareup/signout/SignOutRunner;

.field private final starter:Lcom/squareup/onboarding/OnboardingStarter;

.field private visibleSections:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/applet/AppletSectionsListEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsAppletSectionsList;Lflow/Flow;Lcom/squareup/util/Device;Lcom/squareup/badbus/BadBus;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;Ljavax/inject/Provider;Lcom/squareup/onboarding/OnboardingStarter;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lcom/squareup/signout/SignOutRunner;Lcom/squareup/onboarding/BannerButtonResolver;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/SettingsAppletSectionsList;",
            "Lflow/Flow;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/onboarding/OnboardingStarter;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/permissions/EmployeeManagement;",
            "Lcom/squareup/applet/BadgePresenter;",
            "Lcom/squareup/ui/main/DeepLinks;",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            "Lcom/squareup/applet/AppletSelection;",
            "Lcom/squareup/signout/SignOutRunner;",
            "Lcom/squareup/onboarding/BannerButtonResolver;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 97
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 85
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->visibleSections:Ljava/util/List;

    move-object v1, p4

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->badBus:Lcom/squareup/badbus/BadBus;

    move-object v1, p2

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->flow:Lflow/Flow;

    move-object v1, p3

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->device:Lcom/squareup/util/Device;

    move-object v1, p1

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->settingsSections:Lcom/squareup/ui/settings/SettingsAppletSectionsList;

    move-object v1, p5

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 103
    invoke-virtual {p6}, Lcom/squareup/ui/settings/SidebarRefresher;->onRefreshed()Lio/reactivex/Observable;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->listRefresher:Lio/reactivex/Observable;

    move-object v1, p7

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->localeProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->starter:Lcom/squareup/onboarding/OnboardingStarter;

    move-object v1, p9

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object v1, p10

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    move-object v1, p11

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    move-object v1, p12

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->deepLinks:Lcom/squareup/ui/main/DeepLinks;

    move-object/from16 v1, p13

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    move-object/from16 v1, p15

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->appletSelection:Lcom/squareup/applet/AppletSelection;

    move-object/from16 v1, p14

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    move-object/from16 v1, p16

    .line 113
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->signOutRunner:Lcom/squareup/signout/SignOutRunner;

    move-object/from16 v1, p17

    .line 114
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->bannerButtonResolver:Lcom/squareup/onboarding/BannerButtonResolver;

    move-object/from16 v1, p18

    .line 115
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    move-object/from16 v1, p19

    .line 116
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->res:Lcom/squareup/util/Res;

    move-object/from16 v1, p20

    .line 117
    iput-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->features:Lcom/squareup/settings/server/Features;

    .line 119
    iget-object v1, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->visibleSections:Ljava/util/List;

    iget-object v2, v0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->settingsSections:Lcom/squareup/ui/settings/SettingsAppletSectionsList;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/SettingsAppletSectionsList;->getVisibleEntries()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/SettingsSectionsPresenter;)Lcom/squareup/applet/BadgePresenter;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/SettingsSectionsPresenter;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->goOrReplaceTo(Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/settings/SettingsSectionsPresenter;)Lcom/squareup/onboarding/OnboardingStarter;
    .locals 0

    .line 58
    iget-object p0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->starter:Lcom/squareup/onboarding/OnboardingStarter;

    return-object p0
.end method

.method private goOrReplaceTo(Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;Lcom/squareup/container/ContainerTreeKey;)V
    .locals 1

    .line 320
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->settingsSections:Lcom/squareup/ui/settings/SettingsAppletSectionsList;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsList;->setSelectedSection(Lcom/squareup/applet/AppletSection;)V

    .line 322
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 323
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->flow:Lflow/Flow;

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 325
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->flow:Lflow/Flow;

    invoke-static {p1, p2}, Lcom/squareup/container/Flows;->replaceTop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;)V

    .line 327
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/SettingsSectionsView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionsView;->updateList()V

    :goto_0
    return-void
.end method

.method static synthetic lambda$null$7(Lcom/squareup/ui/settings/SettingsSectionsView;Lcom/squareup/banklinking/BankAccountSettings$State;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 170
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionsView;->updateList()V

    return-void
.end method

.method private onFeesUpdate()V
    .locals 1

    .line 294
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    invoke-direct {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->rebuildSectionsAndRefresh()V

    :cond_0
    return-void
.end method

.method private onUsbDevicesChanged()V
    .locals 1

    .line 300
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    invoke-direct {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->rebuildSectionsAndRefreshIfSidebar()V

    :cond_0
    return-void
.end method

.method private rebuildSectionsAndRefresh()V
    .locals 2

    .line 313
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->visibleSections:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 314
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->visibleSections:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->settingsSections:Lcom/squareup/ui/settings/SettingsAppletSectionsList;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/SettingsAppletSectionsList;->getVisibleEntries()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 315
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/SettingsSectionsView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsSectionsView;->updateList()V

    return-void
.end method

.method private rebuildSectionsAndRefreshIfSidebar()V
    .locals 1

    .line 307
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-nez v0, :cond_0

    .line 308
    invoke-direct {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->rebuildSectionsAndRefresh()V

    :cond_0
    return-void
.end method

.method private snapToSelectedSection()V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 333
    :goto_0
    iget-object v2, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->visibleSections:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 334
    iget-object v2, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->visibleSections:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/applet/AppletSectionsListEntry;

    invoke-virtual {v2}, Lcom/squareup/applet/AppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->settingsSections:Lcom/squareup/ui/settings/SettingsAppletSectionsList;

    invoke-virtual {v3}, Lcom/squareup/ui/settings/SettingsAppletSectionsList;->getLastSelectedSection()Lcom/squareup/applet/AppletSection;

    move-result-object v3

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 339
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/SettingsSectionsView;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/settings/SettingsSectionsView;->scrollToSection(I)V

    return-void
.end method


# virtual methods
.method getBannerButton()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/onboarding/BannerButton;",
            ">;"
        }
    .end annotation

    .line 281
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->NOTIFICATION_CENTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    new-instance v0, Lcom/squareup/onboarding/BannerButton;

    sget-object v1, Lcom/squareup/onboarding/BannerButton$Kind;->NONE:Lcom/squareup/onboarding/BannerButton$Kind;

    invoke-direct {v0, v1}, Lcom/squareup/onboarding/BannerButton;-><init>(Lcom/squareup/onboarding/BannerButton$Kind;)V

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->bannerButtonResolver:Lcom/squareup/onboarding/BannerButtonResolver;

    invoke-virtual {v0}, Lcom/squareup/onboarding/BannerButtonResolver;->bannerButton()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method getEntry(I)Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
    .locals 1

    .line 290
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->visibleSections:Ljava/util/List;

    add-int/lit8 p1, p1, -0x1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    return-object p1
.end method

.method getHeaderForSectionAtPosition(I)Ljava/lang/String;
    .locals 0

    .line 247
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getEntry(I)Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getGroupingTitle()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getSectionCount()I
    .locals 1

    .line 219
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->visibleSections:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method getSectionGroupingTitleIdAtPosition(I)J
    .locals 2

    .line 251
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getEntry(I)Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getGroupingTitle()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method getSectionName(I)Ljava/lang/String;
    .locals 0

    .line 227
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getEntry(I)Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getTitleText()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getSectionValue(I)Lio/reactivex/Observable;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 235
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getEntry(I)Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getValueTextObservable()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method getSectionValueShortText(I)Ljava/lang/String;
    .locals 0

    .line 243
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getEntry(I)Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getShortValueText()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method getSignOutHeaderText()Ljava/lang/String;
    .locals 2

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getSubunitName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 261
    iget-object v1, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getEmail()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method isDevelopmentRow(I)Z
    .locals 0

    .line 231
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getEntry(I)Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/AppletSection;->isDevSection()Z

    move-result p1

    return p1
.end method

.method isEditable(Lcom/squareup/applet/AppletSection;)Z
    .locals 1

    .line 214
    invoke-virtual {p1}, Lcom/squareup/applet/AppletSection;->isRestricted()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 215
    invoke-virtual {p1}, Lcom/squareup/applet/AppletSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/applet/SectionAccess;->getPermissions()Ljava/util/Set;

    move-result-object p1

    .line 214
    invoke-interface {v0, p1}, Lcom/squareup/permissions/EmployeeManagement;->hasAnyPermission(Ljava/util/Set;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method isSectionSelected(I)Z
    .locals 1

    .line 223
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getEntry(I)Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->settingsSections:Lcom/squareup/ui/settings/SettingsAppletSectionsList;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsAppletSectionsList;->getLastSelectedSection()Lcom/squareup/applet/AppletSection;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method isSectionValueABadge(I)Z
    .locals 0

    .line 239
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getEntry(I)Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->hasBadgeValue()Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$null$3$SettingsSectionsPresenter(Lcom/squareup/marin/widgets/ActionBarView;Lcom/squareup/applet/Applet;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    .line 146
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/registerlib/R$string;->settings_title:I

    .line 147
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 145
    invoke-interface {v0, p1, v1, p2}, Lcom/squareup/applet/ActionBarNavigationHelper;->makeActionBarForAppletActivationScreen(Lcom/squareup/marin/widgets/MarinActionBar;Ljava/lang/String;Lcom/squareup/applet/Applet;)V

    return-void
.end method

.method public synthetic lambda$null$5$SettingsSectionsPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 166
    invoke-direct {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->rebuildSectionsAndRefreshIfSidebar()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$0$SettingsSectionsPresenter(Lcom/squareup/settings/server/FeesUpdate;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 124
    invoke-direct {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->onFeesUpdate()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$1$SettingsSectionsPresenter(Lcom/squareup/hardware/UsbDevicesChangedEvent;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 126
    invoke-direct {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->onUsbDevicesChanged()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$SettingsSectionsPresenter(Lkotlin/Unit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 131
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 133
    invoke-direct {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->rebuildSectionsAndRefresh()V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onLoad$4$SettingsSectionsPresenter(Lcom/squareup/marin/widgets/ActionBarView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->appletSelection:Lcom/squareup/applet/AppletSelection;

    invoke-interface {v0}, Lcom/squareup/applet/AppletSelection;->selectedApplet()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$wA9La3L08kMfSBVyXq_fMsUIYrQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$wA9La3L08kMfSBVyXq_fMsUIYrQ;-><init>(Lcom/squareup/ui/settings/SettingsSectionsPresenter;Lcom/squareup/marin/widgets/ActionBarView;)V

    .line 144
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$SettingsSectionsPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->settingsAvailable()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$TiEPodkMbCLXoZk7EELS2u0nR0Y;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$TiEPodkMbCLXoZk7EELS2u0nR0Y;-><init>(Lcom/squareup/ui/settings/SettingsSectionsPresenter;)V

    .line 166
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onLoad$8$SettingsSectionsPresenter(Lcom/squareup/ui/settings/SettingsSectionsView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    invoke-interface {v0}, Lcom/squareup/banklinking/BankAccountSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$r-UhUYmx1t4dBuqjsKSRl4-W4UA;

    invoke-direct {v1, p1}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$r-UhUYmx1t4dBuqjsKSRl4-W4UA;-><init>(Lcom/squareup/ui/settings/SettingsSectionsView;)V

    .line 170
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method onBannerClicked(Ljava/lang/String;)V
    .locals 2

    if-nez p1, :cond_0

    .line 268
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v0, Lcom/squareup/permissions/Permission;->CREATE_BANK_ACCOUNT:Lcom/squareup/permissions/Permission;

    new-instance v1, Lcom/squareup/ui/settings/SettingsSectionsPresenter$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter$3;-><init>(Lcom/squareup/ui/settings/SettingsSectionsPresenter;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->deepLinks:Lcom/squareup/ui/main/DeepLinks;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/DeepLinks;->handleInternalDeepLink(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    invoke-interface {v0, p1}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/settings/server/FeesUpdate;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$LKR1aX-_--t6r3xvx3_ihTiBo6I;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$LKR1aX-_--t6r3xvx3_ihTiBo6I;-><init>(Lcom/squareup/ui/settings/SettingsSectionsPresenter;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/hardware/UsbDevicesChangedEvent;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$eZKtLX_r1qZDWKqzFoHvSbff5pw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$eZKtLX_r1qZDWKqzFoHvSbff5pw;-><init>(Lcom/squareup/ui/settings/SettingsSectionsPresenter;)V

    .line 126
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 125
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->listRefresher:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$oS27WlNsjev9ae1zRy3nfqEe7No;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$oS27WlNsjev9ae1zRy3nfqEe7No;-><init>(Lcom/squareup/ui/settings/SettingsSectionsPresenter;)V

    .line 130
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 128
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 140
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/SettingsSectionsView;

    .line 141
    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsSectionsView;->getActionBar()Lcom/squareup/marin/widgets/ActionBarView;

    move-result-object v1

    .line 143
    new-instance v2, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$43WV8K88RlseQFKxOWGcPk5uHn4;

    invoke-direct {v2, p0, v1}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$43WV8K88RlseQFKxOWGcPk5uHn4;-><init>(Lcom/squareup/ui/settings/SettingsSectionsPresenter;Lcom/squareup/marin/widgets/ActionBarView;)V

    invoke-static {v0, v2}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 150
    iget-object v2, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    invoke-virtual {v2, v1}, Lcom/squareup/applet/BadgePresenter;->takeView(Ljava/lang/Object;)V

    .line 151
    new-instance v2, Lcom/squareup/ui/settings/SettingsSectionsPresenter$1;

    invoke-direct {v2, p0, v1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter$1;-><init>(Lcom/squareup/ui/settings/SettingsSectionsPresenter;Lcom/squareup/marin/widgets/ActionBarView;)V

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/ActionBarView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 161
    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsSectionsView;->initializeList()V

    .line 162
    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsSectionsView;->updateList()V

    .line 164
    new-instance v1, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$neRyeo5poIkx1mNLuvx3sYeIvlU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$neRyeo5poIkx1mNLuvx3sYeIvlU;-><init>(Lcom/squareup/ui/settings/SettingsSectionsPresenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 168
    new-instance v1, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$9kHjO3H-smIbSOXwTW_ydCjR4fw;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsPresenter$9kHjO3H-smIbSOXwTW_ydCjR4fw;-><init>(Lcom/squareup/ui/settings/SettingsSectionsPresenter;Lcom/squareup/ui/settings/SettingsSectionsView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    if-nez p1, :cond_0

    .line 175
    invoke-direct {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->snapToSelectedSection()V

    :cond_0
    return-void
.end method

.method onSectionClicked(I)V
    .locals 3

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->isSectionSelected(I)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 186
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 187
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 189
    :cond_1
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getEntry(I)Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;

    move-result-object p1

    .line 190
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->logClickEvent()V

    .line 192
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->isEditable(Lcom/squareup/applet/AppletSection;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 193
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSection;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->goOrReplaceTo(Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;Lcom/squareup/container/ContainerTreeKey;)V

    goto :goto_0

    .line 195
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->shouldAskForPasscode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 197
    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getSection()Lcom/squareup/applet/AppletSection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/applet/SectionAccess;->getPermissions()Ljava/util/Set;

    move-result-object v0

    const-string v1, "permissions"

    .line 200
    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonEmpty(Ljava/util/Collection;Ljava/lang/String;)Ljava/util/Collection;

    .line 202
    iget-object v1, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    new-instance v2, Lcom/squareup/ui/settings/SettingsSectionsPresenter$2;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter$2;-><init>(Lcom/squareup/ui/settings/SettingsSectionsPresenter;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;)V

    invoke-virtual {v1, v0, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAnyAccessGranted(Ljava/util/Set;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 208
    :cond_3
    new-instance v0, Lcom/squareup/ui/settings/DelegateLockoutScreen;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;->getTitleText()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/DelegateLockoutScreen;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->goOrReplaceTo(Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;Lcom/squareup/container/ContainerTreeKey;)V

    :goto_0
    return-void
.end method

.method onSignOutClicked()V
    .locals 1

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->signOutRunner:Lcom/squareup/signout/SignOutRunner;

    invoke-virtual {v0}, Lcom/squareup/signout/SignOutRunner;->attemptSignout()V

    return-void
.end method
