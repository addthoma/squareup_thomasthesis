.class public final Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;
.super Ljava/lang/Object;
.source "TimeTrackingSettingsScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final timeTrackingSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/TimeTrackingSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/TimeTrackingSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;->timeTrackingSettingsProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/TimeTrackingSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/permissions/TimeTrackingSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lflow/Flow;)Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;
    .locals 1

    .line 52
    new-instance v0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;-><init>(Lcom/squareup/permissions/TimeTrackingSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lflow/Flow;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;->timeTrackingSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/TimeTrackingSettings;

    iget-object v1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/Analytics;

    iget-object v2, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Device;

    iget-object v3, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflow/Flow;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;->newInstance(Lcom/squareup/permissions/TimeTrackingSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lflow/Flow;)Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner_Factory;->get()Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;

    move-result-object v0

    return-object v0
.end method
