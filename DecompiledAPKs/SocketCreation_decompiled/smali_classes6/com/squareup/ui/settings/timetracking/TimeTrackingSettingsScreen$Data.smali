.class public final Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen$Data;
.super Ljava/lang/Object;
.source "TimeTrackingSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Data"
.end annotation


# instance fields
.field final showBackButton:Z

.field final timeTrackingSettingsState:Lcom/squareup/permissions/TimeTrackingSettings$State;


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/TimeTrackingSettings$State;Z)V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen$Data;->timeTrackingSettingsState:Lcom/squareup/permissions/TimeTrackingSettings$State;

    .line 45
    iput-boolean p2, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen$Data;->showBackButton:Z

    return-void
.end method
