.class public interface abstract Lcom/squareup/ui/settings/SettingsAppletGateway;
.super Ljava/lang/Object;
.source "SettingsAppletGateway.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/SettingsAppletGateway$NoSettingsAppletGateway;
    }
.end annotation


# static fields
.field public static final APPLET_INTENT_SCREEN_EXTRA:Ljava/lang/String; = "SETTINGS"


# virtual methods
.method public abstract activate()V
.end method

.method public abstract activateBankAccountSettings()V
.end method

.method public abstract activateDepositsSettings()V
.end method

.method public abstract isBankAccountsVisible()Z
.end method

.method public abstract isInstantDepositsVisible()Z
.end method
