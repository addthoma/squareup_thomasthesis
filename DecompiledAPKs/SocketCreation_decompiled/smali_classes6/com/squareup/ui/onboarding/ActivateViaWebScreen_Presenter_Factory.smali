.class public final Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "ActivateViaWebScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final urlHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationUrlHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationUrlHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;->urlHelperProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ActivationUrlHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;"
        }
    .end annotation

    .line 51
    new-instance v6, Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/ActivationUrlHelper;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;
    .locals 7

    .line 56
    new-instance v6, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;-><init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/ActivationUrlHelper;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;->urlHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/onboarding/ActivationUrlHelper;

    iget-object v3, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/Res;

    iget-object v4, p0, Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;->newInstance(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/ActivationUrlHelper;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ActivateViaWebScreen_Presenter_Factory;->get()Lcom/squareup/ui/onboarding/ActivateViaWebScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
