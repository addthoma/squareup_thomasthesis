.class public Lcom/squareup/ui/onboarding/ActivationRetryView;
.super Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;
.source "ActivationRetryView.java"


# instance fields
.field private final confirmLaterPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

.field presenter:Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final serverCallView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const-class p2, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Component;->inject(Lcom/squareup/ui/onboarding/ActivationRetryView;)V

    .line 33
    new-instance p2, Lcom/squareup/caller/ProgressAndFailureView;

    invoke-direct {p2, p1}, Lcom/squareup/caller/ProgressAndFailureView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/ActivationRetryView;->serverCallView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    .line 34
    new-instance p2, Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/ConfirmationPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/ActivationRetryView;->confirmLaterPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationRetryView;->presenter:Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivationRetryView;->confirmLaterPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationRetryView;->presenter:Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->progressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivationRetryView;->serverCallView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    invoke-virtual {v0, v1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->dropView(Lcom/squareup/caller/ProgressAndFailurePresenter$View;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationRetryView;->presenter:Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 74
    invoke-super {p0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 38
    invoke-super {p0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->onFinishInflate()V

    .line 40
    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_retry_title:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivationRetryView;->setTitleText(I)V

    .line 41
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ActivationRetryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 42
    sget v1, Lcom/squareup/onboarding/flow/R$string;->activation_declined_explanation_contact_support:I

    .line 43
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/text/style/URLSpan;

    sget v3, Lcom/squareup/onboarding/flow/R$string;->help_url:I

    .line 44
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    .line 43
    invoke-static {v1, v2}, Lcom/squareup/text/Spannables;->span(Ljava/lang/CharSequence;Landroid/text/style/CharacterStyle;)Landroid/text/Spannable;

    move-result-object v0

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ActivationRetryView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/onboarding/flow/R$string;->onboarding_retry_message:I

    invoke-static {v1, v2}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "activation_declined_explanation_contact_support"

    .line 46
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 48
    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivationRetryView;->setMessageText(Ljava/lang/CharSequence;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationRetryView;->messageView:Lcom/squareup/widgets/MessageView;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setGravity(I)V

    .line 50
    sget v0, Lcom/squareup/cardreader/ui/R$string;->try_again:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivationRetryView;->setTopButton(I)V

    .line 51
    sget v0, Lcom/squareup/onboarding/flow/R$string;->later:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivationRetryView;->setBottomButton(I)V

    .line 52
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/onboarding/ActivationRetryView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;F)V

    .line 53
    new-instance v0, Lcom/squareup/ui/onboarding/ActivationRetryView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/ActivationRetryView$1;-><init>(Lcom/squareup/ui/onboarding/ActivationRetryView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivationRetryView;->setTopButtonOnClicked(Landroid/view/View$OnClickListener;)V

    .line 58
    new-instance v0, Lcom/squareup/ui/onboarding/ActivationRetryView$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/ActivationRetryView$2;-><init>(Lcom/squareup/ui/onboarding/ActivationRetryView;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/ActivationRetryView;->setBottomButtonOnClicked(Landroid/view/View$OnClickListener;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationRetryView;->presenter:Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationRetryView;->presenter:Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->progressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivationRetryView;->serverCallView:Lcom/squareup/caller/ProgressAndFailurePresenter$View;

    invoke-virtual {v0, v1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->takeView(Ljava/lang/Object;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationRetryView;->presenter:Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivationRetryView;->confirmLaterPopup:Lcom/squareup/flowlegacy/ConfirmationPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
