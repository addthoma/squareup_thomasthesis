.class Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "OnboardingFinishedScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter$1;->this$0:Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 50
    iget-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter$1;->this$0:Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;

    iget-object p1, p1, Lcom/squareup/ui/onboarding/OnboardingFinishedScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onOnboardingFinished()V

    return-void
.end method
