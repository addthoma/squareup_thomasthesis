.class Lcom/squareup/ui/onboarding/BusinessInfoView$2;
.super Lcom/squareup/debounce/DebouncedChangeOnceTextWatcher;
.source "BusinessInfoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/BusinessInfoView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/BusinessInfoView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/BusinessInfoView;)V
    .locals 0

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView$2;->this$0:Lcom/squareup/ui/onboarding/BusinessInfoView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedChangeOnceTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnFirstTextChange(Landroid/text/Editable;)V
    .locals 0

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView$2;->this$0:Lcom/squareup/ui/onboarding/BusinessInfoView;

    iget-object p1, p1, Lcom/squareup/ui/onboarding/BusinessInfoView;->presenter:Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->onBusinessNameChanged()V

    return-void
.end method
