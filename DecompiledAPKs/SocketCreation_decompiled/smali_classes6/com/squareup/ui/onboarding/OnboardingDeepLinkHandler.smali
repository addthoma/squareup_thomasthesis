.class public Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;
.super Ljava/lang/Object;
.source "OnboardingDeepLinkHandler.java"

# interfaces
.implements Lcom/squareup/deeplinks/DeepLinkHandler;


# instance fields
.field private final onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/OnboardingDiverter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    return-void
.end method


# virtual methods
.method public handleExternal(Landroid/net/Uri;)Lcom/squareup/deeplinks/DeepLinkResult;
    .locals 9

    .line 19
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x5

    const/4 v4, 0x4

    const/4 v5, -0x1

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v8, 0x1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v1, "settings"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_1
    const-string v1, "square-alternative.app.link"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_2
    const-string v1, "root"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_3
    const-string v1, "square.test-app.link"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_4
    const-string v1, "bnc.lt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :sswitch_5
    const-string v1, "square.app.link"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    if-eqz v0, :cond_1

    if-eq v0, v8, :cond_1

    if-eq v0, v7, :cond_1

    if-eq v0, v6, :cond_1

    if-eq v0, v4, :cond_1

    if-eq v0, v3, :cond_1

    goto :goto_4

    .line 28
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_2

    :sswitch_6
    const-string v0, "/finish-onboarding"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_7
    const-string v0, "/activate"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_3

    :sswitch_8
    const-string v0, "/finishOnboard"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 v2, 0x3

    goto :goto_3

    :sswitch_9
    const-string v0, "/finish-onboard"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 v2, 0x2

    goto :goto_3

    :cond_2
    :goto_2
    const/4 v2, -0x1

    :goto_3
    if-eqz v2, :cond_3

    if-eq v2, v8, :cond_3

    if-eq v2, v7, :cond_3

    if-eq v2, v6, :cond_3

    .line 36
    :goto_4
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    sget-object v0, Lcom/squareup/deeplinks/DeepLinkStatus;->UNRECOGNIZED:Lcom/squareup/deeplinks/DeepLinkStatus;

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/deeplinks/DeepLinkStatus;)V

    return-object p1

    .line 33
    :cond_3
    new-instance p1, Lcom/squareup/deeplinks/DeepLinkResult;

    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingHistoryFactory;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/OnboardingDeepLinkHandler;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    invoke-direct {v0, v1}, Lcom/squareup/ui/onboarding/OnboardingHistoryFactory;-><init>(Lcom/squareup/onboarding/OnboardingDiverter;)V

    invoke-direct {p1, v0}, Lcom/squareup/deeplinks/DeepLinkResult;-><init>(Lcom/squareup/ui/main/HistoryFactory;)V

    return-object p1

    :sswitch_data_0
    .sparse-switch
        -0x6d54daa8 -> :sswitch_5
        -0x52893ec1 -> :sswitch_4
        -0x4348448f -> :sswitch_3
        0x3580e2 -> :sswitch_2
        0x1284b78 -> :sswitch_1
        0x5582bc23 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x61f79ce4 -> :sswitch_9
        -0x493ff77b -> :sswitch_8
        -0x2a23c9de -> :sswitch_7
        0x72070f46 -> :sswitch_6
    .end sparse-switch
.end method
