.class public final Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;
.super Ljava/lang/Object;
.source "ActivationCallModule_ProvideApplyServerCallPresenterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
        "Lcom/squareup/server/activation/ApplyBody;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final activationServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/activation/ActivationService;",
            ">;"
        }
    .end annotation
.end field

.field private final firstReceivedResponseProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;"
        }
    .end annotation
.end field

.field private final glassSpinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final resourcesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/activation/ActivationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p2, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->activationServiceProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p3, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->firstReceivedResponseProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p4, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->resourcesProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p5, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->runnerProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p6, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->glassSpinnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/activation/ActivationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;)",
            "Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;"
        }
    .end annotation

    .line 61
    new-instance v7, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static provideApplyServerCallPresenter(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/server/activation/ActivationService;Lcom/squareup/onboarding/ShareableReceivedResponse;Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/onboarding/ActivationStatusPresenter;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/server/activation/ActivationService;",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/ApplyBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ")",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/ApplyBody;",
            ">;"
        }
    .end annotation

    .line 68
    invoke-static/range {p0 .. p5}, Lcom/squareup/ui/onboarding/ActivationCallModule;->provideApplyServerCallPresenter(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/server/activation/ActivationService;Lcom/squareup/onboarding/ShareableReceivedResponse;Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/ActivationStatusPresenter;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/ApplyBody;",
            ">;"
        }
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->activationServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/server/activation/ActivationService;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->firstReceivedResponseProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/onboarding/ShareableReceivedResponse;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->resourcesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->glassSpinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/register/widgets/GlassSpinner;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->provideApplyServerCallPresenter(Lcom/squareup/thread/executor/MainThread;Lcom/squareup/server/activation/ActivationService;Lcom/squareup/onboarding/ShareableReceivedResponse;Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/register/widgets/GlassSpinner;)Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ActivationCallModule_ProvideApplyServerCallPresenterFactory;->get()Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    move-result-object v0

    return-object v0
.end method
