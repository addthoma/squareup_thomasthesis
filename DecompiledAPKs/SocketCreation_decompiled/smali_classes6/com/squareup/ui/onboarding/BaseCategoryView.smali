.class abstract Lcom/squareup/ui/onboarding/BaseCategoryView;
.super Landroid/widget/LinearLayout;
.source "BaseCategoryView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/LinearLayout;"
    }
.end annotation


# instance fields
.field private categories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field

.field private radios:Lcom/squareup/widgets/CheckableGroup;

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;

.field private final warningPopup:Lcom/squareup/flowlegacy/WarningPopup;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    new-instance p2, Lcom/squareup/flowlegacy/WarningPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/WarningPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 85
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 86
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected getCategories()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->categories:Ljava/util/List;

    return-object v0
.end method

.method protected getSelected()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->categories:Ljava/util/List;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->radios:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->getCheckedId()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    goto :goto_0

    .line 51
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->categories:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    :goto_0
    return-object v1
.end method

.method protected getWarningPopup()Lcom/squareup/flowlegacy/WarningPopup;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    return-object v0
.end method

.method public hideSubtitle()V
    .locals 2

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->subtitle:Lcom/squareup/widgets/MessageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 32
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 34
    sget v0, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->title:Lcom/squareup/widgets/MessageView;

    .line 35
    sget v0, Lcom/squareup/onboarding/flow/R$id;->subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 36
    sget v0, Lcom/squareup/onboarding/flow/R$id;->radios:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CheckableGroup;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->radios:Lcom/squareup/widgets/CheckableGroup;

    return-void
.end method

.method protected setCategories(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->categories:Ljava/util/List;

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->radios:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->removeAllViews()V

    .line 57
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/BaseCategoryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->radios:Lcom/squareup/widgets/CheckableGroup;

    const/4 v2, 0x1

    invoke-static {v0, v1, p1, v2}, Lcom/squareup/ui/CheckableGroups;->addAsRows(Landroid/view/LayoutInflater;Lcom/squareup/widgets/CheckableGroup;Ljava/util/List;Z)V

    return-void
.end method

.method protected setCategorySelectedListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->radios:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method protected setSelectedIndex(I)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->radios:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    return-void
.end method

.method protected setSubtitle(I)V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->subtitle:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    return-void
.end method

.method protected setTitle(I)V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->title:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    return-void
.end method

.method protected setTitle(Ljava/lang/String;)V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BaseCategoryView;->title:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
