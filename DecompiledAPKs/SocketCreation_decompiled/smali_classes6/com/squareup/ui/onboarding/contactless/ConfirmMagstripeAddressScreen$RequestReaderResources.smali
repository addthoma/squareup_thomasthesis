.class Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$RequestReaderResources;
.super Lcom/squareup/request/RequestMessages;
.source "ConfirmMagstripeAddressScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RequestReaderResources"
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;)V
    .locals 2

    .line 338
    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_apply_progress_title:I

    sget v1, Lcom/squareup/onboarding/flow/R$string;->onboarding_apply_failure_title:I

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/request/RequestMessages;-><init>(Lcom/squareup/util/Res;II)V

    return-void
.end method


# virtual methods
.method public getLoadingCompleteMessage()Ljava/lang/String;
    .locals 2

    .line 342
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$RequestReaderResources;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->onboarding_reader_on_the_way:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
