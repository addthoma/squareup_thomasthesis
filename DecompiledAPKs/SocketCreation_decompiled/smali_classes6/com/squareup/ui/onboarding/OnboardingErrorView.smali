.class public Lcom/squareup/ui/onboarding/OnboardingErrorView;
.super Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;
.source "OnboardingErrorView.java"


# instance fields
.field presenter:Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    const-class p2, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Component;->inject(Lcom/squareup/ui/onboarding/OnboardingErrorView;)V

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingErrorView;->presenter:Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 26
    invoke-super {p0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 20
    invoke-super {p0}, Lcom/squareup/ui/onboarding/OnboardingPromptBaseView;->onFinishInflate()V

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingErrorView;->presenter:Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
