.class final Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$3$2;
.super Lkotlin/jvm/internal/Lambda;
.source "DepositOptionsReactor.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$3;->invoke(Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnContinueClickedFromDepositOptionsConfirmationDialog;",
        "Lcom/squareup/workflow/legacy/FinishWith<",
        "+",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult$LinkLater;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/legacy/FinishWith;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult$LinkLater;",
        "it",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnContinueClickedFromDepositOptionsConfirmationDialog;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$3$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$3$2;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$3$2;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$3$2;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$3$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnContinueClickedFromDepositOptionsConfirmationDialog;)Lcom/squareup/workflow/legacy/FinishWith;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnContinueClickedFromDepositOptionsConfirmationDialog;",
            ")",
            "Lcom/squareup/workflow/legacy/FinishWith<",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult$LinkLater;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    new-instance p1, Lcom/squareup/workflow/legacy/FinishWith;

    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult$LinkLater;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult$LinkLater;

    invoke-direct {p1, v0}, Lcom/squareup/workflow/legacy/FinishWith;-><init>(Ljava/lang/Object;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 81
    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnContinueClickedFromDepositOptionsConfirmationDialog;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$onReact$3$2;->invoke(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnContinueClickedFromDepositOptionsConfirmationDialog;)Lcom/squareup/workflow/legacy/FinishWith;

    move-result-object p1

    return-object p1
.end method
