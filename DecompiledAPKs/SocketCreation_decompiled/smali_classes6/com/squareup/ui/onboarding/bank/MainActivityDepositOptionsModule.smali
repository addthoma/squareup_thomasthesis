.class public abstract Lcom/squareup/ui/onboarding/bank/MainActivityDepositOptionsModule;
.super Ljava/lang/Object;
.source "DepositOptionModules.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\'J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\'\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/MainActivityDepositOptionsModule;",
        "",
        "()V",
        "provideDepositOptionsStarterFactory",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;",
        "mainActivityFactory",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory;",
        "provideDepositOptionsWorkflowResultHandler",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;",
        "handler",
        "Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;",
        "provideDepositOptionsWorkflowRunner",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;",
        "mainActivityRunner",
        "Lcom/squareup/ui/onboarding/bank/MainActivityDepositOptionsWorkflowRunner;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract provideDepositOptionsStarterFactory(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory;)Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideDepositOptionsWorkflowResultHandler(Lcom/squareup/ui/onboarding/bank/SetupGuideDepositOptionsResultHandler;)Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract provideDepositOptionsWorkflowRunner(Lcom/squareup/ui/onboarding/bank/MainActivityDepositOptionsWorkflowRunner;)Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
