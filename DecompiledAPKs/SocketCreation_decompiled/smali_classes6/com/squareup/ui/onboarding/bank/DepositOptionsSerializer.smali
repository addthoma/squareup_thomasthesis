.class public final Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;
.super Ljava/lang/Object;
.source "DepositOptionsSerializer.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDepositOptionsSerializer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DepositOptionsSerializer.kt\ncom/squareup/ui/onboarding/bank/DepositOptionsSerializer\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n+ 3 BuffersProtos.kt\ncom/squareup/workflow/BuffersProtos\n*L\n1#1,204:1\n180#2:205\n140#2:206\n140#2:207\n140#2:208\n140#2:209\n140#2:210\n140#2:212\n56#3:211\n56#3:213\n*E\n*S KotlinDebug\n*F\n+ 1 DepositOptionsSerializer.kt\ncom/squareup/ui/onboarding/bank/DepositOptionsSerializer\n*L\n89#1:205\n89#1:206\n89#1:207\n89#1:208\n89#1:209\n89#1:210\n89#1:212\n89#1:211\n89#1:213\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0008\u001a\u00020\u0004J\u000c\u0010\t\u001a\u00020\n*\u00020\u000bH\u0007J\u000c\u0010\u000c\u001a\u00020\r*\u00020\u000bH\u0007J\u0014\u0010\u000e\u001a\u00020\u000f*\u00020\u000f2\u0006\u0010\u0008\u001a\u00020\u0004H\u0002J\u0014\u0010\u0010\u001a\u00020\u000f*\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\nH\u0007J\u0014\u0010\u0012\u001a\u00020\u000f*\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\rH\u0007\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;",
        "",
        "()V",
        "deserializeState",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "snapshotState",
        "state",
        "readFailureMessage",
        "Lcom/squareup/receiving/FailureMessage;",
        "Lokio/BufferedSource;",
        "readInvalidInput",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;",
        "writeDepositConfig",
        "Lokio/BufferedSink;",
        "writeFailureMessage",
        "failureMessage",
        "writeInvalidInput",
        "invalidInput",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 53
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$writeDepositConfig(Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;Lokio/BufferedSink;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lokio/BufferedSink;
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->writeDepositConfig(Lokio/BufferedSink;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lokio/BufferedSink;

    move-result-object p0

    return-object p0
.end method

.method private final writeDepositConfig(Lokio/BufferedSink;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lokio/BufferedSink;
    .locals 1

    if-eqz p2, :cond_0

    .line 158
    check-cast p2, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;

    .line 159
    invoke-virtual {p2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;->getSameDayDepositSelected()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    .line 160
    invoke-virtual {p2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;->getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;

    move-result-object p2

    check-cast p2, Ljava/lang/Enum;

    invoke-static {p1, p2}, Lcom/squareup/workflow/SnapshotKt;->writeOptionalEnumByOrdinal(Lokio/BufferedSink;Ljava/lang/Enum;)Lokio/BufferedSink;

    return-object p1

    .line 158
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.ui.onboarding.bank.DepositOptionsReactor.DepositOptionsState.BankLinking"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final deserializeState(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;
    .locals 4

    const-string v0, "snapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    invoke-virtual {p1}, Lcom/squareup/workflow/Snapshot;->bytes()Lokio/ByteString;

    move-result-object p1

    .line 205
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p1}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p1

    check-cast p1, Lokio/BufferedSource;

    .line 90
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v0

    .line 91
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "Class.forName(className)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lkotlin/jvm/JvmClassMappingKt;->getKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    .line 94
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$SelectingDepositSpeed;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$SelectingDepositSpeed;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$SelectingDepositSpeed;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 95
    :cond_0
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$SelectingDepositSpeed;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$SelectingDepositSpeed;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$SelectingDepositSpeed;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 96
    :cond_1
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$ConfirmingLater;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$ConfirmingLater;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$ConfirmingLater;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 97
    :cond_2
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$ConfirmingLater;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$ConfirmingLater;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$ConfirmingLater;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 98
    :cond_3
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$ConfirmingSameDayDepositFee;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$ConfirmingSameDayDepositFee;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$ConfirmingSameDayDepositFee;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 99
    :cond_4
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$ConfirmingSameDayDepositFee;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$ConfirmingSameDayDepositFee;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$ConfirmingSameDayDepositFee;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 101
    :cond_5
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 102
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    .line 206
    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$1;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v1}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Enum;

    check-cast p1, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 101
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;

    invoke-direct {v1, v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;-><init>(ZLcom/squareup/protos/client/bankaccount/BankAccountType;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 105
    :cond_6
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$ConfirmingLater;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 106
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    .line 207
    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$2;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$2;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v1}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Enum;

    check-cast p1, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 105
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$ConfirmingLater;

    invoke-direct {v1, v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$ConfirmingLater;-><init>(ZLcom/squareup/protos/client/bankaccount/BankAccountType;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 109
    :cond_7
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$SelectingAccountType;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 110
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    .line 208
    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$3;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v1}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Enum;

    check-cast p1, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 109
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$SelectingAccountType;

    invoke-direct {v1, v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$SelectingAccountType;-><init>(ZLcom/squareup/protos/client/bankaccount/BankAccountType;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 113
    :cond_8
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$HasInvalidInput;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 114
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    .line 209
    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$4;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$4;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v1}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Enum;

    check-cast v1, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 116
    sget-object v2, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;

    invoke-virtual {v2, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->readInvalidInput(Lokio/BufferedSource;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;

    move-result-object p1

    .line 113
    new-instance v2, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$HasInvalidInput;

    invoke-direct {v2, v0, v1, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$HasInvalidInput;-><init>(ZLcom/squareup/protos/client/bankaccount/BankAccountType;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;)V

    move-object p1, v2

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 118
    :cond_9
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 119
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    .line 210
    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$5;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$5;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v1}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Enum;

    check-cast v1, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 211
    sget-object v2, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v3, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    invoke-virtual {v2, v3}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    .line 118
    new-instance v2, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;

    invoke-direct {v2, v0, v1, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;-><init>(ZLcom/squareup/protos/client/bankaccount/BankAccountType;Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)V

    move-object p1, v2

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 123
    :cond_a
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 124
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    .line 212
    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$6;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$$special$$inlined$readOptionalEnumByOrdinal$6;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v1}, Lcom/squareup/workflow/SnapshotKt;->readNullable(Lokio/BufferedSource;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Enum;

    check-cast v1, Lcom/squareup/protos/client/bankaccount/BankAccountType;

    .line 126
    sget-object v2, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;

    invoke-virtual {v2, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer;->readFailureMessage(Lokio/BufferedSource;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 123
    new-instance v2, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;

    invoke-direct {v2, v0, v1, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;-><init>(ZLcom/squareup/protos/client/bankaccount/BankAccountType;Lcom/squareup/receiving/FailureMessage;)V

    move-object p1, v2

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 129
    :cond_b
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$PrepareToLinkDebitCard;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$PrepareToLinkDebitCard;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$PrepareToLinkDebitCard;-><init>(Z)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 130
    :cond_c
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 131
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result v0

    .line 213
    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->Companion:Lcom/squareup/wire/ProtoAdapter$Companion;

    const-class v2, Lcom/squareup/protos/client/bills/CardData;

    invoke-virtual {v1, v2}, Lcom/squareup/wire/ProtoAdapter$Companion;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/squareup/workflow/BuffersProtos;->readProtoWithLength(Lokio/BufferedSource;Lcom/squareup/wire/ProtoAdapter;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/bills/CardData;

    .line 130
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;

    invoke-direct {v1, v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;-><init>(ZLcom/squareup/protos/client/bills/CardData;)V

    move-object p1, v1

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 133
    :cond_d
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;-><init>(Z)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 135
    :cond_e
    const-class p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccess;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_f

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccess;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccess;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 136
    :cond_f
    const-class p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSkip;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_10

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSkip;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSkip;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 137
    :cond_10
    const-class p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardFailure;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_11

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardFailure;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 138
    :cond_11
    const-class p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupSuccess;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_12

    .line 139
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupSuccess;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupSuccess;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto/16 :goto_0

    .line 140
    :cond_12
    const-class p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupFailure;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_13

    .line 141
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupFailure;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto :goto_0

    .line 142
    :cond_13
    const-class p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPending;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_14

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPending;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPending;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto :goto_0

    .line 143
    :cond_14
    const-class p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSkip;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_15

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSkip;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSkip;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto :goto_0

    .line 144
    :cond_15
    const-class p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardFailure;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_16

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardFailure;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto :goto_0

    .line 145
    :cond_16
    const-class p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupSuccess;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_17

    .line 146
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupSuccess;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupSuccess;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto :goto_0

    .line 147
    :cond_17
    const-class p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupFailure;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_18

    .line 148
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupFailure;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    goto :goto_0

    .line 149
    :cond_18
    const-class p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankFailure;

    invoke-static {p1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_19

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankFailure;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;

    :goto_0
    return-object p1

    .line 151
    :cond_19
    new-instance p1, Ljava/lang/IllegalArgumentException;

    .line 152
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid DepositOptionsState class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public final readFailureMessage(Lokio/BufferedSource;)Lcom/squareup/receiving/FailureMessage;
    .locals 3

    const-string v0, "$this$readFailureMessage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    new-instance v0, Lcom/squareup/receiving/FailureMessage;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readBooleanFromInt(Lokio/BufferedSource;)Z

    move-result p1

    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/receiving/FailureMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public final readInvalidInput(Lokio/BufferedSource;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;
    .locals 3

    const-string v0, "$this$readInvalidInput"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readUtf8WithLength(Lokio/BufferedSource;)Ljava/lang/String;

    move-result-object p1

    .line 188
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object p1

    const-string v0, "Class.forName(className)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1}, Lkotlin/jvm/JvmClassMappingKt;->getKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object p1

    .line 191
    const-class v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountHolder;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountHolder;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountHolder;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;

    goto :goto_0

    .line 192
    :cond_0
    const-class v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$InvalidRoutingNumber;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$InvalidRoutingNumber;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$InvalidRoutingNumber;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;

    goto :goto_0

    .line 193
    :cond_1
    const-class v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountNumber;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountNumber;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountNumber;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;

    goto :goto_0

    .line 194
    :cond_2
    const-class v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$RoutingAndAccountNumbersMatch;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$RoutingAndAccountNumbersMatch;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$RoutingAndAccountNumbersMatch;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;

    goto :goto_0

    .line 195
    :cond_3
    const-class v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountType;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountType;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountType;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;

    :goto_0
    return-object p1

    .line 196
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 197
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid InvalidInput class: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 196
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final snapshotState(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/workflow/Snapshot;
    .locals 2

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsSerializer$snapshotState$1;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public final writeFailureMessage(Lokio/BufferedSink;Lcom/squareup/receiving/FailureMessage;)Lokio/BufferedSink;
    .locals 1

    const-string v0, "$this$writeFailureMessage"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "failureMessage"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    invoke-virtual {p2}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 168
    invoke-virtual {p2}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    .line 169
    invoke-virtual {p2}, Lcom/squareup/receiving/FailureMessage;->getRetryable()Z

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/workflow/SnapshotKt;->writeBooleanAsInt(Lokio/BufferedSink;Z)Lokio/BufferedSink;

    return-object p1
.end method

.method public final writeInvalidInput(Lokio/BufferedSink;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;)Lokio/BufferedSink;
    .locals 1

    const-string v0, "$this$writeInvalidInput"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invalidInput"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    const-string v0, "invalidInput.javaClass.name"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {p1, p2}, Lcom/squareup/workflow/SnapshotKt;->writeUtf8WithLength(Lokio/BufferedSink;Ljava/lang/String;)Lokio/BufferedSink;

    return-object p1
.end method
