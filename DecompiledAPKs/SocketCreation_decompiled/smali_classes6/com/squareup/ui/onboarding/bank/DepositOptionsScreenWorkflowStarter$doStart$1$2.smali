.class final Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1$2;
.super Lkotlin/jvm/internal/Lambda;
.source "DepositOptionsScreenWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;->invoke(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/workflow/legacy/Screen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
        "it",
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1$2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1$2;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1$2;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1$2;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1$2;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$GoBack;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$GoBack;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnBackClickedFromDepositBankLinkingScreen;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnBackClickedFromDepositBankLinkingScreen;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;

    goto :goto_0

    .line 180
    :cond_0
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$LaterClicked;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$LaterClicked;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnLaterClickedFromDepositBankLinkingScreen;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnLaterClickedFromDepositBankLinkingScreen;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;

    goto :goto_0

    .line 181
    :cond_1
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$AccountTypeClicked;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$AccountTypeClicked;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnAccountTypeClicked;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnAccountTypeClicked;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;

    goto :goto_0

    .line 182
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$InvalidInput;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$InvalidInput;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$InvalidInput;->getInvalidInput()Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;

    goto :goto_0

    .line 183
    :cond_3
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$LinkBank;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnLinkingBank;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$LinkBank;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$LinkBank;->getBankAccountDetails()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$OnLinkingBank;-><init>(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;

    :goto_0
    return-object p1

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 93
    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1$2;->invoke(Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;

    move-result-object p1

    return-object p1
.end method
