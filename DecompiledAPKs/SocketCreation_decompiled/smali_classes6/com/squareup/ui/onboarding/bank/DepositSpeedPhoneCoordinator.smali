.class public final Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "DepositSpeedPhoneCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDepositSpeedPhoneCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DepositSpeedPhoneCoordinator.kt\ncom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator\n*L\n1#1,91:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B#\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u00a2\u0006\u0002\u0010\u0008J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0016\u0010\u0017\u001a\u00020\u00132\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0019H\u0002J(\u0010\u001a\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0016\u0010\u001b\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J&\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u001d\u001a\u00020\u001e2\u000c\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0019H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;",
        "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;",
        "Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneScreen;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "depositMethods",
        "Lcom/squareup/widgets/CheckableGroup;",
        "sameDayDepositDescription",
        "Lcom/squareup/marketfont/MarketTextView;",
        "subtitle",
        "Lcom/squareup/widgets/MessageView;",
        "title",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "onDepositMethodSelected",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "onScreen",
        "screen",
        "setUpActionBar",
        "canGoBack",
        "",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private depositMethods:Lcom/squareup/widgets/CheckableGroup;

.field private sameDayDepositDescription:Lcom/squareup/marketfont/MarketTextView;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->screens:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 19
    iget-object p0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$onDepositMethodSelected(Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->onDepositMethodSelected(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->onScreen(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V

    return-void
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;)V
    .locals 0

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 84
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 85
    sget v0, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 86
    sget v0, Lcom/squareup/onboarding/flow/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 87
    sget v0, Lcom/squareup/onboarding/flow/R$id;->deposit_methods:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CheckableGroup;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->depositMethods:Lcom/squareup/widgets/CheckableGroup;

    .line 88
    sget v0, Lcom/squareup/onboarding/flow/R$id;->same_day_deposit_description:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->sameDayDepositDescription:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method private final onDepositMethodSelected(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;",
            ">;)V"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->depositMethods:Lcom/squareup/widgets/CheckableGroup;

    if-nez v0, :cond_0

    const-string v1, "depositMethods"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->getCheckedId()I

    move-result v0

    sget v1, Lcom/squareup/onboarding/flow/R$id;->deposit_speed_same_day:I

    if-ne v0, v1, :cond_1

    .line 77
    sget-object v0, Lcom/squareup/ui/onboarding/bank/SameDayDepositSelected;->INSTANCE:Lcom/squareup/ui/onboarding/bank/SameDayDepositSelected;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    goto :goto_0

    .line 79
    :cond_1
    sget-object v0, Lcom/squareup/ui/onboarding/bank/NextBusinessDayDepositSelected;->INSTANCE:Lcom/squareup/ui/onboarding/bank/NextBusinessDayDepositSelected;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private final onScreen(Landroid/view/View;Lcom/squareup/workflow/legacy/Screen;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;",
            ">;)V"
        }
    .end annotation

    .line 43
    iget-object v0, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;->getCanGoBack()Z

    move-result v0

    iget-object v1, p2, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->setUpActionBar(Landroid/view/View;ZLcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 44
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->sameDayDepositDescription:Lcom/squareup/marketfont/MarketTextView;

    if-nez p1, :cond_0

    const-string v0, "sameDayDepositDescription"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object p2, p2, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p2, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;

    invoke-virtual {p2}, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;->getSameDayDepositDescription()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final setUpActionBar(Landroid/view/View;ZLcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Z",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenEvent;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "title"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/onboarding/flow/R$string;->deposit_speed_subtitle:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1

    const-string v1, "subtitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 56
    iget-object v7, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v7, :cond_2

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 55
    :cond_2
    new-instance v8, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v8}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    const/4 v9, 0x1

    if-eqz p2, :cond_3

    .line 58
    new-instance v10, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$setUpActionBar$$inlined$run$lambda$1;

    move-object v0, v10

    move-object v1, p0

    move v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$setUpActionBar$$inlined$run$lambda$1;-><init>(Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;ZLandroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)V

    check-cast v10, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v10}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 59
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->back:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v8, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphNoText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 60
    invoke-virtual {v8, v9}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 61
    new-instance v10, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$setUpActionBar$$inlined$run$lambda$2;

    move-object v0, v10

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$setUpActionBar$$inlined$run$lambda$2;-><init>(Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;ZLandroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)V

    check-cast v10, Ljava/lang/Runnable;

    invoke-virtual {v8, v10}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    .line 63
    :cond_3
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$setUpActionBar$1$3;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$setUpActionBar$1$3;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 64
    invoke-virtual {v8}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 66
    :goto_0
    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 67
    new-instance v10, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$setUpActionBar$$inlined$run$lambda$3;

    move-object v0, v10

    move-object v1, p0

    move v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$setUpActionBar$$inlined$run$lambda$3;-><init>(Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;ZLandroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)V

    check-cast v10, Ljava/lang/Runnable;

    invoke-virtual {v8, v10}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->depositMethods:Lcom/squareup/widgets/CheckableGroup;

    if-nez v0, :cond_4

    const-string v1, "depositMethods"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->getCheckedId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    goto :goto_1

    :cond_5
    const/4 v9, 0x0

    :goto_1
    invoke-virtual {v8, v9}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 69
    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_later:I

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 70
    new-instance v9, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$setUpActionBar$$inlined$run$lambda$4;

    move-object v0, v9

    move-object v1, p0

    move v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$setUpActionBar$$inlined$run$lambda$4;-><init>(Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;ZLandroid/view/View;Lcom/squareup/workflow/legacy/WorkflowInput;Landroid/content/res/Resources;)V

    check-cast v9, Ljava/lang/Runnable;

    invoke-virtual {v8, v9}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 71
    invoke-virtual {v8}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    .line 56
    invoke-virtual {v7, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$attach$1;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$attach$1;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->bindViews(Landroid/view/View;)V

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->depositMethods:Lcom/squareup/widgets/CheckableGroup;

    if-nez v0, :cond_0

    const-string v1, "depositMethods"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$attach$2;-><init>(Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;)V

    check-cast v1, Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$attach$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator$attach$3;-><init>(Lcom/squareup/ui/onboarding/bank/DepositSpeedPhoneCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { onScreen(view, it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
