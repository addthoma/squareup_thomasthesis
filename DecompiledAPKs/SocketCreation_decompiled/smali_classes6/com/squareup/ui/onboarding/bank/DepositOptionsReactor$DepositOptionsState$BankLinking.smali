.class public abstract Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;
.super Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;
.source "DepositOptionsReactor.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BankLinking"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$ConfirmingLater;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$SelectingAccountType;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$HasInvalidInput;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0006\u000b\u000c\r\u000e\u000f\u0010B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0008X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\n\u0082\u0001\u0006\u0011\u0012\u0013\u0014\u0015\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
        "()V",
        "bankAccountType",
        "Lcom/squareup/protos/client/bankaccount/BankAccountType;",
        "getBankAccountType",
        "()Lcom/squareup/protos/client/bankaccount/BankAccountType;",
        "sameDayDepositSelected",
        "",
        "getSameDayDepositSelected",
        "()Z",
        "BankWarning",
        "ConfirmingLater",
        "HasInvalidInput",
        "LinkingBankAccount",
        "PrepareToLinkBankAccount",
        "SelectingAccountType",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$PrepareToLinkBankAccount;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$ConfirmingLater;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$SelectingAccountType;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$HasInvalidInput;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 111
    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;
.end method

.method public abstract getSameDayDepositSelected()Z
.end method
