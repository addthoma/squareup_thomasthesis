.class public final Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory_Factory;
.super Ljava/lang/Object;
.source "DepositOptionsViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final depositBankLinkingFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final depositCardLinkingFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$Factory;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory_Factory;->depositBankLinkingFactoryProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory_Factory;->depositCardLinkingFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$Factory;",
            ">;)",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$Factory;)Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;-><init>(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory_Factory;->depositBankLinkingFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory_Factory;->depositCardLinkingFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$Factory;

    invoke-static {v0, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory_Factory;->newInstance(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;Lcom/squareup/ui/onboarding/bank/DepositCardLinkingCoordinator$Factory;)Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory_Factory;->get()Lcom/squareup/ui/onboarding/bank/DepositOptionsViewFactory;

    move-result-object v0

    return-object v0
.end method
