.class public final Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "DepositOptionsWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Scope"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDepositOptionsWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DepositOptionsWorkflowRunner.kt\ncom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n+ 3 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,99:1\n35#2:100\n24#3,4:101\n*E\n*S KotlinDebug\n*F\n+ 1 DepositOptionsWorkflowRunner.kt\ncom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope\n*L\n64#1:100\n82#1,4:101\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0018\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0014J\u0008\u0010\u000e\u001a\u00020\u0001H\u0016R\u000e\u0010\u0002\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "parentKey",
        "(Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "getParentKey",
        "Companion",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope$Companion;


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;->Companion:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope$Companion;

    .line 101
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 104
    sput-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "parentKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-super {p0, p1}, Lcom/squareup/ui/main/RegisterTreeKey;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 100
    const-class v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ParentComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 64
    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ParentComponent;

    .line 66
    invoke-interface {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ParentComponent;->depositOptionsWorkflowRunner()Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;

    move-result-object p1

    const-string v1, "builder"

    .line 67
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    return-object v0
.end method

.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 56
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$Scope;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method
