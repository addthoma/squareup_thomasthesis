.class public final Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "LoggedInOnboardingScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;->INSTANCE:Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;

    .line 21
    sget-object v0, Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;->INSTANCE:Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/LoggedInOnboardingScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 16
    const-class v0, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;

    .line 17
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;

    invoke-interface {v0}, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;->getRunner()Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    move-result-object v0

    .line 18
    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
