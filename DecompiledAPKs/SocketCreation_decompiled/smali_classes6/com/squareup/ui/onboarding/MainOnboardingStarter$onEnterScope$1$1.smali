.class final Lcom/squareup/ui/onboarding/MainOnboardingStarter$onEnterScope$1$1;
.super Ljava/lang/Object;
.source "MainOnboardingStarter.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/MainOnboardingStarter$onEnterScope$1;->apply(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod;",
        "it",
        "Lcom/squareup/onboarding/OnboardingType$Variant;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $params:Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;


# direct methods
.method constructor <init>(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/MainOnboardingStarter$onEnterScope$1$1;->$params:Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/onboarding/OnboardingType$Variant;)Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod;
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    sget-object v0, Lcom/squareup/onboarding/OnboardingType$Variant;->SERVER_DRIVEN:Lcom/squareup/onboarding/OnboardingType$Variant;

    if-ne p1, v0, :cond_0

    sget-object p1, Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod$ServerDriven;->INSTANCE:Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod$ServerDriven;

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod$LegacyMode;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/MainOnboardingStarter$onEnterScope$1$1;->$params:Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;

    const-string v1, "params"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p1, v0}, Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod$LegacyMode;-><init>(Lcom/squareup/onboarding/OnboardingStarter$OnboardingParams;)V

    :goto_0
    check-cast p1, Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod;

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/onboarding/OnboardingType$Variant;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/MainOnboardingStarter$onEnterScope$1$1;->apply(Lcom/squareup/onboarding/OnboardingType$Variant;)Lcom/squareup/ui/onboarding/MainOnboardingStarter$OnboardingMethod;

    move-result-object p1

    return-object p1
.end method
