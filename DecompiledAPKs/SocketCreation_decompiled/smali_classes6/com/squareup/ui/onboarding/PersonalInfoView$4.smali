.class Lcom/squareup/ui/onboarding/PersonalInfoView$4;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "PersonalInfoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/PersonalInfoView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/PersonalInfoView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/PersonalInfoView;)V
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView$4;->this$0:Lcom/squareup/ui/onboarding/PersonalInfoView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p3, 0x5

    if-ne p2, p3, :cond_0

    .line 106
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 107
    iget-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoView$4;->this$0:Lcom/squareup/ui/onboarding/PersonalInfoView;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/PersonalInfoView;->access$000(Lcom/squareup/ui/onboarding/PersonalInfoView;)Landroid/widget/TextView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/TextView;->requestFocus()Z

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
