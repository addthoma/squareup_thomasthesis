.class public final Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideOnboardingModelBundleKeyFactory;
.super Ljava/lang/Object;
.source "OnboardingContainer_Module_ProvideOnboardingModelBundleKeyFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/BundleKey<",
        "Lcom/squareup/ui/onboarding/OnboardingModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideOnboardingModelBundleKeyFactory;->gsonProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideOnboardingModelBundleKeyFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;)",
            "Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideOnboardingModelBundleKeyFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideOnboardingModelBundleKeyFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideOnboardingModelBundleKeyFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideOnboardingModelBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation

    .line 37
    invoke-static {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer$Module;->provideOnboardingModelBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/BundleKey;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/BundleKey;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideOnboardingModelBundleKeyFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/Gson;

    invoke-static {v0}, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideOnboardingModelBundleKeyFactory;->provideOnboardingModelBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingContainer_Module_ProvideOnboardingModelBundleKeyFactory;->get()Lcom/squareup/BundleKey;

    move-result-object v0

    return-object v0
.end method
