.class public Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog$Factory;
.super Ljava/lang/Object;
.source "EmptyContinueDialog.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private onSkip(ZLcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;)V
    .locals 0

    if-eqz p1, :cond_0

    .line 47
    invoke-virtual {p2}, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->onWhereSkipped()V

    return-void

    .line 51
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;->onHowSkipped()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 31
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;

    .line 32
    const-class v1, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;

    .line 33
    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;

    .line 34
    invoke-interface {v1}, Lcom/squareup/ui/onboarding/CommonOnboardingActivityComponent;->onboardingIntentData()Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;

    move-result-object v1

    .line 36
    new-instance v2, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/onboarding/flow/R$string;->select_an_option:I

    .line 37
    invoke-virtual {v2, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v2, Lcom/squareup/onboarding/flow/R$string;->select_an_option_detail:I

    .line 38
    invoke-virtual {p1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v2, Lcom/squareup/widgets/R$string;->skip:I

    new-instance v3, Lcom/squareup/ui/onboarding/intent/-$$Lambda$EmptyContinueDialog$Factory$cGZ7m5Out5m85GN7fv6Vl5P4caA;

    invoke-direct {v3, p0, v0, v1}, Lcom/squareup/ui/onboarding/intent/-$$Lambda$EmptyContinueDialog$Factory$cGZ7m5Out5m85GN7fv6Vl5P4caA;-><init>(Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog$Factory;Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;)V

    .line 39
    invoke-virtual {p1, v2, v3}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->ok:I

    .line 41
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 42
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 36
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$create$0$EmptyContinueDialog$Factory(Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;Lcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 40
    invoke-static {p1}, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;->access$000(Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog;)Z

    move-result p1

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/intent/EmptyContinueDialog$Factory;->onSkip(ZLcom/squareup/ui/onboarding/intent/OnboardingIntentRunner;)V

    return-void
.end method
