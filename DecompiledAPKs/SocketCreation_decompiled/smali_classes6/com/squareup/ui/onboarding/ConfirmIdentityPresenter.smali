.class public Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;
.super Lmortar/ViewPresenter;
.source "ConfirmIdentityPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/ConfirmIdentityView;",
        ">;"
    }
.end annotation


# static fields
.field private static final SELECTED_ANSWERS:Ljava/lang/String; = "selected-answers"


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final model:Lcom/squareup/ui/onboarding/OnboardingModel;

.field private final res:Lcom/squareup/util/Res;

.field final updateCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/AnswersBody;",
            ">;"
        }
    .end annotation
.end field

.field private final updateReceivedResponse:Lcom/squareup/onboarding/ShareableReceivedResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/AnswersBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation
.end field

.field final warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/ActivationStatusPresenter;Lcom/squareup/analytics/Analytics;Lcom/squareup/onboarding/ShareableReceivedResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            "Lcom/squareup/ui/onboarding/ActivationStatusPresenter<",
            "Lcom/squareup/server/activation/AnswersBody;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/onboarding/ShareableReceivedResponse<",
            "Lcom/squareup/server/activation/AnswersBody;",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 38
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 27
    new-instance v0, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->res:Lcom/squareup/util/Res;

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 41
    iput-object p3, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->updateCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    .line 42
    iput-object p4, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 43
    iput-object p5, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->updateReceivedResponse:Lcom/squareup/onboarding/ShareableReceivedResponse;

    return-void
.end method

.method private getAnswers()Lcom/squareup/server/activation/AnswersBody;
    .locals 7

    .line 96
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->getSelectedAnswerIndices()[I

    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getQuizQuestions()[Lcom/squareup/server/activation/QuizQuestion;

    move-result-object v1

    .line 99
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    .line 100
    :goto_0
    array-length v4, v1

    if-ge v3, v4, :cond_0

    .line 101
    aget-object v4, v1, v3

    .line 102
    iget-object v5, v4, Lcom/squareup/server/activation/QuizQuestion;->answers:[Ljava/lang/String;

    aget v6, v0, v3

    aget-object v5, v5, v6

    .line 103
    new-instance v6, Lcom/squareup/server/activation/QuizAnswer;

    iget-object v4, v4, Lcom/squareup/server/activation/QuizQuestion;->id:Ljava/lang/String;

    invoke-direct {v6, v4, v5}, Lcom/squareup/server/activation/QuizAnswer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 105
    :cond_0
    new-instance v0, Lcom/squareup/server/activation/AnswersBody;

    invoke-direct {v0, v2}, Lcom/squareup/server/activation/AnswersBody;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method public static synthetic lambda$CZxD_wTZn7WAyL23bZDpUvP8VKQ(Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->onPrimaryButtonTapped()V

    return-void
.end method

.method private onPrimaryButtonTapped()V
    .locals 3

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->isEveryQuestionAnswered()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONFIRM_IDENTITY_CONTINUE_MISSING:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 76
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    sget v2, Lcom/squareup/onboarding/flow/R$string;->onboarding_answer_all:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 79
    iget-object v1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONFIRM_IDENTITY_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->updateCallPresenter:Lcom/squareup/ui/onboarding/ActivationStatusPresenter;

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$ConfirmIdentityPresenter$uAvYK9rInpXklZJgC9-EmEKRioU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ConfirmIdentityPresenter$uAvYK9rInpXklZJgC9-EmEKRioU;-><init>(Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/ActivationStatusPresenter;->call(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onPrimaryButtonTapped$0$ConfirmIdentityPresenter()V
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->updateReceivedResponse:Lcom/squareup/onboarding/ShareableReceivedResponse;

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->getAnswers()Lcom/squareup/server/activation/AnswersBody;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/onboarding/ShareableReceivedResponse;->send(Ljava/lang/Object;)V

    return-void
.end method

.method onAnswerRadioTapped()V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONFIRM_IDENTITY_RADIO:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method protected onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method onLastAnswerRadioTapped()V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONFIRM_IDENTITY_RADIO_LAST:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 47
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;

    .line 50
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v1

    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 52
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    .line 53
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    new-instance v3, Lcom/squareup/ui/onboarding/-$$Lambda$ConfirmIdentityPresenter$CZxD_wTZn7WAyL23bZDpUvP8VKQ;

    invoke-direct {v3, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ConfirmIdentityPresenter$CZxD_wTZn7WAyL23bZDpUvP8VKQ;-><init>(Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;)V

    .line 54
    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v2

    .line 55
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v2

    .line 50
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    if-eqz p1, :cond_0

    const-string v1, "selected-answers"

    .line 59
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 61
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getQuizQuestions()[Lcom/squareup/server/activation/QuizQuestion;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->setQuestions([Lcom/squareup/server/activation/QuizQuestion;[I)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 65
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/ConfirmIdentityPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/ConfirmIdentityView;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/ConfirmIdentityView;->getSelectedAnswerIndices()[I

    move-result-object v0

    const-string v1, "selected-answers"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    return-void
.end method
