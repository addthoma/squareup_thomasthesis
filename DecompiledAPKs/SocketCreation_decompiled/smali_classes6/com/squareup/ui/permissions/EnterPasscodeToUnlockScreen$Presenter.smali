.class public Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "EnterPasscodeToUnlockScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;",
        ">;"
    }
.end annotation


# static fields
.field private static final PASSCODE_LENGTH:I = 0x4


# instance fields
.field private final curatedImage:Lcom/squareup/merchantimages/CuratedImage;

.field private final digits:Ljava/lang/StringBuilder;

.field private final employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

.field private enabled:Z

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private guestModeExpiryLastShownAt:Ljava/util/Date;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final timecardsLauncher:Lcom/squareup/ui/timecards/api/TimecardsLauncher;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/PasscodesSettings;Lcom/squareup/settings/server/EmployeeManagementSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lflow/Flow;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/settings/server/Features;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/timecards/api/TimecardsLauncher;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 83
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    .line 77
    iput-boolean v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->enabled:Z

    .line 84
    iput-object p1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 85
    iput-object p2, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    .line 86
    iput-object p3, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    .line 87
    iput-object p4, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 88
    iput-object p5, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->flow:Lflow/Flow;

    .line 89
    iput-object p6, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    .line 90
    iput-object p7, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    .line 91
    iput-object p8, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    .line 92
    iput-object p9, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->timecardsLauncher:Lcom/squareup/ui/timecards/api/TimecardsLauncher;

    return-void
.end method

.method private subscribeToBackgroundImage()V
    .locals 2

    .line 200
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/permissions/-$$Lambda$EnterPasscodeToUnlockScreen$Presenter$zSd2noKr6uazqhyFSs0gsHed-j0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/permissions/-$$Lambda$EnterPasscodeToUnlockScreen$Presenter$zSd2noKr6uazqhyFSs0gsHed-j0;-><init>(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private updateStars()V
    .locals 2

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 194
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    .line 195
    invoke-virtual {v1, v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->setStarCount(I)V

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 196
    :goto_0
    invoke-virtual {v1, v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->setClearEnabled(Z)V

    return-void
.end method


# virtual methods
.method getLegacyGuestModeExpirationDays(Ljava/util/Date;)J
    .locals 4

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 130
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getMerchantRegisterSettings()Lcom/squareup/settings/server/MerchantRegisterSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/MerchantRegisterSettings;->getLegacyGuestModeExpiration()Ljava/lang/String;

    move-result-object v0

    .line 129
    invoke-static {v0}, Lcom/squareup/util/Times;->tryParseIso8601Date(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    return-wide v0

    .line 134
    :cond_0
    invoke-static {p1, v0}, Lcom/squareup/util/Times;->countDaysBetween(Ljava/util/Date;Ljava/util/Date;)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public synthetic lambda$null$2$EnterPasscodeToUnlockScreen$Presenter(Lcom/squareup/picasso/RequestCreator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 205
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->setBackgroundImage(Lcom/squareup/picasso/RequestCreator;)V

    return-void
.end method

.method public synthetic lambda$passcodeAttempt$0$EnterPasscodeToUnlockScreen$Presenter(Lcom/squareup/permissions/Employee;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 165
    iget-object p1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Dismissed EnterPasscodeToUnlockScreen"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 166
    iput-boolean p1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->enabled:Z

    return-void
.end method

.method public synthetic lambda$passcodeAttempt$1$EnterPasscodeToUnlockScreen$Presenter()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 168
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->incorrectPasscode()V

    :cond_0
    const/4 v0, 0x1

    .line 172
    iput-boolean v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->enabled:Z

    return-void
.end method

.method public synthetic lambda$subscribeToBackgroundImage$3$EnterPasscodeToUnlockScreen$Presenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->curatedImage:Lcom/squareup/merchantimages/CuratedImage;

    invoke-interface {v0}, Lcom/squareup/merchantimages/CuratedImage;->load()Lio/reactivex/Observable;

    move-result-object v0

    .line 202
    invoke-static {}, Lcom/squareup/util/OptionalExtensionsKt;->mapIfPresentObservable()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/permissions/-$$Lambda$EnterPasscodeToUnlockScreen$Presenter$SbeASY8NraTm95vsRWd9OX-6jCg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/permissions/-$$Lambda$EnterPasscodeToUnlockScreen$Presenter$SbeASY8NraTm95vsRWd9OX-6jCg;-><init>(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;)V

    .line 203
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method onClearPressed()V
    .locals 3

    .line 150
    iget-boolean v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->enabled:Z

    if-nez v0, :cond_0

    return-void

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 152
    iget-object v1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 153
    invoke-direct {p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->updateStars()V

    return-void
.end method

.method onClockInOutClicked()V
    .locals 1

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->timecardsLauncher:Lcom/squareup/ui/timecards/api/TimecardsLauncher;

    invoke-interface {v0}, Lcom/squareup/ui/timecards/api/TimecardsLauncher;->showClockInOutFromPasscode()V

    return-void
.end method

.method onGuestUnlockClicked()V
    .locals 8

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Dismissed EnterPasscodeToUnlockScreen"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->unlockGuest()V

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 181
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 182
    invoke-virtual {p0, v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->getLegacyGuestModeExpirationDays(Ljava/util/Date;)J

    move-result-wide v1

    .line 183
    iget-object v3, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    invoke-virtual {v3}, Lcom/squareup/permissions/PasscodesSettings;->isTeamPermissionsEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_1

    iget-object v5, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->guestModeExpiryLastShownAt:Ljava/util/Date;

    if-eqz v5, :cond_0

    .line 185
    invoke-static {v5, v0}, Lcom/squareup/util/Times;->countDaysBetween(Ljava/util/Date;Ljava/util/Date;)J

    move-result-wide v5

    cmp-long v7, v5, v3

    if-lez v7, :cond_1

    .line 186
    :cond_0
    iput-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->guestModeExpiryLastShownAt:Ljava/util/Date;

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->flow:Lflow/Flow;

    new-instance v3, Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen;

    invoke-direct {v3, v1, v2}, Lcom/squareup/ui/permissions/LegacyGuestModeExpirationDialogScreen;-><init>(J)V

    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v3, v1}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    :cond_1
    return-void
.end method

.method onKeyPressed(C)V
    .locals 1

    .line 138
    iget-boolean v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->enabled:Z

    if-nez v0, :cond_0

    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 140
    iget-object p1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    .line 141
    invoke-direct {p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->updateStars()V

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 143
    iget-object p1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->passcodeAttempt(Ljava/lang/String;)V

    .line 145
    iget-object p1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_1
    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 96
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Shown EnterPasscodeToUnlockScreen"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    const/4 v0, 0x4

    .line 101
    invoke-virtual {p1, v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->setExpectedStars(I)V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->digits:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    .line 103
    iget-object v1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getBusinessName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->setTitleText(Ljava/lang/String;)V

    .line 104
    invoke-direct {p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->subscribeToBackgroundImage()V

    .line 105
    invoke-virtual {p1, v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->setStarCount(I)V

    .line 106
    iget-boolean v1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->enabled:Z

    invoke-virtual {p1, v1}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->setPasscodePadEnabled(Z)V

    .line 107
    iget-boolean v1, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->enabled:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->setClearEnabled(Z)V

    .line 108
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->showGuestButton()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->setGuestButtonVisible(Z)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isTimecardEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->setClockInOutButtonVisible(Z)V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    xor-int/2addr v0, v2

    invoke-virtual {p1, v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->setLegacyHeaderVisible(Z)V

    return-void
.end method

.method passcodeAttempt(Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x0

    .line 157
    iput-boolean v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->enabled:Z

    .line 158
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;

    .line 159
    invoke-virtual {v1, v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->setPasscodePadEnabled(Z)V

    .line 160
    invoke-virtual {v1, v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockView;->setClearEnabled(Z)V

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->unlock(Ljava/lang/String;)Lio/reactivex/Maybe;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/permissions/-$$Lambda$EnterPasscodeToUnlockScreen$Presenter$m7vUrI62mg7MetgGh2OF_Xr1sVc;

    invoke-direct {v0, p0}, Lcom/squareup/ui/permissions/-$$Lambda$EnterPasscodeToUnlockScreen$Presenter$m7vUrI62mg7MetgGh2OF_Xr1sVc;-><init>(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;)V

    .line 167
    invoke-static {}, Lio/reactivex/internal/functions/Functions;->emptyConsumer()Lio/reactivex/functions/Consumer;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/permissions/-$$Lambda$EnterPasscodeToUnlockScreen$Presenter$gQ-0pP_1yOJ8y5T-g9Ox2YuTOws;

    invoke-direct {v2, p0}, Lcom/squareup/ui/permissions/-$$Lambda$EnterPasscodeToUnlockScreen$Presenter$gQ-0pP_1yOJ8y5T-g9Ox2YuTOws;-><init>(Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;)V

    .line 164
    invoke-virtual {p1, v0, v1, v2}, Lio/reactivex/Maybe;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Action;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method showGuestButton()Z
    .locals 5

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->employeeManagementSettings:Lcom/squareup/settings/server/EmployeeManagementSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/EmployeeManagementSettings;->isGuestModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    .line 117
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->isTeamPermissionsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->passcodesSettings:Lcom/squareup/permissions/PasscodesSettings;

    .line 118
    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodesSettings;->getLatestState()Lcom/squareup/permissions/PasscodesSettings$State;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/permissions/PasscodesSettings$State;->teamPasscode:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 119
    invoke-virtual {p0, v0}, Lcom/squareup/ui/permissions/EnterPasscodeToUnlockScreen$Presenter;->getLegacyGuestModeExpirationDays(Ljava/util/Date;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
