.class public Lcom/squareup/ui/permissions/PermissionDeniedScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "PermissionDeniedScreen.java"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/permissions/PermissionDeniedScreen$Factory;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/permissions/PermissionDeniedScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;,
        Lcom/squareup/ui/permissions/PermissionDeniedScreen$Component;,
        Lcom/squareup/ui/permissions/PermissionDeniedScreen$ParentComponent;,
        Lcom/squareup/ui/permissions/PermissionDeniedScreen$Factory;
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/permissions/PermissionDeniedScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/ui/permissions/PermissionDeniedScreen;

    invoke-direct {v0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/permissions/PermissionDeniedScreen;->INSTANCE:Lcom/squareup/ui/permissions/PermissionDeniedScreen;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
