.class Lcom/squareup/ui/permissions/PermissionDeniedView$2;
.super Lcom/squareup/padlock/Padlock$OnKeyPressListenerAdapter;
.source "PermissionDeniedView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/permissions/PermissionDeniedView;->showPasscodePad()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/permissions/PermissionDeniedView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/permissions/PermissionDeniedView;)V
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/ui/permissions/PermissionDeniedView$2;->this$0:Lcom/squareup/ui/permissions/PermissionDeniedView;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock$OnKeyPressListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onClearClicked()V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView$2;->this$0:Lcom/squareup/ui/permissions/PermissionDeniedView;

    iget-object v0, v0, Lcom/squareup/ui/permissions/PermissionDeniedView;->presenter:Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->onClearPressed()V

    return-void
.end method

.method public onClearLongpressed()V
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView$2;->this$0:Lcom/squareup/ui/permissions/PermissionDeniedView;

    iget-object v0, v0, Lcom/squareup/ui/permissions/PermissionDeniedView;->presenter:Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->onClearPressed()V

    return-void
.end method

.method public onDigitClicked(I)V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView$2;->this$0:Lcom/squareup/ui/permissions/PermissionDeniedView;

    invoke-static {v0}, Lcom/squareup/ui/permissions/PermissionDeniedView;->access$000(Lcom/squareup/ui/permissions/PermissionDeniedView;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/permissions/PermissionDeniedView$2;->this$0:Lcom/squareup/ui/permissions/PermissionDeniedView;

    iget-object v0, v0, Lcom/squareup/ui/permissions/PermissionDeniedView;->presenter:Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->onKeyPressed(C)V

    return-void
.end method
