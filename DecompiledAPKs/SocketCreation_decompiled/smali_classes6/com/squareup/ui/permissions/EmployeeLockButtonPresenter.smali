.class public Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;
.super Lmortar/Presenter;
.source "EmployeeLockButtonPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;",
        ">;"
    }
.end annotation


# instance fields
.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final lockOrClockButtonHelper:Lcom/squareup/ui/main/LockOrClockButtonHelper;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/ui/main/LockOrClockButtonHelper;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 53
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    .line 55
    iput-object p2, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->lockOrClockButtonHelper:Lcom/squareup/ui/main/LockOrClockButtonHelper;

    .line 57
    iput-object p4, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->res:Lcom/squareup/util/Res;

    .line 58
    iput-object p5, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method private employeeIsGuest()Z
    .locals 1

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->lockOrClockButtonHelper:Lcom/squareup/ui/main/LockOrClockButtonHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/main/LockOrClockButtonHelper;->employeeIsGuest()Z

    move-result v0

    return v0
.end method

.method private getLogInText()Ljava/lang/String;
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->employee_management_lock_button_login:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getLogOutText()Ljava/lang/String;
    .locals 3

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->employee_management_lock_button_logout:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 142
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->getEmployeeInitials()Ljava/lang/String;

    move-result-object v1

    const-string v2, "initials"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 143
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 144
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private shouldShowButton()Z
    .locals 1

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->lockOrClockButtonHelper:Lcom/squareup/ui/main/LockOrClockButtonHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/main/LockOrClockButtonHelper;->shouldShowLockButton()Z

    move-result v0

    return v0
.end method

.method private showClockInButton()Z
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    invoke-interface {v0}, Lcom/squareup/permissions/EmployeeManagement;->shouldShowClockInButton()Z

    move-result v0

    return v0
.end method

.method private updateView()V
    .locals 3

    .line 101
    invoke-direct {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->shouldShowButton()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 102
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;

    .line 104
    invoke-interface {v0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;->showButton()V

    .line 105
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->getContentDescription()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;->setDescription(Ljava/lang/String;)V

    .line 107
    invoke-direct {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->showClockInButton()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    invoke-interface {v0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;->showClock()V

    goto :goto_1

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->USE_UPDATED_GUEST_ACCESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 111
    invoke-direct {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->employeeIsGuest()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->getLogInText()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->getLogOutText()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;->showLoginOut(Ljava/lang/String;)V

    goto :goto_1

    .line 112
    :cond_2
    invoke-direct {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->employeeIsGuest()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 113
    invoke-interface {v0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;->showLegacyGuest()V

    goto :goto_1

    .line 115
    :cond_3
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->getEmployeeInitials()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;->showLegacyLoginOut(Ljava/lang/String;)V

    :cond_4
    :goto_1
    return-void
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;)Lmortar/bundler/BundleService;
    .locals 0

    .line 62
    invoke-interface {p1}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->extractBundleService(Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public getContentDescription()Ljava/lang/String;
    .locals 3

    .line 75
    invoke-direct {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->showClockInButton()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->employee_management_clock_in_button:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 77
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->employeeIsGuest()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->employee_management_lock_button_login:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->employee_management_lock_button_logout:I

    .line 81
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 82
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->getEmployeeInitials()Ljava/lang/String;

    move-result-object v1

    const-string v2, "initials"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 84
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getEmployeeFirstName()Ljava/lang/String;
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->getCurrentEmployee()Lcom/squareup/permissions/Employee;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/permissions/Employee;->firstName:Ljava/lang/String;

    return-object v0
.end method

.method getEmployeeInitials()Ljava/lang/String;
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->lockOrClockButtonHelper:Lcom/squareup/ui/main/LockOrClockButtonHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/main/LockOrClockButtonHelper;->getEmployeeInitials()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$EmployeeLockButtonPresenter(Lcom/squareup/util/Optional;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 69
    invoke-direct {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->updateView()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$EmployeeLockButtonPresenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->currentEmployee()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/permissions/-$$Lambda$EmployeeLockButtonPresenter$kojk3RNsTWNIMVbAD-Zxk53sKOU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/permissions/-$$Lambda$EmployeeLockButtonPresenter$kojk3RNsTWNIMVbAD-Zxk53sKOU;-><init>(Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;)V

    .line 69
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method lockButtonClicked()V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->lockOrClockButtonHelper:Lcom/squareup/ui/main/LockOrClockButtonHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/main/LockOrClockButtonHelper;->lockOrClockButtonClicked()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 66
    invoke-super {p0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    .line 67
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;

    invoke-interface {p1}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;->asView()Landroid/view/View;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/permissions/-$$Lambda$EmployeeLockButtonPresenter$zcPZ7Ck4yx1_C9is0SKGVK8O86A;

    invoke-direct {v0, p0}, Lcom/squareup/ui/permissions/-$$Lambda$EmployeeLockButtonPresenter$zcPZ7Ck4yx1_C9is0SKGVK8O86A;-><init>(Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 71
    invoke-direct {p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->updateView()V

    return-void
.end method
