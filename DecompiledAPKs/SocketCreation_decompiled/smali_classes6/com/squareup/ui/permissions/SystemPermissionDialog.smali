.class public Lcom/squareup/ui/permissions/SystemPermissionDialog;
.super Lcom/squareup/dialog/GlassDialog;
.source "SystemPermissionDialog.java"


# instance fields
.field appNameFormatter:Lcom/squareup/util/AppNameFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/systempermissions/SystemPermission;I)V
    .locals 6

    .line 36
    sget v5, Lcom/squareup/crmupdatecustomer/R$layout;->system_permission_dialog:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/permissions/SystemPermissionDialog;-><init>(Landroid/content/Context;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/systempermissions/SystemPermission;II)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/systempermissions/SystemPermission;II)V
    .locals 1

    .line 42
    sget v0, Lcom/squareup/noho/R$style;->Theme_Noho_Dialog_NoBackground:I

    invoke-direct {p0, p1, v0}, Lcom/squareup/dialog/GlassDialog;-><init>(Landroid/content/Context;I)V

    .line 43
    const-class v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;->inject(Lcom/squareup/ui/permissions/SystemPermissionDialog;)V

    .line 45
    invoke-virtual {p0, p5}, Lcom/squareup/ui/permissions/SystemPermissionDialog;->setContentView(I)V

    .line 46
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/SystemPermissionDialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object p1

    const/4 p5, -0x1

    iput p5, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 47
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/SystemPermissionDialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object p1

    iput p5, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    const/4 p1, 0x0

    .line 48
    invoke-virtual {p0, p1}, Lcom/squareup/ui/permissions/SystemPermissionDialog;->setCanceledOnTouchOutside(Z)V

    .line 49
    invoke-virtual {p0, p1}, Lcom/squareup/ui/permissions/SystemPermissionDialog;->setCancelable(Z)V

    .line 51
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/SystemPermissionDialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    .line 52
    sget p5, Lcom/squareup/crmupdatecustomer/R$id;->permission_glyph:I

    invoke-static {p1, p5}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p5

    check-cast p5, Lcom/squareup/glyph/SquareGlyphView;

    .line 53
    invoke-static {p3}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->permission(Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v0

    invoke-virtual {p5, v0}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 54
    sget p5, Lcom/squareup/crmupdatecustomer/R$id;->permission_title:I

    invoke-static {p1, p5}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p5

    check-cast p5, Lcom/squareup/widgets/MessageView;

    .line 55
    invoke-static {p3}, Lcom/squareup/ui/systempermissions/PermissionMessages;->getExplanationTitle(Lcom/squareup/systempermissions/SystemPermission;)I

    move-result v0

    invoke-virtual {p5, v0}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 56
    sget p5, Lcom/squareup/crmupdatecustomer/R$id;->permission_explanation:I

    invoke-static {p1, p5}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p5

    check-cast p5, Lcom/squareup/widgets/MessageView;

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/permissions/SystemPermissionDialog;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    invoke-interface {v0, p4}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object p4

    invoke-virtual {p5, p4}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    sget p4, Lcom/squareup/crmupdatecustomer/R$id;->permission_enable:I

    invoke-static {p1, p4}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p4

    check-cast p4, Landroid/widget/Button;

    .line 61
    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/permissions/SystemPermissionDialog;->buttonText(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/systempermissions/SystemPermission;)I

    move-result p5

    invoke-virtual {p4, p5}, Landroid/widget/Button;->setText(I)V

    .line 62
    new-instance p5, Lcom/squareup/ui/permissions/SystemPermissionDialog$1;

    invoke-direct {p5, p0, p2, p3}, Lcom/squareup/ui/permissions/SystemPermissionDialog$1;-><init>(Lcom/squareup/ui/permissions/SystemPermissionDialog;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/systempermissions/SystemPermission;)V

    invoke-virtual {p4, p5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    sget p2, Lcom/squareup/crmupdatecustomer/R$id;->permission_cancel:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 70
    new-instance p2, Lcom/squareup/ui/permissions/SystemPermissionDialog$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/permissions/SystemPermissionDialog$2;-><init>(Lcom/squareup/ui/permissions/SystemPermissionDialog;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method private buttonText(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/systempermissions/SystemPermission;)I
    .locals 0

    .line 86
    invoke-virtual {p1, p2}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->canAskDirectly(Lcom/squareup/systempermissions/SystemPermission;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 87
    sget p1, Lcom/squareup/utilities/R$string;->system_permission_settings_button:I

    return p1

    .line 90
    :cond_0
    invoke-static {p2}, Lcom/squareup/ui/systempermissions/PermissionMessages;->buttonText(Lcom/squareup/systempermissions/SystemPermission;)I

    move-result p1

    return p1
.end method


# virtual methods
.method onRequestPermission(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/systempermissions/SystemPermission;)V
    .locals 0

    .line 80
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/SystemPermissionDialog;->dismiss()V

    .line 81
    invoke-virtual {p1, p2}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->requestPermission(Lcom/squareup/systempermissions/SystemPermission;)V

    return-void
.end method
