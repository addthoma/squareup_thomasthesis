.class public final Lcom/squareup/ui/main/CommonMainActivityModule_ProvideImpersonatingFactory;
.super Ljava/lang/Object;
.source "CommonMainActivityModule_ProvideImpersonatingFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/main/CommonMainActivityModule_ProvideImpersonatingFactory;->settingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/main/CommonMainActivityModule_ProvideImpersonatingFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;)",
            "Lcom/squareup/ui/main/CommonMainActivityModule_ProvideImpersonatingFactory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/main/CommonMainActivityModule_ProvideImpersonatingFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/CommonMainActivityModule_ProvideImpersonatingFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideImpersonating(Lcom/squareup/settings/server/AccountStatusSettings;)Z
    .locals 0

    .line 35
    invoke-static {p0}, Lcom/squareup/ui/main/CommonMainActivityModule;->provideImpersonating(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/main/CommonMainActivityModule_ProvideImpersonatingFactory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {v0}, Lcom/squareup/ui/main/CommonMainActivityModule_ProvideImpersonatingFactory;->provideImpersonating(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/main/CommonMainActivityModule_ProvideImpersonatingFactory;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
