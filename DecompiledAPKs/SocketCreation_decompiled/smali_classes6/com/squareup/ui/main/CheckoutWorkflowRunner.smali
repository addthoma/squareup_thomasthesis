.class public interface abstract Lcom/squareup/ui/main/CheckoutWorkflowRunner;
.super Ljava/lang/Object;
.source "CheckoutWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/CheckoutWorkflowRunner$ParentComponent;,
        Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;,
        Lcom/squareup/ui/main/CheckoutWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowRunner<",
        "Lcom/squareup/checkoutflow/CheckoutResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u0000 \u000f2\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u000e\u000f\u0010J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0012\u0010\u0007\u001a\u00020\u00042\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH\'J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH&\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
        "Lcom/squareup/container/PosWorkflowRunner;",
        "Lcom/squareup/checkoutflow/CheckoutResult;",
        "isPaymentWorkflowScreen",
        "",
        "key",
        "Lflow/path/Path;",
        "isSelectMethodScreen",
        "screen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "start",
        "",
        "startArg",
        "Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;",
        "CheckoutScopeParentProvider",
        "Companion",
        "ParentComponent",
        "pos-main-workflow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/main/CheckoutWorkflowRunner$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/ui/main/CheckoutWorkflowRunner$Companion;->$$INSTANCE:Lcom/squareup/ui/main/CheckoutWorkflowRunner$Companion;

    sput-object v0, Lcom/squareup/ui/main/CheckoutWorkflowRunner;->Companion:Lcom/squareup/ui/main/CheckoutWorkflowRunner$Companion;

    return-void
.end method


# virtual methods
.method public abstract isPaymentWorkflowScreen(Lflow/path/Path;)Z
.end method

.method public abstract isSelectMethodScreen(Lcom/squareup/container/ContainerTreeKey;)Z
    .annotation runtime Lkotlin/Deprecated;
        message = "shameful hook solely for use by the first payment tutorial v1 "
    .end annotation
.end method

.method public abstract start(Lcom/squareup/checkoutflow/CheckoutWorkflow$CheckoutflowConfig;)V
.end method
