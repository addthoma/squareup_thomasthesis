.class public final Lcom/squareup/ui/main/ImpersonatingBanner_Factory;
.super Ljava/lang/Object;
.source "ImpersonatingBanner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/ImpersonatingBanner;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final isImpersonatingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/main/ImpersonatingBanner_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/main/ImpersonatingBanner_Factory;->isImpersonatingProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/ImpersonatingBanner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/ui/main/ImpersonatingBanner_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/main/ImpersonatingBanner_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/main/ImpersonatingBanner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Z)Lcom/squareup/ui/main/ImpersonatingBanner;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/ui/main/ImpersonatingBanner;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/main/ImpersonatingBanner;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Z)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/ImpersonatingBanner;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/main/ImpersonatingBanner_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, p0, Lcom/squareup/ui/main/ImpersonatingBanner_Factory;->isImpersonatingProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/ui/main/ImpersonatingBanner_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Z)Lcom/squareup/ui/main/ImpersonatingBanner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/main/ImpersonatingBanner_Factory;->get()Lcom/squareup/ui/main/ImpersonatingBanner;

    move-result-object v0

    return-object v0
.end method
