.class public final Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;
.super Ljava/lang/Object;
.source "CommonRedirectPipelineDecorator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;",
        ">;"
    }
.end annotation


# instance fields
.field private final appIdlingProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;"
        }
    .end annotation
.end field

.field private final appletsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final homeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;"
        }
    .end annotation
.end field

.field private final homeScreenSelectorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/HomeScreenSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final remoteLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final squareDeviceTourProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/SquareDeviceTour;",
            ">;"
        }
    .end annotation
.end field

.field private final whatsNewUiProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewUi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/SquareDeviceTour;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewUi;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/HomeScreenSelector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->squareDeviceTourProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p2, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->whatsNewUiProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p3, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->appletsProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p4, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p5, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->homeScreenSelectorProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p6, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->homeProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p7, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->appIdlingProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p8, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p9, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->remoteLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/SquareDeviceTour;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tour/WhatsNewUi;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/HomeScreenSelector;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/AppIdling;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;)",
            "Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;"
        }
    .end annotation

    .line 70
    new-instance v10, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/SquareDeviceTour;Lcom/squareup/tour/WhatsNewUi;Lcom/squareup/applet/Applets;Lcom/squareup/util/Device;Lcom/squareup/ui/main/HomeScreenSelector;Lcom/squareup/ui/main/Home;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/logging/RemoteLogger;)Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;
    .locals 11

    .line 77
    new-instance v10, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;-><init>(Lcom/squareup/SquareDeviceTour;Lcom/squareup/tour/WhatsNewUi;Lcom/squareup/applet/Applets;Lcom/squareup/util/Device;Lcom/squareup/ui/main/HomeScreenSelector;Lcom/squareup/ui/main/Home;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/logging/RemoteLogger;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;
    .locals 10

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->squareDeviceTourProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/SquareDeviceTour;

    iget-object v0, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->whatsNewUiProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/tour/WhatsNewUi;

    iget-object v0, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->appletsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/applet/Applets;

    iget-object v0, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->homeScreenSelectorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/main/HomeScreenSelector;

    iget-object v0, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->homeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/main/Home;

    iget-object v0, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->appIdlingProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/main/AppIdling;

    iget-object v0, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v0, p0, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->remoteLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/logging/RemoteLogger;

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->newInstance(Lcom/squareup/SquareDeviceTour;Lcom/squareup/tour/WhatsNewUi;Lcom/squareup/applet/Applets;Lcom/squareup/util/Device;Lcom/squareup/ui/main/HomeScreenSelector;Lcom/squareup/ui/main/Home;Lcom/squareup/ui/main/AppIdling;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/logging/RemoteLogger;)Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/main/CommonRedirectPipelineDecorator_Factory;->get()Lcom/squareup/ui/main/CommonRedirectPipelineDecorator;

    move-result-object v0

    return-object v0
.end method
