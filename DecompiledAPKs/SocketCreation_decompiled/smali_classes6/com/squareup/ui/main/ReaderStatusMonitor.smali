.class public Lcom/squareup/ui/main/ReaderStatusMonitor;
.super Ljava/lang/Object;
.source "ReaderStatusMonitor.java"

# interfaces
.implements Lmortar/Scoped;


# static fields
.field private static final BLOCKED_AUDIO_DIALOG:Lcom/squareup/register/widgets/WarningDialogScreen;

.field private static final SAMPLE_RATE_UNSET_DIALOG:Lcom/squareup/register/widgets/WarningDialogScreen;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

.field private final application:Landroid/app/Application;

.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field private final cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final posContainer:Lcom/squareup/ui/main/PosContainer;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 47
    new-instance v0, Lcom/squareup/register/widgets/WarningDialogScreen;

    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->blocked_audio_title:I

    sget v3, Lcom/squareup/cardreader/ui/R$string;->blocked_audio_message:I

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/squareup/register/widgets/WarningDialogScreen;

    .line 51
    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "-BLOCKED_AUDIO"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;ZLjava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/main/ReaderStatusMonitor;->BLOCKED_AUDIO_DIALOG:Lcom/squareup/register/widgets/WarningDialogScreen;

    .line 53
    new-instance v0, Lcom/squareup/register/widgets/WarningDialogScreen;

    new-instance v1, Lcom/squareup/widgets/warning/WarningIds;

    sget v2, Lcom/squareup/cardreader/ui/R$string;->sample_rate_unset_title:I

    sget v3, Lcom/squareup/cardreader/ui/R$string;->sample_rate_unset_message:I

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/squareup/register/widgets/WarningDialogScreen;

    .line 57
    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "-SAMPLE_RATE_UNSET"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v3, v2}, Lcom/squareup/register/widgets/WarningDialogScreen;-><init>(Lcom/squareup/widgets/warning/Warning;ZLjava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/main/ReaderStatusMonitor;->SAMPLE_RATE_UNSET_DIALOG:Lcom/squareup/register/widgets/WarningDialogScreen;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/badbus/BadBus;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/settings/server/Features;Ldagger/Lazy;Lcom/squareup/ui/main/PosContainer;Landroid/app/Application;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            "Lcom/squareup/settings/server/Features;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Landroid/app/Application;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->analytics:Lcom/squareup/analytics/Analytics;

    .line 76
    iput-object p2, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    .line 77
    iput-object p9, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->application:Landroid/app/Application;

    .line 78
    iput-object p3, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->badBus:Lcom/squareup/badbus/BadBus;

    .line 79
    iput-object p4, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    .line 80
    iput-object p5, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 81
    iput-object p6, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->features:Lcom/squareup/settings/server/Features;

    .line 82
    iput-object p7, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->flow:Ldagger/Lazy;

    .line 83
    iput-object p8, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->posContainer:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method

.method private dismissAnyRecorderErrorDialog()V
    .locals 4

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/main/-$$Lambda$ReaderStatusMonitor$NzUECBByuTX9OBhBb3ABKhRqlaQ;

    invoke-direct {v2, p0}, Lcom/squareup/ui/main/-$$Lambda$ReaderStatusMonitor$NzUECBByuTX9OBhBb3ABKhRqlaQ;-><init>(Lcom/squareup/ui/main/ReaderStatusMonitor;)V

    const-string v3, "dismiss dialogs"

    invoke-direct {v1, v3, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic lambda$gFXjj0WwctSpAnQuQk5e0MdA4dU(Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/wavpool/swipe/Recorder$State;Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/main/ReaderStatusMonitor;->setRecorderState(Lcom/squareup/wavpool/swipe/Recorder$State;Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method

.method private setRecorderState(Lcom/squareup/wavpool/swipe/Recorder$State;Lcom/squareup/container/ContainerTreeKey;)V
    .locals 2

    .line 112
    sget-object v0, Lcom/squareup/ui/main/ReaderStatusMonitor$1;->$SwitchMap$com$squareup$wavpool$swipe$Recorder$State:[I

    invoke-virtual {p1}, Lcom/squareup/wavpool/swipe/Recorder$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 142
    new-instance p2, Ljava/lang/AssertionError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unhandled recorder state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p2

    .line 139
    :pswitch_0
    invoke-direct {p0}, Lcom/squareup/ui/main/ReaderStatusMonitor;->dismissAnyRecorderErrorDialog()V

    goto :goto_0

    .line 117
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->IGNORE_SYSTEM_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-nez p1, :cond_1

    sget-object p1, Lcom/squareup/systempermissions/SystemPermission;->MICROPHONE:Lcom/squareup/systempermissions/SystemPermission;

    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->application:Landroid/app/Application;

    .line 118
    invoke-virtual {p1, v0}, Lcom/squareup/systempermissions/SystemPermission;->hasPermission(Landroid/content/Context;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 120
    const-class p1, Lcom/squareup/container/layer/FullSheet;

    invoke-static {p2, p1}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 121
    iget-object p1, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderHubUtils;->maybeGetAudioReader()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 123
    iget-object p2, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;->READER_MIC_PERMISSION_MISSING:Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;

    invoke-virtual {p2, p1, v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->startEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/ui/settings/paymentdevices/ReaderState$Type;)V

    .line 127
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/main/ReaderStatusMonitor;->showAudioPermissionCard()V

    goto :goto_0

    .line 130
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {}, Lcom/squareup/analytics/event/v1/AudioBlockingDialogEvent;->showing()Lcom/squareup/analytics/event/v1/AudioBlockingDialogEvent;

    move-result-object p2

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 132
    iget-object p1, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->flow:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Flow;

    sget-object p2, Lcom/squareup/ui/main/ReaderStatusMonitor;->BLOCKED_AUDIO_DIALOG:Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 114
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->flow:Ldagger/Lazy;

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lflow/Flow;

    sget-object p2, Lcom/squareup/ui/main/ReaderStatusMonitor;->SAMPLE_RATE_UNSET_DIALOG:Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-virtual {p1, p2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public synthetic lambda$dismissAnyRecorderErrorDialog$0$ReaderStatusMonitor(Lflow/History;)Lcom/squareup/container/Command;
    .locals 6

    .line 153
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    .line 156
    invoke-virtual {p1}, Lflow/History;->framesFromBottom()Ljava/lang/Iterable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 157
    sget-object v5, Lcom/squareup/ui/main/ReaderStatusMonitor;->SAMPLE_RATE_UNSET_DIALOG:Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    .line 161
    :cond_0
    sget-object v5, Lcom/squareup/ui/main/ReaderStatusMonitor;->BLOCKED_AUDIO_DIALOG:Lcom/squareup/register/widgets/WarningDialogScreen;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 162
    iget-object v3, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {}, Lcom/squareup/analytics/event/v1/AudioBlockingDialogEvent;->dismissing()Lcom/squareup/analytics/event/v1/AudioBlockingDialogEvent;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_1

    .line 166
    :cond_1
    invoke-virtual {v0, v4}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_0

    :cond_2
    if-eqz v3, :cond_3

    .line 169
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    sget-object v0, Lflow/Direction;->BACKWARD:Lflow/Direction;

    goto :goto_2

    :cond_3
    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    :goto_2
    invoke-static {p1, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/wavpool/swipe/Recorder$State;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->posContainer:Lcom/squareup/ui/main/PosContainer;

    .line 88
    invoke-static {v1}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-static {}, Lcom/squareup/util/Rx2Tuples;->toPair()Lio/reactivex/functions/BiFunction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/main/-$$Lambda$ReaderStatusMonitor$gFXjj0WwctSpAnQuQk5e0MdA4dU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/-$$Lambda$ReaderStatusMonitor$gFXjj0WwctSpAnQuQk5e0MdA4dU;-><init>(Lcom/squareup/ui/main/ReaderStatusMonitor;)V

    .line 89
    invoke-static {v1}, Lcom/squareup/util/Rx2Tuples;->expandPair(Lio/reactivex/functions/BiConsumer;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 87
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public showAudioPermissionCard()V
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    invoke-static {}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;->createAudioPermissionNoTitleText()Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showCardReaderCard()V
    .locals 3

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiReaderSettingsController;->isApiReaderSettingsRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    .line 103
    invoke-static {}, Lflow/History;->emptyBuilder()Lflow/History$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/main/BlankScreen;->INSTANCE:Lcom/squareup/ui/main/BlankScreen;

    invoke-virtual {v1, v2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;

    invoke-virtual {v1, v2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v1

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    .line 102
    invoke-virtual {v0, v1, v2}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    goto :goto_0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/ReaderStatusMonitor;->flow:Ldagger/Lazy;

    invoke-interface {v0}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;->INSTANCE:Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
