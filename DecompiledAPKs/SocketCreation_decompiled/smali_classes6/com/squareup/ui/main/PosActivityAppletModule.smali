.class public abstract Lcom/squareup/ui/main/PosActivityAppletModule;
.super Ljava/lang/Object;
.source "PosActivityAppletModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideActivitySearchPaymentStarter(Lcom/squareup/ui/activity/PosActivitySearchPaymentStarter;)Lcom/squareup/ui/activity/ActivitySearchPaymentStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
