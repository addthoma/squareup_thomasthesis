.class public final Lcom/squareup/ui/main/PosIntentParser_Factory;
.super Ljava/lang/Object;
.source "PosIntentParser_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/PosIntentParser;",
        ">;"
    }
.end annotation


# instance fields
.field private final adAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final apiTransactionFactoryParserProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ApiTransactionFactory$Parser;",
            ">;"
        }
    .end annotation
.end field

.field private final appletsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;"
        }
    .end annotation
.end field

.field private final deepLinksProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;"
        }
    .end annotation
.end field

.field private final deferredDeepLinkPreferenceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final ohSnapProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final timecardsLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ApiTransactionFactory$Parser;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p2, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->adAnalyticsProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p3, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p4, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->ohSnapProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p5, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->apiTransactionFactoryParserProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p6, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->deepLinksProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p7, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->appletsProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p8, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->timecardsLauncherProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p9, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->deferredDeepLinkPreferenceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/PosIntentParser_Factory;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ApiTransactionFactory$Parser;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/DeepLinks;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/Applets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;>;)",
            "Lcom/squareup/ui/main/PosIntentParser_Factory;"
        }
    .end annotation

    .line 71
    new-instance v10, Lcom/squareup/ui/main/PosIntentParser_Factory;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/main/PosIntentParser_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v10
.end method

.method public static newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;Ljava/lang/Object;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/applet/Applets;Lcom/squareup/ui/timecards/api/TimecardsLauncher;Lcom/f2prateek/rx/preferences2/Preference;)Lcom/squareup/ui/main/PosIntentParser;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/log/OhSnapLogger;",
            "Ljava/lang/Object;",
            "Lcom/squareup/ui/main/DeepLinks;",
            "Lcom/squareup/applet/Applets;",
            "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/ui/main/PosIntentParser;"
        }
    .end annotation

    .line 78
    new-instance v10, Lcom/squareup/ui/main/PosIntentParser;

    move-object v5, p4

    check-cast v5, Lcom/squareup/ui/main/ApiTransactionFactory$Parser;

    move-object v0, v10

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/main/PosIntentParser;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/ui/main/ApiTransactionFactory$Parser;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/applet/Applets;Lcom/squareup/ui/timecards/api/TimecardsLauncher;Lcom/f2prateek/rx/preferences2/Preference;)V

    return-object v10
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/PosIntentParser;
    .locals 10

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->adAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/adanalytics/AdAnalytics;

    iget-object v0, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->ohSnapProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/log/OhSnapLogger;

    iget-object v0, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->apiTransactionFactoryParserProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v5

    iget-object v0, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->deepLinksProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/main/DeepLinks;

    iget-object v0, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->appletsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/applet/Applets;

    iget-object v0, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->timecardsLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/timecards/api/TimecardsLauncher;

    iget-object v0, p0, Lcom/squareup/ui/main/PosIntentParser_Factory;->deferredDeepLinkPreferenceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/f2prateek/rx/preferences2/Preference;

    invoke-static/range {v1 .. v9}, Lcom/squareup/ui/main/PosIntentParser_Factory;->newInstance(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/analytics/Analytics;Lcom/squareup/log/OhSnapLogger;Ljava/lang/Object;Lcom/squareup/ui/main/DeepLinks;Lcom/squareup/applet/Applets;Lcom/squareup/ui/timecards/api/TimecardsLauncher;Lcom/f2prateek/rx/preferences2/Preference;)Lcom/squareup/ui/main/PosIntentParser;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/main/PosIntentParser_Factory;->get()Lcom/squareup/ui/main/PosIntentParser;

    move-result-object v0

    return-object v0
.end method
