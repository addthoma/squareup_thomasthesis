.class public Lcom/squareup/ui/main/AddNoteScreen$Module;
.super Ljava/lang/Object;
.source "AddNoteScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/AddNoteScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static providesAddNoteBundleKey()Lcom/squareup/BundleKey;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Named;
        value = "AddNote"
    .end annotation

    const-string v0, "addNote"

    .line 110
    invoke-static {v0}, Lcom/squareup/BundleKey;->string(Ljava/lang/String;)Lcom/squareup/BundleKey;

    move-result-object v0

    return-object v0
.end method
