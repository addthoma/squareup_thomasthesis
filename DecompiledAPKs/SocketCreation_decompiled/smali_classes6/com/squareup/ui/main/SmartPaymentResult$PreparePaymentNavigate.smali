.class public final Lcom/squareup/ui/main/SmartPaymentResult$PreparePaymentNavigate;
.super Ljava/lang/Object;
.source "SmartPaymentResult.java"

# interfaces
.implements Lcom/squareup/ui/main/SmartPaymentResult;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/SmartPaymentResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PreparePaymentNavigate"
.end annotation


# instance fields
.field public final cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/squareup/ui/main/SmartPaymentResult$PreparePaymentNavigate;->cardReaderInfo:Lcom/squareup/cardreader/CardReaderInfo;

    return-void
.end method


# virtual methods
.method public dispatch(Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;)V
    .locals 0

    .line 76
    invoke-interface {p1, p0}, Lcom/squareup/ui/main/SmartPaymentResult$SmartPaymentResultHandler;->onResult(Lcom/squareup/ui/main/SmartPaymentResult$PreparePaymentNavigate;)V

    return-void
.end method
