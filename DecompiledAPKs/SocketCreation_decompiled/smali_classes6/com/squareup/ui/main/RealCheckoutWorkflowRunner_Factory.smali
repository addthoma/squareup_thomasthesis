.class public final Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "RealCheckoutWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final checkoutScopeParentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentProcessingEventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/PaymentViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final workflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/CheckoutWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/CheckoutWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/PaymentViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;->paymentViewFactoryProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;->checkoutScopeParentProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;->paymentProcessingEventSinkProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/CheckoutWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/PaymentViewFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
            ">;)",
            "Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/checkoutflow/CheckoutWorkflow;Lcom/squareup/tenderpayment/PaymentViewFactory;Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;
    .locals 7

    .line 59
    new-instance v6, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;-><init>(Lcom/squareup/checkoutflow/CheckoutWorkflow;Lcom/squareup/tenderpayment/PaymentViewFactory;Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;
    .locals 5

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/checkoutflow/CheckoutWorkflow;

    iget-object v1, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;->paymentViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/tenderpayment/PaymentViewFactory;

    iget-object v2, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;->checkoutScopeParentProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;

    iget-object v3, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/main/PosContainer;

    iget-object v4, p0, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;->paymentProcessingEventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;->newInstance(Lcom/squareup/checkoutflow/CheckoutWorkflow;Lcom/squareup/tenderpayment/PaymentViewFactory;Lcom/squareup/ui/main/CheckoutWorkflowRunner$CheckoutScopeParentProvider;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/main/RealCheckoutWorkflowRunner_Factory;->get()Lcom/squareup/ui/main/RealCheckoutWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
