.class public Lcom/squareup/ui/main/AddNoteScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "AddNoteScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/AddNoteScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/main/AddNoteView;",
        ">;"
    }
.end annotation


# instance fields
.field private final addNoteScreenRunner:Lcom/squareup/ui/main/AddNoteScreenRunner;

.field private final bundleKey:Lcom/squareup/BundleKey;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final diag:Lcom/squareup/ui/DiagnosticCrasher;

.field private final res:Lcom/squareup/util/Res;

.field private final toastFactory:Lcom/squareup/util/ToastFactory;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/DiagnosticCrasher;Lcom/squareup/ui/main/AddNoteScreenRunner;Lcom/squareup/util/ToastFactory;Lcom/squareup/BundleKey;)V
    .locals 0
    .param p6    # Lcom/squareup/BundleKey;
        .annotation runtime Ljavax/inject/Named;
            value = "AddNote"
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/ui/DiagnosticCrasher;",
            "Lcom/squareup/ui/main/AddNoteScreenRunner;",
            "Lcom/squareup/util/ToastFactory;",
            "Lcom/squareup/BundleKey<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 51
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 53
    iput-object p2, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 54
    iput-object p3, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->diag:Lcom/squareup/ui/DiagnosticCrasher;

    .line 55
    iput-object p4, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->addNoteScreenRunner:Lcom/squareup/ui/main/AddNoteScreenRunner;

    .line 56
    iput-object p5, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->toastFactory:Lcom/squareup/util/ToastFactory;

    .line 57
    iput-object p6, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->bundleKey:Lcom/squareup/BundleKey;

    return-void
.end method


# virtual methods
.method done(Z)V
    .locals 3

    .line 90
    invoke-virtual {p0}, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/AddNoteView;

    if-eqz p1, :cond_1

    .line 92
    invoke-virtual {v0}, Lcom/squareup/ui/main/AddNoteView;->getNote()Ljava/lang/String;

    move-result-object p1

    .line 93
    iget-object v1, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->diag:Lcom/squareup/ui/DiagnosticCrasher;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/DiagnosticCrasher;->maybeCrashOrLog(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->toastFactory:Lcom/squareup/util/ToastFactory;

    sget v1, Lcom/squareup/billhistoryui/R$string;->diagnostics_logged:I

    const/4 v2, 0x1

    invoke-interface {p1, v1, v2}, Lcom/squareup/util/ToastFactory;->showText(II)V

    const-string p1, ""

    .line 98
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1, p1}, Lcom/squareup/payment/Transaction;->setKeypadNote(Ljava/lang/String;)V

    .line 100
    :cond_1
    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->addNoteScreenRunner:Lcom/squareup/ui/main/AddNoteScreenRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/main/AddNoteScreenRunner;->finishNoteScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$0$AddNoteScreen$Presenter()V
    .locals 1

    const/4 v0, 0x0

    .line 68
    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->done(Z)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$AddNoteScreen$Presenter()V
    .locals 1

    const/4 v0, 0x1

    .line 70
    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->done(Z)V

    return-void
.end method

.method onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    .line 85
    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->done(Z)V

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 61
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 62
    iget-object v1, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->diag:Lcom/squareup/ui/DiagnosticCrasher;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const-string v1, "Crash codes: %s"

    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    invoke-virtual {p0}, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/AddNoteView;

    iget-object v1, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getKeypadNote()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/AddNoteView;->setNote(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/AddNoteView;

    invoke-virtual {v0}, Lcom/squareup/ui/main/AddNoteView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 67
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->add_note:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 68
    new-instance v1, Lcom/squareup/ui/main/-$$Lambda$AddNoteScreen$Presenter$Ql5sRslrZKbLSSm1OpKKyQZXPik;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/-$$Lambda$AddNoteScreen$Presenter$Ql5sRslrZKbLSSm1OpKKyQZXPik;-><init>(Lcom/squareup/ui/main/AddNoteScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 69
    iget-object v1, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->done:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 70
    new-instance v1, Lcom/squareup/ui/main/-$$Lambda$AddNoteScreen$Presenter$AQGIRuc8ZbhtO_Z9BF0VQVMS6Ds;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/-$$Lambda$AddNoteScreen$Presenter$AQGIRuc8ZbhtO_Z9BF0VQVMS6Ds;-><init>(Lcom/squareup/ui/main/AddNoteScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showPrimaryButton(Ljava/lang/Runnable;)V

    if-nez p1, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/AddNoteView;

    invoke-virtual {p1}, Lcom/squareup/ui/main/AddNoteView;->requestInitialFocus()V

    goto :goto_0

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/AddNoteView;

    iget-object v1, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->bundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {v1, p1}, Lcom/squareup/BundleKey;->get(Landroid/os/Bundle;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/AddNoteView;->setNote(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->bundleKey:Lcom/squareup/BundleKey;

    invoke-virtual {p0}, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/AddNoteView;

    invoke-virtual {v1}, Lcom/squareup/ui/main/AddNoteView;->getNote()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/BundleKey;->put(Landroid/os/Bundle;Ljava/lang/Object;)V

    .line 81
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    return-void
.end method
