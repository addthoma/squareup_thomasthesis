.class Lcom/squareup/ui/main/r12education/R12EducationView$R12EducationAdapter;
.super Landroidx/viewpager/widget/PagerAdapter;
.source "R12EducationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "R12EducationAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/r12education/R12EducationView;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/main/r12education/R12EducationView;)V
    .locals 0

    .line 370
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$R12EducationAdapter;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-direct {p0}, Landroidx/viewpager/widget/PagerAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/main/r12education/R12EducationView;Lcom/squareup/ui/main/r12education/R12EducationView$1;)V
    .locals 0

    .line 370
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/r12education/R12EducationView$R12EducationAdapter;-><init>(Lcom/squareup/ui/main/r12education/R12EducationView;)V

    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .line 383
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    .line 372
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$R12EducationAdapter;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$200(Lcom/squareup/ui/main/r12education/R12EducationView;)Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->getPanelCount()I

    move-result v0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 1

    .line 376
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$R12EducationAdapter;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$200(Lcom/squareup/ui/main/r12education/R12EducationView;)Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelAt(I)Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    move-result-object p2

    .line 377
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$R12EducationAdapter;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    iget-object v0, v0, Lcom/squareup/ui/main/r12education/R12EducationView;->countryCode:Lcom/squareup/CountryCode;

    invoke-virtual {p2, p1, v0}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->buildContent(Landroid/view/ViewGroup;Lcom/squareup/CountryCode;)Landroid/view/View;

    move-result-object p2

    .line 378
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object p2
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 0

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
