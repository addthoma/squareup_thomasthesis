.class Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$7;
.super Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;
.source "R12EducationViewPanelTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->populateStandardPanelTransitions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

.field final synthetic val$tapPhoneToSwipe:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V
    .locals 0

    .line 346
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$7;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    iput-object p2, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$7;->val$tapPhoneToSwipe:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;-><init>()V

    return-void
.end method


# virtual methods
.method tweenElement(Lcom/squareup/ui/main/r12education/R12EducationView$Element;Landroid/view/View;FLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 9

    move-object v0, p0

    move-object v3, p2

    move-object v6, p5

    .line 350
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$14;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$Element:[I

    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    .line 370
    iget-object v1, v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$7;->val$tapPhoneToSwipe:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;->tweenElement(Lcom/squareup/ui/main/r12education/R12EducationView$Element;Landroid/view/View;FLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    const/high16 v2, 0x3f000000    # 0.5f

    .line 359
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$400()Landroid/view/animation/Interpolator;

    move-result-object v4

    move v5, p3

    invoke-static {p3, v1, v2, v4}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFFLandroid/view/animation/Interpolator;)F

    move-result v1

    .line 360
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    .line 361
    iget-object v2, v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$7;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    .line 362
    invoke-static {v2}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object v2

    iget v2, v2, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->tapCardWithHandBottomMargin:I

    neg-int v2, v2

    .line 361
    invoke-static {p2, p5, v2}, Lcom/squareup/ui/main/r12education/Tweens;->alignBottomEdgeToCenterY(Landroid/view/View;Landroid/view/View;I)V

    .line 363
    iget-object v2, v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$7;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    .line 364
    invoke-static {v2}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object v2

    iget v2, v2, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->tapCardWithHandLeftMargin:I

    neg-int v2, v2

    move-object v4, p4

    .line 363
    invoke-static {p2, p5, p4, v2, v1}, Lcom/squareup/ui/main/r12education/Tweens;->slideOutRightFromAlignEdgeToCenter(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IF)V

    goto :goto_0

    .line 354
    :cond_1
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->hide(Landroid/view/View;)V

    :goto_0
    return-void
.end method
