.class Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;
.super Ljava/lang/Object;
.source "R12EducationViewPanelTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ElementValueHolder"
.end annotation


# static fields
.field private static final CABLE_MARGIN_DP:I = 0x4b

.field private static final PHONE_APPLE_PAY_BOTTOM_MARGIN_DP:I = 0x2c

.field private static final PHONE_APPLE_PAY_LEFT_MARGIN_DP:I = 0x29

.field private static final PHONE_R4_BOTTOM_MARGIN_DP:I = 0x69

.field private static final TAP_CARD_WITH_HAND_BOTTOM_MARGIN_DP:I = 0x14

.field private static final TAP_CARD_WITH_HAND_LEFT_MARGIN_DP:I = 0x1e


# instance fields
.field final cableMargin:I

.field final chipCardMargin:I

.field final phoneApplePayBottomMargin:I

.field final phoneApplePayLeftMargin:I

.field final phoneR4BottomMargin:I

.field final tapCardWithHandBottomMargin:I

.field final tapCardWithHandLeftMargin:I


# direct methods
.method private constructor <init>(Landroid/content/res/Resources;)V
    .locals 3

    .line 676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/4 v1, 0x1

    const/high16 v2, 0x42240000    # 41.0f

    .line 679
    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->phoneApplePayBottomMargin:I

    const/high16 v2, 0x42300000    # 44.0f

    .line 681
    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->phoneApplePayLeftMargin:I

    const/high16 v2, 0x41f00000    # 30.0f

    .line 683
    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->tapCardWithHandBottomMargin:I

    const/high16 v2, 0x41a00000    # 20.0f

    .line 685
    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->tapCardWithHandLeftMargin:I

    const/high16 v2, 0x42d20000    # 105.0f

    .line 687
    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->phoneR4BottomMargin:I

    const/high16 v2, 0x42960000    # 75.0f

    .line 688
    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->cableMargin:I

    .line 689
    sget v0, Lcom/squareup/readertutorial/R$drawable;->r12_education_r12:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 690
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    iput p1, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->chipCardMargin:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/res/Resources;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$1;)V
    .locals 0

    .line 650
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;-><init>(Landroid/content/res/Resources;)V

    return-void
.end method
