.class Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$11;
.super Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;
.source "R12EducationViewPanelTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->populateCountryPrefersContactlessCardsTransitions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)V
    .locals 0

    .line 489
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$11;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;-><init>()V

    return-void
.end method


# virtual methods
.method tweenElement(Lcom/squareup/ui/main/r12education/R12EducationView$Element;Landroid/view/View;FLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 2

    .line 493
    sget-object p4, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$14;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$Element:[I

    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->ordinal()I

    move-result p1

    aget p1, p4, p1

    const/4 p4, 0x1

    if-eq p1, p4, :cond_4

    const/4 p4, 0x2

    const/high16 v0, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    if-eq p1, p4, :cond_3

    const/4 p4, 0x3

    if-eq p1, p4, :cond_2

    const/4 p4, 0x6

    const/high16 p5, 0x3f800000    # 1.0f

    if-eq p1, p4, :cond_1

    const/4 p4, 0x7

    if-eq p1, p4, :cond_0

    .line 543
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->hide(Landroid/view/View;)V

    goto :goto_0

    .line 525
    :cond_0
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$200()Landroid/view/animation/Interpolator;

    move-result-object p1

    invoke-static {p3, v0, p5, p1}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFFLandroid/view/animation/Interpolator;)F

    move-result p1

    .line 527
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    .line 528
    iget-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$11;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    .line 529
    invoke-static {p3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object p3

    iget p3, p3, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->tapCardWithHandBottomMargin:I

    neg-int p3, p3

    .line 528
    invoke-static {p2, p7, p3}, Lcom/squareup/ui/main/r12education/Tweens;->alignBottomEdgeToCenterY(Landroid/view/View;Landroid/view/View;I)V

    .line 530
    iget-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$11;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    .line 531
    invoke-static {p3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object p3

    iget p3, p3, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->tapCardWithHandLeftMargin:I

    neg-int p3, p3

    .line 530
    invoke-static {p2, p7, p6, p3, p1}, Lcom/squareup/ui/main/r12education/Tweens;->slideInRightToAlignEdgeToCenter(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IF)V

    goto :goto_0

    :cond_1
    const p1, 0x3f733333    # 0.95f

    .line 536
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$200()Landroid/view/animation/Interpolator;

    move-result-object p4

    invoke-static {p3, p1, p5, p4}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFFLandroid/view/animation/Interpolator;)F

    move-result p1

    .line 537
    invoke-static {p2, p1}, Lcom/squareup/ui/main/r12education/Tweens;->fadeIn(Landroid/view/View;F)V

    goto :goto_0

    :cond_2
    const p1, 0x3d4ccccd    # 0.05f

    .line 508
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$400()Landroid/view/animation/Interpolator;

    move-result-object p4

    invoke-static {p3, v1, p1, p4}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFFLandroid/view/animation/Interpolator;)F

    move-result p1

    .line 509
    invoke-static {p2, p1}, Lcom/squareup/ui/main/r12education/Tweens;->fadeOut(Landroid/view/View;F)V

    goto :goto_0

    .line 497
    :cond_3
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$400()Landroid/view/animation/Interpolator;

    move-result-object p1

    invoke-static {p3, v1, v0, p1}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFFLandroid/view/animation/Interpolator;)F

    move-result p1

    .line 499
    invoke-static {p2, p1}, Lcom/squareup/ui/main/r12education/Tweens;->fadeOut(Landroid/view/View;F)V

    .line 500
    invoke-static {p2, p5}, Lcom/squareup/ui/main/r12education/Tweens;->centerY(Landroid/view/View;Landroid/view/View;)V

    .line 501
    iget-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$11;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    invoke-static {p3}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object p3

    iget p3, p3, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->cableMargin:I

    invoke-static {p2, p5, p3, p1}, Lcom/squareup/ui/main/r12education/Tweens;->slideOutLeftFromAlignEdgeToCenter(Landroid/view/View;Landroid/view/View;IF)V

    goto :goto_0

    .line 515
    :cond_4
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    const p1, 0x3e19999a    # 0.15f

    const p4, 0x3f666666    # 0.9f

    .line 517
    invoke-static {p3, p1, p4}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFF)F

    move-result p1

    .line 518
    invoke-static {p2, p5, p7, p1}, Lcom/squareup/ui/main/r12education/Tweens;->translateCenterY(Landroid/view/View;Landroid/view/View;Landroid/view/View;F)V

    .line 519
    invoke-static {p2, p5, p7, p1}, Lcom/squareup/ui/main/r12education/Tweens;->translateCenterX(Landroid/view/View;Landroid/view/View;Landroid/view/View;F)V

    :goto_0
    return-void
.end method
