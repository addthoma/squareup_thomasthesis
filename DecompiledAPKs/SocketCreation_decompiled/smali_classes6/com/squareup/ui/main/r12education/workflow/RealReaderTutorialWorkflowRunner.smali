.class public final Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;
.super Lcom/squareup/container/WorkflowV2Runner;
.source "RealReaderTutorialWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/WorkflowV2Runner<",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;",
        ">;",
        "Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003B-\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u001a\u001a\u00020\u00122\u0006\u0010\u001b\u001a\u00020\u001cH\u0014J\u0010\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000RL\u0010\u0010\u001a:\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u0002\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0015j\u0002`\u00160\u0013j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0014`\u00170\u00118TX\u0094\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0019\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;",
        "Lcom/squareup/container/WorkflowV2Runner;",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;",
        "Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "speedTestWorkflow",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestWorkflow;",
        "flow",
        "Ldagger/Lazy;",
        "Lflow/Flow;",
        "r12TutorialViewFactory",
        "Lcom/squareup/ui/main/r12education/workflow/R12TutorialViewFactory;",
        "(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/main/r12education/workflow/SpeedTestWorkflow;Ldagger/Lazy;Lcom/squareup/ui/main/r12education/workflow/R12TutorialViewFactory;)V",
        "useUpdatingMessaging",
        "",
        "workflow",
        "Lcom/squareup/workflow/Workflow;",
        "",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "getWorkflow",
        "()Lcom/squareup/workflow/Workflow;",
        "onEnterScope",
        "newScope",
        "Lmortar/MortarScope;",
        "start",
        "reader-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final flow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final speedTestWorkflow:Lcom/squareup/ui/main/r12education/workflow/SpeedTestWorkflow;

.field private useUpdatingMessaging:Z


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/main/r12education/workflow/SpeedTestWorkflow;Ldagger/Lazy;Lcom/squareup/ui/main/r12education/workflow/R12TutorialViewFactory;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/ui/main/r12education/workflow/SpeedTestWorkflow;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;",
            "Lcom/squareup/ui/main/r12education/workflow/R12TutorialViewFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "speedTestWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "r12TutorialViewFactory"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    const-class v0, Lcom/squareup/ui/main/r12education/workflow/ReaderTutorialWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v0, "ReaderTutorialWorkflowRunner::class.java.name"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->nextHistory()Lio/reactivex/Observable;

    move-result-object v3

    move-object v4, p4

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 26
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/WorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p2, p0, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->speedTestWorkflow:Lcom/squareup/ui/main/r12education/workflow/SpeedTestWorkflow;

    iput-object p3, p0, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->flow:Ldagger/Lazy;

    return-void
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;)Ldagger/Lazy;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->flow:Ldagger/Lazy;

    return-object p0
.end method

.method public static final synthetic access$getUseUpdatingMessaging$p(Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;)Z
    .locals 0

    .line 21
    iget-boolean p0, p0, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->useUpdatingMessaging:Z

    return p0
.end method

.method public static final synthetic access$setUseUpdatingMessaging$p(Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;Z)V
    .locals 0

    .line 21
    iput-boolean p1, p0, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->useUpdatingMessaging:Z

    return-void
.end method

.method public static final synthetic access$startOrRestart(Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;)V
    .locals 0

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->startOrRestart()V

    return-void
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/Workflow<",
            "Lkotlin/Unit;",
            "Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;"
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->speedTestWorkflow:Lcom/squareup/ui/main/r12education/workflow/SpeedTestWorkflow;

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "newScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-super {p0, p1}, Lcom/squareup/container/WorkflowV2Runner;->onEnterScope(Lmortar/MortarScope;)V

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->onUpdateScreens()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$onEnterScope$1;

    iget-object v2, p0, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-direct {v1, v2}, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$onEnterScope$1;-><init>(Lcom/squareup/ui/main/PosContainer;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onUpdateScreens().subscribe(container::pushStack)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    .line 46
    invoke-virtual {p0}, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$onEnterScope$2;-><init>(Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "onResult().subscribe {\n \u2026OrRestart()\n      }\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-static {v0, p1}, Lcom/squareup/mortar/DisposablesKt;->disposeOnExit(Lio/reactivex/disposables/Disposable;Lmortar/MortarScope;)V

    return-void
.end method

.method public start(Z)V
    .locals 0

    .line 36
    iput-boolean p1, p0, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->useUpdatingMessaging:Z

    .line 37
    invoke-virtual {p0}, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->startOrRestart()V

    return-void
.end method
