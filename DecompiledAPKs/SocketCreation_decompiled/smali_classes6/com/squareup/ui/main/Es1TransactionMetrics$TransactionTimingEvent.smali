.class public Lcom/squareup/ui/main/Es1TransactionMetrics$TransactionTimingEvent;
.super Lcom/squareup/analytics/event/v1/TimingEvent;
.source "Es1TransactionMetrics.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/Es1TransactionMetrics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TransactionTimingEvent"
.end annotation


# instance fields
.field public final customer_duration_ms:J

.field public final merchant_duration_ms:J

.field public final total_duration_ms:J

.field public final transaction_id:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;JJJ)V
    .locals 1

    .line 96
    sget-object v0, Lcom/squareup/analytics/RegisterTimingName;->TRANSACTION_TIME:Lcom/squareup/analytics/RegisterTimingName;

    invoke-direct {p0, v0}, Lcom/squareup/analytics/event/v1/TimingEvent;-><init>(Lcom/squareup/analytics/RegisterTimingName;)V

    .line 97
    iput-wide p2, p0, Lcom/squareup/ui/main/Es1TransactionMetrics$TransactionTimingEvent;->total_duration_ms:J

    .line 98
    iput-wide p4, p0, Lcom/squareup/ui/main/Es1TransactionMetrics$TransactionTimingEvent;->customer_duration_ms:J

    .line 99
    iput-wide p6, p0, Lcom/squareup/ui/main/Es1TransactionMetrics$TransactionTimingEvent;->merchant_duration_ms:J

    .line 100
    iput-object p1, p0, Lcom/squareup/ui/main/Es1TransactionMetrics$TransactionTimingEvent;->transaction_id:Ljava/lang/String;

    return-void
.end method
