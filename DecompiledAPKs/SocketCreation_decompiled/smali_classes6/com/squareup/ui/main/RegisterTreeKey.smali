.class public abstract Lcom/squareup/ui/main/RegisterTreeKey;
.super Lcom/squareup/container/ContainerTreeKey;
.source "RegisterTreeKey.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/RegisterTreeKey$Starter;
    }
.end annotation


# static fields
.field private static final WATCH_FOR_LEAKS:Ljava/lang/String; = "watch-for-leaks"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 3

    .line 44
    invoke-super {p0, p1}, Lcom/squareup/container/ContainerTreeKey;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object v0

    .line 46
    const-class v1, Lcom/squareup/ui/component/ComponentFactoryComponent;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->componentInParent(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/component/ComponentFactoryComponent;

    .line 47
    invoke-interface {v1}, Lcom/squareup/ui/component/ComponentFactoryComponent;->componentFactory()Lcom/squareup/ui/component/ComponentFactory;

    move-result-object v1

    .line 48
    invoke-interface {v1, p1, p0}, Lcom/squareup/ui/component/ComponentFactory;->createComponent(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;

    move-result-object p1

    const-string v1, "watch-for-leaks"

    if-eqz p1, :cond_0

    .line 51
    invoke-static {v0, p1}, Lcom/squareup/dagger/Components;->addAsScopeService(Lmortar/MortarScope$Builder;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    const/4 p1, 0x1

    .line 52
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 54
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Ljava/lang/Object;)Lmortar/MortarScope$Builder;

    .line 60
    :goto_0
    const-class p1, Lcom/squareup/ui/main/RegisterTreeKey$Starter;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/main/RegisterTreeKey$Starter;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/squareup/ui/main/RegisterTreeKey$Starter;-><init>(Lcom/squareup/ui/main/RegisterTreeKey$1;)V

    invoke-virtual {v0, p1, v1}, Lmortar/MortarScope$Builder;->withService(Ljava/lang/String;Lmortar/Scoped;)Lmortar/MortarScope$Builder;

    return-object v0
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->UNKNOWN:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method
