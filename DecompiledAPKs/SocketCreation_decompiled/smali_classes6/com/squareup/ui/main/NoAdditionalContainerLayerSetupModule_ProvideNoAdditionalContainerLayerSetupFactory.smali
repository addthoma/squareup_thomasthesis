.class public final Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule_ProvideNoAdditionalContainerLayerSetupFactory;
.super Ljava/lang/Object;
.source "NoAdditionalContainerLayerSetupModule_ProvideNoAdditionalContainerLayerSetupFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/container/AdditionalContainerLayerSetup;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule_ProvideNoAdditionalContainerLayerSetupFactory;->module:Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule;

    return-void
.end method

.method public static create(Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule;)Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule_ProvideNoAdditionalContainerLayerSetupFactory;
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule_ProvideNoAdditionalContainerLayerSetupFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule_ProvideNoAdditionalContainerLayerSetupFactory;-><init>(Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule;)V

    return-object v0
.end method

.method public static provideNoAdditionalContainerLayerSetup(Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule;)Lcom/squareup/container/AdditionalContainerLayerSetup;
    .locals 1

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule;->provideNoAdditionalContainerLayerSetup()Lcom/squareup/container/AdditionalContainerLayerSetup;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/container/AdditionalContainerLayerSetup;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/container/AdditionalContainerLayerSetup;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule_ProvideNoAdditionalContainerLayerSetupFactory;->module:Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule;

    invoke-static {v0}, Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule_ProvideNoAdditionalContainerLayerSetupFactory;->provideNoAdditionalContainerLayerSetup(Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule;)Lcom/squareup/container/AdditionalContainerLayerSetup;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/main/NoAdditionalContainerLayerSetupModule_ProvideNoAdditionalContainerLayerSetupFactory;->get()Lcom/squareup/container/AdditionalContainerLayerSetup;

    move-result-object v0

    return-object v0
.end method
