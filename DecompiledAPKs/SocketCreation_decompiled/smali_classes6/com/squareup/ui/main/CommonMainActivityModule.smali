.class public abstract Lcom/squareup/ui/main/CommonMainActivityModule;
.super Ljava/lang/Object;
.source "CommonMainActivityModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedInUi;,
        Lcom/squareup/pauses/PausesModule;,
        Lcom/squareup/register/widgets/NohoDatePickerDialogModule;,
        Lcom/squareup/register/widgets/NohoTimePickerDialogModule;,
        Lcom/squareup/intermission/IntermissionModule;,
        Lcom/squareup/buyer/language/BuyerLanguageSelectionMainActivityModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideActivityVisibilityPresenter()Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 144
    new-instance v0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    invoke-direct {v0}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;-><init>()V

    return-object v0
.end method

.method static provideContainerActivityDelegate(Lcom/squareup/util/Device;)Lcom/squareup/container/ContainerActivityDelegate;
    .locals 6
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 72
    new-instance v0, Lcom/squareup/ui/main/CommonMainActivityModule$1;

    invoke-direct {v0}, Lcom/squareup/ui/main/CommonMainActivityModule$1;-><init>()V

    .line 86
    new-instance v1, Lcom/squareup/container/ContainerActivityDelegate;

    new-instance v2, Lcom/squareup/container/WorkflowRunnerViewFactory;

    invoke-direct {v2}, Lcom/squareup/container/WorkflowRunnerViewFactory;-><init>()V

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/squareup/container/ContainerViewFactory;

    new-instance v4, Lcom/squareup/container/ContainerTreeKeyViewFactory;

    invoke-direct {v4}, Lcom/squareup/container/ContainerTreeKeyViewFactory;-><init>()V

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-direct {v1, p0, v0, v2, v3}, Lcom/squareup/container/ContainerActivityDelegate;-><init>(Lcom/squareup/util/Device;Lcom/squareup/container/ContainerBackgroundsProvider;Lcom/squareup/container/ContainerViewFactory;[Lcom/squareup/container/ContainerViewFactory;)V

    return-object v1
.end method

.method static provideFlow(Lcom/squareup/ui/main/MainActivityContainer;)Lflow/Flow;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 92
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer;->getFlow()Lflow/Flow;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 94
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 96
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v2

    const-string p0, "Cannot inject flow before %s loads, try injecting Lazy<Flow>."

    .line 95
    invoke-static {p0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static provideGlassConfirmController()Lcom/squareup/widgets/glass/GlassConfirmController;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 63
    invoke-static {}, Lcom/squareup/widgets/glass/GlassConfirmController;->instance()Lcom/squareup/widgets/glass/GlassConfirmController;

    move-result-object v0

    return-object v0
.end method

.method static provideImpersonating(Lcom/squareup/settings/server/AccountStatusSettings;)Z
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 59
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/settings/server/UserSettings;->isImpersonating()Z

    move-result p0

    return p0
.end method

.method static providePosContainerRunner(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/ui/main/PosContainer;
    .locals 0
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 103
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityContainer;->asPosContainer()Lcom/squareup/ui/main/PosContainer;

    move-result-object p0

    return-object p0
.end method

.method static provideWorkingItemBundleKey(Lcom/google/gson/Gson;)Lcom/squareup/BundleKey;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/Gson;",
            ")",
            "Lcom/squareup/BundleKey<",
            "Lcom/squareup/configure/item/WorkingItem;",
            ">;"
        }
    .end annotation

    .line 110
    const-class v0, Lcom/squareup/configure/item/WorkingItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/squareup/configure/item/WorkingItem;

    invoke-static {p0, v0, v1}, Lcom/squareup/BundleKey;->json(Lcom/google/gson/Gson;Ljava/lang/String;Ljava/lang/Class;)Lcom/squareup/BundleKey;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method abstract bindContainer(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/container/ContainerPresenter;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/MainActivityContainer;",
            ")",
            "Lcom/squareup/container/ContainerPresenter<",
            "Lcom/squareup/ui/main/MainActivityScope$View;",
            ">;"
        }
    .end annotation
.end method

.method abstract bindPaymentCompletionMonitor(Lcom/squareup/ui/main/RealPaymentCompletionMonitor;)Lcom/squareup/ui/main/PaymentCompletionMonitor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract bindRootViewBinder(Lcom/squareup/ui/main/MainActivityRootViewBinder;)Lcom/squareup/rootview/RootViewBinder;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideMainActivityBundlersAsSet()Ljava/util/Set;
    .annotation runtime Ldagger/multibindings/Multibinds;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lmortar/bundler/Bundler;",
            ">;"
        }
    .end annotation
.end method

.method abstract provideMainActivityDelegatesAsSet()Ljava/util/Set;
    .annotation runtime Ldagger/multibindings/Multibinds;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/ui/ActivityDelegate;",
            ">;"
        }
    .end annotation
.end method

.method abstract provideMainActivityScopeRegistrar(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)Lmortar/Scoped;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideMainActivityScopeServicesAsSet()Ljava/util/Set;
    .annotation runtime Ldagger/multibindings/Multibinds;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation
.end method

.method abstract provideNfcProcessor(Lcom/squareup/ui/NfcProcessor;)Lcom/squareup/ui/NfcProcessorInterface;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideNfcProcessorDisplaysWarningScreen(Lcom/squareup/ui/RealDisplaysWarningScreen;)Lcom/squareup/ui/NfcProcessor$DisplaysWarningScreen;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePosBrowserLauncher(Lcom/squareup/util/PosBrowserLauncher;)Lcom/squareup/util/BrowserLauncher;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideR12BlockingUpdateConditionalContentLauncher(Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher;)Lcom/squareup/ui/main/ContentLauncher;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/R12BlockingUpdateScreenLauncher;",
            ")",
            "Lcom/squareup/ui/main/ContentLauncher<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end method

.method abstract provideTouchEventMonitor(Lcom/squareup/ui/RealTouchEventMonitor;)Lcom/squareup/ui/TouchEventMonitor;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
