.class public final Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;
.super Ljava/lang/Object;
.source "RealPaymentCompletionMonitor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/RealPaymentCompletionMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final autoVoidProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AutoVoid;",
            ">;"
        }
    .end annotation
.end field

.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentIncompleteNotifierProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/PaymentIncompleteNotifier;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AutoVoid;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/PaymentIncompleteNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;->autoVoidProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;->badBusProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;->pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;->paymentIncompleteNotifierProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;->transactionProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/AutoVoid;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notifications/PaymentIncompleteNotifier;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;)",
            "Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;"
        }
    .end annotation

    .line 57
    new-instance v7, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/payment/AutoVoid;Lcom/squareup/badbus/BadBus;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/notifications/PaymentIncompleteNotifier;Lcom/squareup/payment/Transaction;)Lcom/squareup/ui/main/RealPaymentCompletionMonitor;
    .locals 8

    .line 63
    new-instance v7, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/main/RealPaymentCompletionMonitor;-><init>(Lcom/squareup/payment/AutoVoid;Lcom/squareup/badbus/BadBus;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/notifications/PaymentIncompleteNotifier;Lcom/squareup/payment/Transaction;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/RealPaymentCompletionMonitor;
    .locals 7

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;->autoVoidProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/AutoVoid;

    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/badbus/BadBus;

    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;->pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/pauses/PauseAndResumeRegistrar;

    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;->paymentIncompleteNotifierProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/notifications/PaymentIncompleteNotifier;

    iget-object v0, p0, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/Transaction;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;->newInstance(Lcom/squareup/payment/AutoVoid;Lcom/squareup/badbus/BadBus;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/notifications/PaymentIncompleteNotifier;Lcom/squareup/payment/Transaction;)Lcom/squareup/ui/main/RealPaymentCompletionMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/main/RealPaymentCompletionMonitor_Factory;->get()Lcom/squareup/ui/main/RealPaymentCompletionMonitor;

    move-result-object v0

    return-object v0
.end method
