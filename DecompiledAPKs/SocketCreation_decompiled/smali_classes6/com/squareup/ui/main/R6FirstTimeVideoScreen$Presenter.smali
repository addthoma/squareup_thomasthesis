.class public Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "R6FirstTimeVideoScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/R6FirstTimeVideoScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/main/R6FirstTimeVideoView;",
        ">;"
    }
.end annotation


# static fields
.field static final VIDEO_ID:I


# instance fields
.field private final flow:Lflow/Flow;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 49
    sget v0, Lcom/squareup/readertutorial/R$raw;->r6_first_time_video:I

    sput v0, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->VIDEO_ID:I

    return-void
.end method

.method constructor <init>(Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 51
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->flow:Lflow/Flow;

    return-void
.end method

.method private getMovieMarginsWithinView(Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 4

    .line 88
    iget v0, p1, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    .line 89
    iget p1, p1, Landroid/graphics/Point;->y:I

    int-to-float p1, p1

    .line 91
    invoke-virtual {p0}, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/R6FirstTimeVideoView;

    .line 92
    invoke-virtual {v1}, Lcom/squareup/ui/main/R6FirstTimeVideoView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    .line 93
    invoke-virtual {v1}, Lcom/squareup/ui/main/R6FirstTimeVideoView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float v3, v2, p1

    mul-float p1, p1, v3

    mul-float v3, v3, v0

    cmpl-float v0, v3, v1

    if-lez v0, :cond_0

    div-float v0, v1, v3

    goto :goto_0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    mul-float p1, p1, v0

    mul-float v0, v0, v3

    .line 105
    new-instance v3, Landroid/graphics/Point;

    sub-float/2addr v2, p1

    float-to-int p1, v2

    sub-float/2addr v1, v0

    float-to-int v0, v1

    invoke-direct {v3, p1, v0}, Landroid/graphics/Point;-><init>(II)V

    return-object v3
.end method


# virtual methods
.method finishMovie()V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method getVideoUri(Lcom/squareup/ui/main/R6FirstTimeVideoView;)Landroid/net/Uri;
    .locals 2

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android.resource://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    invoke-virtual {p1}, Lcom/squareup/ui/main/R6FirstTimeVideoView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->VIDEO_ID:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getResourcePackageName(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "/"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget p1, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->VIDEO_ID:I

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 67
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    return-object p1
.end method

.method public onExitScope()V
    .locals 1

    .line 61
    invoke-virtual {p0}, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/R6FirstTimeVideoView;

    invoke-virtual {v0}, Lcom/squareup/ui/main/R6FirstTimeVideoView;->stopPlayback()V

    :cond_0
    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 56
    invoke-virtual {p0}, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/R6FirstTimeVideoView;

    .line 57
    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->getVideoUri(Lcom/squareup/ui/main/R6FirstTimeVideoView;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/R6FirstTimeVideoView;->prepareMovie(Landroid/net/Uri;)V

    return-void
.end method

.method onPlaybackError(II)V
    .locals 0

    .line 116
    invoke-virtual {p0}, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/R6FirstTimeVideoView;

    invoke-virtual {p1}, Lcom/squareup/ui/main/R6FirstTimeVideoView;->displayFallbackImage()V

    return-void
.end method

.method onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2

    const/4 v0, 0x1

    .line 72
    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 73
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result p1

    invoke-direct {v0, v1, p1}, Landroid/graphics/Point;-><init>(II)V

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/R6FirstTimeVideoView;

    .line 75
    invoke-direct {p0, v0}, Lcom/squareup/ui/main/R6FirstTimeVideoScreen$Presenter;->getMovieMarginsWithinView(Landroid/graphics/Point;)Landroid/graphics/Point;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/R6FirstTimeVideoView;->showTextAndCancelButton(Landroid/graphics/Point;)V

    .line 76
    invoke-virtual {p1}, Lcom/squareup/ui/main/R6FirstTimeVideoView;->startPlayback()V

    return-void
.end method
