.class final Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2$1;
.super Ljava/lang/Object;
.source "MainActivityContainer.kt"

# interfaces
.implements Lflow/TraversalCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2;->dispatch(Lflow/Traversal;Lflow/TraversalCallback;)Lcom/squareup/container/DispatchStep$Result;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "onTraversalCompleted"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $callback:Lflow/TraversalCallback;

.field final synthetic $newScreen:Lcom/squareup/container/ContainerTreeKey;

.field final synthetic $viewForSquelchedTransition:Lcom/squareup/ui/main/MainActivityScope$View;

.field final synthetic this$0:Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2;Lcom/squareup/ui/main/MainActivityScope$View;Lflow/TraversalCallback;Lcom/squareup/container/ContainerTreeKey;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2;

    iput-object p2, p0, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2$1;->$viewForSquelchedTransition:Lcom/squareup/ui/main/MainActivityScope$View;

    iput-object p3, p0, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2$1;->$callback:Lflow/TraversalCallback;

    iput-object p4, p0, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2$1;->$newScreen:Lcom/squareup/container/ContainerTreeKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTraversalCompleted()V
    .locals 2

    .line 363
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2$1;->$viewForSquelchedTransition:Lcom/squareup/ui/main/MainActivityScope$View;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/squareup/ui/main/MainActivityScope$View;->finishEnterFullScreen()V

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2$1;->$callback:Lflow/TraversalCallback;

    invoke-interface {v0}, Lflow/TraversalCallback;->onTraversalCompleted()V

    .line 369
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2$1;->this$0:Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2;

    iget-object v0, v0, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2;->this$0:Lcom/squareup/ui/main/MainActivityContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/MainActivityContainer;->access$getEmvSwipePassthroughEnabler$p(Lcom/squareup/ui/main/MainActivityContainer;)Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityContainer$buildDispatchPipeline$2$1;->$newScreen:Lcom/squareup/container/ContainerTreeKey;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;->refreshSwipePassthrough(Lcom/squareup/container/ContainerTreeKey;)V

    return-void
.end method
