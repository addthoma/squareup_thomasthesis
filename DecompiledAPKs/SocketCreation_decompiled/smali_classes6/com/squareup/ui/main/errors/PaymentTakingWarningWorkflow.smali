.class public Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;
.super Lcom/squareup/ui/main/errors/WarningWorkflow;
.source "PaymentTakingWarningWorkflow.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# static fields
.field private static final INITIAL_DELAY_MILLIS:J = 0xfa0L


# instance fields
.field private final buttonFlowStarter:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

.field private final paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

.field private final smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;


# direct methods
.method protected constructor <init>(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/main/SmartPaymentFlowStarter;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/main/errors/WarningWorkflow;-><init>(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;)V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->buttonFlowStarter:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    .line 29
    iput-object p4, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    .line 31
    iput-object p5, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;)Lcom/squareup/ui/main/errors/PaymentInputHandler;
    .locals 0

    .line 16
    iget-object p0, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    return-object p0
.end method

.method public static synthetic lambda$j8Bpjv1JzF4eUSS23K995QE3CYY(Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;Lcom/squareup/ui/main/SmartPaymentResult;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    return-void
.end method

.method private navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    return-void
.end method


# virtual methods
.method protected handleButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V
    .locals 2

    .line 71
    sget-object v0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow$2;->$SwitchMap$com$squareup$ui$main$errors$WarningScreenButtonConfig$ButtonBehaviorType:[I

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 78
    invoke-super {p0, p1}, Lcom/squareup/ui/main/errors/WarningWorkflow;->handleButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V

    return-void

    .line 75
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->buttonFlowStarter:Lcom/squareup/ui/main/errors/ButtonFlowStarter;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/ButtonFlowStarter;->killTenderReaderInitiatedAndTurnOffReader()V

    :cond_1
    return-void
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->getEvents()Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/main/errors/TakeSwipePayment;

    .line 36
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/main/errors/-$$Lambda$eP2ncm2bmkstGZNV12NkY835Ybk;->INSTANCE:Lcom/squareup/ui/main/errors/-$$Lambda$eP2ncm2bmkstGZNV12NkY835Ybk;

    .line 37
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/main/errors/-$$Lambda$m0uRpXRFlFcOEAkFB0qU7iFYiTk;

    invoke-direct {v2, v1}, Lcom/squareup/ui/main/errors/-$$Lambda$m0uRpXRFlFcOEAkFB0qU7iFYiTk;-><init>(Lcom/squareup/ui/tender/TenderStarter;)V

    .line 38
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 35
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->getEvents()Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/main/errors/TakeDipPayment;

    .line 41
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/main/errors/-$$Lambda$vyy9aDWI4s2W-JT3iYqHIXzHA8w;->INSTANCE:Lcom/squareup/ui/main/errors/-$$Lambda$vyy9aDWI4s2W-JT3iYqHIXzHA8w;

    .line 42
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/main/errors/-$$Lambda$KoMirAh_teyLkuCUxcKEWLiape4;

    invoke-direct {v2, v1}, Lcom/squareup/ui/main/errors/-$$Lambda$KoMirAh_teyLkuCUxcKEWLiape4;-><init>(Lcom/squareup/ui/main/errors/PaymentInputHandler;)V

    .line 43
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 40
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->getEvents()Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/main/errors/TakeTapPayment;

    .line 46
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/main/errors/-$$Lambda$dakt9vkpAaF3xdpytuXY50Yvnso;->INSTANCE:Lcom/squareup/ui/main/errors/-$$Lambda$dakt9vkpAaF3xdpytuXY50Yvnso;

    .line 47
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/main/errors/-$$Lambda$PaymentTakingWarningWorkflow$j8Bpjv1JzF4eUSS23K995QE3CYY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/errors/-$$Lambda$PaymentTakingWarningWorkflow$j8Bpjv1JzF4eUSS23K995QE3CYY;-><init>(Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;)V

    .line 48
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 45
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->getEvents()Lio/reactivex/Observable;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/main/errors/ReportReaderIssue;

    .line 51
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/main/errors/-$$Lambda$zW5zlzwg1CK218mlW_SrWt9kJQ4;->INSTANCE:Lcom/squareup/ui/main/errors/-$$Lambda$zW5zlzwg1CK218mlW_SrWt9kJQ4;

    .line 52
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/main/errors/-$$Lambda$j72C0BidDZmSnBCKL_PCGEuDIgw;

    invoke-direct {v2, v1}, Lcom/squareup/ui/main/errors/-$$Lambda$j72C0BidDZmSnBCKL_PCGEuDIgw;-><init>(Lcom/squareup/ui/main/errors/PaymentInputHandler;)V

    .line 53
    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 50
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 55
    new-instance v0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow$1;-><init>(Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;)V

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
