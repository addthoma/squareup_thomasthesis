.class public abstract Lcom/squareup/ui/main/errors/RootScreenHandler;
.super Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;
.source "RootScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$UpdateRegisterScreenHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$DipRequiredRootScreenHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$DeviceUnsupportedScreenHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$DisableNfcWarningScreenHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$R12BlockingUpdateScreenHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$R12LowBatteryScreenHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$R6LowBatteryScreenHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$TamperErrorScreenHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$PostFwupDisconnectScreenHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$FirmwareUpdateErrorScreenHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$SecureSessionFailedHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$PaymentDeclinedScreenHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$GenericFailedScreenHandler;,
        Lcom/squareup/ui/main/errors/RootScreenHandler$GenericReaderWarningScreenHandler;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field protected final accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field protected final apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field protected final application:Landroid/app/Application;

.field protected final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field protected final goBackAfterWarning:Lcom/squareup/ui/main/errors/GoBackAfterWarning;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field protected final nfcState:Lcom/squareup/ui/AndroidNfcState;

.field private final readerStatusMonitor:Lcom/squareup/ui/main/ReaderStatusMonitor;

.field protected final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field protected final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)V
    .locals 0

    .line 68
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->application:Landroid/app/Application;

    .line 70
    iput-object p2, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->accessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 71
    iput-object p3, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 72
    iput-object p4, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 73
    iput-object p7, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    .line 74
    iput-object p8, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->transaction:Lcom/squareup/payment/Transaction;

    .line 75
    iput-object p10, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    .line 76
    iput-object p5, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->goBackAfterWarning:Lcom/squareup/ui/main/errors/GoBackAfterWarning;

    .line 77
    iput-object p9, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    .line 78
    iput-object p6, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 79
    iput-object p12, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->nfcState:Lcom/squareup/ui/AndroidNfcState;

    .line 80
    iput-object p11, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->readerStatusMonitor:Lcom/squareup/ui/main/ReaderStatusMonitor;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/main/errors/RootScreenHandler;)V
    .locals 0

    .line 49
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/RootScreenHandler;->toastMustRemoveHeadset()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/main/errors/RootScreenHandler;)Lcom/squareup/hudtoaster/HudToaster;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    return-object p0
.end method

.method private toastMustRemoveHeadset()V
    .locals 4

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget v1, Lcom/squareup/cardreader/vector/icons/R$drawable;->icon_audio_reader_slash_120:I

    sget v2, Lcom/squareup/cardreader/ui/R$string;->remove_reader_heading:I

    sget v3, Lcom/squareup/cardreader/ui/R$string;->remove_reader_message:I

    invoke-interface {v0, v1, v2, v3}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(III)Z

    return-void
.end method


# virtual methods
.method protected goHome()V
    .locals 1

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/main/errors/RootScreenHandler;->goBackAfterWarning:Lcom/squareup/ui/main/errors/GoBackAfterWarning;

    invoke-interface {v0}, Lcom/squareup/ui/main/errors/GoBackAfterWarning;->goBack()V

    return-void
.end method

.method protected goHomeAndRestartSecureSessionButton(ILcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
    .locals 2

    .line 150
    new-instance v0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    new-instance v1, Lcom/squareup/ui/main/errors/RootScreenHandler$5;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/main/errors/RootScreenHandler$5;-><init>(Lcom/squareup/ui/main/errors/RootScreenHandler;Lcom/squareup/cardreader/CardReaderId;)V

    invoke-direct {v0, p1, v1}, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;-><init>(ILcom/squareup/debounce/DebouncedOnClickListener;)V

    return-object v0
.end method

.method protected goHomeButton(I)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
    .locals 2

    .line 120
    new-instance v0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    new-instance v1, Lcom/squareup/ui/main/errors/RootScreenHandler$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/errors/RootScreenHandler$3;-><init>(Lcom/squareup/ui/main/errors/RootScreenHandler;)V

    invoke-direct {v0, p1, v1}, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;-><init>(ILcom/squareup/debounce/DebouncedOnClickListener;)V

    return-object v0
.end method

.method protected goHomeIfCardReaderRemoved(ILcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
    .locals 2

    .line 129
    new-instance v0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    new-instance v1, Lcom/squareup/ui/main/errors/RootScreenHandler$4;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/main/errors/RootScreenHandler$4;-><init>(Lcom/squareup/ui/main/errors/RootScreenHandler;Lcom/squareup/cardreader/CardReaderId;)V

    invoke-direct {v0, p1, v1}, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;-><init>(ILcom/squareup/debounce/DebouncedOnClickListener;)V

    return-object v0
.end method

.method protected goToNfcSettings(I)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
    .locals 2

    .line 112
    new-instance v0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    new-instance v1, Lcom/squareup/ui/main/errors/RootScreenHandler$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/errors/RootScreenHandler$2;-><init>(Lcom/squareup/ui/main/errors/RootScreenHandler;)V

    invoke-direct {v0, p1, v1}, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;-><init>(ILcom/squareup/debounce/DebouncedOnClickListener;)V

    return-object v0
.end method

.method protected goToPlayStore(I)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
    .locals 2

    .line 164
    new-instance v0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    new-instance v1, Lcom/squareup/ui/main/errors/RootScreenHandler$6;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/errors/RootScreenHandler$6;-><init>(Lcom/squareup/ui/main/errors/RootScreenHandler;)V

    invoke-direct {v0, p1, v1}, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;-><init>(ILcom/squareup/debounce/DebouncedOnClickListener;)V

    return-object v0
.end method

.method protected goToSettingScreen(ILjava/lang/String;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
    .locals 2

    .line 177
    new-instance v0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    new-instance v1, Lcom/squareup/ui/main/errors/RootScreenHandler$7;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/main/errors/RootScreenHandler$7;-><init>(Lcom/squareup/ui/main/errors/RootScreenHandler;Ljava/lang/String;)V

    invoke-direct {v0, p1, v1}, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;-><init>(ILcom/squareup/debounce/DebouncedOnClickListener;)V

    return-object v0
.end method

.method public handleBackPressed()Z
    .locals 1

    .line 84
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/RootScreenHandler;->goHome()V

    const/4 v0, 0x1

    return v0
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    .line 89
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/RootScreenHandler;->goHome()V

    return-void
.end method

.method protected openBrowserHelpPage(ILjava/lang/String;Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
    .locals 2

    .line 99
    new-instance v0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    new-instance v1, Lcom/squareup/ui/main/errors/RootScreenHandler$1;

    invoke-direct {v1, p0, p3, p2}, Lcom/squareup/ui/main/errors/RootScreenHandler$1;-><init>(Lcom/squareup/ui/main/errors/RootScreenHandler;Lcom/squareup/cardreader/CardReaderId;Ljava/lang/String;)V

    invoke-direct {v0, p1, v1}, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;-><init>(ILcom/squareup/debounce/DebouncedOnClickListener;)V

    return-object v0
.end method
