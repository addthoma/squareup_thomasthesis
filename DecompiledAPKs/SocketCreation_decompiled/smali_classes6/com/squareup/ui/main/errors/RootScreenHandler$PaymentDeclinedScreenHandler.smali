.class public Lcom/squareup/ui/main/errors/RootScreenHandler$PaymentDeclinedScreenHandler;
.super Lcom/squareup/ui/main/errors/RootScreenHandler;
.source "RootScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/RootScreenHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PaymentDeclinedScreenHandler"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/util/Res;Lcom/squareup/ui/AndroidNfcState;)V
    .locals 13
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p13

    .line 257
    invoke-direct/range {v0 .. v12}, Lcom/squareup/ui/main/errors/RootScreenHandler;-><init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)V

    move-object/from16 v1, p12

    .line 260
    iput-object v1, v0, Lcom/squareup/ui/main/errors/RootScreenHandler$PaymentDeclinedScreenHandler;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public from(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
    .locals 2

    .line 264
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    iget-object v1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 265
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget v1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->titleId:I

    .line 266
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedTitle:Ljava/lang/String;

    .line 267
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedTitle(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget v1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->messageId:I

    .line 268
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedMessage:Ljava/lang/String;

    .line 269
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedMessage(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->emv_warning_screen_done:I

    .line 270
    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/errors/RootScreenHandler$PaymentDeclinedScreenHandler;->goHomeButton(I)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->defaultButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 271
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    return-object p1
.end method
