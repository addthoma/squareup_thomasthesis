.class public Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Module;
.super Ljava/lang/Object;
.source "NoPaymentWarningScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Module;->this$0:Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideInitialViewData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$Module;->this$0:Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;->getInitialViewData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;

    move-result-object p1

    return-object p1
.end method
