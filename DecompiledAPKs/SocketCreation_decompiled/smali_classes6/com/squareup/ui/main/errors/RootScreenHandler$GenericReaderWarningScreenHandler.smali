.class public Lcom/squareup/ui/main/errors/RootScreenHandler$GenericReaderWarningScreenHandler;
.super Lcom/squareup/ui/main/errors/RootScreenHandler;
.source "RootScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/RootScreenHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GenericReaderWarningScreenHandler"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method constructor <init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 202
    invoke-direct/range {p0 .. p12}, Lcom/squareup/ui/main/errors/RootScreenHandler;-><init>(Landroid/app/Application;Landroid/view/accessibility/AccessibilityManager;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/main/errors/GoBackAfterWarning;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/ui/AndroidNfcState;)V

    return-void
.end method


# virtual methods
.method public from(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
    .locals 2

    .line 208
    new-instance v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 209
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget v1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->titleId:I

    .line 210
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget v1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->messageId:I

    .line 211
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedTitle:Ljava/lang/String;

    .line 212
    invoke-virtual {v0, v1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedTitle(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedMessage:Ljava/lang/String;

    .line 213
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->localizedMessage(Ljava/lang/String;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->try_again:I

    .line 214
    invoke-virtual {p0, v0}, Lcom/squareup/ui/main/errors/RootScreenHandler$GenericReaderWarningScreenHandler;->goHomeButton(I)Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->defaultButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 215
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    return-object p1
.end method
