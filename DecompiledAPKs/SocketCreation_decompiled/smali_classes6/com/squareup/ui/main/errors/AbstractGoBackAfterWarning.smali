.class public abstract Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;
.super Ljava/lang/Object;
.source "AbstractGoBackAfterWarning.java"

# interfaces
.implements Lcom/squareup/ui/main/errors/GoBackAfterWarning;


# instance fields
.field private final apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field private final paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

.field private final readerStatusMonitor:Lcom/squareup/ui/main/ReaderStatusMonitor;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method protected constructor <init>(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->transaction:Lcom/squareup/payment/Transaction;

    .line 28
    iput-object p3, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    .line 29
    iput-object p4, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    .line 30
    iput-object p5, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->readerStatusMonitor:Lcom/squareup/ui/main/ReaderStatusMonitor;

    .line 31
    iput-object p6, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    return-void
.end method


# virtual methods
.method protected doCommonRedirections()Z
    .locals 3

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->splitTenderSupported()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->transaction:Lcom/squareup/payment/Transaction;

    .line 44
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0}, Lcom/squareup/ui/tender/TenderStarter;->goToSplitTender()V

    return v1

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->transaction:Lcom/squareup/payment/Transaction;

    .line 48
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {v0}, Lcom/squareup/api/ApiReaderSettingsController;->isApiReaderSettingsRequest()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    sget-object v2, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-interface {v0, v2}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->readerStatusMonitor:Lcom/squareup/ui/main/ReaderStatusMonitor;

    invoke-virtual {v0}, Lcom/squareup/ui/main/ReaderStatusMonitor;->showCardReaderCard()V

    return v1

    :cond_2
    const/4 v0, 0x0

    return v0

    .line 49
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    sget-object v2, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-interface {v0, v2}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0}, Lcom/squareup/ui/tender/TenderStarter;->startTenderFlow()V

    return v1
.end method

.method public abstract goBack()V
.end method
