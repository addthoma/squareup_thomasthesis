.class public Lcom/squareup/ui/main/errors/WarningCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "WarningCoordinator.java"


# instance fields
.field private bottomDefaultButton:Landroid/widget/Button;

.field private cancelButton:Lcom/squareup/glyph/SquareGlyphView;

.field private glyphText:Lcom/squareup/marin/widgets/MarinGlyphMessage;

.field private final mainScheduler:Lrx/Scheduler;

.field private topAlternativeButton:Landroid/widget/Button;

.field private final workflow:Lcom/squareup/ui/main/errors/WarningWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/errors/WarningWorkflow;Lrx/Scheduler;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->workflow:Lcom/squareup/ui/main/errors/WarningWorkflow;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->mainScheduler:Lrx/Scheduler;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/main/errors/WarningCoordinator;)Lcom/squareup/ui/main/errors/WarningWorkflow;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->workflow:Lcom/squareup/ui/main/errors/WarningWorkflow;

    return-object p0
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 132
    sget v0, Lcom/squareup/cardreader/ui/R$id;->cancel_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->cancelButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 133
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_warning_bottom_default_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->bottomDefaultButton:Landroid/widget/Button;

    .line 134
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_warning_top_alternative_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->topAlternativeButton:Landroid/widget/Button;

    .line 135
    sget v0, Lcom/squareup/cardreader/ui/R$id;->reader_warning_glyph_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iput-object p1, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->glyphText:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    return-void
.end method

.method static synthetic lambda$null$2(Lcom/squareup/ui/main/errors/WarningScreenData;)Ljava/lang/Boolean;
    .locals 0

    .line 70
    iget-object p0, p0, Lcom/squareup/ui/main/errors/WarningScreenData;->timeout:Ljava/lang/Long;

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private updateCancelButton(Z)V
    .locals 2

    if-nez p1, :cond_0

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->cancelButton:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    return-void

    .line 85
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->cancelButton:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->cancelButton:Lcom/squareup/glyph/SquareGlyphView;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->workflow:Lcom/squareup/ui/main/errors/WarningWorkflow;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/main/errors/-$$Lambda$S2YZADuWY_ZY0q2e7Z4wjJZc7pQ;

    invoke-direct {v1, v0}, Lcom/squareup/ui/main/errors/-$$Lambda$S2YZADuWY_ZY0q2e7Z4wjJZc7pQ;-><init>(Lcom/squareup/ui/main/errors/WarningWorkflow;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private updateDefaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)V
    .locals 2

    if-eqz p1, :cond_2

    .line 90
    iget-boolean v0, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->visible:Z

    if-nez v0, :cond_0

    goto :goto_1

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->bottomDefaultButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->bottomDefaultButton:Landroid/widget/Button;

    iget-boolean v1, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->enabled:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 97
    iget-object v0, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->localizedText:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->bottomDefaultButton:Landroid/widget/Button;

    iget-object v1, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->localizedText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->bottomDefaultButton:Landroid/widget/Button;

    iget v1, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->textId:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 103
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->bottomDefaultButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/main/errors/WarningCoordinator$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/main/errors/WarningCoordinator$1;-><init>(Lcom/squareup/ui/main/errors/WarningCoordinator;Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    .line 91
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->bottomDefaultButton:Landroid/widget/Button;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method private updateTopAlternativeButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)V
    .locals 2

    if-eqz p1, :cond_2

    .line 111
    iget-boolean v0, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->visible:Z

    if-nez v0, :cond_0

    goto :goto_1

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->topAlternativeButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->topAlternativeButton:Landroid/widget/Button;

    iget-boolean v1, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->enabled:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 118
    iget-object v0, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->localizedText:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->topAlternativeButton:Landroid/widget/Button;

    iget-object v1, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->localizedText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->topAlternativeButton:Landroid/widget/Button;

    iget v1, p1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;->textId:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 124
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->topAlternativeButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/main/errors/WarningCoordinator$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/main/errors/WarningCoordinator$2;-><init>(Lcom/squareup/ui/main/errors/WarningCoordinator;Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    .line 112
    :cond_2
    :goto_1
    iget-object p1, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->topAlternativeButton:Landroid/widget/Button;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/errors/WarningCoordinator;->bindViews(Landroid/view/View;)V

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->workflow:Lcom/squareup/ui/main/errors/WarningWorkflow;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/main/errors/-$$Lambda$mazwkNHAlbN5_GIJ97Ltk111eL0;

    invoke-direct {v1, v0}, Lcom/squareup/ui/main/errors/-$$Lambda$mazwkNHAlbN5_GIJ97Ltk111eL0;-><init>(Lcom/squareup/ui/main/errors/WarningWorkflow;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 42
    new-instance v0, Lcom/squareup/ui/main/errors/-$$Lambda$WarningCoordinator$kbY-kYLRw411i4QQwC1HiIbNtHc;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/errors/-$$Lambda$WarningCoordinator$kbY-kYLRw411i4QQwC1HiIbNtHc;-><init>(Lcom/squareup/ui/main/errors/WarningCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 69
    new-instance v0, Lcom/squareup/ui/main/errors/-$$Lambda$WarningCoordinator$acgDi8l0y9bu59ri58gEWKmDeTc;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/errors/-$$Lambda$WarningCoordinator$acgDi8l0y9bu59ri58gEWKmDeTc;-><init>(Lcom/squareup/ui/main/errors/WarningCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$1$WarningCoordinator()Lrx/Subscription;
    .locals 2

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->workflow:Lcom/squareup/ui/main/errors/WarningWorkflow;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/WarningWorkflow;->screenData()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/main/errors/-$$Lambda$WarningCoordinator$TbujXWck0m5KDAZy7o2w_KazAik;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/errors/-$$Lambda$WarningCoordinator$TbujXWck0m5KDAZy7o2w_KazAik;-><init>(Lcom/squareup/ui/main/errors/WarningCoordinator;)V

    .line 43
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$5$WarningCoordinator()Lrx/Subscription;
    .locals 2

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->workflow:Lcom/squareup/ui/main/errors/WarningWorkflow;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/WarningWorkflow;->screenData()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/main/errors/-$$Lambda$WarningCoordinator$XNt5vaQJGbM1GNhEtQN_AECx_s8;->INSTANCE:Lcom/squareup/ui/main/errors/-$$Lambda$WarningCoordinator$XNt5vaQJGbM1GNhEtQN_AECx_s8;

    .line 70
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/main/errors/-$$Lambda$WarningCoordinator$_pT8x1jmTlBoINNORjKe9V6ZDdU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/errors/-$$Lambda$WarningCoordinator$_pT8x1jmTlBoINNORjKe9V6ZDdU;-><init>(Lcom/squareup/ui/main/errors/WarningCoordinator;)V

    .line 71
    invoke-virtual {v0, v1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/main/errors/-$$Lambda$WarningCoordinator$9CY9hBuOmmLZEQIziFfukjjv_yE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/errors/-$$Lambda$WarningCoordinator$9CY9hBuOmmLZEQIziFfukjjv_yE;-><init>(Lcom/squareup/ui/main/errors/WarningCoordinator;)V

    .line 77
    invoke-static {v1}, Lcom/squareup/util/RxTuples;->expandPair(Lrx/functions/Action2;)Lrx/functions/Action1;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$WarningCoordinator(Lcom/squareup/ui/main/errors/WarningScreenData;)V
    .locals 2

    .line 44
    iget-boolean v0, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->cancellable:Z

    invoke-direct {p0, v0}, Lcom/squareup/ui/main/errors/WarningCoordinator;->updateCancelButton(Z)V

    .line 45
    iget-object v0, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->defaultButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    invoke-direct {p0, v0}, Lcom/squareup/ui/main/errors/WarningCoordinator;->updateDefaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)V

    .line 46
    iget-object v0, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->topAlternativeButton:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;

    invoke-direct {p0, v0}, Lcom/squareup/ui/main/errors/WarningCoordinator;->updateTopAlternativeButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig;)V

    .line 48
    iget-object v0, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->glyphText:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget-object v1, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    goto :goto_0

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->glyphText:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget-object v1, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->vectorId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setVector(I)V

    .line 54
    :goto_0
    iget-object v0, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->localizedMessage:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->glyphText:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget-object v1, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->localizedMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 56
    :cond_1
    iget-object v0, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->messageId:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->glyphText:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget-object v1, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->messageId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setMessage(I)V

    .line 60
    :cond_2
    :goto_1
    iget-object v0, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->localizedTitle:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->glyphText:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget-object p1, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->localizedTitle:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 63
    :cond_3
    iget v0, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->titleId:I

    if-lez v0, :cond_4

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    const-string v1, "WarningCoordinator::attach titleId is invalid"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->glyphText:Lcom/squareup/marin/widgets/MarinGlyphMessage;

    iget p1, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->titleId:I

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphMessage;->setTitle(I)V

    :goto_3
    return-void
.end method

.method public synthetic lambda$null$3$WarningCoordinator(Lcom/squareup/ui/main/errors/WarningScreenData;)Lrx/Observable;
    .locals 4

    .line 72
    iget-object v0, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->timeout:Ljava/lang/Long;

    .line 73
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->mainScheduler:Lrx/Scheduler;

    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->timer(JLjava/util/concurrent/TimeUnit;Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/ui/main/errors/WarningScreenData;->timeoutBehavior:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    .line 74
    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    .line 75
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v1

    .line 72
    invoke-static {v0, p1, v1}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$4$WarningCoordinator(Ljava/lang/Long;Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V
    .locals 0

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/main/errors/WarningCoordinator;->workflow:Lcom/squareup/ui/main/errors/WarningWorkflow;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/main/errors/WarningWorkflow;->onTimeout(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V

    return-void
.end method
