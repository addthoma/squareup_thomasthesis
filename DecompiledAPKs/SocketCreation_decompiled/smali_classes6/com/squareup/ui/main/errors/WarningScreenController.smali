.class interface abstract Lcom/squareup/ui/main/errors/WarningScreenController;
.super Ljava/lang/Object;
.source "WarningScreenController.java"


# virtual methods
.method public abstract onBackPressed()V
.end method

.method public abstract onCancelPressed()V
.end method

.method public abstract onDefaultButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V
.end method

.method public abstract onTopAlternativeButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V
.end method

.method public abstract screenData()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/main/errors/WarningScreenData;",
            ">;"
        }
    .end annotation
.end method
