.class public abstract Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;
.super Ljava/lang/Object;
.source "ReaderWarningScreenHandler.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;
.implements Lmortar/Scoped;
.implements Lcom/squareup/pauses/PausesAndResumes;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private defaultEmvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

.field private presenter:Lcom/squareup/ui/main/errors/ReaderWarningPresenter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract from(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
.end method

.method protected getPresenter()Lcom/squareup/ui/main/errors/ReaderWarningPresenter;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;->presenter:Lcom/squareup/ui/main/errors/ReaderWarningPresenter;

    return-object v0
.end method

.method public handleBackPressed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public handleFailedSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 0

    return-void
.end method

.method public handleSuccessfulSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V
    .locals 0

    return-void
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public onResume()V
    .locals 0

    return-void
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;->defaultEmvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;->processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;->defaultEmvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    invoke-interface {v0, p1}, Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;->processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method final setDefaultEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;->defaultEmvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;

    return-void
.end method

.method final setPresenter(Lcom/squareup/ui/main/errors/ReaderWarningPresenter;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;->presenter:Lcom/squareup/ui/main/errors/ReaderWarningPresenter;

    return-void
.end method
