.class Lcom/squareup/ui/main/DeepLinks$LinkEvent;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "DeepLinks.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/DeepLinks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LinkEvent"
.end annotation


# instance fields
.field final sourceApplication:Ljava/lang/String;

.field final url:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 291
    invoke-direct {p0, p1}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    .line 292
    iput-object p2, p0, Lcom/squareup/ui/main/DeepLinks$LinkEvent;->url:Ljava/lang/String;

    .line 293
    iput-object p3, p0, Lcom/squareup/ui/main/DeepLinks$LinkEvent;->sourceApplication:Ljava/lang/String;

    return-void
.end method
