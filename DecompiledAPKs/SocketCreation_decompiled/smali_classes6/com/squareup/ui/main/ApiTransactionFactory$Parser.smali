.class Lcom/squareup/ui/main/ApiTransactionFactory$Parser;
.super Ljava/lang/Object;
.source "ApiTransactionFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/ApiTransactionFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Parser"
.end annotation


# instance fields
.field private final apiTransactionController:Lcom/squareup/api/ApiTransactionController;

.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;


# direct methods
.method constructor <init>(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/api/ApiTransactionController;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/main/ApiTransactionFactory$Parser;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    .line 43
    iput-object p2, p0, Lcom/squareup/ui/main/ApiTransactionFactory$Parser;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    return-void
.end method


# virtual methods
.method public applies(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "com.squareup.pos.action.CHARGE"

    .line 47
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public parseIntent(Landroid/content/Intent;)Lcom/squareup/ui/main/ApiTransactionFactory;
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 55
    iget-object v2, v0, Lcom/squareup/ui/main/ApiTransactionFactory$Parser;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    invoke-virtual {v2}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "com.squareup.pos.CLIENT_ID"

    .line 56
    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-wide/16 v2, 0x0

    const-string v5, "com.squareup.pos.AUTO_RETURN_TIMEOUT_MS"

    .line 57
    invoke-virtual {v1, v5, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v11

    .line 59
    invoke-static/range {p1 .. p1}, Lcom/squareup/api/ApiTenderType;->extractTenderTypes(Landroid/content/Intent;)Ljava/util/Set;

    move-result-object v2

    const-string v3, "com.squareup.pos.API_VERSION"

    .line 60
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/api/ApiVersion;->parse(Ljava/lang/String;)Lcom/squareup/api/ApiVersion;

    move-result-object v13

    const-string v3, "SEQUENCE_UUID"

    .line 61
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-wide/16 v6, -0x1

    const-string v3, "REQUEST_START_TIME"

    .line 63
    invoke-virtual {v1, v3, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v8

    cmp-long v3, v8, v6

    if-eqz v3, :cond_0

    const-string v3, "com.squareup.pos.STATE"

    .line 67
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v3, "com.android.browser.application_id"

    .line 68
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const-string v3, "callback_url"

    .line 69
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const/4 v3, 0x0

    const-string v6, "com.squareup.pos.ALLOW_SPLIT_TENDER"

    .line 70
    invoke-virtual {v1, v6, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v16

    const-string v6, "com.squareup.pos.DELAY_CAPTURE"

    .line 71
    invoke-virtual {v1, v6, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 72
    iget-object v3, v0, Lcom/squareup/ui/main/ApiTransactionFactory$Parser;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    move-wide v6, v8

    move-object v8, v14

    move-object v9, v15

    invoke-virtual/range {v3 .. v10}, Lcom/squareup/api/ApiTransactionController;->startTransaction(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    iget-object v3, v0, Lcom/squareup/ui/main/ApiTransactionFactory$Parser;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    new-instance v4, Lcom/squareup/api/ApiTransactionParams;

    move-object v6, v4

    move-object v7, v2

    move-object v8, v13

    move-wide v9, v11

    move/from16 v11, v16

    move v12, v1

    invoke-direct/range {v6 .. v12}, Lcom/squareup/api/ApiTransactionParams;-><init>(Ljava/util/Set;Lcom/squareup/api/ApiVersion;JZZ)V

    invoke-virtual {v3, v4}, Lcom/squareup/api/ApiTransactionState;->setApiTransactionParams(Lcom/squareup/api/ApiTransactionParams;)V

    goto :goto_0

    .line 65
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "ApiActivity intent should have set the start time."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 78
    :cond_1
    :goto_0
    new-instance v1, Lcom/squareup/ui/main/ApiTransactionFactory;

    invoke-direct {v1}, Lcom/squareup/ui/main/ApiTransactionFactory;-><init>()V

    return-object v1
.end method
