.class public final Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "SystemPermissionRevokedScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/ModalBodyScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen$Component;,
        Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 19
    new-instance v0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen;

    invoke-direct {v0}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen;->INSTANCE:Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen;

    .line 39
    sget-object v0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen;->INSTANCE:Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen;

    .line 40
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SYSTEM_PERMISSIONS_PERMISSION_DENIED:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getHideMaster()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public screenLayout()I
    .locals 1

    .line 47
    sget v0, Lcom/squareup/common/bootstrap/R$layout;->system_permission_revoked_view:I

    return v0
.end method
