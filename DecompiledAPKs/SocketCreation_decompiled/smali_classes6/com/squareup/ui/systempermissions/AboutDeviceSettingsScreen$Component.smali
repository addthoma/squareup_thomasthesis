.class public interface abstract Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen$Component;
.super Ljava/lang/Object;
.source "AboutDeviceSettingsScreen.java"

# interfaces
.implements Lcom/squareup/marin/widgets/MarinCardActionBarView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/marin/widgets/MarinCardActionBarModule;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/systempermissions/AboutDeviceSettingsView;)V
.end method
