.class public Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;
.super Lmortar/ViewPresenter;
.source "SystemPermissionRevokedPresenter.java"

# interfaces
.implements Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;",
        ">;",
        "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;"
    }
.end annotation


# instance fields
.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private missingPermission:Lcom/squareup/systempermissions/SystemPermission;

.field private final permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/ui/main/PosContainer;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    return-void
.end method

.method private getButtonText(Lcom/squareup/systempermissions/SystemPermission;)I
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->canAskDirectly(Lcom/squareup/systempermissions/SystemPermission;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-static {p1}, Lcom/squareup/ui/systempermissions/PermissionMessages;->buttonText(Lcom/squareup/systempermissions/SystemPermission;)I

    move-result p1

    goto :goto_0

    :cond_0
    sget p1, Lcom/squareup/utilities/R$string;->system_permission_settings_button:I

    :goto_0
    return p1
.end method

.method private lookForMissingPermission()V
    .locals 4

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/squareup/systempermissions/SystemPermission;

    sget-object v2, Lcom/squareup/systempermissions/SystemPermission;->LOCATION:Lcom/squareup/systempermissions/SystemPermission;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/systempermissions/SystemPermission;->PHONE:Lcom/squareup/systempermissions/SystemPermission;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget-object v2, Lcom/squareup/systempermissions/SystemPermission;->STORAGE:Lcom/squareup/systempermissions/SystemPermission;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getFirstDenied([Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/systempermissions/SystemPermission;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->missingPermission:Lcom/squareup/systempermissions/SystemPermission;

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->missingPermission:Lcom/squareup/systempermissions/SystemPermission;

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;

    iget-object v1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->missingPermission:Lcom/squareup/systempermissions/SystemPermission;

    invoke-direct {p0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->getButtonText(Lcom/squareup/systempermissions/SystemPermission;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->setPermission(Lcom/squareup/systempermissions/SystemPermission;I)V

    goto :goto_0

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    :goto_0
    return-void
.end method


# virtual methods
.method onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onDenied(ILcom/squareup/systempermissions/SystemPermission;)V
    .locals 0

    .line 50
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->lookForMissingPermission()V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 31
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 32
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-virtual {p1, p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->addPermissionListener(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->removePermissionListener(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;)V

    .line 37
    invoke-super {p0}, Lmortar/ViewPresenter;->onExitScope()V

    return-void
.end method

.method public onGranted(ILcom/squareup/systempermissions/SystemPermission;)V
    .locals 0

    .line 46
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->lookForMissingPermission()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 41
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->lookForMissingPermission()V

    return-void
.end method

.method onRequestPermission()V
    .locals 4

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    iget-object v1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->missingPermission:Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->getStatus(Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;

    move-result-object v0

    .line 60
    sget-object v1, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter$1;->$SwitchMap$com$squareup$ui$systempermissions$SystemPermissionsPresenter$Status:[I

    invoke-virtual {v0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 67
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->lookForMissingPermission()V

    goto :goto_0

    .line 70
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown permission status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$Status;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 64
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->permissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    iget-object v1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->missingPermission:Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->requestPermission(Lcom/squareup/systempermissions/SystemPermission;)V

    :goto_0
    return-void
.end method
