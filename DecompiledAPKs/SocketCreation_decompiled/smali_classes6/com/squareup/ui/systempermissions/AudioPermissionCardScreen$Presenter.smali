.class public Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "AudioPermissionCardScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/systempermissions/AudioPermissionCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private card:Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;

.field private final cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

.field private final flow:Lflow/Flow;

.field permissionListener:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;

.field private final res:Lcom/squareup/util/Res;

.field private final systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

.field private tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;


# direct methods
.method public constructor <init>(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/tutorialv2/TutorialCore;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 75
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 76
    iput-object p2, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    .line 77
    iput-object p3, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->flow:Lflow/Flow;

    .line 78
    iput-object p4, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 79
    iput-object p5, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    .line 80
    iput-object p6, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    .line 82
    new-instance p3, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$1;

    invoke-direct {p3, p0, p1, p2}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$1;-><init>(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V

    iput-object p3, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->permissionListener:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;)Lflow/Flow;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->flow:Lflow/Flow;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;)Z
    .locals 0

    .line 61
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->hasView()Z

    move-result p0

    return p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;)I
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->getButtonText()I

    move-result p0

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;)Ljava/lang/Object;
    .locals 0

    .line 61
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private buildDefaultConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;
    .locals 1

    .line 151
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 152
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 153
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideSecondaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    return-object v0
.end method

.method private configureAudioPermissionCardView()V
    .locals 4

    .line 134
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/systempermissions/AudioPermissionCardView;

    .line 135
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->buildDefaultConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/systempermissions/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v3, v2}, Lcom/squareup/ui/systempermissions/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 137
    iget-object v2, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->card:Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;

    invoke-static {v2}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;->access$400(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 138
    iget-object v2, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/cardreader/ui/R$string;->square_readers:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    .line 140
    :cond_0
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->updateUpButtonGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 142
    :goto_0
    invoke-virtual {v0}, Lcom/squareup/ui/systempermissions/AudioPermissionCardView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 143
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->getButtonText()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/AudioPermissionCardView;->setupLayout(I)V

    return-void
.end method

.method private getButtonText()I
    .locals 2

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    sget-object v1, Lcom/squareup/systempermissions/SystemPermission;->MICROPHONE:Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->canAskDirectly(Lcom/squareup/systempermissions/SystemPermission;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/systempermissions/SystemPermission;->MICROPHONE:Lcom/squareup/systempermissions/SystemPermission;

    .line 158
    invoke-static {v0}, Lcom/squareup/ui/systempermissions/PermissionMessages;->buttonText(Lcom/squareup/systempermissions/SystemPermission;)I

    move-result v0

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/utilities/R$string;->system_permission_settings_button:I

    :goto_0
    return v0
.end method


# virtual methods
.method askForMicPermission()V
    .locals 2

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    sget-object v1, Lcom/squareup/systempermissions/SystemPermission;->MICROPHONE:Lcom/squareup/systempermissions/SystemPermission;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->requestPermission(Lcom/squareup/systempermissions/SystemPermission;)V

    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 100
    invoke-static {p1}, Lcom/squareup/container/ContainerTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->card:Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen;

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    iget-object v1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->permissionListener:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->addPermissionListener(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->readerStates()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/cardreader/CardReaderInfo;->AUDIO_READER_ADDRESS:Ljava/lang/String;

    .line 104
    invoke-static {v1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracleFilters;->filterByReaderAddress(Ljava/lang/String;)Lrx/Observable$Transformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$2;-><init>(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;)V

    .line 105
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/Observer;)Lrx/Subscription;

    move-result-object v0

    .line 103
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 119
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v0, "Shown AudioPermissionCardScreen"

    invoke-interface {p1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 2

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->systemPermissionsPresenter:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    iget-object v1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->permissionListener:Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;->removePermissionListener(Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;)V

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    const-string v1, "Dismissed AudioPermissionCardScreen"

    invoke-interface {v0, v1}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;)V

    .line 125
    invoke-super {p0}, Lmortar/ViewPresenter;->onExitScope()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 129
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 130
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->configureAudioPermissionCardView()V

    return-void
.end method
