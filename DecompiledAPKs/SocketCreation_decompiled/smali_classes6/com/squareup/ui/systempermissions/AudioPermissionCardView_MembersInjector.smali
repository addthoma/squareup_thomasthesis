.class public final Lcom/squareup/ui/systempermissions/AudioPermissionCardView_MembersInjector;
.super Ljava/lang/Object;
.source "AudioPermissionCardView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/systempermissions/AudioPermissionCardView;",
        ">;"
    }
.end annotation


# instance fields
.field private final appNameFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardView_MembersInjector;->appNameFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/systempermissions/AudioPermissionCardView;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/systempermissions/AudioPermissionCardView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/systempermissions/AudioPermissionCardView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAppNameFormatter(Lcom/squareup/ui/systempermissions/AudioPermissionCardView;Lcom/squareup/util/AppNameFormatter;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/systempermissions/AudioPermissionCardView;Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardView;->presenter:Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/systempermissions/AudioPermissionCardView;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/systempermissions/AudioPermissionCardView_MembersInjector;->injectPresenter(Lcom/squareup/ui/systempermissions/AudioPermissionCardView;Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardView_MembersInjector;->appNameFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AppNameFormatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/systempermissions/AudioPermissionCardView_MembersInjector;->injectAppNameFormatter(Lcom/squareup/ui/systempermissions/AudioPermissionCardView;Lcom/squareup/util/AppNameFormatter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/systempermissions/AudioPermissionCardView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/systempermissions/AudioPermissionCardView_MembersInjector;->injectMembers(Lcom/squareup/ui/systempermissions/AudioPermissionCardView;)V

    return-void
.end method
