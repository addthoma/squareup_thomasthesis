.class Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$1;
.super Ljava/lang/Object;
.source "AudioPermissionCardScreen.java"

# interfaces
.implements Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter$PermissionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;-><init>(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/tutorialv2/TutorialCore;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;

.field final synthetic val$cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

.field final synthetic val$cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;


# direct methods
.method constructor <init>(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$1;->this$0:Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$1;->val$cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    iput-object p3, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$1;->val$cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDenied(ILcom/squareup/systempermissions/SystemPermission;)V
    .locals 0

    .line 92
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$1;->this$0:Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->access$100(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;)Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/systempermissions/SystemPermission;->MICROPHONE:Lcom/squareup/systempermissions/SystemPermission;

    if-ne p2, p1, :cond_0

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$1;->this$0:Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->access$300(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/systempermissions/AudioPermissionCardView;

    iget-object p2, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$1;->this$0:Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;

    invoke-static {p2}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->access$200(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/systempermissions/AudioPermissionCardView;->setButtonText(I)V

    :cond_0
    return-void
.end method

.method public onGranted(ILcom/squareup/systempermissions/SystemPermission;)V
    .locals 0

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$1;->val$cardReaderHubUtils:Lcom/squareup/cardreader/CardReaderHubUtils;

    invoke-virtual {p1}, Lcom/squareup/cardreader/CardReaderHubUtils;->maybeGetAudioReader()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 86
    iget-object p2, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$1;->val$cardReaderOracle:Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;->stopEventOverrideForReader(Lcom/squareup/cardreader/CardReaderInfo;)V

    .line 88
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$1;->this$0:Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->access$000(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;)Lflow/Flow;

    move-result-object p1

    invoke-virtual {p1}, Lflow/Flow;->goBack()Z

    return-void
.end method
