.class public Lcom/squareup/widgets/AlwaysResponsiveFrameLayout;
.super Landroid/widget/FrameLayout;
.source "AlwaysResponsiveFrameLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;
    }
.end annotation


# static fields
.field private static final TABLET_WIDTH_MULTIPLIER:F = 0.65f


# instance fields
.field private adjustments:Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    invoke-virtual {p0}, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/utilities/ui/R$bool;->sq_is_tablet:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p2

    .line 28
    invoke-static {p1}, Lcom/squareup/util/ScreenParameters;->getScreenDimens(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object p1

    .line 29
    iget v0, p1, Landroid/graphics/Point;->x:I

    .line 30
    iget p1, p1, Landroid/graphics/Point;->y:I

    if-ge p1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz p2, :cond_1

    .line 35
    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 36
    new-instance p2, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;

    int-to-float p1, p1

    const v0, 0x3f266666    # 0.65f

    mul-float p1, p1, v0

    float-to-int p1, p1

    invoke-direct {p2, p1, p0}, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;-><init>(ILandroid/view/View;)V

    iput-object p2, p0, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout;->adjustments:Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;

    goto :goto_1

    :cond_1
    if-eqz v1, :cond_2

    .line 41
    new-instance p2, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;

    invoke-direct {p2, p1, p0}, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;-><init>(ILandroid/view/View;)V

    iput-object p2, p0, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout;->adjustments:Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    .line 45
    iput-object p1, p0, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout;->adjustments:Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;

    :goto_1
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4

    .line 55
    iget-object v0, p0, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout;->adjustments:Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;

    if-eqz v0, :cond_0

    .line 56
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 57
    iget-object v1, p0, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout;->adjustments:Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;

    invoke-static {v1}, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;->access$000(Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;)I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 59
    iget-object v1, p0, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout;->adjustments:Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;

    invoke-static {v1}, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;->access$100(Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;)I

    move-result v1

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout;->adjustments:Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;

    .line 60
    invoke-static {v3}, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;->access$200(Lcom/squareup/widgets/AlwaysResponsiveFrameLayout$Adjustments;)I

    move-result v3

    add-int/2addr v0, v3

    invoke-virtual {p0}, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout;->getPaddingBottom()I

    move-result v3

    .line 59
    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/squareup/widgets/AlwaysResponsiveFrameLayout;->setPadding(IIII)V

    .line 63
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void
.end method
