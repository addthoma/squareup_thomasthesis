.class public Lcom/squareup/widgets/MarinBadgeView;
.super Lcom/squareup/marketfont/MarketTextView;
.source "MarinBadgeView.java"

# interfaces
.implements Lcom/squareup/marin/widgets/Badgeable;


# instance fields
.field protected final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/widgets/MarinBadgeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    sget p1, Lcom/squareup/marin/R$style;->TextAppearance_Marin_Badge_Hamburger:I

    invoke-static {p0, p1}, Landroidx/core/widget/TextViewCompat;->setTextAppearance(Landroid/widget/TextView;I)V

    .line 31
    sget-object p1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/MarinBadgeView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    const/16 p1, 0x11

    .line 32
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/MarinBadgeView;->setGravity(I)V

    const/4 p1, 0x0

    .line 33
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/MarinBadgeView;->setClickable(Z)V

    const/4 p1, 0x1

    .line 34
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/MarinBadgeView;->setDuplicateParentStateEnabled(Z)V

    .line 36
    invoke-virtual {p0}, Lcom/squareup/widgets/MarinBadgeView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/widgets/MarinBadgeView;->shortAnimTimeMs:I

    .line 38
    invoke-virtual {p0}, Lcom/squareup/widgets/MarinBadgeView;->hideBadge()V

    return-void
.end method

.method private setSize(I)V
    .locals 2

    .line 75
    invoke-virtual {p0}, Lcom/squareup/widgets/MarinBadgeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 76
    invoke-virtual {p0}, Lcom/squareup/widgets/MarinBadgeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 77
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 78
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 79
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/MarinBadgeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public hideBadge()V
    .locals 1

    .line 42
    iget v0, p0, Lcom/squareup/widgets/MarinBadgeView;->shortAnimTimeMs:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    return-void
.end method

.method public showBadge(Ljava/lang/CharSequence;)V
    .locals 1

    .line 46
    iget v0, p0, Lcom/squareup/widgets/MarinBadgeView;->shortAnimTimeMs:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 47
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/MarinBadgeView;->setText(Ljava/lang/CharSequence;)V

    const-string v0, ""

    .line 48
    invoke-virtual {v0, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 49
    sget p1, Lcom/squareup/marin/R$drawable;->icon_blue_dot_12:I

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/MarinBadgeView;->setBackgroundResource(I)V

    .line 50
    sget p1, Lcom/squareup/marin/R$dimen;->priority_no_text_badge_size:I

    invoke-direct {p0, p1}, Lcom/squareup/widgets/MarinBadgeView;->setSize(I)V

    goto :goto_0

    .line 52
    :cond_0
    sget p1, Lcom/squareup/marin/R$drawable;->marin_badge_background:I

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/MarinBadgeView;->setBackgroundResource(I)V

    .line 53
    sget p1, Lcom/squareup/marin/R$dimen;->priority_badge_size:I

    invoke-direct {p0, p1}, Lcom/squareup/widgets/MarinBadgeView;->setSize(I)V

    :goto_0
    return-void
.end method

.method public showFatalPriorityBadge()V
    .locals 1

    .line 65
    iget v0, p0, Lcom/squareup/widgets/MarinBadgeView;->shortAnimTimeMs:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    const/4 v0, 0x0

    .line 66
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/MarinBadgeView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    sget v0, Lcom/squareup/marin/R$drawable;->marin_badge_background_fatal:I

    invoke-virtual {p0, v0}, Lcom/squareup/widgets/MarinBadgeView;->setBackgroundResource(I)V

    .line 68
    sget v0, Lcom/squareup/marin/R$dimen;->priority_badge_size:I

    invoke-direct {p0, v0}, Lcom/squareup/widgets/MarinBadgeView;->setSize(I)V

    return-void
.end method

.method public showHighPriorityBadge(Ljava/lang/CharSequence;)V
    .locals 1

    .line 58
    iget v0, p0, Lcom/squareup/widgets/MarinBadgeView;->shortAnimTimeMs:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    .line 59
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/MarinBadgeView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    sget p1, Lcom/squareup/marin/R$drawable;->marin_badge_background_red:I

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/MarinBadgeView;->setBackgroundResource(I)V

    .line 61
    sget p1, Lcom/squareup/marin/R$dimen;->priority_badge_size:I

    invoke-direct {p0, p1}, Lcom/squareup/widgets/MarinBadgeView;->setSize(I)V

    return-void
.end method
