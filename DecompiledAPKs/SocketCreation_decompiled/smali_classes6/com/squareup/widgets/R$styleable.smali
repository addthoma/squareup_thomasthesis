.class public final Lcom/squareup/widgets/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AutoPaddingTextView:[I

.field public static final AutoPaddingTextView_autoPadding:I = 0x0

.field public static final AutoPaddingTextView_autoPaddingTextViewStyle:I = 0x1

.field public static final BorderedScrollView:[I

.field public static final BorderedScrollView_borderColor:I = 0x0

.field public static final CardBackground:[I

.field public static final CardBackground_marinCardBackground:I = 0x0

.field public static final CenteredFrameLayout_LayoutParams:[I

.field public static final CenteredFrameLayout_LayoutParams_centerOffsetLeft:I = 0x0

.field public static final CheckableGroup:[I

.field public static final CheckableGroup_android_divider:I = 0x0

.field public static final CheckableGroup_isHorizontal:I = 0x1

.field public static final CheckableGroup_singleChoice:I = 0x2

.field public static final CheckableGroup_sqCheckedButton:I = 0x3

.field public static final CheckableGroup_twoColumns:I = 0x4

.field public static final CheckablePreservedLabelRow:[I

.field public static final CheckablePreservedLabelRow_checkablePreservedLabelRowStyle:I = 0x0

.field public static final CheckablePreservedLabelRow_checkablePreservedLabelViewInternalStyle:I = 0x1

.field public static final ColumnLayout:[I

.field public static final ColumnLayout_android_divider:I = 0x0

.field public static final ColumnLayout_sq_marginBetweenColumns:I = 0x1

.field public static final ColumnLayout_twoColumns:I = 0x2

.field public static final DigitInputView:[I

.field public static final DigitInputView_android_textSize:I = 0x0

.field public static final DigitInputView_digitCount:I = 0x1

.field public static final DigitInputView_digitInputViewStyle:I = 0x2

.field public static final DropDownContainer:[I

.field public static final DropDownContainer_dropDownContainerStyle:I = 0x0

.field public static final DropDownContainer_dropDownDuration:I = 0x1

.field public static final DropDownContainer_glassCancel:I = 0x2

.field public static final DropDownContainer_scrimColor:I = 0x3

.field public static final HalfAndHalfLayout:[I

.field public static final HalfAndHalfLayout_android_orientation:I = 0x0

.field public static final HoloCompat:[I

.field public static final HoloCompat_square_buttonBarButtonStyle:I = 0x0

.field public static final HoloCompat_square_buttonBarStyle:I = 0x1

.field public static final HoloCompat_square_dividerHorizontal:I = 0x2

.field public static final HoloCompat_square_dividerVertical:I = 0x3

.field public static final HorizontalThreeChildLayout:[I

.field public static final HorizontalThreeChildLayout_firstChildPercentage:I = 0x0

.field public static final HorizontalThreeChildLayout_secondChildPercentage:I = 0x1

.field public static final HorizontalThreeChildLayout_thirdChildPercentage:I = 0x2

.field public static final JaggedLineView:[I

.field public static final JaggedLineView_borderColor:I = 0x0

.field public static final JaggedLineView_fillColor:I = 0x1

.field public static final JaggedLineView_triangleCount:I = 0x2

.field public static final MessageView:[I

.field public static final MessageView_android_ellipsize:I = 0x3

.field public static final MessageView_android_gravity:I = 0x4

.field public static final MessageView_android_includeFontPadding:I = 0x6

.field public static final MessageView_android_shadowColor:I = 0x7

.field public static final MessageView_android_shadowDx:I = 0x8

.field public static final MessageView_android_shadowDy:I = 0x9

.field public static final MessageView_android_shadowRadius:I = 0xa

.field public static final MessageView_android_text:I = 0x5

.field public static final MessageView_android_textColor:I = 0x2

.field public static final MessageView_android_textSize:I = 0x0

.field public static final MessageView_android_textStyle:I = 0x1

.field public static final MessageView_equalizeLines:I = 0xb

.field public static final MessageView_textJustification:I = 0xc

.field public static final MessageView_weight:I = 0xd

.field public static final NameValueImageRow:[I

.field public static final NameValueImageRow_android_src:I = 0x0

.field public static final NameValueImageRow_android_text:I = 0x1

.field public static final NameValueImageRow_nameValueRowNameStyle:I = 0x2

.field public static final NameValueImageRow_nameValueRowStyle:I = 0x3

.field public static final NameValueImageRow_shortText:I = 0x4

.field public static final NameValueRow:[I

.field public static final NameValueRow_android_text:I = 0x0

.field public static final NameValueRow_nameValueRowNameStyle:I = 0x1

.field public static final NameValueRow_nameValueRowStyle:I = 0x2

.field public static final NameValueRow_nameValueRowValueStyle:I = 0x3

.field public static final NameValueRow_shortText:I = 0x4

.field public static final NameValueRow_showPercent:I = 0x5

.field public static final NameValueRow_value:I = 0x6

.field public static final NullStateHorizontalScrollView:[I

.field public static final NullStateHorizontalScrollView_nullStateText:I = 0x0

.field public static final NullStateListView:[I

.field public static final NullStateListView_nullStateNegativeTopMargin:I = 0x0

.field public static final NullStateListView_nullStateText:I = 0x1

.field public static final Padlock:[I

.field public static final Padlock_android_lineSpacingExtra:I = 0x0

.field public static final Padlock_backspaceColor:I = 0x1

.field public static final Padlock_backspaceDisabledColor:I = 0x2

.field public static final Padlock_backspaceSelector:I = 0x3

.field public static final Padlock_buttonSelector:I = 0x4

.field public static final Padlock_buttonTextSize:I = 0x5

.field public static final Padlock_clearTextColor:I = 0x6

.field public static final Padlock_clearTextDisabledColor:I = 0x7

.field public static final Padlock_deleteType:I = 0x8

.field public static final Padlock_digitColor:I = 0x9

.field public static final Padlock_digitDisabledColor:I = 0xa

.field public static final Padlock_drawDividerLines:I = 0xb

.field public static final Padlock_drawLeftLine:I = 0xc

.field public static final Padlock_drawRightLine:I = 0xd

.field public static final Padlock_drawTopLine:I = 0xe

.field public static final Padlock_horizontalDividerStyle:I = 0xf

.field public static final Padlock_lettersColor:I = 0x10

.field public static final Padlock_lettersDisabledColor:I = 0x11

.field public static final Padlock_lettersTextSize:I = 0x12

.field public static final Padlock_lineColor:I = 0x13

.field public static final Padlock_pinMode:I = 0x14

.field public static final Padlock_pinSubmitColor:I = 0x15

.field public static final Padlock_showDecimal:I = 0x16

.field public static final Padlock_submitColor:I = 0x17

.field public static final Padlock_submitDisabledColor:I = 0x18

.field public static final Padlock_submitSelector:I = 0x19

.field public static final Padlock_submitType:I = 0x1a

.field public static final Padlock_tabletMode:I = 0x1b

.field public static final PairLayout:[I

.field public static final PairLayout_android_orientation:I = 0x0

.field public static final PairLayout_childDimen:I = 0x1

.field public static final PairLayout_childPercentage:I = 0x2

.field public static final PairLayout_primaryChild:I = 0x3

.field public static final PreservedLabelView:[I

.field public static final PreservedLabelView_allowMultiLine:I = 0x0

.field public static final PreservedLabelView_preservedRowLayoutLabelStyle:I = 0x1

.field public static final PreservedLabelView_preservedRowLayoutTitleStyle:I = 0x2

.field public static final PreservedLabelView_spacing:I = 0x3

.field public static final PreservedLabelView_verticalSpacing:I = 0x4

.field public static final ResponsiveView:[I

.field public static final ResponsiveView_isResponsive:I = 0x0

.field public static final ScalingRadioButton:[I

.field public static final ScalingRadioButton_android_includeFontPadding:I = 0x0

.field public static final ScalingRadioButton_minTextSize:I = 0x1

.field public static final ScalingRadioButton_weight:I = 0x2

.field public static final ScalingTextView:[I

.field public static final ScalingTextView_android_includeFontPadding:I = 0x0

.field public static final ScalingTextView_android_shadowColor:I = 0x1

.field public static final ScalingTextView_android_shadowDx:I = 0x2

.field public static final ScalingTextView_android_shadowDy:I = 0x3

.field public static final ScalingTextView_android_shadowRadius:I = 0x4

.field public static final ScalingTextView_minTextSize:I = 0x5

.field public static final ScalingTextView_weight:I = 0x6

.field public static final SectionHeaderRow:[I

.field public static final SectionHeaderRow_sectionHeaderRowStyle:I = 0x0

.field public static final ShorteningTextView:[I

.field public static final ShorteningTextView_ellipsizeShortText:I = 0x0

.field public static final ShorteningTextView_shortText:I = 0x1

.field public static final SlidingTwoTabLayout:[I

.field public static final SlidingTwoTabLayout_indicatorColor:I = 0x0

.field public static final SlidingTwoTabLayout_indicatorHeight:I = 0x1

.field public static final TagPercentMarginLinearLayout:[I

.field public static final TagPercentMarginLinearLayout_marginPercentage:I = 0x0

.field public static final ThemedAlertDialog:[I

.field public static final ThemedAlertDialog_themedAlertDialogTheme:I = 0x0

.field public static final TitlePageIndicator:[I

.field public static final TitlePageIndicator_android_textColor:I = 0x1

.field public static final TitlePageIndicator_android_textSize:I = 0x0

.field public static final TitlePageIndicator_clipPadding:I = 0x2

.field public static final TitlePageIndicator_footerColor:I = 0x3

.field public static final TitlePageIndicator_footerIndicatorHeight:I = 0x4

.field public static final TitlePageIndicator_footerIndicatorStyle:I = 0x5

.field public static final TitlePageIndicator_footerIndicatorUnderlinePadding:I = 0x6

.field public static final TitlePageIndicator_footerLineHeight:I = 0x7

.field public static final TitlePageIndicator_footerPadding:I = 0x8

.field public static final TitlePageIndicator_selectedBold:I = 0x9

.field public static final TitlePageIndicator_selectedColor:I = 0xa

.field public static final TitlePageIndicator_titlePadding:I = 0xb

.field public static final TitlePageIndicator_topPadding:I = 0xc

.field public static final ToggleButtonRow:[I

.field public static final ToggleButtonRow_android_text:I = 0x0

.field public static final ToggleButtonRow_shortText:I = 0x1

.field public static final ToggleButtonRow_toggleButtonRowStyle:I = 0x2

.field public static final ToggleButtonRow_toggleButtonRowTextStyle:I = 0x3

.field public static final ToggleButtonRow_type:I = 0x4

.field public static final TriangleView:[I

.field public static final TriangleView_triangleColor:I = 0x0

.field public static final UnderlinePageIndicator:[I

.field public static final UnderlinePageIndicator_android_background:I = 0x0

.field public static final UnderlinePageIndicator_fadeDelay:I = 0x1

.field public static final UnderlinePageIndicator_fadeLength:I = 0x2

.field public static final UnderlinePageIndicator_fades:I = 0x3

.field public static final UnderlinePageIndicator_lineWidth:I = 0x4

.field public static final UnderlinePageIndicator_selectedColor:I = 0x5

.field public static final UnderlinePageIndicator_strokeWidth:I = 0x6

.field public static final WindowBackground:[I

.field public static final WindowBackground_marinWindowBackground:I = 0x0

.field public static final WrappingNameValueRow:[I

.field public static final WrappingNameValueRow_android_text:I = 0x0

.field public static final WrappingNameValueRow_showPercent:I = 0x1

.field public static final WrappingNameValueRow_value:I = 0x2

.field public static final WrappingNameValueRow_wrappingNameValueRowStyle:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v0, 0x2

    new-array v1, v0, [I

    .line 334
    fill-array-data v1, :array_0

    sput-object v1, Lcom/squareup/widgets/R$styleable;->AutoPaddingTextView:[I

    const/4 v1, 0x1

    new-array v2, v1, [I

    const/4 v3, 0x0

    const v4, 0x7f040065

    aput v4, v2, v3

    .line 337
    sput-object v2, Lcom/squareup/widgets/R$styleable;->BorderedScrollView:[I

    new-array v2, v1, [I

    const v4, 0x7f04029d

    aput v4, v2, v3

    .line 339
    sput-object v2, Lcom/squareup/widgets/R$styleable;->CardBackground:[I

    new-array v2, v1, [I

    const v4, 0x7f04009c

    aput v4, v2, v3

    .line 341
    sput-object v2, Lcom/squareup/widgets/R$styleable;->CenteredFrameLayout_LayoutParams:[I

    const/4 v2, 0x5

    new-array v4, v2, [I

    .line 343
    fill-array-data v4, :array_1

    sput-object v4, Lcom/squareup/widgets/R$styleable;->CheckableGroup:[I

    new-array v4, v0, [I

    .line 349
    fill-array-data v4, :array_2

    sput-object v4, Lcom/squareup/widgets/R$styleable;->CheckablePreservedLabelRow:[I

    const/4 v4, 0x3

    new-array v5, v4, [I

    .line 352
    fill-array-data v5, :array_3

    sput-object v5, Lcom/squareup/widgets/R$styleable;->ColumnLayout:[I

    new-array v5, v4, [I

    .line 356
    fill-array-data v5, :array_4

    sput-object v5, Lcom/squareup/widgets/R$styleable;->DigitInputView:[I

    const/4 v5, 0x4

    new-array v6, v5, [I

    .line 360
    fill-array-data v6, :array_5

    sput-object v6, Lcom/squareup/widgets/R$styleable;->DropDownContainer:[I

    new-array v6, v1, [I

    const v7, 0x10100c4

    aput v7, v6, v3

    .line 365
    sput-object v6, Lcom/squareup/widgets/R$styleable;->HalfAndHalfLayout:[I

    new-array v6, v5, [I

    .line 367
    fill-array-data v6, :array_6

    sput-object v6, Lcom/squareup/widgets/R$styleable;->HoloCompat:[I

    new-array v6, v4, [I

    .line 372
    fill-array-data v6, :array_7

    sput-object v6, Lcom/squareup/widgets/R$styleable;->HorizontalThreeChildLayout:[I

    new-array v6, v4, [I

    .line 376
    fill-array-data v6, :array_8

    sput-object v6, Lcom/squareup/widgets/R$styleable;->JaggedLineView:[I

    const/16 v6, 0xe

    new-array v6, v6, [I

    .line 380
    fill-array-data v6, :array_9

    sput-object v6, Lcom/squareup/widgets/R$styleable;->MessageView:[I

    new-array v6, v2, [I

    .line 395
    fill-array-data v6, :array_a

    sput-object v6, Lcom/squareup/widgets/R$styleable;->NameValueImageRow:[I

    const/4 v6, 0x7

    new-array v7, v6, [I

    .line 401
    fill-array-data v7, :array_b

    sput-object v7, Lcom/squareup/widgets/R$styleable;->NameValueRow:[I

    new-array v7, v1, [I

    const v8, 0x7f0402ff

    aput v8, v7, v3

    .line 409
    sput-object v7, Lcom/squareup/widgets/R$styleable;->NullStateHorizontalScrollView:[I

    new-array v7, v0, [I

    .line 411
    fill-array-data v7, :array_c

    sput-object v7, Lcom/squareup/widgets/R$styleable;->NullStateListView:[I

    const/16 v7, 0x1c

    new-array v7, v7, [I

    .line 414
    fill-array-data v7, :array_d

    sput-object v7, Lcom/squareup/widgets/R$styleable;->Padlock:[I

    new-array v7, v5, [I

    .line 443
    fill-array-data v7, :array_e

    sput-object v7, Lcom/squareup/widgets/R$styleable;->PairLayout:[I

    new-array v7, v2, [I

    .line 448
    fill-array-data v7, :array_f

    sput-object v7, Lcom/squareup/widgets/R$styleable;->PreservedLabelView:[I

    new-array v7, v1, [I

    const v8, 0x7f04021a

    aput v8, v7, v3

    .line 454
    sput-object v7, Lcom/squareup/widgets/R$styleable;->ResponsiveView:[I

    new-array v4, v4, [I

    .line 456
    fill-array-data v4, :array_10

    sput-object v4, Lcom/squareup/widgets/R$styleable;->ScalingRadioButton:[I

    new-array v4, v6, [I

    .line 460
    fill-array-data v4, :array_11

    sput-object v4, Lcom/squareup/widgets/R$styleable;->ScalingTextView:[I

    new-array v4, v1, [I

    const v7, 0x7f04033b

    aput v7, v4, v3

    .line 468
    sput-object v4, Lcom/squareup/widgets/R$styleable;->SectionHeaderRow:[I

    new-array v4, v0, [I

    .line 470
    fill-array-data v4, :array_12

    sput-object v4, Lcom/squareup/widgets/R$styleable;->ShorteningTextView:[I

    new-array v0, v0, [I

    .line 473
    fill-array-data v0, :array_13

    sput-object v0, Lcom/squareup/widgets/R$styleable;->SlidingTwoTabLayout:[I

    new-array v0, v1, [I

    const v4, 0x7f040294

    aput v4, v0, v3

    .line 476
    sput-object v0, Lcom/squareup/widgets/R$styleable;->TagPercentMarginLinearLayout:[I

    new-array v0, v1, [I

    const v4, 0x7f040451

    aput v4, v0, v3

    .line 478
    sput-object v0, Lcom/squareup/widgets/R$styleable;->ThemedAlertDialog:[I

    const/16 v0, 0xd

    new-array v0, v0, [I

    .line 480
    fill-array-data v0, :array_14

    sput-object v0, Lcom/squareup/widgets/R$styleable;->TitlePageIndicator:[I

    new-array v0, v2, [I

    .line 494
    fill-array-data v0, :array_15

    sput-object v0, Lcom/squareup/widgets/R$styleable;->ToggleButtonRow:[I

    new-array v0, v1, [I

    const v2, 0x7f04047a

    aput v2, v0, v3

    .line 500
    sput-object v0, Lcom/squareup/widgets/R$styleable;->TriangleView:[I

    new-array v0, v6, [I

    .line 502
    fill-array-data v0, :array_16

    sput-object v0, Lcom/squareup/widgets/R$styleable;->UnderlinePageIndicator:[I

    new-array v0, v1, [I

    const v1, 0x7f0402a2

    aput v1, v0, v3

    .line 510
    sput-object v0, Lcom/squareup/widgets/R$styleable;->WindowBackground:[I

    new-array v0, v5, [I

    .line 512
    fill-array-data v0, :array_17

    sput-object v0, Lcom/squareup/widgets/R$styleable;->WrappingNameValueRow:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f040039
        0x7f04003a
    .end array-data

    :array_1
    .array-data 4
        0x1010129
        0x7f040217
        0x7f040354
        0x7f040370
        0x7f04048c
    .end array-data

    :array_2
    .array-data 4
        0x7f04009e
        0x7f04009f
    .end array-data

    :array_3
    .array-data 4
        0x1010129
        0x7f0403c6
        0x7f04048c
    .end array-data

    :array_4
    .array-data 4
        0x1010095
        0x7f040125
        0x7f040127
    .end array-data

    :array_5
    .array-data 4
        0x7f040147
        0x7f040148
        0x7f04019f
        0x7f040335
    .end array-data

    :array_6
    .array-data 4
        0x7f0403cd
        0x7f0403ce
        0x7f0403cf
        0x7f0403d0
    .end array-data

    :array_7
    .array-data 4
        0x7f040186
        0x7f04033a
        0x7f040453
    .end array-data

    :array_8
    .array-data 4
        0x7f040065
        0x7f040183
        0x7f04047b
    .end array-data

    :array_9
    .array-data 4
        0x1010095
        0x1010097
        0x1010098
        0x10100ab
        0x10100af
        0x101014f
        0x101015f
        0x1010161
        0x1010162
        0x1010163
        0x1010164
        0x7f04015f
        0x7f04044b
        0x7f040497
    .end array-data

    :array_a
    .array-data 4
        0x1010119
        0x101014f
        0x7f0402c5
        0x7f0402c6
        0x7f040347
    .end array-data

    :array_b
    .array-data 4
        0x101014f
        0x7f0402c5
        0x7f0402c6
        0x7f0402c7
        0x7f040347
        0x7f04034e
        0x7f040491
    .end array-data

    :array_c
    .array-data 4
        0x7f0402fe
        0x7f0402ff
    .end array-data

    :array_d
    .array-data 4
        0x1010217
        0x7f04004d
        0x7f04004e
        0x7f04004f
        0x7f040086
        0x7f04008b
        0x7f0400c1
        0x7f0400c2
        0x7f04011c
        0x7f040124
        0x7f040126
        0x7f040139
        0x7f04013a
        0x7f04013b
        0x7f04013c
        0x7f0401c4
        0x7f040278
        0x7f040279
        0x7f04027a
        0x7f04027d
        0x7f040313
        0x7f040314
        0x7f040349
        0x7f0403fe
        0x7f0403ff
        0x7f040400
        0x7f040401
        0x7f040429
    .end array-data

    :array_e
    .array-data 4
        0x10100c4
        0x7f0400aa
        0x7f0400ab
        0x7f04031f
    .end array-data

    :array_f
    .array-data 4
        0x7f04002d
        0x7f04031c
        0x7f04031d
        0x7f04035c
        0x7f040494
    .end array-data

    :array_10
    .array-data 4
        0x101015f
        0x7f0402c2
        0x7f040497
    .end array-data

    :array_11
    .array-data 4
        0x101015f
        0x1010161
        0x1010162
        0x1010163
        0x1010164
        0x7f0402c2
        0x7f040497
    .end array-data

    :array_12
    .array-data 4
        0x7f040154
        0x7f040347
    .end array-data

    :array_13
    .array-data 4
        0x7f04020c
        0x7f04020e
    .end array-data

    :array_14
    .array-data 4
        0x1010095
        0x1010098
        0x7f0400c4
        0x7f040196
        0x7f040197
        0x7f040198
        0x7f040199
        0x7f04019a
        0x7f04019b
        0x7f040340
        0x7f040341
        0x7f040465
        0x7f040474
    .end array-data

    :array_15
    .array-data 4
        0x101014f
        0x7f040347
        0x7f04046b
        0x7f04046c
        0x7f04048d
    .end array-data

    :array_16
    .array-data 4
        0x10100d4
        0x7f040179
        0x7f04017a
        0x7f04017b
        0x7f040282
        0x7f040341
        0x7f0403fa
    .end array-data

    :array_17
    .array-data 4
        0x101014f
        0x7f04034e
        0x7f040491
        0x7f0404a3
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 332
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
