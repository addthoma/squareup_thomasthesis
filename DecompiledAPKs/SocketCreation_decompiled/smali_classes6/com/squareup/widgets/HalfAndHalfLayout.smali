.class public Lcom/squareup/widgets/HalfAndHalfLayout;
.super Landroid/view/ViewGroup;
.source "HalfAndHalfLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;
    }
.end annotation


# instance fields
.field private orientation:Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 24
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    sget-object v0, Lcom/squareup/widgets/R$styleable;->HalfAndHalfLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 26
    sget p2, Lcom/squareup/widgets/R$styleable;->HalfAndHalfLayout_android_orientation:I

    const/4 v0, -0x1

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    if-eq p2, v0, :cond_0

    .line 31
    invoke-static {}, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;->values()[Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    move-result-object v0

    aget-object p2, v0, p2

    iput-object p2, p0, Lcom/squareup/widgets/HalfAndHalfLayout;->orientation:Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    .line 32
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    .line 29
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "HalfAndHalfLayout requires an android:orientation specified"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private getChildHeight(I)I
    .locals 2

    .line 73
    invoke-virtual {p0}, Lcom/squareup/widgets/HalfAndHalfLayout;->getPaddingTop()I

    move-result v0

    sub-int/2addr p1, v0

    invoke-virtual {p0}, Lcom/squareup/widgets/HalfAndHalfLayout;->getPaddingBottom()I

    move-result v0

    sub-int/2addr p1, v0

    .line 74
    iget-object v0, p0, Lcom/squareup/widgets/HalfAndHalfLayout;->orientation:Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    sget-object v1, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;->VERTICAL:Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    if-ne v0, v1, :cond_0

    shr-int/lit8 p1, p1, 0x1

    :cond_0
    return p1
.end method

.method private getChildWidth(I)I
    .locals 2

    .line 79
    invoke-virtual {p0}, Lcom/squareup/widgets/HalfAndHalfLayout;->getPaddingLeft()I

    move-result v0

    sub-int/2addr p1, v0

    invoke-virtual {p0}, Lcom/squareup/widgets/HalfAndHalfLayout;->getPaddingRight()I

    move-result v0

    sub-int/2addr p1, v0

    .line 80
    iget-object v0, p0, Lcom/squareup/widgets/HalfAndHalfLayout;->orientation:Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    sget-object v1, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;->VERTICAL:Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    shr-int/lit8 p1, p1, 0x1

    :goto_0
    return p1
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 6

    .line 50
    invoke-virtual {p0}, Lcom/squareup/widgets/HalfAndHalfLayout;->getPaddingLeft()I

    move-result p1

    .line 51
    invoke-virtual {p0}, Lcom/squareup/widgets/HalfAndHalfLayout;->getPaddingTop()I

    move-result v0

    sub-int/2addr p4, p2

    .line 52
    invoke-direct {p0, p4}, Lcom/squareup/widgets/HalfAndHalfLayout;->getChildWidth(I)I

    move-result p2

    sub-int/2addr p5, p3

    .line 53
    invoke-direct {p0, p5}, Lcom/squareup/widgets/HalfAndHalfLayout;->getChildHeight(I)I

    move-result p3

    .line 55
    invoke-virtual {p0}, Lcom/squareup/widgets/HalfAndHalfLayout;->getChildCount()I

    move-result p4

    const/4 p5, 0x0

    :goto_0
    if-ge p5, p4, :cond_1

    .line 57
    invoke-virtual {p0, p5}, Lcom/squareup/widgets/HalfAndHalfLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 60
    iget-object v2, p0, Lcom/squareup/widgets/HalfAndHalfLayout;->orientation:Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    sget-object v3, Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;->VERTICAL:Lcom/squareup/widgets/HalfAndHalfLayout$Orientation;

    if-ne v2, v3, :cond_0

    mul-int v2, p5, p3

    add-int/2addr v2, v0

    move v3, v2

    move v2, p1

    goto :goto_1

    :cond_0
    mul-int v2, p5, p2

    add-int/2addr v2, p1

    move v3, v0

    :goto_1
    add-int v4, v2, p2

    add-int v5, v3, p3

    .line 67
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .line 36
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 37
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p2

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/widgets/HalfAndHalfLayout;->getChildWidth(I)I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 39
    invoke-direct {p0, p2}, Lcom/squareup/widgets/HalfAndHalfLayout;->getChildHeight(I)I

    move-result v2

    invoke-static {v2, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/widgets/HalfAndHalfLayout;->getChildCount()I

    move-result v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    .line 43
    invoke-virtual {p0, v3}, Lcom/squareup/widgets/HalfAndHalfLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Landroid/view/View;->measure(II)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 46
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/HalfAndHalfLayout;->setMeasuredDimension(II)V

    return-void
.end method
