.class public Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;
.super Landroid/widget/ArrayAdapter;
.source "EmailAutoCompleteFilterAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailDomains;,
        Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailFilter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final domains:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final nameFilter:Landroid/widget/Filter;

.field private final viewResourceId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/squareup/util/Res;)V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailDomains;

    invoke-direct {v0, p2}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailDomains;-><init>(Lcom/squareup/util/Res;)V

    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;-><init>(Landroid/content/Context;Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailDomains;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailDomains;)V
    .locals 2

    .line 32
    sget v0, Lcom/squareup/widgets/R$layout;->auto_complete_row:I

    invoke-virtual {p2}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailDomains;->getDomains()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 33
    invoke-virtual {p2}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailDomains;->getDomains()Ljava/util/ArrayList;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;->domains:Ljava/util/List;

    .line 34
    sget p1, Lcom/squareup/widgets/R$layout;->auto_complete_row:I

    iput p1, p0, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;->viewResourceId:I

    .line 35
    new-instance p1, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailFilter;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$EmailFilter;-><init>(Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter$1;)V

    iput-object p1, p0, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;->nameFilter:Landroid/widget/Filter;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;)Ljava/util/List;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;->domains:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public getFilter()Landroid/widget/Filter;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;->nameFilter:Landroid/widget/Filter;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    if-nez p2, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p2

    iget p3, p0, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;->viewResourceId:I

    const/4 v0, 0x0

    invoke-virtual {p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 43
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    if-eqz p1, :cond_1

    .line 45
    move-object p3, p2

    check-cast p3, Landroid/widget/TextView;

    if-eqz p3, :cond_1

    .line 47
    invoke-virtual {p3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-object p2
.end method
