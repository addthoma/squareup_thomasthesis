.class final Lcom/squareup/widgets/warning/WarningStrings$1;
.super Ljava/lang/Object;
.source "WarningStrings.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/warning/WarningStrings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/widgets/warning/WarningStrings;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/widgets/warning/WarningStrings;
    .locals 2

    .line 46
    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 44
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/warning/WarningStrings$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/widgets/warning/WarningStrings;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/widgets/warning/WarningStrings;
    .locals 0

    .line 50
    new-array p1, p1, [Lcom/squareup/widgets/warning/WarningStrings;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 44
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/warning/WarningStrings$1;->newArray(I)[Lcom/squareup/widgets/warning/WarningStrings;

    move-result-object p1

    return-object p1
.end method
