.class Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;
.super Ljava/lang/Object;
.source "EqualizingTextView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/EqualizingTextView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LayoutDelta"
.end annotation


# instance fields
.field public final average:F

.field public final max:F


# direct methods
.method constructor <init>(FF)V
    .locals 0

    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    iput p1, p0, Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;->average:F

    .line 219
    iput p2, p0, Lcom/squareup/widgets/EqualizingTextView$LayoutDelta;->max:F

    return-void
.end method
