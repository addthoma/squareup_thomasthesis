.class public Lcom/squareup/widgets/MessageView;
.super Landroid/widget/FrameLayout;
.source "MessageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/MessageView$TextJustification;
    }
.end annotation


# static fields
.field private static final BUNDLE_SUPER_KEY:Ljava/lang/String; = "parcelable"

.field private static final BUNDLE_TEXT_KEY:Ljava/lang/String; = "text"

.field private static final DEFAULT_GRAVITY:I = 0x800033


# instance fields
.field private equalizeLines:Ljava/lang/Boolean;

.field private final forceTextJustificationLeft:Z

.field private final isSymbolBased:Z

.field private textJustification:Lcom/squareup/widgets/MessageView$TextJustification;

.field private final textView:Lcom/squareup/widgets/EqualizingTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    sget-object v0, Lcom/squareup/widgets/MessageView$TextJustification;->GRAVITY:Lcom/squareup/widgets/MessageView$TextJustification;

    iput-object v0, p0, Lcom/squareup/widgets/MessageView;->textJustification:Lcom/squareup/widgets/MessageView$TextJustification;

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 47
    sget-object v1, Lcom/squareup/widgets/R$styleable;->MessageView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 48
    sget v2, Lcom/squareup/widgets/R$bool;->message_view_symbol_based:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/squareup/widgets/MessageView;->isSymbolBased:Z

    .line 51
    iget-boolean v2, p0, Lcom/squareup/widgets/MessageView;->isSymbolBased:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 53
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v2, p0, Lcom/squareup/widgets/MessageView;->equalizeLines:Ljava/lang/Boolean;

    goto :goto_0

    .line 54
    :cond_0
    sget v2, Lcom/squareup/widgets/R$styleable;->MessageView_equalizeLines:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 55
    sget v2, Lcom/squareup/widgets/R$styleable;->MessageView_equalizeLines:I

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/squareup/widgets/MessageView;->equalizeLines:Ljava/lang/Boolean;

    goto :goto_0

    .line 57
    :cond_1
    iput-object v3, p0, Lcom/squareup/widgets/MessageView;->equalizeLines:Ljava/lang/Boolean;

    .line 61
    :goto_0
    new-instance v2, Lcom/squareup/widgets/EqualizingTextView;

    invoke-direct {v2, p1, p2}, Lcom/squareup/widgets/EqualizingTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    .line 63
    iget-object v2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Lcom/squareup/widgets/EqualizingTextView;->setId(I)V

    .line 64
    iget-object v2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/squareup/widgets/EqualizingTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 65
    iget-object v2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/squareup/widgets/EqualizingTextView;->setFocusable(Z)V

    .line 66
    iget-object v2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {p0, v2}, Lcom/squareup/widgets/MessageView;->addView(Landroid/view/View;)V

    .line 69
    iget-object v2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v2}, Lcom/squareup/widgets/EqualizingTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v5, -0x2

    .line 70
    iput v5, v2, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 71
    iput v5, v2, Landroid/widget/FrameLayout$LayoutParams;->height:I

    const v5, 0x800033

    .line 72
    iput v5, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 73
    invoke-virtual {v2, v4, v4, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 74
    iget-object v6, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v6, v2}, Lcom/squareup/widgets/EqualizingTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 75
    iget-object v2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v2, v4, v4, v4, v4}, Lcom/squareup/widgets/EqualizingTextView;->setPadding(IIII)V

    .line 76
    iget-object v2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v2, v5}, Lcom/squareup/widgets/EqualizingTextView;->setGravity(I)V

    .line 77
    iget-object v2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v2, v4}, Lcom/squareup/widgets/EqualizingTextView;->setVisibility(I)V

    .line 78
    iget-object v2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v2, v3, v3, v3, v3}, Lcom/squareup/widgets/EqualizingTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 79
    iget-object v2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v2, v3}, Lcom/squareup/widgets/EqualizingTextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 82
    sget-object v2, Lcom/squareup/widgets/R$styleable;->MessageView:[I

    sget v3, Lcom/squareup/widgets/R$styleable;->MessageView_weight:I

    invoke-static {p1, p2, v2, v3, v4}, Lcom/squareup/marketfont/MarketUtils;->getWeight(Landroid/content/Context;Landroid/util/AttributeSet;[III)Lcom/squareup/marketfont/MarketFont$Weight;

    move-result-object p2

    .line 84
    invoke-virtual {p0}, Lcom/squareup/widgets/MessageView;->isInEditMode()Z

    move-result v2

    if-nez v2, :cond_2

    .line 85
    iget-object v2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v2}, Lcom/squareup/widgets/EqualizingTextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {p1, p2, v3}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/widgets/EqualizingTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 86
    iget-object p1, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-static {p1, p2}, Lcom/squareup/marketfont/MarketUtils;->setTextViewTypeface(Landroid/widget/TextView;Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 90
    :cond_2
    iget-boolean p1, p0, Lcom/squareup/widgets/MessageView;->isSymbolBased:Z

    if-nez p1, :cond_3

    .line 91
    new-instance p1, Lcom/squareup/widgets/OrphanedCharacterTextWatcher;

    iget-object p2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-direct {p1, p2}, Lcom/squareup/widgets/OrphanedCharacterTextWatcher;-><init>(Landroid/widget/TextView;)V

    .line 92
    iget-object p2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/EqualizingTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 95
    iget-object p2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {p2}, Lcom/squareup/widgets/EqualizingTextView;->getEditableText()Landroid/text/Editable;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/OrphanedCharacterTextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    .line 99
    :cond_3
    sget p1, Lcom/squareup/widgets/R$bool;->message_view_force_justification_left:I

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/widgets/MessageView;->forceTextJustificationLeft:Z

    .line 100
    sget p1, Lcom/squareup/widgets/R$styleable;->MessageView_textJustification:I

    sget-object p2, Lcom/squareup/widgets/MessageView$TextJustification;->GRAVITY:Lcom/squareup/widgets/MessageView$TextJustification;

    iget p2, p2, Lcom/squareup/widgets/MessageView$TextJustification;->value:I

    .line 101
    invoke-virtual {v1, p1, p2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p1

    .line 100
    invoke-static {p1}, Lcom/squareup/widgets/MessageView$TextJustification;->fromValue(I)Lcom/squareup/widgets/MessageView$TextJustification;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/MessageView;->setTextJustification(Lcom/squareup/widgets/MessageView$TextJustification;)V

    .line 104
    sget p1, Lcom/squareup/widgets/R$styleable;->MessageView_android_gravity:I

    invoke-virtual {v1, p1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result p1

    if-eqz p1, :cond_4

    .line 105
    sget p1, Lcom/squareup/widgets/R$styleable;->MessageView_android_gravity:I

    invoke-virtual {v1, p1, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/MessageView;->setGravity(I)V

    .line 108
    :cond_4
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private updateTextJustificationGravity()V
    .locals 4

    .line 228
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textJustification:Lcom/squareup/widgets/MessageView$TextJustification;

    invoke-static {v0}, Lcom/squareup/widgets/MessageView$TextJustification;->access$000(Lcom/squareup/widgets/MessageView$TextJustification;)I

    move-result v0

    .line 229
    iget-object v1, p0, Lcom/squareup/widgets/MessageView;->textJustification:Lcom/squareup/widgets/MessageView$TextJustification;

    sget-object v2, Lcom/squareup/widgets/MessageView$TextJustification;->GRAVITY:Lcom/squareup/widgets/MessageView$TextJustification;

    const v3, 0x800007

    if-ne v1, v2, :cond_0

    .line 230
    invoke-virtual {p0}, Lcom/squareup/widgets/MessageView;->getGravity()I

    move-result v0

    and-int/2addr v0, v3

    .line 233
    :cond_0
    iget-object v1, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v1}, Lcom/squareup/widgets/EqualizingTextView;->getGravity()I

    move-result v1

    and-int v2, v1, v3

    xor-int/2addr v1, v2

    or-int/2addr v1, v0

    .line 237
    iget-object v2, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v2, v1}, Lcom/squareup/widgets/EqualizingTextView;->setGravity(I)V

    .line 241
    iget-object v1, p0, Lcom/squareup/widgets/MessageView;->equalizeLines:Ljava/lang/Boolean;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    :goto_0
    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    iget-boolean v1, p0, Lcom/squareup/widgets/MessageView;->isSymbolBased:Z

    if-nez v1, :cond_2

    if-eq v0, v3, :cond_2

    goto :goto_0

    .line 243
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v0, v2}, Lcom/squareup/widgets/EqualizingTextView;->setEqualizationDisabled(Z)V

    .line 245
    invoke-virtual {p0}, Lcom/squareup/widgets/MessageView;->invalidate()V

    return-void
.end method


# virtual methods
.method getGravity()I
    .locals 1

    .line 197
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v0}, Lcom/squareup/widgets/EqualizingTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    return v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v0}, Lcom/squareup/widgets/EqualizingTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTextView()Landroid/widget/TextView;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .line 184
    check-cast p1, Landroid/os/Bundle;

    .line 185
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    const-string v1, "text"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/EqualizingTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "parcelable"

    .line 186
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    .line 187
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .line 177
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 178
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    const-string v2, "parcelable"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 179
    iget-object v1, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v1}, Lcom/squareup/widgets/EqualizingTextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    const-string v2, "text"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v0
.end method

.method public setClickable(Z)V
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/EqualizingTextView;->setClickable(Z)V

    return-void
.end method

.method public setEqualizeLines(Z)V
    .locals 1

    .line 156
    iget-boolean v0, p0, Lcom/squareup/widgets/MessageView;->isSymbolBased:Z

    if-eqz v0, :cond_0

    return-void

    .line 160
    :cond_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/widgets/MessageView;->equalizeLines:Ljava/lang/Boolean;

    .line 161
    invoke-direct {p0}, Lcom/squareup/widgets/MessageView;->updateTextJustificationGravity()V

    return-void
.end method

.method public setFreezesText(Z)V
    .locals 1

    .line 150
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/EqualizingTextView;->setFreezesText(Z)V

    return-void
.end method

.method public setGravity(I)V
    .locals 1

    .line 220
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v0}, Lcom/squareup/widgets/EqualizingTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 221
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 222
    iget-object p1, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/EqualizingTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 223
    invoke-direct {p0}, Lcom/squareup/widgets/MessageView;->updateTextJustificationGravity()V

    return-void
.end method

.method public setMaxWidth(I)V
    .locals 1

    .line 209
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/EqualizingTextView;->setMaxWidth(I)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 173
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/EqualizingTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setText(I)V
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/EqualizingTextView;->setText(I)V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 124
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/EqualizingTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTextAndVisibility(I)V
    .locals 2

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 145
    :goto_0
    invoke-static {p0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    if-eq p1, v0, :cond_1

    .line 146
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    :cond_1
    return-void
.end method

.method public setTextAndVisibility(Ljava/lang/CharSequence;)V
    .locals 1

    .line 136
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 137
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/widgets/EqualizingTextView;->setTextAppearance(Landroid/content/Context;I)V

    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .line 201
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/EqualizingTextView;->setTextColor(I)V

    return-void
.end method

.method public setTextColor(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/EqualizingTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method setTextJustification(Lcom/squareup/widgets/MessageView$TextJustification;)V
    .locals 1

    const-string v0, "justification"

    .line 191
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 192
    iget-boolean v0, p0, Lcom/squareup/widgets/MessageView;->forceTextJustificationLeft:Z

    if-eqz v0, :cond_0

    sget-object p1, Lcom/squareup/widgets/MessageView$TextJustification;->LEFT:Lcom/squareup/widgets/MessageView$TextJustification;

    :cond_0
    iput-object p1, p0, Lcom/squareup/widgets/MessageView;->textJustification:Lcom/squareup/widgets/MessageView$TextJustification;

    .line 193
    invoke-direct {p0}, Lcom/squareup/widgets/MessageView;->updateTextJustificationGravity()V

    return-void
.end method

.method public setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V
    .locals 1

    .line 165
    iget-object v0, p0, Lcom/squareup/widgets/MessageView;->textView:Lcom/squareup/widgets/EqualizingTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/EqualizingTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    return-void
.end method
