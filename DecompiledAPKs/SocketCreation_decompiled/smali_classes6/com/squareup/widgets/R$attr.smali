.class public final Lcom/squareup/widgets/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final alertDialogButtonBarBackground:I = 0x7f040023

.field public static final alertDialogButtonBarHeight:I = 0x7f040024

.field public static final alertDialogButtonOuterPadding:I = 0x7f040026

.field public static final alertDialogDividerBackground:I = 0x7f040028

.field public static final alertDialogDividerHeight:I = 0x7f040029

.field public static final alertDialogTitleHeight:I = 0x7f04002c

.field public static final allowMultiLine:I = 0x7f04002d

.field public static final autoPadding:I = 0x7f040039

.field public static final autoPaddingTextViewStyle:I = 0x7f04003a

.field public static final backspaceColor:I = 0x7f04004d

.field public static final backspaceDisabledColor:I = 0x7f04004e

.field public static final backspaceSelector:I = 0x7f04004f

.field public static final borderColor:I = 0x7f040065

.field public static final buttonSelector:I = 0x7f040086

.field public static final buttonTextSize:I = 0x7f04008b

.field public static final centerOffsetLeft:I = 0x7f04009c

.field public static final checkablePreservedLabelRowStyle:I = 0x7f04009e

.field public static final checkablePreservedLabelViewInternalStyle:I = 0x7f04009f

.field public static final chevronVisibility:I = 0x7f0400a9

.field public static final childDimen:I = 0x7f0400aa

.field public static final childPercentage:I = 0x7f0400ab

.field public static final clearTextColor:I = 0x7f0400c1

.field public static final clearTextDisabledColor:I = 0x7f0400c2

.field public static final clipPadding:I = 0x7f0400c4

.field public static final digitColor:I = 0x7f040124

.field public static final digitCount:I = 0x7f040125

.field public static final digitDisabledColor:I = 0x7f040126

.field public static final digitInputViewStyle:I = 0x7f040127

.field public static final drawLeftLine:I = 0x7f04013a

.field public static final drawRightLine:I = 0x7f04013b

.field public static final drawTopLine:I = 0x7f04013c

.field public static final dropDownContainerStyle:I = 0x7f040147

.field public static final dropDownDuration:I = 0x7f040148

.field public static final ellipsizeShortText:I = 0x7f040154

.field public static final equalizeLines:I = 0x7f04015f

.field public static final fadeDelay:I = 0x7f040179

.field public static final fadeLength:I = 0x7f04017a

.field public static final fades:I = 0x7f04017b

.field public static final fillColor:I = 0x7f040183

.field public static final firstChildPercentage:I = 0x7f040186

.field public static final footerColor:I = 0x7f040196

.field public static final footerIndicatorHeight:I = 0x7f040197

.field public static final footerIndicatorStyle:I = 0x7f040198

.field public static final footerIndicatorUnderlinePadding:I = 0x7f040199

.field public static final footerLineHeight:I = 0x7f04019a

.field public static final footerPadding:I = 0x7f04019b

.field public static final glassCancel:I = 0x7f04019f

.field public static final horizontalDividerStyle:I = 0x7f0401c4

.field public static final indicatorColor:I = 0x7f04020c

.field public static final indicatorHeight:I = 0x7f04020e

.field public static final isHorizontal:I = 0x7f040217

.field public static final isResponsive:I = 0x7f04021a

.field public static final lettersColor:I = 0x7f040278

.field public static final lettersDisabledColor:I = 0x7f040279

.field public static final lettersTextSize:I = 0x7f04027a

.field public static final lineColor:I = 0x7f04027d

.field public static final lineWidth:I = 0x7f040282

.field public static final marginPercentage:I = 0x7f040294

.field public static final marinCardBackground:I = 0x7f04029d

.field public static final marinWindowBackground:I = 0x7f0402a2

.field public static final minTextSize:I = 0x7f0402c2

.field public static final nameValueRowNameStyle:I = 0x7f0402c5

.field public static final nameValueRowStyle:I = 0x7f0402c6

.field public static final nameValueRowValueStyle:I = 0x7f0402c7

.field public static final nullStateNegativeTopMargin:I = 0x7f0402fe

.field public static final nullStateText:I = 0x7f0402ff

.field public static final pinMode:I = 0x7f040313

.field public static final pinSubmitColor:I = 0x7f040314

.field public static final preservedRowLayoutLabelStyle:I = 0x7f04031c

.field public static final preservedRowLayoutTitleStyle:I = 0x7f04031d

.field public static final primaryChild:I = 0x7f04031f

.field public static final scrimColor:I = 0x7f040335

.field public static final secondChildPercentage:I = 0x7f04033a

.field public static final sectionHeaderRowStyle:I = 0x7f04033b

.field public static final selectedBold:I = 0x7f040340

.field public static final selectedColor:I = 0x7f040341

.field public static final shortText:I = 0x7f040347

.field public static final showDecimal:I = 0x7f040349

.field public static final showPercent:I = 0x7f04034e

.field public static final singleChoice:I = 0x7f040354

.field public static final spacing:I = 0x7f04035c

.field public static final sqCheckedButton:I = 0x7f040370

.field public static final sq_marginBetweenColumns:I = 0x7f0403c6

.field public static final square_buttonBarButtonStyle:I = 0x7f0403cd

.field public static final square_buttonBarStyle:I = 0x7f0403ce

.field public static final square_dividerHorizontal:I = 0x7f0403cf

.field public static final square_dividerVertical:I = 0x7f0403d0

.field public static final square_layout:I = 0x7f0403d4

.field public static final square_listItemLayout:I = 0x7f0403d5

.field public static final square_switchStyle:I = 0x7f0403d7

.field public static final state_checked:I = 0x7f0403e5

.field public static final strokeWidth:I = 0x7f0403fa

.field public static final submitColor:I = 0x7f0403fe

.field public static final submitDisabledColor:I = 0x7f0403ff

.field public static final submitSelector:I = 0x7f040400

.field public static final tabletMode:I = 0x7f040429

.field public static final textJustification:I = 0x7f04044b

.field public static final themedAlertDialogTheme:I = 0x7f040451

.field public static final thirdChildPercentage:I = 0x7f040453

.field public static final titlePadding:I = 0x7f040465

.field public static final toggleButtonRowStyle:I = 0x7f04046b

.field public static final toggleButtonRowTextStyle:I = 0x7f04046c

.field public static final topPadding:I = 0x7f040474

.field public static final triangleColor:I = 0x7f04047a

.field public static final triangleCount:I = 0x7f04047b

.field public static final twoColumns:I = 0x7f04048c

.field public static final type:I = 0x7f04048d

.field public static final value:I = 0x7f040491

.field public static final verticalSpacing:I = 0x7f040494

.field public static final weight:I = 0x7f040497

.field public static final wrappingNameValueRowStyle:I = 0x7f0404a3


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
