.class Lcom/squareup/widgets/OrphanedCharacterTextWatcher;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "OrphanedCharacterTextWatcher.java"


# static fields
.field private static final MIN_ORPHANED_CHARACTERS:I = 0xa

.field private static final NBSP:C = '\u00a0'

.field private static final SP:C = ' '


# instance fields
.field private ignoreEvents:Z

.field private final view:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/widgets/OrphanedCharacterTextWatcher;->view:Landroid/widget/TextView;

    return-void
.end method

.method private findIndices(Ljava/lang/CharSequence;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 109
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, -0x1

    :goto_0
    if-ge v3, v1, :cond_2

    .line 111
    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    const/16 v6, 0xa

    if-ne v5, v6, :cond_0

    if-le v4, v2, :cond_1

    .line 114
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v4, -0x1

    goto :goto_1

    .line 117
    :cond_0
    invoke-static {v5}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v5

    if-nez v5, :cond_1

    move v4, v3

    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x1

    if-le v1, p1, :cond_3

    sub-int/2addr v1, p1

    .line 123
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    return-object v0
.end method

.method private insertNonBreakingSpaces()V
    .locals 7

    .line 33
    iget-object v0, p0, Lcom/squareup/widgets/OrphanedCharacterTextWatcher;->view:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 34
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 36
    invoke-direct {p0, v0}, Lcom/squareup/widgets/OrphanedCharacterTextWatcher;->findIndices(Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v6, 0xa

    .line 37
    invoke-direct {p0, v1, v6, v4, v5}, Lcom/squareup/widgets/OrphanedCharacterTextWatcher;->insertNonBreakingSpaces(Landroid/text/SpannableStringBuilder;III)Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    goto :goto_2

    :cond_1
    :goto_1
    const/4 v3, 0x1

    :goto_2
    add-int/lit8 v4, v5, 0x1

    goto :goto_0

    :cond_2
    if-eqz v3, :cond_3

    .line 41
    iget-object v0, p0, Lcom/squareup/widgets/OrphanedCharacterTextWatcher;->view:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    return-void
.end method

.method private insertNonBreakingSpaces(Landroid/text/SpannableStringBuilder;III)Z
    .locals 9

    sub-int p3, p4, p3

    .line 53
    div-int/lit8 p3, p3, 0x2

    .line 54
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    move-result p2

    const/4 v0, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v5, 0x0

    :goto_0
    if-lez p4, :cond_7

    if-ge v2, p3, :cond_7

    .line 60
    invoke-virtual {p1, p4}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v6

    add-int/lit8 v7, p4, -0x1

    .line 61
    invoke-virtual {p1, v7}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v7

    const/16 v8, 0xa

    if-eq v6, v8, :cond_7

    if-ne v7, v8, :cond_0

    goto :goto_2

    :cond_0
    const/4 v7, 0x1

    if-nez v3, :cond_1

    const/16 v8, 0x3e

    if-ne v6, v8, :cond_1

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    if-eqz v3, :cond_2

    const/16 v8, 0x3c

    if-ne v6, v8, :cond_2

    const/4 v3, 0x0

    goto :goto_1

    :cond_2
    if-nez v3, :cond_6

    const/16 v8, 0x20

    if-ne v6, v8, :cond_6

    const/16 v6, 0xa0

    if-eq v4, v0, :cond_3

    add-int/lit8 v5, v4, 0x1

    .line 77
    invoke-static {v6}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v4, v5, v8}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/4 v5, 0x1

    :cond_3
    if-lt v2, p2, :cond_4

    goto :goto_2

    :cond_4
    if-ne v2, v7, :cond_5

    add-int/lit8 v4, p4, 0x1

    .line 87
    invoke-static {v6}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, p4, v4, v5}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/4 v4, -0x1

    const/4 v5, 0x1

    goto :goto_1

    :cond_5
    move v4, p4

    :cond_6
    :goto_1
    add-int/lit8 p4, p4, -0x1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_7
    :goto_2
    return v5
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .line 24
    iget-boolean p1, p0, Lcom/squareup/widgets/OrphanedCharacterTextWatcher;->ignoreEvents:Z

    if-nez p1, :cond_0

    const/4 p1, 0x1

    .line 25
    iput-boolean p1, p0, Lcom/squareup/widgets/OrphanedCharacterTextWatcher;->ignoreEvents:Z

    .line 26
    invoke-direct {p0}, Lcom/squareup/widgets/OrphanedCharacterTextWatcher;->insertNonBreakingSpaces()V

    const/4 p1, 0x0

    .line 27
    iput-boolean p1, p0, Lcom/squareup/widgets/OrphanedCharacterTextWatcher;->ignoreEvents:Z

    :cond_0
    return-void
.end method
