.class public Lcom/squareup/widgets/SelectableAutoCompleteEditText;
.super Lcom/squareup/marketfont/MarketAutoCompleteTextView;
.source "SelectableAutoCompleteEditText.java"

# interfaces
.implements Lcom/squareup/text/HasSelectableText;


# instance fields
.field private scrubbingTextWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

.field private final selectionWatchers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/text/SelectionWatcher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1}, Lcom/squareup/marketfont/MarketAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 25
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->selectionWatchers:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/squareup/marketfont/MarketAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->selectionWatchers:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/marketfont/MarketAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->selectionWatchers:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->selectionWatchers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 52
    instance-of v0, p1, Lcom/squareup/text/ScrubbingTextWatcher;

    if-eqz v0, :cond_1

    .line 53
    iget-object v0, p0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->scrubbingTextWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    if-nez v0, :cond_0

    .line 57
    move-object v0, p1

    check-cast v0, Lcom/squareup/text/ScrubbingTextWatcher;

    iput-object v0, p0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->scrubbingTextWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    goto :goto_0

    .line 54
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "One ScrubbingTextWatcher per TextView please. Use ScrubbingTextWatcher#addScrubber() to combine additional scrubbers."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 59
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/squareup/marketfont/MarketAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public synthetic lambda$postRestartInput$0$SelectableAutoCompleteEditText()V
    .locals 0

    .line 90
    invoke-static {p0}, Lcom/squareup/util/Views;->restartInput(Landroid/view/View;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .line 47
    invoke-super {p0}, Lcom/squareup/marketfont/MarketAutoCompleteTextView;->onAttachedToWindow()V

    .line 48
    invoke-virtual {p0}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->dismissDropDown()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 42
    invoke-super {p0}, Lcom/squareup/marketfont/MarketAutoCompleteTextView;->onFinishInflate()V

    const v0, 0x106000b

    .line 43
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->setDropDownBackgroundResource(I)V

    return-void
.end method

.method protected onSelectionChanged(II)V
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->selectionWatchers:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 78
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/text/SelectionWatcher;

    .line 79
    invoke-interface {v1, p1, p2}, Lcom/squareup/text/SelectionWatcher;->onSelectionChanged(II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public postRestartInput()V
    .locals 1

    .line 90
    new-instance v0, Lcom/squareup/widgets/-$$Lambda$SelectableAutoCompleteEditText$l9oB_FuadbOD4WMVnpxxbk9C7CE;

    invoke-direct {v0, p0}, Lcom/squareup/widgets/-$$Lambda$SelectableAutoCompleteEditText$l9oB_FuadbOD4WMVnpxxbk9C7CE;-><init>(Lcom/squareup/widgets/SelectableAutoCompleteEditText;)V

    invoke-virtual {p0, v0}, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public removeSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->selectionWatchers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public removeTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .line 63
    invoke-super {p0, p1}, Lcom/squareup/marketfont/MarketAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->scrubbingTextWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    if-ne v0, p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/squareup/widgets/SelectableAutoCompleteEditText;->scrubbingTextWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    :cond_0
    return-void
.end method
