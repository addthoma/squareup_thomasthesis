.class public final Lcom/squareup/widgets/JaggedLineView;
.super Landroid/view/View;
.source "JaggedLineView.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nJaggedLineView.kt\nKotlin\n*S Kotlin\n*F\n+ 1 JaggedLineView.kt\ncom/squareup/widgets/JaggedLineView\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,127:1\n1360#2:128\n1429#2,3:129\n1642#2,2:132\n*E\n*S KotlinDebug\n*F\n+ 1 JaggedLineView.kt\ncom/squareup/widgets/JaggedLineView\n*L\n74#1:128\n74#1,3:129\n76#1,2:132\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001b\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0014R\u001e\u0010\u0007\u001a\u00020\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\t\u0010\n\"\u0004\u0008\u000b\u0010\u000cR\u001e\u0010\r\u001a\u00020\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000e\u0010\n\"\u0004\u0008\u000f\u0010\u000cR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0014\u001a\u00020\u0008X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0015\u0010\n\"\u0004\u0008\u0016\u0010\u000c\u00a8\u0006\u001b"
    }
    d2 = {
        "Lcom/squareup/widgets/JaggedLineView;",
        "Landroid/view/View;",
        "context",
        "Landroid/content/Context;",
        "attrs",
        "Landroid/util/AttributeSet;",
        "(Landroid/content/Context;Landroid/util/AttributeSet;)V",
        "borderColor",
        "",
        "getBorderColor",
        "()I",
        "setBorderColor",
        "(I)V",
        "fillColor",
        "getFillColor",
        "setFillColor",
        "paint",
        "Landroid/graphics/Paint;",
        "path",
        "Landroid/graphics/Path;",
        "triangleCount",
        "getTriangleCount",
        "setTriangleCount",
        "onDraw",
        "",
        "canvas",
        "Landroid/graphics/Canvas;",
        "widgets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private borderColor:I

.field private fillColor:I

.field private final paint:Landroid/graphics/Paint;

.field private final path:Landroid/graphics/Path;

.field private triangleCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1, v0}, Lcom/squareup/widgets/JaggedLineView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/squareup/widgets/JaggedLineView;->path:Landroid/graphics/Path;

    .line 47
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/widgets/JaggedLineView;->paint:Landroid/graphics/Paint;

    const/4 v0, -0x1

    .line 48
    iput v0, p0, Lcom/squareup/widgets/JaggedLineView;->fillColor:I

    .line 49
    iput v0, p0, Lcom/squareup/widgets/JaggedLineView;->borderColor:I

    const/16 v0, 0xa

    .line 50
    iput v0, p0, Lcom/squareup/widgets/JaggedLineView;->triangleCount:I

    .line 53
    sget-object v0, Lcom/squareup/widgets/R$styleable;->JaggedLineView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 54
    sget p2, Lcom/squareup/widgets/R$styleable;->JaggedLineView_fillColor:I

    iget v0, p0, Lcom/squareup/widgets/JaggedLineView;->fillColor:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/JaggedLineView;->fillColor:I

    .line 55
    sget p2, Lcom/squareup/widgets/R$styleable;->JaggedLineView_borderColor:I

    iget v0, p0, Lcom/squareup/widgets/JaggedLineView;->borderColor:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/JaggedLineView;->borderColor:I

    .line 56
    sget p2, Lcom/squareup/widgets/R$styleable;->JaggedLineView_triangleCount:I

    iget v0, p0, Lcom/squareup/widgets/JaggedLineView;->triangleCount:I

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/JaggedLineView;->triangleCount:I

    .line 57
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    .line 43
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/JaggedLineView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public final getBorderColor()I
    .locals 1

    .line 49
    iget v0, p0, Lcom/squareup/widgets/JaggedLineView;->borderColor:I

    return v0
.end method

.method public final getFillColor()I
    .locals 1

    .line 48
    iget v0, p0, Lcom/squareup/widgets/JaggedLineView;->fillColor:I

    return v0
.end method

.method public final getTriangleCount()I
    .locals 1

    .line 50
    iget v0, p0, Lcom/squareup/widgets/JaggedLineView;->triangleCount:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    const-string v0, "canvas"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 62
    invoke-virtual {p0}, Lcom/squareup/widgets/JaggedLineView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/squareup/widgets/JaggedLineView;->triangleCount:I

    div-int/2addr v0, v1

    .line 65
    invoke-virtual {p0}, Lcom/squareup/widgets/JaggedLineView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 66
    invoke-virtual {p0}, Lcom/squareup/widgets/JaggedLineView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    .line 69
    iget-object v3, p0, Lcom/squareup/widgets/JaggedLineView;->path:Landroid/graphics/Path;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v4}, Landroid/graphics/Path;->moveTo(FF)V

    .line 70
    iget-object v3, p0, Lcom/squareup/widgets/JaggedLineView;->path:Landroid/graphics/Path;

    invoke-virtual {v3, v4, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 74
    new-instance v3, Lkotlin/ranges/IntRange;

    invoke-virtual {p0}, Lcom/squareup/widgets/JaggedLineView;->getWidth()I

    move-result v5

    add-int/2addr v5, v0

    const/4 v6, 0x0

    invoke-direct {v3, v6, v5}, Lkotlin/ranges/IntRange;-><init>(II)V

    check-cast v3, Lkotlin/ranges/IntProgression;

    invoke-static {v3, v0}, Lkotlin/ranges/RangesKt;->step(Lkotlin/ranges/IntProgression;I)Lkotlin/ranges/IntProgression;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 128
    new-instance v5, Ljava/util/ArrayList;

    const/16 v6, 0xa

    invoke-static {v3, v6}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v5, Ljava/util/Collection;

    .line 129
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    move-object v6, v3

    check-cast v6, Lkotlin/collections/IntIterator;

    invoke-virtual {v6}, Lkotlin/collections/IntIterator;->nextInt()I

    move-result v6

    int-to-float v6, v6

    .line 74
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 131
    :cond_0
    check-cast v5, Ljava/util/List;

    check-cast v5, Ljava/lang/Iterable;

    .line 75
    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->zipWithNext(Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object v3

    check-cast v3, Ljava/lang/Iterable;

    .line 132
    invoke-interface {v3}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lkotlin/Pair;

    invoke-virtual {v5}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Number;

    invoke-virtual {v6}, Ljava/lang/Number;->floatValue()F

    move-result v6

    invoke-virtual {v5}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Number;

    invoke-virtual {v5}, Ljava/lang/Number;->floatValue()F

    move-result v5

    .line 79
    iget-object v7, p0, Lcom/squareup/widgets/JaggedLineView;->path:Landroid/graphics/Path;

    add-float/2addr v6, v5

    const/4 v8, 0x2

    int-to-float v8, v8

    div-float/2addr v6, v8

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v6, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 80
    iget-object v6, p0, Lcom/squareup/widgets/JaggedLineView;->path:Landroid/graphics/Path;

    invoke-virtual {v6, v5, v1}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_1

    .line 85
    :cond_1
    iget v3, p0, Lcom/squareup/widgets/JaggedLineView;->triangleCount:I

    mul-int v3, v3, v0

    int-to-float v3, v3

    int-to-float v0, v0

    add-float/2addr v0, v2

    sub-float v5, v4, v1

    sub-float/2addr v0, v3

    div-float/2addr v5, v0

    mul-float v3, v3, v5

    sub-float/2addr v3, v1

    const/4 v0, -0x1

    int-to-float v0, v0

    mul-float v3, v3, v0

    .line 97
    invoke-virtual {p0}, Lcom/squareup/widgets/JaggedLineView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float v5, v5, v0

    add-float/2addr v5, v3

    .line 101
    iget-object v0, p0, Lcom/squareup/widgets/JaggedLineView;->path:Landroid/graphics/Path;

    const/4 v1, 0x1

    int-to-float v1, v1

    sub-float v9, v2, v1

    invoke-virtual {v0, v9, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 102
    iget-object v0, p0, Lcom/squareup/widgets/JaggedLineView;->path:Landroid/graphics/Path;

    invoke-virtual {v0, v9, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 105
    iget-object v0, p0, Lcom/squareup/widgets/JaggedLineView;->path:Landroid/graphics/Path;

    invoke-virtual {v0, v4, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 108
    iget-object v0, p0, Lcom/squareup/widgets/JaggedLineView;->path:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/widgets/JaggedLineView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/widgets/JaggedLineView;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/squareup/widgets/JaggedLineView;->fillColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 111
    iget-object v0, p0, Lcom/squareup/widgets/JaggedLineView;->path:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/squareup/widgets/JaggedLineView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/widgets/JaggedLineView;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/squareup/widgets/JaggedLineView;->borderColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 115
    iget-object v0, p0, Lcom/squareup/widgets/JaggedLineView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 116
    iget-object v0, p0, Lcom/squareup/widgets/JaggedLineView;->path:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/squareup/widgets/JaggedLineView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/widgets/JaggedLineView;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/squareup/widgets/JaggedLineView;->fillColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v10, 0x0

    .line 121
    iget-object v11, p0, Lcom/squareup/widgets/JaggedLineView;->paint:Landroid/graphics/Paint;

    move-object v6, p1

    invoke-virtual/range {v6 .. v11}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 124
    iget-object p1, p0, Lcom/squareup/widgets/JaggedLineView;->path:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    return-void
.end method

.method public final setBorderColor(I)V
    .locals 0

    .line 49
    iput p1, p0, Lcom/squareup/widgets/JaggedLineView;->borderColor:I

    return-void
.end method

.method public final setFillColor(I)V
    .locals 0

    .line 48
    iput p1, p0, Lcom/squareup/widgets/JaggedLineView;->fillColor:I

    return-void
.end method

.method public final setTriangleCount(I)V
    .locals 0

    .line 50
    iput p1, p0, Lcom/squareup/widgets/JaggedLineView;->triangleCount:I

    return-void
.end method
