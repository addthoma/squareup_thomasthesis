.class public final enum Lcom/squareup/url/UrlType;
.super Ljava/lang/Enum;
.source "UrlType.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/url/UrlType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\u0005\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/url/UrlType;",
        "",
        "(Ljava/lang/String;I)V",
        "RECURRING_SERIES",
        "SINGLE_INVOICE",
        "ESTIMATE",
        "invoices-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/url/UrlType;

.field public static final enum ESTIMATE:Lcom/squareup/url/UrlType;

.field public static final enum RECURRING_SERIES:Lcom/squareup/url/UrlType;

.field public static final enum SINGLE_INVOICE:Lcom/squareup/url/UrlType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/url/UrlType;

    new-instance v1, Lcom/squareup/url/UrlType;

    const/4 v2, 0x0

    const-string v3, "RECURRING_SERIES"

    invoke-direct {v1, v3, v2}, Lcom/squareup/url/UrlType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/url/UrlType;->RECURRING_SERIES:Lcom/squareup/url/UrlType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/url/UrlType;

    const/4 v2, 0x1

    const-string v3, "SINGLE_INVOICE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/url/UrlType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/url/UrlType;->SINGLE_INVOICE:Lcom/squareup/url/UrlType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/url/UrlType;

    const/4 v2, 0x2

    const-string v3, "ESTIMATE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/url/UrlType;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/url/UrlType;->ESTIMATE:Lcom/squareup/url/UrlType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/url/UrlType;->$VALUES:[Lcom/squareup/url/UrlType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/url/UrlType;
    .locals 1

    const-class v0, Lcom/squareup/url/UrlType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/url/UrlType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/url/UrlType;
    .locals 1

    sget-object v0, Lcom/squareup/url/UrlType;->$VALUES:[Lcom/squareup/url/UrlType;

    invoke-virtual {v0}, [Lcom/squareup/url/UrlType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/url/UrlType;

    return-object v0
.end method
