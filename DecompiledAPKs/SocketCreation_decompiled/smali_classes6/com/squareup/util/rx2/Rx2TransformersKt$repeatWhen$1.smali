.class final Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$1;
.super Ljava/lang/Object;
.source "Rx2Transformers.kt"

# interfaces
.implements Lio/reactivex/ObservableTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2TransformersKt;->repeatWhen(Lio/reactivex/Observable;JJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/ObservableTransformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Upstream:",
        "Ljava/lang/Object;",
        "Downstream:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableTransformer<",
        "TT;TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u0001H\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00042\u0014\u0010\u0005\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u0001H\u0002H\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "T",
        "kotlin.jvm.PlatformType",
        "",
        "it",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $maxIdleTime:J

.field final synthetic $minIdleTime:J

.field final synthetic $refreshRequested:Lio/reactivex/Observable;

.field final synthetic $scheduler:Lio/reactivex/Scheduler;

.field final synthetic $timeUnit:Ljava/util/concurrent/TimeUnit;


# direct methods
.method constructor <init>(Lio/reactivex/Observable;JJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$1;->$refreshRequested:Lio/reactivex/Observable;

    iput-wide p2, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$1;->$minIdleTime:J

    iput-wide p4, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$1;->$maxIdleTime:J

    iput-object p6, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$1;->$timeUnit:Ljava/util/concurrent/TimeUnit;

    iput-object p7, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$1;->$scheduler:Lio/reactivex/Scheduler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "TT;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 211
    iget-object v2, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$1;->$refreshRequested:Lio/reactivex/Observable;

    iget-wide v3, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$1;->$minIdleTime:J

    iget-wide v5, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$1;->$maxIdleTime:J

    iget-object v7, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$1;->$timeUnit:Ljava/util/concurrent/TimeUnit;

    iget-object v8, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$1;->$scheduler:Lio/reactivex/Scheduler;

    move-object v1, p1

    invoke-static/range {v1 .. v8}, Lcom/squareup/util/rx2/Rx2TransformersKt;->repeatWhen(Lio/reactivex/Observable;Lio/reactivex/Observable;JJLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/squareup/util/rx2/Rx2TransformersKt$repeatWhen$1;->apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    check-cast p1, Lio/reactivex/ObservableSource;

    return-object p1
.end method
