.class final Lcom/squareup/util/rx2/Rx2Files$watch$1;
.super Ljava/lang/Object;
.source "Rx2Files.kt"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2Files;->watch(Ljava/io/File;I)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "Ljava/io/File;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $file:Ljava/io/File;

.field final synthetic $mask:I


# direct methods
.method constructor <init>(Ljava/io/File;I)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2Files$watch$1;->$file:Ljava/io/File;

    iput p2, p0, Lcom/squareup/util/rx2/Rx2Files$watch$1;->$mask:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    new-instance v0, Lcom/squareup/util/rx2/Rx2Files$watch$1$fileObserver$1;

    iget-object v1, p0, Lcom/squareup/util/rx2/Rx2Files$watch$1;->$file:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/squareup/util/rx2/Rx2Files$watch$1;->$mask:I

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/squareup/util/rx2/Rx2Files$watch$1$fileObserver$1;-><init>(Lcom/squareup/util/rx2/Rx2Files$watch$1;Lio/reactivex/ObservableEmitter;Ljava/lang/String;I)V

    .line 31
    new-instance v1, Lcom/squareup/util/rx2/Rx2Files$watch$1$1;

    invoke-direct {v1, v0}, Lcom/squareup/util/rx2/Rx2Files$watch$1$1;-><init>(Lcom/squareup/util/rx2/Rx2Files$watch$1$fileObserver$1;)V

    check-cast v1, Lio/reactivex/functions/Cancellable;

    invoke-interface {p1, v1}, Lio/reactivex/ObservableEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    .line 32
    invoke-virtual {v0}, Lcom/squareup/util/rx2/Rx2Files$watch$1$fileObserver$1;->startWatching()V

    return-void
.end method
