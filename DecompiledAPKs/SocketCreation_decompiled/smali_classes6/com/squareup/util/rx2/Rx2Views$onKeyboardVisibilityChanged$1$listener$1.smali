.class public final Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$listener$1;
.super Ljava/lang/Object;
.source "Rx2Views.kt"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;->subscribe(Lio/reactivex/ObservableEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0004\u001a\u00020\u0005H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$listener$1",
        "Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;",
        "displayFrame",
        "Landroid/graphics/Rect;",
        "onGlobalLayout",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $emitter:Lio/reactivex/ObservableEmitter;

.field private final displayFrame:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;


# direct methods
.method constructor <init>(Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;Lio/reactivex/ObservableEmitter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter;",
            ")V"
        }
    .end annotation

    .line 103
    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$listener$1;->this$0:Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;

    iput-object p2, p0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$listener$1;->$emitter:Lio/reactivex/ObservableEmitter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$listener$1;->displayFrame:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .line 107
    iget-object v0, p0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$listener$1;->this$0:Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;

    iget-object v0, v0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;->$this_onKeyboardVisibilityChanged:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$listener$1;->displayFrame:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$listener$1;->this$0:Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;

    iget-object v0, v0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;->$this_onKeyboardVisibilityChanged:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    const-string v1, "rootView"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v2, p0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$listener$1;->displayFrame:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    iget-object v2, p0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$listener$1;->this$0:Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;

    iget-object v2, v2, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1;->$this_onKeyboardVisibilityChanged:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3e19999a    # 0.15f

    mul-float v1, v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 110
    :goto_0
    iget-object v1, p0, Lcom/squareup/util/rx2/Rx2Views$onKeyboardVisibilityChanged$1$listener$1;->$emitter:Lio/reactivex/ObservableEmitter;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v1, v0}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    return-void
.end method
