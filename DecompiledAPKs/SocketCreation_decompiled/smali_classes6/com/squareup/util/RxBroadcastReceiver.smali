.class public final Lcom/squareup/util/RxBroadcastReceiver;
.super Ljava/lang/Object;
.source "RxBroadcastReceiver.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a+\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00032\u0012\u0010\u0004\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00060\u0005\"\u00020\u0006H\u0007\u00a2\u0006\u0002\u0010\u0007\u001a3\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00032\u0006\u0010\u0008\u001a\u00020\t2\u0012\u0010\u0004\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00060\u0005\"\u00020\u0006H\u0007\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "registerForIntents",
        "Lrx/Observable;",
        "Landroid/content/Intent;",
        "Landroid/content/Context;",
        "actions",
        "",
        "",
        "(Landroid/content/Context;[Ljava/lang/String;)Lrx/Observable;",
        "backpressure",
        "Lrx/Emitter$BackpressureMode;",
        "(Landroid/content/Context;Lrx/Emitter$BackpressureMode;[Ljava/lang/String;)Lrx/Observable;",
        "android-hairball_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final varargs registerForIntents(Landroid/content/Context;Lrx/Emitter$BackpressureMode;[Ljava/lang/String;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lrx/Emitter$BackpressureMode;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use registerForIntents2(actions) instead."
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "registerForIntents2(actions)"
            imports = {}
        .end subannotation
    .end annotation

    const-string v0, "$this$registerForIntents"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "backpressure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    new-instance v0, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/util/RxBroadcastReceiver$registerForIntents$1;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    check-cast v0, Lrx/functions/Action1;

    invoke-static {v0, p1}, Lrx/Observable;->create(Lrx/functions/Action1;Lrx/Emitter$BackpressureMode;)Lrx/Observable;

    move-result-object p0

    const-string p1, "Observable.create({ emit\u2026ions))\n  }, backpressure)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final varargs registerForIntents(Landroid/content/Context;[Ljava/lang/String;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Lrx/Observable<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/Deprecated;
        message = "Use registerForIntents2(actions) instead."
        replaceWith = .subannotation Lkotlin/ReplaceWith;
            expression = "registerForIntents2(actions)"
            imports = {}
        .end subannotation
    .end annotation

    const-string v0, "$this$registerForIntents"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actions"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    sget-object v0, Lrx/Emitter$BackpressureMode;->ERROR:Lrx/Emitter$BackpressureMode;

    array-length v1, p1

    invoke-static {p1, v1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    invoke-static {p0, v0, p1}, Lcom/squareup/util/RxBroadcastReceiver;->registerForIntents(Landroid/content/Context;Lrx/Emitter$BackpressureMode;[Ljava/lang/String;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method
