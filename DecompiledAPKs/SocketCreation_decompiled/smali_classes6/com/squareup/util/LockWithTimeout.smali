.class public Lcom/squareup/util/LockWithTimeout;
.super Ljava/lang/Object;
.source "LockWithTimeout.java"


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field volatile locked:Z

.field volatile lockedAtMs:J

.field private final timeout:J


# direct methods
.method public constructor <init>(Lcom/squareup/util/Clock;J)V
    .locals 2

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/squareup/util/LockWithTimeout;->locked:Z

    const-wide/16 v0, -0x1

    .line 6
    iput-wide v0, p0, Lcom/squareup/util/LockWithTimeout;->lockedAtMs:J

    .line 12
    iput-object p1, p0, Lcom/squareup/util/LockWithTimeout;->clock:Lcom/squareup/util/Clock;

    .line 13
    iput-wide p2, p0, Lcom/squareup/util/LockWithTimeout;->timeout:J

    return-void
.end method


# virtual methods
.method public declared-synchronized tryLock()Z
    .locals 7

    monitor-enter p0

    .line 17
    :try_start_0
    iget-object v0, p0, Lcom/squareup/util/LockWithTimeout;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    .line 18
    iget-wide v2, p0, Lcom/squareup/util/LockWithTimeout;->lockedAtMs:J

    sub-long v2, v0, v2

    .line 20
    iget-boolean v4, p0, Lcom/squareup/util/LockWithTimeout;->locked:Z

    if-eqz v4, :cond_0

    iget-wide v4, p0, Lcom/squareup/util/LockWithTimeout;->timeout:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v6, v2, v4

    if-gez v6, :cond_0

    const/4 v0, 0x0

    .line 21
    monitor-exit p0

    return v0

    :cond_0
    const/4 v2, 0x1

    .line 23
    :try_start_1
    iput-boolean v2, p0, Lcom/squareup/util/LockWithTimeout;->locked:Z

    .line 24
    iput-wide v0, p0, Lcom/squareup/util/LockWithTimeout;->lockedAtMs:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 25
    monitor-exit p0

    return v2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized unlock()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    .line 30
    :try_start_0
    iput-boolean v0, p0, Lcom/squareup/util/LockWithTimeout;->locked:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
