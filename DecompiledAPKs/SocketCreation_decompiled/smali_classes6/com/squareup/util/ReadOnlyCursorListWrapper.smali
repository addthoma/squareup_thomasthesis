.class public abstract Lcom/squareup/util/ReadOnlyCursorListWrapper;
.super Ljava/lang/Object;
.source "ReadOnlyCursorListWrapper.java"

# interfaces
.implements Lcom/squareup/util/ReadOnlyCursorList;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/util/ReadOnlyCursorList<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final list:Lcom/squareup/util/ReadOnlyCursorList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/ReadOnlyCursorList<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/ReadOnlyCursorList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/ReadOnlyCursorList<",
            "TT;>;)V"
        }
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/util/ReadOnlyCursorListWrapper;->list:Lcom/squareup/util/ReadOnlyCursorList;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 33
    iget-object v0, p0, Lcom/squareup/util/ReadOnlyCursorListWrapper;->list:Lcom/squareup/util/ReadOnlyCursorList;

    invoke-interface {v0}, Lcom/squareup/util/ReadOnlyCursorList;->close()V

    return-void
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/util/ReadOnlyCursorListWrapper;->list:Lcom/squareup/util/ReadOnlyCursorList;

    invoke-interface {v0, p1}, Lcom/squareup/util/ReadOnlyCursorList;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public isClosed()Z
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/util/ReadOnlyCursorListWrapper;->list:Lcom/squareup/util/ReadOnlyCursorList;

    invoke-interface {v0}, Lcom/squareup/util/ReadOnlyCursorList;->isClosed()Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/util/ReadOnlyCursorListWrapper;->list:Lcom/squareup/util/ReadOnlyCursorList;

    invoke-interface {v0}, Lcom/squareup/util/ReadOnlyCursorList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TT;>;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/util/ReadOnlyCursorListWrapper;->list:Lcom/squareup/util/ReadOnlyCursorList;

    invoke-interface {v0}, Lcom/squareup/util/ReadOnlyCursorList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/util/ReadOnlyCursorListWrapper;->list:Lcom/squareup/util/ReadOnlyCursorList;

    invoke-interface {v0}, Lcom/squareup/util/ReadOnlyCursorList;->size()I

    move-result v0

    return v0
.end method
