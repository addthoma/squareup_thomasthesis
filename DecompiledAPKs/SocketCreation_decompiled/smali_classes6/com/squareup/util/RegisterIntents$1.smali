.class final Lcom/squareup/util/RegisterIntents$1;
.super Landroid/webkit/WebChromeClient;
.source "RegisterIntents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/RegisterIntents;->launchEmbeddedBrowser(Landroid/content/Context;Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$backButton:Lcom/squareup/glyph/SquareGlyphView;

.field final synthetic val$forwardButton:Lcom/squareup/glyph/SquareGlyphView;

.field final synthetic val$progressBar:Landroid/widget/ProgressBar;

.field final synthetic val$title:Landroid/widget/TextView;

.field final synthetic val$webView:Landroid/webkit/WebView;


# direct methods
.method constructor <init>(Landroid/widget/ProgressBar;Landroid/widget/TextView;Landroid/webkit/WebView;Lcom/squareup/glyph/SquareGlyphView;Lcom/squareup/glyph/SquareGlyphView;)V
    .locals 0

    .line 98
    iput-object p1, p0, Lcom/squareup/util/RegisterIntents$1;->val$progressBar:Landroid/widget/ProgressBar;

    iput-object p2, p0, Lcom/squareup/util/RegisterIntents$1;->val$title:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/squareup/util/RegisterIntents$1;->val$webView:Landroid/webkit/WebView;

    iput-object p4, p0, Lcom/squareup/util/RegisterIntents$1;->val$backButton:Lcom/squareup/glyph/SquareGlyphView;

    iput-object p5, p0, Lcom/squareup/util/RegisterIntents$1;->val$forwardButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 2

    .line 100
    iget-object v0, p0, Lcom/squareup/util/RegisterIntents$1;->val$progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 101
    iget-object v0, p0, Lcom/squareup/util/RegisterIntents$1;->val$progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getMax()I

    move-result v1

    if-eq p2, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 102
    invoke-super {p0, p1, p2}, Landroid/webkit/WebChromeClient;->onProgressChanged(Landroid/webkit/WebView;I)V

    .line 104
    iget-object p1, p0, Lcom/squareup/util/RegisterIntents$1;->val$title:Landroid/widget/TextView;

    iget-object p2, p0, Lcom/squareup/util/RegisterIntents$1;->val$webView:Landroid/webkit/WebView;

    invoke-virtual {p2}, Landroid/webkit/WebView;->getTitle()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/util/RegisterIntents$1;->val$backButton:Lcom/squareup/glyph/SquareGlyphView;

    iget-object p2, p0, Lcom/squareup/util/RegisterIntents$1;->val$webView:Landroid/webkit/WebView;

    invoke-virtual {p2}, Landroid/webkit/WebView;->canGoBack()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setEnabled(Z)V

    .line 106
    iget-object p1, p0, Lcom/squareup/util/RegisterIntents$1;->val$forwardButton:Lcom/squareup/glyph/SquareGlyphView;

    iget-object p2, p0, Lcom/squareup/util/RegisterIntents$1;->val$webView:Landroid/webkit/WebView;

    invoke-virtual {p2}, Landroid/webkit/WebView;->canGoForward()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setEnabled(Z)V

    return-void
.end method
