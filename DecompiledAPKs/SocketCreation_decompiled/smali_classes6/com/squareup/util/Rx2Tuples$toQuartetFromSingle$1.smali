.class final Lcom/squareup/util/Rx2Tuples$toQuartetFromSingle$1;
.super Ljava/lang/Object;
.source "Rx2Tuples.kt"

# interfaces
.implements Lio/reactivex/functions/BiFunction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/Rx2Tuples;->toQuartetFromSingle()Lio/reactivex/functions/BiFunction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/BiFunction<",
        "TD;",
        "Lcom/squareup/util/tuple/Triplet<",
        "TA;TB;TC;>;",
        "Lcom/squareup/util/tuple/Quartet<",
        "TD;TA;TB;TC;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u001a\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0006\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0006\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0002*\u00020\u00062\u0006\u0010\u0007\u001a\u0002H\u00022\u0018\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\tH\n\u00a2\u0006\u0004\u0008\n\u0010\u000b"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/tuple/Quartet;",
        "D",
        "A",
        "B",
        "C",
        "",
        "t",
        "<name for destructuring parameter 1>",
        "Lcom/squareup/util/tuple/Triplet;",
        "apply",
        "(Ljava/lang/Object;Lcom/squareup/util/tuple/Triplet;)Lcom/squareup/util/tuple/Quartet;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/Rx2Tuples$toQuartetFromSingle$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/util/Rx2Tuples$toQuartetFromSingle$1;

    invoke-direct {v0}, Lcom/squareup/util/Rx2Tuples$toQuartetFromSingle$1;-><init>()V

    sput-object v0, Lcom/squareup/util/Rx2Tuples$toQuartetFromSingle$1;->INSTANCE:Lcom/squareup/util/Rx2Tuples$toQuartetFromSingle$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;Lcom/squareup/util/tuple/Triplet;)Lcom/squareup/util/tuple/Quartet;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;",
            "Lcom/squareup/util/tuple/Triplet<",
            "TA;TB;TC;>;)",
            "Lcom/squareup/util/tuple/Quartet<",
            "TD;TA;TB;TC;>;"
        }
    .end annotation

    const-string v0, "t"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "<name for destructuring parameter 1>"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/squareup/util/tuple/Triplet;->component1()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/util/tuple/Triplet;->component2()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/util/tuple/Triplet;->component3()Ljava/lang/Object;

    move-result-object p2

    .line 88
    new-instance v2, Lcom/squareup/util/tuple/Quartet;

    invoke-direct {v2, p1, v0, v1, p2}, Lcom/squareup/util/tuple/Quartet;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method public bridge synthetic apply(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 20
    check-cast p2, Lcom/squareup/util/tuple/Triplet;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/util/Rx2Tuples$toQuartetFromSingle$1;->apply(Ljava/lang/Object;Lcom/squareup/util/tuple/Triplet;)Lcom/squareup/util/tuple/Quartet;

    move-result-object p1

    return-object p1
.end method
