.class public final Lcom/squareup/util/AndroidModule_ProvideProcessUserHandleFactory;
.super Ljava/lang/Object;
.source "AndroidModule_ProvideProcessUserHandleFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/AndroidModule_ProvideProcessUserHandleFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Landroid/os/UserHandle;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/util/AndroidModule_ProvideProcessUserHandleFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/util/AndroidModule_ProvideProcessUserHandleFactory$InstanceHolder;->access$000()Lcom/squareup/util/AndroidModule_ProvideProcessUserHandleFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideProcessUserHandle()Landroid/os/UserHandle;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/util/AndroidModule;->provideProcessUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserHandle;

    return-object v0
.end method


# virtual methods
.method public get()Landroid/os/UserHandle;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/util/AndroidModule_ProvideProcessUserHandleFactory;->provideProcessUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/util/AndroidModule_ProvideProcessUserHandleFactory;->get()Landroid/os/UserHandle;

    move-result-object v0

    return-object v0
.end method
