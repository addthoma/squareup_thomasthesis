.class public final Lcom/squareup/util/RxContentUriSingleItemAccessor;
.super Ljava/lang/Object;
.source "RxContentUriSingleItemAccessor.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxContentUriSingleItemAccessor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxContentUriSingleItemAccessor.kt\ncom/squareup/util/RxContentUriSingleItemAccessor\n*L\n1#1,110:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0018\u0000*\u0004\u0008\u0000\u0010\u00012\u00020\u0002B#\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0008\u00a2\u0006\u0002\u0010\tJ\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u000eJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0010H\u0002J\u0008\u0010\u0012\u001a\u00020\u0013H\u0002J\u0013\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\u0017R\u0016\u0010\n\u001a\n \u000c*\u0004\u0018\u00010\u000b0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/util/RxContentUriSingleItemAccessor;",
        "T",
        "",
        "context",
        "Landroid/content/Context;",
        "contentUri",
        "Landroid/net/Uri;",
        "strategy",
        "Lcom/squareup/util/AccessorStrategy;",
        "(Landroid/content/Context;Landroid/net/Uri;Lcom/squareup/util/AccessorStrategy;)V",
        "contentResolver",
        "Landroid/content/ContentResolver;",
        "kotlin.jvm.PlatformType",
        "get",
        "Lio/reactivex/Observable;",
        "getErrorMsg",
        "",
        "start",
        "requireCursor",
        "Landroid/database/Cursor;",
        "set",
        "",
        "value",
        "(Ljava/lang/Object;)V",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final contentResolver:Landroid/content/ContentResolver;

.field private final contentUri:Landroid/net/Uri;

.field private final strategy:Lcom/squareup/util/AccessorStrategy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/util/AccessorStrategy<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Lcom/squareup/util/AccessorStrategy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Lcom/squareup/util/AccessorStrategy<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contentUri"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "strategy"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor;->contentUri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor;->strategy:Lcom/squareup/util/AccessorStrategy;

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor;->contentResolver:Landroid/content/ContentResolver;

    return-void
.end method

.method public static final synthetic access$getErrorMsg(Lcom/squareup/util/RxContentUriSingleItemAccessor;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 33
    invoke-direct {p0, p1}, Lcom/squareup/util/RxContentUriSingleItemAccessor;->getErrorMsg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getStrategy$p(Lcom/squareup/util/RxContentUriSingleItemAccessor;)Lcom/squareup/util/AccessorStrategy;
    .locals 0

    .line 33
    iget-object p0, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor;->strategy:Lcom/squareup/util/AccessorStrategy;

    return-object p0
.end method

.method public static final synthetic access$requireCursor(Lcom/squareup/util/RxContentUriSingleItemAccessor;)Landroid/database/Cursor;
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/util/RxContentUriSingleItemAccessor;->requireCursor()Landroid/database/Cursor;

    move-result-object p0

    return-object p0
.end method

.method private final getErrorMsg(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " when querying "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor;->contentUri:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final requireCursor()Landroid/database/Cursor;
    .locals 2

    .line 63
    iget-object v0, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor;->contentResolver:Landroid/content/ContentResolver;

    const-string v1, "contentResolver"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor;->contentUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/squareup/util/ContentResolversKt;->query(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const-string v0, "received null cursor"

    .line 64
    invoke-direct {p0, v0}, Lcom/squareup/util/RxContentUriSingleItemAccessor;->getErrorMsg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 63
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method


# virtual methods
.method public final get()Lio/reactivex/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    .line 41
    iget-object v0, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor;->contentResolver:Landroid/content/ContentResolver;

    const-string v1, "contentResolver"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor;->contentUri:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/squareup/util/RxContentObserverKt;->listenForChanges$default(Landroid/content/ContentResolver;Landroid/net/Uri;ZLrx/Emitter$BackpressureMode;ILjava/lang/Object;)Lrx/Observable;

    move-result-object v0

    .line 42
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 43
    new-instance v1, Lcom/squareup/util/RxContentUriSingleItemAccessor$get$1;

    invoke-direct {v1, p0}, Lcom/squareup/util/RxContentUriSingleItemAccessor$get$1;-><init>(Lcom/squareup/util/RxContentUriSingleItemAccessor;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "contentResolver.listenFo\u2026 0)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final set(Ljava/lang/Object;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor;->contentResolver:Landroid/content/ContentResolver;

    const-string v1, "contentResolver"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    iget-object v1, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor;->contentUri:Landroid/net/Uri;

    .line 56
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 57
    iget-object v3, p0, Lcom/squareup/util/RxContentUriSingleItemAccessor;->strategy:Lcom/squareup/util/AccessorStrategy;

    const-string v4, "key"

    invoke-interface {v3, v2, v4, p1}, Lcom/squareup/util/AccessorStrategy;->putValueInValues(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Object;)V

    .line 54
    invoke-static {v0, v1, v2}, Lcom/squareup/util/ContentResolversKt;->update(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)I

    return-void
.end method
