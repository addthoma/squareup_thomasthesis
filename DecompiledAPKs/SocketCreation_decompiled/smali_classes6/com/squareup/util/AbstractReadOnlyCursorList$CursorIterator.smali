.class Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;
.super Ljava/lang/Object;
.source "AbstractReadOnlyCursorList.java"

# interfaces
.implements Ljava/util/ListIterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/AbstractReadOnlyCursorList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CursorIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/ListIterator<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private position:I

.field final synthetic this$0:Lcom/squareup/util/AbstractReadOnlyCursorList;


# direct methods
.method private constructor <init>(Lcom/squareup/util/AbstractReadOnlyCursorList;)V
    .locals 0

    .line 50
    iput-object p1, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->this$0:Lcom/squareup/util/AbstractReadOnlyCursorList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, -0x1

    .line 51
    iput p1, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->position:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/util/AbstractReadOnlyCursorList;Lcom/squareup/util/AbstractReadOnlyCursorList$1;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;-><init>(Lcom/squareup/util/AbstractReadOnlyCursorList;)V

    return-void
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 54
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public hasNext()Z
    .locals 3

    .line 58
    iget-object v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->this$0:Lcom/squareup/util/AbstractReadOnlyCursorList;

    invoke-virtual {v0}, Lcom/squareup/util/AbstractReadOnlyCursorList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->position:I

    iget-object v2, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->this$0:Lcom/squareup/util/AbstractReadOnlyCursorList;

    invoke-virtual {v2}, Lcom/squareup/util/AbstractReadOnlyCursorList;->size()I

    move-result v2

    sub-int/2addr v2, v1

    if-ge v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hasPrevious()Z
    .locals 2

    .line 62
    iget-object v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->this$0:Lcom/squareup/util/AbstractReadOnlyCursorList;

    invoke-virtual {v0}, Lcom/squareup/util/AbstractReadOnlyCursorList;->isEmpty()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->position:I

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public next()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 66
    iget-object v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->this$0:Lcom/squareup/util/AbstractReadOnlyCursorList;

    invoke-virtual {v0}, Lcom/squareup/util/AbstractReadOnlyCursorList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->position:I

    iget-object v1, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->this$0:Lcom/squareup/util/AbstractReadOnlyCursorList;

    invoke-virtual {v1}, Lcom/squareup/util/AbstractReadOnlyCursorList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_0

    .line 69
    iget v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->position:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->position:I

    .line 70
    iget-object v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->this$0:Lcom/squareup/util/AbstractReadOnlyCursorList;

    iget v1, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->position:I

    invoke-virtual {v0, v1}, Lcom/squareup/util/AbstractReadOnlyCursorList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 67
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public nextIndex()I
    .locals 1

    .line 74
    iget v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->position:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public previous()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->this$0:Lcom/squareup/util/AbstractReadOnlyCursorList;

    invoke-virtual {v0}, Lcom/squareup/util/AbstractReadOnlyCursorList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->position:I

    if-eqz v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    .line 81
    iput v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->position:I

    .line 82
    iget-object v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->this$0:Lcom/squareup/util/AbstractReadOnlyCursorList;

    iget v1, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->position:I

    invoke-virtual {v0, v1}, Lcom/squareup/util/AbstractReadOnlyCursorList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 79
    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public previousIndex()I
    .locals 1

    .line 86
    iget v0, p0, Lcom/squareup/util/AbstractReadOnlyCursorList$CursorIterator;->position:I

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public remove()V
    .locals 1

    .line 90
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public set(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 94
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method
