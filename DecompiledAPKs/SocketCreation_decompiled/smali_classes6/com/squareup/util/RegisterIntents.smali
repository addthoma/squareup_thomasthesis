.class public Lcom/squareup/util/RegisterIntents;
.super Ljava/lang/Object;
.source "RegisterIntents.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static internalLaunchBrowser(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .line 44
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 46
    invoke-static {}, Lcom/squareup/util/X2Build;->isSquareDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    invoke-static {p0, p1}, Lcom/squareup/util/RegisterIntents;->launchEmbeddedBrowser(Landroid/content/Context;Landroid/net/Uri;)V

    return-void

    .line 51
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const/high16 v1, 0x10000000

    .line 52
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 53
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.android.browser.application_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    invoke-static {v0, p0}, Lcom/squareup/util/Intents;->isIntentAvailable(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    .line 57
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object p1

    const/high16 v1, 0x10000

    .line 58
    invoke-virtual {p1, v0, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object p1

    iget-object p1, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object p1, p1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    const-string v1, "com.htc.HtcLinkifyDispatcher"

    .line 59
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 61
    sget p1, Lcom/squareup/linkutilities/impl/pos/R$string;->browser_chooser_title:I

    .line 63
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 62
    invoke-static {v0, p1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object p1

    .line 64
    invoke-virtual {p0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 66
    :cond_1
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 68
    :goto_0
    instance-of p1, p0, Landroid/app/Activity;

    if-eqz p1, :cond_3

    .line 70
    check-cast p0, Landroid/app/Activity;

    invoke-virtual {p0, v2, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto :goto_1

    .line 75
    :cond_2
    sget v0, Lcom/squareup/linkutilities/impl/pos/R$string;->please_visit_url:I

    .line 76
    invoke-static {p0, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 77
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 78
    invoke-static {p0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object p0

    invoke-virtual {p0}, Landroid/widget/Toast;->show()V

    :cond_3
    :goto_1
    return-void
.end method

.method public static launchBrowser(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 36
    invoke-static {p0, p1}, Lcom/squareup/util/RegisterIntents;->internalLaunchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private static launchEmbeddedBrowser(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 13

    .line 84
    new-instance v0, Landroid/app/Dialog;

    const v1, 0x1030009

    invoke-direct {v0, p0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 85
    sget v1, Lcom/squareup/linkutilities/impl/pos/R$layout;->browser:I

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    .line 87
    sget v1, Lcom/squareup/linkutilities/impl/pos/R$id;->webview:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    .line 88
    sget v2, Lcom/squareup/linkutilities/impl/pos/R$id;->web_close_button:I

    invoke-static {p0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/squareup/glyph/SquareGlyphView;

    .line 89
    sget v2, Lcom/squareup/linkutilities/impl/pos/R$id;->web_back_button:I

    invoke-static {p0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/squareup/glyph/SquareGlyphView;

    .line 90
    sget v2, Lcom/squareup/linkutilities/impl/pos/R$id;->web_forward_button:I

    invoke-static {p0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Lcom/squareup/glyph/SquareGlyphView;

    .line 91
    sget v2, Lcom/squareup/linkutilities/impl/pos/R$id;->web_reload_button:I

    invoke-static {p0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Lcom/squareup/glyph/SquareGlyphView;

    .line 92
    sget v2, Lcom/squareup/linkutilities/impl/pos/R$id;->title_text_view:I

    invoke-static {p0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Landroid/widget/TextView;

    .line 94
    sget v2, Lcom/squareup/linkutilities/impl/pos/R$id;->web_progress_bar:I

    invoke-static {p0, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Landroid/widget/ProgressBar;

    const/16 v2, 0x64

    .line 95
    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 97
    new-instance v2, Landroid/webkit/WebViewClient;

    invoke-direct {v2}, Landroid/webkit/WebViewClient;-><init>()V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 98
    new-instance v12, Lcom/squareup/util/RegisterIntents$1;

    move-object v2, v12

    move-object v5, v1

    move-object v6, v9

    move-object v7, v10

    invoke-direct/range {v2 .. v7}, Lcom/squareup/util/RegisterIntents$1;-><init>(Landroid/widget/ProgressBar;Landroid/widget/TextView;Landroid/webkit/WebView;Lcom/squareup/glyph/SquareGlyphView;Lcom/squareup/glyph/SquareGlyphView;)V

    invoke-virtual {v1, v12}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 109
    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    const/4 v2, 0x0

    .line 110
    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setVerticalScrollBarEnabled(Z)V

    .line 111
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 112
    new-instance p1, Lcom/squareup/util/RegisterIntents$2;

    invoke-direct {p1, v0}, Lcom/squareup/util/RegisterIntents$2;-><init>(Landroid/app/Dialog;)V

    invoke-virtual {v8, p1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    new-instance p1, Lcom/squareup/util/RegisterIntents$3;

    invoke-direct {p1, v1}, Lcom/squareup/util/RegisterIntents$3;-><init>(Landroid/webkit/WebView;)V

    invoke-virtual {v9, p1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    new-instance p1, Lcom/squareup/util/RegisterIntents$4;

    invoke-direct {p1, v1}, Lcom/squareup/util/RegisterIntents$4;-><init>(Landroid/webkit/WebView;)V

    invoke-virtual {v10, p1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    new-instance p1, Lcom/squareup/util/RegisterIntents$5;

    invoke-direct {p1, v1}, Lcom/squareup/util/RegisterIntents$5;-><init>(Landroid/webkit/WebView;)V

    invoke-virtual {v11, p1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 134
    invoke-virtual {v0, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 135
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method
