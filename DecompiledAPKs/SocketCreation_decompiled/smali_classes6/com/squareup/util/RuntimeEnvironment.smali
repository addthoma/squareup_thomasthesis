.class public final Lcom/squareup/util/RuntimeEnvironment;
.super Ljava/lang/Object;
.source "RuntimeEnvironment.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\r\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0005\u0010\u0002\u001a\u0004\u0008\u0003\u0010\u0006R\u001c\u0010\u0007\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u0008\u0010\u0002\u001a\u0004\u0008\u0007\u0010\u0006R\u001c\u0010\t\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\n\u0010\u0002\u001a\u0004\u0008\t\u0010\u0006R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u000c\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\r\u0010\u0002\u001a\u0004\u0008\u000c\u0010\u0006R\u001c\u0010\u000e\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u000e\n\u0000\u0012\u0004\u0008\u000f\u0010\u0002\u001a\u0004\u0008\u000e\u0010\u0006R\u000e\u0010\u0010\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/util/RuntimeEnvironment;",
        "",
        "()V",
        "isAndroid",
        "",
        "isAndroid$annotations",
        "()Z",
        "isAnyTest",
        "isAnyTest$annotations",
        "isInstrumentationTest",
        "isInstrumentationTest$annotations",
        "isJunitAvailable",
        "isJvm",
        "isJvm$annotations",
        "isJvmTest",
        "isJvmTest$annotations",
        "isRobolectricAvailable",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/RuntimeEnvironment;

.field private static final isAndroid:Z

.field private static final isAnyTest:Z

.field private static final isInstrumentationTest:Z

.field private static final isJunitAvailable:Z

.field private static final isJvm:Z

.field private static final isJvmTest:Z

.field private static final isRobolectricAvailable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 6
    new-instance v0, Lcom/squareup/util/RuntimeEnvironment;

    invoke-direct {v0}, Lcom/squareup/util/RuntimeEnvironment;-><init>()V

    sput-object v0, Lcom/squareup/util/RuntimeEnvironment;->INSTANCE:Lcom/squareup/util/RuntimeEnvironment;

    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_0
    const-string v2, "org.robolectric.RobolectricTestRunner"

    .line 8
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x1

    goto :goto_0

    :catch_0
    const/4 v2, 0x0

    .line 7
    :goto_0
    sput-boolean v2, Lcom/squareup/util/RuntimeEnvironment;->isRobolectricAvailable:Z

    :try_start_1
    const-string v2, "org.junit.Test"

    .line 15
    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v2, 0x1

    goto :goto_1

    :catch_1
    const/4 v2, 0x0

    .line 14
    :goto_1
    sput-boolean v2, Lcom/squareup/util/RuntimeEnvironment;->isJunitAvailable:Z

    .line 28
    sget-boolean v2, Lcom/squareup/util/RuntimeEnvironment;->isRobolectricAvailable:Z

    if-nez v2, :cond_1

    const-string v2, "java.vm.vendor"

    .line 27
    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "The Android Project"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "java.vm.name"

    .line 28
    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Dalvik"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    goto :goto_2

    :cond_1
    const/4 v2, 0x0

    :goto_2
    sput-boolean v2, Lcom/squareup/util/RuntimeEnvironment;->isAndroid:Z

    .line 31
    sget-boolean v2, Lcom/squareup/util/RuntimeEnvironment;->isAndroid:Z

    xor-int/2addr v2, v1

    sput-boolean v2, Lcom/squareup/util/RuntimeEnvironment;->isJvm:Z

    .line 34
    sget-boolean v2, Lcom/squareup/util/RuntimeEnvironment;->isJvm:Z

    if-eqz v2, :cond_2

    sget-boolean v2, Lcom/squareup/util/RuntimeEnvironment;->isJunitAvailable:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    goto :goto_3

    :cond_2
    const/4 v2, 0x0

    :goto_3
    sput-boolean v2, Lcom/squareup/util/RuntimeEnvironment;->isJvmTest:Z

    .line 36
    sget-boolean v2, Lcom/squareup/util/RuntimeEnvironment;->isAndroid:Z

    if-eqz v2, :cond_3

    sget-boolean v2, Lcom/squareup/util/RuntimeEnvironment;->isJunitAvailable:Z

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    goto :goto_4

    :cond_3
    const/4 v2, 0x0

    :goto_4
    sput-boolean v2, Lcom/squareup/util/RuntimeEnvironment;->isInstrumentationTest:Z

    .line 38
    sget-boolean v2, Lcom/squareup/util/RuntimeEnvironment;->isJvmTest:Z

    if-nez v2, :cond_4

    sget-boolean v2, Lcom/squareup/util/RuntimeEnvironment;->isInstrumentationTest:Z

    if-eqz v2, :cond_5

    :cond_4
    const/4 v0, 0x1

    :cond_5
    sput-boolean v0, Lcom/squareup/util/RuntimeEnvironment;->isAnyTest:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final isAndroid()Z
    .locals 1

    .line 26
    sget-boolean v0, Lcom/squareup/util/RuntimeEnvironment;->isAndroid:Z

    return v0
.end method

.method public static synthetic isAndroid$annotations()V
    .locals 0
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    return-void
.end method

.method public static final isAnyTest()Z
    .locals 1

    .line 38
    sget-boolean v0, Lcom/squareup/util/RuntimeEnvironment;->isAnyTest:Z

    return v0
.end method

.method public static synthetic isAnyTest$annotations()V
    .locals 0
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    return-void
.end method

.method public static final isInstrumentationTest()Z
    .locals 1

    .line 36
    sget-boolean v0, Lcom/squareup/util/RuntimeEnvironment;->isInstrumentationTest:Z

    return v0
.end method

.method public static synthetic isInstrumentationTest$annotations()V
    .locals 0
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    return-void
.end method

.method public static final isJvm()Z
    .locals 1

    .line 31
    sget-boolean v0, Lcom/squareup/util/RuntimeEnvironment;->isJvm:Z

    return v0
.end method

.method public static synthetic isJvm$annotations()V
    .locals 0
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    return-void
.end method

.method public static final isJvmTest()Z
    .locals 1

    .line 34
    sget-boolean v0, Lcom/squareup/util/RuntimeEnvironment;->isJvmTest:Z

    return v0
.end method

.method public static synthetic isJvmTest$annotations()V
    .locals 0
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    return-void
.end method
