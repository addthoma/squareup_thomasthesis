.class public final Lcom/squareup/util/Optional$Empty;
.super Lcom/squareup/util/Optional;
.source "Optional.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/Optional;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Empty"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0003\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u00c6\u0002\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0013\u0010\n\u001a\u00020\u00052\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0096\u0002J\u001c\u0010\r\u001a\u00020\u00002\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00050\u000fH\u0016J,\u0010\u0010\u001a\u00020\u0000\"\u0008\u0008\u0001\u0010\u0011*\u00020\u000c2\u0018\u0010\u0012\u001a\u0014\u0012\u0004\u0012\u00020\u0002\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00110\u00010\u000fH\u0016J\u0008\u0010\u0013\u001a\u00020\u0014H\u0016J\u001c\u0010\u0015\u001a\u00020\u00162\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00160\u000fH\u0016J*\u0010\u0018\u001a\u00020\u00162\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00160\u000f2\u000c\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u001aH\u0016J(\u0010\u001b\u001a\u00020\u0000\"\u0008\u0008\u0001\u0010\u0011*\u00020\u000c2\u0014\u0010\u0012\u001a\u0010\u0012\u0004\u0012\u00020\u0002\u0012\u0006\u0012\u0004\u0018\u0001H\u00110\u000fH\u0016J\"\u0010\u001c\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u0012\u0010\u001d\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00020\u00010\u001aH\u0016J \u0010\u001e\u001a\u00020\u0002\"\u0008\u0008\u0001\u0010\u001f*\u00020 2\u000c\u0010!\u001a\u0008\u0012\u0004\u0012\u0002H\u001f0\u001aH\u0016J\u0008\u0010\"\u001a\u00020#H\u0016R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0004\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u00028VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/util/Optional$Empty;",
        "Lcom/squareup/util/Optional;",
        "",
        "()V",
        "isPresent",
        "",
        "()Z",
        "value",
        "getValue",
        "()Ljava/lang/Void;",
        "equals",
        "other",
        "",
        "filter",
        "predicate",
        "Lkotlin/Function1;",
        "flatMap",
        "U",
        "mapper",
        "hashCode",
        "",
        "ifPresent",
        "",
        "action",
        "ifPresentOrElse",
        "emptyAction",
        "Lkotlin/Function0;",
        "map",
        "or",
        "supplier",
        "orElseThrow",
        "X",
        "",
        "exceptionSupplier",
        "toString",
        "",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/Optional$Empty;

.field private static final isPresent:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 155
    new-instance v0, Lcom/squareup/util/Optional$Empty;

    invoke-direct {v0}, Lcom/squareup/util/Optional$Empty;-><init>()V

    sput-object v0, Lcom/squareup/util/Optional$Empty;->INSTANCE:Lcom/squareup/util/Optional$Empty;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 155
    invoke-direct {p0, v0}, Lcom/squareup/util/Optional;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 175
    move-object v0, p0

    check-cast v0, Lcom/squareup/util/Optional$Empty;

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public filter(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional$Empty;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "*",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/util/Optional$Empty;"
        }
    .end annotation

    const-string v0, "predicate"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public bridge synthetic filter(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;
    .locals 0

    .line 155
    invoke-virtual {p0, p1}, Lcom/squareup/util/Optional$Empty;->filter(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional$Empty;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/Optional;

    return-object p1
.end method

.method public flatMap(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional$Empty;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "*+",
            "Lcom/squareup/util/Optional<",
            "+TU;>;>;)",
            "Lcom/squareup/util/Optional$Empty;"
        }
    .end annotation

    const-string v0, "mapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public bridge synthetic flatMap(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;
    .locals 0

    .line 155
    invoke-virtual {p0, p1}, Lcom/squareup/util/Optional$Empty;->flatMap(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional$Empty;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/Optional;

    return-object p1
.end method

.method public bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .line 155
    invoke-virtual {p0}, Lcom/squareup/util/Optional$Empty;->getValue()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/Void;
    .locals 2

    .line 156
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "No value present"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public ifPresent(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "*",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public ifPresentOrElse(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "*",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "emptyAction"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 165
    invoke-interface {p2}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    return-void
.end method

.method public isPresent()Z
    .locals 1

    .line 157
    sget-boolean v0, Lcom/squareup/util/Optional$Empty;->isPresent:Z

    return v0
.end method

.method public map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional$Empty;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<U:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "*+TU;>;)",
            "Lcom/squareup/util/Optional$Empty;"
        }
    .end annotation

    const-string v0, "mapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public bridge synthetic map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;
    .locals 0

    .line 155
    invoke-virtual {p0, p1}, Lcom/squareup/util/Optional$Empty;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional$Empty;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/Optional;

    return-object p1
.end method

.method public or(Lkotlin/jvm/functions/Function0;)Lcom/squareup/util/Optional;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "+",
            "Lcom/squareup/util/Optional;",
            ">;)",
            "Lcom/squareup/util/Optional;"
        }
    .end annotation

    const-string v0, "supplier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/Optional;

    return-object p1
.end method

.method public bridge synthetic orElseThrow(Lkotlin/jvm/functions/Function0;)Ljava/lang/Object;
    .locals 0

    .line 155
    invoke-virtual {p0, p1}, Lcom/squareup/util/Optional$Empty;->orElseThrow(Lkotlin/jvm/functions/Function0;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public orElseThrow(Lkotlin/jvm/functions/Function0;)Ljava/lang/Void;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<X:",
            "Ljava/lang/Throwable;",
            ">(",
            "Lkotlin/jvm/functions/Function0<",
            "+TX;>;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    const-string v0, "exceptionSupplier"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "Optional.empty"

    return-object v0
.end method
