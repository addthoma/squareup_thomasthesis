.class public Lcom/squareup/util/Numbers;
.super Ljava/lang/Object;
.source "Numbers.java"


# static fields
.field public static final DECIMAL:Ljava/util/regex/Pattern;

.field public static final NUMBER:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "[0-9]*\\.?[0-9]*"

    .line 9
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/Numbers;->DECIMAL:Ljava/util/regex/Pattern;

    const-string v0, "[0-9,\\.]*"

    .line 10
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/Numbers;->NUMBER:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static isDecimalSeparator(Ljava/lang/String;)Z
    .locals 3

    .line 118
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result p0

    const/16 v0, 0x2e

    if-ne p0, v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public static isOrderNumber(Ljava/lang/String;)Z
    .locals 2

    .line 105
    invoke-static {p0}, Lcom/squareup/util/Numbers;->normalizeSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    const/16 v0, 0x30

    const/16 v1, 0x2e

    .line 106
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p0

    const/16 v1, 0x20

    .line 107
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p0

    const/16 v1, 0x23

    .line 108
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p0

    .line 109
    sget-object v0, Lcom/squareup/util/Numbers;->NUMBER:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/regex/Matcher;->matches()Z

    move-result p0

    return p0
.end method

.method private static normalizeDecimal(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 73
    invoke-static {p0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return-object v1

    :cond_0
    const/4 v0, 0x0

    .line 75
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2d

    const/4 v4, 0x1

    if-ne v2, v3, :cond_1

    .line 77
    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    const/4 v0, 0x1

    .line 81
    :cond_1
    invoke-static {p0}, Lcom/squareup/util/Numbers;->normalizeSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 82
    invoke-static {p0}, Lcom/squareup/util/Numbers;->isDecimalSeparator(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    return-object v1

    .line 86
    :cond_2
    invoke-static {p0}, Lcom/squareup/util/Numbers;->stripTrailingDecimalZeros(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 90
    sget-object v2, Lcom/squareup/util/Numbers;->DECIMAL:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_3

    return-object v1

    .line 93
    :cond_3
    invoke-static {p0}, Lcom/squareup/util/Numbers;->isDecimalSeparator(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string p0, "0"

    return-object p0

    :cond_4
    if-eqz v0, :cond_5

    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_5
    return-object p0
.end method

.method private static normalizeSeparators(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/16 v0, 0x2c

    const/16 v1, 0x2e

    .line 114
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static parseDecimal(Ljava/lang/String;)Ljava/math/BigDecimal;
    .locals 1

    .line 68
    invoke-static {p0}, Lcom/squareup/util/Numbers;->normalizeDecimal(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto :goto_0

    .line 69
    :cond_0
    new-instance v0, Ljava/math/BigDecimal;

    invoke-direct {v0, p0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method

.method public static parseFormattedPercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;
    .locals 4

    const-string v0, "%"

    .line 50
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x0

    if-lez v0, :cond_0

    add-int/lit8 v2, v0, -0x1

    .line 52
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isSpaceChar(C)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 54
    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 56
    :cond_0
    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 59
    :cond_1
    :goto_0
    invoke-static {p0, p1}, Lcom/squareup/util/Numbers;->parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;

    move-result-object p0

    return-object p0
.end method

.method public static parsePercentage(Ljava/lang/String;Lcom/squareup/util/Percentage;)Lcom/squareup/util/Percentage;
    .locals 1

    .line 23
    invoke-static {p0}, Lcom/squareup/util/Numbers;->normalizeDecimal(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    if-nez p0, :cond_0

    return-object p1

    .line 27
    :cond_0
    invoke-static {p0}, Lcom/squareup/util/Percentage;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object p0

    .line 28
    sget-object v0, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    invoke-virtual {p0, v0}, Lcom/squareup/util/Percentage;->compareTo(Lcom/squareup/util/Percentage;)I

    move-result v0

    if-ltz v0, :cond_2

    sget-object v0, Lcom/squareup/util/Percentage;->ONE_HUNDRED:Lcom/squareup/util/Percentage;

    .line 29
    invoke-virtual {p0, v0}, Lcom/squareup/util/Percentage;->compareTo(Lcom/squareup/util/Percentage;)I

    move-result v0

    if-lez v0, :cond_1

    goto :goto_0

    :cond_1
    return-object p0

    :cond_2
    :goto_0
    return-object p1
.end method

.method private static stripTrailingDecimalZeros(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 122
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    return-object p0

    :cond_0
    const/16 v0, 0x2e

    .line 123
    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    return-object p0

    .line 128
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    :goto_0
    if-le v1, v0, :cond_2

    add-int/lit8 v2, v1, -0x1

    .line 129
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x30

    if-ne v2, v3, :cond_2

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    .line 132
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
