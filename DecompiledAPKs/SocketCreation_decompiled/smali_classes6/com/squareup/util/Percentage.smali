.class public final Lcom/squareup/util/Percentage;
.super Ljava/lang/Object;
.source "Percentage.kt"

# interfaces
.implements Ljava/lang/Comparable;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/Percentage$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/util/Percentage;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPercentage.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Percentage.kt\ncom/squareup/util/Percentage\n*L\n1#1,209:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0006\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0018\u0000  2\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u0001 B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0006\u0010\u000b\u001a\u00020\u0004J\u0006\u0010\u000c\u001a\u00020\rJ\u0006\u0010\u000e\u001a\u00020\rJ\u0011\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u0000H\u0096\u0002J\u0006\u0010\u0011\u001a\u00020\u0012J\u0006\u0010\u0013\u001a\u00020\u0012J\u0013\u0010\u0014\u001a\u00020\u00072\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0015H\u0096\u0002J\u0008\u0010\u0016\u001a\u00020\u0004H\u0016J\u0006\u0010\u0017\u001a\u00020\u0000J\u000e\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0019J\u0006\u0010\u001b\u001a\u00020\u001cJ\u0008\u0010\u001d\u001a\u00020\u001cH\u0016J\u0018\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u0004H\u0002R\u0011\u0010\u0006\u001a\u00020\u00078F\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0008R\u0011\u0010\t\u001a\u00020\u00078F\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\u0008R\u0011\u0010\n\u001a\u00020\u00078F\u00a2\u0006\u0006\u001a\u0004\u0008\n\u0010\u0008R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/util/Percentage;",
        "",
        "Ljava/io/Serializable;",
        "value",
        "",
        "(I)V",
        "isNegative",
        "",
        "()Z",
        "isPositive",
        "isZero",
        "basisPoints",
        "bigDecimalRate",
        "Ljava/math/BigDecimal;",
        "bigDecimalValue",
        "compareTo",
        "other",
        "doubleRate",
        "",
        "doubleValue",
        "equals",
        "",
        "hashCode",
        "negate",
        "percentOf",
        "",
        "amount",
        "stringSnapshot",
        "",
        "toString",
        "scale",
        "shift",
        "Companion",
        "public"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final BASIS_POINT_TO_VALUE_ROUNDING:I = 0x1f4

.field private static final BASIS_POINT_TO_VALUE_SCALE:I = 0x3e8

.field public static final Companion:Lcom/squareup/util/Percentage$Companion;

.field public static final FIFTEEN:Lcom/squareup/util/Percentage;

.field public static final NEGATIVE_ONE:Lcom/squareup/util/Percentage;

.field public static final ONE:Lcom/squareup/util/Percentage;

.field public static final ONE_HUNDRED:Lcom/squareup/util/Percentage;

.field private static final PERCENT_OF_ROUNDING:I = 0x4c4b40

.field private static final PERCENT_OF_SCALE:I = 0x989680

.field private static final PERCENT_OF_SHIFT:I = 0x7

.field private static final SCALE:I = 0x186a0

.field private static final SHIFT:I = 0x5

.field public static final TEN:Lcom/squareup/util/Percentage;

.field public static final TWENTY:Lcom/squareup/util/Percentage;

.field public static final TWENTY_FIVE:Lcom/squareup/util/Percentage;

.field public static final ZERO:Lcom/squareup/util/Percentage;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/util/Percentage$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/util/Percentage$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    .line 26
    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/Percentage;->NEGATIVE_ONE:Lcom/squareup/util/Percentage;

    .line 28
    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/Percentage;->ZERO:Lcom/squareup/util/Percentage;

    .line 30
    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/Percentage;->ONE:Lcom/squareup/util/Percentage;

    .line 32
    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/Percentage;->TEN:Lcom/squareup/util/Percentage;

    .line 34
    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/Percentage;->FIFTEEN:Lcom/squareup/util/Percentage;

    .line 36
    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/Percentage;->TWENTY:Lcom/squareup/util/Percentage;

    .line 38
    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/Percentage;->TWENTY_FIVE:Lcom/squareup/util/Percentage;

    .line 40
    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object v0

    sput-object v0, Lcom/squareup/util/Percentage;->ONE_HUNDRED:Lcom/squareup/util/Percentage;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/util/Percentage;->value:I

    return-void
.end method

.method public synthetic constructor <init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/util/Percentage;-><init>(I)V

    return-void
.end method

.method public static final fromBasisPoints(I)Lcom/squareup/util/Percentage;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/util/Percentage$Companion;->fromBasisPoints(I)Lcom/squareup/util/Percentage;

    move-result-object p0

    return-object p0
.end method

.method public static final fromDouble(D)Lcom/squareup/util/Percentage;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/util/Percentage$Companion;->fromDouble(D)Lcom/squareup/util/Percentage;

    move-result-object p0

    return-object p0
.end method

.method public static final fromInt(I)Lcom/squareup/util/Percentage;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/util/Percentage$Companion;->fromInt(I)Lcom/squareup/util/Percentage;

    move-result-object p0

    return-object p0
.end method

.method public static final fromNullableDouble(Ljava/lang/Double;)Lcom/squareup/util/Percentage;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/util/Percentage$Companion;->fromNullableDouble(Ljava/lang/Double;)Lcom/squareup/util/Percentage;

    move-result-object p0

    return-object p0
.end method

.method public static final fromRate(D)Lcom/squareup/util/Percentage;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/util/Percentage$Companion;->fromRate(D)Lcom/squareup/util/Percentage;

    move-result-object p0

    return-object p0
.end method

.method public static final fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/util/Percentage$Companion;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object p0

    return-object p0
.end method

.method private final toString(II)Ljava/lang/String;
    .locals 4

    .line 153
    iget v0, p0, Lcom/squareup/util/Percentage;->value:I

    if-gez v0, :cond_0

    neg-int v0, v0

    const-string v1, "-"

    goto :goto_0

    :cond_0
    const-string v1, ""

    .line 160
    :goto_0
    div-int v2, v0, p1

    .line 161
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    mul-int v2, v2, p1

    sub-int/2addr v0, v2

    if-nez v0, :cond_1

    .line 165
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_3

    .line 167
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    .line 169
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v2, 0x30

    if-ge v0, p2, :cond_2

    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 173
    :cond_2
    :goto_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result p2

    if-ne p2, v2, :cond_4

    const-string p2, "fraction"

    .line 174
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p2, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-eqz p1, :cond_3

    invoke-virtual {p1, p2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    const-string p2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 177
    :cond_4
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x2e

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_3
    return-object p1
.end method


# virtual methods
.method public final basisPoints()I
    .locals 1

    .line 121
    iget v0, p0, Lcom/squareup/util/Percentage;->value:I

    add-int/lit16 v0, v0, 0x1f4

    div-int/lit16 v0, v0, 0x3e8

    return v0
.end method

.method public final bigDecimalRate()Ljava/math/BigDecimal;
    .locals 3

    .line 116
    new-instance v0, Ljava/math/BigDecimal;

    const v1, 0x989680

    const/4 v2, 0x7

    invoke-direct {p0, v1, v2}, Lcom/squareup/util/Percentage;->toString(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final bigDecimalValue()Ljava/math/BigDecimal;
    .locals 2

    .line 111
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p0}, Lcom/squareup/util/Percentage;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public compareTo(Lcom/squareup/util/Percentage;)I
    .locals 1

    const-string v0, "other"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    iget v0, p0, Lcom/squareup/util/Percentage;->value:I

    iget p1, p1, Lcom/squareup/util/Percentage;->value:I

    if-ge v0, p1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    if-le v0, p1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/util/Percentage;

    invoke-virtual {p0, p1}, Lcom/squareup/util/Percentage;->compareTo(Lcom/squareup/util/Percentage;)I

    move-result p1

    return p1
.end method

.method public final doubleRate()D
    .locals 4

    .line 107
    iget v0, p0, Lcom/squareup/util/Percentage;->value:I

    int-to-double v0, v0

    const v2, 0x989680

    int-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public final doubleValue()D
    .locals 4

    .line 109
    iget v0, p0, Lcom/squareup/util/Percentage;->value:I

    int-to-double v0, v0

    const v2, 0x186a0

    int-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 129
    instance-of v0, p1, Lcom/squareup/util/Percentage;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget v0, p0, Lcom/squareup/util/Percentage;->value:I

    check-cast p1, Lcom/squareup/util/Percentage;

    iget p1, p1, Lcom/squareup/util/Percentage;->value:I

    if-ne v0, p1, :cond_1

    const/4 v1, 0x1

    :cond_1
    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 1

    .line 141
    iget v0, p0, Lcom/squareup/util/Percentage;->value:I

    return v0
.end method

.method public final isNegative()Z
    .locals 1

    .line 100
    iget v0, p0, Lcom/squareup/util/Percentage;->value:I

    if-gez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isPositive()Z
    .locals 1

    .line 95
    iget v0, p0, Lcom/squareup/util/Percentage;->value:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isZero()Z
    .locals 1

    .line 105
    iget v0, p0, Lcom/squareup/util/Percentage;->value:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final negate()Lcom/squareup/util/Percentage;
    .locals 2

    .line 126
    new-instance v0, Lcom/squareup/util/Percentage;

    iget v1, p0, Lcom/squareup/util/Percentage;->value:I

    neg-int v1, v1

    invoke-direct {v0, v1}, Lcom/squareup/util/Percentage;-><init>(I)V

    return-object v0
.end method

.method public final percentOf(J)J
    .locals 4

    .line 192
    iget v0, p0, Lcom/squareup/util/Percentage;->value:I

    int-to-long v0, v0

    mul-long p1, p1, v0

    const v0, 0x989680

    int-to-long v0, v0

    .line 193
    rem-long v2, p1, v0

    long-to-int v3, v2

    const v2, 0x4c4b40

    if-ne v3, v2, :cond_1

    .line 197
    div-long/2addr p1, v0

    long-to-int p2, p1

    and-int/lit8 p1, p2, 0x1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    add-int/lit8 p2, p2, 0x1

    :goto_0
    int-to-long p1, p2

    return-wide p1

    :cond_1
    int-to-long v2, v2

    add-long/2addr p1, v2

    .line 206
    div-long/2addr p1, v0

    return-wide p1
.end method

.method public final stringSnapshot()Ljava/lang/String;
    .locals 2

    const v0, 0x186a0

    const/4 v1, 0x5

    .line 184
    invoke-direct {p0, v0, v1}, Lcom/squareup/util/Percentage;->toString(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 186
    invoke-virtual {p0}, Lcom/squareup/util/Percentage;->stringSnapshot()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
