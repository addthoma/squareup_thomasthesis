.class Lcom/squareup/queue/QueueService$PrioritizedQueues;
.super Ljava/lang/Object;
.source "QueueService.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/QueueService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrioritizedQueues"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "Lcom/squareup/queue/QueueService$RetrofitQueueStarter;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/queue/QueueService;


# direct methods
.method private constructor <init>(Lcom/squareup/queue/QueueService;)V
    .locals 0

    .line 516
    iput-object p1, p0, Lcom/squareup/queue/QueueService$PrioritizedQueues;->this$0:Lcom/squareup/queue/QueueService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/QueueService$1;)V
    .locals 0

    .line 516
    invoke-direct {p0, p1}, Lcom/squareup/queue/QueueService$PrioritizedQueues;-><init>(Lcom/squareup/queue/QueueService;)V

    return-void
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lcom/squareup/queue/QueueService$RetrofitQueueStarter;",
            ">;"
        }
    .end annotation

    .line 518
    iget-object v0, p0, Lcom/squareup/queue/QueueService$PrioritizedQueues;->this$0:Lcom/squareup/queue/QueueService;

    invoke-static {v0}, Lcom/squareup/queue/QueueService;->access$600(Lcom/squareup/queue/QueueService;)Lcom/squareup/queue/QueueService$LoggedInDependencies;

    move-result-object v0

    .line 519
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_0

    .line 523
    new-instance v8, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;

    iget-object v3, p0, Lcom/squareup/queue/QueueService$PrioritizedQueues;->this$0:Lcom/squareup/queue/QueueService;

    invoke-static {}, Lcom/squareup/MortarLoggedIn;->getLoggedInScope()Lmortar/MortarScope;

    move-result-object v4

    iget-object v5, v0, Lcom/squareup/queue/QueueService$LoggedInDependencies;->captureQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v6, v0, Lcom/squareup/queue/QueueService$LoggedInDependencies;->loggedInTaskWatcher:Lcom/squareup/queue/TaskWatcher;

    const-string v7, "Capture task"

    move-object v2, v8

    invoke-direct/range {v2 .. v7}, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;-><init>(Lcom/squareup/queue/QueueService;Lmortar/MortarScope;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/TaskWatcher;Ljava/lang/String;)V

    invoke-interface {v1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 529
    :cond_0
    new-instance v2, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;

    iget-object v10, p0, Lcom/squareup/queue/QueueService$PrioritizedQueues;->this$0:Lcom/squareup/queue/QueueService;

    invoke-static {v10}, Lcom/squareup/queue/QueueService;->access$700(Lcom/squareup/queue/QueueService;)Lmortar/MortarScope;

    move-result-object v11

    iget-object v3, p0, Lcom/squareup/queue/QueueService$PrioritizedQueues;->this$0:Lcom/squareup/queue/QueueService;

    iget-object v12, v3, Lcom/squareup/queue/QueueService;->storeAndForwardQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v3, p0, Lcom/squareup/queue/QueueService$PrioritizedQueues;->this$0:Lcom/squareup/queue/QueueService;

    iget-object v13, v3, Lcom/squareup/queue/QueueService;->loggedOutTaskWatcher:Lcom/squareup/queue/TaskWatcher;

    const-string v14, "Store and forward task"

    move-object v9, v2

    invoke-direct/range {v9 .. v14}, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;-><init>(Lcom/squareup/queue/QueueService;Lmortar/MortarScope;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/TaskWatcher;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_2

    .line 534
    iget-object v2, p0, Lcom/squareup/queue/QueueService$PrioritizedQueues;->this$0:Lcom/squareup/queue/QueueService;

    iget-object v2, v2, Lcom/squareup/queue/QueueService;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->USE_SEPARATE_QUEUE_FOR_LOCAL_PAYMENTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 535
    new-instance v2, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;

    iget-object v4, p0, Lcom/squareup/queue/QueueService$PrioritizedQueues;->this$0:Lcom/squareup/queue/QueueService;

    invoke-static {v4}, Lcom/squareup/queue/QueueService;->access$700(Lcom/squareup/queue/QueueService;)Lmortar/MortarScope;

    move-result-object v5

    iget-object v6, v0, Lcom/squareup/queue/QueueService$LoggedInDependencies;->localPaymentsQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v7, v0, Lcom/squareup/queue/QueueService$LoggedInDependencies;->loggedInTaskWatcher:Lcom/squareup/queue/TaskWatcher;

    const-string v8, "Local Payments task"

    move-object v3, v2

    invoke-direct/range {v3 .. v8}, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;-><init>(Lcom/squareup/queue/QueueService;Lmortar/MortarScope;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/TaskWatcher;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 540
    :cond_1
    new-instance v2, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;

    iget-object v10, p0, Lcom/squareup/queue/QueueService$PrioritizedQueues;->this$0:Lcom/squareup/queue/QueueService;

    invoke-static {}, Lcom/squareup/MortarLoggedIn;->getLoggedInScope()Lmortar/MortarScope;

    move-result-object v11

    iget-object v12, v0, Lcom/squareup/queue/QueueService$LoggedInDependencies;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v13, v0, Lcom/squareup/queue/QueueService$LoggedInDependencies;->loggedInTaskWatcher:Lcom/squareup/queue/TaskWatcher;

    const-string v14, "Non-capture task"

    move-object v9, v2

    invoke-direct/range {v9 .. v14}, Lcom/squareup/queue/QueueService$RetrofitQueueStarter;-><init>(Lcom/squareup/queue/QueueService;Lmortar/MortarScope;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/queue/TaskWatcher;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 545
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method
