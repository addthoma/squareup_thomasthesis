.class public final Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;
.super Lcom/squareup/queue/retrofit/RetrofitQueueFactory;
.source "SqliteRetrofitQueueFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001BO\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0004\u0012\u00020\u0004\u0012\u000e\u0012\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00060\u00050\u0003\u0012\u0010\u0010\u0007\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00060\u0008\u0012\u0010\u0010\t\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00060\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u00a2\u0006\u0002\u0010\rJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0004H\u0016R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0004\u0012\u00020\u0004\u0012\u000e\u0012\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00060\u00050\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;",
        "Lcom/squareup/queue/retrofit/RetrofitQueueFactory;",
        "sqliteQueueFactory",
        "Lrx/functions/Func1;",
        "Ljava/io/File;",
        "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        "taskInjector",
        "Lcom/squareup/tape/TaskInjector;",
        "converter",
        "Lcom/squareup/tape/FileObjectQueue$Converter;",
        "queueServiceStarter",
        "Lcom/squareup/queue/QueueServiceStarter;",
        "(Lrx/functions/Func1;Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/queue/QueueServiceStarter;)V",
        "open",
        "Lcom/squareup/queue/retrofit/RetrofitQueue;",
        "file",
        "transaction_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

.field private final sqliteQueueFactory:Lrx/functions/Func1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func1<",
            "Ljava/io/File;",
            "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "*>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/functions/Func1;Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/queue/QueueServiceStarter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/Func1<",
            "Ljava/io/File;",
            "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "*>;>;>;",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "*>;>;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask<",
            "*>;>;",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ")V"
        }
    .end annotation

    const-string v0, "sqliteQueueFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "taskInjector"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "converter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "queueServiceStarter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p2, p3}, Lcom/squareup/queue/retrofit/RetrofitQueueFactory;-><init>(Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;)V

    iput-object p1, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;->sqliteQueueFactory:Lrx/functions/Func1;

    iput-object p4, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    return-void
.end method


# virtual methods
.method public open(Ljava/io/File;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 3

    const-string v0, "file"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;->sqliteQueueFactory:Lrx/functions/Func1;

    invoke-interface {v0, p1}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    .line 28
    new-instance v0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;

    const-string v1, "sqliteQueue"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;->taskInjector:Lcom/squareup/tape/TaskInjector;

    const-string v2, "taskInjector"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/queue/sqlite/SqliteRetrofitQueue;-><init>(Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;Lcom/squareup/tape/TaskInjector;Lcom/squareup/queue/QueueServiceStarter;)V

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-object v0
.end method

.method public bridge synthetic open(Ljava/io/File;)Lcom/squareup/tape/ObjectQueue;
    .locals 0

    .line 19
    invoke-virtual {p0, p1}, Lcom/squareup/queue/sqlite/SqliteRetrofitQueueFactory;->open(Ljava/io/File;)Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object p1

    check-cast p1, Lcom/squareup/tape/ObjectQueue;

    return-object p1
.end method
