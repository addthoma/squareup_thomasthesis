.class public Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;
.super Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;
.source "LocalPaymentsSqliteQueue.java"

# interfaces
.implements Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        ">;",
        "Lcom/squareup/queue/sqlite/LocalPaymentsMonitor;"
    }
.end annotation


# instance fields
.field private final converter:Lcom/squareup/queue/sqlite/LocalPaymentConverter;

.field private final mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final store:Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;


# direct methods
.method public constructor <init>(Lcom/squareup/queue/sqlite/shared/SqliteQueue;Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;Lcom/squareup/queue/sqlite/LocalPaymentConverter;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0
    .param p4    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/shared/SqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;",
            "Lcom/squareup/queue/sqlite/LocalPaymentConverter;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")V"
        }
    .end annotation

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;-><init>(Lcom/squareup/queue/sqlite/shared/SqliteQueue;)V

    .line 32
    iput-object p2, p0, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;->store:Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;

    .line 33
    iput-object p3, p0, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;->converter:Lcom/squareup/queue/sqlite/LocalPaymentConverter;

    .line 34
    iput-object p4, p0, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method


# virtual methods
.method public allLocalPaymentsAsBillHistory(Lcom/squareup/util/Res;)Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            ")",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 49
    invoke-virtual {p0}, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;->localPaymentsCount()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;->store:Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;

    .line 51
    invoke-virtual {v1}, Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;->allLocalPaymentEntriesAsStream()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v1, v2}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v1

    new-instance v2, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;

    iget-object v3, p0, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;->converter:Lcom/squareup/queue/sqlite/LocalPaymentConverter;

    invoke-direct {v2, p1, v3}, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;)V

    .line 50
    invoke-static {v1, v2}, Lcom/squareup/queue/sqlite/SqliteQueues;->convertStreamAndBuffer(Lrx/Observable;Lrx/functions/Func1;)Lrx/Observable$Transformer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lrx/Observable;->compose(Lrx/Observable$Transformer;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public fetchTransaction(Lcom/squareup/util/Res;Ljava/lang/String;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Res;",
            "Ljava/lang/String;",
            ")",
            "Lrx/Single<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/billhistory/model/BillHistory;",
            ">;>;"
        }
    .end annotation

    .line 58
    iget-object v0, p0, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 59
    iget-object v0, p0, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;->store:Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;

    invoke-virtual {v0, p2}, Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;->fetchEntry(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/queue/sqlite/-$$Lambda$LocalPaymentsSqliteQueue$lBhjmSdMptdh34KlvmjI4g4IjmA;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/sqlite/-$$Lambda$LocalPaymentsSqliteQueue$lBhjmSdMptdh34KlvmjI4g4IjmA;-><init>(Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;Lcom/squareup/util/Res;)V

    .line 60
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 59
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$fetchTransaction$0$LocalPaymentsSqliteQueue(Lcom/squareup/util/Res;Lcom/squareup/util/Optional;)Lcom/squareup/util/Optional;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 60
    new-instance v0, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;

    iget-object v1, p0, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;->converter:Lcom/squareup/queue/sqlite/LocalPaymentConverter;

    invoke-direct {v0, p1, v1}, Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance p1, Lcom/squareup/queue/sqlite/-$$Lambda$RIzm1BaqjtHucGNQdyANYg83D_Q;

    invoke-direct {p1, v0}, Lcom/squareup/queue/sqlite/-$$Lambda$RIzm1BaqjtHucGNQdyANYg83D_Q;-><init>(Lcom/squareup/queue/sqlite/SqliteQueues$ToBillHistory;)V

    invoke-virtual {p2, p1}, Lcom/squareup/util/Optional;->map(Lkotlin/jvm/functions/Function1;)Lcom/squareup/util/Optional;

    move-result-object p1

    return-object p1
.end method

.method public localPaymentsCount()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 38
    iget-object v0, p0, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 39
    iget-object v0, p0, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;->store:Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;->count()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public ripenedLocalPaymentsCount()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;->mainThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 44
    iget-object v0, p0, Lcom/squareup/queue/sqlite/LocalPaymentsSqliteQueue;->store:Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;

    invoke-virtual {v0}, Lcom/squareup/queue/sqlite/LocalPaymentSqliteStore;->ripenedCount()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method
