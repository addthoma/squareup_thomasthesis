.class public final Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;
.super Ljava/lang/Object;
.source "PendingCapturesEntry.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/PendingCapturesEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0012\n\u0002\u0008\u0004\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0018\u001a\u00020\u00052\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u001b\u001a\u00020\t2\u0006\u0010\u001c\u001a\u00020\u001dH\u0007J1\u0010\u001e\u001a\u00020\u00052\u0008\u0010\u001f\u001a\u0004\u0018\u00010\t2\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u001b\u001a\u00020\t2\u0006\u0010\u001c\u001a\u00020\u001dH\u0007\u00a2\u0006\u0002\u0010 R\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0017\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0007R\u001f\u0010\u000b\u001a\u0010\u0012\u000c\u0012\n \r*\u0004\u0018\u00010\u00050\u00050\u000c\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0017\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0007R\u0017\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0007R\u0017\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0007R\u0017\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0007\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;",
        "",
        "()V",
        "ALL_ENTRIES_MAPPER",
        "Lcom/squareup/sqldelight/prerelease/RowMapper;",
        "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
        "getALL_ENTRIES_MAPPER",
        "()Lcom/squareup/sqldelight/prerelease/RowMapper;",
        "COUNT_MAPPER",
        "",
        "getCOUNT_MAPPER",
        "FACTORY",
        "Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;",
        "kotlin.jvm.PlatformType",
        "getFACTORY",
        "()Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;",
        "FIRST_ENTRY_MAPPER",
        "getFIRST_ENTRY_MAPPER",
        "GET_ENTRY_MAPPER",
        "getGET_ENTRY_MAPPER",
        "OLDEST_ENTRY_MAPPER",
        "getOLDEST_ENTRY_MAPPER",
        "RIPENED_COUNT_MAPPER",
        "getRIPENED_COUNT_MAPPER",
        "newPendingCapturesEntry",
        "entryId",
        "",
        "timestampMs",
        "data",
        "",
        "newPendingCapturesEntryForTest",
        "_id",
        "(Ljava/lang/Long;Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
        "queue_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 60
    invoke-direct {p0}, Lcom/squareup/queue/sqlite/PendingCapturesEntry$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getALL_ENTRIES_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
            ">;"
        }
    .end annotation

    .line 78
    invoke-static {}, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->access$getALL_ENTRIES_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    return-object v0
.end method

.method public final getCOUNT_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 73
    invoke-static {}, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->access$getCOUNT_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    return-object v0
.end method

.method public final getFACTORY()Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory<",
            "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
            ">;"
        }
    .end annotation

    .line 61
    invoke-static {}, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->access$getFACTORY$cp()Lcom/squareup/queue/sqlite/PendingCapturesModel$Factory;

    move-result-object v0

    return-object v0
.end method

.method public final getFIRST_ENTRY_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
            ">;"
        }
    .end annotation

    .line 75
    invoke-static {}, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->access$getFIRST_ENTRY_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    return-object v0
.end method

.method public final getGET_ENTRY_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
            ">;"
        }
    .end annotation

    .line 76
    invoke-static {}, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->access$getGET_ENTRY_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    return-object v0
.end method

.method public final getOLDEST_ENTRY_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Lcom/squareup/queue/sqlite/PendingCapturesEntry;",
            ">;"
        }
    .end annotation

    .line 77
    invoke-static {}, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->access$getOLDEST_ENTRY_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    return-object v0
.end method

.method public final getRIPENED_COUNT_MAPPER()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 74
    invoke-static {}, Lcom/squareup/queue/sqlite/PendingCapturesEntry;->access$getRIPENED_COUNT_MAPPER$cp()Lcom/squareup/sqldelight/prerelease/RowMapper;

    move-result-object v0

    return-object v0
.end method

.method public final newPendingCapturesEntry(Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/PendingCapturesEntry;
    .locals 8
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "data"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    new-instance v0, Lcom/squareup/queue/sqlite/PendingCapturesEntry;

    invoke-static {p1}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x0

    const/4 v7, 0x0

    move-object v1, v0

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/squareup/queue/sqlite/PendingCapturesEntry;-><init>(Ljava/lang/Long;Ljava/lang/String;J[BLkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final newPendingCapturesEntryForTest(Ljava/lang/Long;Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/PendingCapturesEntry;
    .locals 8
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    const-string v0, "data"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    new-instance v0, Lcom/squareup/queue/sqlite/PendingCapturesEntry;

    invoke-static {p2}, Lcom/squareup/util/Strings;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/squareup/queue/sqlite/PendingCapturesEntry;-><init>(Ljava/lang/Long;Ljava/lang/String;J[BLkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
