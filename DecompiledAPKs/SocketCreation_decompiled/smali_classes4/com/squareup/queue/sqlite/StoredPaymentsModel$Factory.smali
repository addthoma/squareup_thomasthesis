.class public final Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;
.super Ljava/lang/Object;
.source "StoredPaymentsModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/StoredPaymentsModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$GetEntryQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/queue/sqlite/StoredPaymentsModel;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final creator:Lcom/squareup/queue/sqlite/StoredPaymentsModel$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/sqlite/StoredPaymentsModel$Creator<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/queue/sqlite/StoredPaymentsModel$Creator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/sqlite/StoredPaymentsModel$Creator<",
            "TT;>;)V"
        }
    .end annotation

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iput-object p1, p0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;->creator:Lcom/squareup/queue/sqlite/StoredPaymentsModel$Creator;

    return-void
.end method


# virtual methods
.method public allDistinctEntries()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 165
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "stored_payments"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT _id, entry_id, MAX(timestamp_ms) AS timestamp_ms, data\nFROM stored_payments\nGROUP BY entry_id\nORDER BY timestamp_ms DESC"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public allDistinctEntriesMapper(Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesCreator;)Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R::",
            "Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesModel;",
            ">(",
            "Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesCreator<",
            "TR;>;)",
            "Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesMapper<",
            "TR;>;"
        }
    .end annotation

    .line 216
    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesMapper;

    invoke-direct {v0, p1}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesMapper;-><init>(Lcom/squareup/queue/sqlite/StoredPaymentsModel$AllDistinctEntriesCreator;)V

    return-object v0
.end method

.method public allDistinctEntryIds()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 175
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "stored_payments"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT DISTINCT(entry_id)\nFROM stored_payments"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public allDistinctEntryIdsMapper()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 220
    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$3;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$3;-><init>(Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;)V

    return-object v0
.end method

.method public count()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 126
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "stored_payments"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT COUNT(*)\nFROM stored_payments"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public countMapper()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 182
    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$1;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$1;-><init>(Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;)V

    return-object v0
.end method

.method public distinctCount()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 134
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "stored_payments"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT COUNT(DISTINCT entry_id)\nFROM stored_payments"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public distinctCountMapper()Lcom/squareup/sqldelight/prerelease/RowMapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 191
    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$2;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$2;-><init>(Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;)V

    return-object v0
.end method

.method public firstEntry()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 142
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "stored_payments"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT *\nFROM stored_payments\nWHERE _id = ( SELECT MIN(_id) FROM stored_payments )"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public firstEntryMapper()Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper<",
            "TT;>;"
        }
    .end annotation

    .line 201
    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper;-><init>(Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;)V

    return-object v0
.end method

.method public getEntry(Ljava/lang/String;)Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 1

    .line 151
    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$GetEntryQuery;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory$GetEntryQuery;-><init>(Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;Ljava/lang/String;)V

    return-object v0
.end method

.method public getEntryMapper()Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper<",
            "TT;>;"
        }
    .end annotation

    .line 206
    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper;-><init>(Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;)V

    return-object v0
.end method

.method public oldestEntry()Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;
    .locals 3

    .line 156
    new-instance v0, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;

    new-instance v1, Lcom/squareup/sqldelight/prerelease/internal/TableSet;

    const-string v2, "stored_payments"

    filled-new-array {v2}, [Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/sqldelight/prerelease/internal/TableSet;-><init>([Ljava/lang/String;)V

    const-string v2, "SELECT *\nFROM stored_payments\nWHERE timestamp_ms = ( SELECT MIN(timestamp_ms) FROM stored_payments )"

    invoke-direct {v0, v2, v1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;-><init>(Ljava/lang/String;Ljava/util/Set;)V

    return-object v0
.end method

.method public oldestEntryMapper()Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper<",
            "TT;>;"
        }
    .end annotation

    .line 211
    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/StoredPaymentsModel$Mapper;-><init>(Lcom/squareup/queue/sqlite/StoredPaymentsModel$Factory;)V

    return-object v0
.end method
