.class public interface abstract Lcom/squareup/queue/sqlite/TasksModel;
.super Ljava/lang/Object;
.source "TasksModel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/sqlite/TasksModel$DeleteAllEntries;,
        Lcom/squareup/queue/sqlite/TasksModel$DeleteFirstEntry;,
        Lcom/squareup/queue/sqlite/TasksModel$InsertEntry;,
        Lcom/squareup/queue/sqlite/TasksModel$Factory;,
        Lcom/squareup/queue/sqlite/TasksModel$Mapper;,
        Lcom/squareup/queue/sqlite/TasksModel$Creator;
    }
.end annotation


# static fields
.field public static final CREATE_TABLE:Ljava/lang/String; = "CREATE TABLE tasks (\n  -- Alias for ROWID.\n  _id INTEGER PRIMARY KEY,\n\n  /*\n   * Identifier associated with the task represented by this entry (currently for logging purposes\n   * only).\n   */\n  entry_id TEXT NOT NULL,\n\n  -- Timestamp of the task, in milliseconds since epoch.\n  timestamp_ms INTEGER NOT NULL CHECK (timestamp_ms >= 0),\n\n  -- True (i.e., 1) if the task represents a local payment.\n  is_local_payment INTEGER NOT NULL CHECK (is_local_payment IN (0, 1)),\n\n  -- Binary representation of the task.\n  data BLOB NOT NULL\n)"

.field public static final DATA:Ljava/lang/String; = "data"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ENTRY_ID:Ljava/lang/String; = "entry_id"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final IS_LOCAL_PAYMENT:Ljava/lang/String; = "is_local_payment"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TABLE_NAME:Ljava/lang/String; = "tasks"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TIMESTAMP_MS:Ljava/lang/String; = "timestamp_ms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final _ID:Ljava/lang/String; = "_id"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# virtual methods
.method public abstract _id()Ljava/lang/Long;
.end method

.method public abstract data()[B
.end method

.method public abstract entry_id()Ljava/lang/String;
.end method

.method public abstract is_local_payment()J
.end method

.method public abstract timestamp_ms()J
.end method
