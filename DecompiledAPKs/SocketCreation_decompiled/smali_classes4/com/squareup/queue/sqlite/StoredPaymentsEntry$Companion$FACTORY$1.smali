.class final Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion$FACTORY$1;
.super Ljava/lang/Object;
.source "StoredPaymentsEntry.kt"

# interfaces
.implements Lcom/squareup/queue/sqlite/StoredPaymentsModel$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/sqlite/StoredPaymentsEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/queue/sqlite/StoredPaymentsModel;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/sqlite/StoredPaymentsModel$Creator<",
        "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0012\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u0008H\n\u00a2\u0006\u0004\u0008\t\u0010\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/queue/sqlite/StoredPaymentsEntry;",
        "_id",
        "",
        "entry_id",
        "",
        "timestamp_ms",
        "data",
        "",
        "create",
        "(Ljava/lang/Long;Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/StoredPaymentsEntry;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion$FACTORY$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion$FACTORY$1;

    invoke-direct {v0}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion$FACTORY$1;-><init>()V

    sput-object v0, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion$FACTORY$1;->INSTANCE:Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion$FACTORY$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Long;Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/StoredPaymentsEntry;
    .locals 8

    const-string v0, "entry_id"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "data"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    new-instance v0, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;

    const/4 v7, 0x0

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry;-><init>(Ljava/lang/Long;Ljava/lang/String;J[BLkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public bridge synthetic create(Ljava/lang/Long;Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/StoredPaymentsModel;
    .locals 0

    .line 57
    invoke-virtual/range {p0 .. p5}, Lcom/squareup/queue/sqlite/StoredPaymentsEntry$Companion$FACTORY$1;->create(Ljava/lang/Long;Ljava/lang/String;J[B)Lcom/squareup/queue/sqlite/StoredPaymentsEntry;

    move-result-object p1

    check-cast p1, Lcom/squareup/queue/sqlite/StoredPaymentsModel;

    return-object p1
.end method
