.class public final Lcom/squareup/queue/sqlite/QueueStoresKt;
.super Ljava/lang/Object;
.source "QueueStores.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nQueueStores.kt\nKotlin\n*S Kotlin\n*F\n+ 1 QueueStores.kt\ncom/squareup/queue/sqlite/QueueStoresKt\n*L\n1#1,238:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000x\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u001a:\u0010\u0000\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u00042\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0001H\u0000\u001aB\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0000\u001a\u000c\u0010\u0012\u001a\u00020\u0013*\u00020\tH\u0000\u001a\u001a\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0015*\u00020\t2\u0006\u0010\u0016\u001a\u00020\u0017H\u0000\u001a]\u0010\u0018\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u00030\u00190\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0010\u001a\u00020\u00112\u0019\u0010\u001c\u001a\u0015\u0012\u0004\u0012\u00020\u001e\u0012\u0006\u0012\u0004\u0018\u0001H\u00030\u001d\u00a2\u0006\u0002\u0008\u001fH\u0000\u001ah\u0010 \u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\n\u0008\u0002\u0010!\u001a\u0004\u0018\u0001H\u00032\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0010\u001a\u00020\u00112\u0019\u0010\u001c\u001a\u0015\u0012\u0004\u0012\u00020\u001e\u0012\u0006\u0012\u0004\u0018\u0001H\u00030\u001d\u00a2\u0006\u0002\u0008\u001fH\u0000\u00a2\u0006\u0002\u0010\"\u001aT\u0010#\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u0008\u0012\u0004\u0012\u0002H\u00030\u00012\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0010\u001a\u00020\u00112\u0018\u0010$\u001a\u0014\u0012\u0004\u0012\u0002H\u0003\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00060\u00010\u001dH\u0000\u001a\u000c\u0010%\u001a\u00020&*\u00020\'H\u0000\u001a\u001c\u0010(\u001a\u00020\u0006*\u0004\u0018\u00010)2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\'0\rH\u0000\u001a*\u0010*\u001a\n\u0012\u0004\u0012\u0002H\u0003\u0018\u00010\u0002\"\u0004\u0008\u0000\u0010\u0003*\u0004\u0018\u00010)2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\rH\u0000\u001a-\u0010+\u001a\u0004\u0018\u0001H\u0003\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u0004\u0018\u00010)2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\rH\u0000\u00a2\u0006\u0002\u0010,\u001a\u000c\u0010-\u001a\u00020\'*\u00020&H\u0000\u001a,\u0010.\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0004*\u0004\u0018\u00010)2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\rH\u0000\u00a8\u0006/"
    }
    d2 = {
        "allEntriesAsList",
        "Lio/reactivex/Observable;",
        "",
        "T",
        "",
        "count",
        "",
        "allEntries",
        "allEntriesAsStream",
        "Lcom/squareup/sqlbrite3/BriteDatabase;",
        "statement",
        "Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;",
        "mapper",
        "Lcom/squareup/sqldelight/prerelease/RowMapper;",
        "scheduler",
        "Lio/reactivex/Scheduler;",
        "errorMessage",
        "",
        "closeReactive",
        "Lio/reactivex/Completable;",
        "delete",
        "Lio/reactivex/Single;",
        "deleteFirst",
        "Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;",
        "queryOptionalResults",
        "Lcom/squareup/util/Optional;",
        "disposables",
        "Lio/reactivex/disposables/CompositeDisposable;",
        "queryMapper",
        "Lkotlin/Function1;",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        "Lkotlin/ExtensionFunctionType;",
        "queryResults",
        "defaultValue",
        "(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Ljava/lang/Object;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;",
        "ripenedCount",
        "toRipenedCount",
        "toBoolean",
        "",
        "",
        "toCount",
        "Landroid/database/Cursor;",
        "toEntries",
        "toEntry",
        "(Landroid/database/Cursor;Lcom/squareup/sqldelight/prerelease/RowMapper;)Ljava/lang/Object;",
        "toLong",
        "toStream",
        "queue_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final allEntriesAsList(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;",
            "Lio/reactivex/Observable<",
            "TT;>;)",
            "Lio/reactivex/Observable<",
            "Ljava/util/List<",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "count"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "allEntries"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    new-instance v0, Lcom/squareup/queue/sqlite/QueueStoresKt$allEntriesAsList$1;

    invoke-direct {v0, p1}, Lcom/squareup/queue/sqlite/QueueStoresKt$allEntriesAsList$1;-><init>(Lio/reactivex/Observable;)V

    check-cast v0, Lio/reactivex/ObservableTransformer;

    invoke-virtual {p0, v0}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "count.compose<List<T>> {\u2026(count)\n      }\n    }\n  }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final allEntriesAsStream(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Lcom/squareup/sqldelight/prerelease/RowMapper;Lio/reactivex/Scheduler;Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/sqlbrite3/BriteDatabase;",
            "Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "TT;>;",
            "Lio/reactivex/Scheduler;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$allEntriesAsStream"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "statement"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mapper"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorMessage"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    invoke-virtual {p1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;->getTables()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    check-cast p1, Landroidx/sqlite/db/SupportSQLiteQuery;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->createQuery(Ljava/lang/Iterable;Landroidx/sqlite/db/SupportSQLiteQuery;)Lcom/squareup/sqlbrite3/QueryObservable;

    move-result-object p0

    .line 134
    invoke-virtual {p0, p3}, Lcom/squareup/sqlbrite3/QueryObservable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p0

    .line 135
    new-instance p1, Lcom/squareup/queue/sqlite/QueueStoresKt$allEntriesAsStream$1;

    invoke-direct {p1, p2}, Lcom/squareup/queue/sqlite/QueueStoresKt$allEntriesAsStream$1;-><init>(Lcom/squareup/sqldelight/prerelease/RowMapper;)V

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    .line 139
    new-instance p1, Lcom/squareup/queue/sqlite/QueueStoresKt$allEntriesAsStream$2;

    invoke-direct {p1, p4}, Lcom/squareup/queue/sqlite/QueueStoresKt$allEntriesAsStream$2;-><init>(Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "createQuery(statement.ta\u2026ption(errorMessage, it) }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final closeReactive(Lcom/squareup/sqlbrite3/BriteDatabase;)Lio/reactivex/Completable;
    .locals 1

    const-string v0, "$this$closeReactive"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    new-instance v0, Lcom/squareup/queue/sqlite/QueueStoresKt$closeReactive$1;

    invoke-direct {v0, p0}, Lcom/squareup/queue/sqlite/QueueStoresKt$closeReactive$1;-><init>(Lcom/squareup/sqlbrite3/BriteDatabase;)V

    check-cast v0, Lio/reactivex/functions/Action;

    invoke-static {v0}, Lio/reactivex/Completable;->fromAction(Lio/reactivex/functions/Action;)Lio/reactivex/Completable;

    move-result-object p0

    const-string v0, "Completable.fromAction { this.close() }"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final delete(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/sqlbrite3/BriteDatabase;",
            "Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;",
            ")",
            "Lio/reactivex/Single<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$delete"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "deleteFirst"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    new-instance v0, Lcom/squareup/queue/sqlite/QueueStoresKt$delete$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/sqlite/QueueStoresKt$delete$1;-><init>(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightStatement;)V

    check-cast v0, Ljava/util/concurrent/Callable;

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object p0

    const-string p1, "Single.fromCallable {\n  \u2026irst entry\", e)\n    }\n  }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final queryOptionalResults(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/sqlbrite3/BriteDatabase;",
            "Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;",
            "Lio/reactivex/Scheduler;",
            "Lio/reactivex/disposables/CompositeDisposable;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            "+TT;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/util/Optional<",
            "TT;>;>;"
        }
    .end annotation

    const-string v0, "$this$queryOptionalResults"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "statement"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "disposables"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorMessage"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "queryMapper"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    invoke-virtual {p1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;->getTables()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    check-cast p1, Landroidx/sqlite/db/SupportSQLiteQuery;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->createQuery(Ljava/lang/Iterable;Landroidx/sqlite/db/SupportSQLiteQuery;)Lcom/squareup/sqlbrite3/QueryObservable;

    move-result-object p0

    .line 111
    invoke-virtual {p0, p2}, Lcom/squareup/sqlbrite3/QueryObservable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "createQuery(statement.ta\u2026  .subscribeOn(scheduler)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    new-instance p1, Lcom/squareup/queue/sqlite/QueueStoresKt$queryOptionalResults$1;

    invoke-direct {p1, p5}, Lcom/squareup/queue/sqlite/QueueStoresKt$queryOptionalResults$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, p1}, Lcom/squareup/util/rx2/Rx2Kt;->mapNotNull(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p0

    .line 113
    sget-object p1, Lcom/squareup/util/Optional;->Companion:Lcom/squareup/util/Optional$Companion;

    invoke-virtual {p1}, Lcom/squareup/util/Optional$Companion;->empty()Lcom/squareup/util/Optional;

    move-result-object p1

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->defaultIfEmpty(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p0

    const/4 p1, 0x1

    .line 114
    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p0

    .line 115
    new-instance p1, Lcom/squareup/queue/sqlite/QueueStoresKt$queryOptionalResults$2;

    invoke-direct {p1, p3}, Lcom/squareup/queue/sqlite/QueueStoresKt$queryOptionalResults$2;-><init>(Lio/reactivex/disposables/CompositeDisposable;)V

    check-cast p1, Lio/reactivex/functions/Consumer;

    const/4 p2, 0x0

    invoke-virtual {p0, p2, p1}, Lio/reactivex/observables/ConnectableObservable;->autoConnect(ILio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p0

    .line 116
    new-instance p1, Lcom/squareup/queue/sqlite/QueueStoresKt$queryOptionalResults$3;

    invoke-direct {p1, p4}, Lcom/squareup/queue/sqlite/QueueStoresKt$queryOptionalResults$3;-><init>(Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "createQuery(statement.ta\u2026ption(errorMessage, it) }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final queryResults(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Ljava/lang/Object;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/sqlbrite3/BriteDatabase;",
            "Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;",
            "TT;",
            "Lio/reactivex/Scheduler;",
            "Lio/reactivex/disposables/CompositeDisposable;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            "+TT;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$queryResults"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "statement"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "disposables"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorMessage"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "queryMapper"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p1}, Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;->getTables()Ljava/util/Set;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    check-cast p1, Landroidx/sqlite/db/SupportSQLiteQuery;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->createQuery(Ljava/lang/Iterable;Landroidx/sqlite/db/SupportSQLiteQuery;)Lcom/squareup/sqlbrite3/QueryObservable;

    move-result-object p0

    .line 51
    invoke-virtual {p0, p3}, Lcom/squareup/sqlbrite3/QueryObservable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "createQuery(statement.ta\u2026  .subscribeOn(scheduler)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    new-instance p1, Lcom/squareup/queue/sqlite/QueueStoresKt$queryResults$1;

    invoke-direct {p1, p6, p2}, Lcom/squareup/queue/sqlite/QueueStoresKt$queryResults$1;-><init>(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, p1}, Lcom/squareup/util/rx2/Rx2Kt;->mapNotNull(Lio/reactivex/Observable;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p0

    if-eqz p2, :cond_0

    .line 55
    invoke-virtual {p0, p2}, Lio/reactivex/Observable;->defaultIfEmpty(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p0

    :cond_0
    const/4 p1, 0x1

    .line 60
    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p0

    const/4 p1, 0x0

    .line 61
    new-instance p2, Lcom/squareup/queue/sqlite/QueueStoresKt$queryResults$3;

    invoke-direct {p2, p4}, Lcom/squareup/queue/sqlite/QueueStoresKt$queryResults$3;-><init>(Lio/reactivex/disposables/CompositeDisposable;)V

    check-cast p2, Lio/reactivex/functions/Consumer;

    invoke-virtual {p0, p1, p2}, Lio/reactivex/observables/ConnectableObservable;->autoConnect(ILio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p0

    .line 62
    new-instance p1, Lcom/squareup/queue/sqlite/QueueStoresKt$queryResults$4;

    invoke-direct {p1, p5}, Lcom/squareup/queue/sqlite/QueueStoresKt$queryResults$4;-><init>(Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "createQuery(statement.ta\u2026ption(errorMessage, it) }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static synthetic queryResults$default(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Ljava/lang/Object;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/Observable;
    .locals 7

    and-int/lit8 p7, p7, 0x2

    if-eqz p7, :cond_0

    const/4 p2, 0x0

    :cond_0
    move-object v2, p2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    .line 44
    invoke-static/range {v0 .. v6}, Lcom/squareup/queue/sqlite/QueueStoresKt;->queryResults(Lcom/squareup/sqlbrite3/BriteDatabase;Lcom/squareup/sqldelight/prerelease/SqlDelightQuery;Ljava/lang/Object;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static final ripenedCount(Lio/reactivex/Observable;Lio/reactivex/Scheduler;Lio/reactivex/disposables/CompositeDisposable;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/Observable<",
            "TT;>;",
            "Lio/reactivex/Scheduler;",
            "Lio/reactivex/disposables/CompositeDisposable;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-TT;+",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;>;)",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$ripenedCount"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "scheduler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "disposables"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "errorMessage"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "toRipenedCount"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "subscribeOn(scheduler)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v0, 0x7d0

    int-to-long v0, v0

    .line 197
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 196
    invoke-static {p0, v0, v1, v2, p1}, Lcom/squareup/util/rx2/Rx2TransformersKt;->adaptiveSample(Lio/reactivex/Observable;JLjava/util/concurrent/TimeUnit;Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p0

    .line 199
    new-instance p1, Lcom/squareup/queue/sqlite/QueueStoresKt$sam$io_reactivex_functions_Function$0;

    invoke-direct {p1, p4}, Lcom/squareup/queue/sqlite/QueueStoresKt$sam$io_reactivex_functions_Function$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    const/4 p1, 0x0

    .line 200
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-virtual {p0, p4}, Lio/reactivex/Observable;->defaultIfEmpty(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p0

    .line 201
    invoke-virtual {p0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p0

    const/4 p4, 0x1

    .line 202
    invoke-virtual {p0, p4}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p0

    .line 203
    new-instance p4, Lcom/squareup/queue/sqlite/QueueStoresKt$ripenedCount$1;

    invoke-direct {p4, p2}, Lcom/squareup/queue/sqlite/QueueStoresKt$ripenedCount$1;-><init>(Lio/reactivex/disposables/CompositeDisposable;)V

    check-cast p4, Lio/reactivex/functions/Consumer;

    invoke-virtual {p0, p1, p4}, Lio/reactivex/observables/ConnectableObservable;->autoConnect(ILio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p0

    .line 204
    new-instance p1, Lcom/squareup/queue/sqlite/QueueStoresKt$ripenedCount$2;

    invoke-direct {p1, p3}, Lcom/squareup/queue/sqlite/QueueStoresKt$ripenedCount$2;-><init>(Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/functions/Function;

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "subscribeOn(scheduler)\n \u2026ption(errorMessage, it) }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toBoolean(J)Z
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v2, 0x1

    const-wide/16 v4, 0x0

    cmp-long v6, v4, p0

    if-lez v6, :cond_0

    goto :goto_0

    :cond_0
    cmp-long v4, v2, p0

    if-ltz v4, :cond_1

    const/4 v4, 0x1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_3

    cmp-long v4, p0, v2

    if-nez v4, :cond_2

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    :goto_2
    return v0

    .line 234
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "QueueStores::toBoolean val is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p0, " but should being either 0 or 1)"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 233
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final toCount(Landroid/database/Cursor;Lcom/squareup/sqldelight/prerelease/RowMapper;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    const-string v0, "mapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    invoke-static {p0, p1}, Lcom/squareup/queue/sqlite/QueueStoresKt;->toEntry(Landroid/database/Cursor;Lcom/squareup/sqldelight/prerelease/RowMapper;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Long;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide p0

    long-to-int p1, p0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public static final toEntries(Landroid/database/Cursor;Lcom/squareup/sqldelight/prerelease/RowMapper;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/database/Cursor;",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "TT;>;)",
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "mapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    if-eqz p0, :cond_1

    .line 148
    move-object v1, p0

    check-cast v1, Ljava/io/Closeable;

    check-cast v0, Ljava/lang/Throwable;

    :try_start_0
    move-object v2, v1

    check-cast v2, Landroid/database/Cursor;

    .line 149
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    check-cast v2, Ljava/util/List;

    .line 150
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 151
    invoke-interface {p1, p0}, Lcom/squareup/sqldelight/prerelease/RowMapper;->map(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 148
    :cond_0
    invoke-static {v1, v0}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    move-object v0, v2

    goto :goto_1

    :catchall_0
    move-exception p0

    :try_start_1
    throw p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    invoke-static {v1, p0}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw p1

    :cond_1
    :goto_1
    return-object v0
.end method

.method public static final toEntry(Landroid/database/Cursor;Lcom/squareup/sqldelight/prerelease/RowMapper;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/database/Cursor;",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "TT;>;)TT;"
        }
    .end annotation

    const-string v0, "mapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    move-object v0, p0

    check-cast v0, Ljava/io/Closeable;

    const/4 v1, 0x0

    move-object v2, v1

    check-cast v2, Ljava/lang/Throwable;

    :try_start_0
    move-object v3, v0

    check-cast v3, Landroid/database/Cursor;

    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    invoke-interface {p1, p0}, Lcom/squareup/sqldelight/prerelease/RowMapper;->map(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-static {v0, v2}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    return-object v1

    :catchall_0
    move-exception p0

    :try_start_1
    throw p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception p1

    invoke-static {v0, p0}, Lkotlin/io/CloseableKt;->closeFinally(Ljava/io/Closeable;Ljava/lang/Throwable;)V

    throw p1
.end method

.method public static final toLong(Z)J
    .locals 2

    if-eqz p0, :cond_0

    const-wide/16 v0, 0x1

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0
.end method

.method public static final toStream(Landroid/database/Cursor;Lcom/squareup/sqldelight/prerelease/RowMapper;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/database/Cursor;",
            "Lcom/squareup/sqldelight/prerelease/RowMapper<",
            "TT;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "mapper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p0, :cond_0

    .line 158
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "Observable.empty()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 161
    :cond_0
    new-instance v0, Lcom/squareup/queue/sqlite/QueueStoresKt$toStream$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/sqlite/QueueStoresKt$toStream$1;-><init>(Landroid/database/Cursor;Lcom/squareup/sqldelight/prerelease/RowMapper;)V

    check-cast v0, Lio/reactivex/ObservableOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object p0

    const-string p1, "Observable\n        .crea\u2026(e)\n          }\n        }"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :goto_0
    return-object p0
.end method
