.class public final Lcom/squareup/queue/sqlite/QueueStores;
.super Ljava/lang/Object;
.source "QueueStores.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\u00020\u00048\u0006X\u0087T\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\u0005\u0010\u0002\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/queue/sqlite/QueueStores;",
        "",
        "()V",
        "RIPENED_PAYMENT_MINIMUM_MS",
        "",
        "RIPENED_PAYMENT_MINIMUM_MS$annotations",
        "queue_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/queue/sqlite/QueueStores;

.field public static final RIPENED_PAYMENT_MINIMUM_MS:I = 0x7d0


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/queue/sqlite/QueueStores;

    invoke-direct {v0}, Lcom/squareup/queue/sqlite/QueueStores;-><init>()V

    sput-object v0, Lcom/squareup/queue/sqlite/QueueStores;->INSTANCE:Lcom/squareup/queue/sqlite/QueueStores;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic RIPENED_PAYMENT_MINIMUM_MS$annotations()V
    .locals 0

    return-void
.end method
