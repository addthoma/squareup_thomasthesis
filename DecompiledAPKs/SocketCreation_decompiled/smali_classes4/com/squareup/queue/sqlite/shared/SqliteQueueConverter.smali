.class public interface abstract Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;
.super Ljava/lang/Object;
.source "SqliteQueueConverter.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        "Q:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0008\u0007\u0008f\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00022\u00020\u0002J\u001c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00010\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0005H&J\u0015\u0010\u0007\u001a\u00028\u00012\u0006\u0010\u0008\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\tJ\u0015\u0010\n\u001a\u00028\u00002\u0006\u0010\u000b\u001a\u00028\u0001H&\u00a2\u0006\u0002\u0010\t\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/queue/sqlite/shared/SqliteQueueConverter;",
        "S",
        "",
        "Q",
        "toQueueEntries",
        "",
        "storeEntries",
        "toQueueEntry",
        "storeEntry",
        "(Ljava/lang/Object;)Ljava/lang/Object;",
        "toStoreEntry",
        "queueEntry",
        "queue_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract toQueueEntries(Ljava/util/List;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+TS;>;)",
            "Ljava/util/List<",
            "TQ;>;"
        }
    .end annotation
.end method

.method public abstract toQueueEntry(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TS;)TQ;"
        }
    .end annotation
.end method

.method public abstract toStoreEntry(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TQ;)TS;"
        }
    .end annotation
.end method
