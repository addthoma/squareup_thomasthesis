.class public interface abstract Lcom/squareup/queue/PaymentTask;
.super Ljava/lang/Object;
.source "PaymentTask.java"

# interfaces
.implements Lcom/squareup/queue/LoggedInTransactionTask;
.implements Lcom/squareup/queue/PendingPayment;


# virtual methods
.method public abstract getAdjustments()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getItemizations()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTicketId()Ljava/lang/String;
.end method

.method public abstract getTime()J
.end method

.method public abstract getTotal()Lcom/squareup/protos/common/Money;
.end method
