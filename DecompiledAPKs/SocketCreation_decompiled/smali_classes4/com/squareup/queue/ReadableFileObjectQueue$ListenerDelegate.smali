.class Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;
.super Ljava/lang/Object;
.source "ReadableFileObjectQueue.java"

# interfaces
.implements Lcom/squareup/tape/ObjectQueue$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/ReadableFileObjectQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ListenerDelegate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/tape/ObjectQueue$Listener<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final delegate:Lcom/squareup/tape/ObjectQueue$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/tape/ObjectQueue$Listener<",
            "TT;>;"
        }
    .end annotation
.end field

.field private ignoreEvents:Z


# direct methods
.method constructor <init>(Lcom/squareup/tape/ObjectQueue$Listener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue$Listener<",
            "TT;>;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;->delegate:Lcom/squareup/tape/ObjectQueue$Listener;

    return-void
.end method


# virtual methods
.method public onAdd(Lcom/squareup/tape/ObjectQueue;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "TT;>;TT;)V"
        }
    .end annotation

    .line 51
    iget-boolean v0, p0, Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;->ignoreEvents:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;->delegate:Lcom/squareup/tape/ObjectQueue$Listener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lcom/squareup/tape/ObjectQueue$Listener;->onAdd(Lcom/squareup/tape/ObjectQueue;Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public onRemove(Lcom/squareup/tape/ObjectQueue;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue<",
            "TT;>;)V"
        }
    .end annotation

    .line 55
    iget-boolean v0, p0, Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;->ignoreEvents:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;->delegate:Lcom/squareup/tape/ObjectQueue$Listener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/squareup/tape/ObjectQueue$Listener;->onRemove(Lcom/squareup/tape/ObjectQueue;)V

    :cond_0
    return-void
.end method

.method setIgnoreEvents(Z)V
    .locals 0

    .line 47
    iput-boolean p1, p0, Lcom/squareup/queue/ReadableFileObjectQueue$ListenerDelegate;->ignoreEvents:Z

    return-void
.end method
