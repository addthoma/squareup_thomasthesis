.class public Lcom/squareup/queue/retrofit/RetrofitTaskQueue;
.super Lcom/squareup/tape/TaskQueue;
.source "RetrofitTaskQueue.java"

# interfaces
.implements Lcom/squareup/queue/retrofit/RetrofitQueue;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/tape/TaskQueue<",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        ">;",
        "Lcom/squareup/queue/retrofit/RetrofitQueue;"
    }
.end annotation


# instance fields
.field protected final backingFileQueue:Lcom/squareup/tape/FileObjectQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/tape/FileObjectQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation
.end field

.field private final closer:Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;


# direct methods
.method public constructor <init>(Lcom/squareup/tape/FileObjectQueue;Lcom/squareup/tape/TaskInjector;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/FileObjectQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0, p1, p2}, Lcom/squareup/tape/TaskQueue;-><init>(Lcom/squareup/tape/ObjectQueue;Lcom/squareup/tape/TaskInjector;)V

    .line 27
    iput-object p1, p0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->backingFileQueue:Lcom/squareup/tape/FileObjectQueue;

    .line 28
    new-instance p1, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;

    iget-object p2, p0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->backingFileQueue:Lcom/squareup/tape/FileObjectQueue;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/queue/retrofit/-$$Lambda$fgNtUBs_dh4w6BdZD9M4Z1kOrxs;

    invoke-direct {v0, p2}, Lcom/squareup/queue/retrofit/-$$Lambda$fgNtUBs_dh4w6BdZD9M4Z1kOrxs;-><init>(Lcom/squareup/tape/FileObjectQueue;)V

    invoke-direct {p1, v0}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;-><init>(Lrx/functions/Action0;)V

    iput-object p1, p0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->closer:Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;

    return-void
.end method


# virtual methods
.method public add(Lcom/squareup/queue/retrofit/RetrofitTask;)V
    .locals 0

    .line 42
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 43
    invoke-super {p0, p1}, Lcom/squareup/tape/TaskQueue;->add(Lcom/squareup/tape/Task;)V

    return-void
.end method

.method public bridge synthetic add(Lcom/squareup/tape/Task;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->add(Lcom/squareup/queue/retrofit/RetrofitTask;)V

    return-void
.end method

.method public bridge synthetic add(Ljava/lang/Object;)V
    .locals 0

    .line 18
    check-cast p1, Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->add(Lcom/squareup/queue/retrofit/RetrofitTask;)V

    return-void
.end method

.method public asList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 67
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 68
    iget-object v0, p0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->backingFileQueue:Lcom/squareup/tape/FileObjectQueue;

    invoke-virtual {v0}, Lcom/squareup/tape/FileObjectQueue;->asList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1

    .line 57
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 58
    iget-object v0, p0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->closer:Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;

    invoke-virtual {v0}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;->close()V

    return-void
.end method

.method public delayClose(Z)V
    .locals 1

    .line 62
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 63
    iget-object v0, p0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->closer:Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue$DelayableCloser;->delayClose(Z)V

    return-void
.end method

.method public peek()Lcom/squareup/queue/retrofit/RetrofitTask;
    .locals 1

    .line 32
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 33
    invoke-super {p0}, Lcom/squareup/tape/TaskQueue;->peek()Lcom/squareup/tape/Task;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitTask;

    return-object v0
.end method

.method public bridge synthetic peek()Lcom/squareup/tape/Task;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->peek()Lcom/squareup/queue/retrofit/RetrofitTask;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic peek()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;->peek()Lcom/squareup/queue/retrofit/RetrofitTask;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 0

    .line 47
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 48
    invoke-super {p0}, Lcom/squareup/tape/TaskQueue;->remove()V

    return-void
.end method

.method public setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue$Listener<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;)V"
        }
    .end annotation

    .line 52
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 53
    invoke-super {p0, p1}, Lcom/squareup/tape/TaskQueue;->setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V

    return-void
.end method

.method public size()I
    .locals 1

    .line 37
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 38
    invoke-super {p0}, Lcom/squareup/tape/TaskQueue;->size()I

    move-result v0

    return v0
.end method
