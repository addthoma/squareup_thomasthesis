.class public Lcom/squareup/queue/retrofit/RetrofitQueueFactory;
.super Ljava/lang/Object;
.source "RetrofitQueueFactory.java"

# interfaces
.implements Lcom/squareup/queue/retrofit/QueueFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/retrofit/QueueFactory<",
        "Lcom/squareup/queue/retrofit/RetrofitQueue;",
        ">;"
    }
.end annotation


# instance fields
.field protected final converter:Lcom/squareup/tape/FileObjectQueue$Converter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation
.end field

.field protected final taskInjector:Lcom/squareup/tape/TaskInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/queue/retrofit/RetrofitQueueFactory;->taskInjector:Lcom/squareup/tape/TaskInjector;

    .line 24
    iput-object p2, p0, Lcom/squareup/queue/retrofit/RetrofitQueueFactory;->converter:Lcom/squareup/tape/FileObjectQueue$Converter;

    return-void
.end method


# virtual methods
.method protected createFileQueue(Ljava/io/File;)Lcom/squareup/tape/FileObjectQueue;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lcom/squareup/tape/FileObjectQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;"
        }
    .end annotation

    .line 38
    :try_start_0
    new-instance v0, Lcom/squareup/tape/FileObjectQueue;

    iget-object v1, p0, Lcom/squareup/queue/retrofit/RetrofitQueueFactory;->converter:Lcom/squareup/tape/FileObjectQueue$Converter;

    invoke-direct {v0, p1, v1}, Lcom/squareup/tape/FileObjectQueue;-><init>(Ljava/io/File;Lcom/squareup/tape/FileObjectQueue$Converter;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    .line 40
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    :try_start_1
    new-instance v0, Lcom/squareup/tape/FileObjectQueue;

    iget-object v1, p0, Lcom/squareup/queue/retrofit/RetrofitQueueFactory;->converter:Lcom/squareup/tape/FileObjectQueue$Converter;

    invoke-direct {v0, p1, v1}, Lcom/squareup/tape/FileObjectQueue;-><init>(Ljava/io/File;Lcom/squareup/tape/FileObjectQueue$Converter;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    return-object v0

    :catch_1
    move-exception p1

    .line 44
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Still unable to open queue after delete and retry"

    invoke-direct {v0, v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 47
    :cond_0
    new-instance v1, Lcom/squareup/tape/FileException;

    const-string v2, "Failed to recreate corrupt QueueFile."

    invoke-direct {v1, v2, v0, p1}, Lcom/squareup/tape/FileException;-><init>(Ljava/lang/String;Ljava/io/IOException;Ljava/io/File;)V

    throw v1
.end method

.method public open(Ljava/io/File;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 2

    .line 28
    invoke-virtual {p0, p1}, Lcom/squareup/queue/retrofit/RetrofitQueueFactory;->createFileQueue(Ljava/io/File;)Lcom/squareup/tape/FileObjectQueue;

    move-result-object p1

    .line 29
    new-instance v0, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;

    iget-object v1, p0, Lcom/squareup/queue/retrofit/RetrofitQueueFactory;->taskInjector:Lcom/squareup/tape/TaskInjector;

    invoke-direct {v0, p1, v1}, Lcom/squareup/queue/retrofit/RetrofitTaskQueue;-><init>(Lcom/squareup/tape/FileObjectQueue;Lcom/squareup/tape/TaskInjector;)V

    return-object v0
.end method

.method public bridge synthetic open(Ljava/io/File;)Lcom/squareup/tape/ObjectQueue;
    .locals 0

    .line 17
    invoke-virtual {p0, p1}, Lcom/squareup/queue/retrofit/RetrofitQueueFactory;->open(Ljava/io/File;)Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object p1

    return-object p1
.end method
