.class public Lcom/squareup/queue/EnqueueSkipReceipt;
.super Lcom/squareup/queue/PostPaymentTask;
.source "EnqueueSkipReceipt.java"


# static fields
.field private static final serialVersionUID:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/squareup/queue/PostPaymentTask;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 24
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/EnqueueSkipReceipt;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 6
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/EnqueueSkipReceipt;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method protected taskFor(Ljava/lang/String;)Lcom/squareup/queue/retrofit/RetrofitTask;
    .locals 1

    .line 10
    new-instance v0, Lcom/squareup/queue/SkipReceipt$Builder;

    invoke-direct {v0}, Lcom/squareup/queue/SkipReceipt$Builder;-><init>()V

    .line 11
    invoke-virtual {v0, p1}, Lcom/squareup/queue/SkipReceipt$Builder;->paymentId(Ljava/lang/String;)Lcom/squareup/queue/SkipReceipt$Builder;

    move-result-object p1

    .line 12
    invoke-virtual {p1}, Lcom/squareup/queue/SkipReceipt$Builder;->build()Lcom/squareup/queue/SkipReceipt;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "EnqueueSkipReceipt{}"

    return-object v0
.end method
