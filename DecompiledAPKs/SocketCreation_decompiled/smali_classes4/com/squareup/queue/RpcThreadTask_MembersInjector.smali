.class public final Lcom/squareup/queue/RpcThreadTask_MembersInjector;
.super Ljava/lang/Object;
.source "RpcThreadTask_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "U:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/RpcThreadTask<",
        "TT;TU;>;>;"
    }
.end annotation


# instance fields
.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/RpcThreadTask<",
            "TT;TU;>;>;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/queue/RpcThreadTask_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectMainScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V
    .locals 0
    .annotation runtime Lcom/squareup/thread/Main;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/queue/RpcThreadTask<",
            "TT;TU;>;",
            "Lrx/Scheduler;",
            ")V"
        }
    .end annotation

    .line 44
    iput-object p1, p0, Lcom/squareup/queue/RpcThreadTask;->mainScheduler:Lrx/Scheduler;

    return-void
.end method

.method public static injectRpcScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V
    .locals 0
    .annotation runtime Lcom/squareup/thread/Rpc;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/queue/RpcThreadTask<",
            "TT;TU;>;",
            "Lrx/Scheduler;",
            ")V"
        }
    .end annotation

    .line 51
    iput-object p1, p0, Lcom/squareup/queue/RpcThreadTask;->rpcScheduler:Lrx/Scheduler;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/RpcThreadTask;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/RpcThreadTask<",
            "TT;TU;>;)V"
        }
    .end annotation

    .line 36
    iget-object v0, p0, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectMainScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectRpcScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/queue/RpcThreadTask;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectMembers(Lcom/squareup/queue/RpcThreadTask;)V

    return-void
.end method
