.class public abstract Lcom/squareup/queue/PostPaymentTask;
.super Ljava/lang/Object;
.source "PostPaymentTask.java"

# interfaces
.implements Lcom/squareup/queue/LoggedInTransactionTask;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field protected final billOrPaymentId:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected final customerId:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field transient lastCapturePaymentId:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;
    .annotation runtime Lcom/squareup/settings/LastLocalPaymentServerId;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field protected final paymentType:Lcom/squareup/PaymentType;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field transient taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    sget-object v0, Lcom/squareup/PaymentType;->BILL2:Lcom/squareup/PaymentType;

    iput-object v0, p0, Lcom/squareup/queue/PostPaymentTask;->paymentType:Lcom/squareup/PaymentType;

    const/4 v0, 0x0

    .line 49
    iput-object v0, p0, Lcom/squareup/queue/PostPaymentTask;->billOrPaymentId:Ljava/lang/String;

    .line 56
    iput-object v0, p0, Lcom/squareup/queue/PostPaymentTask;->customerId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/queue/PostPaymentTask;->billOrPaymentId:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 66
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/queue/PostPaymentTask;->getPaymentType()Lcom/squareup/PaymentType;

    move-result-object v0

    sget-object v1, Lcom/squareup/PaymentType;->CARD:Lcom/squareup/PaymentType;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/queue/PostPaymentTask;->lastCapturePaymentId:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/queue/PostPaymentTask;->lastLocalPaymentServerId:Lcom/squareup/settings/LocalSetting;

    .line 67
    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 70
    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_2

    new-array v0, v3, [Ljava/lang/Object;

    aput-object p0, v0, v2

    const-string v1, "Missing payment ID! Skipping %s."

    .line 72
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 75
    :cond_2
    iget-object v1, p0, Lcom/squareup/queue/PostPaymentTask;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-virtual {p0, v0}, Lcom/squareup/queue/PostPaymentTask;->taskFor(Ljava/lang/String;)Lcom/squareup/queue/retrofit/RetrofitTask;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    const/4 v2, 0x1

    .line 79
    :goto_1
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    invoke-direct {v0, v2}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 25
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/PostPaymentTask;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method protected getPaymentType()Lcom/squareup/PaymentType;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/squareup/queue/PostPaymentTask;->paymentType:Lcom/squareup/PaymentType;

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/PaymentType;->CASH:Lcom/squareup/PaymentType;

    :cond_0
    return-object v0
.end method

.method protected abstract taskFor(Ljava/lang/String;)Lcom/squareup/queue/retrofit/RetrofitTask;
.end method
