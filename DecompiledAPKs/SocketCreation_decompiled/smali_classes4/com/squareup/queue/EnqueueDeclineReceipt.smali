.class public Lcom/squareup/queue/EnqueueDeclineReceipt;
.super Lcom/squareup/queue/PostPaymentTask;
.source "EnqueueDeclineReceipt.java"


# static fields
.field private static final serialVersionUID:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Lcom/squareup/queue/PostPaymentTask;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 20
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/EnqueueDeclineReceipt;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 6
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/EnqueueDeclineReceipt;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method protected taskFor(Ljava/lang/String;)Lcom/squareup/queue/retrofit/RetrofitTask;
    .locals 2

    .line 10
    new-instance v0, Lcom/squareup/queue/DeclineReceipt$Builder;

    invoke-direct {v0}, Lcom/squareup/queue/DeclineReceipt$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/queue/EnqueueDeclineReceipt;->paymentType:Lcom/squareup/PaymentType;

    .line 11
    invoke-virtual {v0, p1, v1}, Lcom/squareup/queue/DeclineReceipt$Builder;->paymentIdOrLegacyBillId(Ljava/lang/String;Lcom/squareup/PaymentType;)Lcom/squareup/queue/DeclineReceipt$Builder;

    move-result-object p1

    .line 12
    invoke-virtual {p1}, Lcom/squareup/queue/DeclineReceipt$Builder;->build()Lcom/squareup/queue/DeclineReceipt;

    move-result-object p1

    return-object p1
.end method
