.class public abstract Lcom/squareup/queue/UploadFailure;
.super Ljava/lang/Object;
.source "UploadFailure.java"

# interfaces
.implements Lcom/squareup/queue/LoggedInTransactionTask;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/queue/UploadFailure$ForSettle;,
        Lcom/squareup/queue/UploadFailure$ForCapture;,
        Lcom/squareup/queue/UploadFailure$CaptureFailureException;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/squareup/queue/retrofit/RetrofitTask;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/queue/LoggedInTransactionTask;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private failure:Lcom/squareup/queue/retrofit/RetrofitTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field transient gson:Lcom/google/gson/Gson;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient ohSnapLogger:Lcom/squareup/log/OhSnapLogger;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/squareup/queue/retrofit/RetrofitTask;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/queue/UploadFailure;->failure:Lcom/squareup/queue/retrofit/RetrofitTask;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/queue/retrofit/RetrofitTask;Lcom/squareup/queue/UploadFailure$1;)V
    .locals 0

    .line 13
    invoke-direct {p0, p1}, Lcom/squareup/queue/UploadFailure;-><init>(Lcom/squareup/queue/retrofit/RetrofitTask;)V

    return-void
.end method


# virtual methods
.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/queue/UploadFailure;->getMessagePrefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/UploadFailure;->gson:Lcom/google/gson/Gson;

    iget-object v2, p0, Lcom/squareup/queue/UploadFailure;->failure:Lcom/squareup/queue/retrofit/RetrofitTask;

    invoke-virtual {v1, v2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/squareup/queue/UploadFailure;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    sget-object v2, Lcom/squareup/log/OhSnapLogger$EventType;->CAPTURE_FAILURE:Lcom/squareup/log/OhSnapLogger$EventType;

    invoke-interface {v1, v2, v0}, Lcom/squareup/log/OhSnapLogger;->logFull(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    .line 37
    new-instance v0, Lcom/squareup/queue/UploadFailure$CaptureFailureException;

    invoke-direct {v0}, Lcom/squareup/queue/UploadFailure$CaptureFailureException;-><init>()V

    invoke-static {v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 38
    new-instance v0, Lcom/squareup/server/SimpleResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    invoke-virtual {p1, v0}, Lcom/squareup/server/SquareCallback;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/UploadFailure;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method getFailure()Lcom/squareup/queue/retrofit/RetrofitTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 30
    iget-object v0, p0, Lcom/squareup/queue/UploadFailure;->failure:Lcom/squareup/queue/retrofit/RetrofitTask;

    return-object v0
.end method

.method abstract getMessagePrefix()Ljava/lang/String;
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method
