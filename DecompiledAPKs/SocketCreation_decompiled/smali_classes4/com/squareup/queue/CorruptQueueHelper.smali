.class public Lcom/squareup/queue/CorruptQueueHelper;
.super Ljava/lang/Object;
.source "CorruptQueueHelper.java"


# instance fields
.field private final corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;


# direct methods
.method public constructor <init>(Lcom/squareup/queue/CorruptQueueRecorder;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/squareup/queue/CorruptQueueHelper;->corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

    return-void
.end method


# virtual methods
.method public allowInSqliteQueue(Lcom/squareup/payment/offline/StoredPayment;)Z
    .locals 0

    if-eqz p1, :cond_0

    .line 57
    iget-object p1, p0, Lcom/squareup/queue/CorruptQueueHelper;->corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

    invoke-virtual {p1}, Lcom/squareup/queue/CorruptQueueRecorder;->hasCorruptQueue()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public allowInSqliteQueue(Lcom/squareup/queue/retrofit/RetrofitTask;)Z
    .locals 0

    if-eqz p1, :cond_0

    .line 45
    instance-of p1, p1, Lcom/squareup/queue/NoOp;

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/queue/CorruptQueueHelper;->corruptQueueRecorder:Lcom/squareup/queue/CorruptQueueRecorder;

    invoke-virtual {p1}, Lcom/squareup/queue/CorruptQueueRecorder;->hasCorruptQueue()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public corruptRetrofitTaskPlaceholder()Lcom/squareup/queue/retrofit/RetrofitTask;
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/queue/NoOp;

    invoke-direct {v0}, Lcom/squareup/queue/NoOp;-><init>()V

    return-object v0
.end method

.method public corruptStoredPaymentPlaceholder()Lcom/squareup/payment/offline/StoredPayment;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
