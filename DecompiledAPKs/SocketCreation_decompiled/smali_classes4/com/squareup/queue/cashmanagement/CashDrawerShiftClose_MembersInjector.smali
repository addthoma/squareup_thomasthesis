.class public final Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose_MembersInjector;
.super Ljava/lang/Object;
.source "CashDrawerShiftClose_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose;",
        ">;"
    }
.end annotation


# instance fields
.field private final cashManagementServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final rpcSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose_MembersInjector;->cashManagementServiceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/cashmanagement/CashManagementService;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose;",
            ">;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectCashManagementService(Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose;Lcom/squareup/server/cashmanagement/CashManagementService;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose;->cashManagementService:Lcom/squareup/server/cashmanagement/CashManagementService;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose_MembersInjector;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectMainScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose_MembersInjector;->rpcSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lrx/Scheduler;

    invoke-static {p1, v0}, Lcom/squareup/queue/RpcThreadTask_MembersInjector;->injectRpcScheduler(Lcom/squareup/queue/RpcThreadTask;Lrx/Scheduler;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose_MembersInjector;->cashManagementServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/cashmanagement/CashManagementService;

    invoke-static {p1, v0}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose_MembersInjector;->injectCashManagementService(Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose;Lcom/squareup/server/cashmanagement/CashManagementService;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 11
    check-cast p1, Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose_MembersInjector;->injectMembers(Lcom/squareup/queue/cashmanagement/CashDrawerShiftClose;)V

    return-void
.end method
