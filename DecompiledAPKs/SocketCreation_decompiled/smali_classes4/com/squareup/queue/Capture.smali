.class public Lcom/squareup/queue/Capture;
.super Ljava/lang/Object;
.source "Capture.java"

# interfaces
.implements Lcom/squareup/queue/CaptureTask;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private adjustmentList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final adjustments:Ljava/lang/String;

.field transient afterCapture:Lcom/squareup/queue/AfterCapture;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final authorizationId:Ljava/lang/String;

.field private final captureType:Lcom/squareup/server/payment/CaptureType;

.field private cardBrand:Lcom/squareup/Card$Brand;

.field private final customerPresence:Lcom/squareup/server/payment/CustomerPresence;

.field private failed:Z

.field private itemizationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private final itemizations:Ljava/lang/String;

.field private final itemsModel:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;"
        }
    .end annotation
.end field

.field transient lastCapturePaymentId:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient messageQueue:Lcom/squareup/util/SquareMessageQueue;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field transient paymentService:Lcom/squareup/server/payment/PaymentService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final receipt:Lcom/squareup/queue/FutureReceipt;

.field private final receiptNote:Ljava/lang/String;

.field private final signatureData:Ljava/lang/String;

.field transient taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final tax:Lcom/squareup/Money;

.field private final taxMoney:Lcom/squareup/protos/common/Money;

.field private final ticketId:Ljava/lang/String;

.field transient tickets:Lcom/squareup/tickets/Tickets;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final time:J

.field private final tip:Lcom/squareup/Money;

.field private final tipMoney:Lcom/squareup/protos/common/Money;

.field private final total:Lcom/squareup/Money;

.field private final totalMoney:Lcom/squareup/protos/common/Money;

.field transient transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final unmaskedDigits:Ljava/lang/String;

.field private final uuid:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/squareup/queue/Capture;Z)V
    .locals 3

    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    iget-object v0, p1, Lcom/squareup/queue/Capture;->authorizationId:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/queue/Capture;->authorizationId:Ljava/lang/String;

    .line 205
    iget-object v0, p1, Lcom/squareup/queue/Capture;->captureType:Lcom/squareup/server/payment/CaptureType;

    iput-object v0, p0, Lcom/squareup/queue/Capture;->captureType:Lcom/squareup/server/payment/CaptureType;

    .line 206
    iget-object v0, p1, Lcom/squareup/queue/Capture;->totalMoney:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/queue/Capture;->totalMoney:Lcom/squareup/protos/common/Money;

    .line 207
    iget-object v0, p1, Lcom/squareup/queue/Capture;->tipMoney:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/queue/Capture;->tipMoney:Lcom/squareup/protos/common/Money;

    .line 208
    iget-object v0, p1, Lcom/squareup/queue/Capture;->taxMoney:Lcom/squareup/protos/common/Money;

    iput-object v0, p0, Lcom/squareup/queue/Capture;->taxMoney:Lcom/squareup/protos/common/Money;

    .line 209
    iget-object v0, p1, Lcom/squareup/queue/Capture;->adjustmentList:Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/queue/Capture;->adjustmentList:Ljava/util/List;

    .line 210
    iget-object v0, p1, Lcom/squareup/queue/Capture;->itemizationList:Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/queue/Capture;->itemizationList:Ljava/util/List;

    .line 211
    iget-object v0, p1, Lcom/squareup/queue/Capture;->cardBrand:Lcom/squareup/Card$Brand;

    iput-object v0, p0, Lcom/squareup/queue/Capture;->cardBrand:Lcom/squareup/Card$Brand;

    .line 212
    iget-object v0, p1, Lcom/squareup/queue/Capture;->unmaskedDigits:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/queue/Capture;->unmaskedDigits:Ljava/lang/String;

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 213
    iget-object v1, p1, Lcom/squareup/queue/Capture;->receiptNote:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "REDACTED_receiptNote"

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    iput-object v1, p0, Lcom/squareup/queue/Capture;->receiptNote:Ljava/lang/String;

    if-eqz p2, :cond_1

    .line 214
    iget-object p2, p1, Lcom/squareup/queue/Capture;->signatureData:Ljava/lang/String;

    if-eqz p2, :cond_1

    const-string p2, "REDACTED_signatureData"

    goto :goto_1

    :cond_1
    move-object p2, v0

    :goto_1
    iput-object p2, p0, Lcom/squareup/queue/Capture;->signatureData:Ljava/lang/String;

    .line 216
    iget-wide v1, p1, Lcom/squareup/queue/Capture;->time:J

    iput-wide v1, p0, Lcom/squareup/queue/Capture;->time:J

    .line 217
    iget-object p2, p1, Lcom/squareup/queue/Capture;->customerPresence:Lcom/squareup/server/payment/CustomerPresence;

    iput-object p2, p0, Lcom/squareup/queue/Capture;->customerPresence:Lcom/squareup/server/payment/CustomerPresence;

    .line 218
    iput-object v0, p0, Lcom/squareup/queue/Capture;->itemsModel:Ljava/util/List;

    .line 220
    iput-object v0, p0, Lcom/squareup/queue/Capture;->receipt:Lcom/squareup/queue/FutureReceipt;

    .line 221
    iget-object p2, p1, Lcom/squareup/queue/Capture;->adjustments:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/queue/Capture;->adjustments:Ljava/lang/String;

    .line 222
    iget-object p2, p1, Lcom/squareup/queue/Capture;->itemizations:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/queue/Capture;->itemizations:Ljava/lang/String;

    .line 223
    iget-boolean p2, p1, Lcom/squareup/queue/Capture;->failed:Z

    iput-boolean p2, p0, Lcom/squareup/queue/Capture;->failed:Z

    .line 224
    iget-object p2, p1, Lcom/squareup/queue/Capture;->total:Lcom/squareup/Money;

    iput-object p2, p0, Lcom/squareup/queue/Capture;->total:Lcom/squareup/Money;

    .line 225
    iget-object p2, p1, Lcom/squareup/queue/Capture;->tip:Lcom/squareup/Money;

    iput-object p2, p0, Lcom/squareup/queue/Capture;->tip:Lcom/squareup/Money;

    .line 226
    iget-object p2, p1, Lcom/squareup/queue/Capture;->tax:Lcom/squareup/Money;

    iput-object p2, p0, Lcom/squareup/queue/Capture;->tax:Lcom/squareup/Money;

    .line 227
    iget-object p2, p1, Lcom/squareup/queue/Capture;->ticketId:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/queue/Capture;->ticketId:Ljava/lang/String;

    .line 228
    iget-object p1, p1, Lcom/squareup/queue/Capture;->uuid:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/queue/Capture;->uuid:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/Card$Brand;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/squareup/server/payment/CustomerPresence;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Lcom/squareup/Card$Brand;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/squareup/server/payment/CustomerPresence;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    if-nez p6, :cond_0

    .line 151
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v6, v0

    goto :goto_0

    :cond_0
    move-object/from16 v6, p6

    :goto_0
    if-nez p7, :cond_1

    .line 152
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v7, v0

    goto :goto_1

    :cond_1
    move-object/from16 v7, p7

    :goto_1
    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-wide/from16 v12, p12

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v18, p16

    move-object/from16 v19, p17

    .line 150
    invoke-direct/range {v0 .. v19}, Lcom/squareup/queue/Capture;-><init>(Ljava/lang/String;ZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/Card$Brand;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/squareup/server/payment/CustomerPresence;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/Card$Brand;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/squareup/server/payment/CustomerPresence;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Lcom/squareup/Card$Brand;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/squareup/server/payment/CustomerPresence;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 173
    iput-object v1, v0, Lcom/squareup/queue/Capture;->authorizationId:Ljava/lang/String;

    if-eqz p2, :cond_0

    .line 174
    sget-object v1, Lcom/squareup/server/payment/CaptureType;->CAPTURE_HUMAN_INITIATED:Lcom/squareup/server/payment/CaptureType;

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/server/payment/CaptureType;->CAPTURE_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/server/payment/CaptureType;

    :goto_0
    iput-object v1, v0, Lcom/squareup/queue/Capture;->captureType:Lcom/squareup/server/payment/CaptureType;

    move-object v1, p3

    .line 175
    iput-object v1, v0, Lcom/squareup/queue/Capture;->totalMoney:Lcom/squareup/protos/common/Money;

    move-object v1, p4

    .line 176
    iput-object v1, v0, Lcom/squareup/queue/Capture;->tipMoney:Lcom/squareup/protos/common/Money;

    move-object v1, p5

    .line 177
    iput-object v1, v0, Lcom/squareup/queue/Capture;->taxMoney:Lcom/squareup/protos/common/Money;

    move-object v1, p6

    .line 178
    iput-object v1, v0, Lcom/squareup/queue/Capture;->adjustmentList:Ljava/util/List;

    move-object v1, p7

    .line 179
    iput-object v1, v0, Lcom/squareup/queue/Capture;->itemizationList:Ljava/util/List;

    move-object v1, p8

    .line 180
    iput-object v1, v0, Lcom/squareup/queue/Capture;->cardBrand:Lcom/squareup/Card$Brand;

    move-object v1, p9

    .line 181
    iput-object v1, v0, Lcom/squareup/queue/Capture;->unmaskedDigits:Ljava/lang/String;

    move-object v1, p10

    .line 182
    iput-object v1, v0, Lcom/squareup/queue/Capture;->receiptNote:Ljava/lang/String;

    move-object v1, p11

    .line 183
    iput-object v1, v0, Lcom/squareup/queue/Capture;->signatureData:Ljava/lang/String;

    move-wide v1, p12

    .line 184
    iput-wide v1, v0, Lcom/squareup/queue/Capture;->time:J

    move-object/from16 v1, p14

    .line 185
    iput-object v1, v0, Lcom/squareup/queue/Capture;->customerPresence:Lcom/squareup/server/payment/CustomerPresence;

    move-object/from16 v1, p15

    .line 186
    iput-object v1, v0, Lcom/squareup/queue/Capture;->itemsModel:Ljava/util/List;

    const/4 v1, 0x0

    .line 188
    iput-object v1, v0, Lcom/squareup/queue/Capture;->receipt:Lcom/squareup/queue/FutureReceipt;

    move-object/from16 v2, p16

    .line 189
    iput-object v2, v0, Lcom/squareup/queue/Capture;->adjustments:Ljava/lang/String;

    move-object/from16 v2, p17

    .line 190
    iput-object v2, v0, Lcom/squareup/queue/Capture;->itemizations:Ljava/lang/String;

    .line 191
    iput-object v1, v0, Lcom/squareup/queue/Capture;->tax:Lcom/squareup/Money;

    iput-object v1, v0, Lcom/squareup/queue/Capture;->tip:Lcom/squareup/Money;

    iput-object v1, v0, Lcom/squareup/queue/Capture;->total:Lcom/squareup/Money;

    move-object/from16 v1, p18

    .line 192
    iput-object v1, v0, Lcom/squareup/queue/Capture;->ticketId:Ljava/lang/String;

    move-object/from16 v1, p19

    .line 193
    iput-object v1, v0, Lcom/squareup/queue/Capture;->uuid:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/queue/Capture;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/squareup/queue/Capture;->uploadFailedCapture()V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/queue/Capture;)Lcom/squareup/queue/FutureReceipt;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/queue/Capture;->receipt:Lcom/squareup/queue/FutureReceipt;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/queue/Capture;)Ljava/lang/String;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/queue/Capture;->signatureData:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$302(Lcom/squareup/queue/Capture;Z)Z
    .locals 0

    .line 52
    iput-boolean p1, p0, Lcom/squareup/queue/Capture;->failed:Z

    return p1
.end method

.method static synthetic access$400(Lcom/squareup/queue/Capture;)Ljava/lang/String;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/queue/Capture;->ticketId:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/queue/Capture;)Ljava/lang/String;
    .locals 0

    .line 52
    iget-object p0, p0, Lcom/squareup/queue/Capture;->uuid:Ljava/lang/String;

    return-object p0
.end method

.method static legacyCaptureForTesting(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/Card$Brand;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/squareup/server/payment/CustomerPresence;)Lcom/squareup/queue/Capture;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Lcom/squareup/Card$Brand;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Lcom/squareup/server/payment/CustomerPresence;",
            ")",
            "Lcom/squareup/queue/Capture;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-wide/from16 v12, p10

    move-object/from16 v14, p12

    .line 162
    new-instance v20, Lcom/squareup/queue/Capture;

    move-object/from16 v0, v20

    .line 164
    invoke-static {}, Lcom/squareup/RegisterGsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v2

    move-object/from16 v6, p4

    invoke-virtual {v2, v6}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 165
    invoke-static {}, Lcom/squareup/RegisterGsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v2

    move-object/from16 v6, p5

    invoke-virtual {v2, v6}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v15, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-direct/range {v0 .. v19}, Lcom/squareup/queue/Capture;-><init>(Ljava/lang/String;ZLcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Ljava/util/List;Ljava/util/List;Lcom/squareup/Card$Brand;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/squareup/server/payment/CustomerPresence;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v20
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .line 458
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 462
    iget-object p1, p0, Lcom/squareup/queue/Capture;->cardBrand:Lcom/squareup/Card$Brand;

    if-nez p1, :cond_0

    .line 463
    sget-object p1, Lcom/squareup/Card$Brand;->UNKNOWN:Lcom/squareup/Card$Brand;

    iput-object p1, p0, Lcom/squareup/queue/Capture;->cardBrand:Lcom/squareup/Card$Brand;

    :cond_0
    return-void
.end method

.method private uploadFailedCapture()V
    .locals 2

    .line 425
    iget-object v0, p0, Lcom/squareup/queue/Capture;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v1, Lcom/squareup/queue/UploadFailure$ForCapture;

    invoke-direct {v1, p0}, Lcom/squareup/queue/UploadFailure$ForCapture;-><init>(Lcom/squareup/queue/Capture;)V

    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public asBill(Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    const/4 v0, 0x1

    .line 429
    invoke-static {p0, p1, v0}, Lcom/squareup/activity/BillHistoryHelper;->asCaptureBuilder(Lcom/squareup/queue/Capture;Lcom/squareup/util/Res;Z)Lcom/squareup/billhistory/model/BillHistory$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    return-object p1
.end method

.method public createMessage()Lcom/squareup/server/payment/CaptureMessage;
    .locals 19

    move-object/from16 v0, p0

    .line 410
    iget-object v1, v0, Lcom/squareup/queue/Capture;->authorizationId:Ljava/lang/String;

    .line 411
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/queue/Capture;->getAdjustments()Ljava/util/List;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/queue/Capture;->getItemizations()Ljava/util/List;

    move-result-object v3

    iget-object v4, v0, Lcom/squareup/queue/Capture;->receiptNote:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/queue/Capture;->itemsModel:Ljava/util/List;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/squareup/server/payment/ItemizationsMessage;->forCapture(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Lcom/squareup/server/payment/ItemizationsMessage;

    move-result-object v13

    .line 414
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/queue/Capture;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 415
    new-instance v2, Lcom/squareup/server/payment/CaptureMessage;

    iget-object v3, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    iget-object v9, v0, Lcom/squareup/queue/Capture;->authorizationId:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/queue/Capture;->getCaptureType()Lcom/squareup/server/payment/CaptureType;

    move-result-object v10

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->currency_code:Lcom/squareup/protos/common/CurrencyCode;

    .line 416
    invoke-virtual {v1}, Lcom/squareup/protos/common/CurrencyCode;->name()Ljava/lang/String;

    move-result-object v11

    iget-object v12, v0, Lcom/squareup/queue/Capture;->customerPresence:Lcom/squareup/server/payment/CustomerPresence;

    new-instance v1, Ljava/util/Date;

    iget-wide v3, v0, Lcom/squareup/queue/Capture;->time:J

    invoke-direct {v1, v3, v4}, Ljava/util/Date;-><init>(J)V

    .line 417
    invoke-static {v1}, Lcom/squareup/util/Times;->asIso8601(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/queue/Capture;->getTax()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v15

    invoke-virtual/range {p0 .. p0}, Lcom/squareup/queue/Capture;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v17

    move-object v6, v2

    invoke-direct/range {v6 .. v18}, Lcom/squareup/server/payment/CaptureMessage;-><init>(JLjava/lang/String;Lcom/squareup/server/payment/CaptureType;Ljava/lang/String;Lcom/squareup/server/payment/CustomerPresence;Lcom/squareup/server/payment/ItemizationsMessage;Ljava/lang/String;JJ)V

    return-object v2
.end method

.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 343
    iget-object v0, p0, Lcom/squareup/queue/Capture;->lastCapturePaymentId:Lcom/squareup/settings/LocalSetting;

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 345
    iget-object v0, p0, Lcom/squareup/queue/Capture;->authorizationId:Ljava/lang/String;

    invoke-static {p0, v0, p1}, Lcom/squareup/payment/offline/StoreAndForwardUtils;->clientErrorIfStoreAndForwardPayment(Lcom/squareup/queue/retrofit/RetrofitTask;Ljava/lang/String;Lcom/squareup/server/SquareCallback;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 347
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/queue/Capture;->createMessage()Lcom/squareup/server/payment/CaptureMessage;

    move-result-object v0

    .line 349
    iget-object v1, p0, Lcom/squareup/queue/Capture;->paymentService:Lcom/squareup/server/payment/PaymentService;

    new-instance v2, Lcom/squareup/queue/Capture$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/queue/Capture$1;-><init>(Lcom/squareup/queue/Capture;Lcom/squareup/server/SquareCallback;)V

    invoke-interface {v1, v0, v2}, Lcom/squareup/server/payment/PaymentService;->capture(Lcom/squareup/server/payment/CaptureMessage;Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 52
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/Capture;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method public getAdjustments()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;"
        }
    .end annotation

    .line 304
    iget-object v0, p0, Lcom/squareup/queue/Capture;->adjustmentList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 305
    iget-object v0, p0, Lcom/squareup/queue/Capture;->adjustments:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/Capture;->adjustmentList:Ljava/util/List;

    goto :goto_0

    .line 309
    :cond_0
    invoke-static {}, Lcom/squareup/RegisterGsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/Capture;->adjustments:Ljava/lang/String;

    invoke-static {}, Lcom/squareup/queue/Itemize;->adjustmentListType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/queue/Capture;->adjustmentList:Ljava/util/List;

    .line 313
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/queue/Capture;->adjustmentList:Ljava/util/List;

    return-object v0
.end method

.method public getAuthorizationId()Ljava/lang/String;
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/squareup/queue/Capture;->authorizationId:Ljava/lang/String;

    return-object v0
.end method

.method public getCaptureType()Lcom/squareup/server/payment/CaptureType;
    .locals 1

    .line 241
    iget-object v0, p0, Lcom/squareup/queue/Capture;->captureType:Lcom/squareup/server/payment/CaptureType;

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/server/payment/CaptureType;->CAPTURE_HUMAN_INITIATED:Lcom/squareup/server/payment/CaptureType;

    :cond_0
    return-object v0
.end method

.method public getCardBrand()Lcom/squareup/Card$Brand;
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/squareup/queue/Capture;->cardBrand:Lcom/squareup/Card$Brand;

    return-object v0
.end method

.method public getItemizations()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;"
        }
    .end annotation

    .line 319
    iget-object v0, p0, Lcom/squareup/queue/Capture;->itemizationList:Ljava/util/List;

    if-nez v0, :cond_1

    .line 320
    iget-object v0, p0, Lcom/squareup/queue/Capture;->itemizations:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/queue/Capture;->itemizationList:Ljava/util/List;

    goto :goto_0

    .line 324
    :cond_0
    invoke-static {}, Lcom/squareup/RegisterGsonProvider;->gson()Lcom/google/gson/Gson;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/queue/Capture;->itemizations:Ljava/lang/String;

    invoke-static {}, Lcom/squareup/queue/Itemize;->itemizationListType()Ljava/lang/reflect/Type;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/queue/Capture;->itemizationList:Ljava/util/List;

    .line 328
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/queue/Capture;->itemizationList:Ljava/util/List;

    return-object v0
.end method

.method public getItemsModel()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;"
        }
    .end annotation

    .line 332
    iget-object v0, p0, Lcom/squareup/queue/Capture;->itemsModel:Ljava/util/List;

    return-object v0
.end method

.method public getReceiptNote()Ljava/lang/String;
    .locals 1

    .line 272
    iget-object v0, p0, Lcom/squareup/queue/Capture;->receiptNote:Ljava/lang/String;

    return-object v0
.end method

.method public getSignatureData()Ljava/lang/String;
    .locals 1

    .line 256
    iget-object v0, p0, Lcom/squareup/queue/Capture;->signatureData:Ljava/lang/String;

    return-object v0
.end method

.method public getTax()Lcom/squareup/protos/common/Money;
    .locals 3

    .line 264
    iget-object v0, p0, Lcom/squareup/queue/Capture;->taxMoney:Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/queue/Capture;->tax:Lcom/squareup/Money;

    invoke-static {v0, v1}, Lcom/squareup/Money;->launderMoney(Lcom/squareup/protos/common/Money;Lcom/squareup/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    .line 266
    sget-object v2, Lcom/squareup/protos/common/CurrencyCode;->USD:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1, v2}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getTicketCloseClientUuid()Ljava/lang/String;
    .locals 1

    .line 284
    iget-object v0, p0, Lcom/squareup/queue/Capture;->uuid:Ljava/lang/String;

    return-object v0
.end method

.method public getTicketId()Ljava/lang/String;
    .locals 1

    .line 280
    iget-object v0, p0, Lcom/squareup/queue/Capture;->ticketId:Ljava/lang/String;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .line 276
    iget-wide v0, p0, Lcom/squareup/queue/Capture;->time:J

    return-wide v0
.end method

.method public getTip()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 260
    iget-object v0, p0, Lcom/squareup/queue/Capture;->tipMoney:Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/queue/Capture;->tip:Lcom/squareup/Money;

    invoke-static {v0, v1}, Lcom/squareup/Money;->launderMoney(Lcom/squareup/protos/common/Money;Lcom/squareup/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getTotal()Lcom/squareup/protos/common/Money;
    .locals 2

    .line 289
    iget-object v0, p0, Lcom/squareup/queue/Capture;->totalMoney:Lcom/squareup/protos/common/Money;

    iget-object v1, p0, Lcom/squareup/queue/Capture;->total:Lcom/squareup/Money;

    invoke-static {v0, v1}, Lcom/squareup/Money;->launderMoney(Lcom/squareup/protos/common/Money;Lcom/squareup/Money;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method

.method public getUnmaskedDigits()Ljava/lang/String;
    .locals 1

    .line 248
    iget-object v0, p0, Lcom/squareup/queue/Capture;->unmaskedDigits:Ljava/lang/String;

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 468
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/Capture;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 52
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/Capture;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public isFailed()Z
    .locals 1

    .line 293
    iget-boolean v0, p0, Lcom/squareup/queue/Capture;->failed:Z

    return v0
.end method

.method public logEnqueued(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 0

    .line 336
    invoke-interface {p1, p0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCaptureEnqueued(Lcom/squareup/queue/Capture;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Lcom/squareup/queue/Capture;
    .locals 2

    .line 433
    new-instance v0, Lcom/squareup/queue/Capture;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/squareup/queue/Capture;-><init>(Lcom/squareup/queue/Capture;Z)V

    return-object v0
.end method

.method public bridge synthetic secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 1

    .line 52
    invoke-virtual {p0}, Lcom/squareup/queue/Capture;->secureCopyWithoutPIIForLogs()Lcom/squareup/queue/Capture;

    move-result-object v0

    return-object v0
.end method

.method public secureCopyWithoutPIIForStoreAndForwardPayments()Lcom/squareup/queue/Capture;
    .locals 2

    .line 437
    new-instance v0, Lcom/squareup/queue/Capture;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/queue/Capture;-><init>(Lcom/squareup/queue/Capture;Z)V

    return-object v0
.end method

.method setFailed(Z)V
    .locals 0

    .line 298
    iput-boolean p1, p0, Lcom/squareup/queue/Capture;->failed:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 441
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Capture{authorizationId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/Capture;->authorizationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", failed=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lcom/squareup/queue/Capture;->failed:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", receiptNote=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/Capture;->receiptNote:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", signatureData len "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/Capture;->signatureData:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 446
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const-string v1, "[null]"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/queue/Capture;->time:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", tip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448
    invoke-virtual {p0}, Lcom/squareup/queue/Capture;->getTip()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tax="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 449
    invoke-virtual {p0}, Lcom/squareup/queue/Capture;->getTax()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", total="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    invoke-virtual {p0}, Lcom/squareup/queue/Capture;->getTotal()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", adjustmentList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 451
    invoke-virtual {p0}, Lcom/squareup/queue/Capture;->getAdjustments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->deepToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", itemizationList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 452
    invoke-virtual {p0}, Lcom/squareup/queue/Capture;->getAdjustments()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->deepToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", customerPresence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/Capture;->customerPresence:Lcom/squareup/server/payment/CustomerPresence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
