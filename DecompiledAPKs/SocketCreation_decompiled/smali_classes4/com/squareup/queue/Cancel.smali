.class public Lcom/squareup/queue/Cancel;
.super Ljava/lang/Object;
.source "Cancel.java"

# interfaces
.implements Lcom/squareup/queue/CancelTask;


# static fields
.field private static final serialVersionUID:J


# instance fields
.field private final authorizationId:Ljava/lang/String;

.field transient paymentService:Lcom/squareup/server/payment/PaymentService;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final reason:Lcom/squareup/server/payment/VoidType;

.field private final uniqueKey:Ljava/lang/String;

.field private final userInitiated:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 26
    iput-object v0, p0, Lcom/squareup/queue/Cancel;->authorizationId:Ljava/lang/String;

    .line 37
    iput-object p1, p0, Lcom/squareup/queue/Cancel;->uniqueKey:Ljava/lang/String;

    .line 38
    invoke-static {p2}, Lcom/squareup/queue/Cancel;->voidTypeFor(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/server/payment/VoidType;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/queue/Cancel;->reason:Lcom/squareup/server/payment/VoidType;

    const/4 p1, 0x0

    .line 39
    iput-boolean p1, p0, Lcom/squareup/queue/Cancel;->userInitiated:Z

    return-void
.end method

.method private static voidTypeFor(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/server/payment/VoidType;
    .locals 3

    .line 43
    sget-object v0, Lcom/squareup/queue/Cancel$1;->$SwitchMap$com$squareup$protos$client$bills$CancelBillRequest$CancelBillType:[I

    invoke-virtual {p0}, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 51
    sget-object p0, Lcom/squareup/server/payment/VoidType;->VOID_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/server/payment/VoidType;

    return-object p0

    .line 54
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_1
    sget-object p0, Lcom/squareup/server/payment/VoidType;->VOID_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/server/payment/VoidType;

    return-object p0

    .line 47
    :cond_2
    sget-object p0, Lcom/squareup/server/payment/VoidType;->VOID_OTHER:Lcom/squareup/server/payment/VoidType;

    return-object p0

    .line 45
    :cond_3
    sget-object p0, Lcom/squareup/server/payment/VoidType;->VOID_HUMAN_INITIATED:Lcom/squareup/server/payment/VoidType;

    return-object p0
.end method


# virtual methods
.method public execute(Lcom/squareup/server/SquareCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/SquareCallback<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;)V"
        }
    .end annotation

    .line 73
    iget-object v0, p0, Lcom/squareup/queue/Cancel;->uniqueKey:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/squareup/queue/Cancel;->authorizationId:Ljava/lang/String;

    invoke-static {p0, v0, p1}, Lcom/squareup/payment/offline/StoreAndForwardUtils;->clientErrorIfStoreAndForwardPayment(Lcom/squareup/queue/retrofit/RetrofitTask;Ljava/lang/String;Lcom/squareup/server/SquareCallback;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/squareup/queue/Cancel;->paymentService:Lcom/squareup/server/payment/PaymentService;

    iget-object v1, p0, Lcom/squareup/queue/Cancel;->authorizationId:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/squareup/server/payment/PaymentService;->cancel(Ljava/lang/String;Lcom/squareup/server/SquareCallback;)V

    goto :goto_0

    .line 80
    :cond_1
    iget-object v1, p0, Lcom/squareup/queue/Cancel;->paymentService:Lcom/squareup/server/payment/PaymentService;

    invoke-virtual {p0}, Lcom/squareup/queue/Cancel;->getReason()Lcom/squareup/server/payment/VoidType;

    move-result-object v2

    invoke-interface {v1, v0, v2, p1}, Lcom/squareup/server/payment/PaymentService;->cancelByUniqueKey(Ljava/lang/String;Lcom/squareup/server/payment/VoidType;Lcom/squareup/server/SquareCallback;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic execute(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/server/SquareCallback;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/Cancel;->execute(Lcom/squareup/server/SquareCallback;)V

    return-void
.end method

.method getAuthorizationId()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/queue/Cancel;->authorizationId:Ljava/lang/String;

    return-object v0
.end method

.method public getReason()Lcom/squareup/server/payment/VoidType;
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/queue/Cancel;->reason:Lcom/squareup/server/payment/VoidType;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/squareup/queue/Cancel;->userInitiated:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/server/payment/VoidType;->VOID_HUMAN_INITIATED:Lcom/squareup/server/payment/VoidType;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/squareup/server/payment/VoidType;->VOID_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/server/payment/VoidType;

    :goto_0
    return-object v0
.end method

.method public getUniqueKey()Ljava/lang/String;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/queue/Cancel;->uniqueKey:Ljava/lang/String;

    return-object v0
.end method

.method public inject(Lcom/squareup/queue/TransactionTasksComponent;)V
    .locals 0

    .line 101
    invoke-interface {p1, p0}, Lcom/squareup/queue/TransactionTasksComponent;->inject(Lcom/squareup/queue/Cancel;)V

    return-void
.end method

.method public bridge synthetic inject(Ljava/lang/Object;)V
    .locals 0

    .line 20
    check-cast p1, Lcom/squareup/queue/TransactionTasksComponent;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/Cancel;->inject(Lcom/squareup/queue/TransactionTasksComponent;)V

    return-void
.end method

.method public logEnqueued(Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 0

    .line 97
    invoke-interface {p1, p0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->logCancelEnqueued(Lcom/squareup/queue/Cancel;)V

    return-void
.end method

.method public secureCopyWithoutPIIForLogs()Ljava/lang/Object;
    .locals 0

    return-object p0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cancel{authorizationId=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/queue/Cancel;->authorizationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ", uniqueKey=\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/queue/Cancel;->uniqueKey:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, ", userInitiated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/queue/Cancel;->userInitiated:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
