.class public final Lcom/squareup/queue/QueueService_MembersInjector;
.super Ljava/lang/Object;
.source "QueueService_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/QueueService;",
        ">;"
    }
.end annotation


# instance fields
.field private final authenticatorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final eventSinkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final foregroundServiceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/foregroundservice/ForegroundServiceStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final jobManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;"
        }
    .end annotation
.end field

.field private final lastQueueServiceStartProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final loggedOutTaskWatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/TaskWatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final ohSnapLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final queueServiceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final remoteLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/TaskWatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/foregroundservice/ForegroundServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/squareup/queue/QueueService_MembersInjector;->authenticatorProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p2, p0, Lcom/squareup/queue/QueueService_MembersInjector;->clockProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p3, p0, Lcom/squareup/queue/QueueService_MembersInjector;->gsonProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p4, p0, Lcom/squareup/queue/QueueService_MembersInjector;->eventSinkProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p5, p0, Lcom/squareup/queue/QueueService_MembersInjector;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p6, p0, Lcom/squareup/queue/QueueService_MembersInjector;->jobManagerProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p7, p0, Lcom/squareup/queue/QueueService_MembersInjector;->loggedOutTaskWatcherProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p8, p0, Lcom/squareup/queue/QueueService_MembersInjector;->storeAndForwardQueueProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p9, p0, Lcom/squareup/queue/QueueService_MembersInjector;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p10, p0, Lcom/squareup/queue/QueueService_MembersInjector;->remoteLoggerProvider:Ljavax/inject/Provider;

    .line 80
    iput-object p11, p0, Lcom/squareup/queue/QueueService_MembersInjector;->foregroundServiceStarterProvider:Ljavax/inject/Provider;

    .line 81
    iput-object p12, p0, Lcom/squareup/queue/QueueService_MembersInjector;->queueServiceStarterProvider:Ljavax/inject/Provider;

    .line 82
    iput-object p13, p0, Lcom/squareup/queue/QueueService_MembersInjector;->lastQueueServiceStartProvider:Ljavax/inject/Provider;

    .line 83
    iput-object p14, p0, Lcom/squareup/queue/QueueService_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/LegacyAuthenticator;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/TaskWatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/OhSnapLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/logging/RemoteLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/foregroundservice/ForegroundServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/QueueService;",
            ">;"
        }
    .end annotation

    .line 98
    new-instance v15, Lcom/squareup/queue/QueueService_MembersInjector;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/queue/QueueService_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static injectAuthenticator(Lcom/squareup/queue/QueueService;Lcom/squareup/account/LegacyAuthenticator;)V
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->authenticator:Lcom/squareup/account/LegacyAuthenticator;

    return-void
.end method

.method public static injectClock(Lcom/squareup/queue/QueueService;Lcom/squareup/util/Clock;)V
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method public static injectConnectivityMonitor(Lcom/squareup/queue/QueueService;Lcom/squareup/connectivity/ConnectivityMonitor;)V
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->connectivityMonitor:Lcom/squareup/connectivity/ConnectivityMonitor;

    return-void
.end method

.method public static injectEventSink(Lcom/squareup/queue/QueueService;Lcom/squareup/badbus/BadEventSink;)V
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->eventSink:Lcom/squareup/badbus/BadEventSink;

    return-void
.end method

.method public static injectFeatures(Lcom/squareup/queue/QueueService;Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 194
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static injectForegroundServiceStarter(Lcom/squareup/queue/QueueService;Lcom/squareup/foregroundservice/ForegroundServiceStarter;)V
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->foregroundServiceStarter:Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    return-void
.end method

.method public static injectGson(Lcom/squareup/queue/QueueService;Lcom/google/gson/Gson;)V
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->gson:Lcom/google/gson/Gson;

    return-void
.end method

.method public static injectJobManager(Lcom/squareup/queue/QueueService;Lcom/squareup/backgroundjob/BackgroundJobManager;)V
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->jobManager:Lcom/squareup/backgroundjob/BackgroundJobManager;

    return-void
.end method

.method public static injectLastQueueServiceStart(Lcom/squareup/queue/QueueService;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/QueueService;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 189
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->lastQueueServiceStart:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public static injectLoggedOutTaskWatcher(Lcom/squareup/queue/QueueService;Ljava/lang/Object;)V
    .locals 0

    .line 153
    check-cast p1, Lcom/squareup/queue/TaskWatcher;

    iput-object p1, p0, Lcom/squareup/queue/QueueService;->loggedOutTaskWatcher:Lcom/squareup/queue/TaskWatcher;

    return-void
.end method

.method public static injectOhSnapLogger(Lcom/squareup/queue/QueueService;Lcom/squareup/log/OhSnapLogger;)V
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    return-void
.end method

.method public static injectQueueServiceStarter(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/QueueServiceStarter;)V
    .locals 0

    .line 182
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    return-void
.end method

.method public static injectRemoteLogger(Lcom/squareup/queue/QueueService;Lcom/squareup/logging/RemoteLogger;)V
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->remoteLogger:Lcom/squareup/logging/RemoteLogger;

    return-void
.end method

.method public static injectStoreAndForwardQueue(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/retrofit/RetrofitQueue;)V
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/queue/QueueService;->storeAndForwardQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/QueueService;)V
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/queue/QueueService_MembersInjector;->authenticatorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/LegacyAuthenticator;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectAuthenticator(Lcom/squareup/queue/QueueService;Lcom/squareup/account/LegacyAuthenticator;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/queue/QueueService_MembersInjector;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Clock;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectClock(Lcom/squareup/queue/QueueService;Lcom/squareup/util/Clock;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/queue/QueueService_MembersInjector;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/Gson;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectGson(Lcom/squareup/queue/QueueService;Lcom/google/gson/Gson;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/queue/QueueService_MembersInjector;->eventSinkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/badbus/BadEventSink;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectEventSink(Lcom/squareup/queue/QueueService;Lcom/squareup/badbus/BadEventSink;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/queue/QueueService_MembersInjector;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectConnectivityMonitor(Lcom/squareup/queue/QueueService;Lcom/squareup/connectivity/ConnectivityMonitor;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/queue/QueueService_MembersInjector;->jobManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/backgroundjob/BackgroundJobManager;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectJobManager(Lcom/squareup/queue/QueueService;Lcom/squareup/backgroundjob/BackgroundJobManager;)V

    .line 108
    iget-object v0, p0, Lcom/squareup/queue/QueueService_MembersInjector;->loggedOutTaskWatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectLoggedOutTaskWatcher(Lcom/squareup/queue/QueueService;Ljava/lang/Object;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/queue/QueueService_MembersInjector;->storeAndForwardQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectStoreAndForwardQueue(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/retrofit/RetrofitQueue;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/queue/QueueService_MembersInjector;->ohSnapLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/log/OhSnapLogger;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectOhSnapLogger(Lcom/squareup/queue/QueueService;Lcom/squareup/log/OhSnapLogger;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/queue/QueueService_MembersInjector;->remoteLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/logging/RemoteLogger;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectRemoteLogger(Lcom/squareup/queue/QueueService;Lcom/squareup/logging/RemoteLogger;)V

    .line 112
    iget-object v0, p0, Lcom/squareup/queue/QueueService_MembersInjector;->foregroundServiceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/foregroundservice/ForegroundServiceStarter;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectForegroundServiceStarter(Lcom/squareup/queue/QueueService;Lcom/squareup/foregroundservice/ForegroundServiceStarter;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/queue/QueueService_MembersInjector;->queueServiceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/QueueServiceStarter;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectQueueServiceStarter(Lcom/squareup/queue/QueueService;Lcom/squareup/queue/QueueServiceStarter;)V

    .line 114
    iget-object v0, p0, Lcom/squareup/queue/QueueService_MembersInjector;->lastQueueServiceStartProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectLastQueueServiceStart(Lcom/squareup/queue/QueueService;Lcom/squareup/settings/LocalSetting;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/queue/QueueService_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/queue/QueueService_MembersInjector;->injectFeatures(Lcom/squareup/queue/QueueService;Lcom/squareup/settings/server/Features;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/queue/QueueService;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/QueueService_MembersInjector;->injectMembers(Lcom/squareup/queue/QueueService;)V

    return-void
.end method
