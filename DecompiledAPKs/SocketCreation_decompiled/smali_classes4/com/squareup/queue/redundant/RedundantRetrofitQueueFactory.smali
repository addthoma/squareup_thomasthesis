.class public Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;
.super Lcom/squareup/queue/retrofit/RetrofitQueueFactory;
.source "RedundantRetrofitQueueFactory.java"


# instance fields
.field private final corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

.field private final sqliteQueueFactory:Lrx/functions/Func1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Func1<",
            "Ljava/io/File;",
            "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lrx/functions/Func1;Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;Lcom/squareup/queue/CorruptQueueHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/functions/Func1<",
            "Ljava/io/File;",
            "Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;",
            "Lcom/squareup/tape/TaskInjector<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/tape/FileObjectQueue$Converter<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/queue/CorruptQueueHelper;",
            ")V"
        }
    .end annotation

    .line 29
    invoke-direct {p0, p2, p3}, Lcom/squareup/queue/retrofit/RetrofitQueueFactory;-><init>(Lcom/squareup/tape/TaskInjector;Lcom/squareup/tape/FileObjectQueue$Converter;)V

    .line 30
    iput-object p1, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;->sqliteQueueFactory:Lrx/functions/Func1;

    .line 31
    iput-object p4, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;->corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

    return-void
.end method


# virtual methods
.method public open(Ljava/io/File;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 4

    .line 35
    invoke-virtual {p0, p1}, Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;->createFileQueue(Ljava/io/File;)Lcom/squareup/tape/FileObjectQueue;

    move-result-object v0

    .line 36
    iget-object v1, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;->sqliteQueueFactory:Lrx/functions/Func1;

    invoke-interface {v1, p1}, Lrx/functions/Func1;->call(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;

    .line 37
    new-instance v1, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;

    iget-object v2, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;->taskInjector:Lcom/squareup/tape/TaskInjector;

    iget-object v3, p0, Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;->corruptQueueHelper:Lcom/squareup/queue/CorruptQueueHelper;

    invoke-direct {v1, v0, p1, v2, v3}, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;-><init>(Lcom/squareup/tape/FileObjectQueue;Lcom/squareup/queue/sqlite/shared/DelegatingSqliteQueue;Lcom/squareup/tape/TaskInjector;Lcom/squareup/queue/CorruptQueueHelper;)V

    return-object v1
.end method

.method public bridge synthetic open(Ljava/io/File;)Lcom/squareup/tape/ObjectQueue;
    .locals 0

    .line 20
    invoke-virtual {p0, p1}, Lcom/squareup/queue/redundant/RedundantRetrofitQueueFactory;->open(Ljava/io/File;)Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-result-object p1

    return-object p1
.end method
