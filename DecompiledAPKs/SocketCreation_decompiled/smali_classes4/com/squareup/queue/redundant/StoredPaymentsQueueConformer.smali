.class public Lcom/squareup/queue/redundant/StoredPaymentsQueueConformer;
.super Lcom/squareup/queue/redundant/QueueConformer;
.source "StoredPaymentsQueueConformer.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/queue/redundant/QueueConformer<",
        "Lcom/squareup/payment/offline/StoredPayment;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/queue/CorruptQueueRecorder;)V
    .locals 6
    .param p3    # Lcom/squareup/thread/executor/SerialExecutor;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-virtual {p1}, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->getTapeQueue()Lcom/squareup/queue/ReadableFileObjectQueue;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/queue/redundant/RedundantStoredPaymentsQueue;->getSqliteQueue()Lcom/squareup/queue/sqlite/StoredPaymentsSqliteQueue;

    move-result-object v2

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/queue/redundant/QueueConformer;-><init>(Lcom/squareup/tape/ObjectQueue;Lcom/squareup/queue/sqlite/shared/SqliteQueue;Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;Lcom/squareup/thread/executor/SerialExecutor;Lcom/squareup/queue/CorruptQueueRecorder;)V

    return-void
.end method
