.class public final Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;
.super Ljava/lang/Object;
.source "TasksQueueConformer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/redundant/TasksQueueConformer;",
        ">;"
    }
.end annotation


# instance fields
.field private final corruptQueueRecorderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final redundantQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/RedundantRetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final tapeQueueListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/RedundantRetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;->redundantQueueProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;->tapeQueueListenerProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;->corruptQueueRecorderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/RedundantRetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ">;)",
            "Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/queue/redundant/RedundantRetrofitQueue;Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/queue/CorruptQueueRecorder;)Lcom/squareup/queue/redundant/TasksQueueConformer;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/queue/redundant/RedundantRetrofitQueue;",
            "Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener<",
            "Lcom/squareup/queue/retrofit/RetrofitTask;",
            ">;",
            "Lcom/squareup/thread/executor/MainThread;",
            "Lcom/squareup/queue/CorruptQueueRecorder;",
            ")",
            "Lcom/squareup/queue/redundant/TasksQueueConformer;"
        }
    .end annotation

    .line 59
    new-instance v6, Lcom/squareup/queue/redundant/TasksQueueConformer;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/queue/redundant/TasksQueueConformer;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/queue/redundant/RedundantRetrofitQueue;Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/queue/CorruptQueueRecorder;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/queue/redundant/TasksQueueConformer;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;->redundantQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/queue/redundant/RedundantRetrofitQueue;

    iget-object v2, p0, Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;->tapeQueueListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;

    iget-object v3, p0, Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/thread/executor/MainThread;

    iget-object v4, p0, Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;->corruptQueueRecorderProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/queue/CorruptQueueRecorder;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/queue/redundant/RedundantRetrofitQueue;Lcom/squareup/queue/redundant/QueueConformer$TapeQueueListener;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/queue/CorruptQueueRecorder;)Lcom/squareup/queue/redundant/TasksQueueConformer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/queue/redundant/TasksQueueConformer_Factory;->get()Lcom/squareup/queue/redundant/TasksQueueConformer;

    move-result-object v0

    return-object v0
.end method
