.class public final Lcom/squareup/queue/QueueJobCreator_Factory;
.super Ljava/lang/Object;
.source "QueueJobCreator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/queue/QueueJobCreator;",
        ">;"
    }
.end annotation


# instance fields
.field private final jobManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;"
        }
    .end annotation
.end field

.field private final jobNotificationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final queueServiceStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/queue/QueueJobCreator_Factory;->jobNotificationManagerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/queue/QueueJobCreator_Factory;->jobManagerProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/queue/QueueJobCreator_Factory;->queueServiceStarterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/queue/QueueJobCreator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/backgroundjob/BackgroundJobManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/QueueServiceStarter;",
            ">;)",
            "Lcom/squareup/queue/QueueJobCreator_Factory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/queue/QueueJobCreator_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/queue/QueueJobCreator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/queue/QueueServiceStarter;)Lcom/squareup/queue/QueueJobCreator;
    .locals 1

    .line 47
    new-instance v0, Lcom/squareup/queue/QueueJobCreator;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/queue/QueueJobCreator;-><init>(Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/queue/QueueServiceStarter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/queue/QueueJobCreator;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/queue/QueueJobCreator_Factory;->jobNotificationManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;

    iget-object v1, p0, Lcom/squareup/queue/QueueJobCreator_Factory;->jobManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/backgroundjob/BackgroundJobManager;

    iget-object v2, p0, Lcom/squareup/queue/QueueJobCreator_Factory;->queueServiceStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/queue/QueueServiceStarter;

    invoke-static {v0, v1, v2}, Lcom/squareup/queue/QueueJobCreator_Factory;->newInstance(Lcom/squareup/backgroundjob/notification/BackgroundJobNotificationManager;Lcom/squareup/backgroundjob/BackgroundJobManager;Lcom/squareup/queue/QueueServiceStarter;)Lcom/squareup/queue/QueueJobCreator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/queue/QueueJobCreator_Factory;->get()Lcom/squareup/queue/QueueJobCreator;

    move-result-object v0

    return-object v0
.end method
