.class public final Lcom/squareup/queue/Capture_MembersInjector;
.super Ljava/lang/Object;
.source "Capture_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/queue/Capture;",
        ">;"
    }
.end annotation


# instance fields
.field private final afterCaptureProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/AfterCapture;",
            ">;"
        }
    .end annotation
.end field

.field private final lastCapturePaymentIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final messageQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/SquareMessageQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/PaymentService;",
            ">;"
        }
    .end annotation
.end field

.field private final taskQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionLedgerManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/AfterCapture;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/SquareMessageQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/PaymentService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/queue/Capture_MembersInjector;->afterCaptureProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p2, p0, Lcom/squareup/queue/Capture_MembersInjector;->lastCapturePaymentIdProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p3, p0, Lcom/squareup/queue/Capture_MembersInjector;->messageQueueProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p4, p0, Lcom/squareup/queue/Capture_MembersInjector;->paymentServiceProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p5, p0, Lcom/squareup/queue/Capture_MembersInjector;->taskQueueProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p6, p0, Lcom/squareup/queue/Capture_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p7, p0, Lcom/squareup/queue/Capture_MembersInjector;->ticketsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/AfterCapture;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/SquareMessageQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/payment/PaymentService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/ledger/TransactionLedgerManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/queue/Capture;",
            ">;"
        }
    .end annotation

    .line 59
    new-instance v8, Lcom/squareup/queue/Capture_MembersInjector;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/queue/Capture_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static injectAfterCapture(Lcom/squareup/queue/Capture;Lcom/squareup/queue/AfterCapture;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/queue/Capture;->afterCapture:Lcom/squareup/queue/AfterCapture;

    return-void
.end method

.method public static injectLastCapturePaymentId(Lcom/squareup/queue/Capture;Lcom/squareup/settings/LocalSetting;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/Capture;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 81
    iput-object p1, p0, Lcom/squareup/queue/Capture;->lastCapturePaymentId:Lcom/squareup/settings/LocalSetting;

    return-void
.end method

.method public static injectMessageQueue(Lcom/squareup/queue/Capture;Lcom/squareup/util/SquareMessageQueue;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/squareup/queue/Capture;->messageQueue:Lcom/squareup/util/SquareMessageQueue;

    return-void
.end method

.method public static injectPaymentService(Lcom/squareup/queue/Capture;Lcom/squareup/server/payment/PaymentService;)V
    .locals 0

    .line 91
    iput-object p1, p0, Lcom/squareup/queue/Capture;->paymentService:Lcom/squareup/server/payment/PaymentService;

    return-void
.end method

.method public static injectTaskQueue(Lcom/squareup/queue/Capture;Lcom/squareup/queue/retrofit/RetrofitQueue;)V
    .locals 0

    .line 97
    iput-object p1, p0, Lcom/squareup/queue/Capture;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-void
.end method

.method public static injectTickets(Lcom/squareup/queue/Capture;Lcom/squareup/tickets/Tickets;)V
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/queue/Capture;->tickets:Lcom/squareup/tickets/Tickets;

    return-void
.end method

.method public static injectTransactionLedgerManager(Lcom/squareup/queue/Capture;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/queue/Capture;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/queue/Capture;)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/queue/Capture_MembersInjector;->afterCaptureProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/AfterCapture;

    invoke-static {p1, v0}, Lcom/squareup/queue/Capture_MembersInjector;->injectAfterCapture(Lcom/squareup/queue/Capture;Lcom/squareup/queue/AfterCapture;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/queue/Capture_MembersInjector;->lastCapturePaymentIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/LocalSetting;

    invoke-static {p1, v0}, Lcom/squareup/queue/Capture_MembersInjector;->injectLastCapturePaymentId(Lcom/squareup/queue/Capture;Lcom/squareup/settings/LocalSetting;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/queue/Capture_MembersInjector;->messageQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/SquareMessageQueue;

    invoke-static {p1, v0}, Lcom/squareup/queue/Capture_MembersInjector;->injectMessageQueue(Lcom/squareup/queue/Capture;Lcom/squareup/util/SquareMessageQueue;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/queue/Capture_MembersInjector;->paymentServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/payment/PaymentService;

    invoke-static {p1, v0}, Lcom/squareup/queue/Capture_MembersInjector;->injectPaymentService(Lcom/squareup/queue/Capture;Lcom/squareup/server/payment/PaymentService;)V

    .line 67
    iget-object v0, p0, Lcom/squareup/queue/Capture_MembersInjector;->taskQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/queue/retrofit/RetrofitQueue;

    invoke-static {p1, v0}, Lcom/squareup/queue/Capture_MembersInjector;->injectTaskQueue(Lcom/squareup/queue/Capture;Lcom/squareup/queue/retrofit/RetrofitQueue;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/queue/Capture_MembersInjector;->transactionLedgerManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-static {p1, v0}, Lcom/squareup/queue/Capture_MembersInjector;->injectTransactionLedgerManager(Lcom/squareup/queue/Capture;Lcom/squareup/payment/ledger/TransactionLedgerManager;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/queue/Capture_MembersInjector;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/Tickets;

    invoke-static {p1, v0}, Lcom/squareup/queue/Capture_MembersInjector;->injectTickets(Lcom/squareup/queue/Capture;Lcom/squareup/tickets/Tickets;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 15
    check-cast p1, Lcom/squareup/queue/Capture;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/Capture_MembersInjector;->injectMembers(Lcom/squareup/queue/Capture;)V

    return-void
.end method
