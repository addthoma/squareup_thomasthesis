.class public Lcom/squareup/queue/StoredPaymentsQueue;
.super Ljava/lang/Object;
.source "StoredPaymentsQueue.java"

# interfaces
.implements Lcom/squareup/tape/ObjectQueue;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/tape/ObjectQueue<",
        "Lcom/squareup/payment/offline/StoredPayment;",
        ">;"
    }
.end annotation


# instance fields
.field protected final delegate:Lcom/squareup/queue/ReadableFileObjectQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/queue/ReadableFileObjectQueue<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;"
        }
    .end annotation
.end field

.field private final fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;


# direct methods
.method public constructor <init>(Lcom/squareup/queue/ReadableFileObjectQueue;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 0
    .param p2    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/FileThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/queue/ReadableFileObjectQueue<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ")V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/queue/StoredPaymentsQueue;->delegate:Lcom/squareup/queue/ReadableFileObjectQueue;

    .line 32
    iput-object p2, p0, Lcom/squareup/queue/StoredPaymentsQueue;->fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    return-void
.end method

.method public static getStoredPaymentsQueue(Ljava/io/File;Lcom/squareup/queue/retrofit/QueueCache;)Lcom/squareup/queue/StoredPaymentsQueue;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lcom/squareup/queue/retrofit/QueueCache<",
            "Lcom/squareup/queue/StoredPaymentsQueue;",
            ">;)",
            "Lcom/squareup/queue/StoredPaymentsQueue;"
        }
    .end annotation

    .line 21
    new-instance v0, Ljava/io/File;

    const-string v1, "store-and-forward"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 22
    invoke-virtual {p1, v0}, Lcom/squareup/queue/retrofit/QueueCache;->getOrOpen(Ljava/io/File;)Lcom/squareup/tape/ObjectQueue;

    move-result-object p0

    check-cast p0, Lcom/squareup/queue/StoredPaymentsQueue;

    return-object p0
.end method


# virtual methods
.method public add(Lcom/squareup/payment/offline/StoredPayment;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/queue/StoredPaymentsQueue;->fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 42
    iget-object v0, p0, Lcom/squareup/queue/StoredPaymentsQueue;->delegate:Lcom/squareup/queue/ReadableFileObjectQueue;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/ReadableFileObjectQueue;->add(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic add(Ljava/lang/Object;)V
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/payment/offline/StoredPayment;

    invoke-virtual {p0, p1}, Lcom/squareup/queue/StoredPaymentsQueue;->add(Lcom/squareup/payment/offline/StoredPayment;)V

    return-void
.end method

.method public close()V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/queue/StoredPaymentsQueue;->fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 68
    iget-object v0, p0, Lcom/squareup/queue/StoredPaymentsQueue;->delegate:Lcom/squareup/queue/ReadableFileObjectQueue;

    invoke-virtual {v0}, Lcom/squareup/queue/ReadableFileObjectQueue;->close()V

    return-void
.end method

.method public peek()Lcom/squareup/payment/offline/StoredPayment;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/queue/StoredPaymentsQueue;->fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 47
    iget-object v0, p0, Lcom/squareup/queue/StoredPaymentsQueue;->delegate:Lcom/squareup/queue/ReadableFileObjectQueue;

    invoke-virtual {v0}, Lcom/squareup/queue/ReadableFileObjectQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/offline/StoredPayment;

    return-object v0
.end method

.method public bridge synthetic peek()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/queue/StoredPaymentsQueue;->peek()Lcom/squareup/payment/offline/StoredPayment;

    move-result-object v0

    return-object v0
.end method

.method public readAll(Lcom/squareup/tape/ObjectQueue$Listener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue$Listener<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;)V"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/queue/StoredPaymentsQueue;->fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 63
    iget-object v0, p0, Lcom/squareup/queue/StoredPaymentsQueue;->delegate:Lcom/squareup/queue/ReadableFileObjectQueue;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/ReadableFileObjectQueue;->readAll(Lcom/squareup/tape/ObjectQueue$Listener;)V

    return-void
.end method

.method public remove()V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/queue/StoredPaymentsQueue;->fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 52
    iget-object v0, p0, Lcom/squareup/queue/StoredPaymentsQueue;->delegate:Lcom/squareup/queue/ReadableFileObjectQueue;

    invoke-virtual {v0}, Lcom/squareup/queue/ReadableFileObjectQueue;->remove()V

    return-void
.end method

.method public setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tape/ObjectQueue$Listener<",
            "Lcom/squareup/payment/offline/StoredPayment;",
            ">;)V"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/queue/StoredPaymentsQueue;->fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 57
    iget-object v0, p0, Lcom/squareup/queue/StoredPaymentsQueue;->delegate:Lcom/squareup/queue/ReadableFileObjectQueue;

    invoke-virtual {v0, p1}, Lcom/squareup/queue/ReadableFileObjectQueue;->setListener(Lcom/squareup/tape/ObjectQueue$Listener;)V

    return-void
.end method

.method public size()I
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/queue/StoredPaymentsQueue;->fileThreadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 37
    iget-object v0, p0, Lcom/squareup/queue/StoredPaymentsQueue;->delegate:Lcom/squareup/queue/ReadableFileObjectQueue;

    invoke-virtual {v0}, Lcom/squareup/queue/ReadableFileObjectQueue;->size()I

    move-result v0

    return v0
.end method
