.class public Lcom/squareup/queue/QueueService$BootReceiver;
.super Landroid/content/BroadcastReceiver;
.source "QueueService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/queue/QueueService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BootReceiver"
.end annotation


# instance fields
.field queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 433
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0

    .line 438
    invoke-static {}, Lcom/squareup/mortar/AppContextWrapper;->appContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/queue/QueueRootModule$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/queue/QueueRootModule$Component;

    invoke-interface {p1, p0}, Lcom/squareup/queue/QueueRootModule$Component;->inject(Lcom/squareup/queue/QueueService$BootReceiver;)V

    .line 439
    iget-object p1, p0, Lcom/squareup/queue/QueueService$BootReceiver;->queueServiceStarter:Lcom/squareup/queue/QueueServiceStarter;

    const-string p2, "Device booted"

    invoke-interface {p1, p2}, Lcom/squareup/queue/QueueServiceStarter;->start(Ljava/lang/String;)V

    return-void
.end method
