.class public final Lcom/squareup/phrase/Phrase;
.super Ljava/lang/Object;
.source "Phrase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/phrase/Phrase$KeyToken;,
        Lcom/squareup/phrase/Phrase$LeftCurlyBracketToken;,
        Lcom/squareup/phrase/Phrase$TextToken;,
        Lcom/squareup/phrase/Phrase$Token;
    }
.end annotation


# static fields
.field private static final EOF:I


# instance fields
.field private curChar:C

.field private curCharIndex:I

.field private formatted:Ljava/lang/CharSequence;

.field private head:Lcom/squareup/phrase/Phrase$Token;

.field private final keys:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final keysToValues:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private final pattern:Ljava/lang/CharSequence;

.field private spanSupportedEnabled:Z


# direct methods
.method private constructor <init>(Ljava/lang/CharSequence;)V
    .locals 2

    .line 266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/phrase/Phrase;->keys:Ljava/util/Set;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/phrase/Phrase;->keysToValues:Ljava/util/Map;

    const/4 v0, 0x1

    .line 71
    iput-boolean v0, p0, Lcom/squareup/phrase/Phrase;->spanSupportedEnabled:Z

    .line 267
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_0

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    :cond_0
    iput-char v1, p0, Lcom/squareup/phrase/Phrase;->curChar:C

    .line 269
    iput-object p1, p0, Lcom/squareup/phrase/Phrase;->pattern:Ljava/lang/CharSequence;

    const/4 p1, 0x0

    .line 275
    :cond_1
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/phrase/Phrase;->token(Lcom/squareup/phrase/Phrase$Token;)Lcom/squareup/phrase/Phrase$Token;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 277
    iget-object v0, p0, Lcom/squareup/phrase/Phrase;->head:Lcom/squareup/phrase/Phrase$Token;

    if-nez v0, :cond_1

    iput-object p1, p0, Lcom/squareup/phrase/Phrase;->head:Lcom/squareup/phrase/Phrase$Token;

    goto :goto_0

    :cond_2
    return-void
.end method

.method private consume()V
    .locals 2

    .line 357
    iget v0, p0, Lcom/squareup/phrase/Phrase;->curCharIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/phrase/Phrase;->curCharIndex:I

    .line 358
    iget v0, p0, Lcom/squareup/phrase/Phrase;->curCharIndex:I

    iget-object v1, p0, Lcom/squareup/phrase/Phrase;->pattern:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/phrase/Phrase;->pattern:Ljava/lang/CharSequence;

    iget v1, p0, Lcom/squareup/phrase/Phrase;->curCharIndex:I

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    :goto_0
    iput-char v0, p0, Lcom/squareup/phrase/Phrase;->curChar:C

    return-void
.end method

.method private createEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;
    .locals 1

    .line 260
    iget-boolean v0, p0, Lcom/squareup/phrase/Phrase;->spanSupportedEnabled:Z

    if-eqz v0, :cond_0

    .line 261
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, p1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    return-object v0

    .line 263
    :cond_0
    new-instance v0, Lcom/squareup/phrase/SimpleEditable;

    invoke-direct {v0, p1}, Lcom/squareup/phrase/SimpleEditable;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public static from(Landroid/app/Fragment;I)Lcom/squareup/phrase/Phrase;
    .locals 0

    .line 83
    invoke-virtual {p0}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    return-object p0
.end method

.method public static from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;
    .locals 0

    .line 103
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    return-object p0
.end method

.method public static from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;
    .locals 0

    .line 113
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    return-object p0
.end method

.method public static from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;
    .locals 0

    .line 93
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/res/Resources;I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    return-object p0
.end method

.method public static from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;
    .locals 1

    .line 153
    new-instance v0, Lcom/squareup/phrase/Phrase;

    invoke-direct {v0, p0}, Lcom/squareup/phrase/Phrase;-><init>(Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public static fromPlural(Landroid/content/Context;II)Lcom/squareup/phrase/Phrase;
    .locals 0

    .line 133
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lcom/squareup/phrase/Phrase;->fromPlural(Landroid/content/res/Resources;II)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    return-object p0
.end method

.method public static fromPlural(Landroid/content/res/Resources;II)Lcom/squareup/phrase/Phrase;
    .locals 0

    .line 143
    invoke-virtual {p0, p1, p2}, Landroid/content/res/Resources;->getQuantityText(II)Ljava/lang/CharSequence;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    return-object p0
.end method

.method public static fromPlural(Landroid/view/View;II)Lcom/squareup/phrase/Phrase;
    .locals 0

    .line 123
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lcom/squareup/phrase/Phrase;->fromPlural(Landroid/content/res/Resources;II)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    return-object p0
.end method

.method private key(Lcom/squareup/phrase/Phrase$Token;)Lcom/squareup/phrase/Phrase$KeyToken;
    .locals 3

    .line 305
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 308
    invoke-direct {p0}, Lcom/squareup/phrase/Phrase;->consume()V

    .line 309
    :goto_0
    iget-char v1, p0, Lcom/squareup/phrase/Phrase;->curChar:C

    const/16 v2, 0x61

    if-lt v1, v2, :cond_0

    const/16 v2, 0x7a

    if-le v1, v2, :cond_1

    :cond_0
    iget-char v1, p0, Lcom/squareup/phrase/Phrase;->curChar:C

    const/16 v2, 0x5f

    if-ne v1, v2, :cond_2

    .line 310
    :cond_1
    iget-char v1, p0, Lcom/squareup/phrase/Phrase;->curChar:C

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 311
    invoke-direct {p0}, Lcom/squareup/phrase/Phrase;->consume()V

    goto :goto_0

    :cond_2
    const/16 v2, 0x7d

    if-ne v1, v2, :cond_4

    .line 318
    invoke-direct {p0}, Lcom/squareup/phrase/Phrase;->consume()V

    .line 321
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_3

    .line 325
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 326
    iget-object v1, p0, Lcom/squareup/phrase/Phrase;->keys:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 327
    new-instance v1, Lcom/squareup/phrase/Phrase$KeyToken;

    invoke-direct {v1, p1, v0}, Lcom/squareup/phrase/Phrase$KeyToken;-><init>(Lcom/squareup/phrase/Phrase$Token;Ljava/lang/String;)V

    return-object v1

    .line 322
    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Empty key: {}"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 316
    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Missing closing brace: }"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private leftCurlyBracket(Lcom/squareup/phrase/Phrase$Token;)Lcom/squareup/phrase/Phrase$LeftCurlyBracketToken;
    .locals 1

    .line 342
    invoke-direct {p0}, Lcom/squareup/phrase/Phrase;->consume()V

    .line 343
    invoke-direct {p0}, Lcom/squareup/phrase/Phrase;->consume()V

    .line 344
    new-instance v0, Lcom/squareup/phrase/Phrase$LeftCurlyBracketToken;

    invoke-direct {v0, p1}, Lcom/squareup/phrase/Phrase$LeftCurlyBracketToken;-><init>(Lcom/squareup/phrase/Phrase$Token;)V

    return-object v0
.end method

.method private lookahead()C
    .locals 2

    .line 349
    iget v0, p0, Lcom/squareup/phrase/Phrase;->curCharIndex:I

    iget-object v1, p0, Lcom/squareup/phrase/Phrase;->pattern:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/phrase/Phrase;->pattern:Ljava/lang/CharSequence;

    iget v1, p0, Lcom/squareup/phrase/Phrase;->curCharIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private text(Lcom/squareup/phrase/Phrase$Token;)Lcom/squareup/phrase/Phrase$TextToken;
    .locals 3

    .line 332
    iget v0, p0, Lcom/squareup/phrase/Phrase;->curCharIndex:I

    .line 334
    :goto_0
    iget-char v1, p0, Lcom/squareup/phrase/Phrase;->curChar:C

    const/16 v2, 0x7b

    if-eq v1, v2, :cond_0

    if-eqz v1, :cond_0

    .line 335
    invoke-direct {p0}, Lcom/squareup/phrase/Phrase;->consume()V

    goto :goto_0

    .line 337
    :cond_0
    new-instance v1, Lcom/squareup/phrase/Phrase$TextToken;

    iget v2, p0, Lcom/squareup/phrase/Phrase;->curCharIndex:I

    sub-int/2addr v2, v0

    invoke-direct {v1, p1, v2}, Lcom/squareup/phrase/Phrase$TextToken;-><init>(Lcom/squareup/phrase/Phrase$Token;I)V

    return-object v1
.end method

.method private token(Lcom/squareup/phrase/Phrase$Token;)Lcom/squareup/phrase/Phrase$Token;
    .locals 3

    .line 284
    iget-char v0, p0, Lcom/squareup/phrase/Phrase;->curChar:C

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/16 v1, 0x7b

    if-ne v0, v1, :cond_3

    .line 288
    invoke-direct {p0}, Lcom/squareup/phrase/Phrase;->lookahead()C

    move-result v0

    if-ne v0, v1, :cond_1

    .line 290
    invoke-direct {p0, p1}, Lcom/squareup/phrase/Phrase;->leftCurlyBracket(Lcom/squareup/phrase/Phrase$Token;)Lcom/squareup/phrase/Phrase$LeftCurlyBracketToken;

    move-result-object p1

    return-object p1

    :cond_1
    const/16 v1, 0x61

    if-lt v0, v1, :cond_2

    const/16 v1, 0x7a

    if-gt v0, v1, :cond_2

    .line 292
    invoke-direct {p0, p1}, Lcom/squareup/phrase/Phrase;->key(Lcom/squareup/phrase/Phrase$Token;)Lcom/squareup/phrase/Phrase$KeyToken;

    move-result-object p1

    return-object p1

    .line 294
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected character \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "\'; expected key."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 298
    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/phrase/Phrase;->text(Lcom/squareup/phrase/Phrase$Token;)Lcom/squareup/phrase/Phrase$TextToken;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public format()Ljava/lang/CharSequence;
    .locals 4

    .line 215
    iget-object v0, p0, Lcom/squareup/phrase/Phrase;->formatted:Ljava/lang/CharSequence;

    if-nez v0, :cond_2

    .line 216
    iget-object v0, p0, Lcom/squareup/phrase/Phrase;->keysToValues:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/phrase/Phrase;->keys:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    iget-object v0, p0, Lcom/squareup/phrase/Phrase;->pattern:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/squareup/phrase/Phrase;->createEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v0

    .line 224
    iget-object v1, p0, Lcom/squareup/phrase/Phrase;->head:Lcom/squareup/phrase/Phrase$Token;

    :goto_0
    if-eqz v1, :cond_0

    .line 225
    iget-object v2, p0, Lcom/squareup/phrase/Phrase;->keysToValues:Ljava/util/Map;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/phrase/Phrase$Token;->expand(Landroid/text/Editable;Ljava/util/Map;)V

    .line 224
    invoke-static {v1}, Lcom/squareup/phrase/Phrase$Token;->access$000(Lcom/squareup/phrase/Phrase$Token;)Lcom/squareup/phrase/Phrase$Token;

    move-result-object v1

    goto :goto_0

    .line 228
    :cond_0
    iput-object v0, p0, Lcom/squareup/phrase/Phrase;->formatted:Ljava/lang/CharSequence;

    goto :goto_1

    .line 217
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/squareup/phrase/Phrase;->keys:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 218
    iget-object v1, p0, Lcom/squareup/phrase/Phrase;->keysToValues:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 219
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Missing keys: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 230
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/squareup/phrase/Phrase;->formatted:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public into(Landroid/widget/TextView;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 248
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    .line 246
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "TextView must not be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;
    .locals 0

    .line 184
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    return-object p1
.end method

.method public put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;
    .locals 2

    .line 164
    iget-object v0, p0, Lcom/squareup/phrase/Phrase;->keys:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    .line 170
    iget-object v0, p0, Lcom/squareup/phrase/Phrase;->keysToValues:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    .line 173
    iput-object p1, p0, Lcom/squareup/phrase/Phrase;->formatted:Ljava/lang/CharSequence;

    return-object p0

    .line 168
    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Null value for \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\'"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    .line 165
    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid key: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public putOptional(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;
    .locals 1

    .line 205
    iget-object v0, p0, Lcom/squareup/phrase/Phrase;->keys:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, p0

    :goto_0
    return-object p1
.end method

.method public putOptional(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/squareup/phrase/Phrase;->keys:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, p0

    :goto_0
    return-object p1
.end method

.method public setSpanSupportEnabled(Z)V
    .locals 0

    .line 240
    iput-boolean p1, p0, Lcom/squareup/phrase/Phrase;->spanSupportedEnabled:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 256
    iget-object v0, p0, Lcom/squareup/phrase/Phrase;->pattern:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
