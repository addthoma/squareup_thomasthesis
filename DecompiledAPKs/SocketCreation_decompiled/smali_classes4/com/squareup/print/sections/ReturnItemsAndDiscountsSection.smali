.class public Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;
.super Ljava/lang/Object;
.source "ReturnItemsAndDiscountsSection.java"


# instance fields
.field public final comps:Lcom/squareup/print/payload/LabelAmountPair;

.field public final discount:Lcom/squareup/print/payload/LabelAmountPair;

.field public final discounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ItemSection;",
            ">;"
        }
    .end annotation
.end field

.field public final items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ItemSection;",
            ">;"
        }
    .end annotation
.end field

.field public final sectionHeader:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ItemSection;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ItemSection;",
            ">;",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    iput-object p1, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->items:Ljava/util/List;

    .line 199
    iput-object p2, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->discounts:Ljava/util/List;

    .line 200
    iput-object p3, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->discount:Lcom/squareup/print/payload/LabelAmountPair;

    .line 201
    iput-object p4, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->comps:Lcom/squareup/print/payload/LabelAmountPair;

    .line 202
    iput-object p5, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->sectionHeader:Ljava/lang/String;

    return-void
.end method

.method private static annotateUntaxedItems(Lcom/squareup/settings/server/AccountStatusSettings;Ljava/util/List;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;)Z"
        }
    .end annotation

    .line 106
    invoke-static {p0}, Lcom/squareup/print/sections/SectionUtils;->isAustralia(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {p1}, Lcom/squareup/print/sections/SectionUtils;->hasBothTaxedAndUntaxedItems(Ljava/util/List;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static annotateVatItemsForMultipleTaxBreakdown(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/TaxBreakdown;)Z
    .locals 0

    .line 111
    invoke-static {p0}, Lcom/squareup/print/sections/SectionUtils;->canShowTaxBreakDownTable(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, p1, Lcom/squareup/util/TaxBreakdown;->taxBreakdownType:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    sget-object p1, Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;->TABLE:Lcom/squareup/util/TaxBreakdown$TaxBreakdownType;

    if-ne p0, p1, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static buildAllDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 5

    .line 117
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnCart;->getReturnDiscountsAsAdjustments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 116
    invoke-static {v0, v1, v2}, Lcom/squareup/print/sections/SectionUtils;->calculateTotalPositiveDiscount(Ljava/util/List;ZZ)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 122
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnCart;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 123
    invoke-virtual {p0, p1}, Lcom/squareup/print/ReceiptFormatter;->discount(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static buildCompDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 5

    .line 145
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnCart;->getReturnDiscountsAsAdjustments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 144
    invoke-static {v0, v1, v2}, Lcom/squareup/print/sections/SectionUtils;->calculateTotalPositiveDiscount(Ljava/util/List;ZZ)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 150
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnCart;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 151
    invoke-virtual {p0, p1}, Lcom/squareup/print/ReceiptFormatter;->comp(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static buildItemizedCart(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/util/TaxBreakdown;Ljava/util/List;ZZZZLcom/squareup/util/Res;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ReceiptFormatter;",
            "Lcom/squareup/util/TaxBreakdown;",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;ZZZZ",
            "Lcom/squareup/util/Res;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ItemSection;",
            ">;"
        }
    .end annotation

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 181
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v5, v2

    check-cast v5, Lcom/squareup/print/PrintableOrderItem;

    const/4 v10, 0x0

    const/4 v12, 0x0

    move-object v3, p0

    move-object v4, p1

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v11, p7

    .line 182
    invoke-static/range {v3 .. v12}, Lcom/squareup/print/sections/ItemSection;->createItemSection(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/print/PrintableOrderItem;ZZZZLjava/lang/String;Lcom/squareup/util/Res;Z)Lcom/squareup/print/sections/ItemSection;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static buildItemizedDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;Z)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ReceiptFormatter;",
            "Lcom/squareup/checkout/ReturnCart;",
            "Z)",
            "Ljava/util/List<",
            "Lcom/squareup/print/sections/ItemSection;",
            ">;"
        }
    .end annotation

    .line 161
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 164
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnCart;->getReturnDiscountsAsAdjustments()Ljava/util/List;

    move-result-object p1

    const/4 v1, 0x1

    .line 163
    invoke-static {p1, v1, p2}, Lcom/squareup/print/sections/SectionUtils;->findAdjustments(Ljava/util/List;ZZ)Ljava/util/List;

    move-result-object p1

    .line 168
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/payment/OrderAdjustment;

    .line 169
    invoke-static {p0, p2}, Lcom/squareup/print/sections/ItemSection;->createItemSection(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/OrderAdjustment;)Lcom/squareup/print/sections/ItemSection;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static buildNoncompDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/print/payload/LabelAmountPair;
    .locals 5

    .line 131
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnCart;->getReturnDiscountsAsAdjustments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    invoke-static {v0, v1, v2}, Lcom/squareup/print/sections/SectionUtils;->calculateTotalPositiveDiscount(Ljava/util/List;ZZ)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    .line 136
    invoke-virtual {p1}, Lcom/squareup/checkout/ReturnCart;->getCurrencyCode()Lcom/squareup/protos/common/CurrencyCode;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 137
    invoke-virtual {p0, p1}, Lcom/squareup/print/ReceiptFormatter;->discount(Lcom/squareup/protos/common/Money;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static fromReturnCart(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;Lcom/squareup/util/TaxBreakdown;Lcom/squareup/settings/server/Features;ZLcom/squareup/util/Res;)Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;
    .locals 9

    .line 53
    invoke-virtual {p2}, Lcom/squareup/checkout/ReturnCart;->getReturnItemsAsCartItems()Ljava/util/List;

    move-result-object v1

    .line 55
    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->REDUCE_PRINTING_WASTE:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p4, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 57
    invoke-static {v1}, Lcom/squareup/print/sections/CoalescedItem;->coalesce(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 60
    :cond_0
    invoke-static {p4}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->showItemizedDiscounts(Lcom/squareup/settings/server/Features;)Z

    move-result v8

    .line 61
    invoke-static {p0, v1}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->annotateUntaxedItems(Lcom/squareup/settings/server/AccountStatusSettings;Ljava/util/List;)Z

    move-result v3

    .line 63
    invoke-static {p0, p3}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->annotateVatItemsForMultipleTaxBreakdown(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/TaxBreakdown;)Z

    move-result v4

    .line 64
    invoke-virtual {p0}, Lcom/squareup/settings/server/AccountStatusSettings;->isHideModifiersOnReceiptsEnabled()Z

    move-result v5

    .line 66
    invoke-static {v1}, Lcom/squareup/print/PrintablePaymentOrder;->convertToReturnItems(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    move-object v0, p1

    move-object v1, p3

    move-object v2, v6

    move v6, p5

    move-object v7, p6

    .line 67
    invoke-static/range {v0 .. v7}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->buildItemizedCart(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/util/TaxBreakdown;Ljava/util/List;ZZZZLcom/squareup/util/Res;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v8, :cond_1

    xor-int/lit8 v2, p5, 0x1

    .line 82
    invoke-static {p1, p2, v2}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->buildItemizedDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;Z)Ljava/util/List;

    move-result-object v2

    move-object v5, v1

    goto :goto_0

    .line 85
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    if-eqz p5, :cond_2

    .line 87
    invoke-static {p1, p2}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->buildNoncompDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v5

    goto :goto_0

    .line 88
    :cond_2
    invoke-static {p1, p2}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->buildAllDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v5

    :goto_0
    if-eqz p5, :cond_3

    .line 92
    invoke-static {p1, p2}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->buildCompDiscounts(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/checkout/ReturnCart;)Lcom/squareup/print/payload/LabelAmountPair;

    move-result-object v1

    .line 94
    :cond_3
    sget v3, Lcom/squareup/print/R$string;->receipt_header_return:I

    invoke-interface {p6, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 96
    new-instance v4, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    move-object p0, v4

    move-object p1, v0

    move-object p2, v2

    move-object p3, v5

    move-object p4, v1

    move-object p5, v3

    invoke-direct/range {p0 .. p5}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Ljava/lang/String;)V

    return-object v4
.end method

.method private renderItemSection(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/print/sections/ItemSection;)V
    .locals 4

    .line 257
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    .line 258
    iget-object v2, p2, Lcom/squareup/print/sections/ItemSection;->baseAmountInputs:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p2, Lcom/squareup/print/sections/ItemSection;->variationAndModifiers:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/print/util/PrintRendererUtils;->appendAsLinesIfNotBlank(Landroid/text/SpannableStringBuilder;[Ljava/lang/CharSequence;)V

    .line 262
    iget-object v1, p2, Lcom/squareup/print/sections/ItemSection;->nameAndQuantity:Ljava/lang/String;

    iget-object v2, p2, Lcom/squareup/print/sections/ItemSection;->totalPrice:Ljava/lang/String;

    invoke-virtual {p1, v1, v2, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->multilineTitleAmountAndText(Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 264
    iget-object v0, p2, Lcom/squareup/print/sections/ItemSection;->note:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 265
    iget-object p2, p2, Lcom/squareup/print/sections/ItemSection;->note:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->fullWidthItalicText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_0
    return-void
.end method

.method private static showItemizedDiscounts(Lcom/squareup/settings/server/Features;)Z
    .locals 1

    .line 101
    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->PRINT_ITEMIZED_DISCOUNTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p0, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 271
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 272
    :cond_1
    check-cast p1, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    .line 273
    iget-object v2, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->items:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->items:Ljava/util/List;

    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->discounts:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->discounts:Ljava/util/List;

    .line 274
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->discount:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v3, p1, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->discount:Lcom/squareup/print/payload/LabelAmountPair;

    .line 275
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->comps:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v3, p1, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->comps:Lcom/squareup/print/payload/LabelAmountPair;

    .line 276
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->sectionHeader:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->sectionHeader:Ljava/lang/String;

    .line 277
    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    .line 281
    iget-object v1, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->items:Ljava/util/List;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->discounts:Ljava/util/List;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->discount:Lcom/squareup/print/payload/LabelAmountPair;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->comps:Lcom/squareup/print/payload/LabelAmountPair;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->sectionHeader:Ljava/lang/String;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 6

    .line 208
    iget-object v0, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->sectionHeader:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->appendSectionHeader(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v0, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/print/sections/ItemSection;

    .line 210
    iget-object v5, v4, Lcom/squareup/print/sections/ItemSection;->diningOptionName:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 211
    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    if-nez v3, :cond_0

    .line 213
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 215
    :cond_0
    invoke-virtual {p1, v5, v1}, Lcom/squareup/print/ThermalBitmapBuilder;->diningOptionHeader(Ljava/lang/CharSequence;Z)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 216
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_1

    :cond_1
    if-eqz v3, :cond_2

    .line 219
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 220
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_1

    .line 222
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 225
    :goto_1
    invoke-direct {p0, p1, v4}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->renderItemSection(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/print/sections/ItemSection;)V

    const/4 v3, 0x0

    move-object v2, v5

    goto :goto_0

    .line 231
    :cond_3
    iget-object v0, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->discounts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 232
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 233
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 236
    :cond_4
    iget-object v0, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->discounts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/sections/ItemSection;

    .line 237
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 238
    invoke-direct {p0, p1, v1}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->renderItemSection(Lcom/squareup/print/ThermalBitmapBuilder;Lcom/squareup/print/sections/ItemSection;)V

    goto :goto_2

    .line 241
    :cond_5
    iget-object v0, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->discount:Lcom/squareup/print/payload/LabelAmountPair;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->comps:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v0, :cond_7

    .line 242
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->tinySpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 245
    :cond_7
    iget-object v0, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->discount:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v0, :cond_8

    .line 246
    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 249
    :cond_8
    iget-object v0, p0, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->comps:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v0, :cond_9

    .line 250
    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 253
    :cond_9
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->mediumSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    return-void
.end method
