.class public final Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;
.super Ljava/lang/Object;
.source "SubtotalAndAdjustmentsSection.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u001d\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0086\u0008\u0018\u0000 32\u00020\u0001:\u00013B_\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u0012\u000c\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ\u000b\u0010\u001f\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000f\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005H\u00c6\u0003J\u000f\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005H\u00c6\u0003J\u000f\u0010\"\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005H\u00c6\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003J\t\u0010&\u001a\u00020\rH\u00c6\u0003Js\u0010\'\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u000e\u0008\u0002\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00052\u000e\u0008\u0002\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00052\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00052\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u00032\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\rH\u00c6\u0001J\u0013\u0010(\u001a\u00020\r2\u0008\u0010)\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010*\u001a\u00020+H\u00d6\u0001J\u001e\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020/2\u000c\u00100\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005H\u0002J\u000e\u00101\u001a\u00020-2\u0006\u0010.\u001a\u00020/J\t\u00102\u001a\u00020\u000bH\u00d6\u0001R\u0017\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010R\u0011\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u001a\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u00058BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0014\u0010\u0010R\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0010R \u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0016\u0010\u0010\"\u0004\u0008\u0017\u0010\u0018R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001aR\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u001cR\u0013\u0010\t\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001d\u0010\u001aR\u0013\u0010\u0008\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001e\u0010\u001a\u00a8\u00064"
    }
    d2 = {
        "Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;",
        "",
        "subtotal",
        "Lcom/squareup/print/payload/LabelAmountPair;",
        "preTaxSurcharges",
        "",
        "additiveTaxes",
        "postTaxSurcharges",
        "tip",
        "swedishRounding",
        "surchargeDisclosure",
        "",
        "alwaysRender",
        "",
        "(Lcom/squareup/print/payload/LabelAmountPair;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Ljava/lang/String;Z)V",
        "getAdditiveTaxes",
        "()Ljava/util/List;",
        "getAlwaysRender",
        "()Z",
        "nonNullAdjustments",
        "getNonNullAdjustments",
        "getPostTaxSurcharges",
        "getPreTaxSurcharges",
        "setPreTaxSurcharges",
        "(Ljava/util/List;)V",
        "getSubtotal",
        "()Lcom/squareup/print/payload/LabelAmountPair;",
        "getSurchargeDisclosure",
        "()Ljava/lang/String;",
        "getSwedishRounding",
        "getTip",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "other",
        "hashCode",
        "",
        "renderAdjustments",
        "",
        "builder",
        "Lcom/squareup/print/ThermalBitmapBuilder;",
        "adjustments",
        "renderBitmap",
        "toString",
        "Companion",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;


# instance fields
.field private final additiveTaxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation
.end field

.field private final alwaysRender:Z

.field private final postTaxSurcharges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation
.end field

.field private preTaxSurcharges:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation
.end field

.field private final subtotal:Lcom/squareup/print/payload/LabelAmountPair;

.field private final surchargeDisclosure:Ljava/lang/String;

.field private final swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

.field private final tip:Lcom/squareup/print/payload/LabelAmountPair;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->Companion:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/print/payload/LabelAmountPair;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Ljava/lang/String;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    const-string v0, "preTaxSurcharges"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additiveTaxes"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "postTaxSurcharges"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    iput-object p2, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->preTaxSurcharges:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    iput-object p4, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->postTaxSurcharges:Ljava/util/List;

    iput-object p5, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    iput-object p6, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    iput-object p7, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->surchargeDisclosure:Ljava/lang/String;

    iput-boolean p8, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->alwaysRender:Z

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;Lcom/squareup/print/payload/LabelAmountPair;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Ljava/lang/String;ZILjava/lang/Object;)Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->preTaxSurcharges:Ljava/util/List;

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->postTaxSurcharges:Ljava/util/List;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    goto :goto_5

    :cond_5
    move-object v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->surchargeDisclosure:Ljava/lang/String;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-boolean v1, v0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->alwaysRender:Z

    goto :goto_7

    :cond_7
    move/from16 v1, p8

    :goto_7
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object p6, v7

    move-object/from16 p7, v8

    move/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->copy(Lcom/squareup/print/payload/LabelAmountPair;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Ljava/lang/String;Z)Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    move-result-object v0

    return-object v0
.end method

.method public static final fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZZ)Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;
    .locals 8
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->Companion:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection$Companion;->fromOrder(Lcom/squareup/print/ReceiptFormatter;Lcom/squareup/payment/Order;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;ZZZ)Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    move-result-object p0

    return-object p0
.end method

.method private final getNonNullAdjustments()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 51
    iget-object v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->preTaxSurcharges:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 52
    iget-object v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 53
    iget-object v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->postTaxSurcharges:Ljava/util/List;

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 55
    iget-object v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v1, :cond_0

    .line 56
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    :cond_0
    iget-object v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v1, :cond_1

    .line 59
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 62
    :cond_1
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final renderAdjustments(Lcom/squareup/print/ThermalBitmapBuilder;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ThermalBitmapBuilder;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;)V"
        }
    .end annotation

    .line 77
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->lightDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 78
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 81
    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 84
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/payload/LabelAmountPair;

    .line 85
    invoke-virtual {p1, v0}, Lcom/squareup/print/ThermalBitmapBuilder;->titleAndAmount(Lcom/squareup/print/payload/LabelAmountPair;)Lcom/squareup/print/ThermalBitmapBuilder;

    goto :goto_0

    .line 88
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 90
    iget-object p2, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->surchargeDisclosure:Ljava/lang/String;

    if-eqz p2, :cond_1

    .line 91
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->thinDivider()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 92
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    .line 93
    iget-object p2, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->surchargeDisclosure:Ljava/lang/String;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p1, p2}, Lcom/squareup/print/ThermalBitmapBuilder;->centeredText(Ljava/lang/CharSequence;)Lcom/squareup/print/ThermalBitmapBuilder;

    .line 94
    invoke-virtual {p1}, Lcom/squareup/print/ThermalBitmapBuilder;->smallSpace()Lcom/squareup/print/ThermalBitmapBuilder;

    :cond_1
    return-void
.end method


# virtual methods
.method public final component1()Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->preTaxSurcharges:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    return-object v0
.end method

.method public final component4()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->postTaxSurcharges:Ljava/util/List;

    return-object v0
.end method

.method public final component5()Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    return-object v0
.end method

.method public final component6()Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    return-object v0
.end method

.method public final component7()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->surchargeDisclosure:Ljava/lang/String;

    return-object v0
.end method

.method public final component8()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->alwaysRender:Z

    return v0
.end method

.method public final copy(Lcom/squareup/print/payload/LabelAmountPair;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Ljava/lang/String;Z)Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;"
        }
    .end annotation

    const-string v0, "preTaxSurcharges"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additiveTaxes"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "postTaxSurcharges"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    move-object v1, v0

    move-object v2, p1

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;-><init>(Lcom/squareup/print/payload/LabelAmountPair;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/squareup/print/payload/LabelAmountPair;Lcom/squareup/print/payload/LabelAmountPair;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v1, p1, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->preTaxSurcharges:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->preTaxSurcharges:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->postTaxSurcharges:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->postTaxSurcharges:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v1, p1, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    iget-object v1, p1, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->surchargeDisclosure:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->surchargeDisclosure:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->alwaysRender:Z

    iget-boolean p1, p1, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->alwaysRender:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAdditiveTaxes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    return-object v0
.end method

.method public final getAlwaysRender()Z
    .locals 1

    .line 44
    iget-boolean v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->alwaysRender:Z

    return v0
.end method

.method public final getPostTaxSurcharges()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->postTaxSurcharges:Ljava/util/List;

    return-object v0
.end method

.method public final getPreTaxSurcharges()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->preTaxSurcharges:Ljava/util/List;

    return-object v0
.end method

.method public final getSubtotal()Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    return-object v0
.end method

.method public final getSurchargeDisclosure()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->surchargeDisclosure:Ljava/lang/String;

    return-object v0
.end method

.method public final getSwedishRounding()Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    return-object v0
.end method

.method public final getTip()Lcom/squareup/print/payload/LabelAmountPair;
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->preTaxSurcharges:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->postTaxSurcharges:Ljava/util/List;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->surchargeDisclosure:Ljava/lang/String;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->alwaysRender:Z

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :cond_7
    add-int/2addr v0, v1

    return v0
.end method

.method public final renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V
    .locals 2

    const-string v0, "builder"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->getNonNullAdjustments()Ljava/util/List;

    move-result-object v0

    .line 68
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->alwaysRender:Z

    if-eqz v1, :cond_1

    .line 69
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->renderAdjustments(Lcom/squareup/print/ThermalBitmapBuilder;Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method public final setPreTaxSurcharges(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/payload/LabelAmountPair;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iput-object p1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->preTaxSurcharges:Ljava/util/List;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SubtotalAndAdjustmentsSection(subtotal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->subtotal:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", preTaxSurcharges="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->preTaxSurcharges:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", additiveTaxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->additiveTaxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", postTaxSurcharges="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->postTaxSurcharges:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->tip:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", swedishRounding="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->swedishRounding:Lcom/squareup/print/payload/LabelAmountPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", surchargeDisclosure="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->surchargeDisclosure:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", alwaysRender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->alwaysRender:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
