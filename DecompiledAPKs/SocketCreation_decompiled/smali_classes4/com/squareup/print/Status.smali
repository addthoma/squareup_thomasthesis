.class public final Lcom/squareup/print/Status;
.super Ljava/lang/Object;
.source "Status.java"


# instance fields
.field public final hasError:Z

.field public final isCashDrawerOpen:Z

.field public final isCoverOpen:Z

.field public final isOffline:Z

.field public final isOutOfPaper:Z


# direct methods
.method public constructor <init>(ZZZZZ)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-boolean p1, p0, Lcom/squareup/print/Status;->isCoverOpen:Z

    .line 13
    iput-boolean p2, p0, Lcom/squareup/print/Status;->isOffline:Z

    .line 14
    iput-boolean p3, p0, Lcom/squareup/print/Status;->isCashDrawerOpen:Z

    .line 15
    iput-boolean p4, p0, Lcom/squareup/print/Status;->hasError:Z

    .line 16
    iput-boolean p5, p0, Lcom/squareup/print/Status;->isOutOfPaper:Z

    return-void
.end method
