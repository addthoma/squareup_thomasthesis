.class public interface abstract Lcom/squareup/print/HardwarePrinterTracker$Listener;
.super Ljava/lang/Object;
.source "HardwarePrinterTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/HardwarePrinterTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract printerConnected(Lcom/squareup/print/HardwarePrinter;)V
.end method

.method public abstract printerDisconnected(Lcom/squareup/print/HardwarePrinter;)V
.end method
