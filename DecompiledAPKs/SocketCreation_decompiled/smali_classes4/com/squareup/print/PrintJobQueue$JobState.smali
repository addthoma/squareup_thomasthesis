.class public final enum Lcom/squareup/print/PrintJobQueue$JobState;
.super Ljava/lang/Enum;
.source "PrintJobQueue.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrintJobQueue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "JobState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/print/PrintJobQueue$JobState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/print/PrintJobQueue$JobState;

.field public static final enum ENQUEUED:Lcom/squareup/print/PrintJobQueue$JobState;

.field public static final enum FAILED:Lcom/squareup/print/PrintJobQueue$JobState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 22
    new-instance v0, Lcom/squareup/print/PrintJobQueue$JobState;

    const/4 v1, 0x0

    const-string v2, "ENQUEUED"

    invoke-direct {v0, v2, v1}, Lcom/squareup/print/PrintJobQueue$JobState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJobQueue$JobState;->ENQUEUED:Lcom/squareup/print/PrintJobQueue$JobState;

    .line 27
    new-instance v0, Lcom/squareup/print/PrintJobQueue$JobState;

    const/4 v2, 0x1

    const-string v3, "FAILED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/print/PrintJobQueue$JobState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintJobQueue$JobState;->FAILED:Lcom/squareup/print/PrintJobQueue$JobState;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/print/PrintJobQueue$JobState;

    .line 20
    sget-object v3, Lcom/squareup/print/PrintJobQueue$JobState;->ENQUEUED:Lcom/squareup/print/PrintJobQueue$JobState;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/print/PrintJobQueue$JobState;->FAILED:Lcom/squareup/print/PrintJobQueue$JobState;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/print/PrintJobQueue$JobState;->$VALUES:[Lcom/squareup/print/PrintJobQueue$JobState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/print/PrintJobQueue$JobState;
    .locals 1

    .line 20
    const-class v0, Lcom/squareup/print/PrintJobQueue$JobState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/PrintJobQueue$JobState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/print/PrintJobQueue$JobState;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/print/PrintJobQueue$JobState;->$VALUES:[Lcom/squareup/print/PrintJobQueue$JobState;

    invoke-virtual {v0}, [Lcom/squareup/print/PrintJobQueue$JobState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/print/PrintJobQueue$JobState;

    return-object v0
.end method
