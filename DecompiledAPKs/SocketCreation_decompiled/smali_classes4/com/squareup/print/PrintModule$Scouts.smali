.class public abstract Lcom/squareup/print/PrintModule$Scouts;
.super Ljava/lang/Object;
.source "PrintModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrintModule;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Scouts"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$providePrinterScoutsProvider$0(Ljava/util/Set;)Ljava/util/Set;
    .locals 0

    return-object p0
.end method

.method static providePrinterScoutsProvider(Lcom/squareup/print/StarMicronicsTcpScout;Lcom/squareup/print/StarMicronicsUsbScout;Lcom/squareup/print/EpsonPrinterScouts;)Lcom/squareup/print/PrinterScoutsProvider;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/print/PrinterScout;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 p0, 0x1

    aput-object p1, v0, p0

    .line 62
    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->asSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    .line 66
    invoke-interface {p2}, Lcom/squareup/print/EpsonPrinterScouts;->getScouts()Ljava/util/List;

    move-result-object p1

    invoke-interface {p0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 67
    new-instance p1, Lcom/squareup/print/-$$Lambda$PrintModule$Scouts$0g3CiYLcU1bByTywCIbGbzHiZWo;

    invoke-direct {p1, p0}, Lcom/squareup/print/-$$Lambda$PrintModule$Scouts$0g3CiYLcU1bByTywCIbGbzHiZWo;-><init>(Ljava/util/Set;)V

    return-object p1
.end method
