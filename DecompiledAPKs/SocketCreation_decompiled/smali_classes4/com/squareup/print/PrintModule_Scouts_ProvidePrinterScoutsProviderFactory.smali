.class public final Lcom/squareup/print/PrintModule_Scouts_ProvidePrinterScoutsProviderFactory;
.super Ljava/lang/Object;
.source "PrintModule_Scouts_ProvidePrinterScoutsProviderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/PrinterScoutsProvider;",
        ">;"
    }
.end annotation


# instance fields
.field private final epsonPrinterScoutsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/EpsonPrinterScouts;",
            ">;"
        }
    .end annotation
.end field

.field private final starMicronicsTcpScoutProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/StarMicronicsTcpScout;",
            ">;"
        }
    .end annotation
.end field

.field private final starMicronicsUsbScoutProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/StarMicronicsUsbScout;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/StarMicronicsTcpScout;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/StarMicronicsUsbScout;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/EpsonPrinterScouts;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/print/PrintModule_Scouts_ProvidePrinterScoutsProviderFactory;->starMicronicsTcpScoutProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/print/PrintModule_Scouts_ProvidePrinterScoutsProviderFactory;->starMicronicsUsbScoutProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/print/PrintModule_Scouts_ProvidePrinterScoutsProviderFactory;->epsonPrinterScoutsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/PrintModule_Scouts_ProvidePrinterScoutsProviderFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/StarMicronicsTcpScout;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/StarMicronicsUsbScout;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/EpsonPrinterScouts;",
            ">;)",
            "Lcom/squareup/print/PrintModule_Scouts_ProvidePrinterScoutsProviderFactory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/print/PrintModule_Scouts_ProvidePrinterScoutsProviderFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/PrintModule_Scouts_ProvidePrinterScoutsProviderFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providePrinterScoutsProvider(Lcom/squareup/print/StarMicronicsTcpScout;Lcom/squareup/print/StarMicronicsUsbScout;Lcom/squareup/print/EpsonPrinterScouts;)Lcom/squareup/print/PrinterScoutsProvider;
    .locals 0

    .line 47
    invoke-static {p0, p1, p2}, Lcom/squareup/print/PrintModule$Scouts;->providePrinterScoutsProvider(Lcom/squareup/print/StarMicronicsTcpScout;Lcom/squareup/print/StarMicronicsUsbScout;Lcom/squareup/print/EpsonPrinterScouts;)Lcom/squareup/print/PrinterScoutsProvider;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/PrinterScoutsProvider;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/print/PrinterScoutsProvider;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/print/PrintModule_Scouts_ProvidePrinterScoutsProviderFactory;->starMicronicsTcpScoutProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/StarMicronicsTcpScout;

    iget-object v1, p0, Lcom/squareup/print/PrintModule_Scouts_ProvidePrinterScoutsProviderFactory;->starMicronicsUsbScoutProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/StarMicronicsUsbScout;

    iget-object v2, p0, Lcom/squareup/print/PrintModule_Scouts_ProvidePrinterScoutsProviderFactory;->epsonPrinterScoutsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/EpsonPrinterScouts;

    invoke-static {v0, v1, v2}, Lcom/squareup/print/PrintModule_Scouts_ProvidePrinterScoutsProviderFactory;->providePrinterScoutsProvider(Lcom/squareup/print/StarMicronicsTcpScout;Lcom/squareup/print/StarMicronicsUsbScout;Lcom/squareup/print/EpsonPrinterScouts;)Lcom/squareup/print/PrinterScoutsProvider;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/print/PrintModule_Scouts_ProvidePrinterScoutsProviderFactory;->get()Lcom/squareup/print/PrinterScoutsProvider;

    move-result-object v0

    return-object v0
.end method
