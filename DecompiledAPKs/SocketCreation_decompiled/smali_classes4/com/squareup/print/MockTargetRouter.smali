.class public Lcom/squareup/print/MockTargetRouter;
.super Ljava/lang/Object;
.source "MockTargetRouter.java"

# interfaces
.implements Lcom/squareup/print/PrintTargetRouter;


# instance fields
.field private final printerFound:Lcom/squareup/print/HardwarePrinter;

.field private final printerToFind:Lcom/squareup/print/HardwarePrinter;

.field private routeResult:Lcom/squareup/print/PrintTargetRouter$RouteResult;


# direct methods
.method public constructor <init>(Lcom/squareup/print/HardwarePrinter;Lcom/squareup/print/HardwarePrinter;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/print/MockTargetRouter;->printerToFind:Lcom/squareup/print/HardwarePrinter;

    .line 13
    iput-object p2, p0, Lcom/squareup/print/MockTargetRouter;->printerFound:Lcom/squareup/print/HardwarePrinter;

    return-void
.end method


# virtual methods
.method public retrieveHardwarePrinterFromTarget(Ljava/lang/String;Z)Lkotlin/Pair;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Lkotlin/Pair<",
            "Lcom/squareup/print/PrintTargetRouter$RouteResult;",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;"
        }
    .end annotation

    .line 19
    sget-object p1, Lcom/squareup/print/MockTargetRouter$1;->$SwitchMap$com$squareup$print$PrintTargetRouter$RouteResult:[I

    iget-object p2, p0, Lcom/squareup/print/MockTargetRouter;->routeResult:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    invoke-virtual {p2}, Lcom/squareup/print/PrintTargetRouter$RouteResult;->ordinal()I

    move-result p2

    aget p1, p1, p2

    const/4 p2, 0x1

    if-eq p1, p2, :cond_2

    const/4 p2, 0x2

    if-eq p1, p2, :cond_1

    const/4 p2, 0x3

    if-eq p1, p2, :cond_1

    const/4 p2, 0x4

    if-ne p1, p2, :cond_0

    .line 26
    new-instance p1, Lkotlin/Pair;

    iget-object p2, p0, Lcom/squareup/print/MockTargetRouter;->routeResult:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    iget-object v0, p0, Lcom/squareup/print/MockTargetRouter;->printerToFind:Lcom/squareup/print/HardwarePrinter;

    invoke-direct {p1, p2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    .line 28
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Cannot understand routeResult "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/print/MockTargetRouter;->routeResult:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 24
    :cond_1
    new-instance p1, Lkotlin/Pair;

    iget-object p2, p0, Lcom/squareup/print/MockTargetRouter;->routeResult:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    const/4 v0, 0x0

    invoke-direct {p1, p2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1

    .line 21
    :cond_2
    new-instance p1, Lkotlin/Pair;

    iget-object p2, p0, Lcom/squareup/print/MockTargetRouter;->routeResult:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    iget-object v0, p0, Lcom/squareup/print/MockTargetRouter;->printerFound:Lcom/squareup/print/HardwarePrinter;

    invoke-direct {p1, p2, v0}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p1
.end method

.method public setNextResult(Lcom/squareup/print/PrintTargetRouter$RouteResult;)V
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/squareup/print/MockTargetRouter;->routeResult:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    return-void
.end method
