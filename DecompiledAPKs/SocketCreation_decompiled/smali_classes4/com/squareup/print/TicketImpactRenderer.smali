.class public Lcom/squareup/print/TicketImpactRenderer;
.super Ljava/lang/Object;
.source "TicketImpactRenderer.java"


# instance fields
.field private final builder:Lcom/squareup/print/text/ImpactTextBuilder;


# direct methods
.method public constructor <init>(Lcom/squareup/print/text/ImpactTextBuilder;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    return-void
.end method


# virtual methods
.method public renderText(Lcom/squareup/print/payload/TicketPayload;Z)Lcom/squareup/print/text/RenderedRows;
    .locals 4

    .line 23
    invoke-virtual {p1}, Lcom/squareup/print/payload/TicketPayload;->isCompactTicket()Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendNewline()V

    .line 25
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/text/ImpactTextBuilder;->ticketTopPadding()V

    .line 28
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/payload/TicketPayload;->isVoidTicket()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    sget-object v1, Lcom/squareup/print/text/Style;->RED:Lcom/squareup/print/text/Style;

    invoke-virtual {v0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->setDefaultStyle(Lcom/squareup/print/text/Style;)V

    .line 30
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/TicketPayload;->voidLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->centeredBlock(Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/text/ImpactTextBuilder;->appendNewline()V

    .line 34
    :cond_1
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/TicketPayload;->ticketName:Ljava/lang/String;

    .line 35
    invoke-static {v1}, Lcom/squareup/util/Strings;->isNumeric(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/squareup/print/payload/TicketPayload;->ticketName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    iget-object v1, p1, Lcom/squareup/print/payload/TicketPayload;->ticketName:Ljava/lang/String;

    .line 34
    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->fullWidthLeftAlignedWrappedText(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/text/ImpactTextBuilder;->divider()V

    .line 38
    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->employeeFirstName:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 39
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/TicketPayload;->time:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->fullWidthLeftAlignedWrappedText(Ljava/lang/String;)V

    goto :goto_1

    .line 41
    :cond_3
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/TicketPayload;->time:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/print/payload/TicketPayload;->employeeFirstName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/print/text/ImpactTextBuilder;->twoColumnsLeftExpandingText(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    :goto_1
    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->covers:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 45
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/TicketPayload;->covers:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->fullWidthLeftAlignedWrappedText(Ljava/lang/String;)V

    .line 48
    :cond_4
    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->fulfillmentSection:Lcom/squareup/print/sections/FulfillmentSection;

    if-eqz v0, :cond_5

    .line 49
    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->fulfillmentSection:Lcom/squareup/print/sections/FulfillmentSection;

    iget-object v1, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {v0, v1}, Lcom/squareup/print/sections/FulfillmentSection;->renderText(Lcom/squareup/print/text/ImpactTextBuilder;)V

    .line 52
    :cond_5
    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 53
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/text/ImpactTextBuilder;->divider()V

    .line 54
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/text/ImpactTextBuilder;->spacing()V

    goto :goto_4

    :cond_6
    const/4 v0, 0x0

    .line 58
    iget-object v1, p1, Lcom/squareup/print/payload/TicketPayload;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/sections/TicketItemBlockSection;

    .line 59
    invoke-virtual {v2}, Lcom/squareup/print/sections/TicketItemBlockSection;->maxQuantityWidth()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_2

    .line 62
    :cond_7
    iget-object v1, p1, Lcom/squareup/print/payload/TicketPayload;->items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/sections/TicketItemBlockSection;

    .line 63
    iget-object v3, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {v2, v3, v0}, Lcom/squareup/print/sections/TicketItemBlockSection;->renderText(Lcom/squareup/print/text/ImpactTextBuilder;I)V

    .line 64
    iget-object v2, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {v2}, Lcom/squareup/print/text/ImpactTextBuilder;->spacing()V

    goto :goto_3

    .line 68
    :cond_8
    :goto_4
    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->note:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 69
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/text/ImpactTextBuilder;->divider()V

    .line 70
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/TicketPayload;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->fullWidthLeftAlignedWrappedText(Ljava/lang/String;)V

    .line 73
    :cond_9
    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->deviceName:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 74
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/text/ImpactTextBuilder;->divider()V

    .line 75
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/TicketPayload;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->fullWidthRightAlignedWrappedText(Ljava/lang/String;)V

    .line 78
    :cond_a
    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->receiptNumbers:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->printerStation:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 79
    :cond_b
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {v0}, Lcom/squareup/print/text/ImpactTextBuilder;->divider()V

    .line 80
    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->receiptNumbers:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 81
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/TicketPayload;->receiptNumbers:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->fullWidthLeftAlignedWrappedText(Ljava/lang/String;)V

    .line 83
    :cond_c
    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->printerStation:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 84
    iget-object v0, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    iget-object v1, p1, Lcom/squareup/print/payload/TicketPayload;->printerStation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/print/text/ImpactTextBuilder;->fullWidthLeftAlignedWrappedText(Ljava/lang/String;)V

    :cond_d
    if-eqz p2, :cond_e

    .line 89
    iget-object p2, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {p2}, Lcom/squareup/print/text/ImpactTextBuilder;->divider()V

    .line 90
    iget-object p2, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    iget-object v0, p1, Lcom/squareup/print/payload/TicketPayload;->reprintLabel:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lcom/squareup/print/text/ImpactTextBuilder;->centeredBlock(Ljava/lang/String;)V

    .line 93
    :cond_e
    invoke-virtual {p1}, Lcom/squareup/print/payload/TicketPayload;->isCompactTicket()Z

    move-result p1

    if-nez p1, :cond_f

    .line 94
    iget-object p1, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {p1}, Lcom/squareup/print/text/ImpactTextBuilder;->appendNewline()V

    .line 96
    :cond_f
    iget-object p1, p0, Lcom/squareup/print/TicketImpactRenderer;->builder:Lcom/squareup/print/text/ImpactTextBuilder;

    invoke-virtual {p1}, Lcom/squareup/print/text/ImpactTextBuilder;->render()Lcom/squareup/print/text/RenderedRows;

    move-result-object p1

    return-object p1
.end method
