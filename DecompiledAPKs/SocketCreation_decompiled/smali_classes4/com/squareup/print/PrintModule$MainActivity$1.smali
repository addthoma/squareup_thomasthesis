.class final Lcom/squareup/print/PrintModule$MainActivity$1;
.super Ljava/lang/Object;
.source "PrintModule.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/print/PrintModule$MainActivity;->scopeHardwarePrinterTrackerListeners(Lcom/squareup/print/HardwarePrinterTracker;Ljava/util/Set;)Lmortar/Scoped;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

.field final synthetic val$hardwarePrinterTrackerListeners:Ljava/util/Set;


# direct methods
.method constructor <init>(Ljava/util/Set;Lcom/squareup/print/HardwarePrinterTracker;)V
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/print/PrintModule$MainActivity$1;->val$hardwarePrinterTrackerListeners:Ljava/util/Set;

    iput-object p2, p0, Lcom/squareup/print/PrintModule$MainActivity$1;->val$hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 155
    iget-object p1, p0, Lcom/squareup/print/PrintModule$MainActivity$1;->val$hardwarePrinterTrackerListeners:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/HardwarePrinterTracker$Listener;

    .line 156
    iget-object v1, p0, Lcom/squareup/print/PrintModule$MainActivity$1;->val$hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v1, v0}, Lcom/squareup/print/HardwarePrinterTracker;->addListener(Lcom/squareup/print/HardwarePrinterTracker$Listener;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 3

    .line 161
    iget-object v0, p0, Lcom/squareup/print/PrintModule$MainActivity$1;->val$hardwarePrinterTrackerListeners:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/HardwarePrinterTracker$Listener;

    .line 162
    iget-object v2, p0, Lcom/squareup/print/PrintModule$MainActivity$1;->val$hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v2, v1}, Lcom/squareup/print/HardwarePrinterTracker;->removeListener(Lcom/squareup/print/HardwarePrinterTracker$Listener;)V

    goto :goto_0

    :cond_0
    return-void
.end method
