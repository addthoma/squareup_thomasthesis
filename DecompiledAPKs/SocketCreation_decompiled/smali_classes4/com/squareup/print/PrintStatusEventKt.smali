.class public final Lcom/squareup/print/PrintStatusEventKt;
.super Ljava/lang/Object;
.source "PrintStatusEvent.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u00a8\u0006\u0004"
    }
    d2 = {
        "forPrintEnqueued",
        "Lcom/squareup/print/PrintStatusEvent;",
        "job",
        "Lcom/squareup/print/PrintJob;",
        "hardware_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final forPrintEnqueued(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/PrintStatusEvent;
    .locals 3

    const-string v0, "job"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    new-instance v0, Lcom/squareup/print/PrintStatusEvent;

    sget-object v1, Lcom/squareup/eventstream/v1/EventStream$Name;->ACTION:Lcom/squareup/eventstream/v1/EventStream$Name;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->PRINT_JOB_ENQUEUED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/print/PrintStatusEvent;-><init>(Lcom/squareup/eventstream/v1/EventStream$Name;Lcom/squareup/analytics/RegisterActionName;Lcom/squareup/print/PrintJob;)V

    return-object v0
.end method
