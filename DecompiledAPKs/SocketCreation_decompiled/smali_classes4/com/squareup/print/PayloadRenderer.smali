.class public interface abstract Lcom/squareup/print/PayloadRenderer;
.super Ljava/lang/Object;
.source "PayloadRenderer.java"


# virtual methods
.method public abstract renderBitmap(Lcom/squareup/print/PrintablePayload;Lcom/squareup/print/HardwarePrinter$BitmapRenderingSpecs;Lcom/squareup/print/PrintJob;)Landroid/graphics/Bitmap;
.end method

.method public abstract renderText(Lcom/squareup/print/PrintablePayload;Lcom/squareup/print/PrintJob;I)Lcom/squareup/print/text/RenderedRows;
.end method
