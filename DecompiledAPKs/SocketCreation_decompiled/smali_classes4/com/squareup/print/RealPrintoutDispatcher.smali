.class public final Lcom/squareup/print/RealPrintoutDispatcher;
.super Ljava/lang/Object;
.source "PrintoutDispatcher.kt"

# interfaces
.implements Lcom/squareup/print/PrintoutDispatcher;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPrintoutDispatcher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PrintoutDispatcher.kt\ncom/squareup/print/RealPrintoutDispatcher\n+ 2 RxKotlin.kt\ncom/squareup/util/rx2/Singles\n+ 3 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,214:1\n1452#2:215\n250#3,2:216\n*E\n*S KotlinDebug\n*F\n+ 1 PrintoutDispatcher.kt\ncom/squareup/print/RealPrintoutDispatcher\n*L\n124#1:215\n193#1,2:216\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ(\u0010\u000b\u001a\u000c\u0012\u0008\u0012\u00060\rj\u0002`\u000e0\u000c2\u0006\u0010\u000f\u001a\u00020\u00102\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012H\u0016J(\u0010\u0014\u001a\u000c\u0012\u0008\u0012\u00060\rj\u0002`\u000e0\u000c2\u0006\u0010\u000f\u001a\u00020\u00152\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012H\u0016J(\u0010\u0016\u001a\u000c\u0012\u0008\u0012\u00060\rj\u0002`\u000e0\u000c2\u0006\u0010\u000f\u001a\u00020\u00172\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012H\u0016J(\u0010\u0018\u001a\u000c\u0012\u0008\u0012\u00060\rj\u0002`\u000e0\u000c2\u0006\u0010\u000f\u001a\u00020\u00192\u000c\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012H\u0016J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0016\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u000c2\u0006\u0010\u001f\u001a\u00020\u001dH\u0002J\u000e\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u001d0\u000cH\u0002J\u0010\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$H\u0002J\u0012\u0010%\u001a\u000c\u0012\u0008\u0012\u00060\rj\u0002`\u000e0\u000cH\u0002J\u000c\u0010&\u001a\u00020\'*\u00020\u001dH\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/print/RealPrintoutDispatcher;",
        "Lcom/squareup/print/PrintoutDispatcher;",
        "orderPrintingDispatcher",
        "Lcom/squareup/print/OrderPrintingDispatcher;",
        "receiptSender",
        "Lcom/squareup/receipt/ReceiptSender;",
        "printSpooler",
        "Lcom/squareup/print/PrintSpooler;",
        "hardwarePrinterTracker",
        "Lcom/squareup/print/HardwarePrinterTracker;",
        "(Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/HardwarePrinterTracker;)V",
        "dispatchAuthSlip",
        "Lio/reactivex/Single;",
        "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
        "Lcom/squareup/print/PrintJobResult;",
        "printout",
        "Lcom/squareup/print/AuthSlipPrintout;",
        "target",
        "",
        "Lcom/squareup/print/PrinterStation;",
        "dispatchOrderTicket",
        "Lcom/squareup/print/TicketStubPrintout;",
        "dispatchReceipt",
        "Lcom/squareup/print/ReceiptPrintout;",
        "dispatchVoidTicket",
        "Lcom/squareup/print/VoidTicketPrintout;",
        "getPrinter",
        "Lcom/squareup/print/HardwarePrinter;",
        "forPrintJob",
        "Lcom/squareup/print/PrintJob;",
        "listenForAttemptedPrintJob",
        "targetPrintJob",
        "listenForEnqueuedPrintJob",
        "throwForPrinterNotFound",
        "",
        "hardwareInfo",
        "Lcom/squareup/print/HardwarePrinter$HardwareInfo;",
        "waitForEnqueuedJobResult",
        "isAttempted",
        "",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final printSpooler:Lcom/squareup/print/PrintSpooler;

.field private final receiptSender:Lcom/squareup/receipt/ReceiptSender;


# direct methods
.method public constructor <init>(Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/receipt/ReceiptSender;Lcom/squareup/print/PrintSpooler;Lcom/squareup/print/HardwarePrinterTracker;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderPrintingDispatcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "receiptSender"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printSpooler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "hardwarePrinterTracker"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iput-object p2, p0, Lcom/squareup/print/RealPrintoutDispatcher;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    iput-object p3, p0, Lcom/squareup/print/RealPrintoutDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    iput-object p4, p0, Lcom/squareup/print/RealPrintoutDispatcher;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    return-void
.end method

.method public static final synthetic access$getOrderPrintingDispatcher$p(Lcom/squareup/print/RealPrintoutDispatcher;)Lcom/squareup/print/OrderPrintingDispatcher;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/squareup/print/RealPrintoutDispatcher;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    return-object p0
.end method

.method public static final synthetic access$getPrintSpooler$p(Lcom/squareup/print/RealPrintoutDispatcher;)Lcom/squareup/print/PrintSpooler;
    .locals 0

    .line 66
    iget-object p0, p0, Lcom/squareup/print/RealPrintoutDispatcher;->printSpooler:Lcom/squareup/print/PrintSpooler;

    return-object p0
.end method

.method public static final synthetic access$getPrinter(Lcom/squareup/print/RealPrintoutDispatcher;Lcom/squareup/print/PrintJob;)Lcom/squareup/print/HardwarePrinter;
    .locals 0

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/print/RealPrintoutDispatcher;->getPrinter(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/HardwarePrinter;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$isAttempted(Lcom/squareup/print/RealPrintoutDispatcher;Lcom/squareup/print/PrintJob;)Z
    .locals 0

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/print/RealPrintoutDispatcher;->isAttempted(Lcom/squareup/print/PrintJob;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$listenForAttemptedPrintJob(Lcom/squareup/print/RealPrintoutDispatcher;Lcom/squareup/print/PrintJob;)Lio/reactivex/Single;
    .locals 0

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/print/RealPrintoutDispatcher;->listenForAttemptedPrintJob(Lcom/squareup/print/PrintJob;)Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$waitForEnqueuedJobResult(Lcom/squareup/print/RealPrintoutDispatcher;)Lio/reactivex/Single;
    .locals 0

    .line 66
    invoke-direct {p0}, Lcom/squareup/print/RealPrintoutDispatcher;->waitForEnqueuedJobResult()Lio/reactivex/Single;

    move-result-object p0

    return-object p0
.end method

.method private final getPrinter(Lcom/squareup/print/PrintJob;)Lcom/squareup/print/HardwarePrinter;
    .locals 5

    .line 191
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/print/PrintJob$PrintAttempt;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    const-string v0, "forPrintJob.latestPrintAttempt.hardwareInfo"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/squareup/print/RealPrintoutDispatcher;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v0}, Lcom/squareup/print/HardwarePrinterTracker;->getAllAvailableHardwarePrinters()Ljava/util/Collection;

    move-result-object v0

    const-string v1, "hardwarePrinterTracker.a\u2026AvailableHardwarePrinters"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Iterable;

    .line 216
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/print/HardwarePrinter;

    const-string v4, "it"

    .line 193
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/print/HardwarePrinter;->getHardwareInfo()Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    move-result-object v3

    const-string v4, "it.hardwareInfo"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 217
    :goto_0
    check-cast v1, Lcom/squareup/print/HardwarePrinter;

    if-eqz v1, :cond_2

    return-object v1

    .line 194
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/print/RealPrintoutDispatcher;->throwForPrinterNotFound(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Ljava/lang/Void;

    throw v2
.end method

.method private final isAttempted(Lcom/squareup/print/PrintJob;)Z
    .locals 1

    .line 211
    invoke-virtual {p1}, Lcom/squareup/print/PrintJob;->getLatestPrintAttempt()Lcom/squareup/print/PrintJob$PrintAttempt;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/print/PrintJob$PrintAttempt;->result:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    sget-object v0, Lcom/squareup/print/PrintJob$PrintAttempt$Result;->NOT_YET_ATTEMPTED:Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    if-eq p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private final listenForAttemptedPrintJob(Lcom/squareup/print/PrintJob;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrintJob;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/print/PrintJob;",
            ">;"
        }
    .end annotation

    .line 174
    new-instance v0, Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1;-><init>(Lcom/squareup/print/RealPrintoutDispatcher;Lcom/squareup/print/PrintJob;)V

    check-cast v0, Lio/reactivex/SingleOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "Single.create { emitter \u2026tJobStatusListener)\n    }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method private final listenForEnqueuedPrintJob()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/print/PrintJob;",
            ">;"
        }
    .end annotation

    .line 158
    new-instance v0, Lcom/squareup/print/RealPrintoutDispatcher$listenForEnqueuedPrintJob$1;

    invoke-direct {v0, p0}, Lcom/squareup/print/RealPrintoutDispatcher$listenForEnqueuedPrintJob$1;-><init>(Lcom/squareup/print/RealPrintoutDispatcher;)V

    check-cast v0, Lio/reactivex/SingleOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "Single.create { emitter \u2026tJobStatusListener)\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final throwForPrinterNotFound(Lcom/squareup/print/HardwarePrinter$HardwareInfo;)Ljava/lang/Void;
    .locals 11

    .line 198
    iget-object v0, p0, Lcom/squareup/print/RealPrintoutDispatcher;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {v0}, Lcom/squareup/print/HardwarePrinterTracker;->getAllAvailableHardwarePrinters()Ljava/util/Collection;

    move-result-object v0

    const-string v1, "hardwarePrinterTracker.a\u2026AvailableHardwarePrinters"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v2, v0

    check-cast v2, Ljava/lang/Iterable;

    const-string v0, "["

    .line 199
    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string v0, "]"

    .line 200
    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    .line 201
    sget-object v0, Lcom/squareup/print/RealPrintoutDispatcher$throwForPrinterNotFound$availablePrinterIds$1;->INSTANCE:Lcom/squareup/print/RealPrintoutDispatcher$throwForPrinterNotFound$availablePrinterIds$1;

    move-object v8, v0

    check-cast v8, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x19

    const/4 v10, 0x0

    .line 198
    invoke-static/range {v2 .. v10}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 205
    new-instance v1, Ljava/lang/IllegalStateException;

    .line 206
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Didn\'t find printer "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " in available printers: "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 205
    invoke-direct {v1, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method private final waitForEnqueuedJobResult()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
            ">;"
        }
    .end annotation

    .line 151
    invoke-direct {p0}, Lcom/squareup/print/RealPrintoutDispatcher;->listenForEnqueuedPrintJob()Lio/reactivex/Single;

    move-result-object v0

    .line 152
    new-instance v1, Lcom/squareup/print/RealPrintoutDispatcher$waitForEnqueuedJobResult$1;

    invoke-direct {v1, p0}, Lcom/squareup/print/RealPrintoutDispatcher$waitForEnqueuedJobResult$1;-><init>(Lcom/squareup/print/RealPrintoutDispatcher;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 153
    new-instance v1, Lcom/squareup/print/RealPrintoutDispatcher$waitForEnqueuedJobResult$2;

    invoke-direct {v1, p0}, Lcom/squareup/print/RealPrintoutDispatcher$waitForEnqueuedJobResult$2;-><init>(Lcom/squareup/print/RealPrintoutDispatcher;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "listenForEnqueuedPrintJo\u2026etPrinterAndGetStatus() }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public dispatchAuthSlip(Lcom/squareup/print/AuthSlipPrintout;Ljava/util/Collection;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/AuthSlipPrintout;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
            ">;"
        }
    .end annotation

    const-string v0, "printout"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "target"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    new-instance v0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/RealPrintoutDispatcher$dispatchAuthSlip$1;-><init>(Lcom/squareup/print/RealPrintoutDispatcher;Lcom/squareup/print/AuthSlipPrintout;Ljava/util/Collection;)V

    check-cast v0, Lio/reactivex/SingleOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.create { emitter \u2026opy, target\n      )\n    }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public dispatchOrderTicket(Lcom/squareup/print/TicketStubPrintout;Ljava/util/Collection;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/TicketStubPrintout;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
            ">;"
        }
    .end annotation

    const-string v0, "printout"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "target"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    new-instance v0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;-><init>(Lcom/squareup/print/RealPrintoutDispatcher;Lcom/squareup/print/TicketStubPrintout;Ljava/util/Collection;)V

    check-cast v0, Lio/reactivex/SingleOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.create { emitter \u2026out.type}\")\n      }\n    }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public dispatchReceipt(Lcom/squareup/print/ReceiptPrintout;Ljava/util/Collection;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/ReceiptPrintout;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
            ">;"
        }
    .end annotation

    const-string v0, "printout"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "target"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    new-instance v0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchReceipt$callbackObs$1;

    invoke-direct {v0, p0}, Lcom/squareup/print/RealPrintoutDispatcher$dispatchReceipt$callbackObs$1;-><init>(Lcom/squareup/print/RealPrintoutDispatcher;)V

    check-cast v0, Lio/reactivex/SingleOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object v0

    const-string v1, "Single.create<PrintJobRe\u2026lable(sub::dispose)\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    iget-object v1, p0, Lcom/squareup/print/RealPrintoutDispatcher;->receiptSender:Lcom/squareup/receipt/ReceiptSender;

    invoke-virtual {p1}, Lcom/squareup/print/ReceiptPrintout;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/print/ReceiptPrintout;->getReceiptRenderType()Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    move-result-object p1

    invoke-interface {v1, v2, p1, p2}, Lcom/squareup/receipt/ReceiptSender;->onMaybePrintReceipt(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Ljava/util/Collection;)Lrx/Observable;

    move-result-object p1

    const-string p2, "receiptSender.onMaybePri\u2026eceiptRenderType, target)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 121
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    const-wide/16 v1, 0x1

    .line 122
    invoke-virtual {p1, v1, v2}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object p1

    .line 123
    invoke-virtual {p1}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object p1

    .line 124
    sget-object p2, Lcom/squareup/util/rx2/Singles;->INSTANCE:Lcom/squareup/util/rx2/Singles;

    const-string p2, "printObs"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lio/reactivex/SingleSource;

    .line 215
    check-cast v0, Lio/reactivex/SingleSource;

    new-instance p2, Lcom/squareup/print/RealPrintoutDispatcher$dispatchReceipt$$inlined$zip$1;

    invoke-direct {p2}, Lcom/squareup/print/RealPrintoutDispatcher$dispatchReceipt$$inlined$zip$1;-><init>()V

    check-cast p2, Lio/reactivex/functions/BiFunction;

    invoke-static {v0, p1, p2}, Lio/reactivex/Single;->zip(Lio/reactivex/SingleSource;Lio/reactivex/SingleSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.zip(s1, s2, BiFun\u2026-> zipper.invoke(t, u) })"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public dispatchVoidTicket(Lcom/squareup/print/VoidTicketPrintout;Ljava/util/Collection;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/VoidTicketPrintout;",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
            ">;"
        }
    .end annotation

    const-string v0, "printout"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "target"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    new-instance v0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchVoidTicket$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/RealPrintoutDispatcher$dispatchVoidTicket$1;-><init>(Lcom/squareup/print/RealPrintoutDispatcher;Lcom/squareup/print/VoidTicketPrintout;Ljava/util/Collection;)V

    check-cast v0, Lio/reactivex/SingleOnSubscribe;

    invoke-static {v0}, Lio/reactivex/Single;->create(Lio/reactivex/SingleOnSubscribe;)Lio/reactivex/Single;

    move-result-object p1

    const-string p2, "Single.create { emitter \u2026ems, target\n      )\n    }"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
