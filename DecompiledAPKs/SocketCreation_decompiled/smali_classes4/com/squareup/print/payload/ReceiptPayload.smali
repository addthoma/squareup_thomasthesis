.class public final Lcom/squareup/print/payload/ReceiptPayload;
.super Lcom/squareup/print/PrintablePayload;
.source "ReceiptPayload.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;,
        Lcom/squareup/print/payload/ReceiptPayload$RenderType;,
        Lcom/squareup/print/payload/ReceiptPayload$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nReceiptPayload.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ReceiptPayload.kt\ncom/squareup/print/payload/ReceiptPayload\n*L\n1#1,127:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u00086\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0003ijkB\u00a1\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0010\u0008\u001a\u0004\u0018\u00010\t\u0012\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u0012\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u0012\u0008\u0010\u0014\u001a\u0004\u0018\u00010\u0015\u0012\u0008\u0010\u0016\u001a\u0004\u0018\u00010\u0017\u0012\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019\u0012\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0008\u0010\"\u001a\u0004\u0018\u00010#\u00a2\u0006\u0002\u0010$J\t\u0010G\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010H\u001a\u0004\u0018\u00010\u0015H\u00c6\u0003J\u000b\u0010I\u001a\u0004\u0018\u00010\u0017H\u00c6\u0003J\u000b\u0010J\u001a\u0004\u0018\u00010\u0019H\u00c6\u0003J\u000b\u0010K\u001a\u0004\u0018\u00010\u001bH\u00c6\u0003J\t\u0010L\u001a\u00020\u001dH\u00c6\u0003J\u000b\u0010M\u001a\u0004\u0018\u00010\u001fH\u00c6\u0003J\t\u0010N\u001a\u00020!H\u00c6\u0003J\u000b\u0010O\u001a\u0004\u0018\u00010#H\u00c6\u0003J\t\u0010P\u001a\u00020\u0005H\u00c6\u0003J\t\u0010Q\u001a\u00020\u0007H\u00c6\u0003J\u000b\u0010R\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u000b\u0010S\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003J\t\u0010T\u001a\u00020\rH\u00c6\u0003J\t\u0010U\u001a\u00020\u000fH\u00c6\u0003J\u000b\u0010V\u001a\u0004\u0018\u00010\u0011H\u00c6\u0003J\u000b\u0010W\u001a\u0004\u0018\u00010\u0013H\u00c6\u0003J\u00c7\u0001\u0010X\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\n\u0008\u0002\u0010\u0008\u001a\u0004\u0018\u00010\t2\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000f2\n\u0008\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00112\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00132\n\u0008\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00152\n\u0008\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u00172\n\u0008\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u00192\n\u0008\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\u0008\u0008\u0002\u0010\u001c\u001a\u00020\u001d2\n\u0008\u0002\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0008\u0008\u0002\u0010 \u001a\u00020!2\n\u0008\u0002\u0010\"\u001a\u0004\u0018\u00010#H\u00c6\u0001J\u0013\u0010Y\u001a\u00020Z2\u0008\u0010[\u001a\u0004\u0018\u00010\\H\u00d6\u0003J\n\u0010]\u001a\u0004\u0018\u00010^H\u0016J\u0008\u0010_\u001a\u00020`H\u0016J\t\u0010a\u001a\u00020bH\u00d6\u0001J\u0018\u0010c\u001a\u00020d2\u0006\u0010e\u001a\u00020f2\u0006\u0010g\u001a\u00020ZH\u0016J\t\u0010h\u001a\u00020`H\u00d6\u0001R\u0013\u0010\u001e\u001a\u0004\u0018\u00010\u001f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008%\u0010&R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010(R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008)\u0010*R\u0011\u0010\u001c\u001a\u00020\u001d\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008+\u0010,R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008-\u0010.R\u0013\u0010\u0008\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008/\u00100R\u0013\u0010\u001a\u001a\u0004\u0018\u00010\u001b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00081\u00102R\u0013\u0010\"\u001a\u0004\u0018\u00010#\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00083\u00104R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00085\u00106R\u0011\u0010 \u001a\u00020!\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00087\u00108R\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0015\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00089\u0010:R\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008;\u0010<R\u0013\u0010\u0018\u001a\u0004\u0018\u00010\u0019\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008=\u0010>R\u0013\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008?\u0010@R\u0011\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008A\u0010BR\u0013\u0010\u0016\u001a\u0004\u0018\u00010\u0017\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008C\u0010DR\u0011\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008E\u0010F\u00a8\u0006l"
    }
    d2 = {
        "Lcom/squareup/print/payload/ReceiptPayload;",
        "Lcom/squareup/print/PrintablePayload;",
        "header",
        "Lcom/squareup/print/sections/HeaderSection;",
        "codes",
        "Lcom/squareup/print/sections/CodesSection;",
        "emv",
        "Lcom/squareup/print/sections/EmvSection;",
        "itemsAndDiscounts",
        "Lcom/squareup/print/sections/ItemsAndDiscountsSection;",
        "subtotalSection",
        "Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;",
        "totalSection",
        "Lcom/squareup/print/sections/TotalSection;",
        "tender",
        "Lcom/squareup/print/sections/TenderSection;",
        "refunds",
        "Lcom/squareup/print/sections/RefundsSection;",
        "returns",
        "Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;",
        "returnSubtotals",
        "Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;",
        "tip",
        "Lcom/squareup/print/papersig/TipSections;",
        "signature",
        "Lcom/squareup/print/papersig/SignatureSection;",
        "multipleTaxBreakdown",
        "Lcom/squareup/print/sections/MultipleTaxBreakdownSection;",
        "footer",
        "Lcom/squareup/print/sections/FooterSection;",
        "barcode",
        "Lcom/squareup/print/sections/BarcodeSection;",
        "renderType",
        "Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
        "receiptType",
        "Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;",
        "(Lcom/squareup/print/sections/HeaderSection;Lcom/squareup/print/sections/CodesSection;Lcom/squareup/print/sections/EmvSection;Lcom/squareup/print/sections/ItemsAndDiscountsSection;Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;Lcom/squareup/print/sections/TotalSection;Lcom/squareup/print/sections/TenderSection;Lcom/squareup/print/sections/RefundsSection;Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;Lcom/squareup/print/papersig/TipSections;Lcom/squareup/print/papersig/SignatureSection;Lcom/squareup/print/sections/MultipleTaxBreakdownSection;Lcom/squareup/print/sections/FooterSection;Lcom/squareup/print/sections/BarcodeSection;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;)V",
        "getBarcode",
        "()Lcom/squareup/print/sections/BarcodeSection;",
        "getCodes",
        "()Lcom/squareup/print/sections/CodesSection;",
        "getEmv",
        "()Lcom/squareup/print/sections/EmvSection;",
        "getFooter",
        "()Lcom/squareup/print/sections/FooterSection;",
        "getHeader",
        "()Lcom/squareup/print/sections/HeaderSection;",
        "getItemsAndDiscounts",
        "()Lcom/squareup/print/sections/ItemsAndDiscountsSection;",
        "getMultipleTaxBreakdown",
        "()Lcom/squareup/print/sections/MultipleTaxBreakdownSection;",
        "getReceiptType",
        "()Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;",
        "getRefunds",
        "()Lcom/squareup/print/sections/RefundsSection;",
        "getRenderType",
        "()Lcom/squareup/print/payload/ReceiptPayload$RenderType;",
        "getReturnSubtotals",
        "()Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;",
        "getReturns",
        "()Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;",
        "getSignature",
        "()Lcom/squareup/print/papersig/SignatureSection;",
        "getSubtotalSection",
        "()Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;",
        "getTender",
        "()Lcom/squareup/print/sections/TenderSection;",
        "getTip",
        "()Lcom/squareup/print/papersig/TipSections;",
        "getTotalSection",
        "()Lcom/squareup/print/sections/TotalSection;",
        "component1",
        "component10",
        "component11",
        "component12",
        "component13",
        "component14",
        "component15",
        "component16",
        "component17",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "copy",
        "equals",
        "",
        "other",
        "",
        "getAnalyticsName",
        "Lcom/squareup/analytics/RegisterActionName;",
        "getAnalyticsPrintJobType",
        "",
        "hashCode",
        "",
        "renderBitmap",
        "Landroid/graphics/Bitmap;",
        "thermalBitmapBuilder",
        "Lcom/squareup/print/ThermalBitmapBuilder;",
        "isReprint",
        "toString",
        "Builder",
        "ReceiptType",
        "RenderType",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final barcode:Lcom/squareup/print/sections/BarcodeSection;

.field private final codes:Lcom/squareup/print/sections/CodesSection;

.field private final emv:Lcom/squareup/print/sections/EmvSection;

.field private final footer:Lcom/squareup/print/sections/FooterSection;

.field private final header:Lcom/squareup/print/sections/HeaderSection;

.field private final itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

.field private final multipleTaxBreakdown:Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

.field private final receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

.field private final refunds:Lcom/squareup/print/sections/RefundsSection;

.field private final renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

.field private final returnSubtotals:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

.field private final returns:Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

.field private final signature:Lcom/squareup/print/papersig/SignatureSection;

.field private final subtotalSection:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

.field private final tender:Lcom/squareup/print/sections/TenderSection;

.field private final tip:Lcom/squareup/print/papersig/TipSections;

.field private final totalSection:Lcom/squareup/print/sections/TotalSection;


# direct methods
.method public constructor <init>(Lcom/squareup/print/sections/HeaderSection;Lcom/squareup/print/sections/CodesSection;Lcom/squareup/print/sections/EmvSection;Lcom/squareup/print/sections/ItemsAndDiscountsSection;Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;Lcom/squareup/print/sections/TotalSection;Lcom/squareup/print/sections/TenderSection;Lcom/squareup/print/sections/RefundsSection;Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;Lcom/squareup/print/papersig/TipSections;Lcom/squareup/print/papersig/SignatureSection;Lcom/squareup/print/sections/MultipleTaxBreakdownSection;Lcom/squareup/print/sections/FooterSection;Lcom/squareup/print/sections/BarcodeSection;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;)V
    .locals 9

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p14

    move-object/from16 v7, p16

    const-string v8, "header"

    invoke-static {p1, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "codes"

    invoke-static {p2, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "emv"

    invoke-static {p3, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "totalSection"

    invoke-static {p6, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "tender"

    invoke-static {v5, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "footer"

    invoke-static {v6, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v8, "renderType"

    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0}, Lcom/squareup/print/PrintablePayload;-><init>()V

    iput-object v1, v0, Lcom/squareup/print/payload/ReceiptPayload;->header:Lcom/squareup/print/sections/HeaderSection;

    iput-object v2, v0, Lcom/squareup/print/payload/ReceiptPayload;->codes:Lcom/squareup/print/sections/CodesSection;

    iput-object v3, v0, Lcom/squareup/print/payload/ReceiptPayload;->emv:Lcom/squareup/print/sections/EmvSection;

    move-object v1, p4

    iput-object v1, v0, Lcom/squareup/print/payload/ReceiptPayload;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    move-object v1, p5

    iput-object v1, v0, Lcom/squareup/print/payload/ReceiptPayload;->subtotalSection:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    iput-object v4, v0, Lcom/squareup/print/payload/ReceiptPayload;->totalSection:Lcom/squareup/print/sections/TotalSection;

    iput-object v5, v0, Lcom/squareup/print/payload/ReceiptPayload;->tender:Lcom/squareup/print/sections/TenderSection;

    move-object/from16 v1, p8

    iput-object v1, v0, Lcom/squareup/print/payload/ReceiptPayload;->refunds:Lcom/squareup/print/sections/RefundsSection;

    move-object/from16 v1, p9

    iput-object v1, v0, Lcom/squareup/print/payload/ReceiptPayload;->returns:Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    move-object/from16 v1, p10

    iput-object v1, v0, Lcom/squareup/print/payload/ReceiptPayload;->returnSubtotals:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    move-object/from16 v1, p11

    iput-object v1, v0, Lcom/squareup/print/payload/ReceiptPayload;->tip:Lcom/squareup/print/papersig/TipSections;

    move-object/from16 v1, p12

    iput-object v1, v0, Lcom/squareup/print/payload/ReceiptPayload;->signature:Lcom/squareup/print/papersig/SignatureSection;

    move-object/from16 v1, p13

    iput-object v1, v0, Lcom/squareup/print/payload/ReceiptPayload;->multipleTaxBreakdown:Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    iput-object v6, v0, Lcom/squareup/print/payload/ReceiptPayload;->footer:Lcom/squareup/print/sections/FooterSection;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/squareup/print/payload/ReceiptPayload;->barcode:Lcom/squareup/print/sections/BarcodeSection;

    iput-object v7, v0, Lcom/squareup/print/payload/ReceiptPayload;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    move-object/from16 v1, p17

    iput-object v1, v0, Lcom/squareup/print/payload/ReceiptPayload;->receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/print/payload/ReceiptPayload;Lcom/squareup/print/sections/HeaderSection;Lcom/squareup/print/sections/CodesSection;Lcom/squareup/print/sections/EmvSection;Lcom/squareup/print/sections/ItemsAndDiscountsSection;Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;Lcom/squareup/print/sections/TotalSection;Lcom/squareup/print/sections/TenderSection;Lcom/squareup/print/sections/RefundsSection;Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;Lcom/squareup/print/papersig/TipSections;Lcom/squareup/print/papersig/SignatureSection;Lcom/squareup/print/sections/MultipleTaxBreakdownSection;Lcom/squareup/print/sections/FooterSection;Lcom/squareup/print/sections/BarcodeSection;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;ILjava/lang/Object;)Lcom/squareup/print/payload/ReceiptPayload;
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p18

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/squareup/print/payload/ReceiptPayload;->header:Lcom/squareup/print/sections/HeaderSection;

    goto :goto_0

    :cond_0
    move-object/from16 v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/squareup/print/payload/ReceiptPayload;->codes:Lcom/squareup/print/sections/CodesSection;

    goto :goto_1

    :cond_1
    move-object/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/print/payload/ReceiptPayload;->emv:Lcom/squareup/print/sections/EmvSection;

    goto :goto_2

    :cond_2
    move-object/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/print/payload/ReceiptPayload;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    goto :goto_3

    :cond_3
    move-object/from16 v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/print/payload/ReceiptPayload;->subtotalSection:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    goto :goto_4

    :cond_4
    move-object/from16 v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/print/payload/ReceiptPayload;->totalSection:Lcom/squareup/print/sections/TotalSection;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/print/payload/ReceiptPayload;->tender:Lcom/squareup/print/sections/TenderSection;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/print/payload/ReceiptPayload;->refunds:Lcom/squareup/print/sections/RefundsSection;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v1, 0x100

    if-eqz v10, :cond_8

    iget-object v10, v0, Lcom/squareup/print/payload/ReceiptPayload;->returns:Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v1, 0x200

    if-eqz v11, :cond_9

    iget-object v11, v0, Lcom/squareup/print/payload/ReceiptPayload;->returnSubtotals:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v1, 0x400

    if-eqz v12, :cond_a

    iget-object v12, v0, Lcom/squareup/print/payload/ReceiptPayload;->tip:Lcom/squareup/print/papersig/TipSections;

    goto :goto_a

    :cond_a
    move-object/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v1, 0x800

    if-eqz v13, :cond_b

    iget-object v13, v0, Lcom/squareup/print/payload/ReceiptPayload;->signature:Lcom/squareup/print/papersig/SignatureSection;

    goto :goto_b

    :cond_b
    move-object/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v1, 0x1000

    if-eqz v14, :cond_c

    iget-object v14, v0, Lcom/squareup/print/payload/ReceiptPayload;->multipleTaxBreakdown:Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    goto :goto_c

    :cond_c
    move-object/from16 v14, p13

    :goto_c
    and-int/lit16 v15, v1, 0x2000

    if-eqz v15, :cond_d

    iget-object v15, v0, Lcom/squareup/print/payload/ReceiptPayload;->footer:Lcom/squareup/print/sections/FooterSection;

    goto :goto_d

    :cond_d
    move-object/from16 v15, p14

    :goto_d
    move-object/from16 p14, v15

    and-int/lit16 v15, v1, 0x4000

    if-eqz v15, :cond_e

    iget-object v15, v0, Lcom/squareup/print/payload/ReceiptPayload;->barcode:Lcom/squareup/print/sections/BarcodeSection;

    goto :goto_e

    :cond_e
    move-object/from16 v15, p15

    :goto_e
    const v16, 0x8000

    and-int v16, v1, v16

    move-object/from16 p15, v15

    if-eqz v16, :cond_f

    iget-object v15, v0, Lcom/squareup/print/payload/ReceiptPayload;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    goto :goto_f

    :cond_f
    move-object/from16 v15, p16

    :goto_f
    const/high16 v16, 0x10000

    and-int v1, v1, v16

    if-eqz v1, :cond_10

    iget-object v1, v0, Lcom/squareup/print/payload/ReceiptPayload;->receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    goto :goto_10

    :cond_10
    move-object/from16 v1, p17

    :goto_10
    move-object/from16 p1, v2

    move-object/from16 p2, v3

    move-object/from16 p3, v4

    move-object/from16 p4, v5

    move-object/from16 p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v10

    move-object/from16 p10, v11

    move-object/from16 p11, v12

    move-object/from16 p12, v13

    move-object/from16 p13, v14

    move-object/from16 p16, v15

    move-object/from16 p17, v1

    invoke-virtual/range {p0 .. p17}, Lcom/squareup/print/payload/ReceiptPayload;->copy(Lcom/squareup/print/sections/HeaderSection;Lcom/squareup/print/sections/CodesSection;Lcom/squareup/print/sections/EmvSection;Lcom/squareup/print/sections/ItemsAndDiscountsSection;Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;Lcom/squareup/print/sections/TotalSection;Lcom/squareup/print/sections/TenderSection;Lcom/squareup/print/sections/RefundsSection;Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;Lcom/squareup/print/papersig/TipSections;Lcom/squareup/print/papersig/SignatureSection;Lcom/squareup/print/sections/MultipleTaxBreakdownSection;Lcom/squareup/print/sections/FooterSection;Lcom/squareup/print/sections/BarcodeSection;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;)Lcom/squareup/print/payload/ReceiptPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/print/sections/HeaderSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->header:Lcom/squareup/print/sections/HeaderSection;

    return-object v0
.end method

.method public final component10()Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->returnSubtotals:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    return-object v0
.end method

.method public final component11()Lcom/squareup/print/papersig/TipSections;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->tip:Lcom/squareup/print/papersig/TipSections;

    return-object v0
.end method

.method public final component12()Lcom/squareup/print/papersig/SignatureSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->signature:Lcom/squareup/print/papersig/SignatureSection;

    return-object v0
.end method

.method public final component13()Lcom/squareup/print/sections/MultipleTaxBreakdownSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->multipleTaxBreakdown:Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    return-object v0
.end method

.method public final component14()Lcom/squareup/print/sections/FooterSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->footer:Lcom/squareup/print/sections/FooterSection;

    return-object v0
.end method

.method public final component15()Lcom/squareup/print/sections/BarcodeSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->barcode:Lcom/squareup/print/sections/BarcodeSection;

    return-object v0
.end method

.method public final component16()Lcom/squareup/print/payload/ReceiptPayload$RenderType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    return-object v0
.end method

.method public final component17()Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    return-object v0
.end method

.method public final component2()Lcom/squareup/print/sections/CodesSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->codes:Lcom/squareup/print/sections/CodesSection;

    return-object v0
.end method

.method public final component3()Lcom/squareup/print/sections/EmvSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->emv:Lcom/squareup/print/sections/EmvSection;

    return-object v0
.end method

.method public final component4()Lcom/squareup/print/sections/ItemsAndDiscountsSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    return-object v0
.end method

.method public final component5()Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->subtotalSection:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    return-object v0
.end method

.method public final component6()Lcom/squareup/print/sections/TotalSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->totalSection:Lcom/squareup/print/sections/TotalSection;

    return-object v0
.end method

.method public final component7()Lcom/squareup/print/sections/TenderSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->tender:Lcom/squareup/print/sections/TenderSection;

    return-object v0
.end method

.method public final component8()Lcom/squareup/print/sections/RefundsSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->refunds:Lcom/squareup/print/sections/RefundsSection;

    return-object v0
.end method

.method public final component9()Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->returns:Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    return-object v0
.end method

.method public final copy(Lcom/squareup/print/sections/HeaderSection;Lcom/squareup/print/sections/CodesSection;Lcom/squareup/print/sections/EmvSection;Lcom/squareup/print/sections/ItemsAndDiscountsSection;Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;Lcom/squareup/print/sections/TotalSection;Lcom/squareup/print/sections/TenderSection;Lcom/squareup/print/sections/RefundsSection;Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;Lcom/squareup/print/papersig/TipSections;Lcom/squareup/print/papersig/SignatureSection;Lcom/squareup/print/sections/MultipleTaxBreakdownSection;Lcom/squareup/print/sections/FooterSection;Lcom/squareup/print/sections/BarcodeSection;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;)Lcom/squareup/print/payload/ReceiptPayload;
    .locals 20

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v16, p16

    move-object/from16 v17, p17

    const-string v0, "header"

    move-object/from16 v18, v1

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "codes"

    move-object/from16 v1, p2

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emv"

    move-object/from16 v1, p3

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "totalSection"

    move-object/from16 v1, p6

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tender"

    move-object/from16 v1, p7

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "footer"

    move-object/from16 v1, p14

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renderType"

    move-object/from16 v1, p16

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v19, Lcom/squareup/print/payload/ReceiptPayload;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/print/payload/ReceiptPayload;-><init>(Lcom/squareup/print/sections/HeaderSection;Lcom/squareup/print/sections/CodesSection;Lcom/squareup/print/sections/EmvSection;Lcom/squareup/print/sections/ItemsAndDiscountsSection;Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;Lcom/squareup/print/sections/TotalSection;Lcom/squareup/print/sections/TenderSection;Lcom/squareup/print/sections/RefundsSection;Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;Lcom/squareup/print/papersig/TipSections;Lcom/squareup/print/papersig/SignatureSection;Lcom/squareup/print/sections/MultipleTaxBreakdownSection;Lcom/squareup/print/sections/FooterSection;Lcom/squareup/print/sections/BarcodeSection;Lcom/squareup/print/payload/ReceiptPayload$RenderType;Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;)V

    return-object v19
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/print/payload/ReceiptPayload;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/print/payload/ReceiptPayload;

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->header:Lcom/squareup/print/sections/HeaderSection;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->header:Lcom/squareup/print/sections/HeaderSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->codes:Lcom/squareup/print/sections/CodesSection;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->codes:Lcom/squareup/print/sections/CodesSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->emv:Lcom/squareup/print/sections/EmvSection;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->emv:Lcom/squareup/print/sections/EmvSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->subtotalSection:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->subtotalSection:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->totalSection:Lcom/squareup/print/sections/TotalSection;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->totalSection:Lcom/squareup/print/sections/TotalSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->tender:Lcom/squareup/print/sections/TenderSection;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->tender:Lcom/squareup/print/sections/TenderSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->refunds:Lcom/squareup/print/sections/RefundsSection;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->refunds:Lcom/squareup/print/sections/RefundsSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->returns:Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->returns:Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->returnSubtotals:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->returnSubtotals:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->tip:Lcom/squareup/print/papersig/TipSections;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->tip:Lcom/squareup/print/papersig/TipSections;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->signature:Lcom/squareup/print/papersig/SignatureSection;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->signature:Lcom/squareup/print/papersig/SignatureSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->multipleTaxBreakdown:Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->multipleTaxBreakdown:Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->footer:Lcom/squareup/print/sections/FooterSection;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->footer:Lcom/squareup/print/sections/FooterSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->barcode:Lcom/squareup/print/sections/BarcodeSection;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->barcode:Lcom/squareup/print/sections/BarcodeSection;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    iget-object v1, p1, Lcom/squareup/print/payload/ReceiptPayload;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    iget-object p1, p1, Lcom/squareup/print/payload/ReceiptPayload;->receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterActionName;
    .locals 3

    .line 76
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    if-eqz v0, :cond_2

    sget-object v1, Lcom/squareup/print/payload/ReceiptPayload$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 79
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_COPY_PAPER:Lcom/squareup/analytics/RegisterActionName;

    goto :goto_0

    .line 78
    :cond_0
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_AUTH_SLIP_PAPER:Lcom/squareup/analytics/RegisterActionName;

    goto :goto_0

    .line 77
    :cond_1
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PRINT_RECEIPT_PAPER:Lcom/squareup/analytics/RegisterActionName;

    :goto_0
    return-object v0

    .line 80
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected receiptType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public getAnalyticsPrintJobType()Ljava/lang/String;
    .locals 3

    .line 73
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;->name()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "Locale.US"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_0
    new-instance v0, Lkotlin/TypeCastException;

    const-string v1, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {v0, v1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string v0, "UNSET"

    :goto_0
    return-object v0
.end method

.method public final getBarcode()Lcom/squareup/print/sections/BarcodeSection;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->barcode:Lcom/squareup/print/sections/BarcodeSection;

    return-object v0
.end method

.method public final getCodes()Lcom/squareup/print/sections/CodesSection;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->codes:Lcom/squareup/print/sections/CodesSection;

    return-object v0
.end method

.method public final getEmv()Lcom/squareup/print/sections/EmvSection;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->emv:Lcom/squareup/print/sections/EmvSection;

    return-object v0
.end method

.method public final getFooter()Lcom/squareup/print/sections/FooterSection;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->footer:Lcom/squareup/print/sections/FooterSection;

    return-object v0
.end method

.method public final getHeader()Lcom/squareup/print/sections/HeaderSection;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->header:Lcom/squareup/print/sections/HeaderSection;

    return-object v0
.end method

.method public final getItemsAndDiscounts()Lcom/squareup/print/sections/ItemsAndDiscountsSection;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    return-object v0
.end method

.method public final getMultipleTaxBreakdown()Lcom/squareup/print/sections/MultipleTaxBreakdownSection;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->multipleTaxBreakdown:Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    return-object v0
.end method

.method public final getReceiptType()Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    return-object v0
.end method

.method public final getRefunds()Lcom/squareup/print/sections/RefundsSection;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->refunds:Lcom/squareup/print/sections/RefundsSection;

    return-object v0
.end method

.method public final getRenderType()Lcom/squareup/print/payload/ReceiptPayload$RenderType;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    return-object v0
.end method

.method public final getReturnSubtotals()Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->returnSubtotals:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    return-object v0
.end method

.method public final getReturns()Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->returns:Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    return-object v0
.end method

.method public final getSignature()Lcom/squareup/print/papersig/SignatureSection;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->signature:Lcom/squareup/print/papersig/SignatureSection;

    return-object v0
.end method

.method public final getSubtotalSection()Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;
    .locals 1

    .line 37
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->subtotalSection:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    return-object v0
.end method

.method public final getTender()Lcom/squareup/print/sections/TenderSection;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->tender:Lcom/squareup/print/sections/TenderSection;

    return-object v0
.end method

.method public final getTip()Lcom/squareup/print/papersig/TipSections;
    .locals 1

    .line 43
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->tip:Lcom/squareup/print/papersig/TipSections;

    return-object v0
.end method

.method public final getTotalSection()Lcom/squareup/print/sections/TotalSection;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->totalSection:Lcom/squareup/print/sections/TotalSection;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/print/payload/ReceiptPayload;->header:Lcom/squareup/print/sections/HeaderSection;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->codes:Lcom/squareup/print/sections/CodesSection;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->emv:Lcom/squareup/print/sections/EmvSection;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->subtotalSection:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->totalSection:Lcom/squareup/print/sections/TotalSection;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->tender:Lcom/squareup/print/sections/TenderSection;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->refunds:Lcom/squareup/print/sections/RefundsSection;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->returns:Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_8

    :cond_8
    const/4 v2, 0x0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->returnSubtotals:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    if-eqz v2, :cond_9

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_9

    :cond_9
    const/4 v2, 0x0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->tip:Lcom/squareup/print/papersig/TipSections;

    if-eqz v2, :cond_a

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_a

    :cond_a
    const/4 v2, 0x0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->signature:Lcom/squareup/print/papersig/SignatureSection;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_b

    :cond_b
    const/4 v2, 0x0

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->multipleTaxBreakdown:Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_c

    :cond_c
    const/4 v2, 0x0

    :goto_c
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->footer:Lcom/squareup/print/sections/FooterSection;

    if-eqz v2, :cond_d

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_d

    :cond_d
    const/4 v2, 0x0

    :goto_d
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->barcode:Lcom/squareup/print/sections/BarcodeSection;

    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_e

    :cond_e
    const/4 v2, 0x0

    :goto_e
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    if-eqz v2, :cond_f

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_f

    :cond_f
    const/4 v2, 0x0

    :goto_f
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/print/payload/ReceiptPayload;->receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    if-eqz v2, :cond_10

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_10
    add-int/2addr v0, v1

    return v0
.end method

.method public renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;Z)Landroid/graphics/Bitmap;
    .locals 0

    const-string p2, "thermalBitmapBuilder"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    iget-object p2, p0, Lcom/squareup/print/payload/ReceiptPayload;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-virtual {p2}, Lcom/squareup/print/payload/ReceiptPayload$RenderType;->getRenderer()Lcom/squareup/print/ReceiptPayloadRenderer;

    move-result-object p2

    invoke-interface {p2, p0, p1}, Lcom/squareup/print/ReceiptPayloadRenderer;->renderBitmap(Lcom/squareup/print/payload/ReceiptPayload;Lcom/squareup/print/ThermalBitmapBuilder;)Landroid/graphics/Bitmap;

    move-result-object p1

    const-string p2, "renderType.renderer.rend\u2026is, thermalBitmapBuilder)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ReceiptPayload(header="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->header:Lcom/squareup/print/sections/HeaderSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", codes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->codes:Lcom/squareup/print/sections/CodesSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", emv="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->emv:Lcom/squareup/print/sections/EmvSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", itemsAndDiscounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->itemsAndDiscounts:Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", subtotalSection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->subtotalSection:Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", totalSection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->totalSection:Lcom/squareup/print/sections/TotalSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tender="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->tender:Lcom/squareup/print/sections/TenderSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", refunds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->refunds:Lcom/squareup/print/sections/RefundsSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", returns="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->returns:Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", returnSubtotals="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->returnSubtotals:Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->tip:Lcom/squareup/print/papersig/TipSections;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->signature:Lcom/squareup/print/papersig/SignatureSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", multipleTaxBreakdown="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->multipleTaxBreakdown:Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", footer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->footer:Lcom/squareup/print/sections/FooterSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", barcode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->barcode:Lcom/squareup/print/sections/BarcodeSection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", renderType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->renderType:Lcom/squareup/print/payload/ReceiptPayload$RenderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", receiptType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/payload/ReceiptPayload;->receiptType:Lcom/squareup/print/payload/ReceiptPayload$ReceiptType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
