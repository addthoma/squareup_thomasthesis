.class final Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1$1;
.super Ljava/lang/Object;
.source "PrintoutDispatcher.kt"

# interfaces
.implements Lio/reactivex/functions/Cancellable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1;->subscribe(Lio/reactivex/SingleEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "cancel"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $printJobStatusListener:Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1$printJobStatusListener$1;

.field final synthetic this$0:Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1;


# direct methods
.method constructor <init>(Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1;Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1$printJobStatusListener$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1$1;->this$0:Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1;

    iput-object p2, p0, Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1$1;->$printJobStatusListener:Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1$printJobStatusListener$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final cancel()V
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1$1;->this$0:Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1;

    iget-object v0, v0, Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1;->this$0:Lcom/squareup/print/RealPrintoutDispatcher;

    invoke-static {v0}, Lcom/squareup/print/RealPrintoutDispatcher;->access$getPrintSpooler$p(Lcom/squareup/print/RealPrintoutDispatcher;)Lcom/squareup/print/PrintSpooler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1$1;->$printJobStatusListener:Lcom/squareup/print/RealPrintoutDispatcher$listenForAttemptedPrintJob$1$printJobStatusListener$1;

    check-cast v1, Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;

    invoke-virtual {v0, v1}, Lcom/squareup/print/PrintSpooler;->removePrintJobStatusListener(Lcom/squareup/print/PrintSpooler$PrintJobStatusListener;)V

    return-void
.end method
