.class public final Lcom/squareup/print/FileThreadPrintQueueExecutor_Factory;
.super Ljava/lang/Object;
.source "FileThreadPrintQueueExecutor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/FileThreadPrintQueueExecutor;",
        ">;"
    }
.end annotation


# instance fields
.field private final fileThreadExecutorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final printJobQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintJobQueue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintJobQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor_Factory;->printJobQueueProvider:Ljavax/inject/Provider;

    .line 27
    iput-object p2, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p3, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor_Factory;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/print/FileThreadPrintQueueExecutor_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintJobQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/concurrent/Executor;",
            ">;)",
            "Lcom/squareup/print/FileThreadPrintQueueExecutor_Factory;"
        }
    .end annotation

    .line 39
    new-instance v0, Lcom/squareup/print/FileThreadPrintQueueExecutor_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/FileThreadPrintQueueExecutor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/print/PrintJobQueue;Lcom/squareup/thread/executor/MainThread;Ljava/util/concurrent/Executor;)Lcom/squareup/print/FileThreadPrintQueueExecutor;
    .locals 1

    .line 44
    new-instance v0, Lcom/squareup/print/FileThreadPrintQueueExecutor;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/print/FileThreadPrintQueueExecutor;-><init>(Lcom/squareup/print/PrintJobQueue;Lcom/squareup/thread/executor/MainThread;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/print/FileThreadPrintQueueExecutor;
    .locals 3

    .line 33
    iget-object v0, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor_Factory;->printJobQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/PrintJobQueue;

    iget-object v1, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor_Factory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/thread/executor/MainThread;

    iget-object v2, p0, Lcom/squareup/print/FileThreadPrintQueueExecutor_Factory;->fileThreadExecutorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, Lcom/squareup/print/FileThreadPrintQueueExecutor_Factory;->newInstance(Lcom/squareup/print/PrintJobQueue;Lcom/squareup/thread/executor/MainThread;Ljava/util/concurrent/Executor;)Lcom/squareup/print/FileThreadPrintQueueExecutor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/print/FileThreadPrintQueueExecutor_Factory;->get()Lcom/squareup/print/FileThreadPrintQueueExecutor;

    move-result-object v0

    return-object v0
.end method
