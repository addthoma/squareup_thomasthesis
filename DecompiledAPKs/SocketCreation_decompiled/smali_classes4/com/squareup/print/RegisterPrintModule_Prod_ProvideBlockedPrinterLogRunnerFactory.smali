.class public final Lcom/squareup/print/RegisterPrintModule_Prod_ProvideBlockedPrinterLogRunnerFactory;
.super Ljava/lang/Object;
.source "RegisterPrintModule_Prod_ProvideBlockedPrinterLogRunnerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/BlockedPrinterLogRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/print/RegisterPrintModule_Prod_ProvideBlockedPrinterLogRunnerFactory;->mainThreadProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/print/RegisterPrintModule_Prod_ProvideBlockedPrinterLogRunnerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;)",
            "Lcom/squareup/print/RegisterPrintModule_Prod_ProvideBlockedPrinterLogRunnerFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/print/RegisterPrintModule_Prod_ProvideBlockedPrinterLogRunnerFactory;

    invoke-direct {v0, p0}, Lcom/squareup/print/RegisterPrintModule_Prod_ProvideBlockedPrinterLogRunnerFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideBlockedPrinterLogRunner(Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/print/BlockedPrinterLogRunner;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/print/RegisterPrintModule$Prod;->provideBlockedPrinterLogRunner(Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/print/BlockedPrinterLogRunner;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/BlockedPrinterLogRunner;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/print/BlockedPrinterLogRunner;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/print/RegisterPrintModule_Prod_ProvideBlockedPrinterLogRunnerFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/thread/executor/MainThread;

    invoke-static {v0}, Lcom/squareup/print/RegisterPrintModule_Prod_ProvideBlockedPrinterLogRunnerFactory;->provideBlockedPrinterLogRunner(Lcom/squareup/thread/executor/MainThread;)Lcom/squareup/print/BlockedPrinterLogRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/print/RegisterPrintModule_Prod_ProvideBlockedPrinterLogRunnerFactory;->get()Lcom/squareup/print/BlockedPrinterLogRunner;

    move-result-object v0

    return-object v0
.end method
