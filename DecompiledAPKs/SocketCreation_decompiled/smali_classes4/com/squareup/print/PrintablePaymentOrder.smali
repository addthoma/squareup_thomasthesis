.class public final Lcom/squareup/print/PrintablePaymentOrder;
.super Ljava/lang/Object;
.source "PrintablePaymentOrder.kt"

# interfaces
.implements Lcom/squareup/print/PrintableOrder;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;,
        Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItemModifier;,
        Lcom/squareup/print/PrintablePaymentOrder$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPrintablePaymentOrder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PrintablePaymentOrder.kt\ncom/squareup/print/PrintablePaymentOrder\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,123:1\n1360#2:124\n1429#2,3:125\n1360#2:128\n1429#2,3:129\n*E\n*S KotlinDebug\n*F\n+ 1 PrintablePaymentOrder.kt\ncom/squareup/print/PrintablePaymentOrder\n*L\n22#1:124\n22#1,3:125\n25#1:128\n25#1,3:129\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 *2\u00020\u0001:\u0003*+,B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010%\u001a\u0004\u0018\u00010&2\u0006\u0010\'\u001a\u00020(H\u0016J\u0008\u0010)\u001a\u00020\u001fH\u0016R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\t\u001a\u00020\n8VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u000cR\u001a\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u000f0\u000eX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0011R\u0016\u0010\u0014\u001a\u0004\u0018\u00010\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0008R\u0016\u0010\u0016\u001a\u0004\u0018\u00010\u0006X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0008R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u000eX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0011R\u0016\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u001dR(\u0010 \u001a\u0004\u0018\u00010\u001f2\u0008\u0010\u001e\u001a\u0004\u0018\u00010\u001f8V@VX\u0096\u000e\u00a2\u0006\u000c\u001a\u0004\u0008!\u0010\"\"\u0004\u0008#\u0010$\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/print/PrintablePaymentOrder;",
        "Lcom/squareup/print/PrintableOrder;",
        "orderSnapshot",
        "Lcom/squareup/payment/OrderSnapshot;",
        "(Lcom/squareup/payment/OrderSnapshot;)V",
        "employeeToken",
        "",
        "getEmployeeToken",
        "()Ljava/lang/String;",
        "enabledSeatCount",
        "",
        "getEnabledSeatCount",
        "()I",
        "notVoidedLockedItems",
        "",
        "Lcom/squareup/print/PrintableOrderItem;",
        "getNotVoidedLockedItems",
        "()Ljava/util/List;",
        "notVoidedUnlockedItems",
        "getNotVoidedUnlockedItems",
        "openTicketNote",
        "getOpenTicketNote",
        "orderIdentifier",
        "getOrderIdentifier",
        "receiptNumbers",
        "getReceiptNumbers",
        "recipient",
        "Lcom/squareup/print/PrintableRecipient;",
        "getRecipient",
        "()Lcom/squareup/print/PrintableRecipient;",
        "value",
        "Ljava/util/Date;",
        "timestampForReprint",
        "getTimestampForReprint",
        "()Ljava/util/Date;",
        "setTimestampForReprint",
        "(Ljava/util/Date;)V",
        "getDiningOption",
        "Lcom/squareup/checkout/DiningOption;",
        "res",
        "Lcom/squareup/util/Res;",
        "getTimestamp",
        "Companion",
        "PrintablePaymentOrderItem",
        "PrintablePaymentOrderItemModifier",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/print/PrintablePaymentOrder$Companion;


# instance fields
.field private final employeeToken:Ljava/lang/String;

.field private final notVoidedLockedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation
.end field

.field private final notVoidedUnlockedItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketNote:Ljava/lang/String;

.field private final orderIdentifier:Ljava/lang/String;

.field private final orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

.field private final receiptNumbers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final recipient:Lcom/squareup/print/PrintableRecipient;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/print/PrintablePaymentOrder$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/print/PrintablePaymentOrder$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/print/PrintablePaymentOrder;->Companion:Lcom/squareup/print/PrintablePaymentOrder$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/payment/OrderSnapshot;)V
    .locals 8

    const-string v0, "orderSnapshot"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    .line 22
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {p1}, Lcom/squareup/payment/OrderSnapshot;->getNotVoidedUnlockedItems()Ljava/util/List;

    move-result-object p1

    const-string v0, "orderSnapshot.notVoidedUnlockedItems"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 124
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 125
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x0

    const-string v6, "it"

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 126
    check-cast v2, Lcom/squareup/checkout/CartItem;

    .line 22
    new-instance v7, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;

    invoke-static {v2, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v7, v2, v5, v4, v3}, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;-><init>(Lcom/squareup/checkout/CartItem;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v0, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 127
    :cond_0
    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder;->notVoidedUnlockedItems:Ljava/util/List;

    .line 25
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {p1}, Lcom/squareup/payment/OrderSnapshot;->getNotVoidedLockedItems()Ljava/util/List;

    move-result-object p1

    const-string v0, "orderSnapshot.notVoidedLockedItems"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Iterable;

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 129
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 130
    check-cast v1, Lcom/squareup/checkout/CartItem;

    .line 25
    new-instance v2, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;

    invoke-static {v1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v1, v5, v4, v3}, Lcom/squareup/print/PrintablePaymentOrder$PrintablePaymentOrderItem;-><init>(Lcom/squareup/checkout/CartItem;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 131
    :cond_1
    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder;->notVoidedLockedItems:Ljava/util/List;

    .line 27
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {p1}, Lcom/squareup/payment/OrderSnapshot;->getEmployeeToken()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder;->employeeToken:Ljava/lang/String;

    .line 32
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {p1}, Lcom/squareup/payment/OrderSnapshot;->getOpenTicketNote()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder;->openTicketNote:Ljava/lang/String;

    .line 34
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {p1}, Lcom/squareup/payment/OrderSnapshot;->orderIdentifierOrNull()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder;->orderIdentifier:Ljava/lang/String;

    .line 42
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder;->receiptNumbers:Ljava/util/List;

    return-void
.end method

.method public static final convertCartItemsToItems(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/print/PrintablePaymentOrder;->Companion:Lcom/squareup/print/PrintablePaymentOrder$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/print/PrintablePaymentOrder$Companion;->convertCartItemsToItems(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final convertItemsToCartItems(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/checkout/CartItem;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/print/PrintablePaymentOrder;->Companion:Lcom/squareup/print/PrintablePaymentOrder$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/print/PrintablePaymentOrder$Companion;->convertItemsToCartItems(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final convertToReturnItems(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation

    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/print/PrintablePaymentOrder;->Companion:Lcom/squareup/print/PrintablePaymentOrder$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/print/PrintablePaymentOrder$Companion;->convertToReturnItems(Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getDiningOption(Lcom/squareup/util/Res;)Lcom/squareup/checkout/DiningOption;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    iget-object p1, p0, Lcom/squareup/print/PrintablePaymentOrder;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {p1}, Lcom/squareup/payment/OrderSnapshot;->getDiningOption()Lcom/squareup/checkout/DiningOption;

    move-result-object p1

    return-object p1
.end method

.method public getEmployeeToken()Ljava/lang/String;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder;->employeeToken:Ljava/lang/String;

    return-object v0
.end method

.method public getEnabledSeatCount()I
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSnapshot;->getSeatingHandler()Lcom/squareup/payment/OrderSeatingHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSeatingHandler;->getEnabledSeats()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getNotVoidedLockedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder;->notVoidedLockedItems:Ljava/util/List;

    return-object v0
.end method

.method public getNotVoidedUnlockedItems()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/print/PrintableOrderItem;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder;->notVoidedUnlockedItems:Ljava/util/List;

    return-object v0
.end method

.method public getOpenTicketNote()Ljava/lang/String;
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder;->openTicketNote:Ljava/lang/String;

    return-object v0
.end method

.method public getOrderIdentifier()Ljava/lang/String;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder;->orderIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method public getReceiptNumbers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 42
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder;->receiptNumbers:Ljava/util/List;

    return-object v0
.end method

.method public getRecipient()Lcom/squareup/print/PrintableRecipient;
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder;->recipient:Lcom/squareup/print/PrintableRecipient;

    return-object v0
.end method

.method public getTimestamp()Ljava/util/Date;
    .locals 2

    .line 36
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSnapshot;->timestampAsDate()Ljava/util/Date;

    move-result-object v0

    const-string v1, "orderSnapshot.timestampAsDate()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getTimestampForReprint()Ljava/util/Date;
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {v0}, Lcom/squareup/payment/OrderSnapshot;->requireTimestampForReprint()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public setTimestampForReprint(Ljava/util/Date;)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/print/PrintablePaymentOrder;->orderSnapshot:Lcom/squareup/payment/OrderSnapshot;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/OrderSnapshot;->setTimestampForReprint(Ljava/util/Date;)V

    return-void
.end method
