.class public interface abstract Lcom/squareup/print/PrintSettings;
.super Ljava/lang/Object;
.source "PrintSettings.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\tH&J\u0012\u0010\n\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005H&J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\tH&J\u0010\u0010\u000c\u001a\u00020\u00032\u0006\u0010\u0008\u001a\u00020\tH&J\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J&\u0010\u000e\u001a\u00020\u00032\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0003H&J.\u0010\u0014\u001a\u00020\u00032\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0015\u001a\u00020\u0016H&J\u001e\u0010\u0017\u001a\u00020\u00032\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00102\u0006\u0010\u0011\u001a\u00020\u0012H&J&\u0010\u0018\u001a\u00020\u00032\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0016H&J$\u0010\u0019\u001a\u00020\u00032\u000c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00102\u000c\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001bH&\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/print/PrintSettings;",
        "",
        "canPrintATicketForEachItem",
        "",
        "printerStation",
        "Lcom/squareup/print/PrinterStation;",
        "canPrintCompactTickets",
        "isFormalReceiptPrintingAvailable",
        "receipt",
        "Lcom/squareup/payment/PaymentReceipt;",
        "shouldPrintATicketForEachItem",
        "shouldPrintAuthSlip",
        "shouldPrintAuthSlipCopy",
        "shouldPrintCompactTickets",
        "shouldPrintTicketForOrder",
        "targetStation",
        "",
        "order",
        "Lcom/squareup/payment/Order;",
        "isPrintingSavedTicket",
        "shouldPrintTicketForPayment",
        "payment",
        "Lcom/squareup/payment/Payment;",
        "shouldPrintTicketStubForOrder",
        "shouldPrintTicketStubForPayment",
        "shouldPrintVoidTicketForTransaction",
        "voidedItems",
        "",
        "Lcom/squareup/checkout/CartItem;",
        "print_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract canPrintATicketForEachItem(Lcom/squareup/print/PrinterStation;)Z
.end method

.method public abstract canPrintCompactTickets(Lcom/squareup/print/PrinterStation;)Z
.end method

.method public abstract isFormalReceiptPrintingAvailable(Lcom/squareup/payment/PaymentReceipt;)Z
.end method

.method public abstract shouldPrintATicketForEachItem(Lcom/squareup/print/PrinterStation;)Z
.end method

.method public abstract shouldPrintAuthSlip(Lcom/squareup/payment/PaymentReceipt;)Z
.end method

.method public abstract shouldPrintAuthSlipCopy(Lcom/squareup/payment/PaymentReceipt;)Z
.end method

.method public abstract shouldPrintCompactTickets(Lcom/squareup/print/PrinterStation;)Z
.end method

.method public abstract shouldPrintTicketForOrder(Ljava/util/Collection;Lcom/squareup/payment/Order;Z)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Lcom/squareup/payment/Order;",
            "Z)Z"
        }
    .end annotation
.end method

.method public abstract shouldPrintTicketForPayment(Ljava/util/Collection;Lcom/squareup/payment/Order;ZLcom/squareup/payment/Payment;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Lcom/squareup/payment/Order;",
            "Z",
            "Lcom/squareup/payment/Payment;",
            ")Z"
        }
    .end annotation
.end method

.method public abstract shouldPrintTicketStubForOrder(Ljava/util/Collection;Lcom/squareup/payment/Order;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Lcom/squareup/payment/Order;",
            ")Z"
        }
    .end annotation
.end method

.method public abstract shouldPrintTicketStubForPayment(Ljava/util/Collection;Lcom/squareup/payment/Order;Lcom/squareup/payment/Payment;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Lcom/squareup/payment/Order;",
            "Lcom/squareup/payment/Payment;",
            ")Z"
        }
    .end annotation
.end method

.method public abstract shouldPrintVoidTicketForTransaction(Ljava/util/Collection;Ljava/util/List;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/checkout/CartItem;",
            ">;)Z"
        }
    .end annotation
.end method
