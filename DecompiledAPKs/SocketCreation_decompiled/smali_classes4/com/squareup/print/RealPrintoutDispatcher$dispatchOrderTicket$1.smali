.class final Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;
.super Ljava/lang/Object;
.source "PrintoutDispatcher.kt"

# interfaces
.implements Lio/reactivex/SingleOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/print/RealPrintoutDispatcher;->dispatchOrderTicket(Lcom/squareup/print/TicketStubPrintout;Ljava/util/Collection;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/SingleOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u001e\u0010\u0002\u001a\u001a\u0012\u0016\u0012\u0014 \u0006*\n\u0018\u00010\u0004j\u0004\u0018\u0001`\u00050\u0004j\u0002`\u00050\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/SingleEmitter;",
        "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
        "Lcom/squareup/print/PrintJobResult;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $printout:Lcom/squareup/print/TicketStubPrintout;

.field final synthetic $target:Ljava/util/Collection;

.field final synthetic this$0:Lcom/squareup/print/RealPrintoutDispatcher;


# direct methods
.method constructor <init>(Lcom/squareup/print/RealPrintoutDispatcher;Lcom/squareup/print/TicketStubPrintout;Ljava/util/Collection;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;->this$0:Lcom/squareup/print/RealPrintoutDispatcher;

    iput-object p2, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;->$printout:Lcom/squareup/print/TicketStubPrintout;

    iput-object p3, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;->$target:Ljava/util/Collection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/SingleEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/SingleEmitter<",
            "Lcom/squareup/print/PrintJob$PrintAttempt$Result;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;->this$0:Lcom/squareup/print/RealPrintoutDispatcher;

    invoke-static {v0}, Lcom/squareup/print/RealPrintoutDispatcher;->access$waitForEnqueuedJobResult(Lcom/squareup/print/RealPrintoutDispatcher;)Lio/reactivex/Single;

    move-result-object v0

    .line 80
    new-instance v1, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1$sub$1;

    invoke-direct {v1, p1}, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1$sub$1;-><init>(Lio/reactivex/SingleEmitter;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 81
    new-instance v1, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1$1;

    invoke-direct {v1, v0}, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1$1;-><init>(Lio/reactivex/disposables/Disposable;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    new-instance v0, Lcom/squareup/print/PrintoutDispatcherKt$sam$io_reactivex_functions_Cancellable$0;

    invoke-direct {v0, v1}, Lcom/squareup/print/PrintoutDispatcherKt$sam$io_reactivex_functions_Cancellable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lio/reactivex/functions/Cancellable;

    invoke-interface {p1, v0}, Lio/reactivex/SingleEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    .line 83
    iget-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;->$printout:Lcom/squareup/print/TicketStubPrintout;

    invoke-virtual {p1}, Lcom/squareup/print/TicketStubPrintout;->getType()Lcom/squareup/print/PrintoutType;

    move-result-object p1

    sget-object v0, Lcom/squareup/print/RealPrintoutDispatcher$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/print/PrintoutType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 87
    iget-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;->this$0:Lcom/squareup/print/RealPrintoutDispatcher;

    invoke-static {p1}, Lcom/squareup/print/RealPrintoutDispatcher;->access$getOrderPrintingDispatcher$p(Lcom/squareup/print/RealPrintoutDispatcher;)Lcom/squareup/print/OrderPrintingDispatcher;

    move-result-object p1

    .line 88
    iget-object v0, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;->$printout:Lcom/squareup/print/TicketStubPrintout;

    invoke-virtual {v0}, Lcom/squareup/print/TicketStubPrintout;->getOrderSnapshot()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Order;

    iget-object v1, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;->$printout:Lcom/squareup/print/TicketStubPrintout;

    invoke-virtual {v1}, Lcom/squareup/print/TicketStubPrintout;->getOrderDisplayName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;->$target:Ljava/util/Collection;

    .line 87
    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/print/OrderPrintingDispatcher;->printTicket(Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/util/Collection;)V

    goto :goto_0

    .line 90
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected printoutType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;->$printout:Lcom/squareup/print/TicketStubPrintout;

    invoke-virtual {v1}, Lcom/squareup/print/TicketStubPrintout;->getType()Lcom/squareup/print/PrintoutType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 84
    :cond_1
    iget-object p1, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;->this$0:Lcom/squareup/print/RealPrintoutDispatcher;

    invoke-static {p1}, Lcom/squareup/print/RealPrintoutDispatcher;->access$getOrderPrintingDispatcher$p(Lcom/squareup/print/RealPrintoutDispatcher;)Lcom/squareup/print/OrderPrintingDispatcher;

    move-result-object p1

    .line 85
    iget-object v0, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;->$printout:Lcom/squareup/print/TicketStubPrintout;

    invoke-virtual {v0}, Lcom/squareup/print/TicketStubPrintout;->getOrderSnapshot()Lcom/squareup/payment/OrderSnapshot;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/Order;

    iget-object v1, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;->$printout:Lcom/squareup/print/TicketStubPrintout;

    invoke-virtual {v1}, Lcom/squareup/print/TicketStubPrintout;->getOrderDisplayName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/print/RealPrintoutDispatcher$dispatchOrderTicket$1;->$target:Ljava/util/Collection;

    .line 84
    invoke-virtual {p1, v0, v1, v2}, Lcom/squareup/print/OrderPrintingDispatcher;->printTicketStub(Lcom/squareup/payment/Order;Ljava/lang/String;Ljava/util/Collection;)V

    :goto_0
    return-void
.end method
