.class public Lcom/squareup/print/PrinterScoutScheduler;
.super Ljava/lang/Object;
.source "PrinterScoutScheduler.java"

# interfaces
.implements Lcom/squareup/print/PrinterScout$ResultListener;


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

.field private final latestResults:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;>;"
        }
    .end annotation
.end field

.field running:Z

.field private final scouts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/print/PrinterScout;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterScoutsProvider;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/print/PrinterScoutScheduler;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    .line 34
    iput-object p3, p0, Lcom/squareup/print/PrinterScoutScheduler;->features:Lcom/squareup/settings/server/Features;

    .line 35
    new-instance p1, Ljava/util/ArrayList;

    invoke-interface {p2}, Lcom/squareup/print/PrinterScoutsProvider;->availableScouts()Ljava/util/Set;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object p1, p0, Lcom/squareup/print/PrinterScoutScheduler;->scouts:Ljava/util/List;

    .line 36
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/print/PrinterScoutScheduler;->latestResults:Ljava/util/LinkedHashMap;

    .line 37
    iget-object p1, p0, Lcom/squareup/print/PrinterScoutScheduler;->scouts:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/print/PrinterScout;

    .line 38
    invoke-virtual {p2, p0}, Lcom/squareup/print/PrinterScout;->setResultListener(Lcom/squareup/print/PrinterScout$ResultListener;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private scheduleScouts()V
    .locals 2

    .line 103
    invoke-direct {p0}, Lcom/squareup/print/PrinterScoutScheduler;->stopAllScouts()V

    .line 105
    iget-object v0, p0, Lcom/squareup/print/PrinterScoutScheduler;->scouts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterScout;

    .line 106
    invoke-virtual {v1}, Lcom/squareup/print/PrinterScout;->start()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private stopAllScouts()V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/squareup/print/PrinterScoutScheduler;->scouts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterScout;

    .line 112
    invoke-virtual {v1}, Lcom/squareup/print/PrinterScout;->stop()V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public onAppPause()V
    .locals 2

    .line 84
    iget-object v0, p0, Lcom/squareup/print/PrinterScoutScheduler;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PRINTER_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "[PrinterScoutScheduler] Stopping all scouts due to app pause."

    .line 85
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/print/PrinterScoutScheduler;->stop()V

    return-void
.end method

.method public onAppResume()V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/print/PrinterScoutScheduler;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PRINTER_DEBUGGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "[PrinterScoutScheduler] Starting all scouts due to app resume."

    .line 71
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/print/PrinterScoutScheduler;->start()V

    return-void
.end method

.method public onResult(Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/print/HardwarePrinter;",
            ">;)V"
        }
    .end annotation

    .line 43
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 44
    iget-object v0, p0, Lcom/squareup/print/PrinterScoutScheduler;->latestResults:Ljava/util/LinkedHashMap;

    const-string v1, "results"

    invoke-static {p2, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 50
    iget-object p2, p0, Lcom/squareup/print/PrinterScoutScheduler;->latestResults:Ljava/util/LinkedHashMap;

    invoke-virtual {p2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 51
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/HardwarePrinter;

    .line 52
    invoke-virtual {v1}, Lcom/squareup/print/HardwarePrinter;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 55
    :cond_1
    iget-object p2, p0, Lcom/squareup/print/PrinterScoutScheduler;->hardwarePrinterTracker:Lcom/squareup/print/HardwarePrinterTracker;

    invoke-virtual {p2, p1}, Lcom/squareup/print/HardwarePrinterTracker;->setAvailableHardwarePrinters(Ljava/util/Map;)V

    return-void
.end method

.method public shutdown()V
    .locals 2

    .line 95
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const/4 v0, 0x0

    .line 96
    iput-boolean v0, p0, Lcom/squareup/print/PrinterScoutScheduler;->running:Z

    .line 97
    iget-object v0, p0, Lcom/squareup/print/PrinterScoutScheduler;->scouts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/print/PrinterScout;

    .line 98
    invoke-virtual {v1}, Lcom/squareup/print/PrinterScout;->shutdown()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public start()V
    .locals 1

    .line 64
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const/4 v0, 0x1

    .line 65
    iput-boolean v0, p0, Lcom/squareup/print/PrinterScoutScheduler;->running:Z

    .line 66
    invoke-direct {p0}, Lcom/squareup/print/PrinterScoutScheduler;->scheduleScouts()V

    return-void
.end method

.method public stop()V
    .locals 1

    .line 78
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    const/4 v0, 0x0

    .line 79
    iput-boolean v0, p0, Lcom/squareup/print/PrinterScoutScheduler;->running:Z

    .line 80
    invoke-direct {p0}, Lcom/squareup/print/PrinterScoutScheduler;->stopAllScouts()V

    return-void
.end method
