.class public final enum Lcom/squareup/print/util/RasterDocument$RasPageEndMode;
.super Ljava/lang/Enum;
.source "RasterDocument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/util/RasterDocument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RasPageEndMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/print/util/RasterDocument$RasPageEndMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

.field public static final enum DEFAULT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

.field public static final enum EJECT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

.field public static final enum FEED_AND_EJECT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

.field public static final enum FEED_AND_FULL_CUT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

.field public static final enum FEED_AND_PARTIAL_CUT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

.field public static final enum FEED_TO_CUTTER:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

.field public static final enum FEED_TO_TEAR_BAR:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

.field public static final enum FULL_CUT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

.field public static final enum NONE:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

.field public static final enum PARTIAL_CUT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 24
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    const/4 v1, 0x0

    const-string v2, "DEFAULT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->DEFAULT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    .line 25
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    const/4 v2, 0x1

    const-string v3, "NONE"

    invoke-direct {v0, v3, v2}, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->NONE:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    .line 26
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    const/4 v3, 0x2

    const-string v4, "FEED_TO_CUTTER"

    invoke-direct {v0, v4, v3}, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->FEED_TO_CUTTER:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    .line 27
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    const/4 v4, 0x3

    const-string v5, "FEED_TO_TEAR_BAR"

    invoke-direct {v0, v5, v4}, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->FEED_TO_TEAR_BAR:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    .line 28
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    const/4 v5, 0x4

    const-string v6, "FULL_CUT"

    invoke-direct {v0, v6, v5}, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->FULL_CUT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    .line 29
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    const/4 v6, 0x5

    const-string v7, "FEED_AND_FULL_CUT"

    invoke-direct {v0, v7, v6}, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->FEED_AND_FULL_CUT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    .line 30
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    const/4 v7, 0x6

    const-string v8, "PARTIAL_CUT"

    invoke-direct {v0, v8, v7}, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->PARTIAL_CUT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    .line 31
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    const/4 v8, 0x7

    const-string v9, "FEED_AND_PARTIAL_CUT"

    invoke-direct {v0, v9, v8}, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->FEED_AND_PARTIAL_CUT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    .line 32
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    const/16 v9, 0x8

    const-string v10, "EJECT"

    invoke-direct {v0, v10, v9}, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->EJECT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    .line 33
    new-instance v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    const/16 v10, 0x9

    const-string v11, "FEED_AND_EJECT"

    invoke-direct {v0, v11, v10}, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->FEED_AND_EJECT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    .line 23
    sget-object v11, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->DEFAULT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    aput-object v11, v0, v1

    sget-object v1, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->NONE:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->FEED_TO_CUTTER:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->FEED_TO_TEAR_BAR:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->FULL_CUT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->FEED_AND_FULL_CUT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->PARTIAL_CUT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->FEED_AND_PARTIAL_CUT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->EJECT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->FEED_AND_EJECT:Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->$VALUES:[Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/print/util/RasterDocument$RasPageEndMode;
    .locals 1

    .line 23
    const-class v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/print/util/RasterDocument$RasPageEndMode;
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->$VALUES:[Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    invoke-virtual {v0}, [Lcom/squareup/print/util/RasterDocument$RasPageEndMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/print/util/RasterDocument$RasPageEndMode;

    return-object v0
.end method
