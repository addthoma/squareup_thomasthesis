.class public Lcom/squareup/print/RealPrinterStation;
.super Lcom/squareup/print/PrinterStation;
.source "RealPrinterStation.java"


# static fields
.field public static final EMPTY_CONFIGURATION:Lcom/squareup/print/PrinterStationConfiguration;


# instance fields
.field private final configurationSetting:Lcom/squareup/settings/GsonLocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/GsonLocalSetting<",
            "Lcom/squareup/print/PrinterStationConfiguration;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/print/PrinterStationConfiguration$Builder;

    invoke-direct {v0}, Lcom/squareup/print/PrinterStationConfiguration$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/print/PrinterStationConfiguration$Builder;->build()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v0

    sput-object v0, Lcom/squareup/print/RealPrinterStation;->EMPTY_CONFIGURATION:Lcom/squareup/print/PrinterStationConfiguration;

    return-void
.end method

.method constructor <init>(Lcom/squareup/settings/GsonLocalSetting;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/GsonLocalSetting<",
            "Lcom/squareup/print/PrinterStationConfiguration;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p2}, Lcom/squareup/print/PrinterStation;-><init>(Ljava/lang/String;)V

    .line 25
    iput-object p1, p0, Lcom/squareup/print/RealPrinterStation;->configurationSetting:Lcom/squareup/settings/GsonLocalSetting;

    return-void
.end method


# virtual methods
.method public commit(Lcom/squareup/print/PrinterStationConfiguration;)V
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStation;->configurationSetting:Lcom/squareup/settings/GsonLocalSetting;

    invoke-virtual {v0, p1}, Lcom/squareup/settings/GsonLocalSetting;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/print/RealPrinterStation;->configurationSetting:Lcom/squareup/settings/GsonLocalSetting;

    invoke-virtual {v0}, Lcom/squareup/settings/GsonLocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/print/PrinterStationConfiguration;

    return-object v0
.end method

.method public getDisabledCategoryIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 66
    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/PrinterStationConfiguration;->disabledCategoryIds:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/PrinterStationConfiguration;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSelectedHardwarePrinterId()Ljava/lang/String;
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/PrinterStationConfiguration;->selectedHardwarePrinterId:Ljava/lang/String;

    return-object v0
.end method

.method public varargs hasAnyRoleIn([Lcom/squareup/print/PrinterStation$Role;)Z
    .locals 5

    .line 52
    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/PrinterStationConfiguration;->selectedRoles:Ljava/util/Set;

    .line 53
    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, p1, v3

    .line 54
    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public hasHardwarePrinterSelected()Z
    .locals 2

    .line 39
    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStation;->getSelectedHardwarePrinterId()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/squareup/print/PrinterStationConfiguration;->NO_PRINTER_SELECTED_ID:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public isAutoPrintItemizedReceiptsEnabled()Z
    .locals 1

    .line 74
    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/print/PrinterStationConfiguration;->autoPrintItemizedReceipts:Z

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStation;->hasHardwarePrinterSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/PrinterStationConfiguration;->selectedRoles:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEnabledForCategoryId(Ljava/lang/String;)Z
    .locals 1

    .line 62
    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/PrinterStationConfiguration;->disabledCategoryIds:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method public isInternal()Z
    .locals 1

    .line 86
    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/print/PrinterStationConfiguration;->internal:Z

    return v0
.end method

.method public isPrintATicketForEachItemEnabled()Z
    .locals 1

    .line 78
    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/print/PrinterStationConfiguration;->printATicketForEachItem:Z

    return v0
.end method

.method public isPrintCompactTicketsEnabled()Z
    .locals 1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/print/PrinterStationConfiguration;->printCompactTickets:Z

    return v0
.end method

.method public printsUncategorizedItems()Z
    .locals 1

    .line 70
    invoke-virtual {p0}, Lcom/squareup/print/RealPrinterStation;->getConfiguration()Lcom/squareup/print/PrinterStationConfiguration;

    move-result-object v0

    iget-boolean v0, v0, Lcom/squareup/print/PrinterStationConfiguration;->printsUncategorizedItems:Z

    return v0
.end method
