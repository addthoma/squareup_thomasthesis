.class public Lcom/squareup/print/DefaultReceiptPayloadRenderer;
.super Ljava/lang/Object;
.source "DefaultReceiptPayloadRenderer.java"

# interfaces
.implements Lcom/squareup/print/ReceiptPayloadRenderer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public renderBitmap(Lcom/squareup/print/payload/ReceiptPayload;Lcom/squareup/print/ThermalBitmapBuilder;)Landroid/graphics/Bitmap;
    .locals 1

    .line 8
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getHeader()Lcom/squareup/print/sections/HeaderSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/HeaderSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 9
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getCodes()Lcom/squareup/print/sections/CodesSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/CodesSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 10
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getEmv()Lcom/squareup/print/sections/EmvSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/EmvSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 12
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getItemsAndDiscounts()Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 13
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getItemsAndDiscounts()Lcom/squareup/print/sections/ItemsAndDiscountsSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/ItemsAndDiscountsSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 15
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getSubtotalSection()Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/SubtotalAndAdjustmentsSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 16
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getReturns()Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 17
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getReturns()Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/ReturnItemsAndDiscountsSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 19
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getReturnSubtotals()Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 20
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getReturnSubtotals()Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/ReturnSubtotalAndAdjustmentsSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 23
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getTotalSection()Lcom/squareup/print/sections/TotalSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/TotalSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 25
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getTender()Lcom/squareup/print/sections/TenderSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/TenderSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 27
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getRefunds()Lcom/squareup/print/sections/RefundsSection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 28
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getRefunds()Lcom/squareup/print/sections/RefundsSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/RefundsSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 31
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getTip()Lcom/squareup/print/papersig/TipSections;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 32
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getTip()Lcom/squareup/print/papersig/TipSections;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/papersig/TipSections;->traditional:Lcom/squareup/print/papersig/TraditionalTipSection;

    if-eqz v0, :cond_4

    .line 33
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getTip()Lcom/squareup/print/papersig/TipSections;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/papersig/TipSections;->traditional:Lcom/squareup/print/papersig/TraditionalTipSection;

    invoke-virtual {v0, p2}, Lcom/squareup/print/papersig/TraditionalTipSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    goto :goto_0

    .line 34
    :cond_4
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getTip()Lcom/squareup/print/papersig/TipSections;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/papersig/TipSections;->quick:Lcom/squareup/print/papersig/QuickTipSection;

    if-eqz v0, :cond_5

    .line 35
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getTip()Lcom/squareup/print/papersig/TipSections;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/print/papersig/TipSections;->quick:Lcom/squareup/print/papersig/QuickTipSection;

    invoke-virtual {v0, p2}, Lcom/squareup/print/papersig/QuickTipSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 39
    :cond_5
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getSignature()Lcom/squareup/print/papersig/SignatureSection;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 40
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getSignature()Lcom/squareup/print/papersig/SignatureSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/papersig/SignatureSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 43
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getMultipleTaxBreakdown()Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 44
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getMultipleTaxBreakdown()Lcom/squareup/print/sections/MultipleTaxBreakdownSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/MultipleTaxBreakdownSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 47
    :cond_7
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getFooter()Lcom/squareup/print/sections/FooterSection;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/squareup/print/sections/FooterSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 50
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getBarcode()Lcom/squareup/print/sections/BarcodeSection;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 51
    invoke-virtual {p1}, Lcom/squareup/print/payload/ReceiptPayload;->getBarcode()Lcom/squareup/print/sections/BarcodeSection;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/print/sections/BarcodeSection;->renderBitmap(Lcom/squareup/print/ThermalBitmapBuilder;)V

    .line 54
    :cond_8
    invoke-virtual {p2}, Lcom/squareup/print/ThermalBitmapBuilder;->render()Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method
