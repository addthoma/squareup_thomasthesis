.class public final enum Lcom/squareup/print/PrintTargetRouter$RouteResult;
.super Ljava/lang/Enum;
.source "PrintTargetRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/PrintTargetRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RouteResult"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/print/PrintTargetRouter$RouteResult;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/print/PrintTargetRouter$RouteResult;

.field public static final enum TARGETED_HARDWARE_PRINTER_AVAILABLE:Lcom/squareup/print/PrintTargetRouter$RouteResult;

.field public static final enum TARGETED_HARDWARE_PRINTER_UNAVAILABLE:Lcom/squareup/print/PrintTargetRouter$RouteResult;

.field public static final enum TARGET_DOES_NOT_EXIST:Lcom/squareup/print/PrintTargetRouter$RouteResult;

.field public static final enum TARGET_HAS_NO_HARDWARE_PRINTER:Lcom/squareup/print/PrintTargetRouter$RouteResult;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 13
    new-instance v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;

    const/4 v1, 0x0

    const-string v2, "TARGETED_HARDWARE_PRINTER_AVAILABLE"

    invoke-direct {v0, v2, v1}, Lcom/squareup/print/PrintTargetRouter$RouteResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGETED_HARDWARE_PRINTER_AVAILABLE:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    .line 18
    new-instance v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;

    const/4 v2, 0x1

    const-string v3, "TARGET_DOES_NOT_EXIST"

    invoke-direct {v0, v3, v2}, Lcom/squareup/print/PrintTargetRouter$RouteResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGET_DOES_NOT_EXIST:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    .line 23
    new-instance v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;

    const/4 v3, 0x2

    const-string v4, "TARGET_HAS_NO_HARDWARE_PRINTER"

    invoke-direct {v0, v4, v3}, Lcom/squareup/print/PrintTargetRouter$RouteResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGET_HAS_NO_HARDWARE_PRINTER:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    .line 28
    new-instance v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;

    const/4 v4, 0x3

    const-string v5, "TARGETED_HARDWARE_PRINTER_UNAVAILABLE"

    invoke-direct {v0, v5, v4}, Lcom/squareup/print/PrintTargetRouter$RouteResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGETED_HARDWARE_PRINTER_UNAVAILABLE:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/print/PrintTargetRouter$RouteResult;

    .line 11
    sget-object v5, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGETED_HARDWARE_PRINTER_AVAILABLE:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGET_DOES_NOT_EXIST:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGET_HAS_NO_HARDWARE_PRINTER:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/print/PrintTargetRouter$RouteResult;->TARGETED_HARDWARE_PRINTER_UNAVAILABLE:Lcom/squareup/print/PrintTargetRouter$RouteResult;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;->$VALUES:[Lcom/squareup/print/PrintTargetRouter$RouteResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/print/PrintTargetRouter$RouteResult;
    .locals 1

    .line 11
    const-class v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/PrintTargetRouter$RouteResult;

    return-object p0
.end method

.method public static values()[Lcom/squareup/print/PrintTargetRouter$RouteResult;
    .locals 1

    .line 11
    sget-object v0, Lcom/squareup/print/PrintTargetRouter$RouteResult;->$VALUES:[Lcom/squareup/print/PrintTargetRouter$RouteResult;

    invoke-virtual {v0}, [Lcom/squareup/print/PrintTargetRouter$RouteResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/print/PrintTargetRouter$RouteResult;

    return-object v0
.end method
