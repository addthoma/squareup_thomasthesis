.class final enum Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;
.super Ljava/lang/Enum;
.source "ThermalBitmapBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/print/ThermalBitmapBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "RuleSize"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

.field public static final enum LARGE:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

.field public static final enum SMALL:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;


# instance fields
.field public final sizePx:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 119
    new-instance v0, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    const/4 v1, 0x0

    const-string v2, "LARGE"

    const/4 v3, 0x4

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->LARGE:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    .line 120
    new-instance v0, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    const/4 v2, 0x2

    const/4 v3, 0x1

    const-string v4, "SMALL"

    invoke-direct {v0, v4, v3, v2}, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    new-array v0, v2, [Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    .line 118
    sget-object v2, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->LARGE:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    aput-object v2, v0, v1

    sget-object v1, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->SMALL:Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->$VALUES:[Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 124
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 125
    iput p3, p0, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->sizePx:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;
    .locals 1

    .line 118
    const-class v0, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    return-object p0
.end method

.method public static values()[Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;
    .locals 1

    .line 118
    sget-object v0, Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->$VALUES:[Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    invoke-virtual {v0}, [Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/print/ThermalBitmapBuilder$RuleSize;

    return-object v0
.end method
