.class public abstract Lcom/squareup/pushmessages/PushMessage;
.super Ljava/lang/Object;
.source "PushMessage.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/pushmessages/PushMessage$TicketsSyncPushMessage;,
        Lcom/squareup/pushmessages/PushMessage$PushNotification;,
        Lcom/squareup/pushmessages/PushMessage$AppointmentsPushNotification;,
        Lcom/squareup/pushmessages/PushMessage$SupportMessagingNotification;,
        Lcom/squareup/pushmessages/PushMessage$CogsSyncPushMessage;,
        Lcom/squareup/pushmessages/PushMessage$AppointmentsSyncPushMessage;,
        Lcom/squareup/pushmessages/PushMessage$InvoicesUnitSettingsSync;,
        Lcom/squareup/pushmessages/PushMessage$InvoicesMetricsSync;,
        Lcom/squareup/pushmessages/PushMessage$EstimateDefaultsSync;,
        Lcom/squareup/pushmessages/PushMessage$OrdersUpdated;,
        Lcom/squareup/pushmessages/PushMessage$CheckoutCreated;,
        Lcom/squareup/pushmessages/PushMessage$MessagesSync;,
        Lcom/squareup/pushmessages/PushMessage$UploadClientLedger;,
        Lcom/squareup/pushmessages/PushMessage$InventoryAvailabilityUpdated;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u000e\u0003\u0004\u0005\u0006\u0007\u0008\t\n\u000b\u000c\r\u000e\u000f\u0010B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u000e\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/pushmessages/PushMessage;",
        "",
        "()V",
        "AppointmentsPushNotification",
        "AppointmentsSyncPushMessage",
        "CheckoutCreated",
        "CogsSyncPushMessage",
        "EstimateDefaultsSync",
        "InventoryAvailabilityUpdated",
        "InvoicesMetricsSync",
        "InvoicesUnitSettingsSync",
        "MessagesSync",
        "OrdersUpdated",
        "PushNotification",
        "SupportMessagingNotification",
        "TicketsSyncPushMessage",
        "UploadClientLedger",
        "Lcom/squareup/pushmessages/PushMessage$TicketsSyncPushMessage;",
        "Lcom/squareup/pushmessages/PushMessage$PushNotification;",
        "Lcom/squareup/pushmessages/PushMessage$AppointmentsPushNotification;",
        "Lcom/squareup/pushmessages/PushMessage$SupportMessagingNotification;",
        "Lcom/squareup/pushmessages/PushMessage$CogsSyncPushMessage;",
        "Lcom/squareup/pushmessages/PushMessage$AppointmentsSyncPushMessage;",
        "Lcom/squareup/pushmessages/PushMessage$InvoicesUnitSettingsSync;",
        "Lcom/squareup/pushmessages/PushMessage$InvoicesMetricsSync;",
        "Lcom/squareup/pushmessages/PushMessage$EstimateDefaultsSync;",
        "Lcom/squareup/pushmessages/PushMessage$OrdersUpdated;",
        "Lcom/squareup/pushmessages/PushMessage$CheckoutCreated;",
        "Lcom/squareup/pushmessages/PushMessage$MessagesSync;",
        "Lcom/squareup/pushmessages/PushMessage$UploadClientLedger;",
        "Lcom/squareup/pushmessages/PushMessage$InventoryAvailabilityUpdated;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 5
    invoke-direct {p0}, Lcom/squareup/pushmessages/PushMessage;-><init>()V

    return-void
.end method
