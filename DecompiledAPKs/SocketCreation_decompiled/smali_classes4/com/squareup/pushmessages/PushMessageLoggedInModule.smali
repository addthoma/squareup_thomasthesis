.class public abstract Lcom/squareup/pushmessages/PushMessageLoggedInModule;
.super Ljava/lang/Object;
.source "PushMessageLoggedInModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static providePushServicesAsSetForLoggedIn(Lcom/squareup/pushmessages/RealPushNotificationNotifier;Lcom/squareup/pushmessages/RealPushServiceRegistrar;)Ljava/util/Set;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation runtime Ldagger/multibindings/ElementsIntoSet;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/pushmessages/RealPushNotificationNotifier;",
            "Lcom/squareup/pushmessages/RealPushServiceRegistrar;",
            ")",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x2

    new-array v0, v0, [Lmortar/Scoped;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 p0, 0x1

    aput-object p1, v0, p0

    .line 27
    invoke-static {v0}, Lcom/squareup/util/SquareCollections;->asSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract bindPushNotificationNotifier(Lcom/squareup/pushmessages/RealPushNotificationNotifier;)Lcom/squareup/pushmessages/PushNotificationNotifier;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindPushServiceRegistrar(Lcom/squareup/pushmessages/RealPushServiceRegistrar;)Lcom/squareup/pushmessages/PushServiceRegistrar;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
