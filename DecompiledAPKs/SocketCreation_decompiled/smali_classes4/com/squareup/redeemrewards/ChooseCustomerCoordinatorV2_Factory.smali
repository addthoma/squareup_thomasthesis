.class public final Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2_Factory;
.super Ljava/lang/Object;
.source "ChooseCustomerCoordinatorV2_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;",
        ">;"
    }
.end annotation


# instance fields
.field private final contactLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2_Factory;->contactLoaderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ">;)",
            "Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;Lcom/squareup/crm/RolodexContactLoader;)Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;-><init>(Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;Lcom/squareup/crm/RolodexContactLoader;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;

    iget-object v1, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2_Factory;->contactLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/crm/RolodexContactLoader;

    invoke-static {v0, v1}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2_Factory;->newInstance(Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;Lcom/squareup/crm/RolodexContactLoader;)Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2_Factory;->get()Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;

    move-result-object v0

    return-object v0
.end method
