.class public final Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "RedeemRewardWorkflowBootstrapScope.kt"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope$ParentComponent;,
        Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope$Component;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c7\u0002\u0018\u00002\u00020\u0001:\u0002\u0005\u0006B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;",
        "Lcom/squareup/ui/main/InMainActivityScope;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "Component",
        "ParentComponent",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 24
    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;

    invoke-direct {v0}, Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;-><init>()V

    sput-object v0, Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;

    .line 39
    sget-object v0, Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;->INSTANCE:Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "forSingleton(RedeemRewardWorkflowBootstrapScope)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/redeemrewards/RedeemRewardWorkflowBootstrapScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method
