.class final Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;
.super Lkotlin/jvm/internal/Lambda;
.source "ChooseCustomerCoordinatorV2.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "state",
        "Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

.field final synthetic $message:Lcom/squareup/widgets/MessageView;

.field final synthetic $progress:Landroid/widget/ProgressBar;

.field final synthetic $r:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Landroid/widget/ProgressBar;Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;Lcom/squareup/widgets/MessageView;Landroid/content/res/Resources;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;->$progress:Landroid/widget/ProgressBar;

    iput-object p2, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;->$contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    iput-object p3, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;->$message:Lcom/squareup/widgets/MessageView;

    iput-object p4, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;->$r:Landroid/content/res/Resources;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 36
    check-cast p1, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;->invoke(Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V
    .locals 4

    if-eqz p1, :cond_1

    .line 88
    sget-object v0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/16 v2, 0x8

    packed-switch v0, :pswitch_data_0

    goto :goto_2

    .line 98
    :pswitch_0
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;->$progress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;->$contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->setVisibility(I)V

    .line 100
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;->$message:Lcom/squareup/widgets/MessageView;

    iget-object v2, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;->$r:Landroid/content/res/Resources;

    .line 101
    sget-object v3, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->SHOWING_FAILED_TO_LOAD:Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;

    if-ne p1, v3, :cond_0

    .line 102
    sget p1, Lcom/squareup/crm/R$string;->crm_failed_to_load_customers:I

    goto :goto_0

    .line 104
    :cond_0
    sget p1, Lcom/squareup/crm/R$string;->crm_contact_search_empty:I

    .line 100
    :goto_0
    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;->$message:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    goto :goto_1

    .line 92
    :pswitch_1
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;->$progress:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 93
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;->$contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->setVisibility(I)V

    .line 94
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;->$message:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, v2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    goto :goto_1

    .line 89
    :pswitch_2
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseCustomerCoordinatorV2$attach$7;->$progress:Landroid/widget/ProgressBar;

    invoke-virtual {p1, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_1
    return-void

    .line 108
    :cond_1
    :goto_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected visual state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
