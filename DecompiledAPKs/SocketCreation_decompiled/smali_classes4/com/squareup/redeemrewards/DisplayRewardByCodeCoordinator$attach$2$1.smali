.class final Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DisplayRewardByCodeCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->invoke(Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Unit;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\n\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n\u00a2\u0006\u0004\u0008\u0003\u0010\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "invoke",
        "(Lkotlin/Unit;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $coupon:Lcom/squareup/protos/client/coupons/Coupon;

.field final synthetic this$0:Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2$1;->this$0:Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;

    iput-object p2, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2$1;->$coupon:Lcom/squareup/protos/client/coupons/Coupon;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2$1;->invoke(Lkotlin/Unit;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Unit;)V
    .locals 2

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object p1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2$1;->this$0:Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;

    iget-object p1, p1, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->this$0:Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;

    invoke-static {p1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;->access$getRunner$p(Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;)Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2$1;->$coupon:Lcom/squareup/protos/client/coupons/Coupon;

    const-string v1, "coupon"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;->applyCouponFromCode(Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 73
    iget-object p1, p0, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2$1;->this$0:Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;

    iget-object p1, p1, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator$attach$2;->this$0:Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;

    invoke-static {p1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;->access$getRunner$p(Lcom/squareup/redeemrewards/DisplayRewardByCodeCoordinator;)Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;->closeDisplayRewardByCodeScreenOnRedeemReward()V

    return-void
.end method
