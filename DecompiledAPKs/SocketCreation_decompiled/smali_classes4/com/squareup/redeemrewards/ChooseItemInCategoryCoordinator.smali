.class public final Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ChooseItemInCategoryCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$RewardItemRow;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseItemInCategoryCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseItemInCategoryCoordinator.kt\ncom/squareup/redeemrewards/ChooseItemInCategoryCoordinator\n+ 2 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 3 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 5 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 6 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,90:1\n49#2:91\n50#2,3:97\n53#2:112\n599#3,4:92\n601#3:96\n310#4,3:100\n313#4,3:109\n35#5,6:103\n1360#6:113\n1429#6,3:114\n950#6:117\n*E\n*S KotlinDebug\n*F\n+ 1 ChooseItemInCategoryCoordinator.kt\ncom/squareup/redeemrewards/ChooseItemInCategoryCoordinator\n*L\n57#1:91\n57#1,3:97\n57#1:112\n57#1,4:92\n57#1:96\n57#1,3:100\n57#1,3:109\n57#1,6:103\n85#1:113\n85#1,3:114\n86#1:117\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001:\u0001,BO\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u000e\u0008\u0001\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u00a2\u0006\u0002\u0010\u0013J\u0010\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020%H\u0016J\u0010\u0010)\u001a\u00020\'2\u0006\u0010*\u001a\u00020+H\u0002R\u001b\u0010\u0014\u001a\u00020\u00158BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0018\u0010\u0019\u001a\u0004\u0008\u0016\u0010\u0017R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001d\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u001eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u001f\u001a\u00020 8BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008#\u0010\u0019\u001a\u0004\u0008!\u0010\"R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "itemPhotos",
        "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
        "priceFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "getActionBar",
        "()Lcom/squareup/noho/NohoActionBar;",
        "actionBar$delegate",
        "Lkotlin/Lazy;",
        "itemsList",
        "",
        "Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$RewardItemRow;",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "getRecyclerView",
        "()Landroidx/recyclerview/widget/RecyclerView;",
        "recyclerView$delegate",
        "rootView",
        "Landroid/view/View;",
        "attach",
        "",
        "view",
        "onScreenData",
        "screenData",
        "Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;",
        "RewardItemRow",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar$delegate:Lkotlin/Lazy;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private final itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

.field private itemsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$RewardItemRow;",
            ">;"
        }
    .end annotation
.end field

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$RewardItemRow;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final recyclerView$delegate:Lkotlin/Lazy;

.field private final res:Lcom/squareup/util/Res;

.field private rootView:Landroid/view/View;

.field private final runner:Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/util/Res;)V
    .locals 1
    .param p5    # Lcom/squareup/text/Formatter;
        .annotation runtime Lcom/squareup/text/ForPercentage;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPhotos"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->runner:Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;

    iput-object p2, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p3, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iput-object p4, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p5, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->percentageFormatter:Lcom/squareup/text/Formatter;

    iput-object p6, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iput-object p7, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p8, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->res:Lcom/squareup/util/Res;

    .line 42
    new-instance p1, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$actionBar$2;

    invoke-direct {p1, p0}, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$actionBar$2;-><init>(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->actionBar$delegate:Lkotlin/Lazy;

    .line 43
    new-instance p1, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$recyclerView$2;

    invoke-direct {p1, p0}, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$recyclerView$2;-><init>(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->recyclerView$delegate:Lkotlin/Lazy;

    return-void
.end method

.method public static final synthetic access$getCurrencyCode$p(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method

.method public static final synthetic access$getDurationFormatter$p(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)Lcom/squareup/text/DurationFormatter;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    return-object p0
.end method

.method public static final synthetic access$getItemPhotos$p(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)Lcom/squareup/ui/photo/ItemPhoto$Factory;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    return-object p0
.end method

.method public static final synthetic access$getPercentageFormatter$p(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method public static final synthetic access$getPriceFormatter$p(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)Lcom/squareup/quantity/PerUnitFormatter;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$getRootView$p(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)Landroid/view/View;
    .locals 1

    .line 29
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->rootView:Landroid/view/View;

    if-nez p0, :cond_0

    const-string v0, "rootView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->runner:Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$onScreenData(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->onScreenData(Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;)V

    return-void
.end method

.method public static final synthetic access$setRootView$p(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;Landroid/view/View;)V
    .locals 0

    .line 29
    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->rootView:Landroid/view/View;

    return-void
.end method

.method private final getActionBar()Lcom/squareup/noho/NohoActionBar;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->actionBar$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    return-object v0
.end method

.method private final getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->recyclerView$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    return-object v0
.end method

.method private final onScreenData(Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;)V
    .locals 3

    .line 84
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;->getItemsData()Ljava/util/Set;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p1, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 114
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 115
    check-cast v1, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    .line 85
    new-instance v2, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$RewardItemRow;

    invoke-direct {v2, v1}, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$RewardItemRow;-><init>(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;)V

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 116
    :cond_0
    check-cast v0, Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 117
    new-instance p1, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$onScreenData$$inlined$sortedBy$1;

    invoke-direct {p1}, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$onScreenData$$inlined$sortedBy$1;-><init>()V

    check-cast p1, Ljava/util/Comparator;

    invoke-static {v0, p1}, Lkotlin/collections/CollectionsKt;->sortedWith(Ljava/lang/Iterable;Ljava/util/Comparator;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->itemsList:Ljava/util/List;

    .line 87
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez p1, :cond_1

    const-string v0, "recycler"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->itemsList:Ljava/util/List;

    if-nez v0, :cond_2

    const-string v1, "itemsList"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-static {v0}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Recycler;->setData(Lcom/squareup/cycler/DataSource;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->rootView:Landroid/view/View;

    .line 55
    invoke-direct {p0}, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->getActionBar()Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    .line 52
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 53
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/redeemrewards/R$string;->redeem_rewards_item_in_category_title:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 54
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$attach$1;

    invoke-direct {v3, p0}, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$attach$1;-><init>(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 55
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    invoke-direct {p0}, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->getRecyclerView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    .line 91
    sget-object v2, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 92
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 93
    new-instance v2, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v2}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 97
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 98
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 101
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v3, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$$special$$inlined$row$1;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 59
    sget v3, Lcom/squareup/librarylist/R$layout;->library_panel_list_noho_row:I

    .line 103
    new-instance v4, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$attach$$inlined$adopt$lambda$1;

    invoke-direct {v4, v3, p0}, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$attach$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)V

    check-cast v4, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v4}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 101
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 100
    invoke-virtual {v2, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 95
    invoke-virtual {v2, v1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    .line 74
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;->runner:Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;->getChooseItemScreenData()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$attach$3;

    invoke-direct {v1, p0}, Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator$attach$3;-><init>(Lcom/squareup/redeemrewards/ChooseItemInCategoryCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void

    .line 92
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
