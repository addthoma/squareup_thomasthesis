.class final Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealRedeemRewardWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->processViewRewardsOutput(Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/redeemrewards/ViewRewardsOutput;)Lcom/squareup/workflow/WorkflowAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/workflow/WorkflowAction$Mutator;",
        "Lcom/squareup/redeemrewards/RedeemRewardState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $nextState:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

.field final synthetic $output:Lcom/squareup/redeemrewards/ViewRewardsOutput;

.field final synthetic $this_processViewRewardsOutput:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

.field final synthetic this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;Lcom/squareup/redeemrewards/ViewRewardsOutput;Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$2;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$2;->$this_processViewRewardsOutput:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    iput-object p3, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$2;->$output:Lcom/squareup/redeemrewards/ViewRewardsOutput;

    iput-object p4, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$2;->$nextState:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/workflow/WorkflowAction$Mutator;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$2;->invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/workflow/WorkflowAction$Mutator;)Ljava/lang/Void;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/WorkflowAction$Mutator<",
            "Lcom/squareup/redeemrewards/RedeemRewardState;",
            ">;)",
            "Ljava/lang/Void;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 431
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$2;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    invoke-static {v0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$getHoldsCoupons$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)Lcom/squareup/checkout/HoldsCoupons;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$2;->$output:Lcom/squareup/redeemrewards/ViewRewardsOutput;

    check-cast v1, Lcom/squareup/redeemrewards/ViewRewardsOutput$ApplyCoupon;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/ViewRewardsOutput$ApplyCoupon;->getCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/checkout/HoldsCoupons;->apply(Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 432
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$2;->this$0:Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;

    invoke-static {v0}, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;->access$getHoldsCustomer$p(Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$2;->$this_processViewRewardsOutput:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    invoke-virtual {v1}, Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2}, Lcom/squareup/payment/Transaction;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    .line 435
    iget-object v0, p0, Lcom/squareup/redeemrewards/RealRedeemRewardWorkflow$processViewRewardsOutput$2;->$nextState:Lcom/squareup/redeemrewards/RedeemRewardState$ViewingRewards;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Mutator;->setState(Ljava/lang/Object;)V

    return-object v2
.end method
