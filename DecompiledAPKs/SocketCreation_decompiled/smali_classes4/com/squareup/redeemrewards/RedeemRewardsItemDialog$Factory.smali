.class public final Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory;
.super Ljava/lang/Object;
.source "RedeemRewardsItemDialog.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/RedeemRewardsItemDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRedeemRewardsItemDialog.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RedeemRewardsItemDialog.kt\ncom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,93:1\n52#2:94\n*E\n*S KotlinDebug\n*F\n+ 1 RedeemRewardsItemDialog.kt\ncom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory\n*L\n24#1:94\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J \u0010\u0008\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "()V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "screenData",
        "Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;",
        "runner",
        "Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory;Landroid/content/Context;Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Landroid/app/Dialog;
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory;->createDialog(Landroid/content/Context;Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Landroid/app/Dialog;

    move-result-object p0

    return-object p0
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Landroid/app/Dialog;
    .locals 2

    .line 41
    invoke-virtual {p2}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;->getItemsData()Ljava/util/Set;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 42
    sget v0, Lcom/squareup/redeemrewards/R$string;->redeem_rewards_item_popup_body:I

    invoke-static {p1, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/content/Context;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 43
    invoke-virtual {p2}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$RedeemRewardItemScreenData;->getItemsData()Ljava/util/Set;

    move-result-object p2

    check-cast p2, Ljava/lang/Iterable;

    invoke-static {p2}, Lkotlin/collections/CollectionsKt;->first(Ljava/lang/Iterable;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    const-string v1, "item_name"

    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 44
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 45
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    .line 47
    :cond_1
    sget p2, Lcom/squareup/redeemrewards/R$string;->redeem_rewards_item_popup_body_category:I

    invoke-virtual {p1, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p2

    const-string v0, "context.getString(R.stri\u2026item_popup_body_category)"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    :goto_0
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 51
    sget v1, Lcom/squareup/redeemrewards/R$string;->redeem_rewards_item_popup_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 52
    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p2

    .line 53
    sget v0, Lcom/squareup/redeemrewards/R$string;->redeem_rewards_item_popup_positive_button:I

    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$createDialog$1;

    invoke-direct {v1, p3, p1}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$createDialog$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Landroid/content/Context;)V

    check-cast v1, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p2, v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 57
    sget p2, Lcom/squareup/noho/R$drawable;->noho_selector_primary_button_background:I

    .line 56
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 60
    sget p2, Lcom/squareup/noho/R$color;->noho_color_selector_primary_button_text:I

    .line 59
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 62
    sget p2, Lcom/squareup/redeemrewards/R$string;->redeem_rewards_item_popup_negative_button:I

    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$createDialog$2;

    invoke-direct {v0, p3}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$createDialog$2;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V

    check-cast v0, Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 65
    new-instance p2, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$createDialog$3;

    invoke-direct {p2, p3}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$createDialog$3;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V

    check-cast p2, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 68
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "ThemedAlertDialog.Builde\u2026    }\n          .create()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/app/Dialog;

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 94
    const-class v0, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;

    .line 25
    invoke-interface {v0}, Lcom/squareup/redeemrewards/RedeemRewardsScope$Component;->redeemRewardsScopeRunner()Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    move-result-object v0

    .line 27
    invoke-virtual {v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->getChooseItemScreenData()Lio/reactivex/Observable;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 28
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v1

    .line 29
    invoke-virtual {v1}, Lio/reactivex/Observable;->singleOrError()Lio/reactivex/Single;

    move-result-object v1

    .line 30
    new-instance v2, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$create$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory$create$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsItemDialog$Factory;Landroid/content/Context;Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "scopeRunner.getChooseIte\u2026 scopeRunner)\n          }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
