.class public final Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ChooseContactForRewardCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChooseContactForRewardCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ChooseContactForRewardCoordinator.kt\ncom/squareup/redeemrewards/ChooseContactForRewardCoordinator\n*L\n1#1,203:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0001 B1\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0010\u0010\u001e\u001a\u00020\u001c2\u0006\u0010\u001f\u001a\u00020\u0005H\u0002R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0015\u001a\u0010\u0012\u000c\u0012\n \u0018*\u0004\u0018\u00010\u00170\u00170\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "contactLoader",
        "Lcom/squareup/crm/RolodexContactLoader;",
        "(Lio/reactivex/Observable;Lcom/squareup/crm/RolodexContactLoader;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "contactList",
        "Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;",
        "message",
        "Lcom/squareup/widgets/MessageView;",
        "progress",
        "Landroid/widget/ProgressBar;",
        "searchBox",
        "Lcom/squareup/ui/XableEditText;",
        "searchTermChanged",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "view",
        "Landroid/view/View;",
        "attach",
        "",
        "bindViews",
        "onScreen",
        "screen",
        "Factory",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

.field private final contactLoader:Lcom/squareup/crm/RolodexContactLoader;

.field private message:Lcom/squareup/widgets/MessageView;

.field private progress:Landroid/widget/ProgressBar;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field

.field private searchBox:Lcom/squareup/ui/XableEditText;

.field private final searchTermChanged:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/crm/RolodexContactLoader;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ")V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "contactLoader"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    .line 61
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<String>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->searchTermChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$getContactList$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;
    .locals 1

    .line 49
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    if-nez p0, :cond_0

    const-string v0, "contactList"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getContactLoader$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/crm/RolodexContactLoader;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    return-object p0
.end method

.method public static final synthetic access$getMessage$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/widgets/MessageView;
    .locals 1

    .line 49
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->message:Lcom/squareup/widgets/MessageView;

    if-nez p0, :cond_0

    const-string v0, "message"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getProgress$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Landroid/widget/ProgressBar;
    .locals 1

    .line 49
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->progress:Landroid/widget/ProgressBar;

    if-nez p0, :cond_0

    const-string v0, "progress"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getSearchBox$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/squareup/ui/XableEditText;
    .locals 1

    .line 49
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->searchBox:Lcom/squareup/ui/XableEditText;

    if-nez p0, :cond_0

    const-string v0, "searchBox"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getSearchTermChanged$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 49
    iget-object p0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->searchTermChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->onScreen(Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;)V

    return-void
.end method

.method public static final synthetic access$setContactList$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    return-void
.end method

.method public static final synthetic access$setMessage$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;Lcom/squareup/widgets/MessageView;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->message:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public static final synthetic access$setProgress$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;Landroid/widget/ProgressBar;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->progress:Landroid/widget/ProgressBar;

    return-void
.end method

.method public static final synthetic access$setSearchBox$p(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;Lcom/squareup/ui/XableEditText;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->searchBox:Lcom/squareup/ui/XableEditText;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 164
    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->view:Landroid/view/View;

    .line 165
    sget-object v0, Lcom/squareup/noho/NohoActionBar;->Companion:Lcom/squareup/noho/NohoActionBar$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoActionBar$Companion;->findIn(Landroid/view/View;)Lcom/squareup/noho/NohoActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 166
    sget v0, Lcom/squareup/redeemrewards/impl/R$id;->crm_search_box:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->searchBox:Lcom/squareup/ui/XableEditText;

    .line 167
    sget v0, Lcom/squareup/redeemrewards/impl/R$id;->crm_contact_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    iput-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    .line 168
    sget v0, Lcom/squareup/redeemrewards/impl/R$id;->crm_search_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->progress:Landroid/widget/ProgressBar;

    .line 169
    sget v0, Lcom/squareup/redeemrewards/impl/R$id;->crm_search_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->message:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method private final onScreen(Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;)V
    .locals 10

    .line 187
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 175
    :cond_0
    iget-object v2, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v2, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/noho/NohoActionBar;->getConfig()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config;->buildUpon()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 176
    sget-object v2, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$onScreen$1;

    invoke-direct {v3, p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$onScreen$1;-><init>(Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 179
    new-instance v2, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;->getTitle()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v3

    .line 181
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;->isLoading()Z

    move-result v1

    if-nez v1, :cond_2

    .line 182
    sget-object v4, Lcom/squareup/noho/NohoActionButtonStyle;->SECONDARY:Lcom/squareup/noho/NohoActionButtonStyle;

    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/redeemrewards/impl/R$string;->redeem_rewards_use_code:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    move-object v5, v1

    check-cast v5, Lcom/squareup/resources/TextModel;

    const/4 v6, 0x0

    new-instance v1, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$onScreen$$inlined$let$lambda$1;

    invoke-direct {v1, p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$onScreen$$inlined$let$lambda$1;-><init>(Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;)V

    move-object v7, v1

    check-cast v7, Lkotlin/jvm/functions/Function0;

    const/4 v8, 0x4

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton$default(Lcom/squareup/noho/NohoActionBar$Config$Builder;Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    goto :goto_0

    .line 185
    :cond_2
    invoke-virtual {v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->hideAction()Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v1

    .line 187
    :goto_0
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 188
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->view:Landroid/view/View;

    if-nez v0, :cond_3

    const-string/jumbo v1, "view"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v1, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$onScreen$3;

    invoke-direct {v1, p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$onScreen$3;-><init>(Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 190
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;->isLoading()Z

    move-result v0

    const-string v1, "searchBox"

    if-eqz v0, :cond_8

    .line 191
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->progress:Landroid/widget/ProgressBar;

    if-nez p1, :cond_4

    const-string v0, "progress"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setVisible(Landroid/view/View;)V

    .line 192
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->message:Lcom/squareup/widgets/MessageView;

    if-nez p1, :cond_5

    const-string v0, "message"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 193
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->searchBox:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    .line 194
    iget-object p1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    if-nez p1, :cond_7

    const-string v0, "contactList"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lcom/squareup/util/Views;->setGone(Landroid/view/View;)V

    goto :goto_2

    .line 196
    :cond_8
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->searchBox:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_9

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    const-string v2, "searchBox.editText"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_a
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;->getSearchTerm()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_c

    .line 197
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->searchBox:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_b

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;->getSearchTerm()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 199
    :cond_c
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    new-instance v1, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;->getSearchTerm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardScreen;->getSearchTerm()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-direct {v1, v2, p1}, Lcom/squareup/crm/RolodexContactLoader$SearchTerm;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v0, v1}, Lcom/squareup/crm/RolodexContactLoader;->setSearchTerm(Lcom/squareup/crm/RolodexContactLoader$SearchTerm;)V

    :goto_2
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 76
    invoke-direct {p0, p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->bindViews(Landroid/view/View;)V

    .line 78
    new-instance v0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$textListener$1;

    invoke-direct {v0, p0}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$textListener$1;-><init>(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)V

    .line 84
    iget-object v1, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->searchBox:Lcom/squareup/ui/XableEditText;

    const-string v2, "searchBox"

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/text/TextWatcher;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 86
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->searchBox:Lcom/squareup/ui/XableEditText;

    if-nez v0, :cond_1

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$1;-><init>(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)V

    check-cast v1, Lcom/squareup/debounce/DebouncedOnEditorActionListener;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    const-string v1, "contactList"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->init(Lcom/squareup/crm/RolodexContactLoader;I)V

    .line 104
    iget-object v0, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->screens:Lio/reactivex/Observable;

    sget-object v2, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$unwrappedScreens$1;->INSTANCE:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$unwrappedScreens$1;

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 106
    sget-object v2, Lcom/squareup/util/rx2/Observables;->INSTANCE:Lcom/squareup/util/rx2/Observables;

    .line 107
    iget-object v3, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->contactLoader:Lcom/squareup/crm/RolodexContactLoader;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/squareup/crm/RolodexContactLoaderHelper;->visualStateOf(Lcom/squareup/crm/RolodexContactLoader;Ljava/lang/String;)Lrx/Observable;

    move-result-object v3

    const-string v4, "RolodexContactLoaderHelp\u2026teOf(contactLoader, null)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v3

    const-string v4, "unwrappedScreens"

    .line 108
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-virtual {v2, v3, v0}, Lcom/squareup/util/rx2/Observables;->combineLatest(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v2

    .line 110
    sget-object v3, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$2;->INSTANCE:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$2;

    check-cast v3, Lio/reactivex/functions/Predicate;

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "Observables.combineLates\u2026n) -> !screen.isLoading }"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    new-instance v3, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;

    invoke-direct {v3, p0, p1}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$3;-><init>(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;Landroid/view/View;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, p1, v3}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 143
    iget-object v2, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->searchTermChanged:Lcom/jakewharton/rxrelay2/PublishRelay;

    check-cast v2, Lio/reactivex/Observable;

    .line 144
    move-object v3, v0

    check-cast v3, Lio/reactivex/ObservableSource;

    invoke-static {v2, v3}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v2

    .line 145
    new-instance v4, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$4;

    invoke-direct {v4, p0}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$4;-><init>(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-static {v2, p1, v4}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 153
    iget-object v2, p0, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;->contactList:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    if-nez v2, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v2}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->onContactClicked()Lrx/Observable;

    move-result-object v1

    const-string v2, "contactList.onContactClicked()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    invoke-static {v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v1

    .line 155
    invoke-static {v1, v3}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v1

    .line 156
    sget-object v2, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$5;->INSTANCE:Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$5;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-static {v1, p1, v2}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 160
    new-instance v1, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$6;

    move-object v2, p0

    check-cast v2, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;

    invoke-direct {v1, v2}, Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator$attach$6;-><init>(Lcom/squareup/redeemrewards/ChooseContactForRewardCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
