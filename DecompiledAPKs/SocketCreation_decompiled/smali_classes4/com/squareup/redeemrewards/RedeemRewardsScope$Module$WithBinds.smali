.class public abstract Lcom/squareup/redeemrewards/RedeemRewardsScope$Module$WithBinds;
.super Ljava/lang/Object;
.source "RedeemRewardsScope.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "WithBinds"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00a7\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H!\u00a2\u0006\u0002\u0008\u0007J\u0015\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008\u000cJ\u0015\u0010\r\u001a\u00020\u000e2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008\u000fJ\u0015\u0010\u0010\u001a\u00020\u00112\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008\u0012J\u0015\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H!\u00a2\u0006\u0002\u0008\u0017J\u0015\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH!\u00a2\u0006\u0002\u0008\u001cJ\u0015\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008\u001fJ\u0015\u0010 \u001a\u00020!2\u0006\u0010\n\u001a\u00020\u000bH!\u00a2\u0006\u0002\u0008\"\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/RedeemRewardsScope$Module$WithBinds;",
        "",
        "(Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;)V",
        "bindEntryHandler",
        "Lcom/squareup/librarylist/LibraryListEntryHandler;",
        "entryHandler",
        "Lcom/squareup/ui/main/CheckoutEntryHandler;",
        "bindEntryHandler$redeem_rewards_release",
        "chooseCustomerScreenRunnerV2",
        "Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;",
        "runner",
        "Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;",
        "chooseCustomerScreenRunnerV2$redeem_rewards_release",
        "chooseItemInCategoryRunner",
        "Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;",
        "chooseItemInCategoryRunner$redeem_rewards_release",
        "displayRewardByCodeScreenRunner",
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;",
        "displayRewardByCodeScreenRunner$redeem_rewards_release",
        "holdsCoupons",
        "Lcom/squareup/checkout/HoldsCoupons;",
        "transactionDiscountAdapter",
        "Lcom/squareup/payment/TransactionDiscountAdapter;",
        "holdsCoupons$redeem_rewards_release",
        "holdsCustomer",
        "Lcom/squareup/payment/crm/HoldsCustomer;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "holdsCustomer$redeem_rewards_release",
        "lookupRewardByCodeScreenRunner",
        "Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Runner;",
        "lookupRewardByCodeScreenRunner$redeem_rewards_release",
        "redeemPointsV2ScreenRunner",
        "Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;",
        "redeemPointsV2ScreenRunner$redeem_rewards_release",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;


# direct methods
.method public constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 54
    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScope$Module$WithBinds;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScope$Module;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindEntryHandler$redeem_rewards_release(Lcom/squareup/ui/main/CheckoutEntryHandler;)Lcom/squareup/librarylist/LibraryListEntryHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract chooseCustomerScreenRunnerV2$redeem_rewards_release(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/redeemrewards/ChooseCustomerScreenV2$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract chooseItemInCategoryRunner$redeem_rewards_release(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/redeemrewards/ChooseItemInCategoryScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract displayRewardByCodeScreenRunner$redeem_rewards_release(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract holdsCoupons$redeem_rewards_release(Lcom/squareup/payment/TransactionDiscountAdapter;)Lcom/squareup/checkout/HoldsCoupons;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract holdsCustomer$redeem_rewards_release(Lcom/squareup/payment/Transaction;)Lcom/squareup/payment/crm/HoldsCustomer;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract lookupRewardByCodeScreenRunner$redeem_rewards_release(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/redeemrewards/LookupRewardByCodeScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract redeemPointsV2ScreenRunner$redeem_rewards_release(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/redeemrewards/RedeemRewardsV2Screen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
