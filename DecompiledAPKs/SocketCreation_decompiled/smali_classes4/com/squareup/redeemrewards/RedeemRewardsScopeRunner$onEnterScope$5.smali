.class final Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$5;
.super Ljava/lang/Object;
.source "RedeemRewardsScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;",
        "Lrx/Observable<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u000e\u0010\u0004\u001a\n \u0003*\u0004\u0018\u00010\u00050\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Observable;",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "kotlin.jvm.PlatformType",
        "c",
        "Lcom/squareup/protos/client/rolodex/Contact;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $onPointsResponseBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

.field final synthetic this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lcom/jakewharton/rxrelay/BehaviorRelay;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$5;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$5;->$onPointsResponseBusy:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$5;->call(Lcom/squareup/protos/client/rolodex/Contact;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/protos/client/rolodex/Contact;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
            ">;"
        }
    .end annotation

    .line 197
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$5;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-static {v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->access$getLoyaltyService$p(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;)Lcom/squareup/loyalty/LoyaltyServiceHelper;

    move-result-object v0

    const-string v1, "c"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->getLoyaltyStatusForContact(Lcom/squareup/protos/client/rolodex/Contact;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    .line 198
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    invoke-virtual {p1}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object p1

    .line 199
    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$5$1;

    invoke-direct {v0, p0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$5$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$5;)V

    check-cast v0, Lrx/functions/Action0;

    invoke-virtual {p1, v0}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    .line 200
    new-instance v0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$5$2;

    invoke-direct {v0, p0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$5$2;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$5;)V

    check-cast v0, Lrx/functions/Action0;

    invoke-virtual {p1, v0}, Lrx/Observable;->doOnTerminate(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
