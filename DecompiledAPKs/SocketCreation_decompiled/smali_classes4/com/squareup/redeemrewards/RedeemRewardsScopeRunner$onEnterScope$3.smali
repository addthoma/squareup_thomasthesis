.class final Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RedeemRewardsScopeRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRedeemRewardsScopeRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RedeemRewardsScopeRunner.kt\ncom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$3\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,607:1\n704#2:608\n777#2,2:609\n*E\n*S KotlinDebug\n*F\n+ 1 RedeemRewardsScopeRunner.kt\ncom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$3\n*L\n173#1:608\n173#1,2:609\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $scope:Lmortar/MortarScope;

.field final synthetic this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Lmortar/MortarScope;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$3;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    iput-object p2, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$3;->$scope:Lmortar/MortarScope;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;

    invoke-virtual {p0, p1}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$3;->invoke(Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;)V
    .locals 4

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    invoke-virtual {p1}, Lcom/squareup/redeemrewards/DisplayRewardByCodeScreen$Runner$LookupRewardResult;->getResponse$redeem_rewards_release()Lcom/squareup/protos/client/coupons/LookupCouponsResponse;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object p1, p1, Lcom/squareup/protos/client/coupons/LookupCouponsResponse;->coupon:Ljava/util/List;

    if-eqz p1, :cond_3

    check-cast p1, Ljava/lang/Iterable;

    .line 608
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/Collection;

    .line 609
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/coupons/Coupon;

    .line 173
    iget-object v2, v2, Lcom/squareup/protos/client/coupons/Coupon;->item_constraint_type:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    sget-object v3, Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;->VARIATION:Lcom/squareup/protos/client/coupons/Coupon$ItemConstraintType;

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 610
    :cond_2
    check-cast v0, Ljava/util/List;

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_5

    .line 176
    iget-object p1, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$3;->this$0:Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;

    invoke-static {p1, v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->access$getAllCouponConstraints(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_4
    invoke-static {p1, v0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;->access$getAllItemsInVariation(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner;Ljava/util/List;)Lrx/Single;

    move-result-object p1

    .line 177
    invoke-static {p1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p1

    .line 178
    iget-object v0, p0, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$3;->$scope:Lmortar/MortarScope;

    new-instance v1, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$3$1;

    invoke-direct {v1, p0}, Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$3$1;-><init>(Lcom/squareup/redeemrewards/RedeemRewardsScopeRunner$onEnterScope$3;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Single;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    :cond_5
    return-void
.end method
