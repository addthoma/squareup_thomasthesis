.class public final Lcom/squareup/redeemrewards/CouponSearch;
.super Ljava/lang/Object;
.source "CouponSearch.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCouponSearch.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CouponSearch.kt\ncom/squareup/redeemrewards/CouponSearch\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,39:1\n1550#2,3:40\n704#2:43\n777#2,2:44\n704#2:46\n777#2:47\n1529#2,3:48\n778#2:51\n1462#2,8:52\n*E\n*S KotlinDebug\n*F\n+ 1 CouponSearch.kt\ncom/squareup/redeemrewards/CouponSearch\n*L\n21#1,3:40\n24#1:43\n24#1,2:44\n29#1:46\n29#1:47\n29#1,3:48\n29#1:51\n36#1,8:52\n*E\n"
.end annotation

.annotation runtime Lkotlin/Deprecated;
    message = "TODO(LOYALTY-3530 Delete with removal of DISCOUNT_APPLY_MULTIPLE_COUPONS"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u00c7\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0007\u001a\u00020\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/CouponSearch;",
        "",
        "()V",
        "getUniqueCoupons",
        "",
        "Lcom/squareup/protos/client/coupons/Coupon;",
        "coupons",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/redeemrewards/CouponSearch;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9
    new-instance v0, Lcom/squareup/redeemrewards/CouponSearch;

    invoke-direct {v0}, Lcom/squareup/redeemrewards/CouponSearch;-><init>()V

    sput-object v0, Lcom/squareup/redeemrewards/CouponSearch;->INSTANCE:Lcom/squareup/redeemrewards/CouponSearch;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getUniqueCoupons(Ljava/util/List;Lcom/squareup/payment/Transaction;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;",
            "Lcom/squareup/payment/Transaction;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/coupons/Coupon;",
            ">;"
        }
    .end annotation

    const-string v0, "coupons"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transaction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 40
    instance-of v1, v0, Ljava/util/Collection;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 41
    :cond_1
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Discount;

    .line 21
    invoke-virtual {v1}, Lcom/squareup/checkout/Discount;->canOnlyBeAppliedToOneItem()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    .line 23
    :goto_0
    check-cast p1, Ljava/lang/Iterable;

    .line 43
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 44
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v5, v4

    check-cast v5, Lcom/squareup/protos/client/coupons/Coupon;

    .line 27
    iget-object v5, v5, Lcom/squareup/protos/client/coupons/Coupon;->reward:Lcom/squareup/protos/client/coupons/CouponReward;

    if-eqz v5, :cond_4

    iget-object v5, v5, Lcom/squareup/protos/client/coupons/CouponReward;->scope:Lcom/squareup/protos/client/coupons/Scope;

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    :goto_2
    sget-object v6, Lcom/squareup/protos/client/coupons/Scope;->ITEM:Lcom/squareup/protos/client/coupons/Scope;

    if-ne v5, v6, :cond_6

    if-nez v0, :cond_5

    goto :goto_3

    :cond_5
    const/4 v5, 0x0

    goto :goto_4

    :cond_6
    :goto_3
    const/4 v5, 0x1

    :goto_4
    if-eqz v5, :cond_3

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 45
    :cond_7
    check-cast v1, Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 46
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p1, Ljava/util/Collection;

    .line 47
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_8
    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/protos/client/coupons/Coupon;

    .line 33
    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->getAddedCouponsAndCartScopeDiscounts()Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    check-cast v5, Ljava/lang/Iterable;

    .line 48
    instance-of v6, v5, Ljava/util/Collection;

    if-eqz v6, :cond_a

    move-object v6, v5

    check-cast v6, Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_a

    :cond_9
    const/4 v4, 0x1

    goto :goto_6

    .line 49
    :cond_a
    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_b
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/squareup/checkout/Discount;

    .line 34
    invoke-static {v4}, Lcom/squareup/checkout/Discounts;->couponDefinitionToken(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Lcom/squareup/checkout/Discount;->getCouponDefinitionToken()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    xor-int/2addr v6, v3

    if-nez v6, :cond_b

    const/4 v4, 0x0

    :goto_6
    if-eqz v4, :cond_8

    invoke-interface {p1, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 51
    :cond_c
    check-cast p1, Ljava/util/List;

    check-cast p1, Ljava/lang/Iterable;

    .line 52
    new-instance p2, Ljava/util/HashSet;

    invoke-direct {p2}, Ljava/util/HashSet;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 54
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_d
    :goto_7
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 55
    move-object v2, v1

    check-cast v2, Lcom/squareup/protos/client/coupons/Coupon;

    .line 36
    invoke-static {v2}, Lcom/squareup/checkout/Discounts;->couponDefinitionToken(Lcom/squareup/protos/client/coupons/Coupon;)Ljava/lang/String;

    move-result-object v2

    .line 56
    invoke-virtual {p2, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 57
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 59
    :cond_e
    check-cast v0, Ljava/util/List;

    return-object v0
.end method
