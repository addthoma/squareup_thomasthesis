.class public abstract Lcom/squareup/redeemrewards/InRedeemRewardScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "InRedeemRewardScope.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0000\u0008&\u0018\u00002\u00020\u0001B\u000f\u0008\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/redeemrewards/InRedeemRewardScope;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "redeemRewardsScope",
        "Lcom/squareup/redeemrewards/RedeemRewardsScope;",
        "(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V",
        "getRedeemRewardsScope$redeem_rewards_release",
        "()Lcom/squareup/redeemrewards/RedeemRewardsScope;",
        "getParentKey",
        "",
        "redeem-rewards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final redeemRewardsScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;


# direct methods
.method protected constructor <init>(Lcom/squareup/redeemrewards/RedeemRewardsScope;)V
    .locals 1

    const-string v0, "redeemRewardsScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/redeemrewards/InRedeemRewardScope;->redeemRewardsScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;

    return-void
.end method


# virtual methods
.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 8
    iget-object v0, p0, Lcom/squareup/redeemrewards/InRedeemRewardScope;->redeemRewardsScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;

    return-object v0
.end method

.method public final getRedeemRewardsScope$redeem_rewards_release()Lcom/squareup/redeemrewards/RedeemRewardsScope;
    .locals 1

    .line 6
    iget-object v0, p0, Lcom/squareup/redeemrewards/InRedeemRewardScope;->redeemRewardsScope:Lcom/squareup/redeemrewards/RedeemRewardsScope;

    return-object v0
.end method
