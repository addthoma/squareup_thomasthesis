.class public final Lcom/squareup/printer/epson/EpsonRenderRowsUtil;
.super Ljava/lang/Object;
.source "EpsonRenderRowsUtil.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\nH\u0002\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/printer/epson/EpsonRenderRowsUtil;",
        "",
        "()V",
        "mapRenderedRowsToCommandBuffer",
        "",
        "epsonPrinterSdk",
        "Lcom/squareup/printer/epson/EpsonPrinterSdk;",
        "renderedRows",
        "Lcom/squareup/print/text/RenderedRows;",
        "sanitizeText",
        "",
        "text",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/printer/epson/EpsonRenderRowsUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12
    new-instance v0, Lcom/squareup/printer/epson/EpsonRenderRowsUtil;

    invoke-direct {v0}, Lcom/squareup/printer/epson/EpsonRenderRowsUtil;-><init>()V

    sput-object v0, Lcom/squareup/printer/epson/EpsonRenderRowsUtil;->INSTANCE:Lcom/squareup/printer/epson/EpsonRenderRowsUtil;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final sanitizeText(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const-string/jumbo v1, "\u00d7"

    const-string/jumbo v2, "x"

    const/4 v3, 0x0

    const/4 v4, 0x4

    const/4 v5, 0x0

    move-object v0, p1

    .line 57
    invoke-static/range {v0 .. v5}, Lkotlin/text/StringsKt;->replace$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "?"

    .line 58
    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->sanitizeTextToAsciiLetters(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object p1
.end method


# virtual methods
.method public final mapRenderedRowsToCommandBuffer(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lcom/squareup/print/text/RenderedRows;)V
    .locals 5

    const-string v0, "epsonPrinterSdk"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renderedRows"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-virtual {p2}, Lcom/squareup/print/text/RenderedRows;->getRows()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x1

    :cond_0
    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/print/text/Row;

    .line 24
    instance-of v3, v2, Lcom/squareup/print/text/Row$StyledRow;

    if-eqz v3, :cond_4

    if-eqz v1, :cond_3

    .line 29
    move-object v1, v2

    check-cast v1, Lcom/squareup/print/text/Row$StyledRow;

    invoke-virtual {v1}, Lcom/squareup/print/text/Row$StyledRow;->getStyles()Ljava/util/Set;

    move-result-object v3

    sget-object v4, Lcom/squareup/print/text/Style;->BOLD:Lcom/squareup/print/text/Style;

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 30
    invoke-virtual {v1}, Lcom/squareup/print/text/Row$StyledRow;->getStyles()Ljava/util/Set;

    move-result-object v1

    sget-object v4, Lcom/squareup/print/text/Style;->RED:Lcom/squareup/print/text/Style;

    invoke-interface {v1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    const/4 v4, -0x2

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    goto :goto_2

    :cond_1
    const/4 v3, -0x2

    :goto_2
    if-eqz v1, :cond_2

    const/4 v1, 0x2

    goto :goto_3

    :cond_2
    const/4 v1, -0x2

    .line 31
    :goto_3
    invoke-interface {p1, v4, v4, v3, v1}, Lcom/squareup/printer/epson/EpsonPrinterSdk;->addTextStyle(IIII)V

    .line 38
    :cond_3
    check-cast v2, Lcom/squareup/print/text/Row$StyledRow;

    invoke-virtual {v2}, Lcom/squareup/print/text/Row$StyledRow;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/printer/epson/EpsonRenderRowsUtil;->sanitizeText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/squareup/printer/epson/EpsonPrinterSdk;->addText(Ljava/lang/String;)V

    const/4 v1, 0x0

    goto :goto_1

    .line 43
    :cond_4
    sget-object v3, Lcom/squareup/print/text/Row$NewLine;->INSTANCE:Lcom/squareup/print/text/Row$NewLine;

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v1, "\n"

    .line 44
    invoke-interface {p1, v1}, Lcom/squareup/printer/epson/EpsonPrinterSdk;->addText(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :cond_5
    instance-of v3, v2, Lcom/squareup/print/text/Row$RawChars;

    if-eqz v3, :cond_0

    check-cast v2, Lcom/squareup/print/text/Row$RawChars;

    invoke-virtual {v2}, Lcom/squareup/print/text/Row$RawChars;->getChars()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/squareup/printer/epson/EpsonPrinterSdk;->addText(Ljava/lang/String;)V

    goto :goto_1

    .line 52
    :cond_6
    invoke-interface {p1, v0}, Lcom/squareup/printer/epson/EpsonPrinterSdk;->addCut(I)V

    return-void
.end method
