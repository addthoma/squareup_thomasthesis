.class public final Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonPrinterConnectionFactory;
.super Ljava/lang/Object;
.source "EpsonPrinterModule_ProvideEpsonPrinterConnectionFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/printer/epson/EpsonPrinterConnection;",
        ">;"
    }
.end annotation


# instance fields
.field private final coroutineScopeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineScope;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineScope;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonPrinterConnectionFactory;->featuresProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonPrinterConnectionFactory;->coroutineScopeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonPrinterConnectionFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lkotlinx/coroutines/CoroutineScope;",
            ">;)",
            "Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonPrinterConnectionFactory;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonPrinterConnectionFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonPrinterConnectionFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideEpsonPrinterConnection(Lcom/squareup/settings/server/Features;Lkotlinx/coroutines/CoroutineScope;)Lcom/squareup/printer/epson/EpsonPrinterConnection;
    .locals 0

    .line 41
    invoke-static {p0, p1}, Lcom/squareup/printer/epson/EpsonPrinterModule;->provideEpsonPrinterConnection(Lcom/squareup/settings/server/Features;Lkotlinx/coroutines/CoroutineScope;)Lcom/squareup/printer/epson/EpsonPrinterConnection;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/printer/epson/EpsonPrinterConnection;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/printer/epson/EpsonPrinterConnection;
    .locals 2

    .line 31
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonPrinterConnectionFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonPrinterConnectionFactory;->coroutineScopeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlinx/coroutines/CoroutineScope;

    invoke-static {v0, v1}, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonPrinterConnectionFactory;->provideEpsonPrinterConnection(Lcom/squareup/settings/server/Features;Lkotlinx/coroutines/CoroutineScope;)Lcom/squareup/printer/epson/EpsonPrinterConnection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/printer/epson/EpsonPrinterModule_ProvideEpsonPrinterConnectionFactory;->get()Lcom/squareup/printer/epson/EpsonPrinterConnection;

    move-result-object v0

    return-object v0
.end method
