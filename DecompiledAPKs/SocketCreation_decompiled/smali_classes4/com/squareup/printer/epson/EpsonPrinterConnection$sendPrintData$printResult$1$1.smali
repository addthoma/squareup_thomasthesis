.class final Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EpsonPrinterConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/printer/epson/PrinterStatusInfo;",
        "Ljava/lang/String;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "code",
        "",
        "status",
        "Lcom/squareup/printer/epson/PrinterStatusInfo;",
        "<anonymous parameter 2>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;


# direct methods
.method constructor <init>(Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 29
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Lcom/squareup/printer/epson/PrinterStatusInfo;

    check-cast p3, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1$1;->invoke(ILcom/squareup/printer/epson/PrinterStatusInfo;Ljava/lang/String;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILcom/squareup/printer/epson/PrinterStatusInfo;Ljava/lang/String;)V
    .locals 8

    const-string p3, "status"

    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 p3, 0xd

    if-ne p1, p3, :cond_0

    return-void

    .line 74
    :cond_0
    new-instance p3, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;

    .line 75
    sget-object v0, Lcom/squareup/printer/epson/EpsonCallbackMapper;->INSTANCE:Lcom/squareup/printer/epson/EpsonCallbackMapper;

    invoke-virtual {v0, p1}, Lcom/squareup/printer/epson/EpsonCallbackMapper;->classifyPrintResult(I)Lcom/squareup/print/PrintJob$PrintAttempt$Result;

    move-result-object v1

    .line 76
    invoke-static {p2}, Lcom/squareup/printer/epson/EpsonPrinterExtensionsKt;->toFormattedString(Lcom/squareup/printer/epson/PrinterStatusInfo;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    .line 77
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0x14

    const/4 v7, 0x0

    move-object v0, p3

    .line 74
    invoke-direct/range {v0 .. v7}, Lcom/squareup/printer/epson/EpsonPrinterConnection$SendPrintDataResult;-><init>(Lcom/squareup/print/PrintJob$PrintAttempt$Result;Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 79
    iget-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1$1;->this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;

    iget-object p1, p1, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$printResult$1;->$deferredPrintResult:Lkotlinx/coroutines/CompletableDeferred;

    invoke-interface {p1, p3}, Lkotlinx/coroutines/CompletableDeferred;->complete(Ljava/lang/Object;)Z

    return-void
.end method
