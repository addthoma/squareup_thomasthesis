.class public final Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;
.super Ljava/lang/Object;
.source "EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/print/EpsonUsbPrinterScout;",
        ">;"
    }
.end annotation


# instance fields
.field private final epsonPrinterFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/printer/epson/EpsonPrinter$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainThreadProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;"
        }
    .end annotation
.end field

.field private final usbManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/printer/epson/EpsonPrinter$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;->usbManagerProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;->epsonPrinterFactoryProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;->mainThreadProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/hardware/usb/UsbManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/printer/epson/EpsonPrinter$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/MainThread;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;"
        }
    .end annotation

    .line 48
    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideEpsonUsbPrinterScout(Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/printer/epson/EpsonPrinter$Factory;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;)Lcom/squareup/print/EpsonUsbPrinterScout;
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/printer/epson/EpsonPrinterModule;->Companion:Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/squareup/printer/epson/EpsonPrinterModule$Companion;->provideEpsonUsbPrinterScout(Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/printer/epson/EpsonPrinter$Factory;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;)Lcom/squareup/print/EpsonUsbPrinterScout;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/print/EpsonUsbPrinterScout;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/print/EpsonUsbPrinterScout;
    .locals 4

    .line 41
    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;->usbManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/hardware/usb/UsbManager;

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;->epsonPrinterFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/printer/epson/EpsonPrinter$Factory;

    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;->mainThreadProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/thread/executor/MainThread;

    iget-object v3, p0, Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;->provideEpsonUsbPrinterScout(Lcom/squareup/hardware/usb/UsbManager;Lcom/squareup/printer/epson/EpsonPrinter$Factory;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/settings/server/Features;)Lcom/squareup/print/EpsonUsbPrinterScout;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/printer/epson/EpsonPrinterModule_Companion_ProvideEpsonUsbPrinterScoutFactory;->get()Lcom/squareup/print/EpsonUsbPrinterScout;

    move-result-object v0

    return-object v0
.end method
