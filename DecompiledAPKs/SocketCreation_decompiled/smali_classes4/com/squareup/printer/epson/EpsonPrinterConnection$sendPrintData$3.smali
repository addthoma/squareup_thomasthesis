.class final Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "EpsonPrinterConnection.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/printer/epson/EpsonPrinterConnection;->sendPrintData(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/print/PrintTimingData;Lcom/squareup/util/Clock;JLkotlin/jvm/functions/Function1;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u008a@\u00a2\u0006\u0004\u0008\u0003\u0010\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lkotlinx/coroutines/CoroutineScope;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.printer.epson.EpsonPrinterConnection$sendPrintData$3"
    f = "EpsonPrinterConnection.kt"
    i = {
        0x0,
        0x1
    }
    l = {
        0x9d,
        0x9f
    }
    m = "invokeSuspend"
    n = {
        "$this$launch",
        "$this$launch"
    }
    s = {
        "L$0",
        "L$0"
    }
.end annotation


# instance fields
.field final synthetic $delayedDisconnectDelayMs:J

.field final synthetic $epsonPrinterInfo:Lcom/squareup/printer/epson/EpsonPrinterInfo;

.field final synthetic $epsonPrinterSdk:Lcom/squareup/printer/epson/EpsonPrinterSdk;

.field L$0:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/CoroutineScope;

.field final synthetic this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;


# direct methods
.method constructor <init>(Lcom/squareup/printer/epson/EpsonPrinterConnection;JLcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/printer/epson/EpsonPrinterSdk;Lkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    iput-wide p2, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->$delayedDisconnectDelayMs:J

    iput-object p4, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->$epsonPrinterInfo:Lcom/squareup/printer/epson/EpsonPrinterInfo;

    iput-object p5, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->$epsonPrinterSdk:Lcom/squareup/printer/epson/EpsonPrinterSdk;

    const/4 p1, 0x2

    invoke-direct {p0, p1, p6}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;

    iget-object v2, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    iget-wide v3, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->$delayedDisconnectDelayMs:J

    iget-object v5, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->$epsonPrinterInfo:Lcom/squareup/printer/epson/EpsonPrinterInfo;

    iget-object v6, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->$epsonPrinterSdk:Lcom/squareup/printer/epson/EpsonPrinterSdk;

    move-object v1, v0

    move-object v7, p2

    invoke-direct/range {v1 .. v7}, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;-><init>(Lcom/squareup/printer/epson/EpsonPrinterConnection;JLcom/squareup/printer/epson/EpsonPrinterInfo;Lcom/squareup/printer/epson/EpsonPrinterSdk;Lkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    iput-object p1, v0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->p$:Lkotlinx/coroutines/CoroutineScope;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 155
    iget v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->label:I

    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-eqz v1, :cond_2

    if-eq v1, v4, :cond_1

    if-ne v1, v3, :cond_0

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->L$0:Ljava/lang/Object;

    check-cast v0, Lkotlinx/coroutines/CoroutineScope;

    :try_start_0
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 164
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 155
    :cond_1
    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->L$0:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/CoroutineScope;

    :try_start_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/util/concurrent/CancellationException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->p$:Lkotlinx/coroutines/CoroutineScope;

    .line 157
    :try_start_2
    iget-wide v5, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->$delayedDisconnectDelayMs:J

    iput-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->L$0:Ljava/lang/Object;

    iput v4, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->label:I

    invoke-static {v5, v6, p0}, Lkotlinx/coroutines/DelayKt;->delay(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_3

    return-object v0

    .line 158
    :cond_3
    :goto_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Disconnecting epson printer "

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->$epsonPrinterInfo:Lcom/squareup/printer/epson/EpsonPrinterInfo;

    invoke-virtual {v4}, Lcom/squareup/printer/epson/EpsonPrinterInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {p1, v4}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    iget-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    iget-object v4, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->$epsonPrinterSdk:Lcom/squareup/printer/epson/EpsonPrinterSdk;

    iput-object v1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->L$0:Ljava/lang/Object;

    iput v3, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->label:I

    invoke-virtual {p1, v4, p0}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->disconnectEpsonPrinter(Lcom/squareup/printer/epson/EpsonPrinterSdk;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v0, :cond_4

    return-object v0

    .line 160
    :cond_4
    :goto_1
    iget-object p1, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->this$0:Lcom/squareup/printer/epson/EpsonPrinterConnection;

    const/4 v0, 0x0

    check-cast v0, Lkotlinx/coroutines/Job;

    invoke-static {p1, v0}, Lcom/squareup/printer/epson/EpsonPrinterConnection;->access$setDelayedDisconnectJob$p(Lcom/squareup/printer/epson/EpsonPrinterConnection;Lkotlinx/coroutines/Job;)V
    :try_end_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 162
    :catch_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Canceling pending disconnect "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/printer/epson/EpsonPrinterConnection$sendPrintData$3;->$epsonPrinterInfo:Lcom/squareup/printer/epson/EpsonPrinterInfo;

    invoke-virtual {v0}, Lcom/squareup/printer/epson/EpsonPrinterInfo;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 164
    :goto_2
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method
