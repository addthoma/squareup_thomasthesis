.class final Lcom/squareup/protos/common/languages/Language$ProtoAdapter_Language;
.super Lcom/squareup/wire/EnumAdapter;
.source "Language.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/languages/Language;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Language"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/protos/common/languages/Language;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 1145
    const-class v0, Lcom/squareup/protos/common/languages/Language;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/protos/common/languages/Language;
    .locals 0

    .line 1150
    invoke-static {p1}, Lcom/squareup/protos/common/languages/Language;->fromValue(I)Lcom/squareup/protos/common/languages/Language;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 1143
    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/languages/Language$ProtoAdapter_Language;->fromValue(I)Lcom/squareup/protos/common/languages/Language;

    move-result-object p1

    return-object p1
.end method
