.class public final Lcom/squareup/protos/common/dinero/Money$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Money.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/dinero/Money;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/dinero/Money;",
        "Lcom/squareup/protos/common/dinero/Money$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public OBSOLETE_currency_code:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

.field public cents:Ljava/lang/Long;

.field public currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 128
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public OBSOLETE_currency_code(Lcom/squareup/api/items/OBSOLETE_CurrencyCode;)Lcom/squareup/protos/common/dinero/Money$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/common/dinero/Money$Builder;->OBSOLETE_currency_code:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/common/dinero/Money;
    .locals 5

    .line 156
    new-instance v0, Lcom/squareup/protos/common/dinero/Money;

    iget-object v1, p0, Lcom/squareup/protos/common/dinero/Money$Builder;->cents:Ljava/lang/Long;

    iget-object v2, p0, Lcom/squareup/protos/common/dinero/Money$Builder;->currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;

    iget-object v3, p0, Lcom/squareup/protos/common/dinero/Money$Builder;->OBSOLETE_currency_code:Lcom/squareup/api/items/OBSOLETE_CurrencyCode;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/common/dinero/Money;-><init>(Ljava/lang/Long;Lcom/squareup/protos/common/dinero/CurrencyCode;Lcom/squareup/api/items/OBSOLETE_CurrencyCode;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lcom/squareup/protos/common/dinero/Money$Builder;->build()Lcom/squareup/protos/common/dinero/Money;

    move-result-object v0

    return-object v0
.end method

.method public cents(Ljava/lang/Long;)Lcom/squareup/protos/common/dinero/Money$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/common/dinero/Money$Builder;->cents:Ljava/lang/Long;

    return-object p0
.end method

.method public currency_code(Lcom/squareup/protos/common/dinero/CurrencyCode;)Lcom/squareup/protos/common/dinero/Money$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/common/dinero/Money$Builder;->currency_code:Lcom/squareup/protos/common/dinero/CurrencyCode;

    return-object p0
.end method
