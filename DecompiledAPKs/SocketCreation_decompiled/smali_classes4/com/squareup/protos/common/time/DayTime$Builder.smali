.class public final Lcom/squareup/protos/common/time/DayTime$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DayTime.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/time/DayTime;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/time/DayTime;",
        "Lcom/squareup/protos/common/time/DayTime$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public day_of_week:Ljava/lang/Integer;

.field public time_at:Lcom/squareup/protos/common/time/LocalTime;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/common/time/DayTime;
    .locals 4

    .line 117
    new-instance v0, Lcom/squareup/protos/common/time/DayTime;

    iget-object v1, p0, Lcom/squareup/protos/common/time/DayTime$Builder;->day_of_week:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/common/time/DayTime$Builder;->time_at:Lcom/squareup/protos/common/time/LocalTime;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/common/time/DayTime;-><init>(Ljava/lang/Integer;Lcom/squareup/protos/common/time/LocalTime;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/common/time/DayTime$Builder;->build()Lcom/squareup/protos/common/time/DayTime;

    move-result-object v0

    return-object v0
.end method

.method public day_of_week(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/DayTime$Builder;
    .locals 0

    .line 106
    iput-object p1, p0, Lcom/squareup/protos/common/time/DayTime$Builder;->day_of_week:Ljava/lang/Integer;

    return-object p0
.end method

.method public time_at(Lcom/squareup/protos/common/time/LocalTime;)Lcom/squareup/protos/common/time/DayTime$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/common/time/DayTime$Builder;->time_at:Lcom/squareup/protos/common/time/LocalTime;

    return-object p0
.end method
