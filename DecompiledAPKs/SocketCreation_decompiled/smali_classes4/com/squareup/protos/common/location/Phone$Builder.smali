.class public final Lcom/squareup/protos/common/location/Phone$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Phone.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/location/Phone;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/location/Phone;",
        "Lcom/squareup/protos/common/location/Phone$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public calling_code:Ljava/lang/String;

.field public number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 105
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/common/location/Phone;
    .locals 4

    .line 130
    new-instance v0, Lcom/squareup/protos/common/location/Phone;

    iget-object v1, p0, Lcom/squareup/protos/common/location/Phone$Builder;->calling_code:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/common/location/Phone$Builder;->number:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/common/location/Phone;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/protos/common/location/Phone$Builder;->build()Lcom/squareup/protos/common/location/Phone;

    move-result-object v0

    return-object v0
.end method

.method public calling_code(Ljava/lang/String;)Lcom/squareup/protos/common/location/Phone$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/common/location/Phone$Builder;->calling_code:Ljava/lang/String;

    return-object p0
.end method

.method public number(Ljava/lang/String;)Lcom/squareup/protos/common/location/Phone$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/common/location/Phone$Builder;->number:Ljava/lang/String;

    return-object p0
.end method
