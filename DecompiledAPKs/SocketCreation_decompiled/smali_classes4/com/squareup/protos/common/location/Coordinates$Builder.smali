.class public final Lcom/squareup/protos/common/location/Coordinates$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Coordinates.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/common/location/Coordinates;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/common/location/Coordinates;",
        "Lcom/squareup/protos/common/location/Coordinates$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public altitude:Ljava/lang/Double;

.field public altitudinal_accuracy:Ljava/lang/Double;

.field public geographic_accuracy:Ljava/lang/Double;

.field public heading:Ljava/lang/Double;

.field public latitude:Ljava/lang/Double;

.field public longitude:Ljava/lang/Double;

.field public speed:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 193
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public altitude(Ljava/lang/Double;)Lcom/squareup/protos/common/location/Coordinates$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/protos/common/location/Coordinates$Builder;->altitude:Ljava/lang/Double;

    return-object p0
.end method

.method public altitudinal_accuracy(Ljava/lang/Double;)Lcom/squareup/protos/common/location/Coordinates$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/common/location/Coordinates$Builder;->altitudinal_accuracy:Ljava/lang/Double;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/common/location/Coordinates;
    .locals 10

    .line 254
    new-instance v9, Lcom/squareup/protos/common/location/Coordinates;

    iget-object v1, p0, Lcom/squareup/protos/common/location/Coordinates$Builder;->latitude:Ljava/lang/Double;

    iget-object v2, p0, Lcom/squareup/protos/common/location/Coordinates$Builder;->longitude:Ljava/lang/Double;

    iget-object v3, p0, Lcom/squareup/protos/common/location/Coordinates$Builder;->altitude:Ljava/lang/Double;

    iget-object v4, p0, Lcom/squareup/protos/common/location/Coordinates$Builder;->geographic_accuracy:Ljava/lang/Double;

    iget-object v5, p0, Lcom/squareup/protos/common/location/Coordinates$Builder;->altitudinal_accuracy:Ljava/lang/Double;

    iget-object v6, p0, Lcom/squareup/protos/common/location/Coordinates$Builder;->heading:Ljava/lang/Double;

    iget-object v7, p0, Lcom/squareup/protos/common/location/Coordinates$Builder;->speed:Ljava/lang/Double;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/common/location/Coordinates;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Double;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 178
    invoke-virtual {p0}, Lcom/squareup/protos/common/location/Coordinates$Builder;->build()Lcom/squareup/protos/common/location/Coordinates;

    move-result-object v0

    return-object v0
.end method

.method public geographic_accuracy(Ljava/lang/Double;)Lcom/squareup/protos/common/location/Coordinates$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/protos/common/location/Coordinates$Builder;->geographic_accuracy:Ljava/lang/Double;

    return-object p0
.end method

.method public heading(Ljava/lang/Double;)Lcom/squareup/protos/common/location/Coordinates$Builder;
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/squareup/protos/common/location/Coordinates$Builder;->heading:Ljava/lang/Double;

    return-object p0
.end method

.method public latitude(Ljava/lang/Double;)Lcom/squareup/protos/common/location/Coordinates$Builder;
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/common/location/Coordinates$Builder;->latitude:Ljava/lang/Double;

    return-object p0
.end method

.method public longitude(Ljava/lang/Double;)Lcom/squareup/protos/common/location/Coordinates$Builder;
    .locals 0

    .line 205
    iput-object p1, p0, Lcom/squareup/protos/common/location/Coordinates$Builder;->longitude:Ljava/lang/Double;

    return-object p0
.end method

.method public speed(Ljava/lang/Double;)Lcom/squareup/protos/common/location/Coordinates$Builder;
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/squareup/protos/common/location/Coordinates$Builder;->speed:Ljava/lang/Double;

    return-object p0
.end method
