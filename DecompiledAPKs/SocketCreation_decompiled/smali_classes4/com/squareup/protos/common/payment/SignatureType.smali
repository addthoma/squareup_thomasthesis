.class public final enum Lcom/squareup/protos/common/payment/SignatureType;
.super Ljava/lang/Enum;
.source "SignatureType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/common/payment/SignatureType$ProtoAdapter_SignatureType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/common/payment/SignatureType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/common/payment/SignatureType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/common/payment/SignatureType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DIGITAL_SIGNATURE:Lcom/squareup/protos/common/payment/SignatureType;

.field public static final enum NO_SIGNATURE:Lcom/squareup/protos/common/payment/SignatureType;

.field public static final enum PAPER_SIGNATURE:Lcom/squareup/protos/common/payment/SignatureType;

.field public static final enum UNKNOWN:Lcom/squareup/protos/common/payment/SignatureType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 17
    new-instance v0, Lcom/squareup/protos/common/payment/SignatureType;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/common/payment/SignatureType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/payment/SignatureType;->UNKNOWN:Lcom/squareup/protos/common/payment/SignatureType;

    .line 22
    new-instance v0, Lcom/squareup/protos/common/payment/SignatureType;

    const/4 v2, 0x1

    const-string v3, "NO_SIGNATURE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/common/payment/SignatureType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/payment/SignatureType;->NO_SIGNATURE:Lcom/squareup/protos/common/payment/SignatureType;

    .line 27
    new-instance v0, Lcom/squareup/protos/common/payment/SignatureType;

    const/4 v3, 0x2

    const-string v4, "DIGITAL_SIGNATURE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/common/payment/SignatureType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/payment/SignatureType;->DIGITAL_SIGNATURE:Lcom/squareup/protos/common/payment/SignatureType;

    .line 32
    new-instance v0, Lcom/squareup/protos/common/payment/SignatureType;

    const/4 v4, 0x3

    const-string v5, "PAPER_SIGNATURE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/common/payment/SignatureType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/common/payment/SignatureType;->PAPER_SIGNATURE:Lcom/squareup/protos/common/payment/SignatureType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/common/payment/SignatureType;

    .line 13
    sget-object v5, Lcom/squareup/protos/common/payment/SignatureType;->UNKNOWN:Lcom/squareup/protos/common/payment/SignatureType;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/common/payment/SignatureType;->NO_SIGNATURE:Lcom/squareup/protos/common/payment/SignatureType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/common/payment/SignatureType;->DIGITAL_SIGNATURE:Lcom/squareup/protos/common/payment/SignatureType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/common/payment/SignatureType;->PAPER_SIGNATURE:Lcom/squareup/protos/common/payment/SignatureType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/common/payment/SignatureType;->$VALUES:[Lcom/squareup/protos/common/payment/SignatureType;

    .line 34
    new-instance v0, Lcom/squareup/protos/common/payment/SignatureType$ProtoAdapter_SignatureType;

    invoke-direct {v0}, Lcom/squareup/protos/common/payment/SignatureType$ProtoAdapter_SignatureType;-><init>()V

    sput-object v0, Lcom/squareup/protos/common/payment/SignatureType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput p3, p0, Lcom/squareup/protos/common/payment/SignatureType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/common/payment/SignatureType;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 50
    :cond_0
    sget-object p0, Lcom/squareup/protos/common/payment/SignatureType;->PAPER_SIGNATURE:Lcom/squareup/protos/common/payment/SignatureType;

    return-object p0

    .line 49
    :cond_1
    sget-object p0, Lcom/squareup/protos/common/payment/SignatureType;->DIGITAL_SIGNATURE:Lcom/squareup/protos/common/payment/SignatureType;

    return-object p0

    .line 48
    :cond_2
    sget-object p0, Lcom/squareup/protos/common/payment/SignatureType;->NO_SIGNATURE:Lcom/squareup/protos/common/payment/SignatureType;

    return-object p0

    .line 47
    :cond_3
    sget-object p0, Lcom/squareup/protos/common/payment/SignatureType;->UNKNOWN:Lcom/squareup/protos/common/payment/SignatureType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/common/payment/SignatureType;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/common/payment/SignatureType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/payment/SignatureType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/common/payment/SignatureType;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/common/payment/SignatureType;->$VALUES:[Lcom/squareup/protos/common/payment/SignatureType;

    invoke-virtual {v0}, [Lcom/squareup/protos/common/payment/SignatureType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/common/payment/SignatureType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 57
    iget v0, p0, Lcom/squareup/protos/common/payment/SignatureType;->value:I

    return v0
.end method
