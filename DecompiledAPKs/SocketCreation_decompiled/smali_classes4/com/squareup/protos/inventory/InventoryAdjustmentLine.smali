.class public final Lcom/squareup/protos/inventory/InventoryAdjustmentLine;
.super Lcom/squareup/wire/Message;
.source "InventoryAdjustmentLine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/inventory/InventoryAdjustmentLine$ProtoAdapter_InventoryAdjustmentLine;,
        Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/inventory/InventoryAdjustmentLine;",
        "Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/inventory/InventoryAdjustmentLine;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_QUANTITY_DELTA:Ljava/lang/Long;

.field public static final DEFAULT_QUANTITY_STATE:Lcom/squareup/protos/inventory/InventoryQuantityState;

.field private static final serialVersionUID:J


# instance fields
.field public final cost:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final item_variation_id:Lcom/squareup/api/sync/ObjectId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.api.sync.ObjectId#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final quantity_delta:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field

.field public final quantity_state:Lcom/squareup/protos/inventory/InventoryQuantityState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.inventory.InventoryQuantityState#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 23
    new-instance v0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$ProtoAdapter_InventoryAdjustmentLine;

    invoke-direct {v0}, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$ProtoAdapter_InventoryAdjustmentLine;-><init>()V

    sput-object v0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 27
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->DEFAULT_QUANTITY_DELTA:Ljava/lang/Long;

    .line 29
    sget-object v0, Lcom/squareup/protos/inventory/InventoryQuantityState;->ON_HAND:Lcom/squareup/protos/inventory/InventoryQuantityState;

    sput-object v0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->DEFAULT_QUANTITY_STATE:Lcom/squareup/protos/inventory/InventoryQuantityState;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/sync/ObjectId;Ljava/lang/Long;Lcom/squareup/protos/inventory/InventoryQuantityState;Lcom/squareup/protos/common/Money;)V
    .locals 6

    .line 77
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;-><init>(Lcom/squareup/api/sync/ObjectId;Ljava/lang/Long;Lcom/squareup/protos/inventory/InventoryQuantityState;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/api/sync/ObjectId;Ljava/lang/Long;Lcom/squareup/protos/inventory/InventoryQuantityState;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 82
    sget-object v0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 83
    iput-object p1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->item_variation_id:Lcom/squareup/api/sync/ObjectId;

    .line 84
    iput-object p2, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->quantity_delta:Ljava/lang/Long;

    .line 85
    iput-object p3, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->quantity_state:Lcom/squareup/protos/inventory/InventoryQuantityState;

    .line 86
    iput-object p4, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->cost:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 103
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 104
    :cond_1
    check-cast p1, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;

    .line 105
    invoke-virtual {p0}, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->item_variation_id:Lcom/squareup/api/sync/ObjectId;

    iget-object v3, p1, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->item_variation_id:Lcom/squareup/api/sync/ObjectId;

    .line 106
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->quantity_delta:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->quantity_delta:Ljava/lang/Long;

    .line 107
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->quantity_state:Lcom/squareup/protos/inventory/InventoryQuantityState;

    iget-object v3, p1, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->quantity_state:Lcom/squareup/protos/inventory/InventoryQuantityState;

    .line 108
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->cost:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->cost:Lcom/squareup/protos/common/Money;

    .line 109
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 114
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 116
    invoke-virtual {p0}, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->item_variation_id:Lcom/squareup/api/sync/ObjectId;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/api/sync/ObjectId;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->quantity_delta:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->quantity_state:Lcom/squareup/protos/inventory/InventoryQuantityState;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/inventory/InventoryQuantityState;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->cost:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 121
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;
    .locals 2

    .line 91
    new-instance v0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;-><init>()V

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->item_variation_id:Lcom/squareup/api/sync/ObjectId;

    iput-object v1, v0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;->item_variation_id:Lcom/squareup/api/sync/ObjectId;

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->quantity_delta:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;->quantity_delta:Ljava/lang/Long;

    .line 94
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->quantity_state:Lcom/squareup/protos/inventory/InventoryQuantityState;

    iput-object v1, v0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;->quantity_state:Lcom/squareup/protos/inventory/InventoryQuantityState;

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->cost:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;->cost:Lcom/squareup/protos/common/Money;

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->newBuilder()Lcom/squareup/protos/inventory/InventoryAdjustmentLine$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->item_variation_id:Lcom/squareup/api/sync/ObjectId;

    if-eqz v1, :cond_0

    const-string v1, ", item_variation_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->item_variation_id:Lcom/squareup/api/sync/ObjectId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->quantity_delta:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", quantity_delta="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->quantity_delta:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 131
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->quantity_state:Lcom/squareup/protos/inventory/InventoryQuantityState;

    if-eqz v1, :cond_2

    const-string v1, ", quantity_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->quantity_state:Lcom/squareup/protos/inventory/InventoryQuantityState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 132
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->cost:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", cost="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/inventory/InventoryAdjustmentLine;->cost:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InventoryAdjustmentLine{"

    .line 133
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
