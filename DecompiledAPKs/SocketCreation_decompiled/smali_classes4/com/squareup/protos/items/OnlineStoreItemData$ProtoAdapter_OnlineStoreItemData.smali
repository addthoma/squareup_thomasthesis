.class final Lcom/squareup/protos/items/OnlineStoreItemData$ProtoAdapter_OnlineStoreItemData;
.super Lcom/squareup/wire/ProtoAdapter;
.source "OnlineStoreItemData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/items/OnlineStoreItemData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_OnlineStoreItemData"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/items/OnlineStoreItemData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 260
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/items/OnlineStoreItemData;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/items/OnlineStoreItemData;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 287
    new-instance v0, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;-><init>()V

    .line 288
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 289
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 305
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 303
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->opted_out_for_auto_sharing(Ljava/lang/Boolean;)Lcom/squareup/protos/items/OnlineStoreItemData$Builder;

    goto :goto_0

    .line 302
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/items/OnlineStorePickupData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/items/OnlineStorePickupData;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->pickup_data(Lcom/squareup/protos/items/OnlineStorePickupData;)Lcom/squareup/protos/items/OnlineStoreItemData$Builder;

    goto :goto_0

    .line 301
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/items/OnlineStoreDonationData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/items/OnlineStoreDonationData;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->donation_data(Lcom/squareup/protos/items/OnlineStoreDonationData;)Lcom/squareup/protos/items/OnlineStoreItemData$Builder;

    goto :goto_0

    .line 300
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/items/OnlineStoreEventData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/items/OnlineStoreEventData;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->event_data(Lcom/squareup/protos/items/OnlineStoreEventData;)Lcom/squareup/protos/items/OnlineStoreItemData$Builder;

    goto :goto_0

    .line 294
    :pswitch_4
    :try_start_0
    sget-object v4, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->type(Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;)Lcom/squareup/protos/items/OnlineStoreItemData$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 296
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 291
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->version(Ljava/lang/Integer;)Lcom/squareup/protos/items/OnlineStoreItemData$Builder;

    goto :goto_0

    .line 309
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 310
    invoke-virtual {v0}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->build()Lcom/squareup/protos/items/OnlineStoreItemData;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 258
    invoke-virtual {p0, p1}, Lcom/squareup/protos/items/OnlineStoreItemData$ProtoAdapter_OnlineStoreItemData;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/items/OnlineStoreItemData;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/items/OnlineStoreItemData;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 276
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/items/OnlineStoreItemData;->version:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 277
    sget-object v0, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/items/OnlineStoreItemData;->type:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 278
    sget-object v0, Lcom/squareup/protos/items/OnlineStoreEventData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/items/OnlineStoreItemData;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 279
    sget-object v0, Lcom/squareup/protos/items/OnlineStoreDonationData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/items/OnlineStoreItemData;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 280
    sget-object v0, Lcom/squareup/protos/items/OnlineStorePickupData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/items/OnlineStoreItemData;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 281
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/items/OnlineStoreItemData;->opted_out_for_auto_sharing:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 282
    invoke-virtual {p2}, Lcom/squareup/protos/items/OnlineStoreItemData;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 258
    check-cast p2, Lcom/squareup/protos/items/OnlineStoreItemData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/items/OnlineStoreItemData$ProtoAdapter_OnlineStoreItemData;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/items/OnlineStoreItemData;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/items/OnlineStoreItemData;)I
    .locals 4

    .line 265
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/items/OnlineStoreItemData;->version:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/items/OnlineStoreItemData;->type:Lcom/squareup/protos/items/OnlineStoreItemData$OnlineStoreItemType;

    const/4 v3, 0x2

    .line 266
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/items/OnlineStoreEventData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/items/OnlineStoreItemData;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    const/4 v3, 0x3

    .line 267
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/items/OnlineStoreDonationData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/items/OnlineStoreItemData;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    const/4 v3, 0x4

    .line 268
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/items/OnlineStorePickupData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/items/OnlineStoreItemData;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    const/4 v3, 0x5

    .line 269
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/items/OnlineStoreItemData;->opted_out_for_auto_sharing:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 270
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 271
    invoke-virtual {p1}, Lcom/squareup/protos/items/OnlineStoreItemData;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 258
    check-cast p1, Lcom/squareup/protos/items/OnlineStoreItemData;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/items/OnlineStoreItemData$ProtoAdapter_OnlineStoreItemData;->encodedSize(Lcom/squareup/protos/items/OnlineStoreItemData;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/items/OnlineStoreItemData;)Lcom/squareup/protos/items/OnlineStoreItemData;
    .locals 2

    .line 315
    invoke-virtual {p1}, Lcom/squareup/protos/items/OnlineStoreItemData;->newBuilder()Lcom/squareup/protos/items/OnlineStoreItemData$Builder;

    move-result-object p1

    .line 316
    iget-object v0, p1, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/items/OnlineStoreEventData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/items/OnlineStoreEventData;

    iput-object v0, p1, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->event_data:Lcom/squareup/protos/items/OnlineStoreEventData;

    .line 317
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/items/OnlineStoreDonationData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/items/OnlineStoreDonationData;

    iput-object v0, p1, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->donation_data:Lcom/squareup/protos/items/OnlineStoreDonationData;

    .line 318
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/items/OnlineStorePickupData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/items/OnlineStorePickupData;

    iput-object v0, p1, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->pickup_data:Lcom/squareup/protos/items/OnlineStorePickupData;

    .line 319
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 320
    invoke-virtual {p1}, Lcom/squareup/protos/items/OnlineStoreItemData$Builder;->build()Lcom/squareup/protos/items/OnlineStoreItemData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 258
    check-cast p1, Lcom/squareup/protos/items/OnlineStoreItemData;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/items/OnlineStoreItemData$ProtoAdapter_OnlineStoreItemData;->redact(Lcom/squareup/protos/items/OnlineStoreItemData;)Lcom/squareup/protos/items/OnlineStoreItemData;

    move-result-object p1

    return-object p1
.end method
