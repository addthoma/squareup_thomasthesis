.class public final Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;
.super Lcom/squareup/wire/Message;
.source "ListDeviceCodesRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$ProtoAdapter_ListDeviceCodesRequest;,
        Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;",
        "Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CURSOR:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_PRODUCT_TYPE:Lcom/squareup/protos/devicesettings/ProductType;

.field private static final serialVersionUID:J


# instance fields
.field public final cursor:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final location_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final product_type:Lcom/squareup/protos/devicesettings/ProductType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.devicesettings.ProductType#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 21
    new-instance v0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$ProtoAdapter_ListDeviceCodesRequest;

    invoke-direct {v0}, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$ProtoAdapter_ListDeviceCodesRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 29
    sget-object v0, Lcom/squareup/protos/devicesettings/ProductType;->UNKNOWN:Lcom/squareup/protos/devicesettings/ProductType;

    sput-object v0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->DEFAULT_PRODUCT_TYPE:Lcom/squareup/protos/devicesettings/ProductType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/devicesettings/ProductType;)V
    .locals 1

    .line 70
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/devicesettings/ProductType;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/devicesettings/ProductType;Lokio/ByteString;)V
    .locals 1

    .line 75
    sget-object v0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 76
    iput-object p1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->cursor:Ljava/lang/String;

    .line 77
    iput-object p2, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->location_id:Ljava/lang/String;

    .line 78
    iput-object p3, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 94
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 95
    :cond_1
    check-cast p1, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->cursor:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->cursor:Ljava/lang/String;

    .line 97
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->location_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->location_id:Ljava/lang/String;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    iget-object p1, p1, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    .line 99
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 104
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->cursor:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/devicesettings/ProductType;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 110
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;
    .locals 2

    .line 83
    new-instance v0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;-><init>()V

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->cursor:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;->cursor:Ljava/lang/String;

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->location_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;->location_id:Ljava/lang/String;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    iput-object v1, v0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->newBuilder()Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->cursor:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", cursor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->cursor:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", location_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->location_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    if-eqz v1, :cond_2

    const-string v1, ", product_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/devicesettings/external/ListDeviceCodesRequest;->product_type:Lcom/squareup/protos/devicesettings/ProductType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListDeviceCodesRequest{"

    .line 121
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
