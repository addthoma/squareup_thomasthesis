.class public final enum Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;
.super Ljava/lang/Enum;
.source "UnifiedActivity.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/UnifiedActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ActivityState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState$ProtoAdapter_ActivityState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DECLINED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

.field public static final enum DISPUTED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

.field public static final enum DISPUTE_REVERSED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

.field public static final enum DISPUTE_WON:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

.field public static final enum IN_REVERSAL:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

.field public static final enum IN_REVIEW:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

.field public static final enum PENDING:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

.field public static final enum REFUNDED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

.field public static final enum REVERSED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

.field public static final enum SETTLED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

.field public static final enum VOIDED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

.field public static final enum WAITING_ON_EXTERNAL:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 501
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "PENDING"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->PENDING:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 506
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const/4 v3, 0x2

    const-string v4, "SETTLED"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->SETTLED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 512
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const/4 v4, 0x3

    const-string v5, "VOIDED"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->VOIDED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 517
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const/4 v5, 0x4

    const-string v6, "DECLINED"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DECLINED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 522
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const/4 v6, 0x5

    const-string v7, "REFUNDED"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->REFUNDED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 527
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const/4 v7, 0x6

    const-string v8, "REVERSED"

    invoke-direct {v0, v8, v6, v7}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->REVERSED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 532
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const/4 v8, 0x7

    const-string v9, "DISPUTED"

    invoke-direct {v0, v9, v7, v8}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DISPUTED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 539
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const/16 v9, 0x8

    const-string v10, "DISPUTE_REVERSED"

    invoke-direct {v0, v10, v8, v9}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DISPUTE_REVERSED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 544
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const/16 v10, 0x9

    const-string v11, "DISPUTE_WON"

    invoke-direct {v0, v11, v9, v10}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DISPUTE_WON:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 550
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const/16 v11, 0xa

    const-string v12, "IN_REVIEW"

    invoke-direct {v0, v12, v10, v11}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->IN_REVIEW:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 556
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const/16 v12, 0xb

    const-string v13, "IN_REVERSAL"

    invoke-direct {v0, v13, v11, v12}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->IN_REVERSAL:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 563
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    const/16 v13, 0xc

    const-string v14, "WAITING_ON_EXTERNAL"

    invoke-direct {v0, v14, v12, v13}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->WAITING_ON_EXTERNAL:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    new-array v0, v13, [Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 496
    sget-object v13, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->PENDING:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->SETTLED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->VOIDED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DECLINED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->REFUNDED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->REVERSED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DISPUTED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DISPUTE_REVERSED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DISPUTE_WON:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->IN_REVIEW:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->IN_REVERSAL:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->WAITING_ON_EXTERNAL:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    aput-object v1, v0, v12

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->$VALUES:[Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    .line 565
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState$ProtoAdapter_ActivityState;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState$ProtoAdapter_ActivityState;-><init>()V

    sput-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 569
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 570
    iput p3, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 589
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->WAITING_ON_EXTERNAL:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object p0

    .line 588
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->IN_REVERSAL:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object p0

    .line 587
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->IN_REVIEW:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object p0

    .line 586
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DISPUTE_WON:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object p0

    .line 585
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DISPUTE_REVERSED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object p0

    .line 584
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DISPUTED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object p0

    .line 583
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->REVERSED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object p0

    .line 582
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->REFUNDED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object p0

    .line 581
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->DECLINED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object p0

    .line 580
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->VOIDED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object p0

    .line 579
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->SETTLED:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object p0

    .line 578
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->PENDING:Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;
    .locals 1

    .line 496
    const-class v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;
    .locals 1

    .line 496
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->$VALUES:[Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    invoke-virtual {v0}, [Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 596
    iget v0, p0, Lcom/squareup/protos/bizbank/UnifiedActivity$ActivityState;->value:I

    return v0
.end method
