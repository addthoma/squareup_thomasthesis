.class public final Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UnifiedActivityDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public modifier_amount:Lcom/squareup/protos/common/Money;

.field public modifier_amount_description:Ljava/lang/String;

.field public modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 306
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;
    .locals 5

    .line 335
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    iget-object v1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    iget-object v2, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_amount_description:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;-><init>(Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;Lcom/squareup/protos/common/Money;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 299
    invoke-virtual {p0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->build()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    move-result-object v0

    return-object v0
.end method

.method public modifier_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;
    .locals 0

    .line 321
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public modifier_amount_description(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;
    .locals 0

    .line 329
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_amount_description:Ljava/lang/String;

    return-object p0
.end method

.method public modifier_type(Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;
    .locals 0

    .line 313
    iput-object p1, p0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    return-object p0
.end method
