.class final Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ProtoAdapter_Modifier;
.super Lcom/squareup/wire/ProtoAdapter;
.source "UnifiedActivityDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Modifier"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 391
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 412
    new-instance v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;-><init>()V

    .line 413
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 414
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 427
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 425
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_amount_description(Ljava/lang/String;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;

    goto :goto_0

    .line 424
    :cond_1
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;

    goto :goto_0

    .line 418
    :cond_2
    :try_start_0
    sget-object v4, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_type(Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 420
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 431
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 432
    invoke-virtual {v0}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->build()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 389
    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ProtoAdapter_Modifier;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 404
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 405
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount:Lcom/squareup/protos/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 406
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount_description:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 407
    invoke-virtual {p2}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 389
    check-cast p2, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ProtoAdapter_Modifier;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;)I
    .locals 4

    .line 396
    sget-object v0, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_type:Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ModifierType;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount:Lcom/squareup/protos/common/Money;

    const/4 v3, 0x2

    .line 397
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->modifier_amount_description:Ljava/lang/String;

    const/4 v3, 0x3

    .line 398
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 399
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 389
    check-cast p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ProtoAdapter_Modifier;->encodedSize(Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;
    .locals 2

    .line 437
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;->newBuilder()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;

    move-result-object p1

    .line 438
    iget-object v0, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->modifier_amount:Lcom/squareup/protos/common/Money;

    .line 439
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 440
    invoke-virtual {p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$Builder;->build()Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 389
    check-cast p1, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier$ProtoAdapter_Modifier;->redact(Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;)Lcom/squareup/protos/bizbank/UnifiedActivityDetails$Modifier;

    move-result-object p1

    return-object p1
.end method
