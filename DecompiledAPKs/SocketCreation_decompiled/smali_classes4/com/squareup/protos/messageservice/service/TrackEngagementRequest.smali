.class public final Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;
.super Lcom/squareup/wire/Message;
.source "TrackEngagementRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$ProtoAdapter_TrackEngagementRequest;,
        Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;",
        "Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACTION:Lcom/squareup/protos/messageservice/service/Action;

.field public static final DEFAULT_CLICK_URL:Ljava/lang/String; = ""

.field public static final DEFAULT_MESSAGE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_REQUEST_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final action:Lcom/squareup/protos/messageservice/service/Action;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.messageservice.service.Action#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final click_url:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final entity:Lcom/squareup/protos/messageservice/service/Entity;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.messageservice.service.Entity#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final message_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final request_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$ProtoAdapter_TrackEngagementRequest;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$ProtoAdapter_TrackEngagementRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 24
    sget-object v0, Lcom/squareup/protos/messageservice/service/Action;->DO_NOT_USE_ACTION:Lcom/squareup/protos/messageservice/service/Action;

    sput-object v0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->DEFAULT_ACTION:Lcom/squareup/protos/messageservice/service/Action;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/messageservice/service/Action;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/Entity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 79
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;-><init>(Lcom/squareup/protos/messageservice/service/Action;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/Entity;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/messageservice/service/Action;Ljava/lang/String;Lcom/squareup/protos/messageservice/service/Entity;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 84
    sget-object v0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 85
    invoke-static {p4, p5}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p6

    const/4 v0, 0x1

    if-gt p6, v0, :cond_0

    .line 88
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->action:Lcom/squareup/protos/messageservice/service/Action;

    .line 89
    iput-object p2, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->click_url:Ljava/lang/String;

    .line 90
    iput-object p3, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    .line 91
    iput-object p4, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->request_token:Ljava/lang/String;

    .line 92
    iput-object p5, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->message_id:Ljava/lang/String;

    return-void

    .line 86
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of request_token, message_id may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 110
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 111
    :cond_1
    check-cast p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;

    .line 112
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->action:Lcom/squareup/protos/messageservice/service/Action;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->action:Lcom/squareup/protos/messageservice/service/Action;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->click_url:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->click_url:Ljava/lang/String;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->request_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->request_token:Ljava/lang/String;

    .line 116
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->message_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->message_id:Ljava/lang/String;

    .line 117
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 122
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->action:Lcom/squareup/protos/messageservice/service/Action;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/Action;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->click_url:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/messageservice/service/Entity;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->request_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->message_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 130
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;
    .locals 2

    .line 97
    new-instance v0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->action:Lcom/squareup/protos/messageservice/service/Action;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->action:Lcom/squareup/protos/messageservice/service/Action;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->click_url:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->click_url:Ljava/lang/String;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->request_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->request_token:Ljava/lang/String;

    .line 102
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->message_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->message_id:Ljava/lang/String;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->newBuilder()Lcom/squareup/protos/messageservice/service/TrackEngagementRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 138
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->action:Lcom/squareup/protos/messageservice/service/Action;

    if-eqz v1, :cond_0

    const-string v1, ", action="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->action:Lcom/squareup/protos/messageservice/service/Action;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 139
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->click_url:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", click_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->click_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    if-eqz v1, :cond_2

    const-string v1, ", entity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->entity:Lcom/squareup/protos/messageservice/service/Entity;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->request_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", request_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->request_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->message_id:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", message_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/TrackEngagementRequest;->message_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TrackEngagementRequest{"

    .line 143
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
