.class public final Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetMessageUnitsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest;",
        "Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public limit:Ljava/lang/Integer;

.field public locale:Lcom/squareup/protos/messageservice/service/Locale;

.field public offset:Ljava/lang/Integer;

.field public placement_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 139
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest;
    .locals 7

    .line 177
    new-instance v6, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest;

    iget-object v1, p0, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;->placement_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;->limit:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;->locale:Lcom/squareup/protos/messageservice/service/Locale;

    iget-object v4, p0, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;->offset:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest;-><init>(Ljava/lang/String;Ljava/lang/Integer;Lcom/squareup/protos/messageservice/service/Locale;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;->build()Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest;

    move-result-object v0

    return-object v0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public locale(Lcom/squareup/protos/messageservice/service/Locale;)Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;->locale:Lcom/squareup/protos/messageservice/service/Locale;

    return-object p0
.end method

.method public offset(Ljava/lang/Integer;)Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;->offset:Ljava/lang/Integer;

    return-object p0
.end method

.method public placement_id(Ljava/lang/String;)Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/messageservice/service/GetMessageUnitsRequest$Builder;->placement_id:Ljava/lang/String;

    return-object p0
.end method
