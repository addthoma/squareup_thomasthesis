.class public final Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpgradeSessionTwoFactorResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;",
        "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public error_message:Ljava/lang/String;

.field public error_title:Ljava/lang/String;

.field public session_token:Ljava/lang/String;

.field public trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 139
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;
    .locals 7

    .line 176
    new-instance v6, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;

    iget-object v1, p0, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;->session_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    iget-object v3, p0, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;->error_title:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;->error_message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;-><init>(Ljava/lang/String;Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;->build()Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;

    move-result-object v0

    return-object v0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public error_title(Ljava/lang/String;)Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;->error_title:Ljava/lang/String;

    return-object p0
.end method

.method public session_token(Ljava/lang/String;)Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;->session_token:Ljava/lang/String;

    return-object p0
.end method

.method public trusted_device_details(Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;)Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse$Builder;->trusted_device_details:Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;

    return-object p0
.end method
