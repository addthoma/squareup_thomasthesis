.class public final Lcom/squareup/protos/register/api/LoginRequest;
.super Lcom/squareup/wire/Message;
.source "LoginRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/register/api/LoginRequest$ProtoAdapter_LoginRequest;,
        Lcom/squareup/protos/register/api/LoginRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/register/api/LoginRequest;",
        "Lcom/squareup/protos/register/api/LoginRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/register/api/LoginRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CAPTCHA_RESPONSE:Ljava/lang/String; = ""

.field public static final DEFAULT_DEVICECHECK_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_DEVICE_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_EMAIL:Ljava/lang/String; = ""

.field public static final DEFAULT_IDEMPOTENCE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_PASSWORD:Ljava/lang/String; = ""

.field public static final DEFAULT_PHONE:Ljava/lang/String; = ""

.field public static final DEFAULT_VERIFICATION_CODE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final captcha_response:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final device_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x5
    .end annotation
.end field

.field public final devicecheck_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final email:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final idempotence_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final password:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final phone:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final trusted_device_details:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.common.TrustedDeviceDetails#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;"
        }
    .end annotation
.end field

.field public final verification_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/protos/register/api/LoginRequest$ProtoAdapter_LoginRequest;

    invoke-direct {v0}, Lcom/squareup/protos/register/api/LoginRequest$ProtoAdapter_LoginRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/register/api/LoginRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 145
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/register/api/LoginRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TrustedDeviceDetails;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 152
    sget-object v0, Lcom/squareup/protos/register/api/LoginRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 153
    invoke-static {p7, p8, p9}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p10

    const/4 v0, 0x1

    if-gt p10, v0, :cond_0

    .line 156
    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest;->password:Ljava/lang/String;

    .line 157
    iput-object p2, p0, Lcom/squareup/protos/register/api/LoginRequest;->verification_code:Ljava/lang/String;

    const-string p1, "trusted_device_details"

    .line 158
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/register/api/LoginRequest;->trusted_device_details:Ljava/util/List;

    .line 159
    iput-object p4, p0, Lcom/squareup/protos/register/api/LoginRequest;->captcha_response:Ljava/lang/String;

    .line 160
    iput-object p5, p0, Lcom/squareup/protos/register/api/LoginRequest;->devicecheck_token:Ljava/lang/String;

    .line 161
    iput-object p6, p0, Lcom/squareup/protos/register/api/LoginRequest;->idempotence_token:Ljava/lang/String;

    .line 162
    iput-object p7, p0, Lcom/squareup/protos/register/api/LoginRequest;->email:Ljava/lang/String;

    .line 163
    iput-object p8, p0, Lcom/squareup/protos/register/api/LoginRequest;->phone:Ljava/lang/String;

    .line 164
    iput-object p9, p0, Lcom/squareup/protos/register/api/LoginRequest;->device_code:Ljava/lang/String;

    return-void

    .line 154
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of email, phone, device_code may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 186
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/register/api/LoginRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 187
    :cond_1
    check-cast p1, Lcom/squareup/protos/register/api/LoginRequest;

    .line 188
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/LoginRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/register/api/LoginRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->password:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginRequest;->password:Ljava/lang/String;

    .line 189
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->verification_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginRequest;->verification_code:Ljava/lang/String;

    .line 190
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->trusted_device_details:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginRequest;->trusted_device_details:Ljava/util/List;

    .line 191
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->captcha_response:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginRequest;->captcha_response:Ljava/lang/String;

    .line 192
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->devicecheck_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginRequest;->devicecheck_token:Ljava/lang/String;

    .line 193
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->idempotence_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginRequest;->idempotence_token:Ljava/lang/String;

    .line 194
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->email:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginRequest;->email:Ljava/lang/String;

    .line 195
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->phone:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/register/api/LoginRequest;->phone:Ljava/lang/String;

    .line 196
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->device_code:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/register/api/LoginRequest;->device_code:Ljava/lang/String;

    .line 197
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 202
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 204
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/LoginRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->password:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->verification_code:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 207
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->trusted_device_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 208
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->captcha_response:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 209
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->devicecheck_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 210
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->idempotence_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 211
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->email:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->phone:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->device_code:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 214
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/register/api/LoginRequest$Builder;
    .locals 2

    .line 169
    new-instance v0, Lcom/squareup/protos/register/api/LoginRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/register/api/LoginRequest$Builder;-><init>()V

    .line 170
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->password:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->password:Ljava/lang/String;

    .line 171
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->verification_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->verification_code:Ljava/lang/String;

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->trusted_device_details:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->trusted_device_details:Ljava/util/List;

    .line 173
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->captcha_response:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->captcha_response:Ljava/lang/String;

    .line 174
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->devicecheck_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->devicecheck_token:Ljava/lang/String;

    .line 175
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->idempotence_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->idempotence_token:Ljava/lang/String;

    .line 176
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->email:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->email:Ljava/lang/String;

    .line 177
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->phone:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->phone:Ljava/lang/String;

    .line 178
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->device_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/register/api/LoginRequest$Builder;->device_code:Ljava/lang/String;

    .line 179
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/LoginRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/register/api/LoginRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/protos/register/api/LoginRequest;->newBuilder()Lcom/squareup/protos/register/api/LoginRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->password:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", password=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->verification_code:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", verification_code=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->trusted_device_details:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", trusted_device_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->trusted_device_details:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 225
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->captcha_response:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", captcha_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->captcha_response:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->devicecheck_token:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", devicecheck_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->devicecheck_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->idempotence_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", idempotence_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->idempotence_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->email:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", email=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->phone:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", phone=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/register/api/LoginRequest;->device_code:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", device_code=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LoginRequest{"

    .line 231
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
