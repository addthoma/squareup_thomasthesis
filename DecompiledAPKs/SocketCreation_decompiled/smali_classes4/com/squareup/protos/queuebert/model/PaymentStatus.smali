.class public final enum Lcom/squareup/protos/queuebert/model/PaymentStatus;
.super Ljava/lang/Enum;
.source "PaymentStatus.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/queuebert/model/PaymentStatus$ProtoAdapter_PaymentStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/queuebert/model/PaymentStatus;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/queuebert/model/PaymentStatus;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/queuebert/model/PaymentStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum AUTHORIZE_FAILURE:Lcom/squareup/protos/queuebert/model/PaymentStatus;

.field public static final enum AUTHORIZE_PENDING:Lcom/squareup/protos/queuebert/model/PaymentStatus;

.field public static final enum AUTHORIZE_SUCCESS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

.field public static final enum CAPTURE_FAILURE:Lcom/squareup/protos/queuebert/model/PaymentStatus;

.field public static final enum CAPTURE_SUCCESS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

.field public static final enum UNKNOWN_PAYMENT_STATUS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

.field public static final enum UNPROCESSED:Lcom/squareup/protos/queuebert/model/PaymentStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 14
    new-instance v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_PAYMENT_STATUS"

    const/4 v3, -0x1

    invoke-direct {v0, v2, v1, v3}, Lcom/squareup/protos/queuebert/model/PaymentStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->UNKNOWN_PAYMENT_STATUS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    .line 16
    new-instance v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;

    const/4 v2, 0x1

    const-string v3, "UNPROCESSED"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/protos/queuebert/model/PaymentStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->UNPROCESSED:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    .line 18
    new-instance v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;

    const/4 v3, 0x2

    const-string v4, "AUTHORIZE_PENDING"

    invoke-direct {v0, v4, v3, v2}, Lcom/squareup/protos/queuebert/model/PaymentStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->AUTHORIZE_PENDING:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    .line 20
    new-instance v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;

    const/4 v4, 0x3

    const-string v5, "AUTHORIZE_SUCCESS"

    invoke-direct {v0, v5, v4, v3}, Lcom/squareup/protos/queuebert/model/PaymentStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->AUTHORIZE_SUCCESS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    .line 22
    new-instance v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;

    const/4 v5, 0x4

    const-string v6, "CAPTURE_SUCCESS"

    invoke-direct {v0, v6, v5, v4}, Lcom/squareup/protos/queuebert/model/PaymentStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->CAPTURE_SUCCESS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    .line 24
    new-instance v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;

    const/4 v6, 0x5

    const-string v7, "AUTHORIZE_FAILURE"

    invoke-direct {v0, v7, v6, v5}, Lcom/squareup/protos/queuebert/model/PaymentStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->AUTHORIZE_FAILURE:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    .line 26
    new-instance v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;

    const/4 v7, 0x6

    const-string v8, "CAPTURE_FAILURE"

    invoke-direct {v0, v8, v7, v6}, Lcom/squareup/protos/queuebert/model/PaymentStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->CAPTURE_FAILURE:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/protos/queuebert/model/PaymentStatus;

    .line 10
    sget-object v8, Lcom/squareup/protos/queuebert/model/PaymentStatus;->UNKNOWN_PAYMENT_STATUS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/protos/queuebert/model/PaymentStatus;->UNPROCESSED:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/queuebert/model/PaymentStatus;->AUTHORIZE_PENDING:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/queuebert/model/PaymentStatus;->AUTHORIZE_SUCCESS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/queuebert/model/PaymentStatus;->CAPTURE_SUCCESS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/queuebert/model/PaymentStatus;->AUTHORIZE_FAILURE:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/queuebert/model/PaymentStatus;->CAPTURE_FAILURE:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->$VALUES:[Lcom/squareup/protos/queuebert/model/PaymentStatus;

    .line 28
    new-instance v0, Lcom/squareup/protos/queuebert/model/PaymentStatus$ProtoAdapter_PaymentStatus;

    invoke-direct {v0}, Lcom/squareup/protos/queuebert/model/PaymentStatus$ProtoAdapter_PaymentStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput p3, p0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/queuebert/model/PaymentStatus;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 47
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->CAPTURE_FAILURE:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-object p0

    .line 46
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->AUTHORIZE_FAILURE:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-object p0

    .line 45
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->CAPTURE_SUCCESS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-object p0

    .line 44
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->AUTHORIZE_SUCCESS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-object p0

    .line 43
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->AUTHORIZE_PENDING:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-object p0

    .line 42
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->UNPROCESSED:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-object p0

    .line 41
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->UNKNOWN_PAYMENT_STATUS:Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-object p0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/queuebert/model/PaymentStatus;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/queuebert/model/PaymentStatus;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->$VALUES:[Lcom/squareup/protos/queuebert/model/PaymentStatus;

    invoke-virtual {v0}, [Lcom/squareup/protos/queuebert/model/PaymentStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/queuebert/model/PaymentStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 54
    iget v0, p0, Lcom/squareup/protos/queuebert/model/PaymentStatus;->value:I

    return v0
.end method
