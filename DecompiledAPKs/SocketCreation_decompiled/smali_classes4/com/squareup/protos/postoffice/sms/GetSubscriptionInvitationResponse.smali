.class public final Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;
.super Lcom/squareup/wire/Message;
.source "GetSubscriptionInvitationResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$ProtoAdapter_GetSubscriptionInvitationResponse;,
        Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;",
        "Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final invitation:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.postoffice.sms.SubscriptionInvitation#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$ProtoAdapter_GetSubscriptionInvitationResponse;

    invoke-direct {v0}, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$ProtoAdapter_GetSubscriptionInvitationResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;)V
    .locals 1

    .line 31
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;-><init>(Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;Lokio/ByteString;)V
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 37
    iput-object p1, p0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;->invitation:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 51
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 52
    :cond_1
    check-cast p1, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;

    .line 53
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;->invitation:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    iget-object p1, p1, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;->invitation:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    .line 54
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 59
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 61
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 62
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;->invitation:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 63
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$Builder;
    .locals 2

    .line 42
    new-instance v0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$Builder;-><init>()V

    .line 43
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;->invitation:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    iput-object v1, v0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$Builder;->invitation:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    .line 44
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;->newBuilder()Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;->invitation:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    if-eqz v1, :cond_0

    const-string v1, ", invitation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/postoffice/sms/GetSubscriptionInvitationResponse;->invitation:Lcom/squareup/protos/postoffice/sms/SubscriptionInvitation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "GetSubscriptionInvitationResponse{"

    .line 72
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
