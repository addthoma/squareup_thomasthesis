.class public final Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpdateNotificationSettingsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse;",
        "Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public status:Lcom/squareup/protos/invoice/v2/common/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 78
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse;
    .locals 3

    .line 88
    new-instance v0, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse;

    iget-object v1, p0, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse$Builder;->status:Lcom/squareup/protos/invoice/v2/common/Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse;-><init>(Lcom/squareup/protos/invoice/v2/common/Status;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse$Builder;->build()Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/invoice/v2/common/Status;)Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse$Builder;
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse$Builder;->status:Lcom/squareup/protos/invoice/v2/common/Status;

    return-object p0
.end method
