.class public final Lcom/squareup/protos/jedi/service/PanelRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PanelRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/jedi/service/PanelRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/jedi/service/PanelRequest;",
        "Lcom/squareup/protos/jedi/service/PanelRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public anonymous_token:Ljava/lang/String;

.field public inputs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/jedi/service/Input;",
            ">;"
        }
    .end annotation
.end field

.field public panel_token:Ljava/lang/String;

.field public session_token:Ljava/lang/String;

.field public transition_id:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 141
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 142
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->inputs:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public anonymous_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelRequest$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->anonymous_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/jedi/service/PanelRequest;
    .locals 8

    .line 173
    new-instance v7, Lcom/squareup/protos/jedi/service/PanelRequest;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->session_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->panel_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->inputs:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->anonymous_token:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->transition_id:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/jedi/service/PanelRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->build()Lcom/squareup/protos/jedi/service/PanelRequest;

    move-result-object v0

    return-object v0
.end method

.method public inputs(Ljava/util/List;)Lcom/squareup/protos/jedi/service/PanelRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/jedi/service/Input;",
            ">;)",
            "Lcom/squareup/protos/jedi/service/PanelRequest$Builder;"
        }
    .end annotation

    .line 156
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 157
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->inputs:Ljava/util/List;

    return-object p0
.end method

.method public panel_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelRequest$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->panel_token:Ljava/lang/String;

    return-object p0
.end method

.method public session_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelRequest$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->session_token:Ljava/lang/String;

    return-object p0
.end method

.method public transition_id(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/PanelRequest$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/PanelRequest$Builder;->transition_id:Ljava/lang/String;

    return-object p0
.end method
