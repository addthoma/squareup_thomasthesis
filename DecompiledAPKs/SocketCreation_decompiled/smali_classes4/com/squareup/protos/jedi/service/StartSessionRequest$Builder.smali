.class public final Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StartSessionRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/jedi/service/StartSessionRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/jedi/service/StartSessionRequest;",
        "Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public anonymous_token:Ljava/lang/String;

.field public client:Ljava/lang/String;

.field public component_library_version:Ljava/lang/String;

.field public country_code:Lcom/squareup/protos/common/countries/Country;

.field public device_type:Lcom/squareup/protos/jedi/service/DeviceType;

.field public employment_token:Ljava/lang/String;

.field public language_code:Lcom/squareup/protos/common/languages/Language;

.field public merchant_token:Ljava/lang/String;

.field public person_token:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 221
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public anonymous_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->anonymous_token:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/jedi/service/StartSessionRequest;
    .locals 13

    .line 276
    new-instance v12, Lcom/squareup/protos/jedi/service/StartSessionRequest;

    iget-object v1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->employment_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->anonymous_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->country_code:Lcom/squareup/protos/common/countries/Country;

    iget-object v5, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->language_code:Lcom/squareup/protos/common/languages/Language;

    iget-object v6, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->merchant_token:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->person_token:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->client:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->component_library_version:Ljava/lang/String;

    iget-object v10, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->device_type:Lcom/squareup/protos/jedi/service/DeviceType;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/jedi/service/StartSessionRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/countries/Country;Lcom/squareup/protos/common/languages/Language;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/jedi/service/DeviceType;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 200
    invoke-virtual {p0}, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->build()Lcom/squareup/protos/jedi/service/StartSessionRequest;

    move-result-object v0

    return-object v0
.end method

.method public client(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;
    .locals 0

    .line 260
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->client:Ljava/lang/String;

    return-object p0
.end method

.method public component_library_version(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->component_library_version:Ljava/lang/String;

    return-object p0
.end method

.method public country_code(Lcom/squareup/protos/common/countries/Country;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->country_code:Lcom/squareup/protos/common/countries/Country;

    return-object p0
.end method

.method public device_type(Lcom/squareup/protos/jedi/service/DeviceType;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;
    .locals 0

    .line 270
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->device_type:Lcom/squareup/protos/jedi/service/DeviceType;

    return-object p0
.end method

.method public employment_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->employment_token:Ljava/lang/String;

    return-object p0
.end method

.method public language_code(Lcom/squareup/protos/common/languages/Language;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->language_code:Lcom/squareup/protos/common/languages/Language;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public person_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->person_token:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/squareup/protos/jedi/service/StartSessionRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
