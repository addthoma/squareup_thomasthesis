.class public final Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MessageWithDescriptors.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;",
        "Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public message_data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lokio/ByteString;",
            ">;"
        }
    .end annotation
.end field

.field public scope:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

.field public type_name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 124
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 125
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->message_data:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;
    .locals 5

    .line 157
    new-instance v0, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->type_name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->message_data:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->scope:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;-><init>(Ljava/lang/String;Ljava/util/List;Lcom/squareup/protos/privacyvault/model/PrivacyScope;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->build()Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    move-result-object v0

    return-object v0
.end method

.method public message_data(Ljava/util/List;)Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lokio/ByteString;",
            ">;)",
            "Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;"
        }
    .end annotation

    .line 140
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->message_data:Ljava/util/List;

    return-object p0
.end method

.method public scope(Lcom/squareup/protos/privacyvault/model/PrivacyScope;)Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->scope:Lcom/squareup/protos/privacyvault/model/PrivacyScope;

    return-object p0
.end method

.method public type_name(Ljava/lang/String;)Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors$Builder;->type_name:Ljava/lang/String;

    return-object p0
.end method
