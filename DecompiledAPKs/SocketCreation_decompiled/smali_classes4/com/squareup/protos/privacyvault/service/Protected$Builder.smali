.class public final Lcom/squareup/protos/privacyvault/service/Protected$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Protected.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/privacyvault/service/Protected;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/privacyvault/service/Protected;",
        "Lcom/squareup/protos/privacyvault/service/Protected$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public binary_data:Lokio/ByteString;

.field public fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

.field public protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

.field public string_data:Ljava/lang/String;

.field public trunk_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 161
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public binary_data(Lokio/ByteString;)Lcom/squareup/protos/privacyvault/service/Protected$Builder;
    .locals 0

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->binary_data:Lokio/ByteString;

    const/4 p1, 0x0

    .line 181
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->string_data:Ljava/lang/String;

    .line 182
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->trunk_token:Ljava/lang/String;

    .line 184
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/privacyvault/service/Protected;
    .locals 8

    .line 228
    new-instance v7, Lcom/squareup/protos/privacyvault/service/Protected;

    iget-object v1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->string_data:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->binary_data:Lokio/ByteString;

    iget-object v3, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    iget-object v4, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->trunk_token:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/privacyvault/service/Protected;-><init>(Ljava/lang/String;Lokio/ByteString;Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;Ljava/lang/String;Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 150
    invoke-virtual {p0}, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->build()Lcom/squareup/protos/privacyvault/service/Protected;

    move-result-object v0

    return-object v0
.end method

.method public fidelius_token(Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;)Lcom/squareup/protos/privacyvault/service/Protected$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    const/4 p1, 0x0

    .line 194
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->string_data:Ljava/lang/String;

    .line 195
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->binary_data:Lokio/ByteString;

    .line 196
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->trunk_token:Ljava/lang/String;

    .line 197
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    return-object p0
.end method

.method public protected_message(Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;)Lcom/squareup/protos/privacyvault/service/Protected$Builder;
    .locals 0

    .line 218
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    const/4 p1, 0x0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->string_data:Ljava/lang/String;

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->binary_data:Lokio/ByteString;

    .line 221
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    .line 222
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->trunk_token:Ljava/lang/String;

    return-object p0
.end method

.method public string_data(Ljava/lang/String;)Lcom/squareup/protos/privacyvault/service/Protected$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->string_data:Ljava/lang/String;

    const/4 p1, 0x0

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->binary_data:Lokio/ByteString;

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->trunk_token:Ljava/lang/String;

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    return-object p0
.end method

.method public trunk_token(Ljava/lang/String;)Lcom/squareup/protos/privacyvault/service/Protected$Builder;
    .locals 0

    .line 206
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->trunk_token:Ljava/lang/String;

    const/4 p1, 0x0

    .line 207
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->string_data:Ljava/lang/String;

    .line 208
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->binary_data:Lokio/ByteString;

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->fidelius_token:Lcom/squareup/protos/privacyvault/service/Protected$ProtectedFideliusTokenWithCategory;

    .line 210
    iput-object p1, p0, Lcom/squareup/protos/privacyvault/service/Protected$Builder;->protected_message:Lcom/squareup/protos/privacyvault/service/MessageWithDescriptors;

    return-object p0
.end method
