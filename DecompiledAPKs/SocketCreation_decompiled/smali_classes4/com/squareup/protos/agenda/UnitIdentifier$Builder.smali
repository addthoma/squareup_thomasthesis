.class public final Lcom/squareup/protos/agenda/UnitIdentifier$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UnitIdentifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/UnitIdentifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/UnitIdentifier;",
        "Lcom/squareup/protos/agenda/UnitIdentifier$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public merchant_token:Ljava/lang/String;

.field public person_token:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 116
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/agenda/UnitIdentifier;
    .locals 5

    .line 136
    new-instance v0, Lcom/squareup/protos/agenda/UnitIdentifier;

    iget-object v1, p0, Lcom/squareup/protos/agenda/UnitIdentifier$Builder;->person_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/agenda/UnitIdentifier$Builder;->merchant_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/agenda/UnitIdentifier$Builder;->unit_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/agenda/UnitIdentifier;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/UnitIdentifier$Builder;->build()Lcom/squareup/protos/agenda/UnitIdentifier;

    move-result-object v0

    return-object v0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/agenda/UnitIdentifier$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/agenda/UnitIdentifier$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public person_token(Ljava/lang/String;)Lcom/squareup/protos/agenda/UnitIdentifier$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/agenda/UnitIdentifier$Builder;->person_token:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/agenda/UnitIdentifier$Builder;
    .locals 0

    .line 130
    iput-object p1, p0, Lcom/squareup/protos/agenda/UnitIdentifier$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
