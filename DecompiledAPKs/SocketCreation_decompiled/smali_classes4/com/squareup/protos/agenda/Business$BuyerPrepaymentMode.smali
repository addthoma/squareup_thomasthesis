.class public final enum Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;
.super Ljava/lang/Enum;
.source "Business.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/Business;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BuyerPrepaymentMode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode$ProtoAdapter_BuyerPrepaymentMode;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALL_CLIENTS_REQUIRED:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

.field public static final enum OFF:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 764
    new-instance v0, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    const/4 v1, 0x0

    const-string v2, "OFF"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;->OFF:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    .line 769
    new-instance v0, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    const/4 v2, 0x1

    const-string v3, "ALL_CLIENTS_REQUIRED"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;->ALL_CLIENTS_REQUIRED:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    .line 760
    sget-object v3, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;->OFF:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;->ALL_CLIENTS_REQUIRED:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;->$VALUES:[Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    .line 771
    new-instance v0, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode$ProtoAdapter_BuyerPrepaymentMode;

    invoke-direct {v0}, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode$ProtoAdapter_BuyerPrepaymentMode;-><init>()V

    sput-object v0, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 775
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 776
    iput p3, p0, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;
    .locals 1

    if-eqz p0, :cond_1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 785
    :cond_0
    sget-object p0, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;->ALL_CLIENTS_REQUIRED:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    return-object p0

    .line 784
    :cond_1
    sget-object p0, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;->OFF:Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;
    .locals 1

    .line 760
    const-class v0, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;
    .locals 1

    .line 760
    sget-object v0, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;->$VALUES:[Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    invoke-virtual {v0}, [Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 792
    iget v0, p0, Lcom/squareup/protos/agenda/Business$BuyerPrepaymentMode;->value:I

    return v0
.end method
