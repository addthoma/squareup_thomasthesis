.class public final Lcom/squareup/protos/agenda/RequestContext$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RequestContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/RequestContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/RequestContext;",
        "Lcom/squareup/protos/agenda/RequestContext$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public business_id:Ljava/lang/String;

.field public merchant_token:Ljava/lang/String;

.field public person_token:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 133
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/agenda/RequestContext;
    .locals 7

    .line 165
    new-instance v6, Lcom/squareup/protos/agenda/RequestContext;

    iget-object v1, p0, Lcom/squareup/protos/agenda/RequestContext$Builder;->business_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/agenda/RequestContext$Builder;->unit_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/agenda/RequestContext$Builder;->merchant_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/agenda/RequestContext$Builder;->person_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/agenda/RequestContext;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/RequestContext$Builder;->build()Lcom/squareup/protos/agenda/RequestContext;

    move-result-object v0

    return-object v0
.end method

.method public business_id(Ljava/lang/String;)Lcom/squareup/protos/agenda/RequestContext$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/agenda/RequestContext$Builder;->business_id:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/agenda/RequestContext$Builder;
    .locals 0

    .line 151
    iput-object p1, p0, Lcom/squareup/protos/agenda/RequestContext$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public person_token(Ljava/lang/String;)Lcom/squareup/protos/agenda/RequestContext$Builder;
    .locals 0

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/agenda/RequestContext$Builder;->person_token:Ljava/lang/String;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/agenda/RequestContext$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/agenda/RequestContext$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
