.class public final Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ClientInstallationIdentity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/agenda/ClientInstallationIdentity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/agenda/ClientInstallationIdentity;",
        "Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public device:Lcom/squareup/protos/common/client/Device;

.field public fully_qualified_application_identifier:Ljava/lang/String;

.field public product:Lcom/squareup/protos/common/client/Product;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 122
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/agenda/ClientInstallationIdentity;
    .locals 5

    .line 153
    new-instance v0, Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    iget-object v1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;->product:Lcom/squareup/protos/common/client/Product;

    iget-object v2, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;->device:Lcom/squareup/protos/common/client/Device;

    iget-object v3, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;->fully_qualified_application_identifier:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/agenda/ClientInstallationIdentity;-><init>(Lcom/squareup/protos/common/client/Product;Lcom/squareup/protos/common/client/Device;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 115
    invoke-virtual {p0}, Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;->build()Lcom/squareup/protos/agenda/ClientInstallationIdentity;

    move-result-object v0

    return-object v0
.end method

.method public device(Lcom/squareup/protos/common/client/Device;)Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;->device:Lcom/squareup/protos/common/client/Device;

    return-object p0
.end method

.method public fully_qualified_application_identifier(Ljava/lang/String;)Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;->fully_qualified_application_identifier:Ljava/lang/String;

    return-object p0
.end method

.method public product(Lcom/squareup/protos/common/client/Product;)Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/agenda/ClientInstallationIdentity$Builder;->product:Lcom/squareup/protos/common/client/Product;

    return-object p0
.end method
