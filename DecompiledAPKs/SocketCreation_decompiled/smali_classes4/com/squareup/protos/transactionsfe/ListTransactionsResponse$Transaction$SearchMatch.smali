.class public final Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;
.super Lcom/squareup/wire/Message;
.source "ListTransactionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SearchMatch"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$ProtoAdapter_SearchMatch;,
        Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MATCHING_VALUE:Ljava/lang/String; = ""

.field public static final DEFAULT_MATCHING_VALUE_LENGTH:Ljava/lang/Integer;

.field public static final DEFAULT_MATCHING_VALUE_START_INDEX:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final matching_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final matching_value_length:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x3
    .end annotation
.end field

.field public final matching_value_start_index:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 832
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$ProtoAdapter_SearchMatch;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$ProtoAdapter_SearchMatch;-><init>()V

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 838
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->DEFAULT_MATCHING_VALUE_START_INDEX:Ljava/lang/Integer;

    .line 840
    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->DEFAULT_MATCHING_VALUE_LENGTH:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 1

    .line 871
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 1

    .line 876
    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 877
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value:Ljava/lang/String;

    .line 878
    iput-object p2, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_start_index:Ljava/lang/Integer;

    .line 879
    iput-object p3, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_length:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 895
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 896
    :cond_1
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;

    .line 897
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value:Ljava/lang/String;

    .line 898
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_start_index:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_start_index:Ljava/lang/Integer;

    .line 899
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_length:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_length:Ljava/lang/Integer;

    .line 900
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 905
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 907
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 908
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 909
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_start_index:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 910
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_length:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 911
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;
    .locals 2

    .line 884
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;-><init>()V

    .line 885
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->matching_value:Ljava/lang/String;

    .line 886
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_start_index:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->matching_value_start_index:Ljava/lang/Integer;

    .line 887
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_length:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->matching_value_length:Ljava/lang/Integer;

    .line 888
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 831
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 918
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 919
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", matching_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 920
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_start_index:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, ", matching_value_start_index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_start_index:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 921
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_length:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    const-string v1, ", matching_value_length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;->matching_value_length:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SearchMatch{"

    .line 922
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
