.class final Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$ProtoAdapter_TenderInformation;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ListTransactionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_TenderInformation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 746
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 769
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;-><init>()V

    .line 770
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 771
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 806
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 799
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 801
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 791
    :cond_1
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->felica_brand(Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 793
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 783
    :cond_2
    :try_start_2
    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->card_brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v4

    .line 785
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 775
    :cond_3
    :try_start_3
    sget-object v4, Lcom/squareup/protos/client/bills/Tender$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/Tender$Type;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->tender_type(Lcom/squareup/protos/client/bills/Tender$Type;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;
    :try_end_3
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v4

    .line 777
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 810
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 811
    invoke-virtual {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 744
    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$ProtoAdapter_TenderInformation;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 760
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 761
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 762
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 763
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 764
    invoke-virtual {p2}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 744
    check-cast p2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$ProtoAdapter_TenderInformation;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;)I
    .locals 4

    .line 751
    sget-object v0, Lcom/squareup/protos/client/bills/Tender$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->tender_type:Lcom/squareup/protos/client/bills/Tender$Type;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->card_brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v3, 0x2

    .line 752
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->felica_brand:Lcom/squareup/protos/client/bills/CardTender$Card$FelicaBrand;

    const/4 v3, 0x3

    .line 753
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v3, 0x4

    .line 754
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 755
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 744
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$ProtoAdapter_TenderInformation;->encodedSize(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;
    .locals 0

    .line 816
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;->newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;

    move-result-object p1

    .line 817
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 818
    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$Builder;->build()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 744
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation$ProtoAdapter_TenderInformation;->redact(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;)Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;

    move-result-object p1

    return-object p1
.end method
