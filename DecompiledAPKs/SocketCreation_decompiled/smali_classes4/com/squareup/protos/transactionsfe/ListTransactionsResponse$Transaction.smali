.class public final Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;
.super Lcom/squareup/wire/Message;
.source "ListTransactionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Transaction"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$ProtoAdapter_Transaction;,
        Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;,
        Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;,
        Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;,
        Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DESCRIPTION:Ljava/lang/String; = ""

.field public static final DEFAULT_HAS_ERROR:Ljava/lang/Boolean;

.field public static final DEFAULT_HAS_RELATED_TRANSACTIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_IS_AWAITING_TIP:Ljava/lang/Boolean;

.field public static final DEFAULT_IS_FULLY_VOIDED:Ljava/lang/Boolean;

.field public static final DEFAULT_IS_TIP_EXPIRING:Ljava/lang/Boolean;

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final bill_id_pair:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final date:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final description:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final has_error:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final has_related_transactions:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final is_awaiting_tip:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final is_fully_voided:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final is_tip_expiring:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final search_match:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.transactionsfe.ListTransactionsResponse$Transaction$SearchMatch#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xc
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;",
            ">;"
        }
    .end annotation
.end field

.field public final tender_information:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.transactionsfe.ListTransactionsResponse$Transaction$TenderInformation#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;",
            ">;"
        }
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.transactionsfe.ListTransactionsResponse$Transaction$Type#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 140
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$ProtoAdapter_Transaction;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$ProtoAdapter_Transaction;-><init>()V

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 144
    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->DO_NOT_USE_TYPE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->DEFAULT_TYPE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    const/4 v0, 0x0

    .line 148
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->DEFAULT_HAS_RELATED_TRANSACTIONS:Ljava/lang/Boolean;

    .line 150
    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->DEFAULT_HAS_ERROR:Ljava/lang/Boolean;

    .line 152
    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->DEFAULT_IS_FULLY_VOIDED:Ljava/lang/Boolean;

    .line 154
    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->DEFAULT_IS_AWAITING_TIP:Ljava/lang/Boolean;

    .line 156
    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->DEFAULT_IS_TIP_EXPIRING:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;",
            ">;)V"
        }
    .end annotation

    .line 277
    sget-object v13, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;Lcom/squareup/protos/common/time/DateTime;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/IdPair;",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$TenderInformation;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$SearchMatch;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 285
    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p13}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 286
    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 287
    iput-object p2, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    .line 288
    iput-object p3, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->date:Lcom/squareup/protos/common/time/DateTime;

    .line 289
    iput-object p4, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->amount:Lcom/squareup/protos/common/Money;

    .line 290
    iput-object p5, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->description:Ljava/lang/String;

    const-string p1, "tender_information"

    .line 291
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->tender_information:Ljava/util/List;

    .line 292
    iput-object p7, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_related_transactions:Ljava/lang/Boolean;

    .line 293
    iput-object p8, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_error:Ljava/lang/Boolean;

    .line 294
    iput-object p9, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_fully_voided:Ljava/lang/Boolean;

    .line 295
    iput-object p10, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_awaiting_tip:Ljava/lang/Boolean;

    .line 296
    iput-object p11, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_tip_expiring:Ljava/lang/Boolean;

    const-string p1, "search_match"

    .line 297
    invoke-static {p1, p12}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->search_match:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 322
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 323
    :cond_1
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;

    .line 324
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 325
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    .line 326
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->date:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->date:Lcom/squareup/protos/common/time/DateTime;

    .line 327
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->amount:Lcom/squareup/protos/common/Money;

    .line 328
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->description:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->description:Ljava/lang/String;

    .line 329
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->tender_information:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->tender_information:Ljava/util/List;

    .line 330
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_related_transactions:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_related_transactions:Ljava/lang/Boolean;

    .line 331
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_error:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_error:Ljava/lang/Boolean;

    .line 332
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_fully_voided:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_fully_voided:Ljava/lang/Boolean;

    .line 333
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_awaiting_tip:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_awaiting_tip:Ljava/lang/Boolean;

    .line 334
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_tip_expiring:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_tip_expiring:Ljava/lang/Boolean;

    .line 335
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->search_match:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->search_match:Ljava/util/List;

    .line 336
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 341
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_a

    .line 343
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 344
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 345
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 346
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->date:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 347
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 348
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->description:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 349
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->tender_information:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 350
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_related_transactions:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 351
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_error:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 352
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_fully_voided:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 353
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_awaiting_tip:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 354
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_tip_expiring:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 355
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->search_match:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 356
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_a
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;
    .locals 2

    .line 302
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;-><init>()V

    .line 303
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    .line 304
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    .line 305
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->date:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->date:Lcom/squareup/protos/common/time/DateTime;

    .line 306
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->amount:Lcom/squareup/protos/common/Money;

    .line 307
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->description:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->description:Ljava/lang/String;

    .line 308
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->tender_information:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->tender_information:Ljava/util/List;

    .line 309
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_related_transactions:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->has_related_transactions:Ljava/lang/Boolean;

    .line 310
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_error:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->has_error:Ljava/lang/Boolean;

    .line 311
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_fully_voided:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->is_fully_voided:Ljava/lang/Boolean;

    .line 312
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_awaiting_tip:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->is_awaiting_tip:Ljava/lang/Boolean;

    .line 313
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_tip_expiring:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->is_tip_expiring:Ljava/lang/Boolean;

    .line 314
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->search_match:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->search_match:Ljava/util/List;

    .line 315
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 139
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 363
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 364
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_0

    const-string v1, ", bill_id_pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 365
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    if-eqz v1, :cond_1

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->type:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 366
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->date:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_2

    const-string v1, ", date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->date:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 367
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 368
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->description:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", description="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->tender_information:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", tender_information="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->tender_information:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 370
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_related_transactions:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", has_related_transactions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_related_transactions:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 371
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_error:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", has_error="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->has_error:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 372
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_fully_voided:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", is_fully_voided="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_fully_voided:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 373
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_awaiting_tip:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", is_awaiting_tip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_awaiting_tip:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 374
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_tip_expiring:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", is_tip_expiring="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->is_tip_expiring:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 375
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->search_match:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, ", search_match="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;->search_match:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_b
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Transaction{"

    .line 376
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
