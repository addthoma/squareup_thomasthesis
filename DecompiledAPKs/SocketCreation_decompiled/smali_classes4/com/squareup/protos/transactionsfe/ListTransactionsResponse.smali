.class public final Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;
.super Lcom/squareup/wire/Message;
.source "ListTransactionsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$ProtoAdapter_ListTransactionsResponse;,
        Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;,
        Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.transactionsfe.ListTransactionsRequest#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final transaction:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.transactionsfe.ListTransactionsResponse$Transaction#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$ProtoAdapter_ListTransactionsResponse;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$ProtoAdapter_ListTransactionsResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;",
            ">;",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;",
            ")V"
        }
    .end annotation

    .line 57
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction;",
            ">;",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 62
    sget-object v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p3, "transaction"

    .line 63
    invoke-static {p3, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->transaction:Ljava/util/List;

    .line 64
    iput-object p2, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 79
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 80
    :cond_1
    check-cast p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->transaction:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->transaction:Ljava/util/List;

    .line 82
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    iget-object p1, p1, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    .line 83
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 88
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->transaction:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 93
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;
    .locals 2

    .line 69
    new-instance v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;-><init>()V

    .line 70
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->transaction:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->transaction:Ljava/util/List;

    .line 71
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    iput-object v1, v0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    .line 72
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 34
    invoke-virtual {p0}, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->newBuilder()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->transaction:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", transaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->transaction:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    if-eqz v1, :cond_1

    const-string v1, ", next_page_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse;->next_page_request:Lcom/squareup/protos/transactionsfe/ListTransactionsRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ListTransactionsResponse{"

    .line 103
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
