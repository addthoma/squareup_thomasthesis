.class final Lcom/squareup/protos/precog/ProductInterestWeight$ProtoAdapter_ProductInterestWeight;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ProductInterestWeight.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/precog/ProductInterestWeight;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ProductInterestWeight"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/precog/ProductInterestWeight;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 102
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/precog/ProductInterestWeight;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/precog/ProductInterestWeight;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 119
    new-instance v0, Lcom/squareup/protos/precog/ProductInterestWeight$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/precog/ProductInterestWeight$Builder;-><init>()V

    .line 120
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 121
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 125
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 123
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/precog/ProductInterestWeight$Builder;->weight(Ljava/lang/Integer;)Lcom/squareup/protos/precog/ProductInterestWeight$Builder;

    goto :goto_0

    .line 129
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/precog/ProductInterestWeight$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 130
    invoke-virtual {v0}, Lcom/squareup/protos/precog/ProductInterestWeight$Builder;->build()Lcom/squareup/protos/precog/ProductInterestWeight;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 100
    invoke-virtual {p0, p1}, Lcom/squareup/protos/precog/ProductInterestWeight$ProtoAdapter_ProductInterestWeight;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/precog/ProductInterestWeight;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/precog/ProductInterestWeight;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 113
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/precog/ProductInterestWeight;->weight:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 114
    invoke-virtual {p2}, Lcom/squareup/protos/precog/ProductInterestWeight;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 100
    check-cast p2, Lcom/squareup/protos/precog/ProductInterestWeight;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/precog/ProductInterestWeight$ProtoAdapter_ProductInterestWeight;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/precog/ProductInterestWeight;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/precog/ProductInterestWeight;)I
    .locals 3

    .line 107
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/precog/ProductInterestWeight;->weight:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 108
    invoke-virtual {p1}, Lcom/squareup/protos/precog/ProductInterestWeight;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 100
    check-cast p1, Lcom/squareup/protos/precog/ProductInterestWeight;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/precog/ProductInterestWeight$ProtoAdapter_ProductInterestWeight;->encodedSize(Lcom/squareup/protos/precog/ProductInterestWeight;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/precog/ProductInterestWeight;)Lcom/squareup/protos/precog/ProductInterestWeight;
    .locals 0

    .line 135
    invoke-virtual {p1}, Lcom/squareup/protos/precog/ProductInterestWeight;->newBuilder()Lcom/squareup/protos/precog/ProductInterestWeight$Builder;

    move-result-object p1

    .line 136
    invoke-virtual {p1}, Lcom/squareup/protos/precog/ProductInterestWeight$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 137
    invoke-virtual {p1}, Lcom/squareup/protos/precog/ProductInterestWeight$Builder;->build()Lcom/squareup/protos/precog/ProductInterestWeight;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 100
    check-cast p1, Lcom/squareup/protos/precog/ProductInterestWeight;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/precog/ProductInterestWeight$ProtoAdapter_ProductInterestWeight;->redact(Lcom/squareup/protos/precog/ProductInterestWeight;)Lcom/squareup/protos/precog/ProductInterestWeight;

    move-result-object p1

    return-object p1
.end method
