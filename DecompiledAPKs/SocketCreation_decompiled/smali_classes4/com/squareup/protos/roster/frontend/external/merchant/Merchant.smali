.class public final Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;
.super Lcom/squareup/wire/Message;
.source "Merchant.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$ProtoAdapter_Merchant;,
        Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;,
        Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;",
        "Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BUSINESS_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_COUNTRY:Lcom/squareup/protos/connect/v2/resources/Country;

.field public static final DEFAULT_CURRENCY:Lcom/squareup/protos/connect/v2/common/Currency;

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LANGUAGE_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

.field private static final serialVersionUID:J


# instance fields
.field public final business_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final country:Lcom/squareup/protos/connect/v2/resources/Country;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Country#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final currency:Lcom/squareup/protos/connect/v2/common/Currency;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Currency#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final language_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.roster.frontend.external.merchant.Merchant$Status#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$ProtoAdapter_Merchant;

    invoke-direct {v0}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$ProtoAdapter_Merchant;-><init>()V

    sput-object v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 37
    sget-object v0, Lcom/squareup/protos/connect/v2/resources/Country;->ZZ:Lcom/squareup/protos/connect/v2/resources/Country;

    sput-object v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->DEFAULT_COUNTRY:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 41
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Currency;->UNKNOWN_CURRENCY:Lcom/squareup/protos/connect/v2/common/Currency;

    sput-object v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->DEFAULT_CURRENCY:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 43
    sget-object v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;->ACTIVE:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    sput-object v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->DEFAULT_STATUS:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Country;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Currency;Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;)V
    .locals 8

    .line 102
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Country;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Currency;Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/resources/Country;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Currency;Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;Lokio/ByteString;)V
    .locals 1

    .line 107
    sget-object v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->id:Ljava/lang/String;

    .line 109
    iput-object p2, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->business_name:Ljava/lang/String;

    .line 110
    iput-object p3, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 111
    iput-object p4, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->language_code:Ljava/lang/String;

    .line 112
    iput-object p5, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 113
    iput-object p6, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->status:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 132
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 133
    :cond_1
    check-cast p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;

    .line 134
    invoke-virtual {p0}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->id:Ljava/lang/String;

    .line 135
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->business_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->business_name:Ljava/lang/String;

    .line 136
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    iget-object v3, p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 137
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->language_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->language_code:Ljava/lang/String;

    .line 138
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    iget-object v3, p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 139
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->status:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    iget-object p1, p1, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->status:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    .line 140
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 145
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 147
    invoke-virtual {p0}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 149
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->business_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 150
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/resources/Country;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->language_code:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Currency;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 153
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->status:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 154
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;
    .locals 2

    .line 118
    new-instance v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;-><init>()V

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->id:Ljava/lang/String;

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->business_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->business_name:Ljava/lang/String;

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    iput-object v1, v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    .line 122
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->language_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->language_code:Ljava/lang/String;

    .line 123
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    iput-object v1, v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    .line 124
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->status:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    iput-object v1, v0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->status:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    .line 125
    invoke-virtual {p0}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->newBuilder()Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->business_name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", business_name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    if-eqz v1, :cond_2

    const-string v1, ", country="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->country:Lcom/squareup/protos/connect/v2/resources/Country;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 165
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->language_code:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", language_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->language_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    if-eqz v1, :cond_4

    const-string v1, ", currency="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->currency:Lcom/squareup/protos/connect/v2/common/Currency;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 167
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->status:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    if-eqz v1, :cond_5

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/roster/frontend/external/merchant/Merchant;->status:Lcom/squareup/protos/roster/frontend/external/merchant/Merchant$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Merchant{"

    .line 168
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
