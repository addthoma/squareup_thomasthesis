.class public final Lcom/squareup/protos/multipass/mobile/AlertButton;
.super Lcom/squareup/wire/Message;
.source "AlertButton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/multipass/mobile/AlertButton$ProtoAdapter_AlertButton;,
        Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/multipass/mobile/AlertButton;",
        "Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/multipass/mobile/AlertButton;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TITLE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.mobile.AlertButtonActionBeginFlow#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.mobile.AlertButtonActionDismiss#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.mobile.AlertButtonActionOpenUrl#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/multipass/mobile/AlertButton$ProtoAdapter_AlertButton;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/mobile/AlertButton$ProtoAdapter_AlertButton;-><init>()V

    sput-object v0, Lcom/squareup/protos/multipass/mobile/AlertButton;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;)V
    .locals 6

    .line 64
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/multipass/mobile/AlertButton;-><init>(Ljava/lang/String;Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;Lokio/ByteString;)V
    .locals 1

    .line 70
    sget-object v0, Lcom/squareup/protos/multipass/mobile/AlertButton;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 71
    invoke-static {p2, p3, p4}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p5

    const/4 v0, 0x1

    if-gt p5, v0, :cond_0

    .line 74
    iput-object p1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->title:Ljava/lang/String;

    .line 75
    iput-object p2, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    .line 76
    iput-object p3, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    .line 77
    iput-object p4, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    return-void

    .line 72
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of dismiss, open_url, begin_flow may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 94
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/multipass/mobile/AlertButton;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 95
    :cond_1
    check-cast p1, Lcom/squareup/protos/multipass/mobile/AlertButton;

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/AlertButton;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/multipass/mobile/AlertButton;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/multipass/mobile/AlertButton;->title:Ljava/lang/String;

    .line 97
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    iget-object v3, p1, Lcom/squareup/protos/multipass/mobile/AlertButton;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    .line 98
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    iget-object v3, p1, Lcom/squareup/protos/multipass/mobile/AlertButton;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    iget-object p1, p1, Lcom/squareup/protos/multipass/mobile/AlertButton;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    .line 100
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 105
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/AlertButton;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->title:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 112
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;
    .locals 2

    .line 82
    new-instance v0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;-><init>()V

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->title:Ljava/lang/String;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    iput-object v1, v0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    iput-object v1, v0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    .line 86
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    iput-object v1, v0, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/AlertButton;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/mobile/AlertButton;->newBuilder()Lcom/squareup/protos/multipass/mobile/AlertButton$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->title:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    if-eqz v1, :cond_1

    const-string v1, ", dismiss="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->dismiss:Lcom/squareup/protos/multipass/mobile/AlertButtonActionDismiss;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    if-eqz v1, :cond_2

    const-string v1, ", open_url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->open_url:Lcom/squareup/protos/multipass/mobile/AlertButtonActionOpenUrl;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 123
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    if-eqz v1, :cond_3

    const-string v1, ", begin_flow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/mobile/AlertButton;->begin_flow:Lcom/squareup/protos/multipass/mobile/AlertButtonActionBeginFlow;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AlertButton{"

    .line 124
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
