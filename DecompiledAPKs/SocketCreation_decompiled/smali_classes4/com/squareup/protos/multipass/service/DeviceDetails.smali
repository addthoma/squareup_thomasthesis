.class public final Lcom/squareup/protos/multipass/service/DeviceDetails;
.super Lcom/squareup/wire/Message;
.source "DeviceDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/multipass/service/DeviceDetails$ProtoAdapter_DeviceDetails;,
        Lcom/squareup/protos/multipass/service/DeviceDetails$Type;,
        Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/multipass/service/DeviceDetails;",
        "Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/multipass/service/DeviceDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DEVICE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_IP_ADDRESS:Ljava/lang/String; = ""

.field public static final DEFAULT_IS_SQUID:Ljava/lang/Boolean;

.field public static final DEFAULT_SECONDARY_DEVICE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_SERIAL:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final device_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final ip_address:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final is_squid:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final secondary_device_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final serial:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x5
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.service.DeviceDetails$Type#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/protos/multipass/service/DeviceDetails$ProtoAdapter_DeviceDetails;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/DeviceDetails$ProtoAdapter_DeviceDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/multipass/service/DeviceDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 30
    sget-object v0, Lcom/squareup/protos/multipass/service/DeviceDetails$Type;->WEB:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    sput-object v0, Lcom/squareup/protos/multipass/service/DeviceDetails;->DEFAULT_TYPE:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    const/4 v0, 0x0

    .line 40
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/multipass/service/DeviceDetails;->DEFAULT_IS_SQUID:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/multipass/service/DeviceDetails$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 8

    .line 112
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/multipass/service/DeviceDetails;-><init>(Lcom/squareup/protos/multipass/service/DeviceDetails$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/multipass/service/DeviceDetails$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 117
    sget-object v0, Lcom/squareup/protos/multipass/service/DeviceDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->type:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    .line 119
    iput-object p2, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->device_id:Ljava/lang/String;

    .line 120
    iput-object p3, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->ip_address:Ljava/lang/String;

    .line 121
    iput-object p4, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->secondary_device_id:Ljava/lang/String;

    .line 122
    iput-object p5, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->serial:Ljava/lang/String;

    .line 123
    iput-object p6, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->is_squid:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 142
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/multipass/service/DeviceDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 143
    :cond_1
    check-cast p1, Lcom/squareup/protos/multipass/service/DeviceDetails;

    .line 144
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/DeviceDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/DeviceDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->type:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    iget-object v3, p1, Lcom/squareup/protos/multipass/service/DeviceDetails;->type:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    .line 145
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->device_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/multipass/service/DeviceDetails;->device_id:Ljava/lang/String;

    .line 146
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->ip_address:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/multipass/service/DeviceDetails;->ip_address:Ljava/lang/String;

    .line 147
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->secondary_device_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/multipass/service/DeviceDetails;->secondary_device_id:Ljava/lang/String;

    .line 148
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->serial:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/multipass/service/DeviceDetails;->serial:Ljava/lang/String;

    .line 149
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->is_squid:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/multipass/service/DeviceDetails;->is_squid:Ljava/lang/Boolean;

    .line 150
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 155
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 157
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/DeviceDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 158
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->type:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/service/DeviceDetails$Type;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 159
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->device_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 160
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->ip_address:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->secondary_device_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 162
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->serial:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 163
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->is_squid:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 164
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;
    .locals 2

    .line 128
    new-instance v0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;-><init>()V

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->type:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->type:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->device_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->device_id:Ljava/lang/String;

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->ip_address:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->ip_address:Ljava/lang/String;

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->secondary_device_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->secondary_device_id:Ljava/lang/String;

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->serial:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->serial:Ljava/lang/String;

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->is_squid:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->is_squid:Ljava/lang/Boolean;

    .line 135
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/DeviceDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 25
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/DeviceDetails;->newBuilder()Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->type:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->type:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 173
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->device_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", device_id=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->ip_address:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", ip_address=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->secondary_device_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", secondary_device_id=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->serial:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", serial=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->is_squid:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", is_squid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails;->is_squid:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DeviceDetails{"

    .line 178
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
