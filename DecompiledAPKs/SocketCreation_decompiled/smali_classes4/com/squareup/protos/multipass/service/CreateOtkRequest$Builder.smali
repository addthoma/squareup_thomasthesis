.class public final Lcom/squareup/protos/multipass/service/CreateOtkRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateOtkRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/service/CreateOtkRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/service/CreateOtkRequest;",
        "Lcom/squareup/protos/multipass/service/CreateOtkRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_credentials:Lcom/squareup/protos/multipass/service/ClientCredentials;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/multipass/service/CreateOtkRequest;
    .locals 3

    .line 93
    new-instance v0, Lcom/squareup/protos/multipass/service/CreateOtkRequest;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/CreateOtkRequest$Builder;->client_credentials:Lcom/squareup/protos/multipass/service/ClientCredentials;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/multipass/service/CreateOtkRequest;-><init>(Lcom/squareup/protos/multipass/service/ClientCredentials;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/CreateOtkRequest$Builder;->build()Lcom/squareup/protos/multipass/service/CreateOtkRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_credentials(Lcom/squareup/protos/multipass/service/ClientCredentials;)Lcom/squareup/protos/multipass/service/CreateOtkRequest$Builder;
    .locals 0

    .line 87
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/CreateOtkRequest$Builder;->client_credentials:Lcom/squareup/protos/multipass/service/ClientCredentials;

    return-object p0
.end method
