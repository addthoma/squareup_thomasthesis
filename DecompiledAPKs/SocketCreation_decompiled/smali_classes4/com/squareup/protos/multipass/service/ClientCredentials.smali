.class public final Lcom/squareup/protos/multipass/service/ClientCredentials;
.super Lcom/squareup/wire/Message;
.source "ClientCredentials.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/multipass/service/ClientCredentials$ProtoAdapter_ClientCredentials;,
        Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/multipass/service/ClientCredentials;",
        "Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/multipass/service/ClientCredentials;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACCESS_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final access_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x6
    .end annotation
.end field

.field public final cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.service.ClientSessionCookie#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.service.DeviceDetails#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final headers:Lcom/squareup/protos/multipass/service/SessionHeaders;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.service.SessionHeaders#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.service.ClientSessionToken#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.multipass.service.ClientWebSessionToken#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/multipass/service/ClientCredentials$ProtoAdapter_ClientCredentials;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/ClientCredentials$ProtoAdapter_ClientCredentials;-><init>()V

    sput-object v0, Lcom/squareup/protos/multipass/service/ClientCredentials;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/multipass/service/DeviceDetails;Lcom/squareup/protos/multipass/service/ClientWebSessionToken;Lcom/squareup/protos/multipass/service/ClientSessionCookie;Lcom/squareup/protos/multipass/service/ClientSessionToken;Lcom/squareup/protos/multipass/service/SessionHeaders;Ljava/lang/String;)V
    .locals 8

    .line 75
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/multipass/service/ClientCredentials;-><init>(Lcom/squareup/protos/multipass/service/DeviceDetails;Lcom/squareup/protos/multipass/service/ClientWebSessionToken;Lcom/squareup/protos/multipass/service/ClientSessionCookie;Lcom/squareup/protos/multipass/service/ClientSessionToken;Lcom/squareup/protos/multipass/service/SessionHeaders;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/multipass/service/DeviceDetails;Lcom/squareup/protos/multipass/service/ClientWebSessionToken;Lcom/squareup/protos/multipass/service/ClientSessionCookie;Lcom/squareup/protos/multipass/service/ClientSessionToken;Lcom/squareup/protos/multipass/service/SessionHeaders;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 81
    sget-object v0, Lcom/squareup/protos/multipass/service/ClientCredentials;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const/4 p7, 0x0

    new-array p7, p7, [Ljava/lang/Object;

    .line 82
    invoke-static {p3, p4, p5, p6, p7}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result p7

    const/4 v0, 0x1

    if-gt p7, v0, :cond_0

    .line 85
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    .line 86
    iput-object p2, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    .line 87
    iput-object p3, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    .line 88
    iput-object p4, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    .line 89
    iput-object p5, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    .line 90
    iput-object p6, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->access_token:Ljava/lang/String;

    return-void

    .line 83
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of cookie, session_id, headers, access_token may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 109
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/multipass/service/ClientCredentials;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 110
    :cond_1
    check-cast p1, Lcom/squareup/protos/multipass/service/ClientCredentials;

    .line 111
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/ClientCredentials;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/multipass/service/ClientCredentials;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    iget-object v3, p1, Lcom/squareup/protos/multipass/service/ClientCredentials;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    .line 112
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    iget-object v3, p1, Lcom/squareup/protos/multipass/service/ClientCredentials;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    iget-object v3, p1, Lcom/squareup/protos/multipass/service/ClientCredentials;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    iget-object v3, p1, Lcom/squareup/protos/multipass/service/ClientCredentials;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    .line 115
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    iget-object v3, p1, Lcom/squareup/protos/multipass/service/ClientCredentials;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    .line 116
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->access_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/multipass/service/ClientCredentials;->access_token:Ljava/lang/String;

    .line 117
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 122
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 124
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/ClientCredentials;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/service/DeviceDetails;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 126
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/service/ClientWebSessionToken;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/service/ClientSessionCookie;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 128
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/service/ClientSessionToken;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 129
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/multipass/service/SessionHeaders;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->access_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 131
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;
    .locals 2

    .line 95
    new-instance v0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;-><init>()V

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    .line 100
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    .line 101
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->access_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->access_token:Ljava/lang/String;

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/ClientCredentials;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/ClientCredentials;->newBuilder()Lcom/squareup/protos/multipass/service/ClientCredentials$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    if-eqz v1, :cond_0

    const-string v1, ", device_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->device_details:Lcom/squareup/protos/multipass/service/DeviceDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 140
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    if-eqz v1, :cond_1

    const-string v1, ", subdomain_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->subdomain_token:Lcom/squareup/protos/multipass/service/ClientWebSessionToken;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    if-eqz v1, :cond_2

    const-string v1, ", cookie="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->cookie:Lcom/squareup/protos/multipass/service/ClientSessionCookie;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 142
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    if-eqz v1, :cond_3

    const-string v1, ", session_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->session_id:Lcom/squareup/protos/multipass/service/ClientSessionToken;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 143
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    if-eqz v1, :cond_4

    const-string v1, ", headers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->headers:Lcom/squareup/protos/multipass/service/SessionHeaders;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 144
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/multipass/service/ClientCredentials;->access_token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", access_token=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ClientCredentials{"

    .line 145
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
