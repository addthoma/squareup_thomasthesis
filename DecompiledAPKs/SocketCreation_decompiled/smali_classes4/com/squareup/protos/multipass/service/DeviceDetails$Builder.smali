.class public final Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DeviceDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/service/DeviceDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/multipass/service/DeviceDetails;",
        "Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public device_id:Ljava/lang/String;

.field public ip_address:Ljava/lang/String;

.field public is_squid:Ljava/lang/Boolean;

.field public secondary_device_id:Ljava/lang/String;

.field public serial:Ljava/lang/String;

.field public type:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 194
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/multipass/service/DeviceDetails;
    .locals 9

    .line 257
    new-instance v8, Lcom/squareup/protos/multipass/service/DeviceDetails;

    iget-object v1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->type:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    iget-object v2, p0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->device_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->ip_address:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->secondary_device_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->serial:Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->is_squid:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/multipass/service/DeviceDetails;-><init>(Lcom/squareup/protos/multipass/service/DeviceDetails$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 181
    invoke-virtual {p0}, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->build()Lcom/squareup/protos/multipass/service/DeviceDetails;

    move-result-object v0

    return-object v0
.end method

.method public device_id(Ljava/lang/String;)Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;
    .locals 0

    .line 217
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->device_id:Ljava/lang/String;

    return-object p0
.end method

.method public ip_address(Ljava/lang/String;)Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;
    .locals 0

    .line 225
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->ip_address:Ljava/lang/String;

    return-object p0
.end method

.method public is_squid(Ljava/lang/Boolean;)Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;
    .locals 0

    .line 251
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->is_squid:Ljava/lang/Boolean;

    return-object p0
.end method

.method public secondary_device_id(Ljava/lang/String;)Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;
    .locals 0

    .line 235
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->secondary_device_id:Ljava/lang/String;

    return-object p0
.end method

.method public serial(Ljava/lang/String;)Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->serial:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/multipass/service/DeviceDetails$Type;)Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lcom/squareup/protos/multipass/service/DeviceDetails$Builder;->type:Lcom/squareup/protos/multipass/service/DeviceDetails$Type;

    return-object p0
.end method
