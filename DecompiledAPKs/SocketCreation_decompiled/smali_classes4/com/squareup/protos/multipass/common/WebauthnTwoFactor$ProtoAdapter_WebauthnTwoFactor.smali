.class final Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$ProtoAdapter_WebauthnTwoFactor;
.super Lcom/squareup/wire/ProtoAdapter;
.source "WebauthnTwoFactor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_WebauthnTwoFactor"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 272
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 301
    new-instance v0, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;-><init>()V

    .line 302
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 303
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 313
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 311
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->person_token(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;

    goto :goto_0

    .line 310
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->challenge(Lokio/ByteString;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;

    goto :goto_0

    .line 309
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->public_key_credential(Ljava/lang/String;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;

    goto :goto_0

    .line 308
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->encrypted_data(Lokio/ByteString;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;

    goto :goto_0

    .line 307
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->signature(Lokio/ByteString;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;

    goto :goto_0

    .line 306
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->signature_count(Ljava/lang/Long;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;

    goto :goto_0

    .line 305
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->credential_id(Lokio/ByteString;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;

    goto :goto_0

    .line 317
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 318
    invoke-virtual {v0}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->build()Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 270
    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$ProtoAdapter_WebauthnTwoFactor;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 289
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->credential_id:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 290
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature_count:Ljava/lang/Long;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 291
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature:Lokio/ByteString;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 292
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->encrypted_data:Lokio/ByteString;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 293
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->public_key_credential:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 294
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->challenge:Lokio/ByteString;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 295
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->person_token:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 296
    invoke-virtual {p2}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 270
    check-cast p2, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$ProtoAdapter_WebauthnTwoFactor;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;)I
    .locals 4

    .line 277
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->credential_id:Lokio/ByteString;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature_count:Ljava/lang/Long;

    const/4 v3, 0x2

    .line 278
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->signature:Lokio/ByteString;

    const/4 v3, 0x3

    .line 279
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->encrypted_data:Lokio/ByteString;

    const/4 v3, 0x4

    .line 280
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->public_key_credential:Ljava/lang/String;

    const/4 v3, 0x5

    .line 281
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->challenge:Lokio/ByteString;

    const/4 v3, 0x6

    .line 282
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->person_token:Ljava/lang/String;

    const/4 v3, 0x7

    .line 283
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 284
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 270
    check-cast p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$ProtoAdapter_WebauthnTwoFactor;->encodedSize(Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;
    .locals 1

    .line 323
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;->newBuilder()Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 324
    iput-object v0, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->credential_id:Lokio/ByteString;

    .line 325
    iput-object v0, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->signature_count:Ljava/lang/Long;

    .line 326
    iput-object v0, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->signature:Lokio/ByteString;

    .line 327
    iput-object v0, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->encrypted_data:Lokio/ByteString;

    .line 328
    iput-object v0, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->public_key_credential:Ljava/lang/String;

    .line 329
    iput-object v0, p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->challenge:Lokio/ByteString;

    .line 330
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 331
    invoke-virtual {p1}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$Builder;->build()Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 270
    check-cast p1, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/multipass/common/WebauthnTwoFactor$ProtoAdapter_WebauthnTwoFactor;->redact(Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;)Lcom/squareup/protos/multipass/common/WebauthnTwoFactor;

    move-result-object p1

    return-object p1
.end method
