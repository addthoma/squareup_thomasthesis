.class public final enum Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;
.super Ljava/lang/Enum;
.source "DiagnosticReportRequest.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum BUGREPORT:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

.field public static final enum CRASH_KERNEL_LOG:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

.field public static final enum IMAGE:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

.field public static final enum KERNEL_LOG:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

.field public static final enum KERNEL_PANIC:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

.field public static final enum KWARNING:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

.field public static final enum LOGCAT:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

.field public static final enum OTHER:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

.field public static final enum TEXT:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

.field public static final enum TOMBSTONE:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

.field public static final enum TRANSACTION_LEDGER:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 570
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    const/4 v1, 0x0

    const-string v2, "OTHER"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->OTHER:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 572
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    const/4 v2, 0x1

    const-string v3, "BUGREPORT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->BUGREPORT:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 574
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    const/4 v3, 0x2

    const-string v4, "TOMBSTONE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->TOMBSTONE:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 576
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    const/4 v4, 0x3

    const-string v5, "CRASH_KERNEL_LOG"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->CRASH_KERNEL_LOG:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 578
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    const/4 v5, 0x4

    const-string v6, "KERNEL_LOG"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->KERNEL_LOG:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 580
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    const/4 v6, 0x5

    const-string v7, "LOGCAT"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->LOGCAT:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 582
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    const/4 v7, 0x6

    const-string v8, "KERNEL_PANIC"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->KERNEL_PANIC:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 587
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    const/4 v8, 0x7

    const-string v9, "IMAGE"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->IMAGE:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 589
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    const/16 v9, 0x8

    const-string v10, "TEXT"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->TEXT:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 591
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    const/16 v10, 0x9

    const-string v11, "TRANSACTION_LEDGER"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->TRANSACTION_LEDGER:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 593
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    const/16 v11, 0xa

    const-string v12, "KWARNING"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->KWARNING:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 569
    sget-object v12, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->OTHER:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    aput-object v12, v0, v1

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->BUGREPORT:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->TOMBSTONE:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->CRASH_KERNEL_LOG:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->KERNEL_LOG:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->LOGCAT:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->KERNEL_PANIC:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->IMAGE:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->TEXT:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->TRANSACTION_LEDGER:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->KWARNING:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    aput-object v1, v0, v11

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->$VALUES:[Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    .line 595
    new-instance v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 599
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 600
    iput p3, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 618
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->KWARNING:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-object p0

    .line 617
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->TRANSACTION_LEDGER:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-object p0

    .line 616
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->TEXT:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-object p0

    .line 615
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->IMAGE:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-object p0

    .line 614
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->KERNEL_PANIC:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-object p0

    .line 613
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->LOGCAT:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-object p0

    .line 612
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->KERNEL_LOG:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-object p0

    .line 611
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->CRASH_KERNEL_LOG:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-object p0

    .line 610
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->TOMBSTONE:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-object p0

    .line 609
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->BUGREPORT:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-object p0

    .line 608
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->OTHER:Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;
    .locals 1

    .line 569
    const-class v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;
    .locals 1

    .line 569
    sget-object v0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->$VALUES:[Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 625
    iget v0, p0, Lcom/squareup/protos/client/tarkin/DiagnosticReportRequest$Item$Type;->value:I

    return v0
.end method
