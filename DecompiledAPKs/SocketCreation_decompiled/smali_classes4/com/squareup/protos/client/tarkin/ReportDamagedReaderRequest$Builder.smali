.class public final Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ReportDamagedReaderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;",
        "Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public merchant_token:Ljava/lang/String;

.field public reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

.field public tamper_data_message_bytes:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 121
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;
    .locals 5

    .line 151
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->tamper_data_message_bytes:Lokio/ByteString;

    iget-object v2, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    iget-object v3, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->merchant_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;-><init>(Lokio/ByteString;Lcom/squareup/protos/client/tarkin/ReaderType;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->build()Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;

    move-result-object v0

    return-object v0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public reader_type(Lcom/squareup/protos/client/tarkin/ReaderType;)Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-object p0
.end method

.method public tamper_data_message_bytes(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->tamper_data_message_bytes:Lokio/ByteString;

    return-object p0
.end method
