.class public final Lcom/squareup/protos/client/tarkin/Asset;
.super Lcom/squareup/wire/Message;
.source "Asset.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tarkin/Asset$ProtoAdapter_Asset;,
        Lcom/squareup/protos/client/tarkin/Asset$Reboot;,
        Lcom/squareup/protos/client/tarkin/Asset$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/tarkin/Asset;",
        "Lcom/squareup/protos/client/tarkin/Asset$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tarkin/Asset;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENCRYPTED_DATA:Lokio/ByteString;

.field public static final DEFAULT_HEADER:Lokio/ByteString;

.field public static final DEFAULT_IS_BLOCKING:Ljava/lang/Boolean;

.field public static final DEFAULT_REQUIRES_REBOOT:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

.field private static final serialVersionUID:J


# instance fields
.field public final block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.AssetEncodedBlockIndexTable#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final encrypted_data:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x1
    .end annotation
.end field

.field public final header:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x3
    .end annotation
.end field

.field public final is_blocking:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final requires_reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.Asset$Reboot#ADAPTER"
        tag = 0x5
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/tarkin/Asset$ProtoAdapter_Asset;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/Asset$ProtoAdapter_Asset;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tarkin/Asset;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 27
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/tarkin/Asset;->DEFAULT_ENCRYPTED_DATA:Lokio/ByteString;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/tarkin/Asset;->DEFAULT_IS_BLOCKING:Ljava/lang/Boolean;

    .line 31
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/tarkin/Asset;->DEFAULT_HEADER:Lokio/ByteString;

    .line 33
    sget-object v0, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->NO:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    sput-object v0, Lcom/squareup/protos/client/tarkin/Asset;->DEFAULT_REQUIRES_REBOOT:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Ljava/lang/Boolean;Lokio/ByteString;Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;Lcom/squareup/protos/client/tarkin/Asset$Reboot;)V
    .locals 7

    .line 87
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/tarkin/Asset;-><init>(Lokio/ByteString;Ljava/lang/Boolean;Lokio/ByteString;Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;Lcom/squareup/protos/client/tarkin/Asset$Reboot;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Ljava/lang/Boolean;Lokio/ByteString;Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;Lcom/squareup/protos/client/tarkin/Asset$Reboot;Lokio/ByteString;)V
    .locals 1

    .line 93
    sget-object v0, Lcom/squareup/protos/client/tarkin/Asset;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 94
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/Asset;->encrypted_data:Lokio/ByteString;

    .line 95
    iput-object p2, p0, Lcom/squareup/protos/client/tarkin/Asset;->is_blocking:Ljava/lang/Boolean;

    .line 96
    iput-object p3, p0, Lcom/squareup/protos/client/tarkin/Asset;->header:Lokio/ByteString;

    .line 97
    iput-object p4, p0, Lcom/squareup/protos/client/tarkin/Asset;->block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

    .line 98
    iput-object p5, p0, Lcom/squareup/protos/client/tarkin/Asset;->requires_reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 116
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/tarkin/Asset;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 117
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/tarkin/Asset;

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/Asset;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/Asset;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->encrypted_data:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/Asset;->encrypted_data:Lokio/ByteString;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->is_blocking:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/Asset;->is_blocking:Ljava/lang/Boolean;

    .line 120
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->header:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/Asset;->header:Lokio/ByteString;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/Asset;->block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->requires_reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    iget-object p1, p1, Lcom/squareup/protos/client/tarkin/Asset;->requires_reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    .line 123
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 128
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/Asset;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->encrypted_data:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->is_blocking:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->header:Lokio/ByteString;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->requires_reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/client/tarkin/Asset$Reboot;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 136
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/tarkin/Asset$Builder;
    .locals 2

    .line 103
    new-instance v0, Lcom/squareup/protos/client/tarkin/Asset$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/Asset$Builder;-><init>()V

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->encrypted_data:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->encrypted_data:Lokio/ByteString;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->is_blocking:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->is_blocking:Ljava/lang/Boolean;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->header:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->header:Lokio/ByteString;

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->requires_reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/Asset$Builder;->requires_reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    .line 109
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/Asset;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/Asset$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/Asset;->newBuilder()Lcom/squareup/protos/client/tarkin/Asset$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 143
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->encrypted_data:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", encrypted_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->encrypted_data:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 145
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->is_blocking:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", is_blocking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->is_blocking:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 146
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->header:Lokio/ByteString;

    if-eqz v1, :cond_2

    const-string v1, ", header="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->header:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 147
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

    if-eqz v1, :cond_3

    const-string v1, ", block_index_table="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->block_index_table:Lcom/squareup/protos/client/tarkin/AssetEncodedBlockIndexTable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 148
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->requires_reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    if-eqz v1, :cond_4

    const-string v1, ", requires_reboot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/Asset;->requires_reboot:Lcom/squareup/protos/client/tarkin/Asset$Reboot;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Asset{"

    .line 149
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
