.class public final Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;
.super Lcom/squareup/wire/Message;
.source "ReportDamagedReaderRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$ProtoAdapter_ReportDamagedReaderRequest;,
        Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;",
        "Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_READER_TYPE:Lcom/squareup/protos/client/tarkin/ReaderType;

.field public static final DEFAULT_TAMPER_DATA_MESSAGE_BYTES:Lokio/ByteString;

.field private static final serialVersionUID:J


# instance fields
.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.tarkin.ReaderType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final tamper_data_message_bytes:Lokio/ByteString;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BYTES"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$ProtoAdapter_ReportDamagedReaderRequest;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$ProtoAdapter_ReportDamagedReaderRequest;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 24
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->DEFAULT_TAMPER_DATA_MESSAGE_BYTES:Lokio/ByteString;

    .line 26
    sget-object v0, Lcom/squareup/protos/client/tarkin/ReaderType;->UNKNOWN:Lcom/squareup/protos/client/tarkin/ReaderType;

    sput-object v0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->DEFAULT_READER_TYPE:Lcom/squareup/protos/client/tarkin/ReaderType;

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/squareup/protos/client/tarkin/ReaderType;Ljava/lang/String;)V
    .locals 1

    .line 60
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;-><init>(Lokio/ByteString;Lcom/squareup/protos/client/tarkin/ReaderType;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lokio/ByteString;Lcom/squareup/protos/client/tarkin/ReaderType;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 66
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->tamper_data_message_bytes:Lokio/ByteString;

    .line 67
    iput-object p2, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 68
    iput-object p3, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->merchant_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 84
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 85
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->tamper_data_message_bytes:Lokio/ByteString;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->tamper_data_message_bytes:Lokio/ByteString;

    .line 87
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    iget-object v3, p1, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 88
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->merchant_token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->merchant_token:Ljava/lang/String;

    .line 89
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 94
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->tamper_data_message_bytes:Lokio/ByteString;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lokio/ByteString;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 98
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/tarkin/ReaderType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 99
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 100
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;
    .locals 2

    .line 73
    new-instance v0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;-><init>()V

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->tamper_data_message_bytes:Lokio/ByteString;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->tamper_data_message_bytes:Lokio/ByteString;

    .line 75
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    .line 76
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->merchant_token:Ljava/lang/String;

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->newBuilder()Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->tamper_data_message_bytes:Lokio/ByteString;

    if-eqz v1, :cond_0

    const-string v1, ", tamper_data_message_bytes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->tamper_data_message_bytes:Lokio/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    if-eqz v1, :cond_1

    const-string v1, ", reader_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->reader_type:Lcom/squareup/protos/client/tarkin/ReaderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 110
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/ReportDamagedReaderRequest;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReportDamagedReaderRequest{"

    .line 111
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
