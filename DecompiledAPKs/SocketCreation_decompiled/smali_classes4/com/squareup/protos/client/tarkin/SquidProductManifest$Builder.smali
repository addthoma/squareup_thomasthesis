.class public final Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SquidProductManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tarkin/SquidProductManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/tarkin/SquidProductManifest;",
        "Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public country_code:Ljava/lang/String;

.field public spe_manifest:Lokio/ByteString;

.field public squid_manifest:Lokio/ByteString;

.field public unit_serial_number:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 132
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/tarkin/SquidProductManifest;
    .locals 7

    .line 160
    new-instance v6, Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    iget-object v1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;->unit_serial_number:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;->squid_manifest:Lokio/ByteString;

    iget-object v3, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;->spe_manifest:Lokio/ByteString;

    iget-object v4, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;->country_code:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/tarkin/SquidProductManifest;-><init>(Ljava/lang/String;Lokio/ByteString;Lokio/ByteString;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 123
    invoke-virtual {p0}, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;->build()Lcom/squareup/protos/client/tarkin/SquidProductManifest;

    move-result-object v0

    return-object v0
.end method

.method public country_code(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;->country_code:Ljava/lang/String;

    return-object p0
.end method

.method public spe_manifest(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;->spe_manifest:Lokio/ByteString;

    return-object p0
.end method

.method public squid_manifest(Lokio/ByteString;)Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;
    .locals 0

    .line 141
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;->squid_manifest:Lokio/ByteString;

    return-object p0
.end method

.method public unit_serial_number(Ljava/lang/String;)Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/tarkin/SquidProductManifest$Builder;->unit_serial_number:Ljava/lang/String;

    return-object p0
.end method
