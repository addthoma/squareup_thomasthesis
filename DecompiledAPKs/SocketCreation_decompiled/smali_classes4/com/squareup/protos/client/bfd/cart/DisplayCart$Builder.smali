.class public final Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DisplayCart.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/bfd/cart/DisplayCart;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/bfd/cart/DisplayCart;",
        "Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

.field public is_card_still_inserted:Ljava/lang/Boolean;

.field public items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/DisplayItem;",
            ">;"
        }
    .end annotation
.end field

.field public offline_mode:Ljava/lang/Boolean;

.field public total_amount:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 141
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 142
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->items:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public banner(Lcom/squareup/protos/client/bfd/cart/DisplayBanner;)Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;
    .locals 0

    .line 152
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/bfd/cart/DisplayCart;
    .locals 8

    .line 173
    new-instance v7, Lcom/squareup/protos/client/bfd/cart/DisplayCart;

    iget-object v1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->items:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->banner:Lcom/squareup/protos/client/bfd/cart/DisplayBanner;

    iget-object v3, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->total_amount:Ljava/lang/Long;

    iget-object v4, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->offline_mode:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->is_card_still_inserted:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/bfd/cart/DisplayCart;-><init>(Ljava/util/List;Lcom/squareup/protos/client/bfd/cart/DisplayBanner;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->build()Lcom/squareup/protos/client/bfd/cart/DisplayCart;

    move-result-object v0

    return-object v0
.end method

.method public is_card_still_inserted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->is_card_still_inserted:Ljava/lang/Boolean;

    return-object p0
.end method

.method public items(Ljava/util/List;)Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bfd/cart/DisplayItem;",
            ">;)",
            "Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;"
        }
    .end annotation

    .line 146
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->items:Ljava/util/List;

    return-object p0
.end method

.method public offline_mode(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->offline_mode:Ljava/lang/Boolean;

    return-object p0
.end method

.method public total_amount(Ljava/lang/Long;)Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/squareup/protos/client/bfd/cart/DisplayCart$Builder;->total_amount:Ljava/lang/Long;

    return-object p0
.end method
