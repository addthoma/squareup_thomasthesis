.class final Lcom/squareup/protos/client/ClientAction$ViewAllInvoices$ProtoAdapter_ViewAllInvoices;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ClientAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ViewAllInvoices"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1915
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1930
    new-instance v0, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices$Builder;-><init>()V

    .line 1931
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1932
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 1935
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1939
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1940
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1913
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices$ProtoAdapter_ViewAllInvoices;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1925
    invoke-virtual {p2}, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1913
    check-cast p2, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices$ProtoAdapter_ViewAllInvoices;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;)I
    .locals 0

    .line 1920
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1913
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices$ProtoAdapter_ViewAllInvoices;->encodedSize(Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;)Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;
    .locals 0

    .line 1945
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;->newBuilder()Lcom/squareup/protos/client/ClientAction$ViewAllInvoices$Builder;

    move-result-object p1

    .line 1946
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1947
    invoke-virtual {p1}, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1913
    check-cast p1, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/ClientAction$ViewAllInvoices$ProtoAdapter_ViewAllInvoices;->redact(Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;)Lcom/squareup/protos/client/ClientAction$ViewAllInvoices;

    move-result-object p1

    return-object p1
.end method
