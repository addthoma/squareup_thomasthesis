.class public final Lcom/squareup/protos/client/cbms/ListDisputesResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListDisputesResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cbms/ListDisputesResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cbms/ListDisputesResponse;",
        "Lcom/squareup/protos/client/cbms/ListDisputesResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cursor:Ljava/lang/Integer;

.field public dispute:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;"
        }
    .end annotation
.end field

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 113
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 114
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/cbms/ListDisputesResponse$Builder;->dispute:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cbms/ListDisputesResponse;
    .locals 5

    .line 138
    new-instance v0, Lcom/squareup/protos/client/cbms/ListDisputesResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/ListDisputesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/cbms/ListDisputesResponse$Builder;->dispute:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/cbms/ListDisputesResponse$Builder;->cursor:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/cbms/ListDisputesResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/ListDisputesResponse$Builder;->build()Lcom/squareup/protos/client/cbms/ListDisputesResponse;

    move-result-object v0

    return-object v0
.end method

.method public cursor(Ljava/lang/Integer;)Lcom/squareup/protos/client/cbms/ListDisputesResponse$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/ListDisputesResponse$Builder;->cursor:Ljava/lang/Integer;

    return-object p0
.end method

.method public dispute(Ljava/util/List;)Lcom/squareup/protos/client/cbms/ListDisputesResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;)",
            "Lcom/squareup/protos/client/cbms/ListDisputesResponse$Builder;"
        }
    .end annotation

    .line 123
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/ListDisputesResponse$Builder;->dispute:Ljava/util/List;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/cbms/ListDisputesResponse$Builder;
    .locals 0

    .line 118
    iput-object p1, p0, Lcom/squareup/protos/client/cbms/ListDisputesResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
