.class public final Lcom/squareup/protos/client/cbms/DisputedPayment;
.super Lcom/squareup/wire/Message;
.source "DisputedPayment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/cbms/DisputedPayment$ProtoAdapter_DisputedPayment;,
        Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;,
        Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;,
        Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;,
        Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/cbms/DisputedPayment;",
        "Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/cbms/DisputedPayment;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BANK_DISPUTED_COMMENT:Ljava/lang/String; = ""

.field public static final DEFAULT_CURRENT_ACTIONABLE_STATUS:Lcom/squareup/protos/client/cbms/ActionableStatus;

.field public static final DEFAULT_DISPUTE_REASON:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

.field public static final DEFAULT_PAYMENT_CARD_BRAND:Lcom/squareup/protos/common/instrument/InstrumentType;

.field public static final DEFAULT_PAYMENT_CARD_PAN_SUFFIX:Ljava/lang/String; = ""

.field public static final DEFAULT_PAYMENT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_PROTECTION_STATE:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

.field public static final DEFAULT_RESOLUTION:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

.field public static final DEFAULT_UNIT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final bank_disputed_comment:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cbms.ActionableStatus#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final current_amount_held:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final current_disputed_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cbms.DisputedPayment$DisputeReasonCategory#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.YearMonthDay#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final information_request:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cbms.InformationRequest#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/cbms/InformationRequest;",
            ">;"
        }
    .end annotation
.end field

.field public final latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.YearMonthDay#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final payment_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xf
    .end annotation
.end field

.field public final payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.instrument.InstrumentType#ADAPTER"
        tag = 0x10
    .end annotation
.end field

.field public final payment_card_pan_suffix:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x11
    .end annotation
.end field

.field public final payment_created_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final payment_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final protection_state:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cbms.DisputedPayment$ProtectionState#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.cbms.DisputedPayment$Resolution#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0xb
    .end annotation
.end field

.field public final submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0xa
    .end annotation
.end field

.field public final unit_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x12
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtoAdapter_DisputedPayment;

    invoke-direct {v0}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtoAdapter_DisputedPayment;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 35
    sget-object v0, Lcom/squareup/protos/client/cbms/ActionableStatus;->UNKNOWN:Lcom/squareup/protos/client/cbms/ActionableStatus;

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment;->DEFAULT_CURRENT_ACTIONABLE_STATUS:Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 37
    sget-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment;->DEFAULT_DISPUTE_REASON:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 41
    sget-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->R_UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment;->DEFAULT_RESOLUTION:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    .line 43
    sget-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->PS_UNKNOWN:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment;->DEFAULT_PROTECTION_STATE:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    .line 45
    sget-object v0, Lcom/squareup/protos/common/instrument/InstrumentType;->UNKNOWN:Lcom/squareup/protos/common/instrument/InstrumentType;

    sput-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment;->DEFAULT_PAYMENT_CARD_BRAND:Lcom/squareup/protos/common/instrument/InstrumentType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;Lokio/ByteString;)V
    .locals 1

    .line 165
    sget-object v0, Lcom/squareup/protos/client/cbms/DisputedPayment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 166
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_token:Ljava/lang/String;

    .line 167
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->unit_token:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->unit_token:Ljava/lang/String;

    .line 168
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 169
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 170
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->bank_disputed_comment:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->bank_disputed_comment:Ljava/lang/String;

    .line 171
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_amount_held:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_amount_held:Lcom/squareup/protos/common/Money;

    .line 172
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    .line 173
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->information_request:Ljava/util/List;

    const-string v0, "information_request"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->information_request:Ljava/util/List;

    .line 174
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    .line 175
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->protection_state:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->protection_state:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    .line 176
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    .line 177
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    .line 178
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 179
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 180
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 181
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_amount:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_amount:Lcom/squareup/protos/common/Money;

    .line 182
    iget-object p2, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;

    iput-object p2, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;

    .line 183
    iget-object p1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_card_pan_suffix:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_pan_suffix:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 214
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 215
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/cbms/DisputedPayment;

    .line 216
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/DisputedPayment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/cbms/DisputedPayment;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_token:Ljava/lang/String;

    .line 217
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->unit_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->unit_token:Ljava/lang/String;

    .line 218
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 219
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 220
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->bank_disputed_comment:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->bank_disputed_comment:Ljava/lang/String;

    .line 221
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_amount_held:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_amount_held:Lcom/squareup/protos/common/Money;

    .line 222
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    .line 223
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->information_request:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->information_request:Ljava/util/List;

    .line 224
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    .line 225
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->protection_state:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->protection_state:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    .line 226
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    .line 227
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    .line 228
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 229
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 230
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 231
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_amount:Lcom/squareup/protos/common/Money;

    .line 232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;

    iget-object v3, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;

    .line 233
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_pan_suffix:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_pan_suffix:Ljava/lang/String;

    .line 234
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 239
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_11

    .line 241
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/DisputedPayment;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/ActionableStatus;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->bank_disputed_comment:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_amount_held:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 248
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 249
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->information_request:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 250
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 251
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->protection_state:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 252
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 253
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 254
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 255
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/YearMonthDay;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 256
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/YearMonthDay;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 257
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 258
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/protos/common/instrument/InstrumentType;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 259
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_10
    add-int/2addr v0, v2

    .line 260
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_11
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;
    .locals 2

    .line 188
    new-instance v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;-><init>()V

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_token:Ljava/lang/String;

    .line 190
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->unit_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->unit_token:Ljava/lang/String;

    .line 191
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    .line 192
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    .line 193
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->bank_disputed_comment:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->bank_disputed_comment:Ljava/lang/String;

    .line 194
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_amount_held:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_amount_held:Lcom/squareup/protos/common/Money;

    .line 195
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    .line 196
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->information_request:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->information_request:Ljava/util/List;

    .line 197
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    .line 198
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->protection_state:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->protection_state:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    .line 199
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    .line 200
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    .line 201
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    .line 202
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 203
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 204
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_amount:Lcom/squareup/protos/common/Money;

    .line 205
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;

    .line 206
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_pan_suffix:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->payment_card_pan_suffix:Ljava/lang/String;

    .line 207
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/DisputedPayment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/client/cbms/DisputedPayment;->newBuilder()Lcom/squareup/protos/client/cbms/DisputedPayment$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 268
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", payment_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->unit_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", unit_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->unit_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    if-eqz v1, :cond_2

    const-string v1, ", current_actionable_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_actionable_status:Lcom/squareup/protos/client/cbms/ActionableStatus;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 271
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    if-eqz v1, :cond_3

    const-string v1, ", dispute_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->dispute_reason:Lcom/squareup/protos/client/cbms/DisputedPayment$DisputeReasonCategory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 272
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->bank_disputed_comment:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", bank_disputed_comment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->bank_disputed_comment:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_amount_held:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", current_amount_held="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_amount_held:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 274
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", current_disputed_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->current_disputed_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 275
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->information_request:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, ", information_request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->information_request:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 276
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    if-eqz v1, :cond_8

    const-string v1, ", resolution="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution:Lcom/squareup/protos/client/cbms/DisputedPayment$Resolution;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 277
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->protection_state:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    if-eqz v1, :cond_9

    const-string v1, ", protection_state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->protection_state:Lcom/squareup/protos/client/cbms/DisputedPayment$ProtectionState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 278
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_a

    const-string v1, ", submitted_to_bank_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->submitted_to_bank_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 279
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_b

    const-string v1, ", resolution_decided_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->resolution_decided_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 280
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_c

    const-string v1, ", payment_created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_created_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 281
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_d

    const-string v1, ", latest_reporting_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->latest_reporting_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 282
    :cond_d
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_e

    const-string v1, ", expected_resolution_date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->expected_resolution_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 283
    :cond_e
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_f

    const-string v1, ", payment_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 284
    :cond_f
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;

    if-eqz v1, :cond_10

    const-string v1, ", payment_card_brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_brand:Lcom/squareup/protos/common/instrument/InstrumentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 285
    :cond_10
    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_pan_suffix:Ljava/lang/String;

    if-eqz v1, :cond_11

    const-string v1, ", payment_card_pan_suffix="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/cbms/DisputedPayment;->payment_card_pan_suffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_11
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DisputedPayment{"

    .line 286
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
