.class public final Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;
.super Lcom/squareup/wire/Message;
.source "BillProcessingResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$ProtoAdapter_BillProcessingResult;,
        Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;,
        Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;,
        Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;",
        "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_STATUS:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final bill_or_payment_id:Lcom/squareup/protos/client/IdPair;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.store_and_forward.bills.BillProcessingResult$Status#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final tender_id:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.IdPair#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;"
        }
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.store_and_forward.bills.BillProcessingResult$Type#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$ProtoAdapter_BillProcessingResult;

    invoke-direct {v0}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$ProtoAdapter_BillProcessingResult;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->TYPE_UNKNOWN:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->DEFAULT_TYPE:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    .line 30
    sget-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->STATUS_UNKNOWN:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    sput-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->DEFAULT_STATUS:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;",
            ")V"
        }
    .end annotation

    .line 75
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;-><init>(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;Lcom/squareup/protos/client/IdPair;Ljava/util/List;Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;",
            "Lcom/squareup/protos/client/IdPair;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/IdPair;",
            ">;",
            "Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 80
    sget-object v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 81
    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->type:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    .line 82
    iput-object p2, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    const-string p1, "tender_id"

    .line 83
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->tender_id:Ljava/util/List;

    .line 84
    iput-object p4, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 101
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 102
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->type:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    iget-object v3, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->type:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    .line 104
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    iget-object v3, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    .line 105
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->tender_id:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->tender_id:Ljava/util/List;

    .line 106
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    iget-object p1, p1, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    .line 107
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 112
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->type:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/IdPair;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->tender_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 119
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;
    .locals 2

    .line 89
    new-instance v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;-><init>()V

    .line 90
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->type:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->type:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    .line 92
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->tender_id:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->tender_id:Ljava/util/List;

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    iput-object v1, v0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->newBuilder()Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 127
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->type:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->type:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 128
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    if-eqz v1, :cond_1

    const-string v1, ", bill_or_payment_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->bill_or_payment_id:Lcom/squareup/protos/client/IdPair;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 129
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->tender_id:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", tender_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->tender_id:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 130
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    if-eqz v1, :cond_3

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult;->status:Lcom/squareup/protos/client/store_and_forward/bills/BillProcessingResult$Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "BillProcessingResult{"

    .line 131
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
