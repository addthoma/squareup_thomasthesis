.class public final Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VerifyAndLinkInstrumentRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;",
        "Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cardholder_name:Ljava/lang/String;

.field public contact_token:Ljava/lang/String;

.field public creator_details:Lcom/squareup/protos/client/CreatorDetails;

.field public entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

.field public linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

.field public unique_key:Ljava/lang/String;

.field public validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 193
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;
    .locals 10

    .line 254
    new-instance v9, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->unique_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->cardholder_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    iget-object v4, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    iget-object v5, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    iget-object v6, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->contact_token:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/instruments/LinkedInstrument;Lcom/squareup/protos/client/instruments/ValidationInformation;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;Ljava/lang/String;Lcom/squareup/protos/client/CreatorDetails;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 178
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->build()Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest;

    move-result-object v0

    return-object v0
.end method

.method public cardholder_name(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->cardholder_name:Ljava/lang/String;

    return-object p0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;
    .locals 0

    .line 239
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public creator_details(Lcom/squareup/protos/client/CreatorDetails;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->creator_details:Lcom/squareup/protos/client/CreatorDetails;

    return-object p0
.end method

.method public entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;
    .locals 0

    .line 231
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    return-object p0
.end method

.method public linked_instrument(Lcom/squareup/protos/client/instruments/LinkedInstrument;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->linked_instrument:Lcom/squareup/protos/client/instruments/LinkedInstrument;

    return-object p0
.end method

.method public unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;
    .locals 0

    .line 200
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->unique_key:Ljava/lang/String;

    return-object p0
.end method

.method public validation_information(Lcom/squareup/protos/client/instruments/ValidationInformation;)Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;
    .locals 0

    .line 223
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/VerifyAndLinkInstrumentRequest$Builder;->validation_information:Lcom/squareup/protos/client/instruments/ValidationInformation;

    return-object p0
.end method
