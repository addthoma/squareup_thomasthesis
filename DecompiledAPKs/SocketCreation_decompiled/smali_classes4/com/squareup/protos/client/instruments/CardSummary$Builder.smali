.class public final Lcom/squareup/protos/client/instruments/CardSummary$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CardSummary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/instruments/CardSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/instruments/CardSummary;",
        "Lcom/squareup/protos/client/instruments/CardSummary$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public card:Lcom/squareup/protos/client/bills/CardTender$Card;

.field public cardholder_name:Ljava/lang/String;

.field public expiration_date:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

.field public gift_card:Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 138
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/instruments/CardSummary;
    .locals 7

    .line 172
    new-instance v6, Lcom/squareup/protos/client/instruments/CardSummary;

    iget-object v1, p0, Lcom/squareup/protos/client/instruments/CardSummary$Builder;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    iget-object v2, p0, Lcom/squareup/protos/client/instruments/CardSummary$Builder;->cardholder_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/instruments/CardSummary$Builder;->expiration_date:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    iget-object v4, p0, Lcom/squareup/protos/client/instruments/CardSummary$Builder;->gift_card:Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/instruments/CardSummary;-><init>(Lcom/squareup/protos/client/bills/CardTender$Card;Ljava/lang/String;Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/instruments/CardSummary$Builder;->build()Lcom/squareup/protos/client/instruments/CardSummary;

    move-result-object v0

    return-object v0
.end method

.method public card(Lcom/squareup/protos/client/bills/CardTender$Card;)Lcom/squareup/protos/client/instruments/CardSummary$Builder;
    .locals 0

    .line 142
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/CardSummary$Builder;->card:Lcom/squareup/protos/client/bills/CardTender$Card;

    return-object p0
.end method

.method public cardholder_name(Ljava/lang/String;)Lcom/squareup/protos/client/instruments/CardSummary$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/CardSummary$Builder;->cardholder_name:Ljava/lang/String;

    return-object p0
.end method

.method public expiration_date(Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;)Lcom/squareup/protos/client/instruments/CardSummary$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/CardSummary$Builder;->expiration_date:Lcom/squareup/protos/client/bills/CardData$KeyedCard$Expiry;

    return-object p0
.end method

.method public gift_card(Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;)Lcom/squareup/protos/client/instruments/CardSummary$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/client/instruments/CardSummary$Builder;->gift_card:Lcom/squareup/protos/client/instruments/CardSummary$GiftCardSummary;

    return-object p0
.end method
