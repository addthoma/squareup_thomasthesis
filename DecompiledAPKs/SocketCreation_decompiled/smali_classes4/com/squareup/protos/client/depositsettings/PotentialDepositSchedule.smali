.class public final Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;
.super Lcom/squareup/wire/Message;
.source "PotentialDepositSchedule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$ProtoAdapter_PotentialDepositSchedule;,
        Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;",
        "Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final deposit_schedule:Lcom/squareup/protos/client/depositsettings/DepositSchedule;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.depositsettings.DepositSchedule#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final requested_cutoff_at:Lcom/squareup/protos/common/time/LocalTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.LocalTime#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$ProtoAdapter_PotentialDepositSchedule;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$ProtoAdapter_PotentialDepositSchedule;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/protos/client/depositsettings/DepositSchedule;)V
    .locals 1

    .line 47
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;-><init>(Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/protos/client/depositsettings/DepositSchedule;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/time/LocalTime;Lcom/squareup/protos/client/depositsettings/DepositSchedule;Lokio/ByteString;)V
    .locals 1

    .line 52
    sget-object v0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 53
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->requested_cutoff_at:Lcom/squareup/protos/common/time/LocalTime;

    .line 54
    iput-object p2, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->deposit_schedule:Lcom/squareup/protos/client/depositsettings/DepositSchedule;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 69
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 70
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;

    .line 71
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->requested_cutoff_at:Lcom/squareup/protos/common/time/LocalTime;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->requested_cutoff_at:Lcom/squareup/protos/common/time/LocalTime;

    .line 72
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->deposit_schedule:Lcom/squareup/protos/client/depositsettings/DepositSchedule;

    iget-object p1, p1, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->deposit_schedule:Lcom/squareup/protos/client/depositsettings/DepositSchedule;

    .line 73
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 78
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 80
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->requested_cutoff_at:Lcom/squareup/protos/common/time/LocalTime;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/LocalTime;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->deposit_schedule:Lcom/squareup/protos/client/depositsettings/DepositSchedule;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/depositsettings/DepositSchedule;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 83
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;
    .locals 2

    .line 59
    new-instance v0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;-><init>()V

    .line 60
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->requested_cutoff_at:Lcom/squareup/protos/common/time/LocalTime;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;->requested_cutoff_at:Lcom/squareup/protos/common/time/LocalTime;

    .line 61
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->deposit_schedule:Lcom/squareup/protos/client/depositsettings/DepositSchedule;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;->deposit_schedule:Lcom/squareup/protos/client/depositsettings/DepositSchedule;

    .line 62
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->newBuilder()Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->requested_cutoff_at:Lcom/squareup/protos/common/time/LocalTime;

    if-eqz v1, :cond_0

    const-string v1, ", requested_cutoff_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->requested_cutoff_at:Lcom/squareup/protos/common/time/LocalTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->deposit_schedule:Lcom/squareup/protos/client/depositsettings/DepositSchedule;

    if-eqz v1, :cond_1

    const-string v1, ", deposit_schedule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/PotentialDepositSchedule;->deposit_schedule:Lcom/squareup/protos/client/depositsettings/DepositSchedule;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PotentialDepositSchedule{"

    .line 93
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
