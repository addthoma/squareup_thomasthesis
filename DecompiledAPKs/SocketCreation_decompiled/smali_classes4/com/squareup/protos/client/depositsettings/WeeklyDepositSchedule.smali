.class public final Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;
.super Lcom/squareup/wire/Message;
.source "WeeklyDepositSchedule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$ProtoAdapter_WeeklyDepositSchedule;,
        Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;",
        "Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PAUSED_SETTLEMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_SETTLE_IF_OPTIONAL:Ljava/lang/Boolean;

.field public static final DEFAULT_TIMEZONE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final day_of_week_deposit_settings:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.depositsettings.DayOfWeekDepositSettings#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
            ">;"
        }
    .end annotation
.end field

.field public final effective_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final paused_settlement:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final settle_if_optional:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final timezone:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$ProtoAdapter_WeeklyDepositSchedule;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$ProtoAdapter_WeeklyDepositSchedule;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->DEFAULT_SETTLE_IF_OPTIONAL:Ljava/lang/Boolean;

    .line 31
    sput-object v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->DEFAULT_PAUSED_SETTLEMENT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 86
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;-><init>(Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/common/time/DateTime;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/time/DateTime;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/depositsettings/DayOfWeekDepositSettings;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 92
    sget-object v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 93
    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->effective_at:Lcom/squareup/protos/common/time/DateTime;

    .line 94
    iput-object p2, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->timezone:Ljava/lang/String;

    const-string p1, "day_of_week_deposit_settings"

    .line 95
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->day_of_week_deposit_settings:Ljava/util/List;

    .line 96
    iput-object p4, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->settle_if_optional:Ljava/lang/Boolean;

    .line 97
    iput-object p5, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->paused_settlement:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 115
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 116
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;

    .line 117
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->effective_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->effective_at:Lcom/squareup/protos/common/time/DateTime;

    .line 118
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->timezone:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->timezone:Ljava/lang/String;

    .line 119
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->day_of_week_deposit_settings:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->day_of_week_deposit_settings:Ljava/util/List;

    .line 120
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->settle_if_optional:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->settle_if_optional:Ljava/lang/Boolean;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->paused_settlement:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->paused_settlement:Ljava/lang/Boolean;

    .line 122
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 127
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->effective_at:Lcom/squareup/protos/common/time/DateTime;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->timezone:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->day_of_week_deposit_settings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->settle_if_optional:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->paused_settlement:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 135
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;
    .locals 2

    .line 102
    new-instance v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;-><init>()V

    .line 103
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->effective_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->effective_at:Lcom/squareup/protos/common/time/DateTime;

    .line 104
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->timezone:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->timezone:Ljava/lang/String;

    .line 105
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->day_of_week_deposit_settings:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->day_of_week_deposit_settings:Ljava/util/List;

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->settle_if_optional:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->settle_if_optional:Ljava/lang/Boolean;

    .line 107
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->paused_settlement:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->paused_settlement:Ljava/lang/Boolean;

    .line 108
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->newBuilder()Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->effective_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_0

    const-string v1, ", effective_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->effective_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 144
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->timezone:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", timezone="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->timezone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->day_of_week_deposit_settings:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", day_of_week_deposit_settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->day_of_week_deposit_settings:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 146
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->settle_if_optional:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", settle_if_optional="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->settle_if_optional:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 147
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->paused_settlement:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", paused_settlement="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/depositsettings/WeeklyDepositSchedule;->paused_settlement:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "WeeklyDepositSchedule{"

    .line 148
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
