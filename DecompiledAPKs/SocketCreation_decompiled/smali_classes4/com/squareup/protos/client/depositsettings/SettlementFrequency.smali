.class public final enum Lcom/squareup/protos/client/depositsettings/SettlementFrequency;
.super Ljava/lang/Enum;
.source "SettlementFrequency.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/depositsettings/SettlementFrequency$ProtoAdapter_SettlementFrequency;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/depositsettings/SettlementFrequency;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/depositsettings/SettlementFrequency;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DAILY:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

.field public static final enum WEEKDAY:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

.field public static final enum WEEKLY:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 14
    new-instance v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->DO_NOT_USE:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    .line 16
    new-instance v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    const/4 v2, 0x1

    const-string v3, "DAILY"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->DAILY:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    .line 18
    new-instance v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    const/4 v3, 0x2

    const-string v4, "WEEKDAY"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->WEEKDAY:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    .line 20
    new-instance v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    const/4 v4, 0x3

    const-string v5, "WEEKLY"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->WEEKLY:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    .line 13
    sget-object v5, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->DO_NOT_USE:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->DAILY:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->WEEKDAY:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->WEEKLY:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->$VALUES:[Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    .line 22
    new-instance v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency$ProtoAdapter_SettlementFrequency;

    invoke-direct {v0}, Lcom/squareup/protos/client/depositsettings/SettlementFrequency$ProtoAdapter_SettlementFrequency;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 27
    iput p3, p0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/depositsettings/SettlementFrequency;
    .locals 1

    if-eqz p0, :cond_3

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 38
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->WEEKLY:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    return-object p0

    .line 37
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->WEEKDAY:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    return-object p0

    .line 36
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->DAILY:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    return-object p0

    .line 35
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->DO_NOT_USE:Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/depositsettings/SettlementFrequency;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/depositsettings/SettlementFrequency;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->$VALUES:[Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/depositsettings/SettlementFrequency;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 45
    iget v0, p0, Lcom/squareup/protos/client/depositsettings/SettlementFrequency;->value:I

    return v0
.end method
