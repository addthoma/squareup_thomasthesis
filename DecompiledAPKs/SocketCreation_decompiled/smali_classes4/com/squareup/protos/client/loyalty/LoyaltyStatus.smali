.class public final Lcom/squareup/protos/client/loyalty/LoyaltyStatus;
.super Lcom/squareup/wire/Message;
.source "LoyaltyStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ProtoAdapter_LoyaltyStatus;,
        Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;,
        Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/loyalty/LoyaltyStatus;",
        "Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BALANCE:Ljava/lang/Integer;

.field public static final DEFAULT_CURRENT_POINTS:Ljava/lang/Long;

.field public static final DEFAULT_EARLY_COMPUTED_REASON:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

.field public static final DEFAULT_EARNED_POINTS:Ljava/lang/Long;

.field public static final DEFAULT_EARNED_REWARD:Ljava/lang/Boolean;

.field public static final DEFAULT_LIFETIME_POINTS:Ljava/lang/Long;

.field public static final DEFAULT_NEW_REWARDS_EARNED:Ljava/lang/Integer;

.field public static final DEFAULT_NEW_STARS_EARNED:Ljava/lang/Long;

.field public static final DEFAULT_OBFUSCATED_PHONE_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_PHONE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_PREVIOUS_STARS_EARNED:Ljava/lang/Long;

.field public static final DEFAULT_PREVIOUS_STARS_REMAINING:Ljava/lang/Long;

.field public static final DEFAULT_REWARD_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final balance:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0xe
    .end annotation
.end field

.field public final current_points:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x9
    .end annotation
.end field

.field public final early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.LoyaltyStatus$ReasonForPointAccrual#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final earned_points:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0xa
    .end annotation
.end field

.field public final earned_reward:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final lifetime_points:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0xc
    .end annotation
.end field

.field public final new_rewards_earned:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT32"
        tag = 0x6
    .end annotation
.end field

.field public final new_stars_earned:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x3
    .end annotation
.end field

.field public final obfuscated_phone_number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final phone_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final previous_stars_earned:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x1
    .end annotation
.end field

.field public final previous_stars_remaining:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x2
    .end annotation
.end field

.field public final reward_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final reward_tiers:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.loyalty.RewardTier#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/RewardTier;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 29
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ProtoAdapter_LoyaltyStatus;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ProtoAdapter_LoyaltyStatus;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 33
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->DEFAULT_PREVIOUS_STARS_EARNED:Ljava/lang/Long;

    .line 35
    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->DEFAULT_PREVIOUS_STARS_REMAINING:Ljava/lang/Long;

    .line 37
    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->DEFAULT_NEW_STARS_EARNED:Ljava/lang/Long;

    const/4 v1, 0x0

    .line 39
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->DEFAULT_EARNED_REWARD:Ljava/lang/Boolean;

    .line 43
    sput-object v2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->DEFAULT_NEW_REWARDS_EARNED:Ljava/lang/Integer;

    .line 49
    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->DEFAULT_CURRENT_POINTS:Ljava/lang/Long;

    .line 51
    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->DEFAULT_EARNED_POINTS:Ljava/lang/Long;

    .line 53
    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->DEFAULT_LIFETIME_POINTS:Ljava/lang/Long;

    .line 55
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->ACCRUAL_REASON_UNKNOWN:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    sput-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->DEFAULT_EARLY_COMPUTED_REASON:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 57
    sput-object v2, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->DEFAULT_BALANCE:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Long;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Ljava/lang/Integer;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/RewardTier;",
            ">;",
            "Ljava/lang/Long;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .line 206
    sget-object v15, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Long;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Ljava/lang/Integer;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/Long;Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;Ljava/lang/Integer;Lokio/ByteString;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/RewardTier;",
            ">;",
            "Ljava/lang/Long;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;",
            "Ljava/lang/Integer;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 214
    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p15

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 215
    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_earned:Ljava/lang/Long;

    move-object v1, p2

    .line 216
    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_remaining:Ljava/lang/Long;

    move-object v1, p3

    .line 217
    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_stars_earned:Ljava/lang/Long;

    move-object v1, p4

    .line 218
    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_reward:Ljava/lang/Boolean;

    move-object v1, p5

    .line 219
    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_name:Ljava/lang/String;

    move-object v1, p6

    .line 220
    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_rewards_earned:Ljava/lang/Integer;

    move-object v1, p7

    .line 221
    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->obfuscated_phone_number:Ljava/lang/String;

    move-object v1, p8

    .line 222
    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->phone_token:Ljava/lang/String;

    move-object v1, p9

    .line 223
    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->current_points:Ljava/lang/Long;

    move-object v1, p10

    .line 224
    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_points:Ljava/lang/Long;

    const-string v1, "reward_tiers"

    move-object v2, p11

    .line 225
    invoke-static {v1, p11}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_tiers:Ljava/util/List;

    move-object v1, p12

    .line 226
    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->lifetime_points:Ljava/lang/Long;

    move-object/from16 v1, p13

    .line 227
    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    move-object/from16 v1, p14

    .line 228
    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 255
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 256
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;

    .line 257
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_earned:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_earned:Ljava/lang/Long;

    .line 258
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_remaining:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_remaining:Ljava/lang/Long;

    .line 259
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_stars_earned:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_stars_earned:Ljava/lang/Long;

    .line 260
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_reward:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_reward:Ljava/lang/Boolean;

    .line 261
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_name:Ljava/lang/String;

    .line 262
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_rewards_earned:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_rewards_earned:Ljava/lang/Integer;

    .line 263
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->obfuscated_phone_number:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->obfuscated_phone_number:Ljava/lang/String;

    .line 264
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->phone_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->phone_token:Ljava/lang/String;

    .line 265
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->current_points:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->current_points:Ljava/lang/Long;

    .line 266
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_points:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_points:Ljava/lang/Long;

    .line 267
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_tiers:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_tiers:Ljava/util/List;

    .line 268
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->lifetime_points:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->lifetime_points:Ljava/lang/Long;

    .line 269
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    iget-object v3, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 270
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    .line 271
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 276
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_d

    .line 278
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 279
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_earned:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 280
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_remaining:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 281
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_stars_earned:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 282
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_reward:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 283
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_name:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 284
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_rewards_earned:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 285
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->obfuscated_phone_number:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 286
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->phone_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 287
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->current_points:Ljava/lang/Long;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 288
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_points:Ljava/lang/Long;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 289
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_tiers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 290
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->lifetime_points:Ljava/lang/Long;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 291
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 292
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :cond_c
    add-int/2addr v0, v2

    .line 293
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_d
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;
    .locals 2

    .line 233
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;-><init>()V

    .line 234
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_earned:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->previous_stars_earned:Ljava/lang/Long;

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_remaining:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->previous_stars_remaining:Ljava/lang/Long;

    .line 236
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_stars_earned:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->new_stars_earned:Ljava/lang/Long;

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_reward:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->earned_reward:Ljava/lang/Boolean;

    .line 238
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->reward_name:Ljava/lang/String;

    .line 239
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_rewards_earned:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->new_rewards_earned:Ljava/lang/Integer;

    .line 240
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->obfuscated_phone_number:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->obfuscated_phone_number:Ljava/lang/String;

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->phone_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->phone_token:Ljava/lang/String;

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->current_points:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->current_points:Ljava/lang/Long;

    .line 243
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_points:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->earned_points:Ljava/lang/Long;

    .line 244
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_tiers:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->reward_tiers:Ljava/util/List;

    .line 245
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->lifetime_points:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->lifetime_points:Ljava/lang/Long;

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    .line 247
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->balance:Ljava/lang/Integer;

    .line 248
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->newBuilder()Lcom/squareup/protos/client/loyalty/LoyaltyStatus$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 300
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 301
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_earned:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", previous_stars_earned="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_earned:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 302
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_remaining:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", previous_stars_remaining="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->previous_stars_remaining:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 303
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_stars_earned:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", new_stars_earned="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_stars_earned:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 304
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_reward:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", earned_reward="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_reward:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 305
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_name:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", reward_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_rewards_earned:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    const-string v1, ", new_rewards_earned="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->new_rewards_earned:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 307
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->obfuscated_phone_number:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", obfuscated_phone_number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->obfuscated_phone_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->phone_token:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", phone_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->phone_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 309
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->current_points:Ljava/lang/Long;

    if-eqz v1, :cond_8

    const-string v1, ", current_points="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->current_points:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 310
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_points:Ljava/lang/Long;

    if-eqz v1, :cond_9

    const-string v1, ", earned_points="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->earned_points:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 311
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_tiers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, ", reward_tiers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->reward_tiers:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 312
    :cond_a
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->lifetime_points:Ljava/lang/Long;

    if-eqz v1, :cond_b

    const-string v1, ", lifetime_points="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->lifetime_points:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 313
    :cond_b
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    if-eqz v1, :cond_c

    const-string v1, ", early_computed_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->early_computed_reason:Lcom/squareup/protos/client/loyalty/LoyaltyStatus$ReasonForPointAccrual;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 314
    :cond_c
    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    const-string v1, ", balance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/LoyaltyStatus;->balance:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "LoyaltyStatus{"

    .line 315
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
