.class public final Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateLoyaltyAccountRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest;",
        "Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_token:Ljava/lang/String;

.field public loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 107
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest;
    .locals 4

    .line 130
    new-instance v0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;->contact_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest;-><init>(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 102
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;->build()Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest;

    move-result-object v0

    return-object v0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;
    .locals 0

    .line 124
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public loyalty_account_mapping(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest$Builder;->loyalty_account_mapping:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    return-object p0
.end method
