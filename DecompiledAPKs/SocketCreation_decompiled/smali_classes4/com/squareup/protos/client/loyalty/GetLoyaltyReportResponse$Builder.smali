.class public final Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetLoyaltyReportResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public average_spend:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
            ">;"
        }
    .end annotation
.end field

.field public average_visits:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
            ">;"
        }
    .end annotation
.end field

.field public data_size:Ljava/lang/Long;

.field public loyalty_customer_count:Ljava/lang/Long;

.field public status:Lcom/squareup/protos/client/Status;

.field public top_customer_counts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;",
            ">;"
        }
    .end annotation
.end field

.field public top_reward_counts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 194
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 195
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->average_visits:Ljava/util/List;

    .line 196
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->average_spend:Ljava/util/List;

    .line 197
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->top_customer_counts:Ljava/util/List;

    .line 198
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->top_reward_counts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public average_spend(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
            ">;)",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;"
        }
    .end annotation

    .line 227
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->average_spend:Ljava/util/List;

    return-object p0
.end method

.method public average_visits(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$AggregateCount;",
            ">;)",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;"
        }
    .end annotation

    .line 218
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->average_visits:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;
    .locals 10

    .line 260
    new-instance v9, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->loyalty_customer_count:Ljava/lang/Long;

    iget-object v3, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->average_visits:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->average_spend:Ljava/util/List;

    iget-object v5, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->top_customer_counts:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->top_reward_counts:Ljava/util/List;

    iget-object v7, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->data_size:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/Long;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 179
    invoke-virtual {p0}, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->build()Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;

    move-result-object v0

    return-object v0
.end method

.method public data_size(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;
    .locals 0

    .line 254
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->data_size:Ljava/lang/Long;

    return-object p0
.end method

.method public loyalty_customer_count(Ljava/lang/Long;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;
    .locals 0

    .line 210
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->loyalty_customer_count:Ljava/lang/Long;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;
    .locals 0

    .line 202
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public top_customer_counts(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;",
            ">;)",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;"
        }
    .end annotation

    .line 236
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 237
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->top_customer_counts:Ljava/util/List;

    return-object p0
.end method

.method public top_reward_counts(Ljava/util/List;)Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$FrequencyCount;",
            ">;)",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;"
        }
    .end annotation

    .line 245
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 246
    iput-object p1, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse$Builder;->top_reward_counts:Ljava/util/List;

    return-object p0
.end method
