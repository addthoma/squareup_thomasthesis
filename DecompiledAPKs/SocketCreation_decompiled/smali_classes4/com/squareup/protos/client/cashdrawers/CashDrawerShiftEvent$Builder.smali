.class public final Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CashDrawerShiftEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_event_id:Ljava/lang/String;

.field public client_payment_id:Ljava/lang/String;

.field public client_refund_id:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

.field public employee_id:Ljava/lang/String;

.field public event_money:Lcom/squareup/protos/common/Money;

.field public event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

.field public occurred_at:Lcom/squareup/protos/client/ISO8601Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 220
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;
    .locals 12

    .line 285
    new-instance v11, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_event_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->employee_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->description:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    iget-object v5, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->event_money:Lcom/squareup/protos/common/Money;

    iget-object v6, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v7, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_payment_id:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_refund_id:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/ISO8601Date;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 201
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;

    move-result-object v0

    return-object v0
.end method

.method public client_event_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_event_id:Ljava/lang/String;

    return-object p0
.end method

.method public client_payment_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;
    .locals 0

    .line 263
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_payment_id:Ljava/lang/String;

    return-object p0
.end method

.method public client_refund_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;
    .locals 0

    .line 271
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->client_refund_id:Ljava/lang/String;

    return-object p0
.end method

.method public description(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->description:Ljava/lang/String;

    return-object p0
.end method

.method public employee(Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;
    .locals 0

    .line 279
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    return-object p0
.end method

.method public employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;
    .locals 0

    .line 232
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->employee_id:Ljava/lang/String;

    return-object p0
.end method

.method public event_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->event_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public event_type(Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;
    .locals 0

    .line 242
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->event_type:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Type;

    return-object p0
.end method

.method public occurred_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent$Builder;->occurred_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method
