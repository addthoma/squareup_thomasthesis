.class final Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$ProtoAdapter_CashDrawerShift;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CashDrawerShift.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CashDrawerShift"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 771
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 842
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;-><init>()V

    .line 843
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 844
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 882
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 880
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->event_data_omitted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto :goto_0

    .line 879
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_employee(Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto :goto_0

    .line 878
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_employee(Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto :goto_0

    .line 877
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_employee(Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto :goto_0

    .line 876
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_device_info(Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto :goto_0

    .line 875
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_device_info(Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto :goto_0

    .line 874
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_device_info(Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto :goto_0

    .line 873
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->business_day_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto :goto_0

    .line 872
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->client_virtual_register_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto :goto_0

    .line 871
    :pswitch_9
    iget-object v3, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->events:Ljava/util/List;

    sget-object v4, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 870
    :pswitch_a
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 869
    :pswitch_b
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 868
    :pswitch_c
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_out_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 867
    :pswitch_d
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_in_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 866
    :pswitch_e
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_refunds_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 865
    :pswitch_f
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_payment_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 864
    :pswitch_10
    sget-object v3, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->starting_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 863
    :pswitch_11
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 862
    :pswitch_12
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ended_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 861
    :pswitch_13
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opened_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 860
    :pswitch_14
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 859
    :pswitch_15
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 858
    :pswitch_16
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 857
    :pswitch_17
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->description(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 856
    :pswitch_18
    iget-object v3, v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->employee_id:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 850
    :pswitch_19
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->state(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    .line 852
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 847
    :pswitch_1a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 846
    :pswitch_1b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    goto/16 :goto_0

    .line 886
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 887
    invoke-virtual {v0}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 769
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$ProtoAdapter_CashDrawerShift;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 809
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->merchant_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 810
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 811
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 812
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->employee_id:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 813
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 814
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee_id:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 815
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee_id:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 816
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee_id:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 817
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 818
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 819
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 820
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->starting_cash_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 821
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_payment_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 822
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 823
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 824
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 825
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 826
    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_cash_money:Lcom/squareup/protos/common/Money;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 827
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->events:Ljava/util/List;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 828
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_virtual_register_id:Ljava/lang/String;

    const/16 v2, 0x14

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 829
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->business_day_id:Ljava/lang/String;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 830
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 831
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 832
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    const/16 v2, 0x18

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 833
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    const/16 v2, 0x19

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 834
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    const/16 v2, 0x1a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 835
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    const/16 v2, 0x1b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 836
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->event_data_omitted:Ljava/lang/Boolean;

    const/16 v2, 0x1c

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 837
    invoke-virtual {p2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 769
    check-cast p2, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$ProtoAdapter_CashDrawerShift;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)I
    .locals 4

    .line 776
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->merchant_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_cash_drawer_shift_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 777
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->state:Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$State;

    const/4 v3, 0x3

    .line 778
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 779
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->employee_id:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->description:Ljava/lang/String;

    const/4 v3, 0x5

    .line 780
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee_id:Ljava/lang/String;

    const/4 v3, 0x6

    .line 781
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee_id:Ljava/lang/String;

    const/4 v3, 0x7

    .line 782
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee_id:Ljava/lang/String;

    const/16 v3, 0x8

    .line 783
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v3, 0x9

    .line 784
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v3, 0xa

    .line 785
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    const/16 v3, 0xb

    .line 786
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->starting_cash_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xc

    .line 787
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_payment_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xd

    .line 788
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xe

    .line 789
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0xf

    .line 790
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x10

    .line 791
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->expected_cash_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x11

    .line 792
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closed_cash_money:Lcom/squareup/protos/common/Money;

    const/16 v3, 0x12

    .line 793
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 794
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->events:Ljava/util/List;

    const/16 v3, 0x13

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->client_virtual_register_id:Ljava/lang/String;

    const/16 v3, 0x14

    .line 795
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->business_day_id:Ljava/lang/String;

    const/16 v3, 0x15

    .line 796
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    const/16 v3, 0x16

    .line 797
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    const/16 v3, 0x17

    .line 798
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    const/16 v3, 0x18

    .line 799
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    const/16 v3, 0x19

    .line 800
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    const/16 v3, 0x1a

    .line 801
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    const/16 v3, 0x1b

    .line 802
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->event_data_omitted:Ljava/lang/Boolean;

    const/16 v3, 0x1c

    .line 803
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 804
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 769
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$ProtoAdapter_CashDrawerShift;->encodedSize(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;
    .locals 2

    .line 892
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;->newBuilder()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 893
    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->description:Ljava/lang/String;

    .line 894
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 895
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 896
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 897
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->starting_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->starting_cash_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->starting_cash_money:Lcom/squareup/protos/common/Money;

    .line 898
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_payment_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_payment_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_payment_money:Lcom/squareup/protos/common/Money;

    .line 899
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_refunds_money:Lcom/squareup/protos/common/Money;

    .line 900
    :cond_5
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_in_money:Lcom/squareup/protos/common/Money;

    .line 901
    :cond_6
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->cash_paid_out_money:Lcom/squareup/protos/common/Money;

    .line 902
    :cond_7
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->expected_cash_money:Lcom/squareup/protos/common/Money;

    .line 903
    :cond_8
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_cash_money:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/protos/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_cash_money:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/Money;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closed_cash_money:Lcom/squareup/protos/common/Money;

    .line 904
    :cond_9
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->events:Ljava/util/List;

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftEvent;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 905
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 906
    :cond_a
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 907
    :cond_b
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    .line 908
    :cond_c
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->opening_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    .line 909
    :cond_d
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->ending_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    .line 910
    :cond_e
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->closing_employee:Lcom/squareup/protos/client/cashdrawers/EmployeeInfo;

    .line 911
    :cond_f
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 912
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 769
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/CashDrawerShift$ProtoAdapter_CashDrawerShift;->redact(Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;)Lcom/squareup/protos/client/cashdrawers/CashDrawerShift;

    move-result-object p1

    return-object p1
.end method
