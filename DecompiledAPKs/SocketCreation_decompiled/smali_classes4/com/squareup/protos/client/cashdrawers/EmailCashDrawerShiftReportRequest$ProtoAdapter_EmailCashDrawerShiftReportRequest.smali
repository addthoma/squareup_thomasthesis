.class final Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$ProtoAdapter_EmailCashDrawerShiftReportRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "EmailCashDrawerShiftReportRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_EmailCashDrawerShiftReportRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 217
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 243
    new-instance v0, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;-><init>()V

    .line 244
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 245
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_5

    const/4 v4, 0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x2

    if-eq v3, v4, :cond_3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    const/4 v4, 0x5

    if-eq v3, v4, :cond_0

    .line 253
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 251
    :cond_0
    sget-object v3, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->rendering_options(Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;

    goto :goto_0

    .line 250
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->email_address(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;

    goto :goto_0

    .line 249
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;

    goto :goto_0

    .line 248
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;

    goto :goto_0

    .line 247
    :cond_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->client_unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;

    goto :goto_0

    .line 257
    :cond_5
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 258
    invoke-virtual {v0}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 215
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$ProtoAdapter_EmailCashDrawerShiftReportRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 233
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_unique_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 234
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->merchant_id:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 235
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 236
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->email_address:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 237
    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 238
    invoke-virtual {p2}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 215
    check-cast p2, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$ProtoAdapter_EmailCashDrawerShiftReportRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;)I
    .locals 4

    .line 222
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_unique_key:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->merchant_id:Ljava/lang/String;

    const/4 v3, 0x2

    .line 223
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->client_cash_drawer_shift_id:Ljava/lang/String;

    const/4 v3, 0x3

    .line 224
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->email_address:Ljava/lang/String;

    const/4 v3, 0x4

    .line 225
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    const/4 v3, 0x5

    .line 226
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 215
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$ProtoAdapter_EmailCashDrawerShiftReportRequest;->encodedSize(Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;
    .locals 2

    .line 263
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;->newBuilder()Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 264
    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->email_address:Ljava/lang/String;

    .line 265
    iget-object v0, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    iput-object v0, p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->rendering_options:Lcom/squareup/protos/client/cashdrawers/CashDrawerShiftReportRenderingOptions;

    .line 266
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 267
    invoke-virtual {p1}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 215
    check-cast p1, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest$ProtoAdapter_EmailCashDrawerShiftReportRequest;->redact(Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;)Lcom/squareup/protos/client/cashdrawers/EmailCashDrawerShiftReportRequest;

    move-result-object p1

    return-object p1
.end method
