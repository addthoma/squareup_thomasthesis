.class public final Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateCashDrawerShiftRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;",
        "Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public business_day_id:Ljava/lang/String;

.field public client_cash_drawer_shift_id:Ljava/lang/String;

.field public client_unique_key:Ljava/lang/String;

.field public client_virtual_register_id:Ljava/lang/String;

.field public device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

.field public merchant_id:Ljava/lang/String;

.field public opened_at:Lcom/squareup/protos/client/ISO8601Date;

.field public opening_employee_id:Ljava/lang/String;

.field public starting_cash_money:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 213
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;
    .locals 12

    .line 272
    new-instance v11, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->client_unique_key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->merchant_id:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->opening_employee_id:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v6, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->starting_cash_money:Lcom/squareup/protos/common/Money;

    iget-object v7, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->client_virtual_register_id:Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->business_day_id:Ljava/lang/String;

    iget-object v9, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/cashdrawers/DeviceInfo;Lokio/ByteString;)V

    return-object v11
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 194
    invoke-virtual {p0}, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->build()Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest;

    move-result-object v0

    return-object v0
.end method

.method public business_day_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 258
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->business_day_id:Ljava/lang/String;

    return-object p0
.end method

.method public client_cash_drawer_shift_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 233
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->client_cash_drawer_shift_id:Ljava/lang/String;

    return-object p0
.end method

.method public client_unique_key(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->client_unique_key:Ljava/lang/String;

    return-object p0
.end method

.method public client_virtual_register_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->client_virtual_register_id:Ljava/lang/String;

    return-object p0
.end method

.method public device_info(Lcom/squareup/protos/client/cashdrawers/DeviceInfo;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 266
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->device_info:Lcom/squareup/protos/client/cashdrawers/DeviceInfo;

    return-object p0
.end method

.method public merchant_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->merchant_id:Ljava/lang/String;

    return-object p0
.end method

.method public opened_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->opened_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public opening_employee_id(Ljava/lang/String;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->opening_employee_id:Ljava/lang/String;

    return-object p0
.end method

.method public starting_cash_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/squareup/protos/client/cashdrawers/CreateCashDrawerShiftRequest$Builder;->starting_cash_money:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
