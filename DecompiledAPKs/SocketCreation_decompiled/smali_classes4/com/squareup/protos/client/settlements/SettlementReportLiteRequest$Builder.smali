.class public final Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SettlementReportLiteRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public batch_request:Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;

.field public request_params:Lcom/squareup/protos/client/settlements/RequestParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 92
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public batch_request(Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;
    .locals 0

    .line 101
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->batch_request:Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;
    .locals 4

    .line 107
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->request_params:Lcom/squareup/protos/client/settlements/RequestParams;

    iget-object v2, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->batch_request:Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;-><init>(Lcom/squareup/protos/client/settlements/RequestParams;Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$BatchRequest;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 87
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest;

    move-result-object v0

    return-object v0
.end method

.method public request_params(Lcom/squareup/protos/client/settlements/RequestParams;)Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteRequest$Builder;->request_params:Lcom/squareup/protos/client/settlements/RequestParams;

    return-object p0
.end method
