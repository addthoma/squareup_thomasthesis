.class public final enum Lcom/squareup/protos/client/settlements/SettlementType;
.super Ljava/lang/Enum;
.source "SettlementType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/settlements/SettlementType$ProtoAdapter_SettlementType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/settlements/SettlementType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/settlements/SettlementType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/settlements/SettlementType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DEBIT_CARD_PAY_IN:Lcom/squareup/protos/client/settlements/SettlementType;

.field public static final enum MANUAL_INSTANT:Lcom/squareup/protos/client/settlements/SettlementType;

.field public static final enum SCHEDULED_INSTANT:Lcom/squareup/protos/client/settlements/SettlementType;

.field public static final enum SETTLEMENT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/settlements/SettlementType;

.field public static final enum STANDARD:Lcom/squareup/protos/client/settlements/SettlementType;

.field public static final enum ZERO_AMOUNT_SETTLEMENT:Lcom/squareup/protos/client/settlements/SettlementType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 14
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementType;

    const/4 v1, 0x0

    const-string v2, "SETTLEMENT_TYPE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/settlements/SettlementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/settlements/SettlementType;->SETTLEMENT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/settlements/SettlementType;

    .line 19
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementType;

    const/4 v2, 0x1

    const-string v3, "ZERO_AMOUNT_SETTLEMENT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/settlements/SettlementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/settlements/SettlementType;->ZERO_AMOUNT_SETTLEMENT:Lcom/squareup/protos/client/settlements/SettlementType;

    .line 24
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementType;

    const/4 v3, 0x2

    const-string v4, "STANDARD"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/settlements/SettlementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/settlements/SettlementType;->STANDARD:Lcom/squareup/protos/client/settlements/SettlementType;

    .line 29
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementType;

    const/4 v4, 0x3

    const-string v5, "SCHEDULED_INSTANT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/settlements/SettlementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/settlements/SettlementType;->SCHEDULED_INSTANT:Lcom/squareup/protos/client/settlements/SettlementType;

    .line 34
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementType;

    const/4 v5, 0x4

    const-string v6, "MANUAL_INSTANT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/settlements/SettlementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/settlements/SettlementType;->MANUAL_INSTANT:Lcom/squareup/protos/client/settlements/SettlementType;

    .line 39
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementType;

    const/4 v6, 0x5

    const-string v7, "DEBIT_CARD_PAY_IN"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/settlements/SettlementType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/settlements/SettlementType;->DEBIT_CARD_PAY_IN:Lcom/squareup/protos/client/settlements/SettlementType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/settlements/SettlementType;

    .line 10
    sget-object v7, Lcom/squareup/protos/client/settlements/SettlementType;->SETTLEMENT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/settlements/SettlementType;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/settlements/SettlementType;->ZERO_AMOUNT_SETTLEMENT:Lcom/squareup/protos/client/settlements/SettlementType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/settlements/SettlementType;->STANDARD:Lcom/squareup/protos/client/settlements/SettlementType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/settlements/SettlementType;->SCHEDULED_INSTANT:Lcom/squareup/protos/client/settlements/SettlementType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/settlements/SettlementType;->MANUAL_INSTANT:Lcom/squareup/protos/client/settlements/SettlementType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/settlements/SettlementType;->DEBIT_CARD_PAY_IN:Lcom/squareup/protos/client/settlements/SettlementType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/settlements/SettlementType;->$VALUES:[Lcom/squareup/protos/client/settlements/SettlementType;

    .line 41
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementType$ProtoAdapter_SettlementType;

    invoke-direct {v0}, Lcom/squareup/protos/client/settlements/SettlementType$ProtoAdapter_SettlementType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/settlements/SettlementType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput p3, p0, Lcom/squareup/protos/client/settlements/SettlementType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/settlements/SettlementType;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 59
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/settlements/SettlementType;->DEBIT_CARD_PAY_IN:Lcom/squareup/protos/client/settlements/SettlementType;

    return-object p0

    .line 58
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/settlements/SettlementType;->MANUAL_INSTANT:Lcom/squareup/protos/client/settlements/SettlementType;

    return-object p0

    .line 57
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/settlements/SettlementType;->SCHEDULED_INSTANT:Lcom/squareup/protos/client/settlements/SettlementType;

    return-object p0

    .line 56
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/settlements/SettlementType;->STANDARD:Lcom/squareup/protos/client/settlements/SettlementType;

    return-object p0

    .line 55
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/settlements/SettlementType;->ZERO_AMOUNT_SETTLEMENT:Lcom/squareup/protos/client/settlements/SettlementType;

    return-object p0

    .line 54
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/settlements/SettlementType;->SETTLEMENT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/settlements/SettlementType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/settlements/SettlementType;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/client/settlements/SettlementType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/settlements/SettlementType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/settlements/SettlementType;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/client/settlements/SettlementType;->$VALUES:[Lcom/squareup/protos/client/settlements/SettlementType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/settlements/SettlementType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/settlements/SettlementType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 66
    iget v0, p0, Lcom/squareup/protos/client/settlements/SettlementType;->value:I

    return v0
.end method
