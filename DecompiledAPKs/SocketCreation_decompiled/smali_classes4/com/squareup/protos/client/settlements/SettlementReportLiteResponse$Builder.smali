.class public final Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SettlementReportLiteResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;",
        "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

.field public settlements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettlementReportLite;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 97
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 98
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->settlements:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public batch_response(Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;)Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;
    .locals 4

    .line 114
    new-instance v0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->settlements:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->batch_response:Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;-><init>(Ljava/util/List;Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$BatchResponse;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 92
    invoke-virtual {p0}, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->build()Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse;

    move-result-object v0

    return-object v0
.end method

.method public settlements(Ljava/util/List;)Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettlementReportLite;",
            ">;)",
            "Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;"
        }
    .end annotation

    .line 102
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/settlements/SettlementReportLiteResponse$Builder;->settlements:Ljava/util/List;

    return-object p0
.end method
