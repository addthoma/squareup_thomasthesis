.class public final enum Lcom/squareup/protos/client/rolodex/ListContactsSortType;
.super Ljava/lang/Enum;
.source "ListContactsSortType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/ListContactsSortType$ProtoAdapter_ListContactsSortType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/rolodex/ListContactsSortType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/rolodex/ListContactsSortType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/ListContactsSortType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CREATED_AT_DESCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

.field public static final enum DISPLAY_NAME_ASCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 11
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "DISPLAY_NAME_ASCENDING"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/protos/client/rolodex/ListContactsSortType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->DISPLAY_NAME_ASCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    .line 13
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    const/4 v3, 0x2

    const-string v4, "CREATED_AT_DESCENDING"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/protos/client/rolodex/ListContactsSortType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->CREATED_AT_DESCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    new-array v0, v3, [Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    .line 10
    sget-object v3, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->DISPLAY_NAME_ASCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->CREATED_AT_DESCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->$VALUES:[Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    .line 15
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListContactsSortType$ProtoAdapter_ListContactsSortType;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ListContactsSortType$ProtoAdapter_ListContactsSortType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput p3, p0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/rolodex/ListContactsSortType;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 29
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->CREATED_AT_DESCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    return-object p0

    .line 28
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->DISPLAY_NAME_ASCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ListContactsSortType;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/rolodex/ListContactsSortType;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->$VALUES:[Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/rolodex/ListContactsSortType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 36
    iget v0, p0, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->value:I

    return v0
.end method
