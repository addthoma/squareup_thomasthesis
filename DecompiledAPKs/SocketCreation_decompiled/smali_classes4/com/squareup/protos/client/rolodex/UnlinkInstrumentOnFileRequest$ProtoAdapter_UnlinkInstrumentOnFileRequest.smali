.class final Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$ProtoAdapter_UnlinkInstrumentOnFileRequest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "UnlinkInstrumentOnFileRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_UnlinkInstrumentOnFileRequest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 155
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 176
    new-instance v0, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;-><init>()V

    .line 177
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 178
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 184
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 182
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;

    goto :goto_0

    .line 181
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->instrument_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;

    goto :goto_0

    .line 180
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->client_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;

    goto :goto_0

    .line 188
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 189
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 153
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$ProtoAdapter_UnlinkInstrumentOnFileRequest;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 168
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;->client_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 169
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;->instrument_token:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 170
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;->contact_token:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 171
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 153
    check-cast p2, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$ProtoAdapter_UnlinkInstrumentOnFileRequest;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;)I
    .locals 4

    .line 160
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;->client_token:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;->instrument_token:Ljava/lang/String;

    const/4 v3, 0x2

    .line 161
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;->contact_token:Ljava/lang/String;

    const/4 v3, 0x3

    .line 162
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 153
    check-cast p1, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$ProtoAdapter_UnlinkInstrumentOnFileRequest;->encodedSize(Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;)Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;
    .locals 0

    .line 194
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;->newBuilder()Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;

    move-result-object p1

    .line 195
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 196
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 153
    check-cast p1, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$ProtoAdapter_UnlinkInstrumentOnFileRequest;->redact(Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;)Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;

    move-result-object p1

    return-object p1
.end method
