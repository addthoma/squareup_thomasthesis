.class public final Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetMergeAllStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;",
        "Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public complete:Ljava/lang/Boolean;

.field public status:Lcom/squareup/protos/client/Status;

.field public total_duplicate_count:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;
    .locals 5

    .line 131
    new-instance v0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;->complete:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;->total_duplicate_count:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/lang/Boolean;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public complete(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;->complete:Ljava/lang/Boolean;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public total_duplicate_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/GetMergeAllStatusResponse$Builder;->total_duplicate_count:Ljava/lang/Integer;

    return-object p0
.end method
