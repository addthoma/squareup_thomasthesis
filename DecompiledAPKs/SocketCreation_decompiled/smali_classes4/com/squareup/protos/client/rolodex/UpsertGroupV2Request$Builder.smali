.class public final Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpsertGroupV2Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request;",
        "Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public group:Lcom/squareup/protos/client/rolodex/GroupV2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request;
    .locals 3

    .line 87
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request$Builder;->group:Lcom/squareup/protos/client/rolodex/GroupV2;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request;-><init>(Lcom/squareup/protos/client/rolodex/GroupV2;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 74
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request$Builder;->build()Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request;

    move-result-object v0

    return-object v0
.end method

.method public group(Lcom/squareup/protos/client/rolodex/GroupV2;)Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request$Builder;
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpsertGroupV2Request$Builder;->group:Lcom/squareup/protos/client/rolodex/GroupV2;

    return-object p0
.end method
