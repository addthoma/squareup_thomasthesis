.class public final Lcom/squareup/protos/client/rolodex/ContactOptions;
.super Lcom/squareup/wire/Message;
.source "ContactOptions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/ContactOptions$ProtoAdapter_ContactOptions;,
        Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/ContactOptions;",
        "Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/ContactOptions;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_INCLUDE_APPLICATION_FIELDS:Ljava/lang/Boolean;

.field public static final DEFAULT_INCLUDE_ATTACHMENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_INCLUDE_ATTRIBUTES:Ljava/lang/Boolean;

.field public static final DEFAULT_INCLUDE_BUYER_SUMMARY:Ljava/lang/Boolean;

.field public static final DEFAULT_INCLUDE_EMAIL_SUBSCRIPTIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_INCLUDE_FREQUENT_ITEMS:Ljava/lang/Boolean;

.field public static final DEFAULT_INCLUDE_GROUPS:Ljava/lang/Boolean;

.field public static final DEFAULT_INCLUDE_INSTRUMENTS_ON_FILE:Ljava/lang/Boolean;

.field public static final DEFAULT_INCLUDE_LOYALTY_APP_DATA:Ljava/lang/Boolean;

.field public static final DEFAULT_INCLUDE_LOYALTY_STATUS:Ljava/lang/Boolean;

.field public static final DEFAULT_INCLUDE_NOTES:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final include_application_fields:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final include_attachments:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final include_attributes:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final include_buyer_summary:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final include_email_subscriptions:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final include_frequent_items:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final include_groups:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final include_instruments_on_file:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final include_loyalty_app_data:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final include_loyalty_status:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final include_notes:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/rolodex/ContactOptions$ProtoAdapter_ContactOptions;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ContactOptions$ProtoAdapter_ContactOptions;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 26
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactOptions;->DEFAULT_INCLUDE_INSTRUMENTS_ON_FILE:Ljava/lang/Boolean;

    .line 28
    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactOptions;->DEFAULT_INCLUDE_LOYALTY_STATUS:Ljava/lang/Boolean;

    .line 30
    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactOptions;->DEFAULT_INCLUDE_NOTES:Ljava/lang/Boolean;

    .line 32
    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactOptions;->DEFAULT_INCLUDE_BUYER_SUMMARY:Ljava/lang/Boolean;

    .line 34
    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactOptions;->DEFAULT_INCLUDE_GROUPS:Ljava/lang/Boolean;

    .line 36
    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactOptions;->DEFAULT_INCLUDE_ATTRIBUTES:Ljava/lang/Boolean;

    .line 38
    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactOptions;->DEFAULT_INCLUDE_EMAIL_SUBSCRIPTIONS:Ljava/lang/Boolean;

    .line 40
    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactOptions;->DEFAULT_INCLUDE_ATTACHMENTS:Ljava/lang/Boolean;

    .line 42
    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactOptions;->DEFAULT_INCLUDE_LOYALTY_APP_DATA:Ljava/lang/Boolean;

    .line 44
    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactOptions;->DEFAULT_INCLUDE_FREQUENT_ITEMS:Ljava/lang/Boolean;

    .line 46
    sput-object v0, Lcom/squareup/protos/client/rolodex/ContactOptions;->DEFAULT_INCLUDE_APPLICATION_FIELDS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 13

    .line 154
    sget-object v12, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/squareup/protos/client/rolodex/ContactOptions;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 162
    sget-object v0, Lcom/squareup/protos/client/rolodex/ContactOptions;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p12}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_instruments_on_file:Ljava/lang/Boolean;

    .line 164
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_loyalty_status:Ljava/lang/Boolean;

    .line 165
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_notes:Ljava/lang/Boolean;

    .line 166
    iput-object p4, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_buyer_summary:Ljava/lang/Boolean;

    .line 167
    iput-object p5, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_groups:Ljava/lang/Boolean;

    .line 168
    iput-object p6, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_attributes:Ljava/lang/Boolean;

    .line 169
    iput-object p7, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_email_subscriptions:Ljava/lang/Boolean;

    .line 170
    iput-object p8, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_attachments:Ljava/lang/Boolean;

    .line 171
    iput-object p9, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_loyalty_app_data:Ljava/lang/Boolean;

    .line 172
    iput-object p10, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_frequent_items:Ljava/lang/Boolean;

    .line 173
    iput-object p11, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_application_fields:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 197
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/ContactOptions;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 198
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/ContactOptions;

    .line 199
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ContactOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/ContactOptions;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_instruments_on_file:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_instruments_on_file:Ljava/lang/Boolean;

    .line 200
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_loyalty_status:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_loyalty_status:Ljava/lang/Boolean;

    .line 201
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_notes:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_notes:Ljava/lang/Boolean;

    .line 202
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_buyer_summary:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_buyer_summary:Ljava/lang/Boolean;

    .line 203
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_groups:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_groups:Ljava/lang/Boolean;

    .line 204
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_attributes:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_attributes:Ljava/lang/Boolean;

    .line 205
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_email_subscriptions:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_email_subscriptions:Ljava/lang/Boolean;

    .line 206
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_attachments:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_attachments:Ljava/lang/Boolean;

    .line 207
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_loyalty_app_data:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_loyalty_app_data:Ljava/lang/Boolean;

    .line 208
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_frequent_items:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_frequent_items:Ljava/lang/Boolean;

    .line 209
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_application_fields:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_application_fields:Ljava/lang/Boolean;

    .line 210
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 215
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 217
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ContactOptions;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 218
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_instruments_on_file:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 219
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_loyalty_status:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_notes:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_buyer_summary:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 222
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_groups:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 223
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_attributes:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 224
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_email_subscriptions:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 225
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_attachments:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 226
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_loyalty_app_data:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 227
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_frequent_items:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 228
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_application_fields:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    .line 229
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;
    .locals 2

    .line 178
    new-instance v0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;-><init>()V

    .line 179
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_instruments_on_file:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_instruments_on_file:Ljava/lang/Boolean;

    .line 180
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_loyalty_status:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_loyalty_status:Ljava/lang/Boolean;

    .line 181
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_notes:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_notes:Ljava/lang/Boolean;

    .line 182
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_buyer_summary:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_buyer_summary:Ljava/lang/Boolean;

    .line 183
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_groups:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_groups:Ljava/lang/Boolean;

    .line 184
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_attributes:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_attributes:Ljava/lang/Boolean;

    .line 185
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_email_subscriptions:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_email_subscriptions:Ljava/lang/Boolean;

    .line 186
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_attachments:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_attachments:Ljava/lang/Boolean;

    .line 187
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_loyalty_app_data:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_loyalty_app_data:Ljava/lang/Boolean;

    .line 188
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_frequent_items:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_frequent_items:Ljava/lang/Boolean;

    .line 189
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_application_fields:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->include_application_fields:Ljava/lang/Boolean;

    .line 190
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ContactOptions;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ContactOptions;->newBuilder()Lcom/squareup/protos/client/rolodex/ContactOptions$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_instruments_on_file:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", include_instruments_on_file="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_instruments_on_file:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 238
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_loyalty_status:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", include_loyalty_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_loyalty_status:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 239
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_notes:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", include_notes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_notes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 240
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_buyer_summary:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", include_buyer_summary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_buyer_summary:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 241
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_groups:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", include_groups="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_groups:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 242
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_attributes:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", include_attributes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_attributes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 243
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_email_subscriptions:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", include_email_subscriptions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_email_subscriptions:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 244
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_attachments:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", include_attachments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_attachments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 245
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_loyalty_app_data:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", include_loyalty_app_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_loyalty_app_data:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 246
    :cond_8
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_frequent_items:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", include_frequent_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_frequent_items:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 247
    :cond_9
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_application_fields:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", include_application_fields="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactOptions;->include_application_fields:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ContactOptions{"

    .line 248
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
