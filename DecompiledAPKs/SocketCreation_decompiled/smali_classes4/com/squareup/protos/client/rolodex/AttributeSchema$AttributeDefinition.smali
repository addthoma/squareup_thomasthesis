.class public final Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;
.super Lcom/squareup/wire/Message;
.source "AttributeSchema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/AttributeSchema;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AttributeDefinition"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$ProtoAdapter_AttributeDefinition;,
        Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENUM_ALLOW_MULTIPLE:Ljava/lang/Boolean;

.field public static final DEFAULT_IS_CONFIGURABLE:Ljava/lang/Boolean;

.field public static final DEFAULT_IS_DEFAULT:Ljava/lang/Boolean;

.field public static final DEFAULT_IS_READ_ONLY:Ljava/lang/Boolean;

.field public static final DEFAULT_IS_VISIBLE_IN_PROFILE:Ljava/lang/Boolean;

.field public static final DEFAULT_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final enum_allow_multiple:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final enum_values:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final is_configurable:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final is_default:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final is_read_only:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final is_visible_in_profile:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.rolodex.AttributeSchema$Type#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 217
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$ProtoAdapter_AttributeDefinition;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$ProtoAdapter_AttributeDefinition;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 225
    sget-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->UNKNOWN:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->DEFAULT_TYPE:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    const/4 v0, 0x0

    .line 227
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->DEFAULT_IS_VISIBLE_IN_PROFILE:Ljava/lang/Boolean;

    .line 229
    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->DEFAULT_IS_READ_ONLY:Ljava/lang/Boolean;

    .line 231
    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->DEFAULT_ENUM_ALLOW_MULTIPLE:Ljava/lang/Boolean;

    .line 233
    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->DEFAULT_IS_DEFAULT:Ljava/lang/Boolean;

    .line 235
    sput-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->DEFAULT_IS_CONFIGURABLE:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 318
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 324
    sget-object v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 325
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    .line 326
    iput-object p2, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    .line 327
    iput-object p3, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 328
    iput-object p4, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    .line 329
    iput-object p5, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    .line 330
    iput-object p6, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_allow_multiple:Ljava/lang/Boolean;

    const-string p1, "enum_values"

    .line 331
    invoke-static {p1, p7}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_values:Ljava/util/List;

    .line 332
    iput-object p8, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_default:Ljava/lang/Boolean;

    .line 333
    iput-object p9, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_configurable:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 355
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 356
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    .line 357
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    .line 358
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    .line 359
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 360
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    .line 361
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    .line 362
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_allow_multiple:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_allow_multiple:Ljava/lang/Boolean;

    .line 363
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_values:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_values:Ljava/util/List;

    .line 364
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_default:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_default:Ljava/lang/Boolean;

    .line 365
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_configurable:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_configurable:Ljava/lang/Boolean;

    .line 366
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 371
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_8

    .line 373
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 374
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 375
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 376
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 377
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 378
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 379
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_allow_multiple:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 380
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 381
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_default:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 382
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_configurable:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 383
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;
    .locals 2

    .line 338
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;-><init>()V

    .line 339
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->key:Ljava/lang/String;

    .line 340
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->name:Ljava/lang/String;

    .line 341
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    .line 342
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->is_visible_in_profile:Ljava/lang/Boolean;

    .line 343
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->is_read_only:Ljava/lang/Boolean;

    .line 344
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_allow_multiple:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->enum_allow_multiple:Ljava/lang/Boolean;

    .line 345
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_values:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->enum_values:Ljava/util/List;

    .line 346
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_default:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->is_default:Ljava/lang/Boolean;

    .line 347
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_configurable:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->is_configurable:Ljava/lang/Boolean;

    .line 348
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 216
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->newBuilder()Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 390
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 391
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 392
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    if-eqz v1, :cond_2

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->type:Lcom/squareup/protos/client/rolodex/AttributeSchema$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 394
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", is_visible_in_profile="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 395
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", is_read_only="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 396
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_allow_multiple:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", enum_allow_multiple="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_allow_multiple:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 397
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, ", enum_values="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->enum_values:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 398
    :cond_6
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_default:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", is_default="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_default:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 399
    :cond_7
    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_configurable:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", is_configurable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_configurable:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AttributeDefinition{"

    .line 400
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
