.class public final Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListMergeProposalRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest;",
        "Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public limit:Ljava/lang/Integer;

.field public paging_key:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 104
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest;
    .locals 4

    .line 125
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;->limit:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;->paging_key:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest;-><init>(Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 99
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest;

    move-result-object v0

    return-object v0
.end method

.method public limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;->limit:Ljava/lang/Integer;

    return-object p0
.end method

.method public paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalRequest$Builder;->paging_key:Ljava/lang/String;

    return-object p0
.end method
