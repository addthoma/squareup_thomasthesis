.class public final Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UnlinkInstrumentOnFileRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;",
        "Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_token:Ljava/lang/String;

.field public contact_token:Ljava/lang/String;

.field public instrument_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 120
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;
    .locals 5

    .line 149
    new-instance v0, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->client_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->instrument_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->contact_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 113
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;
    .locals 0

    .line 127
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->client_token:Ljava/lang/String;

    return-object p0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;
    .locals 0

    .line 143
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public instrument_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UnlinkInstrumentOnFileRequest$Builder;->instrument_token:Ljava/lang/String;

    return-object p0
.end method
