.class public final Lcom/squareup/protos/client/rolodex/ListGroupsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListGroupsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ListGroupsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ListGroupsRequest;",
        "Lcom/squareup/protos/client/rolodex/ListGroupsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public group_options:Lcom/squareup/protos/client/rolodex/GroupOptions;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 83
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ListGroupsRequest;
    .locals 3

    .line 96
    new-instance v0, Lcom/squareup/protos/client/rolodex/ListGroupsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListGroupsRequest$Builder;->group_options:Lcom/squareup/protos/client/rolodex/GroupOptions;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/rolodex/ListGroupsRequest;-><init>(Lcom/squareup/protos/client/rolodex/GroupOptions;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 80
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListGroupsRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/ListGroupsRequest;

    move-result-object v0

    return-object v0
.end method

.method public group_options(Lcom/squareup/protos/client/rolodex/GroupOptions;)Lcom/squareup/protos/client/rolodex/ListGroupsRequest$Builder;
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListGroupsRequest$Builder;->group_options:Lcom/squareup/protos/client/rolodex/GroupOptions;

    return-object p0
.end method
