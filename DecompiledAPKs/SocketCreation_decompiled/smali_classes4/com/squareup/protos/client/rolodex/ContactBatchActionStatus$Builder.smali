.class public final Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ContactBatchActionStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;",
        "Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public total_error_count:Ljava/lang/Integer;

.field public total_success_count:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;
    .locals 4

    .line 114
    new-instance v0, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;->total_success_count:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;->total_error_count:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;->build()Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus;

    move-result-object v0

    return-object v0
.end method

.method public total_error_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;->total_error_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public total_success_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;
    .locals 0

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ContactBatchActionStatus$Builder;->total_success_count:Ljava/lang/Integer;

    return-object p0
.end method
