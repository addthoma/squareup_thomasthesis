.class final Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$ProtoAdapter_Data;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AttributeSchema.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Data"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1049
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1082
    new-instance v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;-><init>()V

    .line 1083
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1084
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 1096
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1094
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->date(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    goto :goto_0

    .line 1093
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    goto :goto_0

    .line 1092
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->email(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    goto :goto_0

    .line 1091
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->phone(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    goto :goto_0

    .line 1090
    :pswitch_4
    iget-object v3, v0, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->enum_values:Ljava/util/List;

    sget-object v4, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1089
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->number_text(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    goto :goto_0

    .line 1088
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->text(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    goto :goto_0

    .line 1087
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->boolean_(Ljava/lang/Boolean;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    goto :goto_0

    .line 1086
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->number(Ljava/lang/Float;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    goto :goto_0

    .line 1100
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1101
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1047
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$ProtoAdapter_Data;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1068
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number:Ljava/lang/Float;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1069
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->boolean_:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1070
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->text:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1071
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number_text:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1072
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->enum_values:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1073
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->phone:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1074
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->email:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1075
    sget-object v0, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1076
    sget-object v0, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->date:Lcom/squareup/protos/common/time/DateTime;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1077
    invoke-virtual {p2}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1047
    check-cast p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$ProtoAdapter_Data;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)I
    .locals 4

    .line 1054
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->FLOAT:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number:Ljava/lang/Float;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->boolean_:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 1055
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->text:Ljava/lang/String;

    const/4 v3, 0x3

    .line 1056
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->number_text:Ljava/lang/String;

    const/4 v3, 0x4

    .line 1057
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    .line 1058
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->enum_values:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->phone:Ljava/lang/String;

    const/4 v3, 0x6

    .line 1059
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->email:Ljava/lang/String;

    const/4 v3, 0x7

    .line 1060
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/location/GlobalAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    const/16 v3, 0x8

    .line 1061
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/DateTime;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->date:Lcom/squareup/protos/common/time/DateTime;

    const/16 v3, 0x9

    .line 1062
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1063
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1047
    check-cast p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$ProtoAdapter_Data;->encodedSize(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;
    .locals 1

    .line 1106
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->newBuilder()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    move-result-object p1

    const/4 v0, 0x0

    .line 1107
    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->phone:Ljava/lang/String;

    .line 1108
    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->email:Ljava/lang/String;

    .line 1109
    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->address:Lcom/squareup/protos/common/location/GlobalAddress;

    .line 1110
    iput-object v0, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->date:Lcom/squareup/protos/common/time/DateTime;

    .line 1111
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1112
    invoke-virtual {p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1047
    check-cast p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$ProtoAdapter_Data;->redact(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    move-result-object p1

    return-object p1
.end method
