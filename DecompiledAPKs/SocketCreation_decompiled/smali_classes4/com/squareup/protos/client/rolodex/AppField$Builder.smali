.class public final Lcom/squareup/protos/client/rolodex/AppField$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AppField.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/AppField;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/AppField;",
        "Lcom/squareup/protos/client/rolodex/AppField$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public app_id:Ljava/lang/String;

.field public date_value:Lcom/squareup/protos/common/time/DateTime;

.field public int_value:Ljava/lang/Long;

.field public name:Ljava/lang/String;

.field public text_value:Ljava/lang/String;

.field public type:Lcom/squareup/protos/client/rolodex/AppField$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 161
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public app_id(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AppField$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AppField$Builder;->app_id:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/AppField;
    .locals 9

    .line 199
    new-instance v8, Lcom/squareup/protos/client/rolodex/AppField;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AppField$Builder;->app_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/AppField$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/AppField$Builder;->type:Lcom/squareup/protos/client/rolodex/AppField$Type;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/AppField$Builder;->text_value:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/AppField$Builder;->date_value:Lcom/squareup/protos/common/time/DateTime;

    iget-object v6, p0, Lcom/squareup/protos/client/rolodex/AppField$Builder;->int_value:Ljava/lang/Long;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/rolodex/AppField;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/rolodex/AppField$Type;Ljava/lang/String;Lcom/squareup/protos/common/time/DateTime;Ljava/lang/Long;Lokio/ByteString;)V

    return-object v8
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 148
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AppField$Builder;->build()Lcom/squareup/protos/client/rolodex/AppField;

    move-result-object v0

    return-object v0
.end method

.method public date_value(Lcom/squareup/protos/common/time/DateTime;)Lcom/squareup/protos/client/rolodex/AppField$Builder;
    .locals 0

    .line 188
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AppField$Builder;->date_value:Lcom/squareup/protos/common/time/DateTime;

    return-object p0
.end method

.method public int_value(Ljava/lang/Long;)Lcom/squareup/protos/client/rolodex/AppField$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AppField$Builder;->int_value:Ljava/lang/Long;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AppField$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AppField$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public text_value(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AppField$Builder;
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AppField$Builder;->text_value:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/rolodex/AppField$Type;)Lcom/squareup/protos/client/rolodex/AppField$Builder;
    .locals 0

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AppField$Builder;->type:Lcom/squareup/protos/client/rolodex/AppField$Type;

    return-object p0
.end method
