.class public final Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "SetContactForPaymentRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest;",
        "Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_token:Ljava/lang/String;

.field public merchant_token:Ljava/lang/String;

.field public payment_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 111
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest;
    .locals 5

    .line 131
    new-instance v0, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;->payment_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;->contact_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;->merchant_token:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 104
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest;

    move-result-object v0

    return-object v0
.end method

.method public contact_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;
    .locals 0

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method public merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;
    .locals 0

    .line 125
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;->merchant_token:Ljava/lang/String;

    return-object p0
.end method

.method public payment_token(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;
    .locals 0

    .line 115
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/SetContactForPaymentRequest$Builder;->payment_token:Ljava/lang/String;

    return-object p0
.end method
