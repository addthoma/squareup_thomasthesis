.class public final Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "UpdateAttributeSchemaResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse;",
        "Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attribute_schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

.field public status:Lcom/squareup/protos/client/Status;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public attribute_schema(Lcom/squareup/protos/client/rolodex/AttributeSchema;)Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse$Builder;
    .locals 0

    .line 102
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse$Builder;->attribute_schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse;
    .locals 4

    .line 113
    new-instance v0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse$Builder;->attribute_schema:Lcom/squareup/protos/client/rolodex/AttributeSchema;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse;-><init>(Lcom/squareup/protos/client/rolodex/AttributeSchema;Lcom/squareup/protos/client/Status;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 90
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse;

    move-result-object v0

    return-object v0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse$Builder;
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/UpdateAttributeSchemaResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method
