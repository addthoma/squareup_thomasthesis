.class public final Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ListMergeProposalResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;",
        "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public duplicate_contact_count:Ljava/lang/Integer;

.field public merge_proposals:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/MergeProposal;",
            ">;"
        }
    .end annotation
.end field

.field public paging_key:Ljava/lang/String;

.field public status:Lcom/squareup/protos/client/Status;

.field public total_duplicate_count:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 148
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 149
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->merge_proposals:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;
    .locals 8

    .line 186
    new-instance v7, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->merge_proposals:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->paging_key:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->total_duplicate_count:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->duplicate_contact_count:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;-><init>(Lcom/squareup/protos/client/Status;Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 137
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->build()Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse;

    move-result-object v0

    return-object v0
.end method

.method public duplicate_contact_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;
    .locals 0

    .line 180
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->duplicate_contact_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public merge_proposals(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/MergeProposal;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;"
        }
    .end annotation

    .line 158
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 159
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->merge_proposals:Ljava/util/List;

    return-object p0
.end method

.method public paging_key(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->paging_key:Ljava/lang/String;

    return-object p0
.end method

.method public status(Lcom/squareup/protos/client/Status;)Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    return-object p0
.end method

.method public total_duplicate_count(Ljava/lang/Integer;)Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;
    .locals 0

    .line 172
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/ListMergeProposalResponse$Builder;->total_duplicate_count:Ljava/lang/Integer;

    return-object p0
.end method
