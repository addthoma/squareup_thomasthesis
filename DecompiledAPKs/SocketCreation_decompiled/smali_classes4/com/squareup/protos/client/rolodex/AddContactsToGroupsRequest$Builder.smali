.class public final Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AddContactsToGroupsRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest;",
        "Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

.field public group_tokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public groups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 108
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 109
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;->groups:Ljava/util/List;

    .line 110
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;->group_tokens:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest;
    .locals 5

    .line 132
    new-instance v0, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    iget-object v2, p0, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;->groups:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;->group_tokens:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest;-><init>(Lcom/squareup/protos/client/rolodex/ContactSet;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 101
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;->build()Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest;

    move-result-object v0

    return-object v0
.end method

.method public contact_set(Lcom/squareup/protos/client/rolodex/ContactSet;)Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;
    .locals 0

    .line 114
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;->contact_set:Lcom/squareup/protos/client/rolodex/ContactSet;

    return-object p0
.end method

.method public group_tokens(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;"
        }
    .end annotation

    .line 125
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 126
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;->group_tokens:Ljava/util/List;

    return-object p0
.end method

.method public groups(Ljava/util/List;)Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;)",
            "Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;"
        }
    .end annotation

    .line 119
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 120
    iput-object p1, p0, Lcom/squareup/protos/client/rolodex/AddContactsToGroupsRequest$Builder;->groups:Ljava/util/List;

    return-object p0
.end method
