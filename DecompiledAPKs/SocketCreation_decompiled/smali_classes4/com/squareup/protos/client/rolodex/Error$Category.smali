.class public final enum Lcom/squareup/protos/client/rolodex/Error$Category;
.super Ljava/lang/Enum;
.source "Error.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Category"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/Error$Category$ProtoAdapter_Category;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/rolodex/Error$Category;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/rolodex/Error$Category;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/Error$Category;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum SUCCESS:Lcom/squareup/protos/client/rolodex/Error$Category;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 181
    new-instance v0, Lcom/squareup/protos/client/rolodex/Error$Category;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "SUCCESS"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/protos/client/rolodex/Error$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Error$Category;->SUCCESS:Lcom/squareup/protos/client/rolodex/Error$Category;

    new-array v0, v1, [Lcom/squareup/protos/client/rolodex/Error$Category;

    .line 180
    sget-object v1, Lcom/squareup/protos/client/rolodex/Error$Category;->SUCCESS:Lcom/squareup/protos/client/rolodex/Error$Category;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/rolodex/Error$Category;->$VALUES:[Lcom/squareup/protos/client/rolodex/Error$Category;

    .line 183
    new-instance v0, Lcom/squareup/protos/client/rolodex/Error$Category$ProtoAdapter_Category;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Error$Category$ProtoAdapter_Category;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Error$Category;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 187
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 188
    iput p3, p0, Lcom/squareup/protos/client/rolodex/Error$Category;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/rolodex/Error$Category;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 196
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/rolodex/Error$Category;->SUCCESS:Lcom/squareup/protos/client/rolodex/Error$Category;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Error$Category;
    .locals 1

    .line 180
    const-class v0, Lcom/squareup/protos/client/rolodex/Error$Category;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/rolodex/Error$Category;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/rolodex/Error$Category;
    .locals 1

    .line 180
    sget-object v0, Lcom/squareup/protos/client/rolodex/Error$Category;->$VALUES:[Lcom/squareup/protos/client/rolodex/Error$Category;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/rolodex/Error$Category;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/rolodex/Error$Category;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 203
    iget v0, p0, Lcom/squareup/protos/client/rolodex/Error$Category;->value:I

    return v0
.end method
