.class public final enum Lcom/squareup/protos/client/rolodex/Filter$Type;
.super Ljava/lang/Enum;
.source "Filter.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/rolodex/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/rolodex/Filter$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/rolodex/Filter$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/rolodex/Filter$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CREATION_SOURCE_FILTER:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum CUSTOM_ATTRIBUTE_BOOLEAN:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum CUSTOM_ATTRIBUTE_EMAIL:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum CUSTOM_ATTRIBUTE_ENUM:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum CUSTOM_ATTRIBUTE_PHONE:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum CUSTOM_ATTRIBUTE_TEXT:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum FEEDBACK:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum HAS_CARD_ON_FILE:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum HAS_LOYALTY:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum HAS_VISITED_LOCATION:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum IS_INSTANT_PROFILE:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum LAST_PAYMENT_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum NO_PAYMENT_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum REACHABLE:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum SEARCH_QUERY:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/rolodex/Filter$Type;

.field public static final enum X_PAYMENTS_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1396
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->UNKNOWN:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1398
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v2, 0x1

    const-string v3, "MANUAL_GROUP"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1400
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v3, 0x2

    const-string v4, "X_PAYMENTS_IN_LAST_Y_DAYS"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->X_PAYMENTS_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1402
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v4, 0x3

    const-string v5, "LAST_PAYMENT_IN_LAST_Y_DAYS"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->LAST_PAYMENT_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1404
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v5, 0x4

    const-string v6, "NO_PAYMENT_IN_LAST_Y_DAYS"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->NO_PAYMENT_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1406
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v6, 0x5

    const-string v7, "REACHABLE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->REACHABLE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1408
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v7, 0x6

    const-string v8, "CUSTOM_ATTRIBUTE_TEXT"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_TEXT:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1410
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/4 v8, 0x7

    const-string v9, "HAS_CARD_ON_FILE"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_CARD_ON_FILE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1412
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v9, 0x8

    const-string v10, "CUSTOM_ATTRIBUTE_PHONE"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_PHONE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1414
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v10, 0x9

    const-string v11, "CUSTOM_ATTRIBUTE_EMAIL"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_EMAIL:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1416
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v11, 0xa

    const-string v12, "CUSTOM_ATTRIBUTE_BOOLEAN"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_BOOLEAN:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1418
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v12, 0xb

    const-string v13, "CUSTOM_ATTRIBUTE_ENUM"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_ENUM:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1420
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v13, 0xc

    const-string v14, "FEEDBACK"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->FEEDBACK:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1422
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v14, 0xd

    const-string v15, "SEARCH_QUERY"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->SEARCH_QUERY:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1424
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v15, 0xe

    const-string v14, "HAS_LOYALTY"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_LOYALTY:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1426
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const-string v14, "HAS_VISITED_LOCATION"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_VISITED_LOCATION:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1428
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const-string v13, "CREATION_SOURCE_FILTER"

    const/16 v14, 0x10

    const/16 v15, 0x10

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->CREATION_SOURCE_FILTER:Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1430
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    const-string v13, "IS_INSTANT_PROFILE"

    const/16 v14, 0x11

    const/16 v15, 0x11

    invoke-direct {v0, v13, v14, v15}, Lcom/squareup/protos/client/rolodex/Filter$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->IS_INSTANT_PROFILE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v0, 0x12

    new-array v0, v0, [Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1395
    sget-object v13, Lcom/squareup/protos/client/rolodex/Filter$Type;->UNKNOWN:Lcom/squareup/protos/client/rolodex/Filter$Type;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/Filter$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->X_PAYMENTS_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->LAST_PAYMENT_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->NO_PAYMENT_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->REACHABLE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_TEXT:Lcom/squareup/protos/client/rolodex/Filter$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_CARD_ON_FILE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_PHONE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_EMAIL:Lcom/squareup/protos/client/rolodex/Filter$Type;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_BOOLEAN:Lcom/squareup/protos/client/rolodex/Filter$Type;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_ENUM:Lcom/squareup/protos/client/rolodex/Filter$Type;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->FEEDBACK:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->SEARCH_QUERY:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_LOYALTY:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_VISITED_LOCATION:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->CREATION_SOURCE_FILTER:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/rolodex/Filter$Type;->IS_INSTANT_PROFILE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->$VALUES:[Lcom/squareup/protos/client/rolodex/Filter$Type;

    .line 1432
    new-instance v0, Lcom/squareup/protos/client/rolodex/Filter$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/protos/client/rolodex/Filter$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1436
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1437
    iput p3, p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/rolodex/Filter$Type;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 1462
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->IS_INSTANT_PROFILE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1461
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->CREATION_SOURCE_FILTER:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1460
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_VISITED_LOCATION:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1459
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_LOYALTY:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1458
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->SEARCH_QUERY:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1457
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->FEEDBACK:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1456
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_ENUM:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1455
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_BOOLEAN:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1454
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_EMAIL:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1453
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_PHONE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1452
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->HAS_CARD_ON_FILE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1451
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->CUSTOM_ATTRIBUTE_TEXT:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1450
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->REACHABLE:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1449
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->NO_PAYMENT_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1448
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->LAST_PAYMENT_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1447
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->X_PAYMENTS_IN_LAST_Y_DAYS:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1446
    :pswitch_10
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->MANUAL_GROUP:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    .line 1445
    :pswitch_11
    sget-object p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->UNKNOWN:Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Filter$Type;
    .locals 1

    .line 1395
    const-class v0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/rolodex/Filter$Type;
    .locals 1

    .line 1395
    sget-object v0, Lcom/squareup/protos/client/rolodex/Filter$Type;->$VALUES:[Lcom/squareup/protos/client/rolodex/Filter$Type;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/rolodex/Filter$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/rolodex/Filter$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1469
    iget v0, p0, Lcom/squareup/protos/client/rolodex/Filter$Type;->value:I

    return v0
.end method
