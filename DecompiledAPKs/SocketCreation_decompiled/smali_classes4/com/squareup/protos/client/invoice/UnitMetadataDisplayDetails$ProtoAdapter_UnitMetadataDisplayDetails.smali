.class final Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$ProtoAdapter_UnitMetadataDisplayDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "UnitMetadataDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_UnitMetadataDisplayDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 612
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 639
    new-instance v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;-><init>()V

    .line 640
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 641
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 650
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 648
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->has_estimates(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    goto :goto_0

    .line 647
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->invoice_automatic_reminder_settings(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    goto :goto_0

    .line 646
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->limits(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    goto :goto_0

    .line 645
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/invoice/UnitAnalytics;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/UnitAnalytics;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->analytics(Lcom/squareup/protos/client/invoice/UnitAnalytics;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    goto :goto_0

    .line 644
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->has_invoices(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    goto :goto_0

    .line 643
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/invoice/UnitMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/UnitMetadata;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->unit_metadata(Lcom/squareup/protos/client/invoice/UnitMetadata;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    goto :goto_0

    .line 654
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 655
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 610
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$ProtoAdapter_UnitMetadataDisplayDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 628
    sget-object v0, Lcom/squareup/protos/client/invoice/UnitMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 629
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_invoices:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 630
    sget-object v0, Lcom/squareup/protos/client/invoice/UnitAnalytics;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 631
    sget-object v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 632
    sget-object v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 633
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_estimates:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 634
    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 610
    check-cast p2, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$ProtoAdapter_UnitMetadataDisplayDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)I
    .locals 4

    .line 617
    sget-object v0, Lcom/squareup/protos/client/invoice/UnitMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_invoices:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 618
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/UnitAnalytics;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    const/4 v3, 0x3

    .line 619
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    const/4 v3, 0x4

    .line 620
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    const/4 v3, 0x5

    .line 621
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->has_estimates:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 622
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 623
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 610
    check-cast p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$ProtoAdapter_UnitMetadataDisplayDetails;->encodedSize(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;
    .locals 2

    .line 660
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;->newBuilder()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;

    move-result-object p1

    .line 661
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/invoice/UnitMetadata;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/UnitMetadata;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->unit_metadata:Lcom/squareup/protos/client/invoice/UnitMetadata;

    .line 662
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/invoice/UnitAnalytics;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/UnitAnalytics;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->analytics:Lcom/squareup/protos/client/invoice/UnitAnalytics;

    .line 663
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->limits:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceFileAttachmentsLimits;

    .line 664
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->invoice_automatic_reminder_settings:Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$InvoiceAutomaticReminderSettings;

    .line 665
    :cond_3
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 666
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$Builder;->build()Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 610
    check-cast p1, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails$ProtoAdapter_UnitMetadataDisplayDetails;->redact(Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;)Lcom/squareup/protos/client/invoice/UnitMetadataDisplayDetails;

    move-result-object p1

    return-object p1
.end method
