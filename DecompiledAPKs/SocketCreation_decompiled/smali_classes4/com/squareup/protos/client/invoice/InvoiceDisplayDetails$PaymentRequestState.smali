.class public final Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;
.super Lcom/squareup/wire/Message;
.source "InvoiceDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PaymentRequestState"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$ProtoAdapter_PaymentRequestState;,
        Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final completed_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final requested_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 759
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$ProtoAdapter_PaymentRequestState;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$ProtoAdapter_PaymentRequestState;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 1

    .line 798
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 803
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 804
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->token:Ljava/lang/String;

    .line 805
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->completed_amount:Lcom/squareup/protos/common/Money;

    .line 806
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->requested_amount:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 822
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 823
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;

    .line 824
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->token:Ljava/lang/String;

    .line 825
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->completed_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->completed_amount:Lcom/squareup/protos/common/Money;

    .line 826
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->requested_amount:Lcom/squareup/protos/common/Money;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->requested_amount:Lcom/squareup/protos/common/Money;

    .line 827
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 832
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 834
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 835
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 836
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->completed_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 837
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->requested_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 838
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;
    .locals 2

    .line 811
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;-><init>()V

    .line 812
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;->token:Ljava/lang/String;

    .line 813
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->completed_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;->completed_amount:Lcom/squareup/protos/common/Money;

    .line 814
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->requested_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;->requested_amount:Lcom/squareup/protos/common/Money;

    .line 815
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 758
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->newBuilder()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 845
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 846
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 847
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->completed_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", completed_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->completed_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 848
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->requested_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", requested_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;->requested_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PaymentRequestState{"

    .line 849
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
