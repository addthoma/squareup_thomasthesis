.class public final enum Lcom/squareup/protos/client/invoice/StateFilter;
.super Ljava/lang/Enum;
.source "StateFilter.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/StateFilter$ProtoAdapter_StateFilter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/invoice/StateFilter;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/StateFilter;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ALL:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum ALL_OUTSTANDING:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum ALL_SENT:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum ALL_UNSUCCESSFUL:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum ARCHIVED:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum CANCELLED:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum DRAFT:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum FAILED:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum OVERDUE:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum PAID:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum RECURRING:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum REFUNDED:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum SCHEDULED:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum UNDELIVERED:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum UNKNOWN:Lcom/squareup/protos/client/invoice/StateFilter;

.field public static final enum UNPAID:Lcom/squareup/protos/client/invoice/StateFilter;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->UNKNOWN:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 16
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v2, 0x1

    const-string v3, "ALL_SENT"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->ALL_SENT:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 21
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v3, 0x2

    const-string v4, "ALL_OUTSTANDING"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->ALL_OUTSTANDING:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 26
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v4, 0x3

    const-string v5, "DRAFT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->DRAFT:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 31
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v5, 0x4

    const-string v6, "UNPAID"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->UNPAID:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 36
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v6, 0x5

    const-string v7, "OVERDUE"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->OVERDUE:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 41
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v7, 0x6

    const-string v8, "PAID"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->PAID:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 46
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/4 v8, 0x7

    const-string v9, "SCHEDULED"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->SCHEDULED:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 51
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/16 v9, 0x8

    const-string v10, "RECURRING"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->RECURRING:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 56
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/16 v10, 0x9

    const-string v11, "REFUNDED"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->REFUNDED:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 61
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/16 v11, 0xa

    const-string v12, "FAILED"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->FAILED:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 66
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/16 v12, 0xb

    const-string v13, "UNDELIVERED"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->UNDELIVERED:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 71
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/16 v13, 0xc

    const-string v14, "CANCELLED"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->CANCELLED:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 76
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/16 v14, 0xd

    const-string v15, "ALL"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->ALL:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 81
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const/16 v15, 0xe

    const-string v14, "ALL_UNSUCCESSFUL"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->ALL_UNSUCCESSFUL:Lcom/squareup/protos/client/invoice/StateFilter;

    .line 86
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter;

    const-string v14, "ARCHIVED"

    const/16 v15, 0xf

    const/16 v13, 0xf

    invoke-direct {v0, v14, v15, v13}, Lcom/squareup/protos/client/invoice/StateFilter;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->ARCHIVED:Lcom/squareup/protos/client/invoice/StateFilter;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/squareup/protos/client/invoice/StateFilter;

    .line 10
    sget-object v13, Lcom/squareup/protos/client/invoice/StateFilter;->UNKNOWN:Lcom/squareup/protos/client/invoice/StateFilter;

    aput-object v13, v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->ALL_SENT:Lcom/squareup/protos/client/invoice/StateFilter;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->ALL_OUTSTANDING:Lcom/squareup/protos/client/invoice/StateFilter;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->DRAFT:Lcom/squareup/protos/client/invoice/StateFilter;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->UNPAID:Lcom/squareup/protos/client/invoice/StateFilter;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->OVERDUE:Lcom/squareup/protos/client/invoice/StateFilter;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->PAID:Lcom/squareup/protos/client/invoice/StateFilter;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->SCHEDULED:Lcom/squareup/protos/client/invoice/StateFilter;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->RECURRING:Lcom/squareup/protos/client/invoice/StateFilter;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->REFUNDED:Lcom/squareup/protos/client/invoice/StateFilter;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->FAILED:Lcom/squareup/protos/client/invoice/StateFilter;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->UNDELIVERED:Lcom/squareup/protos/client/invoice/StateFilter;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->CANCELLED:Lcom/squareup/protos/client/invoice/StateFilter;

    const/16 v2, 0xc

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->ALL:Lcom/squareup/protos/client/invoice/StateFilter;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->ALL_UNSUCCESSFUL:Lcom/squareup/protos/client/invoice/StateFilter;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/StateFilter;->ARCHIVED:Lcom/squareup/protos/client/invoice/StateFilter;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->$VALUES:[Lcom/squareup/protos/client/invoice/StateFilter;

    .line 88
    new-instance v0, Lcom/squareup/protos/client/invoice/StateFilter$ProtoAdapter_StateFilter;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/StateFilter$ProtoAdapter_StateFilter;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 93
    iput p3, p0, Lcom/squareup/protos/client/invoice/StateFilter;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/invoice/StateFilter;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 116
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->ARCHIVED:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 115
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->ALL_UNSUCCESSFUL:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 114
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->ALL:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 113
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->CANCELLED:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 112
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->UNDELIVERED:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 111
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->FAILED:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 110
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->REFUNDED:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 109
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->RECURRING:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 108
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->SCHEDULED:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 107
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->PAID:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 106
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->OVERDUE:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 105
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->UNPAID:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 104
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->DRAFT:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 103
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->ALL_OUTSTANDING:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 102
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->ALL_SENT:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    .line 101
    :pswitch_f
    sget-object p0, Lcom/squareup/protos/client/invoice/StateFilter;->UNKNOWN:Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/StateFilter;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/client/invoice/StateFilter;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/StateFilter;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/invoice/StateFilter;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/client/invoice/StateFilter;->$VALUES:[Lcom/squareup/protos/client/invoice/StateFilter;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/invoice/StateFilter;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/invoice/StateFilter;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 123
    iget v0, p0, Lcom/squareup/protos/client/invoice/StateFilter;->value:I

    return v0
.end method
