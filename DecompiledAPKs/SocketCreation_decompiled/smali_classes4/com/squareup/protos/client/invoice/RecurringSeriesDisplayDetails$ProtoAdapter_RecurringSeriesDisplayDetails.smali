.class final Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$ProtoAdapter_RecurringSeriesDisplayDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "RecurringSeriesDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RecurringSeriesDisplayDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 286
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 315
    new-instance v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;-><init>()V

    .line 316
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 317
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 334
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 332
    :pswitch_0
    sget-object v3, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->start_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;

    goto :goto_0

    .line 331
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/invoice/RecurringSeries;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/invoice/RecurringSeries;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->recurring_series_template(Lcom/squareup/protos/client/invoice/RecurringSeries;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;

    goto :goto_0

    .line 330
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->ended_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;

    goto :goto_0

    .line 329
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->updated_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;

    goto :goto_0

    .line 328
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;

    goto :goto_0

    .line 327
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->sort_date(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;

    goto :goto_0

    .line 321
    :pswitch_6
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    invoke-virtual {v0, v4}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->display_state(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 323
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 338
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 339
    invoke-virtual {v0}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->build()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 284
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$ProtoAdapter_RecurringSeriesDisplayDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 303
    sget-object v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 304
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 305
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 306
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 307
    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 308
    sget-object v0, Lcom/squareup/protos/client/invoice/RecurringSeries;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 309
    sget-object v0, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 310
    invoke-virtual {p2}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 284
    check-cast p2, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$ProtoAdapter_RecurringSeriesDisplayDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)I
    .locals 4

    .line 291
    sget-object v0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x2

    .line 292
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x3

    .line 293
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x4

    .line 294
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    const/4 v3, 0x5

    .line 295
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/RecurringSeries;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    const/4 v3, 0x6

    .line 296
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    const/4 v3, 0x7

    .line 297
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 298
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 284
    check-cast p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$ProtoAdapter_RecurringSeriesDisplayDetails;->encodedSize(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;
    .locals 2

    .line 344
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;->newBuilder()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;

    move-result-object p1

    .line 345
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    .line 346
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 347
    :cond_1
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 348
    :cond_2
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/client/ISO8601Date;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/ISO8601Date;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    .line 349
    :cond_3
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/client/invoice/RecurringSeries;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/invoice/RecurringSeries;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    .line 350
    :cond_4
    iget-object v0, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/common/time/YearMonthDay;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v0, p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 351
    :cond_5
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 352
    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->build()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 284
    check-cast p1, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$ProtoAdapter_RecurringSeriesDisplayDetails;->redact(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object p1

    return-object p1
.end method
