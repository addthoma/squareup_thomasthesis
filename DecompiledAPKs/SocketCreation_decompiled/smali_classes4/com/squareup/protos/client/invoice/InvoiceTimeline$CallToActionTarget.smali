.class public final enum Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;
.super Ljava/lang/Enum;
.source "InvoiceTimeline.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceTimeline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CallToActionTarget"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget$ProtoAdapter_CallToActionTarget;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CONVERT_TO_INVOICE:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

.field public static final enum LINK:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

.field public static final enum SEND_REMINDER:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

.field public static final enum URL:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 136
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->DO_NOT_USE:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    .line 138
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    const/4 v2, 0x1

    const-string v3, "URL"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->URL:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    .line 140
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    const/4 v3, 0x2

    const-string v4, "SEND_REMINDER"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->SEND_REMINDER:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    .line 142
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    const/4 v4, 0x3

    const-string v5, "CONVERT_TO_INVOICE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->CONVERT_TO_INVOICE:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    .line 144
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    const/4 v5, 0x4

    const-string v6, "LINK"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->LINK:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    .line 135
    sget-object v6, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->DO_NOT_USE:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->URL:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->SEND_REMINDER:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->CONVERT_TO_INVOICE:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->LINK:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->$VALUES:[Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    .line 146
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget$ProtoAdapter_CallToActionTarget;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget$ProtoAdapter_CallToActionTarget;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 150
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 151
    iput p3, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 163
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->LINK:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    return-object p0

    .line 162
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->CONVERT_TO_INVOICE:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    return-object p0

    .line 161
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->SEND_REMINDER:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    return-object p0

    .line 160
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->URL:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    return-object p0

    .line 159
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->DO_NOT_USE:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;
    .locals 1

    .line 135
    const-class v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;
    .locals 1

    .line 135
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->$VALUES:[Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 170
    iget v0, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionTarget;->value:I

    return v0
.end method
