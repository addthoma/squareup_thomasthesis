.class public final Lcom/squareup/protos/client/invoice/CloneInvoiceRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CloneInvoiceRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/CloneInvoiceRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/CloneInvoiceRequest;",
        "Lcom/squareup/protos/client/invoice/CloneInvoiceRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public invoice:Lcom/squareup/protos/client/invoice/Invoice;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 80
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/CloneInvoiceRequest;
    .locals 3

    .line 90
    new-instance v0, Lcom/squareup/protos/client/invoice/CloneInvoiceRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/CloneInvoiceRequest$Builder;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/protos/client/invoice/CloneInvoiceRequest;-><init>(Lcom/squareup/protos/client/invoice/Invoice;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 77
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/CloneInvoiceRequest$Builder;->build()Lcom/squareup/protos/client/invoice/CloneInvoiceRequest;

    move-result-object v0

    return-object v0
.end method

.method public invoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/CloneInvoiceRequest$Builder;
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/CloneInvoiceRequest$Builder;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    return-object p0
.end method
