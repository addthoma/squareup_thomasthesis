.class public final Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "RecurringSeriesDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;",
        "Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

.field public ended_at:Lcom/squareup/protos/client/ISO8601Date;

.field public recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

.field public sort_date:Lcom/squareup/protos/client/ISO8601Date;

.field public start_date:Lcom/squareup/protos/common/time/YearMonthDay;

.field public updated_at:Lcom/squareup/protos/client/ISO8601Date;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 181
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;
    .locals 10

    .line 233
    new-instance v9, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v4, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v5, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    iget-object v6, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    iget-object v7, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;-><init>(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/ISO8601Date;Lcom/squareup/protos/client/invoice/RecurringSeries;Lcom/squareup/protos/common/time/YearMonthDay;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 166
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->build()Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails;

    move-result-object v0

    return-object v0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;
    .locals 0

    .line 198
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public display_state(Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;
    .locals 0

    .line 185
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->display_state:Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$DisplayState;

    return-object p0
.end method

.method public ended_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->ended_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public recurring_series_template(Lcom/squareup/protos/client/invoice/RecurringSeries;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->recurring_series_template:Lcom/squareup/protos/client/invoice/RecurringSeries;

    return-object p0
.end method

.method public sort_date(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;
    .locals 0

    .line 193
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public start_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;
    .locals 0

    .line 227
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->start_date:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public updated_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;
    .locals 0

    .line 203
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/RecurringSeriesDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method
