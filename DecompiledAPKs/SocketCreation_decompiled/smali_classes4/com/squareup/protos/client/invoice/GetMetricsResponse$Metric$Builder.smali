.class public final Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetMetricsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;",
        "Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

.field public metric_type:Lcom/squareup/protos/client/invoice/MetricType;

.field public value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 205
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;
    .locals 5

    .line 225
    new-instance v0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;-><init>(Lcom/squareup/protos/client/invoice/MetricType;Lcom/squareup/protos/common/time/DateTimeInterval;Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 198
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric;

    move-result-object v0

    return-object v0
.end method

.method public date_range(Lcom/squareup/protos/common/time/DateTimeInterval;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;
    .locals 0

    .line 214
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->date_range:Lcom/squareup/protos/common/time/DateTimeInterval;

    return-object p0
.end method

.method public metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;
    .locals 0

    .line 209
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->metric_type:Lcom/squareup/protos/client/invoice/MetricType;

    return-object p0
.end method

.method public value(Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;)Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;
    .locals 0

    .line 219
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Builder;->value:Lcom/squareup/protos/client/invoice/GetMetricsResponse$Metric$Value;

    return-object p0
.end method
