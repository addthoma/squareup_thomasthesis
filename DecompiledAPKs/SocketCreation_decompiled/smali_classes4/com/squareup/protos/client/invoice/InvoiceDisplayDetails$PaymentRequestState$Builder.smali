.class public final Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InvoiceDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public completed_amount:Lcom/squareup/protos/common/Money;

.field public requested_amount:Lcom/squareup/protos/common/Money;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 859
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;
    .locals 5

    .line 893
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;->completed_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;->requested_amount:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;-><init>(Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 852
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;

    move-result-object v0

    return-object v0
.end method

.method public completed_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;
    .locals 0

    .line 879
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;->completed_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public requested_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;
    .locals 0

    .line 887
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;->requested_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;
    .locals 0

    .line 866
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
