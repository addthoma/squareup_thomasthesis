.class public final Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;
.super Lcom/squareup/wire/Message;
.source "InvoiceTimeline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceTimeline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CallToActionLink"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$ProtoAdapter_CallToActionLink;,
        Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;,
        Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;",
        "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LINK_TYPE:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

.field public static final DEFAULT_LINK_VALUE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final link_type:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceTimeline$CallToActionLink$CallToActionLinkType#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final link_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 186
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$ProtoAdapter_CallToActionLink;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$ProtoAdapter_CallToActionLink;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 190
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;->DO_NOT_USE:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->DEFAULT_LINK_TYPE:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;Ljava/lang/String;)V
    .locals 1

    .line 207
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;-><init>(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 212
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 213
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_type:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    .line 214
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_value:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 229
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 230
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;

    .line 231
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_type:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_type:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    .line 232
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_value:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_value:Ljava/lang/String;

    .line 233
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 238
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 240
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 241
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_type:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 242
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_value:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 243
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;
    .locals 2

    .line 219
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;-><init>()V

    .line 220
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_type:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;->link_type:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    .line 221
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_value:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;->link_value:Ljava/lang/String;

    .line 222
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 185
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->newBuilder()Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 250
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 251
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_type:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    if-eqz v1, :cond_0

    const-string v1, ", link_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_type:Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink$CallToActionLinkType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 252
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_value:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", link_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTimeline$CallToActionLink;->link_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "CallToActionLink{"

    .line 253
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
