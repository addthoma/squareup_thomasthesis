.class public final Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;
.super Lcom/squareup/wire/Message;
.source "InvoiceReminderInstance.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$ProtoAdapter_InvoiceReminderInstance;,
        Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;,
        Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
        "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CUSTOM_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_RELATIVE_DAYS:Ljava/lang/Integer;

.field public static final DEFAULT_REMINDER_CONFIG_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_STATE:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

.field public static final DEFAULT_TOKEN:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final custom_message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final relative_days:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x5
    .end annotation
.end field

.field public final reminder_config_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final sent_on:Lcom/squareup/protos/common/time/YearMonthDay;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.YearMonthDay#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceReminderInstance$State#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$ProtoAdapter_InvoiceReminderInstance;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$ProtoAdapter_InvoiceReminderInstance;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 33
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;->UNKNOWN:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->DEFAULT_STATE:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    const/4 v0, 0x0

    .line 37
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->DEFAULT_RELATIVE_DAYS:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 8

    .line 97
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;Lcom/squareup/protos/common/time/YearMonthDay;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 102
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 103
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->reminder_config_token:Ljava/lang/String;

    .line 104
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    .line 105
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 106
    iput-object p4, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->custom_message:Ljava/lang/String;

    .line 107
    iput-object p5, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->relative_days:Ljava/lang/Integer;

    .line 108
    iput-object p6, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 127
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 128
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;

    .line 129
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->reminder_config_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->reminder_config_token:Ljava/lang/String;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->custom_message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->custom_message:Ljava/lang/String;

    .line 133
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->relative_days:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->relative_days:Ljava/lang/Integer;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->token:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->token:Ljava/lang/String;

    .line 135
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 140
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_6

    .line 142
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->reminder_config_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/YearMonthDay;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->custom_message:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->relative_days:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->token:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_5
    add-int/2addr v0, v2

    .line 149
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_6
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;
    .locals 2

    .line 113
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;-><init>()V

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->reminder_config_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->reminder_config_token:Ljava/lang/String;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->custom_message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->custom_message:Ljava/lang/String;

    .line 118
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->relative_days:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->relative_days:Ljava/lang/Integer;

    .line 119
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->token:Ljava/lang/String;

    .line 120
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->newBuilder()Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->reminder_config_token:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", reminder_config_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->reminder_config_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    if-eqz v1, :cond_1

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->state:Lcom/squareup/protos/client/invoice/InvoiceReminderInstance$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 159
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    if-eqz v1, :cond_2

    const-string v1, ", sent_on="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->sent_on:Lcom/squareup/protos/common/time/YearMonthDay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 160
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->custom_message:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", custom_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->custom_message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->relative_days:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    const-string v1, ", relative_days="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->relative_days:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 162
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->token:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;->token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InvoiceReminderInstance{"

    .line 163
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
