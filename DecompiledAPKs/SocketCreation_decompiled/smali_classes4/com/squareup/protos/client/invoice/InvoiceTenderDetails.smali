.class public final Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;
.super Lcom/squareup/wire/Message;
.source "InvoiceTenderDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$ProtoAdapter_InvoiceTenderDetails;,
        Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;,
        Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;",
        "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BRAND:Ljava/lang/String; = ""

.field public static final DEFAULT_LAST_FOUR:Ljava/lang/String; = ""

.field public static final DEFAULT_TENDER_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_TENDER_TYPE:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

.field private static final serialVersionUID:J


# instance fields
.field public final brand:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final last_four:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field

.field public final tender_note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.invoice.InvoiceTenderDetails$TenderType#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final tendered_at:Lcom/squareup/protos/common/time/DateTime;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.time.DateTime#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final tip_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final total_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$ProtoAdapter_InvoiceTenderDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$ProtoAdapter_InvoiceTenderDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->CASH:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    sput-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->DEFAULT_TENDER_TYPE:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/DateTime;)V
    .locals 9

    .line 92
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/time/DateTime;Lokio/ByteString;)V
    .locals 1

    .line 98
    sget-object v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 99
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->brand:Ljava/lang/String;

    .line 100
    iput-object p2, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->last_four:Ljava/lang/String;

    .line 101
    iput-object p3, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 102
    iput-object p4, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_note:Ljava/lang/String;

    .line 103
    iput-object p5, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tip_amount:Lcom/squareup/protos/common/Money;

    .line 104
    iput-object p6, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->total_amount:Lcom/squareup/protos/common/Money;

    .line 105
    iput-object p7, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tendered_at:Lcom/squareup/protos/common/time/DateTime;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 125
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 126
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;

    .line 127
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->brand:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->brand:Ljava/lang/String;

    .line 128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->last_four:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->last_four:Ljava/lang/String;

    .line 129
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 130
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_note:Ljava/lang/String;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tip_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tip_amount:Lcom/squareup/protos/common/Money;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->total_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->total_amount:Lcom/squareup/protos/common/Money;

    .line 133
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tendered_at:Lcom/squareup/protos/common/time/DateTime;

    iget-object p1, p1, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tendered_at:Lcom/squareup/protos/common/time/DateTime;

    .line 134
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 139
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 141
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 142
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->brand:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->last_four:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_note:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tip_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->total_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tendered_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/common/time/DateTime;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 149
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;
    .locals 2

    .line 110
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;-><init>()V

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->brand:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->brand:Ljava/lang/String;

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->last_four:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->last_four:Ljava/lang/String;

    .line 113
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    .line 114
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->tender_note:Ljava/lang/String;

    .line 115
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tip_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->tip_amount:Lcom/squareup/protos/common/Money;

    .line 116
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->total_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->total_amount:Lcom/squareup/protos/common/Money;

    .line 117
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tendered_at:Lcom/squareup/protos/common/time/DateTime;

    iput-object v1, v0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->tendered_at:Lcom/squareup/protos/common/time/DateTime;

    .line 118
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->newBuilder()Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 157
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->brand:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", brand="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->brand:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->last_four:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", last_four=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    if-eqz v1, :cond_2

    const-string v1, ", tender_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_type:Lcom/squareup/protos/client/invoice/InvoiceTenderDetails$TenderType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 160
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_note:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", tender_note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tender_note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tip_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", tip_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tip_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 162
    :cond_4
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->total_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", total_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->total_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 163
    :cond_5
    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tendered_at:Lcom/squareup/protos/common/time/DateTime;

    if-eqz v1, :cond_6

    const-string v1, ", tendered_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;->tendered_at:Lcom/squareup/protos/common/time/DateTime;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "InvoiceTenderDetails{"

    .line 164
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
