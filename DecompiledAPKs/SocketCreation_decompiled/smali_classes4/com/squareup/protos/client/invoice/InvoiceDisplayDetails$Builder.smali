.class public final Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InvoiceDisplayDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
        "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public automatic_reminder_instance:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
            ">;"
        }
    .end annotation
.end field

.field public bill_id_pair:Lcom/squareup/protos/client/IdPair;

.field public cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

.field public created_at:Lcom/squareup/protos/client/ISO8601Date;

.field public deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

.field public deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

.field public deprecated_v1_refund:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RefundV1;",
            ">;"
        }
    .end annotation
.end field

.field public display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

.field public event:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceEvent;",
            ">;"
        }
    .end annotation
.end field

.field public first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

.field public invoice:Lcom/squareup/protos/client/invoice/Invoice;

.field public invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

.field public is_archived:Ljava/lang/Boolean;

.field public paid_at:Lcom/squareup/protos/client/ISO8601Date;

.field public payment_request_state:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;",
            ">;"
        }
    .end annotation
.end field

.field public payments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;",
            ">;"
        }
    .end annotation
.end field

.field public refund:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceRefund;",
            ">;"
        }
    .end annotation
.end field

.field public refunded_at:Lcom/squareup/protos/client/ISO8601Date;

.field public sent_at:Lcom/squareup/protos/client/ISO8601Date;

.field public series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

.field public shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

.field public sort_date:Lcom/squareup/protos/client/ISO8601Date;

.field public updated_at:Lcom/squareup/protos/client/ISO8601Date;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 440
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 441
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_v1_refund:Ljava/util/List;

    .line 442
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->event:Ljava/util/List;

    .line 443
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->refund:Ljava/util/List;

    .line 444
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->automatic_reminder_instance:Ljava/util/List;

    .line 445
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->payment_request_state:Ljava/util/List;

    .line 446
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->payments:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public automatic_reminder_instance(Ljava/util/List;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceReminderInstance;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;"
        }
    .end annotation

    .line 585
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 586
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->automatic_reminder_instance:Ljava/util/List;

    return-object p0
.end method

.method public bill_id_pair(Lcom/squareup/protos/client/IdPair;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 524
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->bill_id_pair:Lcom/squareup/protos/client/IdPair;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;
    .locals 2

    .line 630
    new-instance v0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;-><init>(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 393
    invoke-virtual {p0}, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->build()Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    move-result-object v0

    return-object v0
.end method

.method public cancelled_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 492
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->cancelled_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public created_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 468
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->created_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public deprecated_last_edited_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 557
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_last_edited_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public deprecated_last_viewed_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 551
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_last_viewed_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public deprecated_v1_refund(Ljava/util/List;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bills/RefundV1;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 530
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 531
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->deprecated_v1_refund:Ljava/util/List;

    return-object p0
.end method

.method public display_state(Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 516
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->display_state:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$DisplayState;

    return-object p0
.end method

.method public event(Ljava/util/List;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceEvent;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;"
        }
    .end annotation

    .line 544
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 545
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->event:Ljava/util/List;

    return-object p0
.end method

.method public first_sent_date(Lcom/squareup/protos/common/time/YearMonthDay;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 616
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->first_sent_date:Lcom/squareup/protos/common/time/YearMonthDay;

    return-object p0
.end method

.method public invoice(Lcom/squareup/protos/client/invoice/Invoice;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 450
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->invoice:Lcom/squareup/protos/client/invoice/Invoice;

    return-object p0
.end method

.method public invoice_timeline(Lcom/squareup/protos/client/invoice/InvoiceTimeline;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 568
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->invoice_timeline:Lcom/squareup/protos/client/invoice/InvoiceTimeline;

    return-object p0
.end method

.method public is_archived(Ljava/lang/Boolean;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 624
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->is_archived:Ljava/lang/Boolean;

    return-object p0
.end method

.method public paid_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 484
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->paid_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public payment_request_state(Ljava/util/List;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$PaymentRequestState;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;"
        }
    .end annotation

    .line 591
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 592
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->payment_request_state:Ljava/util/List;

    return-object p0
.end method

.method public payments(Ljava/util/List;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceTenderDetails;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;"
        }
    .end annotation

    .line 600
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 601
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->payments:Ljava/util/List;

    return-object p0
.end method

.method public refund(Ljava/util/List;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/InvoiceRefund;",
            ">;)",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;"
        }
    .end annotation

    .line 562
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 563
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->refund:Ljava/util/List;

    return-object p0
.end method

.method public refunded_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 500
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->refunded_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public sent_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 508
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->sent_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public series_details(Lcom/squareup/protos/client/invoice/SeriesDetails;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 539
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->series_details:Lcom/squareup/protos/client/invoice/SeriesDetails;

    return-object p0
.end method

.method public shipping_address(Lcom/squareup/protos/client/invoice/ShippingAddress;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 576
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->shipping_address:Lcom/squareup/protos/client/invoice/ShippingAddress;

    return-object p0
.end method

.method public sort_date(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 460
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->sort_date:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method

.method public updated_at(Lcom/squareup/protos/client/ISO8601Date;)Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;
    .locals 0

    .line 476
    iput-object p1, p0, Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails$Builder;->updated_at:Lcom/squareup/protos/client/ISO8601Date;

    return-object p0
.end method
