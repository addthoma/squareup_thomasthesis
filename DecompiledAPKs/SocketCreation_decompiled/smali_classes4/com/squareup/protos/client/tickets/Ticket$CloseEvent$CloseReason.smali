.class public final enum Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;
.super Ljava/lang/Enum;
.source "Ticket.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tickets/Ticket$CloseEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CloseReason"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason$ProtoAdapter_CloseReason;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CHARGE:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

.field public static final enum CLOSED_FOR_INCOMPATIBLE_CLIENT:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

.field public static final enum DELETION:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

.field public static final enum INVALID:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

.field public static final enum INVOICE_SENT:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

.field public static final enum OFFLINE_CHARGE:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 563
    new-instance v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    const/4 v1, 0x0

    const-string v2, "INVALID"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->INVALID:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    .line 568
    new-instance v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    const/4 v2, 0x1

    const-string v3, "DELETION"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->DELETION:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    .line 573
    new-instance v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    const/4 v3, 0x2

    const-string v4, "CHARGE"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->CHARGE:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    .line 578
    new-instance v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    const/4 v4, 0x3

    const-string v5, "OFFLINE_CHARGE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->OFFLINE_CHARGE:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    .line 583
    new-instance v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    const/4 v5, 0x4

    const-string v6, "INVOICE_SENT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->INVOICE_SENT:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    .line 588
    new-instance v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    const/4 v6, 0x5

    const-string v7, "CLOSED_FOR_INCOMPATIBLE_CLIENT"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->CLOSED_FOR_INCOMPATIBLE_CLIENT:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    .line 559
    sget-object v7, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->INVALID:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->DELETION:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->CHARGE:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->OFFLINE_CHARGE:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->INVOICE_SENT:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->CLOSED_FOR_INCOMPATIBLE_CLIENT:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->$VALUES:[Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    .line 590
    new-instance v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason$ProtoAdapter_CloseReason;

    invoke-direct {v0}, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason$ProtoAdapter_CloseReason;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 594
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 595
    iput p3, p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;
    .locals 1

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 608
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->CLOSED_FOR_INCOMPATIBLE_CLIENT:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    return-object p0

    .line 607
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->INVOICE_SENT:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    return-object p0

    .line 606
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->OFFLINE_CHARGE:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    return-object p0

    .line 605
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->CHARGE:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    return-object p0

    .line 604
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->DELETION:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    return-object p0

    .line 603
    :cond_5
    sget-object p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->INVALID:Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;
    .locals 1

    .line 559
    const-class v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;
    .locals 1

    .line 559
    sget-object v0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->$VALUES:[Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 615
    iget v0, p0, Lcom/squareup/protos/client/tickets/Ticket$CloseEvent$CloseReason;->value:I

    return v0
.end method
