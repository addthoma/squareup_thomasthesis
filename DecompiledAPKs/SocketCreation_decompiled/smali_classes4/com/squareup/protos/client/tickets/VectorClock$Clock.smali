.class public final Lcom/squareup/protos/client/tickets/VectorClock$Clock;
.super Lcom/squareup/wire/Message;
.source "VectorClock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/tickets/VectorClock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Clock"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/tickets/VectorClock$Clock$ProtoAdapter_Clock;,
        Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/tickets/VectorClock$Clock;",
        "Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/tickets/VectorClock$Clock;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_VERSION:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 97
    new-instance v0, Lcom/squareup/protos/client/tickets/VectorClock$Clock$ProtoAdapter_Clock;

    invoke-direct {v0}, Lcom/squareup/protos/client/tickets/VectorClock$Clock$ProtoAdapter_Clock;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 103
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->DEFAULT_VERSION:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1

    .line 118
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/tickets/VectorClock$Clock;-><init>(Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1

    .line 122
    sget-object v0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 123
    iput-object p1, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->name:Ljava/lang/String;

    .line 124
    iput-object p2, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->version:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 139
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 140
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/tickets/VectorClock$Clock;

    .line 141
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->name:Ljava/lang/String;

    .line 142
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->version:Ljava/lang/Long;

    iget-object p1, p1, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->version:Ljava/lang/Long;

    .line 143
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 148
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 150
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 151
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->name:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 152
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->version:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 153
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;
    .locals 2

    .line 129
    new-instance v0, Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;-><init>()V

    .line 130
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;->name:Ljava/lang/String;

    .line 131
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;->version:Ljava/lang/Long;

    .line 132
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->newBuilder()Lcom/squareup/protos/client/tickets/VectorClock$Clock$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 161
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->version:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/tickets/VectorClock$Clock;->version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Clock{"

    .line 163
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
