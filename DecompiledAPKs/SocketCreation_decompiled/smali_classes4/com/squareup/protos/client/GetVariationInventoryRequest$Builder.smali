.class public final Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetVariationInventoryRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/GetVariationInventoryRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/GetVariationInventoryRequest;",
        "Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public state:Lcom/squareup/protos/client/State;

.field public unit_token:Ljava/lang/String;

.field public variation_token:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 121
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 122
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;->variation_token:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/GetVariationInventoryRequest;
    .locals 5

    .line 153
    new-instance v0, Lcom/squareup/protos/client/GetVariationInventoryRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;->unit_token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;->state:Lcom/squareup/protos/client/State;

    iget-object v3, p0, Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;->variation_token:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/GetVariationInventoryRequest;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/State;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;->build()Lcom/squareup/protos/client/GetVariationInventoryRequest;

    move-result-object v0

    return-object v0
.end method

.method public state(Lcom/squareup/protos/client/State;)Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;->state:Lcom/squareup/protos/client/State;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method

.method public variation_token(Ljava/util/List;)Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;"
        }
    .end annotation

    .line 146
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/GetVariationInventoryRequest$Builder;->variation_token:Ljava/util/List;

    return-object p0
.end method
