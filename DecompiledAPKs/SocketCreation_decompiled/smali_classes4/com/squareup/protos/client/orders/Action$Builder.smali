.class public final Lcom/squareup/protos/client/orders/Action$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Action.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/Action;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/Action;",
        "Lcom/squareup/protos/client/orders/Action$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public can_apply_to_line_items:Ljava/lang/Boolean;

.field public long_display_name:Ljava/lang/String;

.field public short_display_name:Ljava/lang/String;

.field public type:Lcom/squareup/protos/client/orders/Action$Type;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 156
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/orders/Action;
    .locals 7

    .line 192
    new-instance v6, Lcom/squareup/protos/client/orders/Action;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/Action$Builder;->type:Lcom/squareup/protos/client/orders/Action$Type;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/Action$Builder;->short_display_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/orders/Action$Builder;->long_display_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/orders/Action$Builder;->can_apply_to_line_items:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/protos/client/orders/Action;-><init>(Lcom/squareup/protos/client/orders/Action$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 147
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/Action$Builder;->build()Lcom/squareup/protos/client/orders/Action;

    move-result-object v0

    return-object v0
.end method

.method public can_apply_to_line_items(Ljava/lang/Boolean;)Lcom/squareup/protos/client/orders/Action$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lcom/squareup/protos/client/orders/Action$Builder;->can_apply_to_line_items:Ljava/lang/Boolean;

    return-object p0
.end method

.method public long_display_name(Ljava/lang/String;)Lcom/squareup/protos/client/orders/Action$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 178
    iput-object p1, p0, Lcom/squareup/protos/client/orders/Action$Builder;->long_display_name:Ljava/lang/String;

    return-object p0
.end method

.method public short_display_name(Ljava/lang/String;)Lcom/squareup/protos/client/orders/Action$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 169
    iput-object p1, p0, Lcom/squareup/protos/client/orders/Action$Builder;->short_display_name:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lcom/squareup/protos/client/orders/Action$Type;)Lcom/squareup/protos/client/orders/Action$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lcom/squareup/protos/client/orders/Action$Builder;->type:Lcom/squareup/protos/client/orders/Action$Type;

    return-object p0
.end method
