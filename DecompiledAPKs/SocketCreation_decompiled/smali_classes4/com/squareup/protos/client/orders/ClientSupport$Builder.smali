.class public final Lcom/squareup/protos/client/orders/ClientSupport$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ClientSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/ClientSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/ClientSupport;",
        "Lcom/squareup/protos/client/orders/ClientSupport$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public supported_actions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/Action$Type;",
            ">;"
        }
    .end annotation
.end field

.field public supported_features:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/ClientSupport$Feature;",
            ">;"
        }
    .end annotation
.end field

.field public supported_groups:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 121
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 122
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/orders/ClientSupport$Builder;->supported_features:Ljava/util/List;

    .line 123
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/orders/ClientSupport$Builder;->supported_groups:Ljava/util/List;

    .line 124
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/orders/ClientSupport$Builder;->supported_actions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/orders/ClientSupport;
    .locals 5

    .line 153
    new-instance v0, Lcom/squareup/protos/client/orders/ClientSupport;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/ClientSupport$Builder;->supported_features:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/ClientSupport$Builder;->supported_groups:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/orders/ClientSupport$Builder;->supported_actions:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/orders/ClientSupport;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/ClientSupport$Builder;->build()Lcom/squareup/protos/client/orders/ClientSupport;

    move-result-object v0

    return-object v0
.end method

.method public supported_actions(Ljava/util/List;)Lcom/squareup/protos/client/orders/ClientSupport$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/Action$Type;",
            ">;)",
            "Lcom/squareup/protos/client/orders/ClientSupport$Builder;"
        }
    .end annotation

    .line 146
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/orders/ClientSupport$Builder;->supported_actions:Ljava/util/List;

    return-object p0
.end method

.method public supported_features(Ljava/util/List;)Lcom/squareup/protos/client/orders/ClientSupport$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/ClientSupport$Feature;",
            ">;)",
            "Lcom/squareup/protos/client/orders/ClientSupport$Builder;"
        }
    .end annotation

    .line 128
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/orders/ClientSupport$Builder;->supported_features:Ljava/util/List;

    return-object p0
.end method

.method public supported_groups(Ljava/util/List;)Lcom/squareup/protos/client/orders/ClientSupport$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/OrderGroup;",
            ">;)",
            "Lcom/squareup/protos/client/orders/ClientSupport$Builder;"
        }
    .end annotation

    .line 137
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/orders/ClientSupport$Builder;->supported_groups:Ljava/util/List;

    return-object p0
.end method
