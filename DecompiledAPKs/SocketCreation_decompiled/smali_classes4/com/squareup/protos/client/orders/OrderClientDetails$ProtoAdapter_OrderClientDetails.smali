.class final Lcom/squareup/protos/client/orders/OrderClientDetails$ProtoAdapter_OrderClientDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "OrderClientDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/OrderClientDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_OrderClientDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/protos/client/orders/OrderClientDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 256
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/protos/client/orders/OrderClientDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/orders/OrderClientDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 285
    new-instance v0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;-><init>()V

    .line 286
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 287
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 304
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 302
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->external_link(Ljava/lang/String;)Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;

    goto :goto_0

    .line 301
    :pswitch_1
    sget-object v3, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->order_display_state_data(Lcom/squareup/protos/client/orders/OrderDisplayStateData;)Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;

    goto :goto_0

    .line 300
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->localized_prompt_description(Ljava/lang/String;)Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;

    goto :goto_0

    .line 299
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->localized_prompt_title(Ljava/lang/String;)Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;

    goto :goto_0

    .line 298
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->fulfillments_display_name(Ljava/lang/String;)Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;

    goto :goto_0

    .line 297
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/client/orders/Channel;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/orders/Channel;

    invoke-virtual {v0, v3}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->channel(Lcom/squareup/protos/client/orders/Channel;)Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;

    goto :goto_0

    .line 291
    :pswitch_6
    :try_start_0
    iget-object v4, v0, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->groups:Ljava/util/List;

    sget-object v5, Lcom/squareup/protos/client/orders/OrderGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v5, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 293
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 308
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 309
    invoke-virtual {v0}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->build()Lcom/squareup/protos/client/orders/OrderClientDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 254
    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/orders/OrderClientDetails$ProtoAdapter_OrderClientDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/protos/client/orders/OrderClientDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/orders/OrderClientDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 273
    sget-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/protos/client/orders/OrderClientDetails;->groups:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 274
    sget-object v0, Lcom/squareup/protos/client/orders/Channel;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/OrderClientDetails;->channel:Lcom/squareup/protos/client/orders/Channel;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 275
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/OrderClientDetails;->fulfillments_display_name:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 276
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_title:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 277
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_description:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 278
    sget-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/OrderClientDetails;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 279
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/protos/client/orders/OrderClientDetails;->external_link:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 280
    invoke-virtual {p2}, Lcom/squareup/protos/client/orders/OrderClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 254
    check-cast p2, Lcom/squareup/protos/client/orders/OrderClientDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/protos/client/orders/OrderClientDetails$ProtoAdapter_OrderClientDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/protos/client/orders/OrderClientDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/protos/client/orders/OrderClientDetails;)I
    .locals 4

    .line 261
    sget-object v0, Lcom/squareup/protos/client/orders/OrderGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;->groups:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/orders/Channel;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;->channel:Lcom/squareup/protos/client/orders/Channel;

    const/4 v3, 0x2

    .line 262
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;->fulfillments_display_name:Ljava/lang/String;

    const/4 v3, 0x3

    .line 263
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_title:Ljava/lang/String;

    const/4 v3, 0x4

    .line 264
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;->localized_prompt_description:Ljava/lang/String;

    const/4 v3, 0x5

    .line 265
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    const/4 v3, 0x6

    .line 266
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/protos/client/orders/OrderClientDetails;->external_link:Ljava/lang/String;

    const/4 v3, 0x7

    .line 267
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 268
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/OrderClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 254
    check-cast p1, Lcom/squareup/protos/client/orders/OrderClientDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/orders/OrderClientDetails$ProtoAdapter_OrderClientDetails;->encodedSize(Lcom/squareup/protos/client/orders/OrderClientDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/protos/client/orders/OrderClientDetails;)Lcom/squareup/protos/client/orders/OrderClientDetails;
    .locals 2

    .line 314
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/OrderClientDetails;->newBuilder()Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;

    move-result-object p1

    .line 315
    iget-object v0, p1, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->channel:Lcom/squareup/protos/client/orders/Channel;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/client/orders/Channel;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->channel:Lcom/squareup/protos/client/orders/Channel;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/orders/Channel;

    iput-object v0, p1, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->channel:Lcom/squareup/protos/client/orders/Channel;

    .line 316
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    iput-object v0, p1, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->order_display_state_data:Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    .line 317
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 318
    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/OrderClientDetails$Builder;->build()Lcom/squareup/protos/client/orders/OrderClientDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 254
    check-cast p1, Lcom/squareup/protos/client/orders/OrderClientDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/orders/OrderClientDetails$ProtoAdapter_OrderClientDetails;->redact(Lcom/squareup/protos/client/orders/OrderClientDetails;)Lcom/squareup/protos/client/orders/OrderClientDetails;

    move-result-object p1

    return-object p1
.end method
