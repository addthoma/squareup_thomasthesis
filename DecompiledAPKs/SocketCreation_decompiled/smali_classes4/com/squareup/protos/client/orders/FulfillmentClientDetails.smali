.class public final Lcom/squareup/protos/client/orders/FulfillmentClientDetails;
.super Lcom/squareup/wire/Message;
.source "FulfillmentClientDetails.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/orders/FulfillmentClientDetails$ProtoAdapter_FulfillmentClientDetails;,
        Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;,
        Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/orders/FulfillmentClientDetails;",
        "Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/orders/FulfillmentClientDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FULFILLMENT_WINDOW_ENDS_AT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final available_actions:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.Action#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/Action;",
            ">;"
        }
    .end annotation
.end field

.field public final courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.FulfillmentClientDetails$Courier#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final fulfillment_window_ends_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$ProtoAdapter_FulfillmentClientDetails;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$ProtoAdapter_FulfillmentClientDetails;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/Action;",
            ">;",
            "Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 58
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;-><init>(Ljava/util/List;Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/orders/Action;",
            ">;",
            "Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 63
    sget-object v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p4, "available_actions"

    .line 64
    invoke-static {p4, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->available_actions:Ljava/util/List;

    .line 65
    iput-object p2, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    .line 66
    iput-object p3, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->fulfillment_window_ends_at:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 82
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 83
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->available_actions:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->available_actions:Ljava/util/List;

    .line 85
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    .line 86
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->fulfillment_window_ends_at:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->fulfillment_window_ends_at:Ljava/lang/String;

    .line 87
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 92
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 94
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->available_actions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 96
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 97
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->fulfillment_window_ends_at:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 98
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;
    .locals 2

    .line 71
    new-instance v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;-><init>()V

    .line 72
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->available_actions:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->available_actions:Ljava/util/List;

    .line 73
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    .line 74
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->fulfillment_window_ends_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->fulfillment_window_ends_at:Ljava/lang/String;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->newBuilder()Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->available_actions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", available_actions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->available_actions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    if-eqz v1, :cond_1

    const-string v1, ", courier="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 108
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->fulfillment_window_ends_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", fulfillment_window_ends_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->fulfillment_window_ends_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FulfillmentClientDetails{"

    .line 109
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
