.class public final Lcom/squareup/protos/client/orders/UpdateOrderResponse;
.super Lcom/squareup/wire/Message;
.source "UpdateOrderResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/orders/UpdateOrderResponse$ProtoAdapter_UpdateOrderResponse;,
        Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/orders/UpdateOrderResponse;",
        "Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/orders/UpdateOrderResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final order:Lcom/squareup/orders/model/Order;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final status:Lcom/squareup/protos/client/Status;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.Status#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/orders/UpdateOrderResponse$ProtoAdapter_UpdateOrderResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/UpdateOrderResponse$ProtoAdapter_UpdateOrderResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/orders/model/Order;)V
    .locals 1

    .line 50
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/protos/client/orders/UpdateOrderResponse;-><init>(Lcom/squareup/protos/client/Status;Lcom/squareup/orders/model/Order;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/Status;Lcom/squareup/orders/model/Order;Lokio/ByteString;)V
    .locals 1

    .line 54
    sget-object v0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 55
    iput-object p1, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->status:Lcom/squareup/protos/client/Status;

    .line 56
    iput-object p2, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->order:Lcom/squareup/orders/model/Order;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 71
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/orders/UpdateOrderResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 72
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/orders/UpdateOrderResponse;

    .line 73
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v3, p1, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->status:Lcom/squareup/protos/client/Status;

    .line 74
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->order:Lcom/squareup/orders/model/Order;

    iget-object p1, p1, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->order:Lcom/squareup/orders/model/Order;

    .line 75
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 80
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 82
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->status:Lcom/squareup/protos/client/Status;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/Status;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->order:Lcom/squareup/orders/model/Order;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 85
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;
    .locals 2

    .line 61
    new-instance v0, Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;-><init>()V

    .line 62
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->status:Lcom/squareup/protos/client/Status;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;->status:Lcom/squareup/protos/client/Status;

    .line 63
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->order:Lcom/squareup/orders/model/Order;

    iput-object v1, v0, Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;->order:Lcom/squareup/orders/model/Order;

    .line 64
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->newBuilder()Lcom/squareup/protos/client/orders/UpdateOrderResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 92
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 93
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->status:Lcom/squareup/protos/client/Status;

    if-eqz v1, :cond_0

    const-string v1, ", status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->status:Lcom/squareup/protos/client/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 94
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->order:Lcom/squareup/orders/model/Order;

    if-eqz v1, :cond_1

    const-string v1, ", order="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/UpdateOrderResponse;->order:Lcom/squareup/orders/model/Order;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "UpdateOrderResponse{"

    .line 95
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
