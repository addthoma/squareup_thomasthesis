.class public final Lcom/squareup/protos/client/orders/AcceptAction$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AcceptAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/AcceptAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/AcceptAction;",
        "Lcom/squareup/protos/client/orders/AcceptAction$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public accepted_at:Ljava/lang/String;

.field public pickup_at:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 102
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public accepted_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/AcceptAction$Builder;
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/protos/client/orders/AcceptAction$Builder;->accepted_at:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/orders/AcceptAction;
    .locals 4

    .line 125
    new-instance v0, Lcom/squareup/protos/client/orders/AcceptAction;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/AcceptAction$Builder;->accepted_at:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/AcceptAction$Builder;->pickup_at:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/orders/AcceptAction;-><init>(Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 97
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/AcceptAction$Builder;->build()Lcom/squareup/protos/client/orders/AcceptAction;

    move-result-object v0

    return-object v0
.end method

.method public pickup_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/AcceptAction$Builder;
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/protos/client/orders/AcceptAction$Builder;->pickup_at:Ljava/lang/String;

    return-object p0
.end method
