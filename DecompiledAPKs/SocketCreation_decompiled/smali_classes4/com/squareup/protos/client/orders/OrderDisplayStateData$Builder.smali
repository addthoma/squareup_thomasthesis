.class public final Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OrderDisplayStateData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/orders/OrderDisplayStateData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/orders/OrderDisplayStateData;",
        "Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public display_name:Ljava/lang/String;

.field public overdue_at:Ljava/lang/String;

.field public state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 133
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/orders/OrderDisplayStateData;
    .locals 5

    .line 170
    new-instance v0, Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    iget-object v1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    iget-object v2, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;->display_name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;->overdue_at:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/orders/OrderDisplayStateData;-><init>(Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 126
    invoke-virtual {p0}, Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;->build()Lcom/squareup/protos/client/orders/OrderDisplayStateData;

    move-result-object v0

    return-object v0
.end method

.method public display_name(Ljava/lang/String;)Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;->display_name:Ljava/lang/String;

    return-object p0
.end method

.method public overdue_at(Ljava/lang/String;)Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;
    .locals 0

    .line 164
    iput-object p1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;->overdue_at:Ljava/lang/String;

    return-object p0
.end method

.method public state(Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;)Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/protos/client/orders/OrderDisplayStateData$Builder;->state:Lcom/squareup/protos/client/orders/OrderDisplayStateData$OrderDisplayState;

    return-object p0
.end method
