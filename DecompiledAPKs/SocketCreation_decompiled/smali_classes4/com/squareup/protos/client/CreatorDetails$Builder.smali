.class public final Lcom/squareup/protos/client/CreatorDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreatorDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/CreatorDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/CreatorDetails;",
        "Lcom/squareup/protos/client/CreatorDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public device_credential:Lcom/squareup/protos/client/DeviceCredential;

.field public employee:Lcom/squareup/protos/client/Employee;

.field public read_only_mobile_staff:Lcom/squareup/protos/client/MobileStaff;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 121
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/CreatorDetails;
    .locals 5

    .line 154
    new-instance v0, Lcom/squareup/protos/client/CreatorDetails;

    iget-object v1, p0, Lcom/squareup/protos/client/CreatorDetails$Builder;->read_only_mobile_staff:Lcom/squareup/protos/client/MobileStaff;

    iget-object v2, p0, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee:Lcom/squareup/protos/client/Employee;

    iget-object v3, p0, Lcom/squareup/protos/client/CreatorDetails$Builder;->device_credential:Lcom/squareup/protos/client/DeviceCredential;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/CreatorDetails;-><init>(Lcom/squareup/protos/client/MobileStaff;Lcom/squareup/protos/client/Employee;Lcom/squareup/protos/client/DeviceCredential;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/CreatorDetails$Builder;->build()Lcom/squareup/protos/client/CreatorDetails;

    move-result-object v0

    return-object v0
.end method

.method public device_credential(Lcom/squareup/protos/client/DeviceCredential;)Lcom/squareup/protos/client/CreatorDetails$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lcom/squareup/protos/client/CreatorDetails$Builder;->device_credential:Lcom/squareup/protos/client/DeviceCredential;

    return-object p0
.end method

.method public employee(Lcom/squareup/protos/client/Employee;)Lcom/squareup/protos/client/CreatorDetails$Builder;
    .locals 0

    .line 136
    iput-object p1, p0, Lcom/squareup/protos/client/CreatorDetails$Builder;->employee:Lcom/squareup/protos/client/Employee;

    return-object p0
.end method

.method public read_only_mobile_staff(Lcom/squareup/protos/client/MobileStaff;)Lcom/squareup/protos/client/CreatorDetails$Builder;
    .locals 0

    .line 128
    iput-object p1, p0, Lcom/squareup/protos/client/CreatorDetails$Builder;->read_only_mobile_staff:Lcom/squareup/protos/client/MobileStaff;

    return-object p0
.end method
