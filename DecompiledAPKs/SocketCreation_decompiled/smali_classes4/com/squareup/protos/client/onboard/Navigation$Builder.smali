.class public final Lcom/squareup/protos/client/onboard/Navigation$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Navigation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/Navigation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/Navigation;",
        "Lcom/squareup/protos/client/onboard/Navigation$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public can_go_back:Ljava/lang/Boolean;

.field public exit_dialog:Lcom/squareup/protos/client/onboard/Dialog;

.field public navigation_buttons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/NavigationButton;",
            ">;"
        }
    .end annotation
.end field

.field public timeout_action:Ljava/lang/String;

.field public timeout_duration:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 142
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 143
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->navigation_buttons:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/onboard/Navigation;
    .locals 8

    .line 174
    new-instance v7, Lcom/squareup/protos/client/onboard/Navigation;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->can_go_back:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->navigation_buttons:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->exit_dialog:Lcom/squareup/protos/client/onboard/Dialog;

    iget-object v4, p0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->timeout_duration:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->timeout_action:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/onboard/Navigation;-><init>(Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/client/onboard/Dialog;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 131
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/Navigation$Builder;->build()Lcom/squareup/protos/client/onboard/Navigation;

    move-result-object v0

    return-object v0
.end method

.method public can_go_back(Ljava/lang/Boolean;)Lcom/squareup/protos/client/onboard/Navigation$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->can_go_back:Ljava/lang/Boolean;

    return-object p0
.end method

.method public exit_dialog(Lcom/squareup/protos/client/onboard/Dialog;)Lcom/squareup/protos/client/onboard/Navigation$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->exit_dialog:Lcom/squareup/protos/client/onboard/Dialog;

    return-object p0
.end method

.method public navigation_buttons(Ljava/util/List;)Lcom/squareup/protos/client/onboard/Navigation$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/onboard/NavigationButton;",
            ">;)",
            "Lcom/squareup/protos/client/onboard/Navigation$Builder;"
        }
    .end annotation

    .line 152
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 153
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->navigation_buttons:Ljava/util/List;

    return-object p0
.end method

.method public timeout_action(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Navigation$Builder;
    .locals 0

    .line 168
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->timeout_action:Ljava/lang/String;

    return-object p0
.end method

.method public timeout_duration(Ljava/lang/Integer;)Lcom/squareup/protos/client/onboard/Navigation$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/Navigation$Builder;->timeout_duration:Ljava/lang/Integer;

    return-object p0
.end method
