.class public final Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StartSessionRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/onboard/StartSessionRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/onboard/StartSessionRequest;",
        "Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public client_request_uuid:Ljava/lang/String;

.field public country_code:Lcom/squareup/protos/common/countries/Country;

.field public flow_name:Ljava/lang/String;

.field public language_code:Lcom/squareup/protos/common/languages/Language;

.field public spec_version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 148
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/onboard/StartSessionRequest;
    .locals 8

    .line 182
    new-instance v7, Lcom/squareup/protos/client/onboard/StartSessionRequest;

    iget-object v1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->country_code:Lcom/squareup/protos/common/countries/Country;

    iget-object v2, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->language_code:Lcom/squareup/protos/common/languages/Language;

    iget-object v3, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->flow_name:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->spec_version:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->client_request_uuid:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/onboard/StartSessionRequest;-><init>(Lcom/squareup/protos/common/countries/Country;Lcom/squareup/protos/common/languages/Language;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v7
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 137
    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->build()Lcom/squareup/protos/client/onboard/StartSessionRequest;

    move-result-object v0

    return-object v0
.end method

.method public client_request_uuid(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->client_request_uuid:Ljava/lang/String;

    return-object p0
.end method

.method public country_code(Lcom/squareup/protos/common/countries/Country;)Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->country_code:Lcom/squareup/protos/common/countries/Country;

    return-object p0
.end method

.method public flow_name(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;
    .locals 0

    .line 166
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->flow_name:Ljava/lang/String;

    return-object p0
.end method

.method public language_code(Lcom/squareup/protos/common/languages/Language;)Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;
    .locals 0

    .line 161
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->language_code:Lcom/squareup/protos/common/languages/Language;

    return-object p0
.end method

.method public spec_version(Ljava/lang/Integer;)Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lcom/squareup/protos/client/onboard/StartSessionRequest$Builder;->spec_version:Ljava/lang/Integer;

    return-object p0
.end method
