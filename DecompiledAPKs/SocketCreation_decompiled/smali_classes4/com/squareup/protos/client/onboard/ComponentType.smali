.class public final enum Lcom/squareup/protos/client/onboard/ComponentType;
.super Ljava/lang/Enum;
.source "ComponentType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/onboard/ComponentType$ProtoAdapter_ComponentType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/onboard/ComponentType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/onboard/ComponentType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ADDRESS:Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final enum BANK_ACCOUNT:Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final enum BUTTON:Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final enum COMPONENT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final enum DATE_PICKER:Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final enum DROPDOWN_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final enum HARDWARE_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final enum IMAGE:Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final enum MULTIPLE_SELECT_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final enum PANEL_TITLE:Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final enum PARAGRAPH:Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final enum PERSON_NAME:Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final enum PHONE_NUMBER:Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final enum SINGLE_SELECT_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

.field public static final enum TEXT_FIELD:Lcom/squareup/protos/client/onboard/ComponentType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 11
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/4 v1, 0x0

    const-string v2, "COMPONENT_TYPE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->COMPONENT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/onboard/ComponentType;

    .line 13
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/4 v2, 0x1

    const-string v3, "PANEL_TITLE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->PANEL_TITLE:Lcom/squareup/protos/client/onboard/ComponentType;

    .line 15
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/4 v3, 0x2

    const-string v4, "TEXT_FIELD"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->TEXT_FIELD:Lcom/squareup/protos/client/onboard/ComponentType;

    .line 17
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/4 v4, 0x3

    const-string v5, "SINGLE_SELECT_LIST"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->SINGLE_SELECT_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    .line 19
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/4 v5, 0x4

    const-string v6, "MULTIPLE_SELECT_LIST"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->MULTIPLE_SELECT_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    .line 21
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/4 v6, 0x5

    const-string v7, "DATE_PICKER"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->DATE_PICKER:Lcom/squareup/protos/client/onboard/ComponentType;

    .line 23
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/4 v7, 0x6

    const-string v8, "BUTTON"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->BUTTON:Lcom/squareup/protos/client/onboard/ComponentType;

    .line 25
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/4 v8, 0x7

    const-string v9, "PARAGRAPH"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->PARAGRAPH:Lcom/squareup/protos/client/onboard/ComponentType;

    .line 27
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/16 v9, 0x8

    const-string v10, "IMAGE"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->IMAGE:Lcom/squareup/protos/client/onboard/ComponentType;

    .line 29
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/16 v10, 0x9

    const-string v11, "PERSON_NAME"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->PERSON_NAME:Lcom/squareup/protos/client/onboard/ComponentType;

    .line 31
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/16 v11, 0xa

    const-string v12, "ADDRESS"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->ADDRESS:Lcom/squareup/protos/client/onboard/ComponentType;

    .line 33
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/16 v12, 0xb

    const-string v13, "PHONE_NUMBER"

    invoke-direct {v0, v13, v12, v12}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->PHONE_NUMBER:Lcom/squareup/protos/client/onboard/ComponentType;

    .line 35
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/16 v13, 0xc

    const-string v14, "BANK_ACCOUNT"

    invoke-direct {v0, v14, v13, v13}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->BANK_ACCOUNT:Lcom/squareup/protos/client/onboard/ComponentType;

    .line 37
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/16 v14, 0xd

    const-string v15, "DROPDOWN_LIST"

    invoke-direct {v0, v15, v14, v14}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->DROPDOWN_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    .line 39
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType;

    const/16 v15, 0xe

    const-string v14, "HARDWARE_LIST"

    invoke-direct {v0, v14, v15, v15}, Lcom/squareup/protos/client/onboard/ComponentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->HARDWARE_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/squareup/protos/client/onboard/ComponentType;

    .line 10
    sget-object v14, Lcom/squareup/protos/client/onboard/ComponentType;->COMPONENT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/onboard/ComponentType;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->PANEL_TITLE:Lcom/squareup/protos/client/onboard/ComponentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->TEXT_FIELD:Lcom/squareup/protos/client/onboard/ComponentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->SINGLE_SELECT_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->MULTIPLE_SELECT_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->DATE_PICKER:Lcom/squareup/protos/client/onboard/ComponentType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->BUTTON:Lcom/squareup/protos/client/onboard/ComponentType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->PARAGRAPH:Lcom/squareup/protos/client/onboard/ComponentType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->IMAGE:Lcom/squareup/protos/client/onboard/ComponentType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->PERSON_NAME:Lcom/squareup/protos/client/onboard/ComponentType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->ADDRESS:Lcom/squareup/protos/client/onboard/ComponentType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->PHONE_NUMBER:Lcom/squareup/protos/client/onboard/ComponentType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->BANK_ACCOUNT:Lcom/squareup/protos/client/onboard/ComponentType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->DROPDOWN_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/onboard/ComponentType;->HARDWARE_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    aput-object v1, v0, v15

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->$VALUES:[Lcom/squareup/protos/client/onboard/ComponentType;

    .line 41
    new-instance v0, Lcom/squareup/protos/client/onboard/ComponentType$ProtoAdapter_ComponentType;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/ComponentType$ProtoAdapter_ComponentType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput p3, p0, Lcom/squareup/protos/client/onboard/ComponentType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/onboard/ComponentType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 68
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->HARDWARE_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    .line 67
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->DROPDOWN_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    .line 66
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->BANK_ACCOUNT:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    .line 65
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->PHONE_NUMBER:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    .line 64
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->ADDRESS:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    .line 63
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->PERSON_NAME:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    .line 62
    :pswitch_6
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->IMAGE:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    .line 61
    :pswitch_7
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->PARAGRAPH:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    .line 60
    :pswitch_8
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->BUTTON:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    .line 59
    :pswitch_9
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->DATE_PICKER:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    .line 58
    :pswitch_a
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->MULTIPLE_SELECT_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    .line 57
    :pswitch_b
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->SINGLE_SELECT_LIST:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    .line 56
    :pswitch_c
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->TEXT_FIELD:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    .line 55
    :pswitch_d
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->PANEL_TITLE:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    .line 54
    :pswitch_e
    sget-object p0, Lcom/squareup/protos/client/onboard/ComponentType;->COMPONENT_TYPE_DO_NOT_USE:Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/ComponentType;
    .locals 1

    .line 10
    const-class v0, Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/onboard/ComponentType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/onboard/ComponentType;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/protos/client/onboard/ComponentType;->$VALUES:[Lcom/squareup/protos/client/onboard/ComponentType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/onboard/ComponentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/onboard/ComponentType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 75
    iget v0, p0, Lcom/squareup/protos/client/onboard/ComponentType;->value:I

    return v0
.end method
