.class public final Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetInstantDepositFeeDetailsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;",
        "Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public fee_basis_points:Ljava/lang/Integer;

.field public fee_fixed_amount:Lcom/squareup/protos/common/Money;

.field public fee_total_amount:Lcom/squareup/protos/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 121
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;
    .locals 5

    .line 153
    new-instance v0, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->fee_basis_points:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->fee_fixed_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p0, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->fee_total_amount:Lcom/squareup/protos/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;-><init>(Ljava/lang/Integer;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 114
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->build()Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse;

    move-result-object v0

    return-object v0
.end method

.method public fee_basis_points(Ljava/lang/Integer;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->fee_basis_points:Ljava/lang/Integer;

    return-object p0
.end method

.method public fee_fixed_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;
    .locals 0

    .line 138
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->fee_fixed_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public fee_total_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;
    .locals 0

    .line 147
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetInstantDepositFeeDetailsResponse$Builder;->fee_total_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method
