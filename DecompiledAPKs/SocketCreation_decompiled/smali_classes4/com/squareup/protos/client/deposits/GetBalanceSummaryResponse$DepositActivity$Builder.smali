.class public final Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBalanceSummaryResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public pending_deposit:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            ">;"
        }
    .end annotation
.end field

.field public total_daily_sales:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 414
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 415
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;->pending_deposit:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;
    .locals 4

    .line 437
    new-instance v0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;->total_daily_sales:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    iget-object v2, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;->pending_deposit:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;-><init>(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 409
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;->build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    move-result-object v0

    return-object v0
.end method

.method public pending_deposit(Ljava/util/List;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/settlements/SettlementReportWrapper;",
            ">;)",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;"
        }
    .end annotation

    .line 430
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 431
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;->pending_deposit:Ljava/util/List;

    return-object p0
.end method

.method public total_daily_sales(Lcom/squareup/protos/client/settlements/SettlementReportWrapper;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;
    .locals 0

    .line 422
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity$Builder;->total_daily_sales:Lcom/squareup/protos/client/settlements/SettlementReportWrapper;

    return-object p0
.end method
