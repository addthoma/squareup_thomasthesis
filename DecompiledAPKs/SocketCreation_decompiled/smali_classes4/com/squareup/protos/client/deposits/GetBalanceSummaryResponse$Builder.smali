.class public final Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "GetBalanceSummaryResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;",
        "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_partial_deposit:Ljava/lang/Boolean;

.field public balance:Lcom/squareup/protos/common/Money;

.field public balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

.field public card_activity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;"
        }
    .end annotation
.end field

.field public deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

.field public eligibility_details:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/deposits/EligibilityDetails;",
            ">;"
        }
    .end annotation
.end field

.field public instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

.field public linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

.field public linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

.field public square_card:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 242
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 243
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->square_card:Ljava/util/List;

    .line 244
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->card_activity:Ljava/util/List;

    .line 245
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->eligibility_details:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public allow_partial_deposit(Ljava/lang/Boolean;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;
    .locals 0

    .line 293
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->allow_partial_deposit:Ljava/lang/Boolean;

    return-object p0
.end method

.method public balance(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;
    .locals 0

    .line 252
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public balance_information(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;
    .locals 0

    .line 315
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    return-object p0
.end method

.method public build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;
    .locals 13

    .line 330
    new-instance v12, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    iget-object v1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->balance:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    iget-object v3, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    iget-object v4, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    iget-object v5, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->square_card:Ljava/util/List;

    iget-object v6, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->allow_partial_deposit:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->card_activity:Ljava/util/List;

    iget-object v8, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    iget-object v9, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->balance_information:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;

    iget-object v10, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->eligibility_details:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;-><init>(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/deposits/InstantDepositDetails;Lcom/squareup/protos/client/bankaccount/BankAccount;Lcom/squareup/protos/client/deposits/CardInfo;Ljava/util/List;Ljava/lang/Boolean;Ljava/util/List;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$BalanceInformation;Ljava/util/List;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 221
    invoke-virtual {p0}, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->build()Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse;

    move-result-object v0

    return-object v0
.end method

.method public card_activity(Ljava/util/List;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/CardActivityEvent;",
            ">;)",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;"
        }
    .end annotation

    .line 301
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 302
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->card_activity:Ljava/util/List;

    return-object p0
.end method

.method public deposit_activity(Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->deposit_activity:Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$DepositActivity;

    return-object p0
.end method

.method public eligibility_details(Ljava/util/List;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/deposits/EligibilityDetails;",
            ">;)",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;"
        }
    .end annotation

    .line 323
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 324
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->eligibility_details:Ljava/util/List;

    return-object p0
.end method

.method public instant_deposit_details(Lcom/squareup/protos/client/deposits/InstantDepositDetails;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;
    .locals 0

    .line 260
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->instant_deposit_details:Lcom/squareup/protos/client/deposits/InstantDepositDetails;

    return-object p0
.end method

.method public linked_bank_account(Lcom/squareup/protos/client/bankaccount/BankAccount;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;
    .locals 0

    .line 268
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->linked_bank_account:Lcom/squareup/protos/client/bankaccount/BankAccount;

    return-object p0
.end method

.method public linked_card(Lcom/squareup/protos/client/deposits/CardInfo;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;
    .locals 0

    .line 276
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->linked_card:Lcom/squareup/protos/client/deposits/CardInfo;

    return-object p0
.end method

.method public square_card(Ljava/util/List;)Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/bizbank/ListCardsResponse$CardData;",
            ">;)",
            "Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;"
        }
    .end annotation

    .line 284
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 285
    iput-object p1, p0, Lcom/squareup/protos/client/deposits/GetBalanceSummaryResponse$Builder;->square_card:Ljava/util/List;

    return-object p0
.end method
