.class public final enum Lcom/squareup/protos/client/deposits/BalanceActivityType;
.super Ljava/lang/Enum;
.source "BalanceActivityType.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/deposits/BalanceActivityType$ProtoAdapter_BalanceActivityType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/protos/client/deposits/BalanceActivityType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/protos/client/deposits/BalanceActivityType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/deposits/BalanceActivityType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum DEBIT_CARD_PAY_IN:Lcom/squareup/protos/client/deposits/BalanceActivityType;

.field public static final enum DO_NOT_USE:Lcom/squareup/protos/client/deposits/BalanceActivityType;

.field public static final enum INSTANT_PAYOUT:Lcom/squareup/protos/client/deposits/BalanceActivityType;

.field public static final enum SAME_DAY_PAYOUT:Lcom/squareup/protos/client/deposits/BalanceActivityType;

.field public static final enum STANDARD_SPEED_PAYOUT:Lcom/squareup/protos/client/deposits/BalanceActivityType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 14
    new-instance v0, Lcom/squareup/protos/client/deposits/BalanceActivityType;

    const/4 v1, 0x0

    const-string v2, "DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/protos/client/deposits/BalanceActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->DO_NOT_USE:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    .line 16
    new-instance v0, Lcom/squareup/protos/client/deposits/BalanceActivityType;

    const/4 v2, 0x1

    const-string v3, "DEBIT_CARD_PAY_IN"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/protos/client/deposits/BalanceActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->DEBIT_CARD_PAY_IN:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    .line 18
    new-instance v0, Lcom/squareup/protos/client/deposits/BalanceActivityType;

    const/4 v3, 0x2

    const-string v4, "SAME_DAY_PAYOUT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/protos/client/deposits/BalanceActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->SAME_DAY_PAYOUT:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    .line 20
    new-instance v0, Lcom/squareup/protos/client/deposits/BalanceActivityType;

    const/4 v4, 0x3

    const-string v5, "STANDARD_SPEED_PAYOUT"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/protos/client/deposits/BalanceActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->STANDARD_SPEED_PAYOUT:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    .line 22
    new-instance v0, Lcom/squareup/protos/client/deposits/BalanceActivityType;

    const/4 v5, 0x4

    const-string v6, "INSTANT_PAYOUT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/protos/client/deposits/BalanceActivityType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->INSTANT_PAYOUT:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/protos/client/deposits/BalanceActivityType;

    .line 13
    sget-object v6, Lcom/squareup/protos/client/deposits/BalanceActivityType;->DO_NOT_USE:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/protos/client/deposits/BalanceActivityType;->DEBIT_CARD_PAY_IN:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/client/deposits/BalanceActivityType;->SAME_DAY_PAYOUT:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/protos/client/deposits/BalanceActivityType;->STANDARD_SPEED_PAYOUT:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/protos/client/deposits/BalanceActivityType;->INSTANT_PAYOUT:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->$VALUES:[Lcom/squareup/protos/client/deposits/BalanceActivityType;

    .line 24
    new-instance v0, Lcom/squareup/protos/client/deposits/BalanceActivityType$ProtoAdapter_BalanceActivityType;

    invoke-direct {v0}, Lcom/squareup/protos/client/deposits/BalanceActivityType$ProtoAdapter_BalanceActivityType;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 29
    iput p3, p0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/protos/client/deposits/BalanceActivityType;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 41
    :cond_0
    sget-object p0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->INSTANT_PAYOUT:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    return-object p0

    .line 40
    :cond_1
    sget-object p0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->STANDARD_SPEED_PAYOUT:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    return-object p0

    .line 39
    :cond_2
    sget-object p0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->SAME_DAY_PAYOUT:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    return-object p0

    .line 38
    :cond_3
    sget-object p0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->DEBIT_CARD_PAY_IN:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    return-object p0

    .line 37
    :cond_4
    sget-object p0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->DO_NOT_USE:Lcom/squareup/protos/client/deposits/BalanceActivityType;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/protos/client/deposits/BalanceActivityType;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/protos/client/deposits/BalanceActivityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/deposits/BalanceActivityType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/protos/client/deposits/BalanceActivityType;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->$VALUES:[Lcom/squareup/protos/client/deposits/BalanceActivityType;

    invoke-virtual {v0}, [Lcom/squareup/protos/client/deposits/BalanceActivityType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/protos/client/deposits/BalanceActivityType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 48
    iget v0, p0, Lcom/squareup/protos/client/deposits/BalanceActivityType;->value:I

    return v0
.end method
