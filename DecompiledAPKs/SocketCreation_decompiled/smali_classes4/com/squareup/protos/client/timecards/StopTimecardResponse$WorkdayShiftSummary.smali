.class public final Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;
.super Lcom/squareup/wire/Message;
.source "StopTimecardResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/StopTimecardResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WorkdayShiftSummary"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$ProtoAdapter_WorkdayShiftSummary;,
        Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;,
        Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;",
        "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PAID_SECONDS:Ljava/lang/Long;

.field public static final DEFAULT_TOTAL_SECONDS:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final job_summaries:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.StopTimecardResponse$WorkdayShiftSummary$JobSummary#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;",
            ">;"
        }
    .end annotation
.end field

.field public final paid_seconds:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x1
    .end annotation
.end field

.field public final total_seconds:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#UINT64"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 163
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$ProtoAdapter_WorkdayShiftSummary;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$ProtoAdapter_WorkdayShiftSummary;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 167
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->DEFAULT_PAID_SECONDS:Ljava/lang/Long;

    .line 169
    sput-object v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->DEFAULT_TOTAL_SECONDS:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;",
            ">;)V"
        }
    .end annotation

    .line 198
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$JobSummary;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 203
    sget-object v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 204
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->paid_seconds:Ljava/lang/Long;

    .line 205
    iput-object p2, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->total_seconds:Ljava/lang/Long;

    const-string p1, "job_summaries"

    .line 206
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 222
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 223
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;

    .line 224
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->paid_seconds:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->paid_seconds:Ljava/lang/Long;

    .line 225
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->total_seconds:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->total_seconds:Ljava/lang/Long;

    .line 226
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    .line 227
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 232
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 234
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 235
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->paid_seconds:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 236
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->total_seconds:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 237
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;
    .locals 2

    .line 211
    new-instance v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;-><init>()V

    .line 212
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->paid_seconds:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;->paid_seconds:Ljava/lang/Long;

    .line 213
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->total_seconds:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;->total_seconds:Ljava/lang/Long;

    .line 214
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;->job_summaries:Ljava/util/List;

    .line 215
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 162
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->newBuilder()Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 246
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->paid_seconds:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", paid_seconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->paid_seconds:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 247
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->total_seconds:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", total_seconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->total_seconds:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 248
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", job_summaries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StopTimecardResponse$WorkdayShiftSummary;->job_summaries:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "WorkdayShiftSummary{"

    .line 249
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
