.class public final Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;
.super Lcom/squareup/wire/Message;
.source "StartTimecardBreakResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$ProtoAdapter_StartTimecardBreakResponse;,
        Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;",
        "Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BREAK_DEFINITION_OUTDATED:Ljava/lang/Boolean;

.field public static final DEFAULT_VALID:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final break_definition_outdated:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final break_definitions:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.TimecardBreakDefinition#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
            ">;"
        }
    .end annotation
.end field

.field public final timecard:Lcom/squareup/protos/client/timecards/Timecard;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.Timecard#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.timecards.TimecardBreak#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final valid:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$ProtoAdapter_StartTimecardBreakResponse;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$ProtoAdapter_StartTimecardBreakResponse;-><init>()V

    sput-object v0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 26
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->DEFAULT_VALID:Ljava/lang/Boolean;

    .line 28
    sput-object v0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->DEFAULT_BREAK_DEFINITION_OUTDATED:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/timecards/Timecard;",
            "Lcom/squareup/protos/client/timecards/TimecardBreak;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 64
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;-><init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/client/timecards/Timecard;Lcom/squareup/protos/client/timecards/TimecardBreak;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/timecards/Timecard;",
            "Lcom/squareup/protos/client/timecards/TimecardBreak;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/timecards/TimecardBreakDefinition;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 70
    sget-object v0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 71
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 72
    iput-object p2, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    const-string p1, "break_definitions"

    .line 73
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->break_definitions:Ljava/util/List;

    .line 74
    iput-object p4, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    .line 75
    iput-object p5, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->break_definition_outdated:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 93
    :cond_0
    instance-of v1, p1, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 94
    :cond_1
    check-cast p1, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;

    .line 95
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 96
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 97
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->break_definitions:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->break_definitions:Ljava/util/List;

    .line 98
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    .line 99
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->break_definition_outdated:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->break_definition_outdated:Ljava/lang/Boolean;

    .line 100
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 105
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 107
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 108
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/Timecard;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 109
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/client/timecards/TimecardBreak;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 110
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->break_definitions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 111
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->break_definition_outdated:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 113
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;
    .locals 2

    .line 80
    new-instance v0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;-><init>()V

    .line 81
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    .line 82
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    .line 83
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->break_definitions:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->break_definitions:Ljava/util/List;

    .line 84
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->valid:Ljava/lang/Boolean;

    .line 85
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->break_definition_outdated:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->break_definition_outdated:Ljava/lang/Boolean;

    .line 86
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->newBuilder()Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 120
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 121
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    if-eqz v1, :cond_0

    const-string v1, ", timecard="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard:Lcom/squareup/protos/client/timecards/Timecard;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 122
    :cond_0
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    if-eqz v1, :cond_1

    const-string v1, ", timecard_break="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->timecard_break:Lcom/squareup/protos/client/timecards/TimecardBreak;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 123
    :cond_1
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->break_definitions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", break_definitions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->break_definitions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124
    :cond_2
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", valid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->valid:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125
    :cond_3
    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->break_definition_outdated:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", break_definition_outdated="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/StartTimecardBreakResponse;->break_definition_outdated:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "StartTimecardBreakResponse{"

    .line 126
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
