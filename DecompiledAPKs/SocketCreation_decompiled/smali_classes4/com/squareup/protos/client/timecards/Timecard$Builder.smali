.class public final Lcom/squareup/protos/client/timecards/Timecard$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Timecard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/protos/client/timecards/Timecard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/protos/client/timecards/Timecard;",
        "Lcom/squareup/protos/client/timecards/Timecard$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public clockin_timestamp_ms:Ljava/lang/Long;

.field public clockin_unit_token:Ljava/lang/String;

.field public clockout_timestamp_ms:Ljava/lang/Long;

.field public clockout_unit_token:Ljava/lang/String;

.field public deleted:Ljava/lang/Boolean;

.field public employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

.field public employee_token:Ljava/lang/String;

.field public hourly_wage:Lcom/squareup/protos/common/Money;

.field public timecard_notes:Ljava/lang/String;

.field public token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 255
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/protos/client/timecards/Timecard;
    .locals 13

    .line 341
    new-instance v12, Lcom/squareup/protos/client/timecards/Timecard;

    iget-object v1, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->employee_token:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockin_unit_token:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockout_unit_token:Ljava/lang/String;

    iget-object v5, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->deleted:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockin_timestamp_ms:Ljava/lang/Long;

    iget-object v7, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockout_timestamp_ms:Ljava/lang/Long;

    iget-object v8, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->hourly_wage:Lcom/squareup/protos/common/Money;

    iget-object v9, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    iget-object v10, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->timecard_notes:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v11

    move-object v0, v12

    invoke-direct/range {v0 .. v11}, Lcom/squareup/protos/client/timecards/Timecard;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;Ljava/lang/Long;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;Ljava/lang/String;Lokio/ByteString;)V

    return-object v12
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 234
    invoke-virtual {p0}, Lcom/squareup/protos/client/timecards/Timecard$Builder;->build()Lcom/squareup/protos/client/timecards/Timecard;

    move-result-object v0

    return-object v0
.end method

.method public clockin_timestamp_ms(Ljava/lang/Long;)Lcom/squareup/protos/client/timecards/Timecard$Builder;
    .locals 0

    .line 302
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockin_timestamp_ms:Ljava/lang/Long;

    return-object p0
.end method

.method public clockin_unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/Timecard$Builder;
    .locals 0

    .line 278
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockin_unit_token:Ljava/lang/String;

    return-object p0
.end method

.method public clockout_timestamp_ms(Ljava/lang/Long;)Lcom/squareup/protos/client/timecards/Timecard$Builder;
    .locals 0

    .line 310
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockout_timestamp_ms:Ljava/lang/Long;

    return-object p0
.end method

.method public clockout_unit_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/Timecard$Builder;
    .locals 0

    .line 286
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->clockout_unit_token:Ljava/lang/String;

    return-object p0
.end method

.method public deleted(Ljava/lang/Boolean;)Lcom/squareup/protos/client/timecards/Timecard$Builder;
    .locals 0

    .line 294
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->deleted:Ljava/lang/Boolean;

    return-object p0
.end method

.method public employee_job_info(Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;)Lcom/squareup/protos/client/timecards/Timecard$Builder;
    .locals 0

    .line 327
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->employee_job_info:Lcom/squareup/protos/client/employeejobs/EmployeeJobInfo;

    return-object p0
.end method

.method public employee_token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/Timecard$Builder;
    .locals 0

    .line 270
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->employee_token:Ljava/lang/String;

    return-object p0
.end method

.method public hourly_wage(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/client/timecards/Timecard$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 319
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->hourly_wage:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public timecard_notes(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/Timecard$Builder;
    .locals 0

    .line 335
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->timecard_notes:Ljava/lang/String;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lcom/squareup/protos/client/timecards/Timecard$Builder;
    .locals 0

    .line 262
    iput-object p1, p0, Lcom/squareup/protos/client/timecards/Timecard$Builder;->token:Ljava/lang/String;

    return-object p0
.end method
